//
//  ModelBtn.h
//  乐销
//
//  Created by 隋林栋 on 2016/12/20.
//  Copyright © 2016年 ping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelBtn : NSObject

@property (nonatomic, strong) NSString * imageName;
@property (nonatomic, strong) NSString * highImageName;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) UIColor * color;
@property int tag;

+ (instancetype)modelWithTitle:(NSString *)title;
+ (instancetype)modelWithTitle:(NSString *)title
                           tag:(int)tag;
+ (instancetype)modelWithTitle:(NSString *)title
                           imageName:(NSString *)imageName
                                 tag:(int)tag;
+ (instancetype)modelWithTitle:(NSString *)title
                     imageName:(NSString *)imageName
                 highImageName:(NSString *)highImageName
                           tag:(int)tag;
+ (instancetype)modelWithTitle:(NSString *)title
                     imageName:(NSString *)imageName
                 highImageName:(NSString *)highImageName
                           tag:(int)tag
                         color:(UIColor *)color;

@end
