//
//  DateArrModel.h
//  神农岛
//
//  Created by 宋晨光 on 16/6/3.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^YearArrayBlock)(NSMutableArray *yArr);
typedef void(^MonthArrayBlock)(NSMutableArray *mArr);

typedef void(^YearIndexBlock)(NSInteger yearIndex);
typedef void(^MonthIndexBlock)(NSInteger monthIndex);

@interface DateArrModel : NSObject

@property(strong,nonatomic)NSMutableArray *yearArr;
@property(strong,nonatomic)NSMutableArray *mothArr;
@property(strong,nonatomic)NSMutableArray *dayArr;

@property(strong,nonatomic)NSString *year;
@property(strong,nonatomic)NSString *month;
@property(strong,nonatomic)NSString *day;

@property (nonatomic ,assign)NSUInteger nowYear;
@property (nonatomic ,assign)NSUInteger nowMonth;
@property (nonatomic ,assign)NSUInteger nowDay;
@property (nonatomic ,assign)NSUInteger nowHour;
@property (nonatomic ,assign)NSUInteger nowMinute;

//@property (nonatomic ,strong)void (^ArrayBlock)(NSMutableArray *yearArr);

-(void)setYearArrayModelWithMinIndex:(int)minIndex maxIndex:(int)maxIndex yearBlock:(YearArrayBlock)yearBlock monthBlock:(MonthArrayBlock)monthBlock;
-(void)getIndexWithTime:(NSString *)time indexYearBlock:(YearIndexBlock)indexYear indexMonthBlock:(MonthIndexBlock)indexMonth;
@end
