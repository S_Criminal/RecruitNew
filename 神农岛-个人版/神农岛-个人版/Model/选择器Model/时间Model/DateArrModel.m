//
//  DateArrModel.m
//  神农岛
//
//  Created by 宋晨光 on 16/6/3.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "DateArrModel.h"

@implementation DateArrModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initData];
        
    }
    return self;
}

-(NSUInteger )nowYear
{
    _nowYear =  [self getCalendatarModelIndex:1];
    return _nowYear ;
}
-(NSUInteger)nowMonth
{
    _nowMonth = [self getCalendatarModelIndex:2];
    return _nowMonth;
}
-(NSUInteger)nowDay
{
    _nowDay = [self getCalendatarModelIndex:3];
    return _nowDay;
}

-(NSUInteger)nowHour
{
    _nowHour = [self getCalendatarModelIndex:4];
    return _nowHour;
}

-(NSUInteger)nowMinute
{
    _nowMinute = [self getCalendatarModelIndex:5];
    return _nowMinute;
}


-(NSUInteger)getCalendatarModelIndex:(NSInteger)index
{
    NSDate *now = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:now];
    NSUInteger nowDate;
    if (index == 1)
    {
        nowDate = [dateComponent year];
    }else if (index == 2){
        nowDate = [dateComponent month];
    }else if (index == 3){
        nowDate = [dateComponent day];
    }
    
    return nowDate;
}

/*
 *  年月数组
 *  @brief maxIndex 当前年份 + maxIndex （0为当前年份）
 */
-(void)setYearArrayModelWithMinIndex:(int)minIndex maxIndex:(int)maxIndex yearBlock:(YearArrayBlock)yearBlock monthBlock:(MonthArrayBlock)monthBlock
{
    self.yearArr = [NSMutableArray array];
    self.mothArr = [NSMutableArray array];
    
    for (int i = minIndex; i <= [self.year integerValue] + maxIndex ; i ++)
    {
        [self.yearArr addObject:[NSString stringWithFormat:@"%d年",i]];
    }
    for (int i = 1; i <= 12 ; i++) {
        if (i < 10) {
            [self.mothArr addObject:[NSString stringWithFormat:@"0%d月",i]];
        }else{
            [self.mothArr addObject:[NSString stringWithFormat:@"%d月",i]];
        }
    }
    yearBlock(self.yearArr);
    monthBlock(self.mothArr);
}

-(void)getIndexWithTime:(NSString *)time indexYearBlock:(YearIndexBlock)indexYear indexMonthBlock:(MonthIndexBlock)indexMonth
{
    NSString * birthYear = [time substringToIndex:4];
    NSString * birthMonth= [time substringWithRange:NSMakeRange(5, 2)];
    //    startCellStr = [NSString stringWithFormat:@"%@-%@-01",birthYear,birthMonth];
    //    birthYear = [self.entLabel.text substringToIndex:4];
    //    birthMonth= [self.entLabel.text substringWithRange:NSMakeRange(5, 2)];
    NSString *year;
    NSString *month;
    for (int i = 0; i < _yearArr.count ; i ++) {
        year = [_yearArr[i] substringToIndex:4];
        if ([birthYear isEqualToString:year])
        {
            indexYear(i);
        }
    }
    for (int i = 0; i < _mothArr.count; i++) {
        
        month = self.mothArr[i];
        
        NSLog(@"%@",month);
        
        if (month.length == 2)
        {
            month = [NSString stringWithFormat:@"0%@",month];
        }
        
        if (month.length == 3)
        {
            month = [month substringToIndex:2];
        }
        
        if ([birthMonth isEqualToString:month])
        {
            indexMonth(i);
        }
    }
}

#pragma mark - dataSource (关于时间选择器)
-(void)initData
{
    NSArray *array = [self getsystemtime];
    //    self.model = [[BirthDayModel alloc]init];
    //    self.model.year = array[0];
    //    self.model.month = array[1];
    //    self.model.day = array[2];
    self.year = array[0];
    self.month = array[1];
    self.day = array[2];
    _yearArr = [NSMutableArray array];
    NSString *yearSystem = array[0];
    int yearCount = [yearSystem intValue];
    yearCount = yearCount - 50;
    for (int i = yearCount; i<yearCount+55; i++) {
        NSString *year = [NSString stringWithFormat:@"%d",i];
        [_yearArr addObject:year];
    }
    _mothArr = [NSMutableArray array];
    for (int i = 1; i<13; i++) {
        NSString *moth = [NSString stringWithFormat:@"%d",i];
        [_mothArr addObject:moth];
    }
    
    //计算2月份的天数
    _dayArr = [NSMutableArray array];
    if ([self.month isEqualToString:@"01"] || [self.month isEqualToString:@"03"] || [self.month isEqualToString:@"05"] || [self.month isEqualToString:@"07"] || [self.month isEqualToString:@"08"] || [self.month isEqualToString:@"10"] || [self.month isEqualToString:@"12"]) {
        
        for (int i = 1; i<32; i++) {
            NSString *day = [NSString stringWithFormat:@"%d",i];
            [_dayArr addObject:day];
        }
        
    }else if ([self.month isEqualToString:@"04"] || [self.month isEqualToString:@"06"] || [self.month isEqualToString:@"09"] || [self.month isEqualToString:@"11"]){
        
        for (int i = 1; i<31; i++) {
            NSString *day = [NSString stringWithFormat:@"%d",i];
            [_dayArr addObject:day];
        }
        
    }else{
        //2月份  可以被400整除 或者是 被4整除不被100整除
        if (([self.year intValue]%4==0 && [self.year intValue]%100!=0) || [self.year intValue]%400==0) {
            //闰29
            for (int i = 1; i<30; i++) {
                NSString *day = [NSString stringWithFormat:@"%d",i];
                [_dayArr addObject:day];
            }
            
        }else{
            //平28
            for (int i = 1; i<29; i++) {
                NSString *day = [NSString stringWithFormat:@"%d",i];
                [_dayArr addObject:day];
            }
            
        }
    }
}

// 获取系统时间
-(NSArray*)getsystemtime{
    
    NSDate *date = [NSDate date];
    NSTimeInterval  sec = [date timeIntervalSinceNow];
    NSDate *currentDate = [[NSDate alloc]initWithTimeIntervalSinceNow:sec];
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *na = [df stringFromDate:currentDate];
    return [na componentsSeparatedByString:@"-"];
}

@end
