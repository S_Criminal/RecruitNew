//
//  BaseClass.m
//
//  Created by 晨光 宋 on 17/1/6
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//


#import "PickerEduModel.h"


NSString *const PickerClassEID = @"EID";
NSString *const PickerClassEname = @"Ename";


@interface PickerEduModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PickerEduModel

@synthesize eID = _eID;
@synthesize ename = _ename;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.eID = [[self objectOrNilForKey:PickerClassEID fromDictionary:dict] doubleValue];
            self.ename = [self objectOrNilForKey:PickerClassEname fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eID] forKey:PickerClassEID];
    [mutableDict setValue:self.ename forKey:PickerClassEname];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.eID = [aDecoder decodeDoubleForKey:PickerClassEID];
    self.ename = [aDecoder decodeObjectForKey:PickerClassEname];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_eID forKey:PickerClassEID];
    [aCoder encodeObject:_ename forKey:PickerClassEname];
}

- (id)copyWithZone:(NSZone *)zone {
    PickerEduModel *copy = [[PickerEduModel alloc] init];
    
    
    
    if (copy) {

        copy.eID = self.eID;
        copy.ename = [self.ename copyWithZone:zone];
    }
    
    return copy;
}


@end
