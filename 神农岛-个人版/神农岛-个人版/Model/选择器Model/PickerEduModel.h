//
//  BaseClass.h
//
//  Created by 晨光 宋 on 17/1/6
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PickerEduModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double eID;
@property (nonatomic, strong) NSString *ename;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
