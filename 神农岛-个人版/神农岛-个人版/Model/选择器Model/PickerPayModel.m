//
//  BaseClass.m
//
//  Created by 晨光 宋 on 17/1/6
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PickerPayModel.h"

NSString *const kBaseClassSID = @"SID";
NSString *const kBaseClassSname = @"Sname";


@interface PickerPayModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PickerPayModel

@synthesize sID = _sID;
@synthesize sname = _sname;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.sID = [[self objectOrNilForKey:kBaseClassSID fromDictionary:dict] doubleValue];
            self.sname = [self objectOrNilForKey:kBaseClassSname fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sID] forKey:kBaseClassSID];
    [mutableDict setValue:self.sname forKey:kBaseClassSname];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.sID = [aDecoder decodeDoubleForKey:kBaseClassSID];
    self.sname = [aDecoder decodeObjectForKey:kBaseClassSname];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:_sID forKey:kBaseClassSID];
    [aCoder encodeObject:_sname forKey:kBaseClassSname];
}

- (id)copyWithZone:(NSZone *)zone {
    PickerPayModel *copy = [[PickerPayModel alloc] init];
    
    
    
    if (copy) {

        copy.sID = self.sID;
        copy.sname = [self.sname copyWithZone:zone];
    }
    
    return copy;
}


@end
