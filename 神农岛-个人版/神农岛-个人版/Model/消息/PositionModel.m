//
//  PositionModel.m
//
//  Created by 晨光 宋 on 17/1/16
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PositionModel.h"


NSString *const kPositionModelPProvice = @"P_Provice";
NSString *const kPositionModelPExp = @"P_Exp";
NSString *const kPositionModelPEducation = @"P_Education";
NSString *const kPositionModelCScale = @"C_Scale";
NSString *const kPositionModelCreateDate = @"CreateDate";
NSString *const kPositionModelUID = @"U_ID";
NSString *const kPositionModelCNature = @"C_Nature";
NSString *const kPositionModelPArea = @"P_Area";
NSString *const kPositionModelCIndustry = @"C_Industry";
NSString *const kPositionModelCNickName = @"C_NickName";
NSString *const kPositionModelIName = @"I_Name";
NSString *const kPositionModelCLogo = @"C_Logo";
NSString *const kPositionModelPPayE = @"P_PayE";
NSString *const kPositionModelCIndustryse = @"C_Industryse";
NSString *const kPositionModelTypes = @"Types";
NSString *const kPositionModelPNature = @"P_Nature";
NSString *const kPositionModelUpdateDate = @"UpdateDate";
NSString *const kPositionModelPTemptation = @"P_Temptation";
NSString *const kPositionModelPType = @"P_Type";
NSString *const kPositionModelPPayS = @"P_PayS";
NSString *const kPositionModelPName = @"P_Name";
NSString *const kPositionModelCAduitStatus = @"C_AduitStatus";
NSString *const kPositionModelINameP = @"I_NameP";
NSString *const kPositionModelID = @"ID";
NSString *const kPositionModelPCity = @"P_City";
NSString *const kPositionModelPAddress = @"P_Address";
NSString *const kPositionModelCName = @"C_Name";
NSString *const kPositionModelPID = @"P_ID";
NSString *const kPositionModelCIndustrys = @"C_Industrys";
NSString *const kPositionModelPPay = @"P_Pay";
NSString *const kPositionModelWName = @"W_Name";
NSString *const kPositionModelStatus = @"Status";


@interface PositionModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PositionModel

@synthesize pProvice = _pProvice;
@synthesize pExp = _pExp;
@synthesize pEducation = _pEducation;
@synthesize cScale = _cScale;
@synthesize createDate = _createDate;
@synthesize uID = _uID;
@synthesize cNature = _cNature;
@synthesize pArea = _pArea;
@synthesize cIndustry = _cIndustry;
@synthesize cNickName = _cNickName;
@synthesize iName = _iName;
@synthesize cLogo = _cLogo;
@synthesize pPayE = _pPayE;
@synthesize cIndustryse = _cIndustryse;
@synthesize types = _types;
@synthesize pNature = _pNature;
@synthesize updateDate = _updateDate;
@synthesize pTemptation = _pTemptation;
@synthesize pType = _pType;
@synthesize pPayS = _pPayS;
@synthesize pName = _pName;
@synthesize cAduitStatus = _cAduitStatus;
@synthesize iNameP = _iNameP;
@synthesize iDProperty = _iDProperty;
@synthesize pCity = _pCity;
@synthesize pAddress = _pAddress;
@synthesize cName = _cName;
@synthesize pID = _pID;
@synthesize cIndustrys = _cIndustrys;
@synthesize pPay = _pPay;
@synthesize wName = _wName;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pProvice = [self objectOrNilForKey:kPositionModelPProvice fromDictionary:dict];
            self.pExp = [self objectOrNilForKey:kPositionModelPExp fromDictionary:dict];
            self.pEducation = [self objectOrNilForKey:kPositionModelPEducation fromDictionary:dict];
            self.cScale = [self objectOrNilForKey:kPositionModelCScale fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kPositionModelCreateDate fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kPositionModelUID fromDictionary:dict] doubleValue];
            self.cNature = [[self objectOrNilForKey:kPositionModelCNature fromDictionary:dict] doubleValue];
            self.pArea = [self objectOrNilForKey:kPositionModelPArea fromDictionary:dict];
            self.cIndustry = [[self objectOrNilForKey:kPositionModelCIndustry fromDictionary:dict] doubleValue];
            self.cNickName = [self objectOrNilForKey:kPositionModelCNickName fromDictionary:dict];
            self.iName = [self objectOrNilForKey:kPositionModelIName fromDictionary:dict];
            self.cLogo = [self objectOrNilForKey:kPositionModelCLogo fromDictionary:dict];
            self.pPayE = [[self objectOrNilForKey:kPositionModelPPayE fromDictionary:dict] doubleValue];
            self.cIndustryse = [self objectOrNilForKey:kPositionModelCIndustryse fromDictionary:dict];
            self.types = [[self objectOrNilForKey:kPositionModelTypes fromDictionary:dict] doubleValue];
            self.pNature = [self objectOrNilForKey:kPositionModelPNature fromDictionary:dict];
            self.updateDate = [self objectOrNilForKey:kPositionModelUpdateDate fromDictionary:dict];
            self.pTemptation = [self objectOrNilForKey:kPositionModelPTemptation fromDictionary:dict];
            self.pType = [[self objectOrNilForKey:kPositionModelPType fromDictionary:dict] doubleValue];
            self.pPayS = [[self objectOrNilForKey:kPositionModelPPayS fromDictionary:dict] doubleValue];
            self.pName = [self objectOrNilForKey:kPositionModelPName fromDictionary:dict];
            self.cAduitStatus = [[self objectOrNilForKey:kPositionModelCAduitStatus fromDictionary:dict] doubleValue];
            self.iNameP = [self objectOrNilForKey:kPositionModelINameP fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kPositionModelID fromDictionary:dict] doubleValue];
            self.pCity = [self objectOrNilForKey:kPositionModelPCity fromDictionary:dict];
            self.pAddress = [self objectOrNilForKey:kPositionModelPAddress fromDictionary:dict];
            self.cName = [self objectOrNilForKey:kPositionModelCName fromDictionary:dict];
            self.pID = [[self objectOrNilForKey:kPositionModelPID fromDictionary:dict] doubleValue];
            self.cIndustrys = [[self objectOrNilForKey:kPositionModelCIndustrys fromDictionary:dict] doubleValue];
            self.pPay = [self objectOrNilForKey:kPositionModelPPay fromDictionary:dict];
            self.wName = [self objectOrNilForKey:kPositionModelWName fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kPositionModelStatus fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.pProvice forKey:kPositionModelPProvice];
    [mutableDict setValue:self.pExp forKey:kPositionModelPExp];
    [mutableDict setValue:self.pEducation forKey:kPositionModelPEducation];
    [mutableDict setValue:self.cScale forKey:kPositionModelCScale];
    [mutableDict setValue:self.createDate forKey:kPositionModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kPositionModelUID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cNature] forKey:kPositionModelCNature];
    [mutableDict setValue:self.pArea forKey:kPositionModelPArea];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustry] forKey:kPositionModelCIndustry];
    [mutableDict setValue:self.cNickName forKey:kPositionModelCNickName];
    [mutableDict setValue:self.iName forKey:kPositionModelIName];
    [mutableDict setValue:self.cLogo forKey:kPositionModelCLogo];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayE] forKey:kPositionModelPPayE];
    [mutableDict setValue:self.cIndustryse forKey:kPositionModelCIndustryse];
    [mutableDict setValue:[NSNumber numberWithDouble:self.types] forKey:kPositionModelTypes];
    [mutableDict setValue:self.pNature forKey:kPositionModelPNature];
    [mutableDict setValue:self.updateDate forKey:kPositionModelUpdateDate];
    [mutableDict setValue:self.pTemptation forKey:kPositionModelPTemptation];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pType] forKey:kPositionModelPType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayS] forKey:kPositionModelPPayS];
    [mutableDict setValue:self.pName forKey:kPositionModelPName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cAduitStatus] forKey:kPositionModelCAduitStatus];
    [mutableDict setValue:self.iNameP forKey:kPositionModelINameP];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kPositionModelID];
    [mutableDict setValue:self.pCity forKey:kPositionModelPCity];
    [mutableDict setValue:self.pAddress forKey:kPositionModelPAddress];
    [mutableDict setValue:self.cName forKey:kPositionModelCName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pID] forKey:kPositionModelPID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustrys] forKey:kPositionModelCIndustrys];
    [mutableDict setValue:self.pPay forKey:kPositionModelPPay];
    [mutableDict setValue:self.wName forKey:kPositionModelWName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kPositionModelStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.pProvice = [aDecoder decodeObjectForKey:kPositionModelPProvice];
    self.pExp = [aDecoder decodeObjectForKey:kPositionModelPExp];
    self.pEducation = [aDecoder decodeObjectForKey:kPositionModelPEducation];
    self.cScale = [aDecoder decodeObjectForKey:kPositionModelCScale];
    self.createDate = [aDecoder decodeObjectForKey:kPositionModelCreateDate];
    self.uID = [aDecoder decodeDoubleForKey:kPositionModelUID];
    self.cNature = [aDecoder decodeDoubleForKey:kPositionModelCNature];
    self.pArea = [aDecoder decodeObjectForKey:kPositionModelPArea];
    self.cIndustry = [aDecoder decodeDoubleForKey:kPositionModelCIndustry];
    self.cNickName = [aDecoder decodeObjectForKey:kPositionModelCNickName];
    self.iName = [aDecoder decodeObjectForKey:kPositionModelIName];
    self.cLogo = [aDecoder decodeObjectForKey:kPositionModelCLogo];
    self.pPayE = [aDecoder decodeDoubleForKey:kPositionModelPPayE];
    self.cIndustryse = [aDecoder decodeObjectForKey:kPositionModelCIndustryse];
    self.types = [aDecoder decodeDoubleForKey:kPositionModelTypes];
    self.pNature = [aDecoder decodeObjectForKey:kPositionModelPNature];
    self.updateDate = [aDecoder decodeObjectForKey:kPositionModelUpdateDate];
    self.pTemptation = [aDecoder decodeObjectForKey:kPositionModelPTemptation];
    self.pType = [aDecoder decodeDoubleForKey:kPositionModelPType];
    self.pPayS = [aDecoder decodeDoubleForKey:kPositionModelPPayS];
    self.pName = [aDecoder decodeObjectForKey:kPositionModelPName];
    self.cAduitStatus = [aDecoder decodeDoubleForKey:kPositionModelCAduitStatus];
    self.iNameP = [aDecoder decodeObjectForKey:kPositionModelINameP];
    self.iDProperty = [aDecoder decodeDoubleForKey:kPositionModelID];
    self.pCity = [aDecoder decodeObjectForKey:kPositionModelPCity];
    self.pAddress = [aDecoder decodeObjectForKey:kPositionModelPAddress];
    self.cName = [aDecoder decodeObjectForKey:kPositionModelCName];
    self.pID = [aDecoder decodeDoubleForKey:kPositionModelPID];
    self.cIndustrys = [aDecoder decodeDoubleForKey:kPositionModelCIndustrys];
    self.pPay = [aDecoder decodeObjectForKey:kPositionModelPPay];
    self.wName = [aDecoder decodeObjectForKey:kPositionModelWName];
    self.status = [aDecoder decodeDoubleForKey:kPositionModelStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_pProvice forKey:kPositionModelPProvice];
    [aCoder encodeObject:_pExp forKey:kPositionModelPExp];
    [aCoder encodeObject:_pEducation forKey:kPositionModelPEducation];
    [aCoder encodeObject:_cScale forKey:kPositionModelCScale];
    [aCoder encodeObject:_createDate forKey:kPositionModelCreateDate];
    [aCoder encodeDouble:_uID forKey:kPositionModelUID];
    [aCoder encodeDouble:_cNature forKey:kPositionModelCNature];
    [aCoder encodeObject:_pArea forKey:kPositionModelPArea];
    [aCoder encodeDouble:_cIndustry forKey:kPositionModelCIndustry];
    [aCoder encodeObject:_cNickName forKey:kPositionModelCNickName];
    [aCoder encodeObject:_iName forKey:kPositionModelIName];
    [aCoder encodeObject:_cLogo forKey:kPositionModelCLogo];
    [aCoder encodeDouble:_pPayE forKey:kPositionModelPPayE];
    [aCoder encodeObject:_cIndustryse forKey:kPositionModelCIndustryse];
    [aCoder encodeDouble:_types forKey:kPositionModelTypes];
    [aCoder encodeObject:_pNature forKey:kPositionModelPNature];
    [aCoder encodeObject:_updateDate forKey:kPositionModelUpdateDate];
    [aCoder encodeObject:_pTemptation forKey:kPositionModelPTemptation];
    [aCoder encodeDouble:_pType forKey:kPositionModelPType];
    [aCoder encodeDouble:_pPayS forKey:kPositionModelPPayS];
    [aCoder encodeObject:_pName forKey:kPositionModelPName];
    [aCoder encodeDouble:_cAduitStatus forKey:kPositionModelCAduitStatus];
    [aCoder encodeObject:_iNameP forKey:kPositionModelINameP];
    [aCoder encodeDouble:_iDProperty forKey:kPositionModelID];
    [aCoder encodeObject:_pCity forKey:kPositionModelPCity];
    [aCoder encodeObject:_pAddress forKey:kPositionModelPAddress];
    [aCoder encodeObject:_cName forKey:kPositionModelCName];
    [aCoder encodeDouble:_pID forKey:kPositionModelPID];
    [aCoder encodeDouble:_cIndustrys forKey:kPositionModelCIndustrys];
    [aCoder encodeObject:_pPay forKey:kPositionModelPPay];
    [aCoder encodeObject:_wName forKey:kPositionModelWName];
    [aCoder encodeDouble:_status forKey:kPositionModelStatus];
}

- (id)copyWithZone:(NSZone *)zone {
    PositionModel *copy = [[PositionModel alloc] init];
    
    
    
    if (copy) {

        copy.pProvice = [self.pProvice copyWithZone:zone];
        copy.pExp = [self.pExp copyWithZone:zone];
        copy.pEducation = [self.pEducation copyWithZone:zone];
        copy.cScale = [self.cScale copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.uID = self.uID;
        copy.cNature = self.cNature;
        copy.pArea = [self.pArea copyWithZone:zone];
        copy.cIndustry = self.cIndustry;
        copy.cNickName = [self.cNickName copyWithZone:zone];
        copy.iName = [self.iName copyWithZone:zone];
        copy.cLogo = [self.cLogo copyWithZone:zone];
        copy.pPayE = self.pPayE;
        copy.cIndustryse = [self.cIndustryse copyWithZone:zone];
        copy.types = self.types;
        copy.pNature = [self.pNature copyWithZone:zone];
        copy.updateDate = [self.updateDate copyWithZone:zone];
        copy.pTemptation = [self.pTemptation copyWithZone:zone];
        copy.pType = self.pType;
        copy.pPayS = self.pPayS;
        copy.pName = [self.pName copyWithZone:zone];
        copy.cAduitStatus = self.cAduitStatus;
        copy.iNameP = [self.iNameP copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.pCity = [self.pCity copyWithZone:zone];
        copy.pAddress = [self.pAddress copyWithZone:zone];
        copy.cName = [self.cName copyWithZone:zone];
        copy.pID = self.pID;
        copy.cIndustrys = self.cIndustrys;
        copy.pPay = [self.pPay copyWithZone:zone];
        copy.wName = [self.wName copyWithZone:zone];
        copy.status = self.status;
    }
    
    return copy;
}


@end
