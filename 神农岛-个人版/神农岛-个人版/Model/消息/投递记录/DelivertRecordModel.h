//
//  DelivertRecordModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DelivertRecordModel : NSObject

@property (nonatomic ,strong) NSString *imgStr;
@property (nonatomic ,strong) NSString *title;
@property (nonatomic ,strong) NSString *timel;
@property (nonatomic ,strong) NSString *content;

@end
