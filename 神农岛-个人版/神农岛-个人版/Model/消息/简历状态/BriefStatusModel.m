//
//  BriefStatusModel.m
//
//  Created by 晨光 宋 on 17/1/23
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "BriefStatusModel.h"


NSString *const kBriefStatusModelID = @"ID";
NSString *const kBriefStatusModelPCity = @"P_City";
NSString *const kBriefStatusModelRID = @"R_ID";
NSString *const kBriefStatusModelPPayS = @"P_PayS";
NSString *const kBriefStatusModelAMessaged = @"A_Messaged";
NSString *const kBriefStatusModelPMerry = @"P_Merry";
NSString *const kBriefStatusModelPAges = @"P_Ages";
NSString *const kBriefStatusModelCIndustry = @"C_Industry";
NSString *const kBriefStatusModelPStartD = @"P_StartD";
NSString *const kBriefStatusModelAMessagee = @"A_Messagee";
NSString *const kBriefStatusModelUpdateDate = @"UpdateDate";
NSString *const kBriefStatusModelCLogo = @"C_Logo";
NSString *const kBriefStatusModelPtNamep = @"Pt_Namep";
NSString *const kBriefStatusModelPDescription = @"P_Description";
NSString *const kBriefStatusModelPAge = @"P_Age";
NSString *const kBriefStatusModelCReplyD = @"C_ReplyD";
NSString *const kBriefStatusModelPTemptation = @"P_Temptation";
NSString *const kBriefStatusModelPName = @"P_Name";
NSString *const kBriefStatusModelARMoblie = @"AR_Moblie";
NSString *const kBriefStatusModelID1 = @"ID1";
NSString *const kBriefStatusModelHRMG = @"HRMG";
NSString *const kBriefStatusModelPType = @"P_Type";
NSString *const kBriefStatusModelCreateDate = @"CreateDate";
NSString *const kBriefStatusModelPNature = @"P_Nature";
NSString *const kBriefStatusModelPEducation = @"P_Education";
NSString *const kBriefStatusModelINameP = @"I_NameP";
NSString *const kBriefStatusModelARName = @"AR_Name";
NSString *const kBriefStatusModelCName = @"C_Name";
NSString *const kBriefStatusModelPPayID = @"P_PayID";
NSString *const kBriefStatusModelSeeDate = @"SeeDate";
NSString *const kBriefStatusModelCAduitStatus = @"C_AduitStatus";
NSString *const kBriefStatusModelCMails = @"C_Mails";
NSString *const kBriefStatusModelHandleR = @"HandleR";
NSString *const kBriefStatusModelPTypes = @"P_Types";
NSString *const kBriefStatusModelPArea = @"P_Area";
NSString *const kBriefStatusModelABack = @"A_Back";
NSString *const kBriefStatusModelIsRecomd = @"IsRecomd";
NSString *const kBriefStatusModelCProcessRate = @"C_ProcessRate";
NSString *const kBriefStatusModelPAddress = @"P_Address";
NSString *const kBriefStatusModelIName = @"I_Name";
NSString *const kBriefStatusModelPDeliver = @"P_Deliver";
NSString *const kBriefStatusModelCScale = @"C_Scale";
NSString *const kBriefStatusModelPExp = @"P_Exp";
NSString *const kBriefStatusModelPSex = @"P_Sex";
NSString *const kBriefStatusModelSeeDatea = @"SeeDatea";
NSString *const kBriefStatusModelPtName = @"Pt_Name";
NSString *const kBriefStatusModelSeeDateb = @"SeeDateb";
NSString *const kBriefStatusModelIsSee = @"IsSee";
NSString *const kBriefStatusModelSeeDatec = @"SeeDatec";
NSString *const kBriefStatusModelPMails = @"P_Mails";
NSString *const kBriefStatusModelPEducationID = @"P_EducationID";
NSString *const kBriefStatusModelSeeDated = @"SeeDated";
NSString *const kBriefStatusModelCProcessT = @"C_ProcessT";
NSString *const kBriefStatusModelSeeDatee = @"SeeDatee";
NSString *const kBriefStatusModelARJobDate = @"AR_JobDate";
NSString *const kBriefStatusModelPPay = @"P_Pay";
NSString *const kBriefStatusModelCIndustrys = @"C_Industrys";
NSString *const kBriefStatusModelPID = @"P_ID";
NSString *const kBriefStatusModelAMessage = @"A_Message";
NSString *const kBriefStatusModelPProvice = @"P_Provice";
NSString *const kBriefStatusModelAMessagea = @"A_Messagea";
NSString *const kBriefStatusModelCID = @"CID";
NSString *const kBriefStatusModelARJobAdress = @"AR_JobAdress";
NSString *const kBriefStatusModelAStatus = @"A_Status";
NSString *const kBriefStatusModelPExpID = @"P_ExpID";
NSString *const kBriefStatusModelUID = @"U_ID";
NSString *const kBriefStatusModelPPayE = @"P_PayE";
NSString *const kBriefStatusModelPPeople1 = @"P_People1";
NSString *const kBriefStatusModelAMessageb = @"A_Messageb";
NSString *const kBriefStatusModelPTypese = @"P_Typese";
NSString *const kBriefStatusModelPVisit = @"P_Visit";
NSString *const kBriefStatusModelAMessagec = @"A_Messagec";
NSString *const kBriefStatusModelIsHandle = @"IsHandle";
NSString *const kBriefStatusModelPPeople = @"P_People";
NSString *const kBriefStatusModelPEndD = @"P_EndD";


@interface BriefStatusModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BriefStatusModel

@synthesize iDProperty = _iDProperty;
@synthesize pCity = _pCity;
@synthesize rID = _rID;
@synthesize pPayS = _pPayS;
@synthesize aMessaged = _aMessaged;
@synthesize pMerry = _pMerry;
@synthesize pAges = _pAges;
@synthesize cIndustry = _cIndustry;
@synthesize pStartD = _pStartD;
@synthesize aMessagee = _aMessagee;
@synthesize updateDate = _updateDate;
@synthesize cLogo = _cLogo;
@synthesize ptNamep = _ptNamep;
@synthesize pDescription = _pDescription;
@synthesize pAge = _pAge;
@synthesize cReplyD = _cReplyD;
@synthesize pTemptation = _pTemptation;
@synthesize pName = _pName;
@synthesize aRMoblie = _aRMoblie;
@synthesize iD1 = _iD1;
@synthesize hRMG = _hRMG;
@synthesize pType = _pType;
@synthesize createDate = _createDate;
@synthesize pNature = _pNature;
@synthesize pEducation = _pEducation;
@synthesize iNameP = _iNameP;
@synthesize aRName = _aRName;
@synthesize cName = _cName;
@synthesize pPayID = _pPayID;
@synthesize seeDate = _seeDate;
@synthesize cAduitStatus = _cAduitStatus;
@synthesize cMails = _cMails;
@synthesize handleR = _handleR;
@synthesize pTypes = _pTypes;
@synthesize pArea = _pArea;
@synthesize aBack = _aBack;
@synthesize isRecomd = _isRecomd;
@synthesize cProcessRate = _cProcessRate;
@synthesize pAddress = _pAddress;
@synthesize iName = _iName;
@synthesize pDeliver = _pDeliver;
@synthesize cScale = _cScale;
@synthesize pExp = _pExp;
@synthesize pSex = _pSex;
@synthesize seeDatea = _seeDatea;
@synthesize ptName = _ptName;
@synthesize seeDateb = _seeDateb;
@synthesize isSee = _isSee;
@synthesize seeDatec = _seeDatec;
@synthesize pMails = _pMails;
@synthesize pEducationID = _pEducationID;
@synthesize seeDated = _seeDated;
@synthesize cProcessT = _cProcessT;
@synthesize seeDatee = _seeDatee;
@synthesize aRJobDate = _aRJobDate;
@synthesize pPay = _pPay;
@synthesize cIndustrys = _cIndustrys;
@synthesize pID = _pID;
@synthesize aMessage = _aMessage;
@synthesize pProvice = _pProvice;
@synthesize aMessagea = _aMessagea;
@synthesize cID = _cID;
@synthesize aRJobAdress = _aRJobAdress;
@synthesize aStatus = _aStatus;
@synthesize pExpID = _pExpID;
@synthesize uID = _uID;
@synthesize pPayE = _pPayE;
@synthesize pPeople1 = _pPeople1;
@synthesize aMessageb = _aMessageb;
@synthesize pTypese = _pTypese;
@synthesize pVisit = _pVisit;
@synthesize aMessagec = _aMessagec;
@synthesize isHandle = _isHandle;
@synthesize pPeople = _pPeople;
@synthesize pEndD = _pEndD;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kBriefStatusModelID fromDictionary:dict] doubleValue];
            self.pCity = [self objectOrNilForKey:kBriefStatusModelPCity fromDictionary:dict];
            self.rID = [[self objectOrNilForKey:kBriefStatusModelRID fromDictionary:dict] doubleValue];
            self.pPayS = [[self objectOrNilForKey:kBriefStatusModelPPayS fromDictionary:dict] doubleValue];
            self.aMessaged = [self objectOrNilForKey:kBriefStatusModelAMessaged fromDictionary:dict];
            self.pMerry = [[self objectOrNilForKey:kBriefStatusModelPMerry fromDictionary:dict] doubleValue];
            self.pAges = [[self objectOrNilForKey:kBriefStatusModelPAges fromDictionary:dict] doubleValue];
            self.cIndustry = [[self objectOrNilForKey:kBriefStatusModelCIndustry fromDictionary:dict] doubleValue];
            self.pStartD = [self objectOrNilForKey:kBriefStatusModelPStartD fromDictionary:dict];
            self.aMessagee = [self objectOrNilForKey:kBriefStatusModelAMessagee fromDictionary:dict];
            self.updateDate = [self objectOrNilForKey:kBriefStatusModelUpdateDate fromDictionary:dict];
            self.cLogo = [self objectOrNilForKey:kBriefStatusModelCLogo fromDictionary:dict];
            self.ptNamep = [self objectOrNilForKey:kBriefStatusModelPtNamep fromDictionary:dict];
            self.pDescription = [self objectOrNilForKey:kBriefStatusModelPDescription fromDictionary:dict];
            self.pAge = [[self objectOrNilForKey:kBriefStatusModelPAge fromDictionary:dict] doubleValue];
            self.cReplyD = [self objectOrNilForKey:kBriefStatusModelCReplyD fromDictionary:dict];
            self.pTemptation = [self objectOrNilForKey:kBriefStatusModelPTemptation fromDictionary:dict];
            self.pName = [self objectOrNilForKey:kBriefStatusModelPName fromDictionary:dict];
            self.aRMoblie = [self objectOrNilForKey:kBriefStatusModelARMoblie fromDictionary:dict];
            self.iD1 = [[self objectOrNilForKey:kBriefStatusModelID1 fromDictionary:dict] doubleValue];
            self.hRMG = [self objectOrNilForKey:kBriefStatusModelHRMG fromDictionary:dict];
            self.pType = [[self objectOrNilForKey:kBriefStatusModelPType fromDictionary:dict] doubleValue];
            self.createDate = [self objectOrNilForKey:kBriefStatusModelCreateDate fromDictionary:dict];
            self.pNature = [self objectOrNilForKey:kBriefStatusModelPNature fromDictionary:dict];
            self.pEducation = [self objectOrNilForKey:kBriefStatusModelPEducation fromDictionary:dict];
            self.iNameP = [self objectOrNilForKey:kBriefStatusModelINameP fromDictionary:dict];
            self.aRName = [self objectOrNilForKey:kBriefStatusModelARName fromDictionary:dict];
            self.cName = [self objectOrNilForKey:kBriefStatusModelCName fromDictionary:dict];
            self.pPayID = [[self objectOrNilForKey:kBriefStatusModelPPayID fromDictionary:dict] doubleValue];
            self.seeDate = [self objectOrNilForKey:kBriefStatusModelSeeDate fromDictionary:dict];
            self.cAduitStatus = [[self objectOrNilForKey:kBriefStatusModelCAduitStatus fromDictionary:dict] doubleValue];
            self.cMails = [self objectOrNilForKey:kBriefStatusModelCMails fromDictionary:dict];
            self.handleR = [self objectOrNilForKey:kBriefStatusModelHandleR fromDictionary:dict];
            self.pTypes = [[self objectOrNilForKey:kBriefStatusModelPTypes fromDictionary:dict] doubleValue];
            self.pArea = [self objectOrNilForKey:kBriefStatusModelPArea fromDictionary:dict];
            self.aBack = [[self objectOrNilForKey:kBriefStatusModelABack fromDictionary:dict] doubleValue];
            self.isRecomd = [[self objectOrNilForKey:kBriefStatusModelIsRecomd fromDictionary:dict] doubleValue];
            self.cProcessRate = [[self objectOrNilForKey:kBriefStatusModelCProcessRate fromDictionary:dict] doubleValue];
            self.pAddress = [self objectOrNilForKey:kBriefStatusModelPAddress fromDictionary:dict];
            self.iName = [self objectOrNilForKey:kBriefStatusModelIName fromDictionary:dict];
            self.pDeliver = [[self objectOrNilForKey:kBriefStatusModelPDeliver fromDictionary:dict] doubleValue];
            self.cScale = [self objectOrNilForKey:kBriefStatusModelCScale fromDictionary:dict];
            self.pExp = [self objectOrNilForKey:kBriefStatusModelPExp fromDictionary:dict];
            self.pSex = [[self objectOrNilForKey:kBriefStatusModelPSex fromDictionary:dict] doubleValue];
            self.seeDatea = [self objectOrNilForKey:kBriefStatusModelSeeDatea fromDictionary:dict];
            self.ptName = [self objectOrNilForKey:kBriefStatusModelPtName fromDictionary:dict];
            self.seeDateb = [self objectOrNilForKey:kBriefStatusModelSeeDateb fromDictionary:dict];
            self.isSee = [[self objectOrNilForKey:kBriefStatusModelIsSee fromDictionary:dict] doubleValue];
            self.seeDatec = [self objectOrNilForKey:kBriefStatusModelSeeDatec fromDictionary:dict];
            self.pMails = [self objectOrNilForKey:kBriefStatusModelPMails fromDictionary:dict];
            self.pEducationID = [[self objectOrNilForKey:kBriefStatusModelPEducationID fromDictionary:dict] doubleValue];
            self.seeDated = [self objectOrNilForKey:kBriefStatusModelSeeDated fromDictionary:dict];
            self.cProcessT = [[self objectOrNilForKey:kBriefStatusModelCProcessT fromDictionary:dict] doubleValue];
            self.seeDatee = [self objectOrNilForKey:kBriefStatusModelSeeDatee fromDictionary:dict];
            self.aRJobDate = [self objectOrNilForKey:kBriefStatusModelARJobDate fromDictionary:dict];
            self.pPay = [self objectOrNilForKey:kBriefStatusModelPPay fromDictionary:dict];
            self.cIndustrys = [[self objectOrNilForKey:kBriefStatusModelCIndustrys fromDictionary:dict] doubleValue];
            self.pID = [[self objectOrNilForKey:kBriefStatusModelPID fromDictionary:dict] doubleValue];
            self.aMessage = [self objectOrNilForKey:kBriefStatusModelAMessage fromDictionary:dict];
            self.pProvice = [self objectOrNilForKey:kBriefStatusModelPProvice fromDictionary:dict];
            self.aMessagea = [self objectOrNilForKey:kBriefStatusModelAMessagea fromDictionary:dict];
            self.cID = [[self objectOrNilForKey:kBriefStatusModelCID fromDictionary:dict] doubleValue];
            self.aRJobAdress = [self objectOrNilForKey:kBriefStatusModelARJobAdress fromDictionary:dict];
            self.aStatus = [[self objectOrNilForKey:kBriefStatusModelAStatus fromDictionary:dict] doubleValue];
            self.pExpID = [[self objectOrNilForKey:kBriefStatusModelPExpID fromDictionary:dict] doubleValue];
            self.uID = [[self objectOrNilForKey:kBriefStatusModelUID fromDictionary:dict] doubleValue];
            self.pPayE = [[self objectOrNilForKey:kBriefStatusModelPPayE fromDictionary:dict] doubleValue];
            self.pPeople1 = [[self objectOrNilForKey:kBriefStatusModelPPeople1 fromDictionary:dict] doubleValue];
            self.aMessageb = [self objectOrNilForKey:kBriefStatusModelAMessageb fromDictionary:dict];
            self.pTypese = [self objectOrNilForKey:kBriefStatusModelPTypese fromDictionary:dict];
            self.pVisit = [[self objectOrNilForKey:kBriefStatusModelPVisit fromDictionary:dict] doubleValue];
            self.aMessagec = [self objectOrNilForKey:kBriefStatusModelAMessagec fromDictionary:dict];
            self.isHandle = [[self objectOrNilForKey:kBriefStatusModelIsHandle fromDictionary:dict] doubleValue];
            self.pPeople = [[self objectOrNilForKey:kBriefStatusModelPPeople fromDictionary:dict] doubleValue];
            self.pEndD = [self objectOrNilForKey:kBriefStatusModelPEndD fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kBriefStatusModelID];
    [mutableDict setValue:self.pCity forKey:kBriefStatusModelPCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rID] forKey:kBriefStatusModelRID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayS] forKey:kBriefStatusModelPPayS];
    [mutableDict setValue:self.aMessaged forKey:kBriefStatusModelAMessaged];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pMerry] forKey:kBriefStatusModelPMerry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pAges] forKey:kBriefStatusModelPAges];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustry] forKey:kBriefStatusModelCIndustry];
    [mutableDict setValue:self.pStartD forKey:kBriefStatusModelPStartD];
    [mutableDict setValue:self.aMessagee forKey:kBriefStatusModelAMessagee];
    [mutableDict setValue:self.updateDate forKey:kBriefStatusModelUpdateDate];
    [mutableDict setValue:self.cLogo forKey:kBriefStatusModelCLogo];
    [mutableDict setValue:self.ptNamep forKey:kBriefStatusModelPtNamep];
    [mutableDict setValue:self.pDescription forKey:kBriefStatusModelPDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pAge] forKey:kBriefStatusModelPAge];
    [mutableDict setValue:self.cReplyD forKey:kBriefStatusModelCReplyD];
    [mutableDict setValue:self.pTemptation forKey:kBriefStatusModelPTemptation];
    [mutableDict setValue:self.pName forKey:kBriefStatusModelPName];
    [mutableDict setValue:self.aRMoblie forKey:kBriefStatusModelARMoblie];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iD1] forKey:kBriefStatusModelID1];
    [mutableDict setValue:self.hRMG forKey:kBriefStatusModelHRMG];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pType] forKey:kBriefStatusModelPType];
    [mutableDict setValue:self.createDate forKey:kBriefStatusModelCreateDate];
    [mutableDict setValue:self.pNature forKey:kBriefStatusModelPNature];
    [mutableDict setValue:self.pEducation forKey:kBriefStatusModelPEducation];
    [mutableDict setValue:self.iNameP forKey:kBriefStatusModelINameP];
    [mutableDict setValue:self.aRName forKey:kBriefStatusModelARName];
    [mutableDict setValue:self.cName forKey:kBriefStatusModelCName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayID] forKey:kBriefStatusModelPPayID];
    [mutableDict setValue:self.seeDate forKey:kBriefStatusModelSeeDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cAduitStatus] forKey:kBriefStatusModelCAduitStatus];
    [mutableDict setValue:self.cMails forKey:kBriefStatusModelCMails];
    [mutableDict setValue:self.handleR forKey:kBriefStatusModelHandleR];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pTypes] forKey:kBriefStatusModelPTypes];
    [mutableDict setValue:self.pArea forKey:kBriefStatusModelPArea];
    [mutableDict setValue:[NSNumber numberWithDouble:self.aBack] forKey:kBriefStatusModelABack];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isRecomd] forKey:kBriefStatusModelIsRecomd];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cProcessRate] forKey:kBriefStatusModelCProcessRate];
    [mutableDict setValue:self.pAddress forKey:kBriefStatusModelPAddress];
    [mutableDict setValue:self.iName forKey:kBriefStatusModelIName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pDeliver] forKey:kBriefStatusModelPDeliver];
    [mutableDict setValue:self.cScale forKey:kBriefStatusModelCScale];
    [mutableDict setValue:self.pExp forKey:kBriefStatusModelPExp];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pSex] forKey:kBriefStatusModelPSex];
    [mutableDict setValue:self.seeDatea forKey:kBriefStatusModelSeeDatea];
    [mutableDict setValue:self.ptName forKey:kBriefStatusModelPtName];
    [mutableDict setValue:self.seeDateb forKey:kBriefStatusModelSeeDateb];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isSee] forKey:kBriefStatusModelIsSee];
    [mutableDict setValue:self.seeDatec forKey:kBriefStatusModelSeeDatec];
    [mutableDict setValue:self.pMails forKey:kBriefStatusModelPMails];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pEducationID] forKey:kBriefStatusModelPEducationID];
    [mutableDict setValue:self.seeDated forKey:kBriefStatusModelSeeDated];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cProcessT] forKey:kBriefStatusModelCProcessT];
    [mutableDict setValue:self.seeDatee forKey:kBriefStatusModelSeeDatee];
    [mutableDict setValue:self.aRJobDate forKey:kBriefStatusModelARJobDate];
    [mutableDict setValue:self.pPay forKey:kBriefStatusModelPPay];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustrys] forKey:kBriefStatusModelCIndustrys];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pID] forKey:kBriefStatusModelPID];
    [mutableDict setValue:self.aMessage forKey:kBriefStatusModelAMessage];
    [mutableDict setValue:self.pProvice forKey:kBriefStatusModelPProvice];
    [mutableDict setValue:self.aMessagea forKey:kBriefStatusModelAMessagea];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cID] forKey:kBriefStatusModelCID];
    [mutableDict setValue:self.aRJobAdress forKey:kBriefStatusModelARJobAdress];
    [mutableDict setValue:[NSNumber numberWithDouble:self.aStatus] forKey:kBriefStatusModelAStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pExpID] forKey:kBriefStatusModelPExpID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kBriefStatusModelUID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayE] forKey:kBriefStatusModelPPayE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPeople1] forKey:kBriefStatusModelPPeople1];
    [mutableDict setValue:self.aMessageb forKey:kBriefStatusModelAMessageb];
    [mutableDict setValue:self.pTypese forKey:kBriefStatusModelPTypese];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pVisit] forKey:kBriefStatusModelPVisit];
    [mutableDict setValue:self.aMessagec forKey:kBriefStatusModelAMessagec];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isHandle] forKey:kBriefStatusModelIsHandle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPeople] forKey:kBriefStatusModelPPeople];
    [mutableDict setValue:self.pEndD forKey:kBriefStatusModelPEndD];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kBriefStatusModelID];
    self.pCity = [aDecoder decodeObjectForKey:kBriefStatusModelPCity];
    self.rID = [aDecoder decodeDoubleForKey:kBriefStatusModelRID];
    self.pPayS = [aDecoder decodeDoubleForKey:kBriefStatusModelPPayS];
    self.aMessaged = [aDecoder decodeObjectForKey:kBriefStatusModelAMessaged];
    self.pMerry = [aDecoder decodeDoubleForKey:kBriefStatusModelPMerry];
    self.pAges = [aDecoder decodeDoubleForKey:kBriefStatusModelPAges];
    self.cIndustry = [aDecoder decodeDoubleForKey:kBriefStatusModelCIndustry];
    self.pStartD = [aDecoder decodeObjectForKey:kBriefStatusModelPStartD];
    self.aMessagee = [aDecoder decodeObjectForKey:kBriefStatusModelAMessagee];
    self.updateDate = [aDecoder decodeObjectForKey:kBriefStatusModelUpdateDate];
    self.cLogo = [aDecoder decodeObjectForKey:kBriefStatusModelCLogo];
    self.ptNamep = [aDecoder decodeObjectForKey:kBriefStatusModelPtNamep];
    self.pDescription = [aDecoder decodeObjectForKey:kBriefStatusModelPDescription];
    self.pAge = [aDecoder decodeDoubleForKey:kBriefStatusModelPAge];
    self.cReplyD = [aDecoder decodeObjectForKey:kBriefStatusModelCReplyD];
    self.pTemptation = [aDecoder decodeObjectForKey:kBriefStatusModelPTemptation];
    self.pName = [aDecoder decodeObjectForKey:kBriefStatusModelPName];
    self.aRMoblie = [aDecoder decodeObjectForKey:kBriefStatusModelARMoblie];
    self.iD1 = [aDecoder decodeDoubleForKey:kBriefStatusModelID1];
    self.hRMG = [aDecoder decodeObjectForKey:kBriefStatusModelHRMG];
    self.pType = [aDecoder decodeDoubleForKey:kBriefStatusModelPType];
    self.createDate = [aDecoder decodeObjectForKey:kBriefStatusModelCreateDate];
    self.pNature = [aDecoder decodeObjectForKey:kBriefStatusModelPNature];
    self.pEducation = [aDecoder decodeObjectForKey:kBriefStatusModelPEducation];
    self.iNameP = [aDecoder decodeObjectForKey:kBriefStatusModelINameP];
    self.aRName = [aDecoder decodeObjectForKey:kBriefStatusModelARName];
    self.cName = [aDecoder decodeObjectForKey:kBriefStatusModelCName];
    self.pPayID = [aDecoder decodeDoubleForKey:kBriefStatusModelPPayID];
    self.seeDate = [aDecoder decodeObjectForKey:kBriefStatusModelSeeDate];
    self.cAduitStatus = [aDecoder decodeDoubleForKey:kBriefStatusModelCAduitStatus];
    self.cMails = [aDecoder decodeObjectForKey:kBriefStatusModelCMails];
    self.handleR = [aDecoder decodeObjectForKey:kBriefStatusModelHandleR];
    self.pTypes = [aDecoder decodeDoubleForKey:kBriefStatusModelPTypes];
    self.pArea = [aDecoder decodeObjectForKey:kBriefStatusModelPArea];
    self.aBack = [aDecoder decodeDoubleForKey:kBriefStatusModelABack];
    self.isRecomd = [aDecoder decodeDoubleForKey:kBriefStatusModelIsRecomd];
    self.cProcessRate = [aDecoder decodeDoubleForKey:kBriefStatusModelCProcessRate];
    self.pAddress = [aDecoder decodeObjectForKey:kBriefStatusModelPAddress];
    self.iName = [aDecoder decodeObjectForKey:kBriefStatusModelIName];
    self.pDeliver = [aDecoder decodeDoubleForKey:kBriefStatusModelPDeliver];
    self.cScale = [aDecoder decodeObjectForKey:kBriefStatusModelCScale];
    self.pExp = [aDecoder decodeObjectForKey:kBriefStatusModelPExp];
    self.pSex = [aDecoder decodeDoubleForKey:kBriefStatusModelPSex];
    self.seeDatea = [aDecoder decodeObjectForKey:kBriefStatusModelSeeDatea];
    self.ptName = [aDecoder decodeObjectForKey:kBriefStatusModelPtName];
    self.seeDateb = [aDecoder decodeObjectForKey:kBriefStatusModelSeeDateb];
    self.isSee = [aDecoder decodeDoubleForKey:kBriefStatusModelIsSee];
    self.seeDatec = [aDecoder decodeObjectForKey:kBriefStatusModelSeeDatec];
    self.pMails = [aDecoder decodeObjectForKey:kBriefStatusModelPMails];
    self.pEducationID = [aDecoder decodeDoubleForKey:kBriefStatusModelPEducationID];
    self.seeDated = [aDecoder decodeObjectForKey:kBriefStatusModelSeeDated];
    self.cProcessT = [aDecoder decodeDoubleForKey:kBriefStatusModelCProcessT];
    self.seeDatee = [aDecoder decodeObjectForKey:kBriefStatusModelSeeDatee];
    self.aRJobDate = [aDecoder decodeObjectForKey:kBriefStatusModelARJobDate];
    self.pPay = [aDecoder decodeObjectForKey:kBriefStatusModelPPay];
    self.cIndustrys = [aDecoder decodeDoubleForKey:kBriefStatusModelCIndustrys];
    self.pID = [aDecoder decodeDoubleForKey:kBriefStatusModelPID];
    self.aMessage = [aDecoder decodeObjectForKey:kBriefStatusModelAMessage];
    self.pProvice = [aDecoder decodeObjectForKey:kBriefStatusModelPProvice];
    self.aMessagea = [aDecoder decodeObjectForKey:kBriefStatusModelAMessagea];
    self.cID = [aDecoder decodeDoubleForKey:kBriefStatusModelCID];
    self.aRJobAdress = [aDecoder decodeObjectForKey:kBriefStatusModelARJobAdress];
    self.aStatus = [aDecoder decodeDoubleForKey:kBriefStatusModelAStatus];
    self.pExpID = [aDecoder decodeDoubleForKey:kBriefStatusModelPExpID];
    self.uID = [aDecoder decodeDoubleForKey:kBriefStatusModelUID];
    self.pPayE = [aDecoder decodeDoubleForKey:kBriefStatusModelPPayE];
    self.pPeople1 = [aDecoder decodeDoubleForKey:kBriefStatusModelPPeople1];
    self.aMessageb = [aDecoder decodeObjectForKey:kBriefStatusModelAMessageb];
    self.pTypese = [aDecoder decodeObjectForKey:kBriefStatusModelPTypese];
    self.pVisit = [aDecoder decodeDoubleForKey:kBriefStatusModelPVisit];
    self.aMessagec = [aDecoder decodeObjectForKey:kBriefStatusModelAMessagec];
    self.isHandle = [aDecoder decodeDoubleForKey:kBriefStatusModelIsHandle];
    self.pPeople = [aDecoder decodeDoubleForKey:kBriefStatusModelPPeople];
    self.pEndD = [aDecoder decodeObjectForKey:kBriefStatusModelPEndD];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kBriefStatusModelID];
    [aCoder encodeObject:_pCity forKey:kBriefStatusModelPCity];
    [aCoder encodeDouble:_rID forKey:kBriefStatusModelRID];
    [aCoder encodeDouble:_pPayS forKey:kBriefStatusModelPPayS];
    [aCoder encodeObject:_aMessaged forKey:kBriefStatusModelAMessaged];
    [aCoder encodeDouble:_pMerry forKey:kBriefStatusModelPMerry];
    [aCoder encodeDouble:_pAges forKey:kBriefStatusModelPAges];
    [aCoder encodeDouble:_cIndustry forKey:kBriefStatusModelCIndustry];
    [aCoder encodeObject:_pStartD forKey:kBriefStatusModelPStartD];
    [aCoder encodeObject:_aMessagee forKey:kBriefStatusModelAMessagee];
    [aCoder encodeObject:_updateDate forKey:kBriefStatusModelUpdateDate];
    [aCoder encodeObject:_cLogo forKey:kBriefStatusModelCLogo];
    [aCoder encodeObject:_ptNamep forKey:kBriefStatusModelPtNamep];
    [aCoder encodeObject:_pDescription forKey:kBriefStatusModelPDescription];
    [aCoder encodeDouble:_pAge forKey:kBriefStatusModelPAge];
    [aCoder encodeObject:_cReplyD forKey:kBriefStatusModelCReplyD];
    [aCoder encodeObject:_pTemptation forKey:kBriefStatusModelPTemptation];
    [aCoder encodeObject:_pName forKey:kBriefStatusModelPName];
    [aCoder encodeObject:_aRMoblie forKey:kBriefStatusModelARMoblie];
    [aCoder encodeDouble:_iD1 forKey:kBriefStatusModelID1];
    [aCoder encodeObject:_hRMG forKey:kBriefStatusModelHRMG];
    [aCoder encodeDouble:_pType forKey:kBriefStatusModelPType];
    [aCoder encodeObject:_createDate forKey:kBriefStatusModelCreateDate];
    [aCoder encodeObject:_pNature forKey:kBriefStatusModelPNature];
    [aCoder encodeObject:_pEducation forKey:kBriefStatusModelPEducation];
    [aCoder encodeObject:_iNameP forKey:kBriefStatusModelINameP];
    [aCoder encodeObject:_aRName forKey:kBriefStatusModelARName];
    [aCoder encodeObject:_cName forKey:kBriefStatusModelCName];
    [aCoder encodeDouble:_pPayID forKey:kBriefStatusModelPPayID];
    [aCoder encodeObject:_seeDate forKey:kBriefStatusModelSeeDate];
    [aCoder encodeDouble:_cAduitStatus forKey:kBriefStatusModelCAduitStatus];
    [aCoder encodeObject:_cMails forKey:kBriefStatusModelCMails];
    [aCoder encodeObject:_handleR forKey:kBriefStatusModelHandleR];
    [aCoder encodeDouble:_pTypes forKey:kBriefStatusModelPTypes];
    [aCoder encodeObject:_pArea forKey:kBriefStatusModelPArea];
    [aCoder encodeDouble:_aBack forKey:kBriefStatusModelABack];
    [aCoder encodeDouble:_isRecomd forKey:kBriefStatusModelIsRecomd];
    [aCoder encodeDouble:_cProcessRate forKey:kBriefStatusModelCProcessRate];
    [aCoder encodeObject:_pAddress forKey:kBriefStatusModelPAddress];
    [aCoder encodeObject:_iName forKey:kBriefStatusModelIName];
    [aCoder encodeDouble:_pDeliver forKey:kBriefStatusModelPDeliver];
    [aCoder encodeObject:_cScale forKey:kBriefStatusModelCScale];
    [aCoder encodeObject:_pExp forKey:kBriefStatusModelPExp];
    [aCoder encodeDouble:_pSex forKey:kBriefStatusModelPSex];
    [aCoder encodeObject:_seeDatea forKey:kBriefStatusModelSeeDatea];
    [aCoder encodeObject:_ptName forKey:kBriefStatusModelPtName];
    [aCoder encodeObject:_seeDateb forKey:kBriefStatusModelSeeDateb];
    [aCoder encodeDouble:_isSee forKey:kBriefStatusModelIsSee];
    [aCoder encodeObject:_seeDatec forKey:kBriefStatusModelSeeDatec];
    [aCoder encodeObject:_pMails forKey:kBriefStatusModelPMails];
    [aCoder encodeDouble:_pEducationID forKey:kBriefStatusModelPEducationID];
    [aCoder encodeObject:_seeDated forKey:kBriefStatusModelSeeDated];
    [aCoder encodeDouble:_cProcessT forKey:kBriefStatusModelCProcessT];
    [aCoder encodeObject:_seeDatee forKey:kBriefStatusModelSeeDatee];
    [aCoder encodeObject:_aRJobDate forKey:kBriefStatusModelARJobDate];
    [aCoder encodeObject:_pPay forKey:kBriefStatusModelPPay];
    [aCoder encodeDouble:_cIndustrys forKey:kBriefStatusModelCIndustrys];
    [aCoder encodeDouble:_pID forKey:kBriefStatusModelPID];
    [aCoder encodeObject:_aMessage forKey:kBriefStatusModelAMessage];
    [aCoder encodeObject:_pProvice forKey:kBriefStatusModelPProvice];
    [aCoder encodeObject:_aMessagea forKey:kBriefStatusModelAMessagea];
    [aCoder encodeDouble:_cID forKey:kBriefStatusModelCID];
    [aCoder encodeObject:_aRJobAdress forKey:kBriefStatusModelARJobAdress];
    [aCoder encodeDouble:_aStatus forKey:kBriefStatusModelAStatus];
    [aCoder encodeDouble:_pExpID forKey:kBriefStatusModelPExpID];
    [aCoder encodeDouble:_uID forKey:kBriefStatusModelUID];
    [aCoder encodeDouble:_pPayE forKey:kBriefStatusModelPPayE];
    [aCoder encodeDouble:_pPeople1 forKey:kBriefStatusModelPPeople1];
    [aCoder encodeObject:_aMessageb forKey:kBriefStatusModelAMessageb];
    [aCoder encodeObject:_pTypese forKey:kBriefStatusModelPTypese];
    [aCoder encodeDouble:_pVisit forKey:kBriefStatusModelPVisit];
    [aCoder encodeObject:_aMessagec forKey:kBriefStatusModelAMessagec];
    [aCoder encodeDouble:_isHandle forKey:kBriefStatusModelIsHandle];
    [aCoder encodeDouble:_pPeople forKey:kBriefStatusModelPPeople];
    [aCoder encodeObject:_pEndD forKey:kBriefStatusModelPEndD];
}

- (id)copyWithZone:(NSZone *)zone {
    BriefStatusModel *copy = [[BriefStatusModel alloc] init];
    
    
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.pCity = [self.pCity copyWithZone:zone];
        copy.rID = self.rID;
        copy.pPayS = self.pPayS;
        copy.aMessaged = [self.aMessaged copyWithZone:zone];
        copy.pMerry = self.pMerry;
        copy.pAges = self.pAges;
        copy.cIndustry = self.cIndustry;
        copy.pStartD = [self.pStartD copyWithZone:zone];
        copy.aMessagee = [self.aMessagee copyWithZone:zone];
        copy.updateDate = [self.updateDate copyWithZone:zone];
        copy.cLogo = [self.cLogo copyWithZone:zone];
        copy.ptNamep = [self.ptNamep copyWithZone:zone];
        copy.pDescription = [self.pDescription copyWithZone:zone];
        copy.pAge = self.pAge;
        copy.cReplyD = [self.cReplyD copyWithZone:zone];
        copy.pTemptation = [self.pTemptation copyWithZone:zone];
        copy.pName = [self.pName copyWithZone:zone];
        copy.aRMoblie = [self.aRMoblie copyWithZone:zone];
        copy.iD1 = self.iD1;
        copy.hRMG = [self.hRMG copyWithZone:zone];
        copy.pType = self.pType;
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.pNature = [self.pNature copyWithZone:zone];
        copy.pEducation = [self.pEducation copyWithZone:zone];
        copy.iNameP = [self.iNameP copyWithZone:zone];
        copy.aRName = [self.aRName copyWithZone:zone];
        copy.cName = [self.cName copyWithZone:zone];
        copy.pPayID = self.pPayID;
        copy.seeDate = [self.seeDate copyWithZone:zone];
        copy.cAduitStatus = self.cAduitStatus;
        copy.cMails = [self.cMails copyWithZone:zone];
        copy.handleR = [self.handleR copyWithZone:zone];
        copy.pTypes = self.pTypes;
        copy.pArea = [self.pArea copyWithZone:zone];
        copy.aBack = self.aBack;
        copy.isRecomd = self.isRecomd;
        copy.cProcessRate = self.cProcessRate;
        copy.pAddress = [self.pAddress copyWithZone:zone];
        copy.iName = [self.iName copyWithZone:zone];
        copy.pDeliver = self.pDeliver;
        copy.cScale = [self.cScale copyWithZone:zone];
        copy.pExp = [self.pExp copyWithZone:zone];
        copy.pSex = self.pSex;
        copy.seeDatea = [self.seeDatea copyWithZone:zone];
        copy.ptName = [self.ptName copyWithZone:zone];
        copy.seeDateb = [self.seeDateb copyWithZone:zone];
        copy.isSee = self.isSee;
        copy.seeDatec = [self.seeDatec copyWithZone:zone];
        copy.pMails = [self.pMails copyWithZone:zone];
        copy.pEducationID = self.pEducationID;
        copy.seeDated = [self.seeDated copyWithZone:zone];
        copy.cProcessT = self.cProcessT;
        copy.seeDatee = [self.seeDatee copyWithZone:zone];
        copy.aRJobDate = [self.aRJobDate copyWithZone:zone];
        copy.pPay = [self.pPay copyWithZone:zone];
        copy.cIndustrys = self.cIndustrys;
        copy.pID = self.pID;
        copy.aMessage = [self.aMessage copyWithZone:zone];
        copy.pProvice = [self.pProvice copyWithZone:zone];
        copy.aMessagea = [self.aMessagea copyWithZone:zone];
        copy.cID = self.cID;
        copy.aRJobAdress = [self.aRJobAdress copyWithZone:zone];
        copy.aStatus = self.aStatus;
        copy.pExpID = self.pExpID;
        copy.uID = self.uID;
        copy.pPayE = self.pPayE;
        copy.pPeople1 = self.pPeople1;
        copy.aMessageb = [self.aMessageb copyWithZone:zone];
        copy.pTypese = [self.pTypese copyWithZone:zone];
        copy.pVisit = self.pVisit;
        copy.aMessagec = [self.aMessagec copyWithZone:zone];
        copy.isHandle = self.isHandle;
        copy.pPeople = self.pPeople;
        copy.pEndD = [self.pEndD copyWithZone:zone];
    }
    
    return copy;
}


@end
