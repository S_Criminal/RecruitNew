//
//  BriefStatusModel.h
//
//  Created by 晨光 宋 on 17/1/23
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BriefStatusModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *pCity;
@property (nonatomic, assign) double rID;
@property (nonatomic, assign) double pPayS;
@property (nonatomic, strong) NSString *aMessaged;
@property (nonatomic, assign) double pMerry;
@property (nonatomic, assign) double pAges;
@property (nonatomic, assign) double cIndustry;
@property (nonatomic, strong) NSString *pStartD;
@property (nonatomic, strong) NSString *aMessagee;
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, strong) NSString *cLogo;
@property (nonatomic, strong) NSString *ptNamep;
@property (nonatomic, strong) NSString *pDescription;
@property (nonatomic, assign) double pAge;
@property (nonatomic, strong) NSString *cReplyD;
@property (nonatomic, strong) NSString *pTemptation;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *aRMoblie;
@property (nonatomic, assign) double iD1;
@property (nonatomic, strong) NSString *hRMG;
@property (nonatomic, assign) double pType;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *pNature;
@property (nonatomic, strong) NSString *pEducation;
@property (nonatomic, strong) NSString *iNameP;
@property (nonatomic, strong) NSString *aRName;
@property (nonatomic, strong) NSString *cName;
@property (nonatomic, assign) double pPayID;
@property (nonatomic, strong) NSString *seeDate;
@property (nonatomic, assign) double cAduitStatus;
@property (nonatomic, strong) NSString *cMails;
@property (nonatomic, strong) NSString *handleR;
@property (nonatomic, assign) double pTypes;
@property (nonatomic, strong) NSString *pArea;
@property (nonatomic, assign) double aBack;
@property (nonatomic, assign) double isRecomd;
@property (nonatomic, assign) double cProcessRate;
@property (nonatomic, strong) NSString *pAddress;
@property (nonatomic, strong) NSString *iName;
@property (nonatomic, assign) double pDeliver;
@property (nonatomic, strong) NSString *cScale;
@property (nonatomic, strong) NSString *pExp;
@property (nonatomic, assign) double pSex;
@property (nonatomic, strong) NSString *seeDatea;
@property (nonatomic, strong) NSString *ptName;
@property (nonatomic, strong) NSString *seeDateb;
@property (nonatomic, assign) double isSee;
@property (nonatomic, strong) NSString *seeDatec;
@property (nonatomic, strong) NSString *pMails;
@property (nonatomic, assign) double pEducationID;
@property (nonatomic, strong) NSString *seeDated;
@property (nonatomic, assign) double cProcessT;
@property (nonatomic, strong) NSString *seeDatee;
@property (nonatomic, strong) NSString *aRJobDate;
@property (nonatomic, strong) NSString *pPay;
@property (nonatomic, assign) double cIndustrys;
@property (nonatomic, assign) double pID;
@property (nonatomic, strong) NSString *aMessage;
@property (nonatomic, strong) NSString *pProvice;
@property (nonatomic, strong) NSString *aMessagea;
@property (nonatomic, assign) double cID;
@property (nonatomic, strong) NSString *aRJobAdress;
@property (nonatomic, assign) double aStatus;
@property (nonatomic, assign) double pExpID;
@property (nonatomic, assign) double uID;
@property (nonatomic, assign) double pPayE;
@property (nonatomic, assign) double pPeople1;
@property (nonatomic, strong) NSString *aMessageb;
@property (nonatomic, strong) NSString *pTypese;
@property (nonatomic, assign) double pVisit;
@property (nonatomic, strong) NSString *aMessagec;
@property (nonatomic, assign) double isHandle;
@property (nonatomic, assign) double pPeople;
@property (nonatomic, strong) NSString *pEndD;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
