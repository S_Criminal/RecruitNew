//
//  SeceretaryModel.h
//
//  Created by 晨光 宋 on 17/2/11
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SeceretaryModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *contents;
@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *readDate;
@property (nonatomic, assign) double uIDS;
@property (nonatomic, assign) double type;
@property (nonatomic, strong) NSString *replyDate;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double sID;
@property (nonatomic, assign) double iDProperty;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
