//
//  SeceretaryModel.m
//
//  Created by 晨光 宋 on 17/2/11
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "SeceretaryModel.h"


NSString *const kSeceretaryModelUID = @"UID";
NSString *const kSeceretaryModelContents = @"Contents";
NSString *const kSeceretaryModelStatus = @"Status";
NSString *const kSeceretaryModelReadDate = @"ReadDate";
NSString *const kSeceretaryModelUIDS = @"UIDS";
NSString *const kSeceretaryModelType = @"Type";
NSString *const kSeceretaryModelReplyDate = @"ReplyDate";
NSString *const kSeceretaryModelCreateDate = @"CreateDate";
NSString *const kSeceretaryModelSID = @"SID";
NSString *const kSeceretaryModelID = @"ID";


@interface SeceretaryModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SeceretaryModel

@synthesize uID = _uID;
@synthesize contents = _contents;
@synthesize status = _status;
@synthesize readDate = _readDate;
@synthesize uIDS = _uIDS;
@synthesize type = _type;
@synthesize replyDate = _replyDate;
@synthesize createDate = _createDate;
@synthesize sID = _sID;
@synthesize iDProperty = _iDProperty;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.uID = [[self objectOrNilForKey:kSeceretaryModelUID fromDictionary:dict] doubleValue];
            self.contents = [self objectOrNilForKey:kSeceretaryModelContents fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kSeceretaryModelStatus fromDictionary:dict] doubleValue];
            self.readDate = [self objectOrNilForKey:kSeceretaryModelReadDate fromDictionary:dict];
            self.uIDS = [[self objectOrNilForKey:kSeceretaryModelUIDS fromDictionary:dict] doubleValue];
            self.type = [[self objectOrNilForKey:kSeceretaryModelType fromDictionary:dict] doubleValue];
            self.replyDate = [self objectOrNilForKey:kSeceretaryModelReplyDate fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kSeceretaryModelCreateDate fromDictionary:dict];
            self.sID = [[self objectOrNilForKey:kSeceretaryModelSID fromDictionary:dict] doubleValue];
            self.iDProperty = [[self objectOrNilForKey:kSeceretaryModelID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kSeceretaryModelUID];
    [mutableDict setValue:self.contents forKey:kSeceretaryModelContents];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kSeceretaryModelStatus];
    [mutableDict setValue:self.readDate forKey:kSeceretaryModelReadDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uIDS] forKey:kSeceretaryModelUIDS];
    [mutableDict setValue:[NSNumber numberWithDouble:self.type] forKey:kSeceretaryModelType];
    [mutableDict setValue:self.replyDate forKey:kSeceretaryModelReplyDate];
    [mutableDict setValue:self.createDate forKey:kSeceretaryModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sID] forKey:kSeceretaryModelSID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kSeceretaryModelID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.uID = [aDecoder decodeDoubleForKey:kSeceretaryModelUID];
    self.contents = [aDecoder decodeObjectForKey:kSeceretaryModelContents];
    self.status = [aDecoder decodeDoubleForKey:kSeceretaryModelStatus];
    self.readDate = [aDecoder decodeObjectForKey:kSeceretaryModelReadDate];
    self.uIDS = [aDecoder decodeDoubleForKey:kSeceretaryModelUIDS];
    self.type = [aDecoder decodeDoubleForKey:kSeceretaryModelType];
    self.replyDate = [aDecoder decodeObjectForKey:kSeceretaryModelReplyDate];
    self.createDate = [aDecoder decodeObjectForKey:kSeceretaryModelCreateDate];
    self.sID = [aDecoder decodeDoubleForKey:kSeceretaryModelSID];
    self.iDProperty = [aDecoder decodeDoubleForKey:kSeceretaryModelID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_uID forKey:kSeceretaryModelUID];
    [aCoder encodeObject:_contents forKey:kSeceretaryModelContents];
    [aCoder encodeDouble:_status forKey:kSeceretaryModelStatus];
    [aCoder encodeObject:_readDate forKey:kSeceretaryModelReadDate];
    [aCoder encodeDouble:_uIDS forKey:kSeceretaryModelUIDS];
    [aCoder encodeDouble:_type forKey:kSeceretaryModelType];
    [aCoder encodeObject:_replyDate forKey:kSeceretaryModelReplyDate];
    [aCoder encodeObject:_createDate forKey:kSeceretaryModelCreateDate];
    [aCoder encodeDouble:_sID forKey:kSeceretaryModelSID];
    [aCoder encodeDouble:_iDProperty forKey:kSeceretaryModelID];
}

- (id)copyWithZone:(NSZone *)zone {
    SeceretaryModel *copy = [[SeceretaryModel alloc] init];
    
    
    
    if (copy) {

        copy.uID = self.uID;
        copy.contents = [self.contents copyWithZone:zone];
        copy.status = self.status;
        copy.readDate = [self.readDate copyWithZone:zone];
        copy.uIDS = self.uIDS;
        copy.type = self.type;
        copy.replyDate = [self.replyDate copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.sID = self.sID;
        copy.iDProperty = self.iDProperty;
    }
    
    return copy;
}


@end
