//
//  PositionModel.h
//
//  Created by 晨光 宋 on 17/1/16
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PositionModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *pProvice;
@property (nonatomic, strong) NSString *pExp;
@property (nonatomic, strong) NSString *pEducation;
@property (nonatomic, strong) NSString *cScale;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double uID;
@property (nonatomic, assign) double cNature;
@property (nonatomic, strong) NSString *pArea;
@property (nonatomic, assign) double cIndustry;
@property (nonatomic, strong) NSString *cNickName;
@property (nonatomic, strong) NSString *iName;
@property (nonatomic, strong) NSString *cLogo;
@property (nonatomic, assign) double pPayE;
@property (nonatomic, strong) NSString *cIndustryse;
@property (nonatomic, assign) double types;
@property (nonatomic, strong) NSString *pNature;
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, strong) NSString *pTemptation;
@property (nonatomic, assign) double pType;
@property (nonatomic, assign) double pPayS;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, assign) double cAduitStatus;
@property (nonatomic, strong) NSString *iNameP;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *pCity;
@property (nonatomic, strong) NSString *pAddress;
@property (nonatomic, strong) NSString *cName;
@property (nonatomic, assign) double pID;
@property (nonatomic, assign) double cIndustrys;
@property (nonatomic, strong) NSString *pPay;
@property (nonatomic, strong) NSString *wName;
@property (nonatomic, assign) double status;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
