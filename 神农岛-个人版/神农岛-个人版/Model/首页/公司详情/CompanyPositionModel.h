//
//  CompanyPositionModel.h
//
//  Created by 晨光 宋 on 17/2/4
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CompanyPositionModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double pTypes;
@property (nonatomic, strong) NSString *pExp;
@property (nonatomic, strong) NSString *pEducation;
@property (nonatomic, strong) NSString *cScale;
@property (nonatomic, strong) NSString *ptName;
@property (nonatomic, strong) NSString *iName;
@property (nonatomic, strong) NSString *cLogo;
@property (nonatomic, assign) double pPayE;
@property (nonatomic, assign) double cID;
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, assign) double row;
@property (nonatomic, assign) double pType;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, assign) double pPayS;
@property (nonatomic, strong) NSString *iNameP;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *pCity;
@property (nonatomic, strong) NSString *pTypese;
@property (nonatomic, assign) double cAduitStatus;
@property (nonatomic, strong) NSString *cName;
@property (nonatomic, strong) NSString *ptNamep;
@property (nonatomic, strong) NSString *pPay;
@property (nonatomic, strong) NSString *wName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
