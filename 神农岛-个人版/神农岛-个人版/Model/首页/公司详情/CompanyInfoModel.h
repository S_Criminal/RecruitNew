//
//  CompanyInfoModel.h
//
//  Created by 晨光 宋 on 17/1/19
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CompanyInfoModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *iName;
@property (nonatomic, strong) NSString *cMails;
@property (nonatomic, strong) NSString *cTitle;
@property (nonatomic, strong) NSString *cArea;
@property (nonatomic, strong) NSString *cVcover;
@property (nonatomic, strong) NSString *cVideo;
@property (nonatomic, strong) NSString *cLogo;
@property (nonatomic, assign) double cIndustrys;
@property (nonatomic, strong) NSString *aduitDate;
@property (nonatomic, strong) NSString *cBusinessPath;
@property (nonatomic, strong) NSString *uName;
@property (nonatomic, assign) double cNature;
@property (nonatomic, assign) double cFund;
@property (nonatomic, strong) NSString *cPath;
@property (nonatomic, strong) NSString *cAddress;
@property (nonatomic, assign) double uID;
@property (nonatomic, assign) double cScaleID;
@property (nonatomic, strong) NSString *cCity;
@property (nonatomic, strong) NSString *uEmail;
@property (nonatomic, strong) NSString *iNameP;
@property (nonatomic, assign) double cLongitude;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *cMobile;
@property (nonatomic, strong) NSString *cClDate;
@property (nonatomic, strong) NSString *cScale;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *cNickName;
@property (nonatomic, strong) NSString *uLphone;
@property (nonatomic, strong) NSString *cPaths;
@property (nonatomic, assign) double cLatitude;
@property (nonatomic, strong) NSString *wName;
@property (nonatomic, strong) NSString *cProvince;
@property (nonatomic, strong) NSString *cPhone;
@property (nonatomic, assign) double cAduitStatus;
@property (nonatomic, strong) NSString *cIntrodutions;
@property (nonatomic, strong) NSString *cName;
@property (nonatomic, assign) double cIndustry;
@property (nonatomic, strong) NSString *cIntrodution;
@property (nonatomic, strong) NSString *cWelfare;
@property (nonatomic, strong) NSString *uPosition;
@property (nonatomic, strong) NSString *cWebSite;
@property (nonatomic, strong) NSString *cIndustryse;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
