//
//  ComWelfareModel.h
//
//  Created by 晨光 宋 on 17/2/7
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ComWelfareModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double cID;
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, assign) double cWDelete;
@property (nonatomic, strong) NSString *cWType;
@property (nonatomic, strong) NSString *cWPatha;
@property (nonatomic, strong) NSString *cWContents;
@property (nonatomic, strong) NSString *cWPath;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double sort;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
