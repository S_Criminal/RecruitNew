//
//  CompanyInfoModel.m
//
//  Created by 晨光 宋 on 17/1/19
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "CompanyInfoModel.h"


NSString *const kCompanyInfoModelIName = @"I_Name";
NSString *const kCompanyInfoModelCMails = @"C_Mails";
NSString *const kCompanyInfoModelCTitle = @"C_Title";
NSString *const kCompanyInfoModelCArea = @"C_Area";
NSString *const kCompanyInfoModelCVcover = @"C_Vcover";
NSString *const kCompanyInfoModelCVideo = @"C_Video";
NSString *const kCompanyInfoModelCLogo = @"C_Logo";
NSString *const kCompanyInfoModelCIndustrys = @"C_Industrys";
NSString *const kCompanyInfoModelAduitDate = @"AduitDate";
NSString *const kCompanyInfoModelCBusinessPath = @"C_BusinessPath";
NSString *const kCompanyInfoModelUName = @"U_Name";
NSString *const kCompanyInfoModelCNature = @"C_Nature";
NSString *const kCompanyInfoModelCFund = @"C_Fund";
NSString *const kCompanyInfoModelCPath = @"C_Path";
NSString *const kCompanyInfoModelCAddress = @"C_Address";
NSString *const kCompanyInfoModelUID = @"UID";
NSString *const kCompanyInfoModelCScaleID = @"C_ScaleID";
NSString *const kCompanyInfoModelCCity = @"C_City";
NSString *const kCompanyInfoModelUEmail = @"U_Email";
NSString *const kCompanyInfoModelINameP = @"I_NameP";
NSString *const kCompanyInfoModelCLongitude = @"C_Longitude";
NSString *const kCompanyInfoModelCreateDate = @"CreateDate";
NSString *const kCompanyInfoModelCMobile = @"C_Mobile";
NSString *const kCompanyInfoModelCClDate = @"C_ClDate";
NSString *const kCompanyInfoModelCScale = @"C_Scale";
NSString *const kCompanyInfoModelID = @"ID";
NSString *const kCompanyInfoModelCNickName = @"C_NickName";
NSString *const kCompanyInfoModelULphone = @"U_Lphone";
NSString *const kCompanyInfoModelCPaths = @"C_Paths";
NSString *const kCompanyInfoModelCLatitude = @"C_Latitude";
NSString *const kCompanyInfoModelWName = @"W_Name";
NSString *const kCompanyInfoModelCProvince = @"C_Province";
NSString *const kCompanyInfoModelCPhone = @"C_Phone";
NSString *const kCompanyInfoModelCAduitStatus = @"C_AduitStatus";
NSString *const kCompanyInfoModelCIntrodutions = @"C_Introdutions";
NSString *const kCompanyInfoModelCName = @"C_Name";
NSString *const kCompanyInfoModelCIndustry = @"C_Industry";
NSString *const kCompanyInfoModelCIntrodution = @"C_Introdution";
NSString *const kCompanyInfoModelCWelfare = @"C_Welfare";
NSString *const kCompanyInfoModelUPosition = @"U_Position";
NSString *const kCompanyInfoModelCWebSite = @"C_WebSite";
NSString *const kCompanyInfoModelCIndustryse = @"C_Industryse";


@interface CompanyInfoModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CompanyInfoModel

@synthesize iName = _iName;
@synthesize cMails = _cMails;
@synthesize cTitle = _cTitle;
@synthesize cArea = _cArea;
@synthesize cVcover = _cVcover;
@synthesize cVideo = _cVideo;
@synthesize cLogo = _cLogo;
@synthesize cIndustrys = _cIndustrys;
@synthesize aduitDate = _aduitDate;
@synthesize cBusinessPath = _cBusinessPath;
@synthesize uName = _uName;
@synthesize cNature = _cNature;
@synthesize cFund = _cFund;
@synthesize cPath = _cPath;
@synthesize cAddress = _cAddress;
@synthesize uID = _uID;
@synthesize cScaleID = _cScaleID;
@synthesize cCity = _cCity;
@synthesize uEmail = _uEmail;
@synthesize iNameP = _iNameP;
@synthesize cLongitude = _cLongitude;
@synthesize createDate = _createDate;
@synthesize cMobile = _cMobile;
@synthesize cClDate = _cClDate;
@synthesize cScale = _cScale;
@synthesize iDProperty = _iDProperty;
@synthesize cNickName = _cNickName;
@synthesize uLphone = _uLphone;
@synthesize cPaths = _cPaths;
@synthesize cLatitude = _cLatitude;
@synthesize wName = _wName;
@synthesize cProvince = _cProvince;
@synthesize cPhone = _cPhone;
@synthesize cAduitStatus = _cAduitStatus;
@synthesize cIntrodutions = _cIntrodutions;
@synthesize cName = _cName;
@synthesize cIndustry = _cIndustry;
@synthesize cIntrodution = _cIntrodution;
@synthesize cWelfare = _cWelfare;
@synthesize uPosition = _uPosition;
@synthesize cWebSite = _cWebSite;
@synthesize cIndustryse = _cIndustryse;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iName = [self objectOrNilForKey:kCompanyInfoModelIName fromDictionary:dict];
            self.cMails = [self objectOrNilForKey:kCompanyInfoModelCMails fromDictionary:dict];
            self.cTitle = [self objectOrNilForKey:kCompanyInfoModelCTitle fromDictionary:dict];
            self.cArea = [self objectOrNilForKey:kCompanyInfoModelCArea fromDictionary:dict];
            self.cVcover = [self objectOrNilForKey:kCompanyInfoModelCVcover fromDictionary:dict];
            self.cVideo = [self objectOrNilForKey:kCompanyInfoModelCVideo fromDictionary:dict];
            self.cLogo = [self objectOrNilForKey:kCompanyInfoModelCLogo fromDictionary:dict];
            self.cIndustrys = [[self objectOrNilForKey:kCompanyInfoModelCIndustrys fromDictionary:dict] doubleValue];
            self.aduitDate = [self objectOrNilForKey:kCompanyInfoModelAduitDate fromDictionary:dict];
            self.cBusinessPath = [self objectOrNilForKey:kCompanyInfoModelCBusinessPath fromDictionary:dict];
            self.uName = [self objectOrNilForKey:kCompanyInfoModelUName fromDictionary:dict];
            self.cNature = [[self objectOrNilForKey:kCompanyInfoModelCNature fromDictionary:dict] doubleValue];
            self.cFund = [[self objectOrNilForKey:kCompanyInfoModelCFund fromDictionary:dict] doubleValue];
            self.cPath = [self objectOrNilForKey:kCompanyInfoModelCPath fromDictionary:dict];
            self.cAddress = [self objectOrNilForKey:kCompanyInfoModelCAddress fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kCompanyInfoModelUID fromDictionary:dict] doubleValue];
            self.cScaleID = [[self objectOrNilForKey:kCompanyInfoModelCScaleID fromDictionary:dict] doubleValue];
            self.cCity = [self objectOrNilForKey:kCompanyInfoModelCCity fromDictionary:dict];
            self.uEmail = [self objectOrNilForKey:kCompanyInfoModelUEmail fromDictionary:dict];
            self.iNameP = [self objectOrNilForKey:kCompanyInfoModelINameP fromDictionary:dict];
            self.cLongitude = [[self objectOrNilForKey:kCompanyInfoModelCLongitude fromDictionary:dict] doubleValue];
            self.createDate = [self objectOrNilForKey:kCompanyInfoModelCreateDate fromDictionary:dict];
            self.cMobile = [self objectOrNilForKey:kCompanyInfoModelCMobile fromDictionary:dict];
            self.cClDate = [self objectOrNilForKey:kCompanyInfoModelCClDate fromDictionary:dict];
            self.cScale = [self objectOrNilForKey:kCompanyInfoModelCScale fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kCompanyInfoModelID fromDictionary:dict] doubleValue];
            self.cNickName = [self objectOrNilForKey:kCompanyInfoModelCNickName fromDictionary:dict];
            self.uLphone = [self objectOrNilForKey:kCompanyInfoModelULphone fromDictionary:dict];
            self.cPaths = [self objectOrNilForKey:kCompanyInfoModelCPaths fromDictionary:dict];
            self.cLatitude = [[self objectOrNilForKey:kCompanyInfoModelCLatitude fromDictionary:dict] doubleValue];
            self.wName = [self objectOrNilForKey:kCompanyInfoModelWName fromDictionary:dict];
            self.cProvince = [self objectOrNilForKey:kCompanyInfoModelCProvince fromDictionary:dict];
            self.cPhone = [self objectOrNilForKey:kCompanyInfoModelCPhone fromDictionary:dict];
            self.cAduitStatus = [[self objectOrNilForKey:kCompanyInfoModelCAduitStatus fromDictionary:dict] doubleValue];
            self.cIntrodutions = [self objectOrNilForKey:kCompanyInfoModelCIntrodutions fromDictionary:dict];
            self.cName = [self objectOrNilForKey:kCompanyInfoModelCName fromDictionary:dict];
            self.cIndustry = [[self objectOrNilForKey:kCompanyInfoModelCIndustry fromDictionary:dict] doubleValue];
            self.cIntrodution = [self objectOrNilForKey:kCompanyInfoModelCIntrodution fromDictionary:dict];
            self.cWelfare = [self objectOrNilForKey:kCompanyInfoModelCWelfare fromDictionary:dict];
            self.uPosition = [self objectOrNilForKey:kCompanyInfoModelUPosition fromDictionary:dict];
            self.cWebSite = [self objectOrNilForKey:kCompanyInfoModelCWebSite fromDictionary:dict];
            self.cIndustryse = [self objectOrNilForKey:kCompanyInfoModelCIndustryse fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iName forKey:kCompanyInfoModelIName];
    [mutableDict setValue:self.cMails forKey:kCompanyInfoModelCMails];
    [mutableDict setValue:self.cTitle forKey:kCompanyInfoModelCTitle];
    [mutableDict setValue:self.cArea forKey:kCompanyInfoModelCArea];
    [mutableDict setValue:self.cVcover forKey:kCompanyInfoModelCVcover];
    [mutableDict setValue:self.cVideo forKey:kCompanyInfoModelCVideo];
    [mutableDict setValue:self.cLogo forKey:kCompanyInfoModelCLogo];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustrys] forKey:kCompanyInfoModelCIndustrys];
    [mutableDict setValue:self.aduitDate forKey:kCompanyInfoModelAduitDate];
    [mutableDict setValue:self.cBusinessPath forKey:kCompanyInfoModelCBusinessPath];
    [mutableDict setValue:self.uName forKey:kCompanyInfoModelUName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cNature] forKey:kCompanyInfoModelCNature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cFund] forKey:kCompanyInfoModelCFund];
    [mutableDict setValue:self.cPath forKey:kCompanyInfoModelCPath];
    [mutableDict setValue:self.cAddress forKey:kCompanyInfoModelCAddress];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kCompanyInfoModelUID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cScaleID] forKey:kCompanyInfoModelCScaleID];
    [mutableDict setValue:self.cCity forKey:kCompanyInfoModelCCity];
    [mutableDict setValue:self.uEmail forKey:kCompanyInfoModelUEmail];
    [mutableDict setValue:self.iNameP forKey:kCompanyInfoModelINameP];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cLongitude] forKey:kCompanyInfoModelCLongitude];
    [mutableDict setValue:self.createDate forKey:kCompanyInfoModelCreateDate];
    [mutableDict setValue:self.cMobile forKey:kCompanyInfoModelCMobile];
    [mutableDict setValue:self.cClDate forKey:kCompanyInfoModelCClDate];
    [mutableDict setValue:self.cScale forKey:kCompanyInfoModelCScale];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kCompanyInfoModelID];
    [mutableDict setValue:self.cNickName forKey:kCompanyInfoModelCNickName];
    [mutableDict setValue:self.uLphone forKey:kCompanyInfoModelULphone];
    [mutableDict setValue:self.cPaths forKey:kCompanyInfoModelCPaths];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cLatitude] forKey:kCompanyInfoModelCLatitude];
    [mutableDict setValue:self.wName forKey:kCompanyInfoModelWName];
    [mutableDict setValue:self.cProvince forKey:kCompanyInfoModelCProvince];
    [mutableDict setValue:self.cPhone forKey:kCompanyInfoModelCPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cAduitStatus] forKey:kCompanyInfoModelCAduitStatus];
    [mutableDict setValue:self.cIntrodutions forKey:kCompanyInfoModelCIntrodutions];
    [mutableDict setValue:self.cName forKey:kCompanyInfoModelCName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustry] forKey:kCompanyInfoModelCIndustry];
    [mutableDict setValue:self.cIntrodution forKey:kCompanyInfoModelCIntrodution];
    [mutableDict setValue:self.cWelfare forKey:kCompanyInfoModelCWelfare];
    [mutableDict setValue:self.uPosition forKey:kCompanyInfoModelUPosition];
    [mutableDict setValue:self.cWebSite forKey:kCompanyInfoModelCWebSite];
    [mutableDict setValue:self.cIndustryse forKey:kCompanyInfoModelCIndustryse];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iName = [aDecoder decodeObjectForKey:kCompanyInfoModelIName];
    self.cMails = [aDecoder decodeObjectForKey:kCompanyInfoModelCMails];
    self.cTitle = [aDecoder decodeObjectForKey:kCompanyInfoModelCTitle];
    self.cArea = [aDecoder decodeObjectForKey:kCompanyInfoModelCArea];
    self.cVcover = [aDecoder decodeObjectForKey:kCompanyInfoModelCVcover];
    self.cVideo = [aDecoder decodeObjectForKey:kCompanyInfoModelCVideo];
    self.cLogo = [aDecoder decodeObjectForKey:kCompanyInfoModelCLogo];
    self.cIndustrys = [aDecoder decodeDoubleForKey:kCompanyInfoModelCIndustrys];
    self.aduitDate = [aDecoder decodeObjectForKey:kCompanyInfoModelAduitDate];
    self.cBusinessPath = [aDecoder decodeObjectForKey:kCompanyInfoModelCBusinessPath];
    self.uName = [aDecoder decodeObjectForKey:kCompanyInfoModelUName];
    self.cNature = [aDecoder decodeDoubleForKey:kCompanyInfoModelCNature];
    self.cFund = [aDecoder decodeDoubleForKey:kCompanyInfoModelCFund];
    self.cPath = [aDecoder decodeObjectForKey:kCompanyInfoModelCPath];
    self.cAddress = [aDecoder decodeObjectForKey:kCompanyInfoModelCAddress];
    self.uID = [aDecoder decodeDoubleForKey:kCompanyInfoModelUID];
    self.cScaleID = [aDecoder decodeDoubleForKey:kCompanyInfoModelCScaleID];
    self.cCity = [aDecoder decodeObjectForKey:kCompanyInfoModelCCity];
    self.uEmail = [aDecoder decodeObjectForKey:kCompanyInfoModelUEmail];
    self.iNameP = [aDecoder decodeObjectForKey:kCompanyInfoModelINameP];
    self.cLongitude = [aDecoder decodeDoubleForKey:kCompanyInfoModelCLongitude];
    self.createDate = [aDecoder decodeObjectForKey:kCompanyInfoModelCreateDate];
    self.cMobile = [aDecoder decodeObjectForKey:kCompanyInfoModelCMobile];
    self.cClDate = [aDecoder decodeObjectForKey:kCompanyInfoModelCClDate];
    self.cScale = [aDecoder decodeObjectForKey:kCompanyInfoModelCScale];
    self.iDProperty = [aDecoder decodeDoubleForKey:kCompanyInfoModelID];
    self.cNickName = [aDecoder decodeObjectForKey:kCompanyInfoModelCNickName];
    self.uLphone = [aDecoder decodeObjectForKey:kCompanyInfoModelULphone];
    self.cPaths = [aDecoder decodeObjectForKey:kCompanyInfoModelCPaths];
    self.cLatitude = [aDecoder decodeDoubleForKey:kCompanyInfoModelCLatitude];
    self.wName = [aDecoder decodeObjectForKey:kCompanyInfoModelWName];
    self.cProvince = [aDecoder decodeObjectForKey:kCompanyInfoModelCProvince];
    self.cPhone = [aDecoder decodeObjectForKey:kCompanyInfoModelCPhone];
    self.cAduitStatus = [aDecoder decodeDoubleForKey:kCompanyInfoModelCAduitStatus];
    self.cIntrodutions = [aDecoder decodeObjectForKey:kCompanyInfoModelCIntrodutions];
    self.cName = [aDecoder decodeObjectForKey:kCompanyInfoModelCName];
    self.cIndustry = [aDecoder decodeDoubleForKey:kCompanyInfoModelCIndustry];
    self.cIntrodution = [aDecoder decodeObjectForKey:kCompanyInfoModelCIntrodution];
    self.cWelfare = [aDecoder decodeObjectForKey:kCompanyInfoModelCWelfare];
    self.uPosition = [aDecoder decodeObjectForKey:kCompanyInfoModelUPosition];
    self.cWebSite = [aDecoder decodeObjectForKey:kCompanyInfoModelCWebSite];
    self.cIndustryse = [aDecoder decodeObjectForKey:kCompanyInfoModelCIndustryse];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iName forKey:kCompanyInfoModelIName];
    [aCoder encodeObject:_cMails forKey:kCompanyInfoModelCMails];
    [aCoder encodeObject:_cTitle forKey:kCompanyInfoModelCTitle];
    [aCoder encodeObject:_cArea forKey:kCompanyInfoModelCArea];
    [aCoder encodeObject:_cVcover forKey:kCompanyInfoModelCVcover];
    [aCoder encodeObject:_cVideo forKey:kCompanyInfoModelCVideo];
    [aCoder encodeObject:_cLogo forKey:kCompanyInfoModelCLogo];
    [aCoder encodeDouble:_cIndustrys forKey:kCompanyInfoModelCIndustrys];
    [aCoder encodeObject:_aduitDate forKey:kCompanyInfoModelAduitDate];
    [aCoder encodeObject:_cBusinessPath forKey:kCompanyInfoModelCBusinessPath];
    [aCoder encodeObject:_uName forKey:kCompanyInfoModelUName];
    [aCoder encodeDouble:_cNature forKey:kCompanyInfoModelCNature];
    [aCoder encodeDouble:_cFund forKey:kCompanyInfoModelCFund];
    [aCoder encodeObject:_cPath forKey:kCompanyInfoModelCPath];
    [aCoder encodeObject:_cAddress forKey:kCompanyInfoModelCAddress];
    [aCoder encodeDouble:_uID forKey:kCompanyInfoModelUID];
    [aCoder encodeDouble:_cScaleID forKey:kCompanyInfoModelCScaleID];
    [aCoder encodeObject:_cCity forKey:kCompanyInfoModelCCity];
    [aCoder encodeObject:_uEmail forKey:kCompanyInfoModelUEmail];
    [aCoder encodeObject:_iNameP forKey:kCompanyInfoModelINameP];
    [aCoder encodeDouble:_cLongitude forKey:kCompanyInfoModelCLongitude];
    [aCoder encodeObject:_createDate forKey:kCompanyInfoModelCreateDate];
    [aCoder encodeObject:_cMobile forKey:kCompanyInfoModelCMobile];
    [aCoder encodeObject:_cClDate forKey:kCompanyInfoModelCClDate];
    [aCoder encodeObject:_cScale forKey:kCompanyInfoModelCScale];
    [aCoder encodeDouble:_iDProperty forKey:kCompanyInfoModelID];
    [aCoder encodeObject:_cNickName forKey:kCompanyInfoModelCNickName];
    [aCoder encodeObject:_uLphone forKey:kCompanyInfoModelULphone];
    [aCoder encodeObject:_cPaths forKey:kCompanyInfoModelCPaths];
    [aCoder encodeDouble:_cLatitude forKey:kCompanyInfoModelCLatitude];
    [aCoder encodeObject:_wName forKey:kCompanyInfoModelWName];
    [aCoder encodeObject:_cProvince forKey:kCompanyInfoModelCProvince];
    [aCoder encodeObject:_cPhone forKey:kCompanyInfoModelCPhone];
    [aCoder encodeDouble:_cAduitStatus forKey:kCompanyInfoModelCAduitStatus];
    [aCoder encodeObject:_cIntrodutions forKey:kCompanyInfoModelCIntrodutions];
    [aCoder encodeObject:_cName forKey:kCompanyInfoModelCName];
    [aCoder encodeDouble:_cIndustry forKey:kCompanyInfoModelCIndustry];
    [aCoder encodeObject:_cIntrodution forKey:kCompanyInfoModelCIntrodution];
    [aCoder encodeObject:_cWelfare forKey:kCompanyInfoModelCWelfare];
    [aCoder encodeObject:_uPosition forKey:kCompanyInfoModelUPosition];
    [aCoder encodeObject:_cWebSite forKey:kCompanyInfoModelCWebSite];
    [aCoder encodeObject:_cIndustryse forKey:kCompanyInfoModelCIndustryse];
}

- (id)copyWithZone:(NSZone *)zone {
    CompanyInfoModel *copy = [[CompanyInfoModel alloc] init];
    
    
    
    if (copy) {

        copy.iName = [self.iName copyWithZone:zone];
        copy.cMails = [self.cMails copyWithZone:zone];
        copy.cTitle = [self.cTitle copyWithZone:zone];
        copy.cArea = [self.cArea copyWithZone:zone];
        copy.cVcover = [self.cVcover copyWithZone:zone];
        copy.cVideo = [self.cVideo copyWithZone:zone];
        copy.cLogo = [self.cLogo copyWithZone:zone];
        copy.cIndustrys = self.cIndustrys;
        copy.aduitDate = [self.aduitDate copyWithZone:zone];
        copy.cBusinessPath = [self.cBusinessPath copyWithZone:zone];
        copy.uName = [self.uName copyWithZone:zone];
        copy.cNature = self.cNature;
        copy.cFund = self.cFund;
        copy.cPath = [self.cPath copyWithZone:zone];
        copy.cAddress = [self.cAddress copyWithZone:zone];
        copy.uID = self.uID;
        copy.cScaleID = self.cScaleID;
        copy.cCity = [self.cCity copyWithZone:zone];
        copy.uEmail = [self.uEmail copyWithZone:zone];
        copy.iNameP = [self.iNameP copyWithZone:zone];
        copy.cLongitude = self.cLongitude;
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.cMobile = [self.cMobile copyWithZone:zone];
        copy.cClDate = [self.cClDate copyWithZone:zone];
        copy.cScale = [self.cScale copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.cNickName = [self.cNickName copyWithZone:zone];
        copy.uLphone = [self.uLphone copyWithZone:zone];
        copy.cPaths = [self.cPaths copyWithZone:zone];
        copy.cLatitude = self.cLatitude;
        copy.wName = [self.wName copyWithZone:zone];
        copy.cProvince = [self.cProvince copyWithZone:zone];
        copy.cPhone = [self.cPhone copyWithZone:zone];
        copy.cAduitStatus = self.cAduitStatus;
        copy.cIntrodutions = [self.cIntrodutions copyWithZone:zone];
        copy.cName = [self.cName copyWithZone:zone];
        copy.cIndustry = self.cIndustry;
        copy.cIntrodution = [self.cIntrodution copyWithZone:zone];
        copy.cWelfare = [self.cWelfare copyWithZone:zone];
        copy.uPosition = [self.uPosition copyWithZone:zone];
        copy.cWebSite = [self.cWebSite copyWithZone:zone];
        copy.cIndustryse = [self.cIndustryse copyWithZone:zone];
    }
    
    return copy;
}


@end
