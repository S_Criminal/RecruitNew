//
//  ComWelfareModel.m
//
//  Created by 晨光 宋 on 17/2/7
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ComWelfareModel.h"


NSString *const kComWelfareModelCID = @"CID";
NSString *const kComWelfareModelUpdateDate = @"UpdateDate";
NSString *const kComWelfareModelCWDelete = @"CW_Delete";
NSString *const kComWelfareModelCWType = @"CW_Type";
NSString *const kComWelfareModelCWPatha = @"CW_Patha";
NSString *const kComWelfareModelCWContents = @"CW_Contents";
NSString *const kComWelfareModelCWPath = @"CW_Path";
NSString *const kComWelfareModelCreateDate = @"CreateDate";
NSString *const kComWelfareModelID = @"ID";
NSString *const kComWelfareModelSort = @"Sort";


@interface ComWelfareModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ComWelfareModel

@synthesize cID = _cID;
@synthesize updateDate = _updateDate;
@synthesize cWDelete = _cWDelete;
@synthesize cWType = _cWType;
@synthesize cWPatha = _cWPatha;
@synthesize cWContents = _cWContents;
@synthesize cWPath = _cWPath;
@synthesize createDate = _createDate;
@synthesize iDProperty = _iDProperty;
@synthesize sort = _sort;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.cID = [[self objectOrNilForKey:kComWelfareModelCID fromDictionary:dict] doubleValue];
            self.updateDate = [self objectOrNilForKey:kComWelfareModelUpdateDate fromDictionary:dict];
            self.cWDelete = [[self objectOrNilForKey:kComWelfareModelCWDelete fromDictionary:dict] doubleValue];
            self.cWType = [self objectOrNilForKey:kComWelfareModelCWType fromDictionary:dict];
            self.cWPatha = [self objectOrNilForKey:kComWelfareModelCWPatha fromDictionary:dict];
            self.cWContents = [self objectOrNilForKey:kComWelfareModelCWContents fromDictionary:dict];
            self.cWPath = [self objectOrNilForKey:kComWelfareModelCWPath fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kComWelfareModelCreateDate fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kComWelfareModelID fromDictionary:dict] doubleValue];
            self.sort = [[self objectOrNilForKey:kComWelfareModelSort fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cID] forKey:kComWelfareModelCID];
    [mutableDict setValue:self.updateDate forKey:kComWelfareModelUpdateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cWDelete] forKey:kComWelfareModelCWDelete];
    [mutableDict setValue:self.cWType forKey:kComWelfareModelCWType];
    [mutableDict setValue:self.cWPatha forKey:kComWelfareModelCWPatha];
    [mutableDict setValue:self.cWContents forKey:kComWelfareModelCWContents];
    [mutableDict setValue:self.cWPath forKey:kComWelfareModelCWPath];
    [mutableDict setValue:self.createDate forKey:kComWelfareModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kComWelfareModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sort] forKey:kComWelfareModelSort];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.cID = [aDecoder decodeDoubleForKey:kComWelfareModelCID];
    self.updateDate = [aDecoder decodeObjectForKey:kComWelfareModelUpdateDate];
    self.cWDelete = [aDecoder decodeDoubleForKey:kComWelfareModelCWDelete];
    self.cWType = [aDecoder decodeObjectForKey:kComWelfareModelCWType];
    self.cWPatha = [aDecoder decodeObjectForKey:kComWelfareModelCWPatha];
    self.cWContents = [aDecoder decodeObjectForKey:kComWelfareModelCWContents];
    self.cWPath = [aDecoder decodeObjectForKey:kComWelfareModelCWPath];
    self.createDate = [aDecoder decodeObjectForKey:kComWelfareModelCreateDate];
    self.iDProperty = [aDecoder decodeDoubleForKey:kComWelfareModelID];
    self.sort = [aDecoder decodeDoubleForKey:kComWelfareModelSort];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_cID forKey:kComWelfareModelCID];
    [aCoder encodeObject:_updateDate forKey:kComWelfareModelUpdateDate];
    [aCoder encodeDouble:_cWDelete forKey:kComWelfareModelCWDelete];
    [aCoder encodeObject:_cWType forKey:kComWelfareModelCWType];
    [aCoder encodeObject:_cWPatha forKey:kComWelfareModelCWPatha];
    [aCoder encodeObject:_cWContents forKey:kComWelfareModelCWContents];
    [aCoder encodeObject:_cWPath forKey:kComWelfareModelCWPath];
    [aCoder encodeObject:_createDate forKey:kComWelfareModelCreateDate];
    [aCoder encodeDouble:_iDProperty forKey:kComWelfareModelID];
    [aCoder encodeDouble:_sort forKey:kComWelfareModelSort];
}

- (id)copyWithZone:(NSZone *)zone {
    ComWelfareModel *copy = [[ComWelfareModel alloc] init];
    
    
    
    if (copy) {

        copy.cID = self.cID;
        copy.updateDate = [self.updateDate copyWithZone:zone];
        copy.cWDelete = self.cWDelete;
        copy.cWType = [self.cWType copyWithZone:zone];
        copy.cWPatha = [self.cWPatha copyWithZone:zone];
        copy.cWContents = [self.cWContents copyWithZone:zone];
        copy.cWPath = [self.cWPath copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.sort = self.sort;
    }
    
    return copy;
}


@end
