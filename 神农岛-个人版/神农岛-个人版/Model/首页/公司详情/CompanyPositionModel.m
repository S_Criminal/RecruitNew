//
//  CompanyPositionModel.m
//
//  Created by 晨光 宋 on 17/2/4
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "CompanyPositionModel.h"


NSString *const kCompanyPositionModelPTypes = @"P_Types";
NSString *const kCompanyPositionModelPExp = @"P_Exp";
NSString *const kCompanyPositionModelPEducation = @"P_Education";
NSString *const kCompanyPositionModelCScale = @"C_Scale";
NSString *const kCompanyPositionModelPtName = @"Pt_Name";
NSString *const kCompanyPositionModelIName = @"I_Name";
NSString *const kCompanyPositionModelCLogo = @"C_Logo";
NSString *const kCompanyPositionModelPPayE = @"P_PayE";
NSString *const kCompanyPositionModelCID = @"C_ID";
NSString *const kCompanyPositionModelUpdateDate = @"UpdateDate";
NSString *const kCompanyPositionModelRow = @"row";
NSString *const kCompanyPositionModelPType = @"P_Type";
NSString *const kCompanyPositionModelPName = @"P_Name";
NSString *const kCompanyPositionModelPPayS = @"P_PayS";
NSString *const kCompanyPositionModelINameP = @"I_NameP";
NSString *const kCompanyPositionModelID = @"ID";
NSString *const kCompanyPositionModelPCity = @"P_City";
NSString *const kCompanyPositionModelPTypese = @"P_Typese";
NSString *const kCompanyPositionModelCAduitStatus = @"C_AduitStatus";
NSString *const kCompanyPositionModelCName = @"C_Name";
NSString *const kCompanyPositionModelPtNamep = @"Pt_Namep";
NSString *const kCompanyPositionModelPPay = @"P_Pay";
NSString *const kCompanyPositionModelWName = @"W_Name";


@interface CompanyPositionModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CompanyPositionModel

@synthesize pTypes = _pTypes;
@synthesize pExp = _pExp;
@synthesize pEducation = _pEducation;
@synthesize cScale = _cScale;
@synthesize ptName = _ptName;
@synthesize iName = _iName;
@synthesize cLogo = _cLogo;
@synthesize pPayE = _pPayE;
@synthesize cID = _cID;
@synthesize updateDate = _updateDate;
@synthesize row = _row;
@synthesize pType = _pType;
@synthesize pName = _pName;
@synthesize pPayS = _pPayS;
@synthesize iNameP = _iNameP;
@synthesize iDProperty = _iDProperty;
@synthesize pCity = _pCity;
@synthesize pTypese = _pTypese;
@synthesize cAduitStatus = _cAduitStatus;
@synthesize cName = _cName;
@synthesize ptNamep = _ptNamep;
@synthesize pPay = _pPay;
@synthesize wName = _wName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pTypes = [[self objectOrNilForKey:kCompanyPositionModelPTypes fromDictionary:dict] doubleValue];
            self.pExp = [self objectOrNilForKey:kCompanyPositionModelPExp fromDictionary:dict];
            self.pEducation = [self objectOrNilForKey:kCompanyPositionModelPEducation fromDictionary:dict];
            self.cScale = [self objectOrNilForKey:kCompanyPositionModelCScale fromDictionary:dict];
            self.ptName = [self objectOrNilForKey:kCompanyPositionModelPtName fromDictionary:dict];
            self.iName = [self objectOrNilForKey:kCompanyPositionModelIName fromDictionary:dict];
            self.cLogo = [self objectOrNilForKey:kCompanyPositionModelCLogo fromDictionary:dict];
            self.pPayE = [[self objectOrNilForKey:kCompanyPositionModelPPayE fromDictionary:dict] doubleValue];
            self.cID = [[self objectOrNilForKey:kCompanyPositionModelCID fromDictionary:dict] doubleValue];
            self.updateDate = [self objectOrNilForKey:kCompanyPositionModelUpdateDate fromDictionary:dict];
            self.row = [[self objectOrNilForKey:kCompanyPositionModelRow fromDictionary:dict] doubleValue];
            self.pType = [[self objectOrNilForKey:kCompanyPositionModelPType fromDictionary:dict] doubleValue];
            self.pName = [self objectOrNilForKey:kCompanyPositionModelPName fromDictionary:dict];
            self.pPayS = [[self objectOrNilForKey:kCompanyPositionModelPPayS fromDictionary:dict] doubleValue];
            self.iNameP = [self objectOrNilForKey:kCompanyPositionModelINameP fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kCompanyPositionModelID fromDictionary:dict] doubleValue];
            self.pCity = [self objectOrNilForKey:kCompanyPositionModelPCity fromDictionary:dict];
            self.pTypese = [self objectOrNilForKey:kCompanyPositionModelPTypese fromDictionary:dict];
            self.cAduitStatus = [[self objectOrNilForKey:kCompanyPositionModelCAduitStatus fromDictionary:dict] doubleValue];
            self.cName = [self objectOrNilForKey:kCompanyPositionModelCName fromDictionary:dict];
            self.ptNamep = [self objectOrNilForKey:kCompanyPositionModelPtNamep fromDictionary:dict];
            self.pPay = [self objectOrNilForKey:kCompanyPositionModelPPay fromDictionary:dict];
            self.wName = [self objectOrNilForKey:kCompanyPositionModelWName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pTypes] forKey:kCompanyPositionModelPTypes];
    [mutableDict setValue:self.pExp forKey:kCompanyPositionModelPExp];
    [mutableDict setValue:self.pEducation forKey:kCompanyPositionModelPEducation];
    [mutableDict setValue:self.cScale forKey:kCompanyPositionModelCScale];
    [mutableDict setValue:self.ptName forKey:kCompanyPositionModelPtName];
    [mutableDict setValue:self.iName forKey:kCompanyPositionModelIName];
    [mutableDict setValue:self.cLogo forKey:kCompanyPositionModelCLogo];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayE] forKey:kCompanyPositionModelPPayE];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cID] forKey:kCompanyPositionModelCID];
    [mutableDict setValue:self.updateDate forKey:kCompanyPositionModelUpdateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.row] forKey:kCompanyPositionModelRow];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pType] forKey:kCompanyPositionModelPType];
    [mutableDict setValue:self.pName forKey:kCompanyPositionModelPName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayS] forKey:kCompanyPositionModelPPayS];
    [mutableDict setValue:self.iNameP forKey:kCompanyPositionModelINameP];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kCompanyPositionModelID];
    [mutableDict setValue:self.pCity forKey:kCompanyPositionModelPCity];
    [mutableDict setValue:self.pTypese forKey:kCompanyPositionModelPTypese];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cAduitStatus] forKey:kCompanyPositionModelCAduitStatus];
    [mutableDict setValue:self.cName forKey:kCompanyPositionModelCName];
    [mutableDict setValue:self.ptNamep forKey:kCompanyPositionModelPtNamep];
    [mutableDict setValue:self.pPay forKey:kCompanyPositionModelPPay];
    [mutableDict setValue:self.wName forKey:kCompanyPositionModelWName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.pTypes = [aDecoder decodeDoubleForKey:kCompanyPositionModelPTypes];
    self.pExp = [aDecoder decodeObjectForKey:kCompanyPositionModelPExp];
    self.pEducation = [aDecoder decodeObjectForKey:kCompanyPositionModelPEducation];
    self.cScale = [aDecoder decodeObjectForKey:kCompanyPositionModelCScale];
    self.ptName = [aDecoder decodeObjectForKey:kCompanyPositionModelPtName];
    self.iName = [aDecoder decodeObjectForKey:kCompanyPositionModelIName];
    self.cLogo = [aDecoder decodeObjectForKey:kCompanyPositionModelCLogo];
    self.pPayE = [aDecoder decodeDoubleForKey:kCompanyPositionModelPPayE];
    self.cID = [aDecoder decodeDoubleForKey:kCompanyPositionModelCID];
    self.updateDate = [aDecoder decodeObjectForKey:kCompanyPositionModelUpdateDate];
    self.row = [aDecoder decodeDoubleForKey:kCompanyPositionModelRow];
    self.pType = [aDecoder decodeDoubleForKey:kCompanyPositionModelPType];
    self.pName = [aDecoder decodeObjectForKey:kCompanyPositionModelPName];
    self.pPayS = [aDecoder decodeDoubleForKey:kCompanyPositionModelPPayS];
    self.iNameP = [aDecoder decodeObjectForKey:kCompanyPositionModelINameP];
    self.iDProperty = [aDecoder decodeDoubleForKey:kCompanyPositionModelID];
    self.pCity = [aDecoder decodeObjectForKey:kCompanyPositionModelPCity];
    self.pTypese = [aDecoder decodeObjectForKey:kCompanyPositionModelPTypese];
    self.cAduitStatus = [aDecoder decodeDoubleForKey:kCompanyPositionModelCAduitStatus];
    self.cName = [aDecoder decodeObjectForKey:kCompanyPositionModelCName];
    self.ptNamep = [aDecoder decodeObjectForKey:kCompanyPositionModelPtNamep];
    self.pPay = [aDecoder decodeObjectForKey:kCompanyPositionModelPPay];
    self.wName = [aDecoder decodeObjectForKey:kCompanyPositionModelWName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_pTypes forKey:kCompanyPositionModelPTypes];
    [aCoder encodeObject:_pExp forKey:kCompanyPositionModelPExp];
    [aCoder encodeObject:_pEducation forKey:kCompanyPositionModelPEducation];
    [aCoder encodeObject:_cScale forKey:kCompanyPositionModelCScale];
    [aCoder encodeObject:_ptName forKey:kCompanyPositionModelPtName];
    [aCoder encodeObject:_iName forKey:kCompanyPositionModelIName];
    [aCoder encodeObject:_cLogo forKey:kCompanyPositionModelCLogo];
    [aCoder encodeDouble:_pPayE forKey:kCompanyPositionModelPPayE];
    [aCoder encodeDouble:_cID forKey:kCompanyPositionModelCID];
    [aCoder encodeObject:_updateDate forKey:kCompanyPositionModelUpdateDate];
    [aCoder encodeDouble:_row forKey:kCompanyPositionModelRow];
    [aCoder encodeDouble:_pType forKey:kCompanyPositionModelPType];
    [aCoder encodeObject:_pName forKey:kCompanyPositionModelPName];
    [aCoder encodeDouble:_pPayS forKey:kCompanyPositionModelPPayS];
    [aCoder encodeObject:_iNameP forKey:kCompanyPositionModelINameP];
    [aCoder encodeDouble:_iDProperty forKey:kCompanyPositionModelID];
    [aCoder encodeObject:_pCity forKey:kCompanyPositionModelPCity];
    [aCoder encodeObject:_pTypese forKey:kCompanyPositionModelPTypese];
    [aCoder encodeDouble:_cAduitStatus forKey:kCompanyPositionModelCAduitStatus];
    [aCoder encodeObject:_cName forKey:kCompanyPositionModelCName];
    [aCoder encodeObject:_ptNamep forKey:kCompanyPositionModelPtNamep];
    [aCoder encodeObject:_pPay forKey:kCompanyPositionModelPPay];
    [aCoder encodeObject:_wName forKey:kCompanyPositionModelWName];
}

- (id)copyWithZone:(NSZone *)zone {
    CompanyPositionModel *copy = [[CompanyPositionModel alloc] init];
    
    
    
    if (copy) {

        copy.pTypes = self.pTypes;
        copy.pExp = [self.pExp copyWithZone:zone];
        copy.pEducation = [self.pEducation copyWithZone:zone];
        copy.cScale = [self.cScale copyWithZone:zone];
        copy.ptName = [self.ptName copyWithZone:zone];
        copy.iName = [self.iName copyWithZone:zone];
        copy.cLogo = [self.cLogo copyWithZone:zone];
        copy.pPayE = self.pPayE;
        copy.cID = self.cID;
        copy.updateDate = [self.updateDate copyWithZone:zone];
        copy.row = self.row;
        copy.pType = self.pType;
        copy.pName = [self.pName copyWithZone:zone];
        copy.pPayS = self.pPayS;
        copy.iNameP = [self.iNameP copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.pCity = [self.pCity copyWithZone:zone];
        copy.pTypese = [self.pTypese copyWithZone:zone];
        copy.cAduitStatus = self.cAduitStatus;
        copy.cName = [self.cName copyWithZone:zone];
        copy.ptNamep = [self.ptNamep copyWithZone:zone];
        copy.pPay = [self.pPay copyWithZone:zone];
        copy.wName = [self.wName copyWithZone:zone];
    }
    
    return copy;
}


@end
