//
//  ResumeDeliveryModel.m
//
//  Created by 晨光 宋 on 17/2/4
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ResumeDeliveryModel.h"


NSString *const kResumeDeliveryModelRType = @"R_Type";
NSString *const kResumeDeliveryModelRTypes = @"R_Types";
NSString *const kResumeDeliveryModelRCity = @"R_City";
NSString *const kResumeDeliveryModelPNamep = @"P_Namep";
NSString *const kResumeDeliveryModelPName = @"P_Name";
NSString *const kResumeDeliveryModelRProvince = @"R_Province";
NSString *const kResumeDeliveryModelID = @"ID";
NSString *const kResumeDeliveryModelUResID = @"U_ResID";


@interface ResumeDeliveryModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ResumeDeliveryModel

@synthesize rType = _rType;
@synthesize rTypes = _rTypes;
@synthesize rCity = _rCity;
@synthesize pNamep = _pNamep;
@synthesize pName = _pName;
@synthesize rProvince = _rProvince;
@synthesize iDProperty = _iDProperty;
@synthesize uResID = _uResID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rType = [[self objectOrNilForKey:kResumeDeliveryModelRType fromDictionary:dict] doubleValue];
            self.rTypes = [[self objectOrNilForKey:kResumeDeliveryModelRTypes fromDictionary:dict] doubleValue];
            self.rCity = [self objectOrNilForKey:kResumeDeliveryModelRCity fromDictionary:dict];
            self.pNamep = [self objectOrNilForKey:kResumeDeliveryModelPNamep fromDictionary:dict];
            self.pName = [self objectOrNilForKey:kResumeDeliveryModelPName fromDictionary:dict];
            self.rProvince = [self objectOrNilForKey:kResumeDeliveryModelRProvince fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kResumeDeliveryModelID fromDictionary:dict] doubleValue];
            self.uResID = [[self objectOrNilForKey:kResumeDeliveryModelUResID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rType] forKey:kResumeDeliveryModelRType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rTypes] forKey:kResumeDeliveryModelRTypes];
    [mutableDict setValue:self.rCity forKey:kResumeDeliveryModelRCity];
    [mutableDict setValue:self.pNamep forKey:kResumeDeliveryModelPNamep];
    [mutableDict setValue:self.pName forKey:kResumeDeliveryModelPName];
    [mutableDict setValue:self.rProvince forKey:kResumeDeliveryModelRProvince];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kResumeDeliveryModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uResID] forKey:kResumeDeliveryModelUResID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.rType = [aDecoder decodeDoubleForKey:kResumeDeliveryModelRType];
    self.rTypes = [aDecoder decodeDoubleForKey:kResumeDeliveryModelRTypes];
    self.rCity = [aDecoder decodeObjectForKey:kResumeDeliveryModelRCity];
    self.pNamep = [aDecoder decodeObjectForKey:kResumeDeliveryModelPNamep];
    self.pName = [aDecoder decodeObjectForKey:kResumeDeliveryModelPName];
    self.rProvince = [aDecoder decodeObjectForKey:kResumeDeliveryModelRProvince];
    self.iDProperty = [aDecoder decodeDoubleForKey:kResumeDeliveryModelID];
    self.uResID = [aDecoder decodeDoubleForKey:kResumeDeliveryModelUResID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_rType forKey:kResumeDeliveryModelRType];
    [aCoder encodeDouble:_rTypes forKey:kResumeDeliveryModelRTypes];
    [aCoder encodeObject:_rCity forKey:kResumeDeliveryModelRCity];
    [aCoder encodeObject:_pNamep forKey:kResumeDeliveryModelPNamep];
    [aCoder encodeObject:_pName forKey:kResumeDeliveryModelPName];
    [aCoder encodeObject:_rProvince forKey:kResumeDeliveryModelRProvince];
    [aCoder encodeDouble:_iDProperty forKey:kResumeDeliveryModelID];
    [aCoder encodeDouble:_uResID forKey:kResumeDeliveryModelUResID];
}

- (id)copyWithZone:(NSZone *)zone {
    ResumeDeliveryModel *copy = [[ResumeDeliveryModel alloc] init];
    
    
    
    if (copy) {

        copy.rType = self.rType;
        copy.rTypes = self.rTypes;
        copy.rCity = [self.rCity copyWithZone:zone];
        copy.pNamep = [self.pNamep copyWithZone:zone];
        copy.pName = [self.pName copyWithZone:zone];
        copy.rProvince = [self.rProvince copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.uResID = self.uResID;
    }
    
    return copy;
}


@end
