//
//  ResumeDeliveryModel.h
//
//  Created by 晨光 宋 on 17/2/4
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ResumeDeliveryModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double rType;
@property (nonatomic, assign) double rTypes;
@property (nonatomic, strong) NSString *rCity;
@property (nonatomic, strong) NSString *pNamep;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *rProvince;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double uResID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
