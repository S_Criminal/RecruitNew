//
//  WorkDetailDescripeModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkDetailDescripeModel : NSObject

@property (nonatomic ,strong) NSArray *datasArray;

@property (nonatomic ,strong) NSString *title;
@property (nonatomic ,strong) NSString *content;


@end
