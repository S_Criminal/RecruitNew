//
//  PositionWorkModel.m
//
//  Created by 晨光 宋 on 17/2/22
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PositionWorkModel.h"


NSString *const kPositionWorkModelID = @"ID";
NSString *const kPositionWorkModelPCity = @"P_City";
NSString *const kPositionWorkModelUName = @"U_Name";
NSString *const kPositionWorkModelPPayS = @"P_PayS";
NSString *const kPositionWorkModelPMerry = @"P_Merry";
NSString *const kPositionWorkModelPAges = @"P_Ages";
NSString *const kPositionWorkModelCIndustry = @"C_Industry";
NSString *const kPositionWorkModelEID = @"EID";
NSString *const kPositionWorkModelUpdateDate = @"UpdateDate";
NSString *const kPositionWorkModelCLogo = @"C_Logo";
NSString *const kPositionWorkModelPtNamep = @"Pt_Namep";
NSString *const kPositionWorkModelPDescription = @"P_Description";
NSString *const kPositionWorkModelPAge = @"P_Age";
NSString *const kPositionWorkModelCReplyD = @"C_ReplyD";
NSString *const kPositionWorkModelPTemptation = @"P_Temptation";
NSString *const kPositionWorkModelULphone = @"U_Lphone";
NSString *const kPositionWorkModelPName = @"P_Name";
NSString *const kPositionWorkModelCVideo = @"C_Video";
NSString *const kPositionWorkModelPType = @"P_Type";
NSString *const kPositionWorkModelPNature = @"P_Nature";
NSString *const kPositionWorkModelUID = @"UID";
NSString *const kPositionWorkModelPEducation = @"P_Education";
NSString *const kPositionWorkModelINameP = @"I_NameP";
NSString *const kPositionWorkModelCName = @"C_Name";
NSString *const kPositionWorkModelPPayID = @"P_PayID";
NSString *const kPositionWorkModelCAduitStatus = @"C_AduitStatus";
NSString *const kPositionWorkModelPTypes = @"P_Types";
NSString *const kPositionWorkModelPArea = @"P_Area";
NSString *const kPositionWorkModelCProcessRate = @"C_ProcessRate";
NSString *const kPositionWorkModelPAddress = @"P_Address";
NSString *const kPositionWorkModelIName = @"I_Name";
NSString *const kPositionWorkModelPDeliver = @"P_Deliver";
NSString *const kPositionWorkModelUHead = @"U_Head";
NSString *const kPositionWorkModelUEmail = @"U_Email";
NSString *const kPositionWorkModelPExp = @"P_Exp";
NSString *const kPositionWorkModelPSex = @"P_Sex";
NSString *const kPositionWorkModelPtName = @"Pt_Name";
NSString *const kPositionWorkModelCScale = @"C_Scale";
NSString *const kPositionWorkModelCVcover = @"C_Vcover";
NSString *const kPositionWorkModelPEducationID = @"P_EducationID";
NSString *const kPositionWorkModelCProcessT = @"C_ProcessT";
NSString *const kPositionWorkModelPPay = @"P_Pay";
NSString *const kPositionWorkModelCIndustrys = @"C_Industrys";
NSString *const kPositionWorkModelWName = @"W_Name";
NSString *const kPositionWorkModelCID = @"CID";
NSString *const kPositionWorkModelPProvice = @"P_Provice";
NSString *const kPositionWorkModelPExpID = @"P_ExpID";
NSString *const kPositionWorkModelUSex = @"U_Sex";
NSString *const kPositionWorkModelPVisit = @"P_Visit";
NSString *const kPositionWorkModelPPayE = @"P_PayE";
NSString *const kPositionWorkModelPTypese = @"P_Typese";
NSString *const kPositionWorkModelUPosition = @"U_Position";
NSString *const kPositionWorkModelPPeople = @"P_People";
NSString *const kPositionWorkModelPEndD = @"P_EndD";


@interface PositionWorkModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PositionWorkModel

@synthesize iDProperty = _iDProperty;
@synthesize pCity = _pCity;
@synthesize uName = _uName;
@synthesize pPayS = _pPayS;
@synthesize pMerry = _pMerry;
@synthesize pAges = _pAges;
@synthesize cIndustry = _cIndustry;
@synthesize eID = _eID;
@synthesize updateDate = _updateDate;
@synthesize cLogo = _cLogo;
@synthesize ptNamep = _ptNamep;
@synthesize pDescription = _pDescription;
@synthesize pAge = _pAge;
@synthesize cReplyD = _cReplyD;
@synthesize pTemptation = _pTemptation;
@synthesize uLphone = _uLphone;
@synthesize pName = _pName;
@synthesize cVideo = _cVideo;
@synthesize pType = _pType;
@synthesize pNature = _pNature;
@synthesize uID = _uID;
@synthesize pEducation = _pEducation;
@synthesize iNameP = _iNameP;
@synthesize cName = _cName;
@synthesize pPayID = _pPayID;
@synthesize cAduitStatus = _cAduitStatus;
@synthesize pTypes = _pTypes;
@synthesize pArea = _pArea;
@synthesize cProcessRate = _cProcessRate;
@synthesize pAddress = _pAddress;
@synthesize iName = _iName;
@synthesize pDeliver = _pDeliver;
@synthesize uHead = _uHead;
@synthesize uEmail = _uEmail;
@synthesize pExp = _pExp;
@synthesize pSex = _pSex;
@synthesize ptName = _ptName;
@synthesize cScale = _cScale;
@synthesize cVcover = _cVcover;
@synthesize pEducationID = _pEducationID;
@synthesize cProcessT = _cProcessT;
@synthesize pPay = _pPay;
@synthesize cIndustrys = _cIndustrys;
@synthesize wName = _wName;
@synthesize cID = _cID;
@synthesize pProvice = _pProvice;
@synthesize pExpID = _pExpID;
@synthesize uSex = _uSex;
@synthesize pVisit = _pVisit;
@synthesize pPayE = _pPayE;
@synthesize pTypese = _pTypese;
@synthesize uPosition = _uPosition;
@synthesize pPeople = _pPeople;
@synthesize pEndD = _pEndD;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kPositionWorkModelID fromDictionary:dict] doubleValue];
            self.pCity = [self objectOrNilForKey:kPositionWorkModelPCity fromDictionary:dict];
            self.uName = [self objectOrNilForKey:kPositionWorkModelUName fromDictionary:dict];
            self.pPayS = [[self objectOrNilForKey:kPositionWorkModelPPayS fromDictionary:dict] doubleValue];
            self.pMerry = [[self objectOrNilForKey:kPositionWorkModelPMerry fromDictionary:dict] doubleValue];
            self.pAges = [[self objectOrNilForKey:kPositionWorkModelPAges fromDictionary:dict] doubleValue];
            self.cIndustry = [[self objectOrNilForKey:kPositionWorkModelCIndustry fromDictionary:dict] doubleValue];
            self.eID = [[self objectOrNilForKey:kPositionWorkModelEID fromDictionary:dict] doubleValue];
            self.updateDate = [self objectOrNilForKey:kPositionWorkModelUpdateDate fromDictionary:dict];
            self.cLogo = [self objectOrNilForKey:kPositionWorkModelCLogo fromDictionary:dict];
            self.ptNamep = [self objectOrNilForKey:kPositionWorkModelPtNamep fromDictionary:dict];
            self.pDescription = [self objectOrNilForKey:kPositionWorkModelPDescription fromDictionary:dict];
            self.pAge = [[self objectOrNilForKey:kPositionWorkModelPAge fromDictionary:dict] doubleValue];
            self.cReplyD = [self objectOrNilForKey:kPositionWorkModelCReplyD fromDictionary:dict];
            self.pTemptation = [self objectOrNilForKey:kPositionWorkModelPTemptation fromDictionary:dict];
            self.uLphone = [self objectOrNilForKey:kPositionWorkModelULphone fromDictionary:dict];
            self.pName = [self objectOrNilForKey:kPositionWorkModelPName fromDictionary:dict];
            self.cVideo = [self objectOrNilForKey:kPositionWorkModelCVideo fromDictionary:dict];
            self.pType = [[self objectOrNilForKey:kPositionWorkModelPType fromDictionary:dict] doubleValue];
            self.pNature = [self objectOrNilForKey:kPositionWorkModelPNature fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kPositionWorkModelUID fromDictionary:dict] doubleValue];
            self.pEducation = [self objectOrNilForKey:kPositionWorkModelPEducation fromDictionary:dict];
            self.iNameP = [self objectOrNilForKey:kPositionWorkModelINameP fromDictionary:dict];
            self.cName = [self objectOrNilForKey:kPositionWorkModelCName fromDictionary:dict];
            self.pPayID = [[self objectOrNilForKey:kPositionWorkModelPPayID fromDictionary:dict] doubleValue];
            self.cAduitStatus = [[self objectOrNilForKey:kPositionWorkModelCAduitStatus fromDictionary:dict] doubleValue];
            self.pTypes = [[self objectOrNilForKey:kPositionWorkModelPTypes fromDictionary:dict] doubleValue];
            self.pArea = [self objectOrNilForKey:kPositionWorkModelPArea fromDictionary:dict];
            self.cProcessRate = [[self objectOrNilForKey:kPositionWorkModelCProcessRate fromDictionary:dict] doubleValue];
            self.pAddress = [self objectOrNilForKey:kPositionWorkModelPAddress fromDictionary:dict];
            self.iName = [self objectOrNilForKey:kPositionWorkModelIName fromDictionary:dict];
            self.pDeliver = [[self objectOrNilForKey:kPositionWorkModelPDeliver fromDictionary:dict] doubleValue];
            self.uHead = [self objectOrNilForKey:kPositionWorkModelUHead fromDictionary:dict];
            self.uEmail = [self objectOrNilForKey:kPositionWorkModelUEmail fromDictionary:dict];
            self.pExp = [self objectOrNilForKey:kPositionWorkModelPExp fromDictionary:dict];
            self.pSex = [[self objectOrNilForKey:kPositionWorkModelPSex fromDictionary:dict] doubleValue];
            self.ptName = [self objectOrNilForKey:kPositionWorkModelPtName fromDictionary:dict];
            self.cScale = [self objectOrNilForKey:kPositionWorkModelCScale fromDictionary:dict];
            self.cVcover = [self objectOrNilForKey:kPositionWorkModelCVcover fromDictionary:dict];
            self.pEducationID = [[self objectOrNilForKey:kPositionWorkModelPEducationID fromDictionary:dict] doubleValue];
            self.cProcessT = [[self objectOrNilForKey:kPositionWorkModelCProcessT fromDictionary:dict] doubleValue];
            self.pPay = [self objectOrNilForKey:kPositionWorkModelPPay fromDictionary:dict];
            self.cIndustrys = [[self objectOrNilForKey:kPositionWorkModelCIndustrys fromDictionary:dict] doubleValue];
            self.wName = [self objectOrNilForKey:kPositionWorkModelWName fromDictionary:dict];
            self.cID = [[self objectOrNilForKey:kPositionWorkModelCID fromDictionary:dict] doubleValue];
            self.pProvice = [self objectOrNilForKey:kPositionWorkModelPProvice fromDictionary:dict];
            self.pExpID = [[self objectOrNilForKey:kPositionWorkModelPExpID fromDictionary:dict] doubleValue];
            self.uSex = [self objectOrNilForKey:kPositionWorkModelUSex fromDictionary:dict];
            self.pVisit = [[self objectOrNilForKey:kPositionWorkModelPVisit fromDictionary:dict] doubleValue];
            self.pPayE = [[self objectOrNilForKey:kPositionWorkModelPPayE fromDictionary:dict] doubleValue];
            self.pTypese = [self objectOrNilForKey:kPositionWorkModelPTypese fromDictionary:dict];
            self.uPosition = [self objectOrNilForKey:kPositionWorkModelUPosition fromDictionary:dict];
            self.pPeople = [[self objectOrNilForKey:kPositionWorkModelPPeople fromDictionary:dict] doubleValue];
            self.pEndD = [self objectOrNilForKey:kPositionWorkModelPEndD fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kPositionWorkModelID];
    [mutableDict setValue:self.pCity forKey:kPositionWorkModelPCity];
    [mutableDict setValue:self.uName forKey:kPositionWorkModelUName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayS] forKey:kPositionWorkModelPPayS];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pMerry] forKey:kPositionWorkModelPMerry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pAges] forKey:kPositionWorkModelPAges];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustry] forKey:kPositionWorkModelCIndustry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eID] forKey:kPositionWorkModelEID];
    [mutableDict setValue:self.updateDate forKey:kPositionWorkModelUpdateDate];
    [mutableDict setValue:self.cLogo forKey:kPositionWorkModelCLogo];
    [mutableDict setValue:self.ptNamep forKey:kPositionWorkModelPtNamep];
    [mutableDict setValue:self.pDescription forKey:kPositionWorkModelPDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pAge] forKey:kPositionWorkModelPAge];
    [mutableDict setValue:self.cReplyD forKey:kPositionWorkModelCReplyD];
    [mutableDict setValue:self.pTemptation forKey:kPositionWorkModelPTemptation];
    [mutableDict setValue:self.uLphone forKey:kPositionWorkModelULphone];
    [mutableDict setValue:self.pName forKey:kPositionWorkModelPName];
    [mutableDict setValue:self.cVideo forKey:kPositionWorkModelCVideo];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pType] forKey:kPositionWorkModelPType];
    [mutableDict setValue:self.pNature forKey:kPositionWorkModelPNature];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kPositionWorkModelUID];
    [mutableDict setValue:self.pEducation forKey:kPositionWorkModelPEducation];
    [mutableDict setValue:self.iNameP forKey:kPositionWorkModelINameP];
    [mutableDict setValue:self.cName forKey:kPositionWorkModelCName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayID] forKey:kPositionWorkModelPPayID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cAduitStatus] forKey:kPositionWorkModelCAduitStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pTypes] forKey:kPositionWorkModelPTypes];
    [mutableDict setValue:self.pArea forKey:kPositionWorkModelPArea];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cProcessRate] forKey:kPositionWorkModelCProcessRate];
    [mutableDict setValue:self.pAddress forKey:kPositionWorkModelPAddress];
    [mutableDict setValue:self.iName forKey:kPositionWorkModelIName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pDeliver] forKey:kPositionWorkModelPDeliver];
    [mutableDict setValue:self.uHead forKey:kPositionWorkModelUHead];
    [mutableDict setValue:self.uEmail forKey:kPositionWorkModelUEmail];
    [mutableDict setValue:self.pExp forKey:kPositionWorkModelPExp];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pSex] forKey:kPositionWorkModelPSex];
    [mutableDict setValue:self.ptName forKey:kPositionWorkModelPtName];
    [mutableDict setValue:self.cScale forKey:kPositionWorkModelCScale];
    [mutableDict setValue:self.cVcover forKey:kPositionWorkModelCVcover];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pEducationID] forKey:kPositionWorkModelPEducationID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cProcessT] forKey:kPositionWorkModelCProcessT];
    [mutableDict setValue:self.pPay forKey:kPositionWorkModelPPay];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cIndustrys] forKey:kPositionWorkModelCIndustrys];
    [mutableDict setValue:self.wName forKey:kPositionWorkModelWName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cID] forKey:kPositionWorkModelCID];
    [mutableDict setValue:self.pProvice forKey:kPositionWorkModelPProvice];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pExpID] forKey:kPositionWorkModelPExpID];
    [mutableDict setValue:self.uSex forKey:kPositionWorkModelUSex];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pVisit] forKey:kPositionWorkModelPVisit];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPayE] forKey:kPositionWorkModelPPayE];
    [mutableDict setValue:self.pTypese forKey:kPositionWorkModelPTypese];
    [mutableDict setValue:self.uPosition forKey:kPositionWorkModelUPosition];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pPeople] forKey:kPositionWorkModelPPeople];
    [mutableDict setValue:self.pEndD forKey:kPositionWorkModelPEndD];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kPositionWorkModelID];
    self.pCity = [aDecoder decodeObjectForKey:kPositionWorkModelPCity];
    self.uName = [aDecoder decodeObjectForKey:kPositionWorkModelUName];
    self.pPayS = [aDecoder decodeDoubleForKey:kPositionWorkModelPPayS];
    self.pMerry = [aDecoder decodeDoubleForKey:kPositionWorkModelPMerry];
    self.pAges = [aDecoder decodeDoubleForKey:kPositionWorkModelPAges];
    self.cIndustry = [aDecoder decodeDoubleForKey:kPositionWorkModelCIndustry];
    self.eID = [aDecoder decodeDoubleForKey:kPositionWorkModelEID];
    self.updateDate = [aDecoder decodeObjectForKey:kPositionWorkModelUpdateDate];
    self.cLogo = [aDecoder decodeObjectForKey:kPositionWorkModelCLogo];
    self.ptNamep = [aDecoder decodeObjectForKey:kPositionWorkModelPtNamep];
    self.pDescription = [aDecoder decodeObjectForKey:kPositionWorkModelPDescription];
    self.pAge = [aDecoder decodeDoubleForKey:kPositionWorkModelPAge];
    self.cReplyD = [aDecoder decodeObjectForKey:kPositionWorkModelCReplyD];
    self.pTemptation = [aDecoder decodeObjectForKey:kPositionWorkModelPTemptation];
    self.uLphone = [aDecoder decodeObjectForKey:kPositionWorkModelULphone];
    self.pName = [aDecoder decodeObjectForKey:kPositionWorkModelPName];
    self.cVideo = [aDecoder decodeObjectForKey:kPositionWorkModelCVideo];
    self.pType = [aDecoder decodeDoubleForKey:kPositionWorkModelPType];
    self.pNature = [aDecoder decodeObjectForKey:kPositionWorkModelPNature];
    self.uID = [aDecoder decodeDoubleForKey:kPositionWorkModelUID];
    self.pEducation = [aDecoder decodeObjectForKey:kPositionWorkModelPEducation];
    self.iNameP = [aDecoder decodeObjectForKey:kPositionWorkModelINameP];
    self.cName = [aDecoder decodeObjectForKey:kPositionWorkModelCName];
    self.pPayID = [aDecoder decodeDoubleForKey:kPositionWorkModelPPayID];
    self.cAduitStatus = [aDecoder decodeDoubleForKey:kPositionWorkModelCAduitStatus];
    self.pTypes = [aDecoder decodeDoubleForKey:kPositionWorkModelPTypes];
    self.pArea = [aDecoder decodeObjectForKey:kPositionWorkModelPArea];
    self.cProcessRate = [aDecoder decodeDoubleForKey:kPositionWorkModelCProcessRate];
    self.pAddress = [aDecoder decodeObjectForKey:kPositionWorkModelPAddress];
    self.iName = [aDecoder decodeObjectForKey:kPositionWorkModelIName];
    self.pDeliver = [aDecoder decodeDoubleForKey:kPositionWorkModelPDeliver];
    self.uHead = [aDecoder decodeObjectForKey:kPositionWorkModelUHead];
    self.uEmail = [aDecoder decodeObjectForKey:kPositionWorkModelUEmail];
    self.pExp = [aDecoder decodeObjectForKey:kPositionWorkModelPExp];
    self.pSex = [aDecoder decodeDoubleForKey:kPositionWorkModelPSex];
    self.ptName = [aDecoder decodeObjectForKey:kPositionWorkModelPtName];
    self.cScale = [aDecoder decodeObjectForKey:kPositionWorkModelCScale];
    self.cVcover = [aDecoder decodeObjectForKey:kPositionWorkModelCVcover];
    self.pEducationID = [aDecoder decodeDoubleForKey:kPositionWorkModelPEducationID];
    self.cProcessT = [aDecoder decodeDoubleForKey:kPositionWorkModelCProcessT];
    self.pPay = [aDecoder decodeObjectForKey:kPositionWorkModelPPay];
    self.cIndustrys = [aDecoder decodeDoubleForKey:kPositionWorkModelCIndustrys];
    self.wName = [aDecoder decodeObjectForKey:kPositionWorkModelWName];
    self.cID = [aDecoder decodeDoubleForKey:kPositionWorkModelCID];
    self.pProvice = [aDecoder decodeObjectForKey:kPositionWorkModelPProvice];
    self.pExpID = [aDecoder decodeDoubleForKey:kPositionWorkModelPExpID];
    self.uSex = [aDecoder decodeObjectForKey:kPositionWorkModelUSex];
    self.pVisit = [aDecoder decodeDoubleForKey:kPositionWorkModelPVisit];
    self.pPayE = [aDecoder decodeDoubleForKey:kPositionWorkModelPPayE];
    self.pTypese = [aDecoder decodeObjectForKey:kPositionWorkModelPTypese];
    self.uPosition = [aDecoder decodeObjectForKey:kPositionWorkModelUPosition];
    self.pPeople = [aDecoder decodeDoubleForKey:kPositionWorkModelPPeople];
    self.pEndD = [aDecoder decodeObjectForKey:kPositionWorkModelPEndD];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kPositionWorkModelID];
    [aCoder encodeObject:_pCity forKey:kPositionWorkModelPCity];
    [aCoder encodeObject:_uName forKey:kPositionWorkModelUName];
    [aCoder encodeDouble:_pPayS forKey:kPositionWorkModelPPayS];
    [aCoder encodeDouble:_pMerry forKey:kPositionWorkModelPMerry];
    [aCoder encodeDouble:_pAges forKey:kPositionWorkModelPAges];
    [aCoder encodeDouble:_cIndustry forKey:kPositionWorkModelCIndustry];
    [aCoder encodeDouble:_eID forKey:kPositionWorkModelEID];
    [aCoder encodeObject:_updateDate forKey:kPositionWorkModelUpdateDate];
    [aCoder encodeObject:_cLogo forKey:kPositionWorkModelCLogo];
    [aCoder encodeObject:_ptNamep forKey:kPositionWorkModelPtNamep];
    [aCoder encodeObject:_pDescription forKey:kPositionWorkModelPDescription];
    [aCoder encodeDouble:_pAge forKey:kPositionWorkModelPAge];
    [aCoder encodeObject:_cReplyD forKey:kPositionWorkModelCReplyD];
    [aCoder encodeObject:_pTemptation forKey:kPositionWorkModelPTemptation];
    [aCoder encodeObject:_uLphone forKey:kPositionWorkModelULphone];
    [aCoder encodeObject:_pName forKey:kPositionWorkModelPName];
    [aCoder encodeObject:_cVideo forKey:kPositionWorkModelCVideo];
    [aCoder encodeDouble:_pType forKey:kPositionWorkModelPType];
    [aCoder encodeObject:_pNature forKey:kPositionWorkModelPNature];
    [aCoder encodeDouble:_uID forKey:kPositionWorkModelUID];
    [aCoder encodeObject:_pEducation forKey:kPositionWorkModelPEducation];
    [aCoder encodeObject:_iNameP forKey:kPositionWorkModelINameP];
    [aCoder encodeObject:_cName forKey:kPositionWorkModelCName];
    [aCoder encodeDouble:_pPayID forKey:kPositionWorkModelPPayID];
    [aCoder encodeDouble:_cAduitStatus forKey:kPositionWorkModelCAduitStatus];
    [aCoder encodeDouble:_pTypes forKey:kPositionWorkModelPTypes];
    [aCoder encodeObject:_pArea forKey:kPositionWorkModelPArea];
    [aCoder encodeDouble:_cProcessRate forKey:kPositionWorkModelCProcessRate];
    [aCoder encodeObject:_pAddress forKey:kPositionWorkModelPAddress];
    [aCoder encodeObject:_iName forKey:kPositionWorkModelIName];
    [aCoder encodeDouble:_pDeliver forKey:kPositionWorkModelPDeliver];
    [aCoder encodeObject:_uHead forKey:kPositionWorkModelUHead];
    [aCoder encodeObject:_uEmail forKey:kPositionWorkModelUEmail];
    [aCoder encodeObject:_pExp forKey:kPositionWorkModelPExp];
    [aCoder encodeDouble:_pSex forKey:kPositionWorkModelPSex];
    [aCoder encodeObject:_ptName forKey:kPositionWorkModelPtName];
    [aCoder encodeObject:_cScale forKey:kPositionWorkModelCScale];
    [aCoder encodeObject:_cVcover forKey:kPositionWorkModelCVcover];
    [aCoder encodeDouble:_pEducationID forKey:kPositionWorkModelPEducationID];
    [aCoder encodeDouble:_cProcessT forKey:kPositionWorkModelCProcessT];
    [aCoder encodeObject:_pPay forKey:kPositionWorkModelPPay];
    [aCoder encodeDouble:_cIndustrys forKey:kPositionWorkModelCIndustrys];
    [aCoder encodeObject:_wName forKey:kPositionWorkModelWName];
    [aCoder encodeDouble:_cID forKey:kPositionWorkModelCID];
    [aCoder encodeObject:_pProvice forKey:kPositionWorkModelPProvice];
    [aCoder encodeDouble:_pExpID forKey:kPositionWorkModelPExpID];
    [aCoder encodeObject:_uSex forKey:kPositionWorkModelUSex];
    [aCoder encodeDouble:_pVisit forKey:kPositionWorkModelPVisit];
    [aCoder encodeDouble:_pPayE forKey:kPositionWorkModelPPayE];
    [aCoder encodeObject:_pTypese forKey:kPositionWorkModelPTypese];
    [aCoder encodeObject:_uPosition forKey:kPositionWorkModelUPosition];
    [aCoder encodeDouble:_pPeople forKey:kPositionWorkModelPPeople];
    [aCoder encodeObject:_pEndD forKey:kPositionWorkModelPEndD];
}

- (id)copyWithZone:(NSZone *)zone {
    PositionWorkModel *copy = [[PositionWorkModel alloc] init];
    
    
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.pCity = [self.pCity copyWithZone:zone];
        copy.uName = [self.uName copyWithZone:zone];
        copy.pPayS = self.pPayS;
        copy.pMerry = self.pMerry;
        copy.pAges = self.pAges;
        copy.cIndustry = self.cIndustry;
        copy.eID = self.eID;
        copy.updateDate = [self.updateDate copyWithZone:zone];
        copy.cLogo = [self.cLogo copyWithZone:zone];
        copy.ptNamep = [self.ptNamep copyWithZone:zone];
        copy.pDescription = [self.pDescription copyWithZone:zone];
        copy.pAge = self.pAge;
        copy.cReplyD = [self.cReplyD copyWithZone:zone];
        copy.pTemptation = [self.pTemptation copyWithZone:zone];
        copy.uLphone = [self.uLphone copyWithZone:zone];
        copy.pName = [self.pName copyWithZone:zone];
        copy.cVideo = [self.cVideo copyWithZone:zone];
        copy.pType = self.pType;
        copy.pNature = [self.pNature copyWithZone:zone];
        copy.uID = self.uID;
        copy.pEducation = [self.pEducation copyWithZone:zone];
        copy.iNameP = [self.iNameP copyWithZone:zone];
        copy.cName = [self.cName copyWithZone:zone];
        copy.pPayID = self.pPayID;
        copy.cAduitStatus = self.cAduitStatus;
        copy.pTypes = self.pTypes;
        copy.pArea = [self.pArea copyWithZone:zone];
        copy.cProcessRate = self.cProcessRate;
        copy.pAddress = [self.pAddress copyWithZone:zone];
        copy.iName = [self.iName copyWithZone:zone];
        copy.pDeliver = self.pDeliver;
        copy.uHead = [self.uHead copyWithZone:zone];
        copy.uEmail = [self.uEmail copyWithZone:zone];
        copy.pExp = [self.pExp copyWithZone:zone];
        copy.pSex = self.pSex;
        copy.ptName = [self.ptName copyWithZone:zone];
        copy.cScale = [self.cScale copyWithZone:zone];
        copy.cVcover = [self.cVcover copyWithZone:zone];
        copy.pEducationID = self.pEducationID;
        copy.cProcessT = self.cProcessT;
        copy.pPay = [self.pPay copyWithZone:zone];
        copy.cIndustrys = self.cIndustrys;
        copy.wName = [self.wName copyWithZone:zone];
        copy.cID = self.cID;
        copy.pProvice = [self.pProvice copyWithZone:zone];
        copy.pExpID = self.pExpID;
        copy.uSex = [self.uSex copyWithZone:zone];
        copy.pVisit = self.pVisit;
        copy.pPayE = self.pPayE;
        copy.pTypese = [self.pTypese copyWithZone:zone];
        copy.uPosition = [self.uPosition copyWithZone:zone];
        copy.pPeople = self.pPeople;
        copy.pEndD = [self.pEndD copyWithZone:zone];
    }
    
    return copy;
}


@end
