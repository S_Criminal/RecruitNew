//
//  PositionWorkModel.h
//
//  Created by 晨光 宋 on 17/2/22
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PositionWorkModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *pCity;
@property (nonatomic, strong) NSString *uName;
@property (nonatomic, assign) double pPayS;
@property (nonatomic, assign) double pMerry;
@property (nonatomic, assign) double pAges;
@property (nonatomic, assign) double cIndustry;
@property (nonatomic, assign) double eID;
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, strong) NSString *cLogo;
@property (nonatomic, strong) NSString *ptNamep;
@property (nonatomic, strong) NSString *pDescription;
@property (nonatomic, assign) double pAge;
@property (nonatomic, strong) NSString *cReplyD;
@property (nonatomic, strong) NSString *pTemptation;
@property (nonatomic, strong) NSString *uLphone;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *cVideo;
@property (nonatomic, assign) double pType;
@property (nonatomic, strong) NSString *pNature;
@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *pEducation;
@property (nonatomic, strong) NSString *iNameP;
@property (nonatomic, strong) NSString *cName;
@property (nonatomic, assign) double pPayID;
@property (nonatomic, assign) double cAduitStatus;
@property (nonatomic, assign) double pTypes;
@property (nonatomic, strong) NSString *pArea;
@property (nonatomic, assign) double cProcessRate;
@property (nonatomic, strong) NSString *pAddress;
@property (nonatomic, strong) NSString *iName;
@property (nonatomic, assign) double pDeliver;
@property (nonatomic, strong) NSString *uHead;
@property (nonatomic, strong) NSString *uEmail;
@property (nonatomic, strong) NSString *pExp;
@property (nonatomic, assign) double pSex;
@property (nonatomic, strong) NSString *ptName;
@property (nonatomic, strong) NSString *cScale;
@property (nonatomic, strong) NSString *cVcover;
@property (nonatomic, assign) double pEducationID;
@property (nonatomic, assign) double cProcessT;
@property (nonatomic, strong) NSString *pPay;
@property (nonatomic, assign) double cIndustrys;
@property (nonatomic, strong) NSString *wName;
@property (nonatomic, assign) double cID;
@property (nonatomic, strong) NSString *pProvice;
@property (nonatomic, assign) double pExpID;
@property (nonatomic, strong) NSString *uSex;
@property (nonatomic, assign) double pVisit;
@property (nonatomic, assign) double pPayE;
@property (nonatomic, strong) NSString *pTypese;
@property (nonatomic, strong) NSString *uPosition;
@property (nonatomic, assign) double pPeople;
@property (nonatomic, strong) NSString *pEndD;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
