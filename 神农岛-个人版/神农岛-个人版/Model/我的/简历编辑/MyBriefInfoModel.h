//
//  MyBriefInfoModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyBriefInfoModel : NSObject

@property (nonatomic ,strong) NSArray *myInfoDatas;
@property (nonatomic ,strong) NSArray *myExperienceDatas;
@property (nonatomic ,strong) NSArray *myPositionTrendDataas;
@property (nonatomic ,strong) NSArray *myAchieveDatas;
@property (nonatomic ,strong) NSArray *myOtherInfoDatas;

@end
