//
//  MyBriefInfoModel.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyBriefInfoModel.h"

@implementation MyBriefInfoModel
/**
 *  数组的第一个元素是图标的名称数组
 *       第二个元素是右边标题的名称数组
 *       第三个元素是右边内容的名称数组
 */

-(NSArray *)myInfoDatas
{
    if (!_myInfoDatas) {
        _myInfoDatas = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"white_myInfo",@"white_mySelf",@"white_Authenticate", nil],[NSArray arrayWithObjects:@"基本信息",@"自我介绍",@"个人认证", nil],[NSArray arrayWithObjects:@"完善基本信息有助于企业HR更好的找到您",@"介绍工作职责，或讲述您正在寻找的机会",@"完善个人认证，有助于您获得过多职位邀请", nil], nil];
    }
    return _myInfoDatas;
}

-(NSArray *)myExperienceDatas
{
    if (!_myExperienceDatas) {
        _myExperienceDatas =  [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"white_edu",@"white_exp", nil],[NSArray arrayWithObjects:@"教育经历",@"工作经历", nil],[NSArray arrayWithObjects:@"添加学历，以真才实学吸引目光",@"展现工作经历，抓住来到身边的机会", nil], nil];
    }
    return _myExperienceDatas;
}

-(NSArray *)myPositionTrendDataas
{
    if (!_myPositionTrendDataas) {
        _myPositionTrendDataas = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"white_city",@"white_position",@"white_pay", nil],[NSArray arrayWithObjects:@"期望城市",@"期望职位",@"薪资要求", nil],[NSArray arrayWithObjects:@"添加期望城市，让HR更好地了解您",@"添加期望职位，让HR更好地了解您",@"添加薪资要求，让HR更好地了解您", nil], nil];
    }
    return _myPositionTrendDataas;
}

-(NSArray *)myAchieveDatas
{
    if (!_myAchieveDatas) {
        _myAchieveDatas = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"white_publish",@"white_cert",@"white_patent",@"white_classes",@"white_project",@"white_prize",@"white_language", nil],[NSArray arrayWithObjects:@"出版作品",@"资质证书",@"专利发明",@"所学课程",@"所做项目",@"荣誉奖项",@"语言能力", nil],[NSArray arrayWithObjects:@"添加出版作品，被搜索到的几率可提高7倍",@"添加资质证书，让其他人认识优秀的您",@"创新技能让您更胜一筹",@"列出您学过的主要课程，展示技能与兴趣",@"技能才华，团队精神，一一展示",@"添加荣誉奖项，让其他人认识更优秀的您",@"升值跳槽，转职海外的制胜法宝", nil], nil];
    }
    return _myAchieveDatas;
}

-(NSArray *)myOtherInfoDatas
{
    if (!_myOtherInfoDatas)
    {
        _myOtherInfoDatas = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"white_life", nil],[NSArray arrayWithObjects:@"生活照片", nil],[NSArray arrayWithObjects:@"添加生活照片，让HR更好地了解您", nil], nil];
    }
    return _myOtherInfoDatas;
}


@end
