//
//  MyBriefNoExistModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyBriefNoExistModel : NSObject
@property (nonatomic ,strong) NSString *title;
@property (nonatomic ,strong) NSString *content;
@property (nonatomic ,strong) NSString *icon;
@property (nonatomic ,strong) NSString *buttonTitle;
@end
