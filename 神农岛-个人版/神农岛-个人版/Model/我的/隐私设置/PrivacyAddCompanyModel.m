//
//  PrivacyAddCompanyModel.m
//
//  Created by 晨光 宋 on 17/2/20
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PrivacyAddCompanyModel.h"


NSString *const kPrivacyAddCompanyModelID = @"ID";
NSString *const kPrivacyAddCompanyModelCName = @"C_Name";
NSString *const kPrivacyAddCompanyModelCNickName = @"C_NickName";


@interface PrivacyAddCompanyModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PrivacyAddCompanyModel

@synthesize iDProperty = _iDProperty;
@synthesize cName = _cName;
@synthesize cNickName = _cNickName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kPrivacyAddCompanyModelID fromDictionary:dict] doubleValue];
            self.cName = [self objectOrNilForKey:kPrivacyAddCompanyModelCName fromDictionary:dict];
            self.cNickName = [self objectOrNilForKey:kPrivacyAddCompanyModelCNickName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kPrivacyAddCompanyModelID];
    [mutableDict setValue:self.cName forKey:kPrivacyAddCompanyModelCName];
    [mutableDict setValue:self.cNickName forKey:kPrivacyAddCompanyModelCNickName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kPrivacyAddCompanyModelID];
    self.cName = [aDecoder decodeObjectForKey:kPrivacyAddCompanyModelCName];
    self.cNickName = [aDecoder decodeObjectForKey:kPrivacyAddCompanyModelCNickName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kPrivacyAddCompanyModelID];
    [aCoder encodeObject:_cName forKey:kPrivacyAddCompanyModelCName];
    [aCoder encodeObject:_cNickName forKey:kPrivacyAddCompanyModelCNickName];
}

- (id)copyWithZone:(NSZone *)zone {
    PrivacyAddCompanyModel *copy = [[PrivacyAddCompanyModel alloc] init];
    
    
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.cName = [self.cName copyWithZone:zone];
        copy.cNickName = [self.cNickName copyWithZone:zone];
    }
    
    return copy;
}


@end
