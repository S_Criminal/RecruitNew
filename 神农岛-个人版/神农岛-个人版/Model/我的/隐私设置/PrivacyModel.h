//
//  PrivacyModel.h
//
//  Created by 晨光 宋 on 17/1/19
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PrivacyModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *cname;
@property (nonatomic, assign) double cID;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double status;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
