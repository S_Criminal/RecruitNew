//
//  PrivacyModel.m
//
//  Created by 晨光 宋 on 17/1/19
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PrivacyModel.h"


NSString *const kPrivacyModelID = @"ID";
NSString *const kPrivacyModelUID = @"UID";
NSString *const kPrivacyModelCname = @"Cname";
NSString *const kPrivacyModelCID = @"CID";
NSString *const kPrivacyModelCreateDate = @"CreateDate";
NSString *const kPrivacyModelStatus = @"Status";


@interface PrivacyModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PrivacyModel

@synthesize iDProperty = _iDProperty;
@synthesize uID = _uID;
@synthesize cname = _cname;
@synthesize cID = _cID;
@synthesize createDate = _createDate;
@synthesize status = _status;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iDProperty = [[self objectOrNilForKey:kPrivacyModelID fromDictionary:dict] doubleValue];
            self.uID = [[self objectOrNilForKey:kPrivacyModelUID fromDictionary:dict] doubleValue];
            self.cname = [self objectOrNilForKey:kPrivacyModelCname fromDictionary:dict];
            self.cID = [[self objectOrNilForKey:kPrivacyModelCID fromDictionary:dict] doubleValue];
            self.createDate = [self objectOrNilForKey:kPrivacyModelCreateDate fromDictionary:dict];
            self.status = [[self objectOrNilForKey:kPrivacyModelStatus fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kPrivacyModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kPrivacyModelUID];
    [mutableDict setValue:self.cname forKey:kPrivacyModelCname];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cID] forKey:kPrivacyModelCID];
    [mutableDict setValue:self.createDate forKey:kPrivacyModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kPrivacyModelStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iDProperty = [aDecoder decodeDoubleForKey:kPrivacyModelID];
    self.uID = [aDecoder decodeDoubleForKey:kPrivacyModelUID];
    self.cname = [aDecoder decodeObjectForKey:kPrivacyModelCname];
    self.cID = [aDecoder decodeDoubleForKey:kPrivacyModelCID];
    self.createDate = [aDecoder decodeObjectForKey:kPrivacyModelCreateDate];
    self.status = [aDecoder decodeDoubleForKey:kPrivacyModelStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_iDProperty forKey:kPrivacyModelID];
    [aCoder encodeDouble:_uID forKey:kPrivacyModelUID];
    [aCoder encodeObject:_cname forKey:kPrivacyModelCname];
    [aCoder encodeDouble:_cID forKey:kPrivacyModelCID];
    [aCoder encodeObject:_createDate forKey:kPrivacyModelCreateDate];
    [aCoder encodeDouble:_status forKey:kPrivacyModelStatus];
}

- (id)copyWithZone:(NSZone *)zone {
    PrivacyModel *copy = [[PrivacyModel alloc] init];
    
    
    
    if (copy) {

        copy.iDProperty = self.iDProperty;
        copy.uID = self.uID;
        copy.cname = [self.cname copyWithZone:zone];
        copy.cID = self.cID;
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.status = self.status;
    }
    
    return copy;
}


@end
