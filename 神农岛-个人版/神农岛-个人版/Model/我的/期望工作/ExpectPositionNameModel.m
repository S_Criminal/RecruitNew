//
//  ExpectPositionNameModel.m
//
//  Created by 晨光 宋 on 17/1/20
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ExpectPositionNameModel.h"


NSString *const kExpectPositionNameModelPSort = @"P_Sort";
NSString *const kExpectPositionNameModelPNamep = @"P_Namep";
NSString *const kExpectPositionNameModelPName = @"P_Name";
NSString *const kExpectPositionNameModelPType = @"P_Type";
NSString *const kExpectPositionNameModelCreateDate = @"CreateDate";
NSString *const kExpectPositionNameModelID = @"ID";
NSString *const kExpectPositionNameModelParentID = @"ParentID";


@interface ExpectPositionNameModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ExpectPositionNameModel

@synthesize pSort = _pSort;
@synthesize pNamep = _pNamep;
@synthesize pName = _pName;
@synthesize pType = _pType;
@synthesize createDate = _createDate;
@synthesize iDProperty = _iDProperty;
@synthesize parentID = _parentID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pSort = [[self objectOrNilForKey:kExpectPositionNameModelPSort fromDictionary:dict] doubleValue];
            self.pNamep = [self objectOrNilForKey:kExpectPositionNameModelPNamep fromDictionary:dict];
            self.pName = [self objectOrNilForKey:kExpectPositionNameModelPName fromDictionary:dict];
            self.pType = [[self objectOrNilForKey:kExpectPositionNameModelPType fromDictionary:dict] doubleValue];
            self.createDate = [self objectOrNilForKey:kExpectPositionNameModelCreateDate fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kExpectPositionNameModelID fromDictionary:dict] doubleValue];
            self.parentID = [[self objectOrNilForKey:kExpectPositionNameModelParentID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pSort] forKey:kExpectPositionNameModelPSort];
    [mutableDict setValue:self.pNamep forKey:kExpectPositionNameModelPNamep];
    [mutableDict setValue:self.pName forKey:kExpectPositionNameModelPName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pType] forKey:kExpectPositionNameModelPType];
    [mutableDict setValue:self.createDate forKey:kExpectPositionNameModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kExpectPositionNameModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.parentID] forKey:kExpectPositionNameModelParentID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.pSort = [aDecoder decodeDoubleForKey:kExpectPositionNameModelPSort];
    self.pNamep = [aDecoder decodeObjectForKey:kExpectPositionNameModelPNamep];
    self.pName = [aDecoder decodeObjectForKey:kExpectPositionNameModelPName];
    self.pType = [aDecoder decodeDoubleForKey:kExpectPositionNameModelPType];
    self.createDate = [aDecoder decodeObjectForKey:kExpectPositionNameModelCreateDate];
    self.iDProperty = [aDecoder decodeDoubleForKey:kExpectPositionNameModelID];
    self.parentID = [aDecoder decodeDoubleForKey:kExpectPositionNameModelParentID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_pSort forKey:kExpectPositionNameModelPSort];
    [aCoder encodeObject:_pNamep forKey:kExpectPositionNameModelPNamep];
    [aCoder encodeObject:_pName forKey:kExpectPositionNameModelPName];
    [aCoder encodeDouble:_pType forKey:kExpectPositionNameModelPType];
    [aCoder encodeObject:_createDate forKey:kExpectPositionNameModelCreateDate];
    [aCoder encodeDouble:_iDProperty forKey:kExpectPositionNameModelID];
    [aCoder encodeDouble:_parentID forKey:kExpectPositionNameModelParentID];
}

- (id)copyWithZone:(NSZone *)zone {
    ExpectPositionNameModel *copy = [[ExpectPositionNameModel alloc] init];
    
    
    
    if (copy) {

        copy.pSort = self.pSort;
        copy.pNamep = [self.pNamep copyWithZone:zone];
        copy.pName = [self.pName copyWithZone:zone];
        copy.pType = self.pType;
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.parentID = self.parentID;
    }
    
    return copy;
}


@end
