//
//  PrendInfoModel.m
//
//  Created by 晨光 宋 on 17/2/10
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PrendInfoModel.h"


NSString *const kPrendInfoModelUID = @"UID";
NSString *const kPrendInfoModelRType = @"R_Type";
NSString *const kPrendInfoModelRTypes = @"R_Types";
NSString *const kPrendInfoModelRPayID = @"R_PayID";
NSString *const kPrendInfoModelRID = @"RID";
NSString *const kPrendInfoModelRCity = @"R_City";
NSString *const kPrendInfoModelPNamep = @"P_Namep";
NSString *const kPrendInfoModelPName = @"P_Name";
NSString *const kPrendInfoModelRProvince = @"R_Province";
NSString *const kPrendInfoModelUResID = @"U_ResID";
NSString *const kPrendInfoModelRPay = @"R_Pay";


@interface PrendInfoModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PrendInfoModel

@synthesize uID = _uID;
@synthesize rType = _rType;
@synthesize rTypes = _rTypes;
@synthesize rPayID = _rPayID;
@synthesize rID = _rID;
@synthesize rCity = _rCity;
@synthesize pNamep = _pNamep;
@synthesize pName = _pName;
@synthesize rProvince = _rProvince;
@synthesize uResID = _uResID;
@synthesize rPay = _rPay;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.uID = [[self objectOrNilForKey:kPrendInfoModelUID fromDictionary:dict] doubleValue];
            self.rType = [[self objectOrNilForKey:kPrendInfoModelRType fromDictionary:dict] doubleValue];
            self.rTypes = [[self objectOrNilForKey:kPrendInfoModelRTypes fromDictionary:dict] doubleValue];
            self.rPayID = [[self objectOrNilForKey:kPrendInfoModelRPayID fromDictionary:dict] doubleValue];
            self.rID = [[self objectOrNilForKey:kPrendInfoModelRID fromDictionary:dict] doubleValue];
            self.rCity = [self objectOrNilForKey:kPrendInfoModelRCity fromDictionary:dict];
            self.pNamep = [self objectOrNilForKey:kPrendInfoModelPNamep fromDictionary:dict];
            self.pName = [self objectOrNilForKey:kPrendInfoModelPName fromDictionary:dict];
            self.rProvince = [self objectOrNilForKey:kPrendInfoModelRProvince fromDictionary:dict];
            self.uResID = [[self objectOrNilForKey:kPrendInfoModelUResID fromDictionary:dict] doubleValue];
            self.rPay = [self objectOrNilForKey:kPrendInfoModelRPay fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kPrendInfoModelUID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rType] forKey:kPrendInfoModelRType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rTypes] forKey:kPrendInfoModelRTypes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rPayID] forKey:kPrendInfoModelRPayID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rID] forKey:kPrendInfoModelRID];
    [mutableDict setValue:self.rCity forKey:kPrendInfoModelRCity];
    [mutableDict setValue:self.pNamep forKey:kPrendInfoModelPNamep];
    [mutableDict setValue:self.pName forKey:kPrendInfoModelPName];
    [mutableDict setValue:self.rProvince forKey:kPrendInfoModelRProvince];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uResID] forKey:kPrendInfoModelUResID];
    [mutableDict setValue:self.rPay forKey:kPrendInfoModelRPay];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.uID = [aDecoder decodeDoubleForKey:kPrendInfoModelUID];
    self.rType = [aDecoder decodeDoubleForKey:kPrendInfoModelRType];
    self.rTypes = [aDecoder decodeDoubleForKey:kPrendInfoModelRTypes];
    self.rPayID = [aDecoder decodeDoubleForKey:kPrendInfoModelRPayID];
    self.rID = [aDecoder decodeDoubleForKey:kPrendInfoModelRID];
    self.rCity = [aDecoder decodeObjectForKey:kPrendInfoModelRCity];
    self.pNamep = [aDecoder decodeObjectForKey:kPrendInfoModelPNamep];
    self.pName = [aDecoder decodeObjectForKey:kPrendInfoModelPName];
    self.rProvince = [aDecoder decodeObjectForKey:kPrendInfoModelRProvince];
    self.uResID = [aDecoder decodeDoubleForKey:kPrendInfoModelUResID];
    self.rPay = [aDecoder decodeObjectForKey:kPrendInfoModelRPay];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_uID forKey:kPrendInfoModelUID];
    [aCoder encodeDouble:_rType forKey:kPrendInfoModelRType];
    [aCoder encodeDouble:_rTypes forKey:kPrendInfoModelRTypes];
    [aCoder encodeDouble:_rPayID forKey:kPrendInfoModelRPayID];
    [aCoder encodeDouble:_rID forKey:kPrendInfoModelRID];
    [aCoder encodeObject:_rCity forKey:kPrendInfoModelRCity];
    [aCoder encodeObject:_pNamep forKey:kPrendInfoModelPNamep];
    [aCoder encodeObject:_pName forKey:kPrendInfoModelPName];
    [aCoder encodeObject:_rProvince forKey:kPrendInfoModelRProvince];
    [aCoder encodeDouble:_uResID forKey:kPrendInfoModelUResID];
    [aCoder encodeObject:_rPay forKey:kPrendInfoModelRPay];
}

- (id)copyWithZone:(NSZone *)zone {
    PrendInfoModel *copy = [[PrendInfoModel alloc] init];
    
    
    
    if (copy) {

        copy.uID = self.uID;
        copy.rType = self.rType;
        copy.rTypes = self.rTypes;
        copy.rPayID = self.rPayID;
        copy.rID = self.rID;
        copy.rCity = [self.rCity copyWithZone:zone];
        copy.pNamep = [self.pNamep copyWithZone:zone];
        copy.pName = [self.pName copyWithZone:zone];
        copy.rProvince = [self.rProvince copyWithZone:zone];
        copy.uResID = self.uResID;
        copy.rPay = [self.rPay copyWithZone:zone];
    }
    
    return copy;
}


@end
