//
//  PrendInfoModel.h
//
//  Created by 晨光 宋 on 17/2/10
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PrendInfoModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double uID;
@property (nonatomic, assign) double rType;
@property (nonatomic, assign) double rTypes;
@property (nonatomic, assign) double rPayID;
@property (nonatomic, assign) double rID;
@property (nonatomic, strong) NSString *rCity;
@property (nonatomic, strong) NSString *pNamep;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *rProvince;
@property (nonatomic, assign) double uResID;
@property (nonatomic, strong) NSString *rPay;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
