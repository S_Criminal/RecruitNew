//
//  MatchSchollModel.h
//
//  Created by   on 17/3/4
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MatchSchollModel : NSObject

@property (nonatomic, strong) NSString *sName;
@property (nonatomic, strong) NSString *sLetter;
@property (nonatomic, strong) NSString *sLogo;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
