//
//  MatchSchollModel.m
//
//  Created by   on 17/3/4
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "MatchSchollModel.h"


NSString *const kMatchSchollModelSName = @"S_Name";
NSString *const kMatchSchollModelSLetter = @"S_Letter";
NSString *const kMatchSchollModelSLogo = @"S_Logo";


@interface MatchSchollModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MatchSchollModel

@synthesize sName = _sName;
@synthesize sLetter = _sLetter;
@synthesize sLogo = _sLogo;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.sName = [self objectOrNilForKey:kMatchSchollModelSName fromDictionary:dict];
            self.sLetter = [self objectOrNilForKey:kMatchSchollModelSLetter fromDictionary:dict];
            self.sLogo = [self objectOrNilForKey:kMatchSchollModelSLogo fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.sName forKey:kMatchSchollModelSName];
    [mutableDict setValue:self.sLetter forKey:kMatchSchollModelSLetter];
    [mutableDict setValue:self.sLogo forKey:kMatchSchollModelSLogo];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}




@end
