//
//  AdimBriefStatusModel.m
//
//  Created by 晨光 宋 on 17/2/18
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "AdimBriefStatusModel.h"


NSString *const kAdimBriefStatusModelRSID = @"RSID";
NSString *const kAdimBriefStatusModelRSname = @"RSname";


@interface AdimBriefStatusModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AdimBriefStatusModel

@synthesize rSID = _rSID;
@synthesize rSname = _rSname;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rSID = [[self objectOrNilForKey:kAdimBriefStatusModelRSID fromDictionary:dict] doubleValue];
            self.rSname = [self objectOrNilForKey:kAdimBriefStatusModelRSname fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rSID] forKey:kAdimBriefStatusModelRSID];
    [mutableDict setValue:self.rSname forKey:kAdimBriefStatusModelRSname];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.rSID = [aDecoder decodeDoubleForKey:kAdimBriefStatusModelRSID];
    self.rSname = [aDecoder decodeObjectForKey:kAdimBriefStatusModelRSname];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_rSID forKey:kAdimBriefStatusModelRSID];
    [aCoder encodeObject:_rSname forKey:kAdimBriefStatusModelRSname];
}

- (id)copyWithZone:(NSZone *)zone {
    AdimBriefStatusModel *copy = [[AdimBriefStatusModel alloc] init];
    
    
    
    if (copy) {

        copy.rSID = self.rSID;
        copy.rSname = [self.rSname copyWithZone:zone];
    }
    
    return copy;
}


@end
