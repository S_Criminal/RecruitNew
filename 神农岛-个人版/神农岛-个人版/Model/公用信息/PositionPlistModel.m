//
//  PositionPlistModel.m
//
//  Created by 晨光 宋 on 17/1/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PositionPlistModel.h"


NSString *const kPositionPlistModelPName = @"P_Name";
NSString *const kPositionPlistModelPSort = @"P_Sort";
NSString *const kPositionPlistModelPType = @"P_Type";
NSString *const kPositionPlistModelPNamep = @"P_Namep";
NSString *const kPositionPlistModelCreateDate = @"CreateDate";
NSString *const kPositionPlistModelID = @"ID";
NSString *const kPositionPlistModelParentID = @"ParentID";


@interface PositionPlistModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PositionPlistModel

@synthesize pName = _pName;
@synthesize pSort = _pSort;
@synthesize pType = _pType;
@synthesize pNamep = _pNamep;
@synthesize createDate = _createDate;
@synthesize iDProperty = _iDProperty;
@synthesize parentID = _parentID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pName = [self objectOrNilForKey:kPositionPlistModelPName fromDictionary:dict];
            self.pSort = [[self objectOrNilForKey:kPositionPlistModelPSort fromDictionary:dict] doubleValue];
            self.pType = [[self objectOrNilForKey:kPositionPlistModelPType fromDictionary:dict] doubleValue];
            self.pNamep = [self objectOrNilForKey:kPositionPlistModelPNamep fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kPositionPlistModelCreateDate fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kPositionPlistModelID fromDictionary:dict] doubleValue];
            self.parentID = [[self objectOrNilForKey:kPositionPlistModelParentID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.pName forKey:kPositionPlistModelPName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pSort] forKey:kPositionPlistModelPSort];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pType] forKey:kPositionPlistModelPType];
    [mutableDict setValue:self.pNamep forKey:kPositionPlistModelPNamep];
    [mutableDict setValue:self.createDate forKey:kPositionPlistModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kPositionPlistModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.parentID] forKey:kPositionPlistModelParentID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.pName = [aDecoder decodeObjectForKey:kPositionPlistModelPName];
    self.pSort = [aDecoder decodeDoubleForKey:kPositionPlistModelPSort];
    self.pType = [aDecoder decodeDoubleForKey:kPositionPlistModelPType];
    self.pNamep = [aDecoder decodeObjectForKey:kPositionPlistModelPNamep];
    self.createDate = [aDecoder decodeObjectForKey:kPositionPlistModelCreateDate];
    self.iDProperty = [aDecoder decodeDoubleForKey:kPositionPlistModelID];
    self.parentID = [aDecoder decodeDoubleForKey:kPositionPlistModelParentID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_pName forKey:kPositionPlistModelPName];
    [aCoder encodeDouble:_pSort forKey:kPositionPlistModelPSort];
    [aCoder encodeDouble:_pType forKey:kPositionPlistModelPType];
    [aCoder encodeObject:_pNamep forKey:kPositionPlistModelPNamep];
    [aCoder encodeObject:_createDate forKey:kPositionPlistModelCreateDate];
    [aCoder encodeDouble:_iDProperty forKey:kPositionPlistModelID];
    [aCoder encodeDouble:_parentID forKey:kPositionPlistModelParentID];
}

- (id)copyWithZone:(NSZone *)zone {
    PositionPlistModel *copy = [[PositionPlistModel alloc] init];
    
    
    
    if (copy) {

        copy.pName = [self.pName copyWithZone:zone];
        copy.pSort = self.pSort;
        copy.pType = self.pType;
        copy.pNamep = [self.pNamep copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.parentID = self.parentID;
    }
    
    return copy;
}


@end
