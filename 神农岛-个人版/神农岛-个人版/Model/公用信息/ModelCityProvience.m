//
//  ModelCityProvience.m
//
//  Created by   on 17/3/23
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ModelCityProvience.h"


NSString *const kModelCityProvienceProvince = @"province";
NSString *const kModelCityProvienceCity = @"city";


@interface ModelCityProvience ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ModelCityProvience

@synthesize province = _province;
@synthesize city = _city;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.province = [self objectOrNilForKey:kModelCityProvienceProvince fromDictionary:dict];
            self.city = [self objectOrNilForKey:kModelCityProvienceCity fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.province forKey:kModelCityProvienceProvince];
    [mutableDict setValue:self.city forKey:kModelCityProvienceCity];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}




@end
