//
//  GeneralImageAndTitleModel.h
//
//  Created by 晨光 宋 on 17/1/22
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GeneralImageAndTitleModel : NSObject 

@property (nonatomic, strong) NSString *imageStr;
@property (nonatomic, strong) NSString *titles;



@end
