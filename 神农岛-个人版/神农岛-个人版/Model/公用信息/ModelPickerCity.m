//
//  ModelPickerCity.m
//
//  Created by   on 17/3/24
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ModelPickerCity.h"


NSString *const kModelPickerCityProvince = @"Province";
NSString *const kModelPickerCityName = @"Name";
NSString *const kModelPickerCityProvinceId = @"ProvinceId";
NSString *const kModelPickerCityAreaCode = @"AreaCode";
NSString *const kModelPickerCityId = @"Id";


@interface ModelPickerCity ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ModelPickerCity

@synthesize province = _province;
@synthesize name = _name;
@synthesize provinceId = _provinceId;
@synthesize areaCode = _areaCode;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.province = [self objectOrNilForKey:kModelPickerCityProvince fromDictionary:dict];
            self.name = [self objectOrNilForKey:kModelPickerCityName fromDictionary:dict];
            self.provinceId = [[self objectOrNilForKey:kModelPickerCityProvinceId fromDictionary:dict] doubleValue];
            self.areaCode = [self objectOrNilForKey:kModelPickerCityAreaCode fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kModelPickerCityId fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.province forKey:kModelPickerCityProvince];
    [mutableDict setValue:self.name forKey:kModelPickerCityName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.provinceId] forKey:kModelPickerCityProvinceId];
    [mutableDict setValue:self.areaCode forKey:kModelPickerCityAreaCode];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kModelPickerCityId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}




@end
