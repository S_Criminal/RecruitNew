//
//  AdimBriefStatusModel.h
//
//  Created by 晨光 宋 on 17/2/18
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AdimBriefStatusModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double rSID;
@property (nonatomic, strong) NSString *rSname;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
