//
//  IndustryModel.m
//
//  Created by 晨光 宋 on 17/1/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "IndustryModel.h"


NSString *const kIndustryModelIName = @"I_Name";
NSString *const kIndustryModelIType = @"I_Type";
NSString *const kIndustryModelISort = @"I_Sort";
NSString *const kIndustryModelINamep = @"I_Namep";
NSString *const kIndustryModelCreateDate = @"CreateDate";
NSString *const kIndustryModelID = @"ID";
NSString *const kIndustryModelParentID = @"ParentID";


@interface IndustryModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IndustryModel

@synthesize iName = _iName;
@synthesize iType = _iType;
@synthesize iSort = _iSort;
@synthesize iNamep = _iNamep;
@synthesize createDate = _createDate;
@synthesize iDProperty = _iDProperty;
@synthesize parentID = _parentID;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.iName = [self objectOrNilForKey:kIndustryModelIName fromDictionary:dict];
            self.iType = [[self objectOrNilForKey:kIndustryModelIType fromDictionary:dict] doubleValue];
            self.iSort = [[self objectOrNilForKey:kIndustryModelISort fromDictionary:dict] doubleValue];
            self.iNamep = [self objectOrNilForKey:kIndustryModelINamep fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kIndustryModelCreateDate fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kIndustryModelID fromDictionary:dict] doubleValue];
            self.parentID = [[self objectOrNilForKey:kIndustryModelParentID fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.iName forKey:kIndustryModelIName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iType] forKey:kIndustryModelIType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iSort] forKey:kIndustryModelISort];
    [mutableDict setValue:self.iNamep forKey:kIndustryModelINamep];
    [mutableDict setValue:self.createDate forKey:kIndustryModelCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kIndustryModelID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.parentID] forKey:kIndustryModelParentID];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.iName = [aDecoder decodeObjectForKey:kIndustryModelIName];
    self.iType = [aDecoder decodeDoubleForKey:kIndustryModelIType];
    self.iSort = [aDecoder decodeDoubleForKey:kIndustryModelISort];
    self.iNamep = [aDecoder decodeObjectForKey:kIndustryModelINamep];
    self.createDate = [aDecoder decodeObjectForKey:kIndustryModelCreateDate];
    self.iDProperty = [aDecoder decodeDoubleForKey:kIndustryModelID];
    self.parentID = [aDecoder decodeDoubleForKey:kIndustryModelParentID];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_iName forKey:kIndustryModelIName];
    [aCoder encodeDouble:_iType forKey:kIndustryModelIType];
    [aCoder encodeDouble:_iSort forKey:kIndustryModelISort];
    [aCoder encodeObject:_iNamep forKey:kIndustryModelINamep];
    [aCoder encodeObject:_createDate forKey:kIndustryModelCreateDate];
    [aCoder encodeDouble:_iDProperty forKey:kIndustryModelID];
    [aCoder encodeDouble:_parentID forKey:kIndustryModelParentID];
}

- (id)copyWithZone:(NSZone *)zone {
    IndustryModel *copy = [[IndustryModel alloc] init];
    
    
    
    if (copy) {

        copy.iName = [self.iName copyWithZone:zone];
        copy.iType = self.iType;
        copy.iSort = self.iSort;
        copy.iNamep = [self.iNamep copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.parentID = self.parentID;
    }
    
    return copy;
}


@end
