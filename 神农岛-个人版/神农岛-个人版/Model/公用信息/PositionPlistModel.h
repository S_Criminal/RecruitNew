//
//  PositionPlistModel.h
//
//  Created by 晨光 宋 on 17/1/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PositionPlistModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *pName;
@property (nonatomic, assign) double pSort;
@property (nonatomic, assign) double pType;
@property (nonatomic, strong) NSString *pNamep;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, assign) double parentID;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
