//
//  ChangeCityModelToArray.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/24.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestApi+Login.h"

@interface ChangeCityModelToArray : NSObject

@property (nonatomic ,strong) NSArray *firstArray;
@property (nonatomic ,strong) NSArray *secondArray;

-(void)changeModelToArray;

@end
