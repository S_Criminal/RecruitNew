//
//  ModelPickerCity.h
//
//  Created by   on 17/3/24
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ModelPickerCity : NSObject

@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double provinceId;
@property (nonatomic, strong) NSString *areaCode;
@property (nonatomic, assign) double internalBaseClassIdentifier;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
