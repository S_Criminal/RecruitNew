//
//  FirstViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "FirstViewController.h"
#import "FirstSearchNavView.h"
#import "PositionCoreData+CoreDataClass.h"
#import "FirstMainManager.h"
#import "NewWorkDetailVC.h"     //职位详情
#import "SearchViewController.h"//搜索界面
#import "LoginViewController.h" //登录界面

/** view || cell */
#import "MainCell.h"
#import "FirstSelectMoreButtonView.h"       //屏蔽View
#import "SliderView.h"          //sliderView section
#import "ExperienceTableView.h" //筛选tableview
#import "FirstSelectCityView.h" //筛选省市view
#import "KYAlertView.h"         //自定义弹框
/** model */
#import "IndustryModel.h"       //行业类别model
#import "PositionPlistModel.h"  //职业类别model
/** tool */
#import "MJRefresh.h"
/** request 请求*/
#import "RequestApi+Login.h"
#import "RequestApi+Main.h" 
/** 自控制器 */
#import "FirstSelectBGViewController.h"

//地理定位
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocation.h>
//prefix
#import "PrefixHeader.pch"

@interface FirstViewController ()<UITableViewDelegate,UITableViewDataSource,SliderViewDelegate,UIScrollViewDelegate,SelectTableDelegate,selectCityDelegate,CLLocationManagerDelegate>

@property (nonatomic ,strong) FirstSearchNavView *searchNav;
@property (nonatomic ,strong) UITableView *firstTableView;
@property (nonatomic ,strong) ExperienceTableView *selectTableView;//筛选tableview
@property (nonatomic ,strong) FirstSelectCityView *selectCityView;  //筛选省市view
@property (nonatomic ,strong) KYAlertView *kyAlertView;
@property (nonatomic ,strong) FirstMainManager *manager;
@property (nonatomic ,strong) NSManagedObjectContext * context;

@property (nonatomic ,strong) NSArray *listArr;
@property (nonatomic ,strong) SliderView *sliderView;
@property (nonatomic ,strong) UIScrollView *scAll;
@property (nonatomic ,strong) FirstSelectMoreButtonView *moreHiddenView;
@property (nonatomic ,strong) UIView *selectBGView;     //筛选器的背景View

@property (nonatomic ,strong) NSMutableArray *industy_Arr;
@property (nonatomic ,strong) NSMutableArray *position_Arr;

@property (nonatomic ,strong) NSArray *listSliderControlArray;

@property (nonatomic ,strong) CLLocationManager *locationManager;

@end

@implementation FirstViewController{
    NSInteger _pageIndex;
    CGFloat bgImageHeight;
    CGFloat tableView_Offset_y;
    BOOL isSelectSliderView;        //是否显示筛选tableview
    /** 筛选条件 */
    NSString *_upLoadProvience;
    NSString *_upLoadIndustry;
    NSString *_upLoadPosition;
    NSString *_positionID;
    NSString *_industryID;
    
    BOOL _selectIndustry;
    BOOL _selectProvience;
    BOOL _selectPosition;
}

#pragma mark 懒加载
-(FirstSearchNavView *)searchNav
{
    if (!_searchNav)
    {
        _searchNav = [[FirstSearchNavView alloc]init];
        [_searchNav setFrame:CGRectMake(0, 0, KWIDTH, NAVIGATIONBAR_HEIGHT)];
        _searchNav.backgroundColor = COLOR_MAINCOLOR;
    }
    return _searchNav;
}

-(UITableView *)firstTableView
{
    if (!_firstTableView)
    {
        _firstTableView = [[UITableView alloc]init];
        [_firstTableView setFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, KWIDTH, KHEIGHT - NAVIGATIONBAR_HEIGHT - TarBarHeight)];
        _firstTableView.delegate    = self;
        _firstTableView.dataSource  = self;
        _firstTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _firstTableView;
}

- (SliderView *)sliderView{
    if (_sliderView == nil) {
        NSString *imageSelect = @"向下箭头实心";
        NSString *imageIsSelect = @"向上箭头";
        _sliderView = [[SliderView alloc]initWithModels:@[[ModelBtn modelWithTitle:@"工作区域" imageName:imageSelect highImageName:imageIsSelect tag:1],[ModelBtn modelWithTitle:@"行业类别" imageName:imageSelect highImageName:imageIsSelect tag:2],[ModelBtn modelWithTitle:@"职业类别" imageName:imageSelect highImageName:imageIsSelect tag:3]] frame:CGRectMake(0, 0, KWIDTH, W(44)) isHasSlider:false isImageLeft:false isFirstOn:false delegate:self];
        UIScrollView *scrollView =  _sliderView.subviews.firstObject;
        self.listSliderControlArray = [NSArray arrayWithArray:scrollView.subviews];
        _sliderView.backgroundColor = [UIColor whiteColor];
    }
    return _sliderView;
}

- (UIScrollView *)scAll{
    if (_scAll == nil) {
        _scAll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.sliderView.bottom +1, KWIDTH, KHEIGHT - self.sliderView.bottom -1)];
        _scAll.contentSize = CGSizeMake(KWIDTH * 2, 0);
        _scAll.backgroundColor = [UIColor clearColor];
        _scAll.delegate = self;
        _scAll.pagingEnabled = true;
        _scAll.showsVerticalScrollIndicator = false;
        _scAll.showsHorizontalScrollIndicator = false;
    }
    return _scAll;
}

-(ExperienceTableView *)selectTableView
{
    if (!_selectTableView) {
        _selectTableView = [[ExperienceTableView alloc]init];
        _selectTableView.backgroundColor = [UIColor clearColor];
        _selectTableView.selectDelegate = self;
    }
    return _selectTableView;
}

-(FirstSelectCityView *)selectCityView
{
    if (!_selectCityView) {
        _selectCityView = [FirstSelectCityView new];
        _selectCityView.delegate = self;
    }
    return _selectCityView;
}

-(KYAlertView *)kyAlertView
{
    if (!_kyAlertView) {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    return _kyAlertView;
}

#pragma mark view load
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    _manager = [FirstMainManager share];
    UIApplication *app = [UIApplication sharedApplication];
    id delegate = app.delegate;
    self.context = [delegate managedObjectContext];
    
    _pageIndex = 1;
    [GlobalData sharedInstance].isLogin = @"YES";
    [self searchConditionIsNull];
    
    [self setUI];
    [self createBasicView];
    [self setUpPlistModel];
    [self setModelIndex:_pageIndex];
    [self setFefresh];
    
}

- (void)postNowAddress
{
    // 判断是否开启定位
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager startUpdatingLocation];
    } else {
        [self alertViewWithMessage:@"定位失败" content:@"请检测是否开启定位设置"];
    }
    if ([[[UIDevice currentDevice]systemVersion] doubleValue] > 8.0)
    {
        // 设置定位权限仅iOS8以上有意义,而且iOS8以上必须添加此行代码
        [self.locationManager requestWhenInUseAuthorization];//前台定位
        //         [self.locationManager requestAlwaysAuthorization];//前后台同时定位
    }
}


#pragma mark 地图定位的代理方法
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject]; // 最后一个值为最新位置
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    __block NSString *longitude;
    __block NSString *latitude;
    __block int i = 0;
    // 根据经纬度反向得出位置城市信息
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count > 0) {
            CLPlacemark *placeMark = placemarks[0];
            NSLog(@"%@",placeMark.locality);
            longitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
            latitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
            if (i==0)
            {
                [RequestApi postLocationsWithKey:[GlobalData sharedInstance].GB_Key longitude:longitude latitude:latitude version:[GlobalMethod getVersion] Delegate:nil success:^(NSDictionary *response) {
                    [GlobalData sharedInstance].isLogin = nil; //请求成功之后置为nil
                } failure:^(NSString *string) {
                    
                }];
            }
            i++;
            //nslog(@"%ld",i);
        } else if (error == nil && placemarks.count == 0) {
            
        } else if (error) {
            //        //nslog(@Location error: %@, error);
        }
    }];
    [manager stopUpdatingLocation];
}


-(void)alertViewWithMessage:(NSString *)message content:(NSString *)content
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:content style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //点击按钮的响应事件；

    }]];
    [self presentViewController:alert animated:true completion:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!kStringIsEmpty([GlobalData sharedInstance].isLogin))//当登录的时候调用一次
    {
        [self postNowAddress];//发送当前地址
    }
    
    if (isSelectSliderView)
    {
        isSelectSliderView = NO;
        self.sliderView.height = !isSelectSliderView ? W(44) : KHEIGHT - NAVIGATION_BarHeight - TarBarHeight;
        [self searchConditionIsNull];
        [self.firstTableView reloadData];
    }
    [self removeSelectTableView];
    [self setFirstModel];
}

-(void)setFirstModel
{
    [RequestApi getUserInfoWithKey:[GlobalData sharedInstance].GB_Key uid:[GlobalData sharedInstance].GB_UID Delegate:nil success:^(NSDictionary *response) {
        if ([response[@"code"]isEqualToNumber:@200]) {
            NSDictionary *d = response[@"datas"];
            NSString *comID = d[@"U_ComID"];
            NSString *resID = d[@"U_ResID"];
            NSLog(@"comID====%@,resID====%@",comID,resID);
            if ([comID integerValue] != 0 )//需要下载企业版
            {
                if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"laodaozhaopincompany://"]]) {
                    if (resID == 0) {
                        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"laodaozhaopincompany://"]];
                    }
                }else{
                    [self showAlertChangeNewVersion];
                }
            }
        }
    } failure:^(NSString *str) {
        
    }];
}

-(void)createBasicView
{
    DSWeak;
    /** 跳转到搜索界面 */
    self.searchNav.tapBlock = ^{
        [weakSelf nextToSearchController];
    };
    /** headerView  */
    UIView *headBGView     = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, bgImageHeight)];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = headBGView.frame;
    imageView.image = [UIImage imageNamed:@"first_BGimage"];
    [headBGView addSubview:imageView];
    self.firstTableView.tableHeaderView = headBGView;
    [self.view addSubview:self.searchNav];
    [self.view addSubview:self.firstTableView];
    
    [self.firstTableView registerNib:[UINib nibWithNibName:@"MainCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
    
}

-(void)setUI
{
    bgImageHeight = W(150);
}

#pragma mark 刷新 加载更多
-(void)setFefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
//    [self.firstTableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
//    [header beginRefreshing];
    
    // 设置header
    self.firstTableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
//    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
//    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.firstTableView.mj_footer = footer;
    [self.firstTableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    if (_selectTableView) {
        [self removeSelectTableView];
    }
    _pageIndex = 1;
    [self searchConditionIsNull];

    [self setModelIndex:_pageIndex];
    [self.firstTableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelIndex:_pageIndex];
    [self.firstTableView.mj_footer endRefreshing];
}
#pragma mark 设置公用信息  （行业类别 & 职业类别）

-(void)setUpPlistModel
{
    _industy_Arr = [NSMutableArray array];
    _position_Arr= [NSMutableArray array];
    [RequestApi getindustrylListDelegate:nil Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr) {
            if ([dic[@"ParentID"] isEqualToNumber:@0])
            {
                IndustryModel *model = [IndustryModel modelObjectWithDictionary:dic];
                [_industy_Arr addObject:model];
            }
        }
    } failure:^(NSString *str) {
        
    }];
    
    [RequestApi getPositionTypeLsWithDelegate:nil Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr) {
            if ([dic[@"ParentID"] isEqualToNumber:@0])
            {
                PositionPlistModel *model = [PositionPlistModel modelObjectWithDictionary:dic];
                [_position_Arr addObject:model];
            }
        }
    } failure:^(NSString *str) {
        
    }];
}

-(void)setModelIndex:(NSInteger)pageIndex
{
    [self chargeExitsSearchCondition];
    DSWeak;
    [_manager getPositionWithkey:[GlobalData sharedInstance].GB_Key Top:@"20" cid:@"" position:@"" positionID:_positionID industryID:_industryID province:_upLoadProvience city:@"" success:^(NSDictionary *dic) {
        
    } PageIndex:_pageIndex status:@"firstPosition"  BlockHandelDoJobSuccess:^(NSString *str) {
        if ([str isEqualToString:@"成功"]){
            [self setCoreData];
        }else if ([str isEqualToString:@"100"]){
            [MBProgressHUD showError:@"网络不好，请检查网络!" toView:self.view];
            [self setCoreData];
        }else if ([str isEqualToString:@"10000"]){
            [MBProgressHUD showError:@"没有更多数据" toView:self.view];
            [weakSelf.firstTableView reloadData];
        }else if([str isEqualToString:@"没有数据"]){
            [MBProgressHUD showError:@"没有更多数据" toView:self.view];
            [weakSelf.firstTableView reloadData];
        }
    }];
}


-(void)setCoreData
{
    NSFetchRequest *request ;
    NSError *error;
    request = [NSFetchRequest fetchRequestWithEntityName:@"PositionCoreData"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"status == %@",@"firstPosition"];
    request.predicate = predicate;
    
    NSArray *resultAry = [self.context executeFetchRequest:request error:&error];
    
    if (resultAry.count == 0) {
        
    }else{
        _listArr = [NSMutableArray arrayWithArray:resultAry];
    }
    NSLog(@"_listArr_listArr%@",_listArr);
    
    [self.firstTableView reloadData];
    
}

#pragma mark TableView的代理方法

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGFloat height = !isSelectSliderView ? W(55) : KHEIGHT - NAVIGATION_BarHeight - TarBarHeight;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, height)];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, W(44), KWIDTH, 10)];
    
    line.tag = 1500;
    
    line.backgroundColor = COLOR_BGCOLOR;
    
    [view addSubview:self.sliderView];
 
    [view addSubview:line];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return !isSelectSliderView ? W(55) : KHEIGHT - NAVIGATION_BarHeight - TarBarHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    //CGFloat h = [cell resetCellModel];
    CGFloat h = W(135);
    if (KWIDTH == 320) {
        return 120;
    }else if (KWIDTH == 375) {
        return 135;
    }else if (KWIDTH == 414) {
        return 145;
    }
    return h;
}

-(MainCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    if (!cell)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    }
    
    if (_listArr.count > 0)
    {
        PositionCoreData *positioModel = _listArr[indexPath.row];
        cell.model = positioModel;
        cell.selectMoreButton.tag = indexPath.row;
        [cell.selectMoreButton addTarget:self action:@selector(tapSelectMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (kStringIsEmpty([GlobalData sharedInstance].GB_Key))
    {
        [self showKyalertView];
        return;
    }
    PositionCoreData *positioModel = _listArr[indexPath.row];
    _manager.workDetail_pid = positioModel.c_Pid;
    NewWorkDetailVC *workDetail = [[NewWorkDetailVC alloc]initWithCid:positioModel.c_Cid pid:positioModel.c_Pid];
    [self.navigationController pushViewController:workDetail animated:YES];
}

-(void)showKyalertView
{
    [self.kyAlertView showAlertView:@"提示" message:@"您还未登录，无法进行此操作！" subBottonTitle:@"取消" cancelButtonTitle:@"登录" handler:^(AlertViewClickBottonType bottonType) {
        if (bottonType == AlertViewClickBottonTypeCancelButton)
        {
            LoginViewController *login = [LoginViewController new];
            [self.navigationController pushViewController:login animated:YES];
        }
    }];
}

#pragma mark 点击屏蔽
-(void)tapSelectMoreButton:(UIButton *)sender
{
    DSWeak;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    MainCell *cell = [self.firstTableView cellForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [self.firstTableView convertRect:cell.frame toView:self.view];
    
    
    PositionCoreData *positioModel = _listArr[indexPath.row];
    
    
    if (!_moreHiddenView)
    {
        _moreHiddenView = [FirstSelectMoreButtonView initWithModel:nil];
        _moreHiddenView.tapRemoveBlock = ^{
            [weakSelf tapRemoveCompany:positioModel.c_Cid PositionCoreData:positioModel];
        };
        [_moreHiddenView setFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:1011];
        CGFloat top = rectInSuperview.origin.y + button.bottom;
        [_moreHiddenView resetViewWithBGViewFrame:top];
        UITapGestureRecognizer *tapHidden = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHiddenMoreView)];
        [_moreHiddenView.currWindow addGestureRecognizer:tapHidden];
    }
    [self.tabBarController.view addSubview:_moreHiddenView];
}

-(void)tapHiddenMoreView
{
//    self.tabBarController.tabBar.hidden = NO;
    [_moreHiddenView removeFromSuperview];
    _moreHiddenView = nil;
}

#pragma mark 确认屏蔽
-(void)tapRemoveCompany:(NSString *)cid PositionCoreData:(PositionCoreData *)model
{
    DSWeak;
    
    NSMutableArray *listArrItems = [NSMutableArray arrayWithArray:self.listArr];
    
    [RequestApi editscompanyWithKey:[GlobalData sharedInstance].GB_Key editID:@"0" cid:model.c_Cid cname:model.c_Name Delegate:nil success:^(NSDictionary *response) {
        [listArrItems removeObject:model];
        [weakSelf.context deleteObject:model];
        weakSelf.listArr = listArrItems;
        [weakSelf setCoreData];
        [weakSelf tapHiddenMoreView];
        [MBProgressHUD showSuccess:@"屏蔽成功" toView:weakSelf.view];
    } failure:^(NSString *str) {
        [weakSelf tapHiddenMoreView];
        [MBProgressHUD showError:str toView:weakSelf.view];
    }];
}


#pragma mark slider delegate
#pragma mark 点击筛选按钮

/** 
 * @brief  isSelectSliderView   用来判断sliderView的高度  NO: W(44)    YES: tableview的高度
 */

- (void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(CustomSliderControl *)control
{
    if (kArrayIsEmpty(self.industy_Arr)) {
        NSLog(@"%@",self.industy_Arr);
        [self setUpPlistModel];
    }
    NSLog(@"%@",self.listSliderControlArray);
    
    NSLog(@"%@",control);
    NSLog(@"%d",control.selected);
    _selectProvience = tag == 0 ? !_selectProvience : NO;
    _selectIndustry  = tag == 1 ? !_selectIndustry : NO;
    _selectPosition  = tag == 2 ? !_selectPosition : NO;
    
    [self breakToTableHeaderTop];
    NSLog(@"%@",self.sliderView);
    NSLog(@"%@",self.sliderView.subviews);
    
    control.selected = !control.selected;
    NSArray *arr;
    if (tag == 0) {
        [self chargeExitsSearchCondition];
        //点击筛选城市时  判断先前的行业类别 职业类别tableview是否正在显示
        self.selectTableView.hidden = YES;
        self.selectCityView.hidden  = NO;
//        _selectProvience = !(_selectIndustry && _selectPosition);
        [self.selectCityView setFrame:CGRectMake(0, 10 + W(44), KWIDTH, KHEIGHT - W(44) - 10 - NAVIGATION_BarHeight - TarBarHeight)];
        isSelectSliderView = _selectProvience;
        isSelectSliderView ? [self.sliderView addSubview:self.selectCityView] : nil;
    }else if (tag == 1){
        arr = _industy_Arr;
        _selectIndustry = _selectProvience ? NO : _selectIndustry;
        isSelectSliderView = _selectIndustry;
        [self.selectTableView setUpTableArray:_industy_Arr count:_industy_Arr.count tag:tag industryTitle:_upLoadIndustry positionTitle:_upLoadPosition];
        [self didSelectTableView];
    }else if (tag == 2){
        arr = _position_Arr;
        _selectPosition = _selectProvience ? NO : _selectPosition;
        isSelectSliderView = _selectPosition;
        [self.selectTableView setUpTableArray:_position_Arr count:_position_Arr.count tag:tag industryTitle:_upLoadIndustry positionTitle:_upLoadPosition];
        [self didSelectTableView];
    }
    
    if (!isSelectSliderView) {
        [self removeSelectTableView];
    }
    
    self.sliderView.height = !isSelectSliderView ? W(44) : KHEIGHT - NAVIGATION_BarHeight - TarBarHeight;
    [self.firstTableView reloadData];
}

#pragma mark 点击筛选 职业 or  行业
-(void)didSelectTableView
{
    [self chargeExitsSearchCondition];
    self.selectCityView.hidden = YES;
    self.selectTableView.hidden = NO;
    [self.selectTableView setFrame:CGRectMake(0, 10 + W(44), KWIDTH, KHEIGHT - W(44) - 10 - NAVIGATION_BarHeight - TarBarHeight)];
    isSelectSliderView ? [self.sliderView addSubview:self.selectTableView] : nil;
}

#pragma mark 确认筛选之后
-(void)protocolSelectExperienceTableView:(NSString *)title selectTag:(NSInteger)tag status:(NSString *)status isFirstOn:(BOOL)isFirstOn
{
    NSLog(@"%@===%ld ==== status%@",title,tag,status);
    _industryID = isFirstOn ?   status : _industryID;
    _positionID = isFirstOn ?  _positionID : status ;
    
    _upLoadIndustry = isFirstOn ? title : _upLoadIndustry;
    _upLoadPosition = isFirstOn ?_upLoadPosition :title ;
    
    [self didSelectView];
}

-(void)selectCityDelegateWithTitle:(NSString *)title
{
    _upLoadProvience = title;
    [self didSelectView];
}

#pragma mark 筛选数据刷新
-(void)didSelectView
{
    _pageIndex = 1;
    isSelectSliderView = NO;
    self.sliderView.height = !isSelectSliderView ? W(44) : KHEIGHT - NAVIGATION_BarHeight - TarBarHeight;
    NSLog(@"%f",self.sliderView.height);
    
    [self removeSelectTableView];
    [self setModelIndex:_pageIndex];
}

#pragma mark 查询是否有筛选条件
-(void)chargeExitsSearchCondition
{
    for (CustomSliderControl *control in self.listSliderControlArray) {
        NSLog(@"%@",control);
        control.labelTitle.textAlignment = NSTextAlignmentCenter;
        switch (control.tag) {
            case 0:
                control.labelTitle.text = kStringIsEmpty(_upLoadProvience) ? @"工作区域" : _upLoadProvience;
                control.labelTitle.textColor = kStringIsEmpty(_upLoadProvience) ? [UIColor blackColor] : COLOR_MAINCOLOR;
                control.iv.image = kStringIsEmpty(_upLoadProvience) ? [UIImage imageNamed:@"向下箭头实心"] : [UIImage imageNamed:@"向上箭头"];
                break;
            case 1:
                control.labelTitle.text = kStringIsEmpty(_upLoadIndustry) ? @"行业类别" : _upLoadIndustry;
                control.labelTitle.textColor = kStringIsEmpty(_upLoadIndustry) ? [UIColor blackColor] : COLOR_MAINCOLOR;
                control.iv.image = kStringIsEmpty(_upLoadIndustry) ? [UIImage imageNamed:@"向下箭头实心"] : [UIImage imageNamed:@"向上箭头"];
                break;
            case 2:
                control.labelTitle.text = kStringIsEmpty(_upLoadPosition) ? @"职业类别" : _upLoadPosition;
                control.labelTitle.textColor = kStringIsEmpty(_upLoadPosition) ? [UIColor blackColor] : COLOR_MAINCOLOR;
                control.iv.image = kStringIsEmpty(_upLoadPosition) ? [UIImage imageNamed:@"向下箭头实心"] : [UIImage imageNamed:@"向上箭头"];
                break;
            default:
                break;
        }
    }
}

#pragma mark 筛选条件置为空

-(void)searchConditionIsNull
{
    _positionID         = @"";
    _industryID         = @"";
    _upLoadProvience    = @"";
    _upLoadIndustry     = @"";
    _upLoadPosition     = @"";
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",scrollView.contentOffset.y);
    if (isSelectSliderView)
    {
        return;
    }
    
    UIView *view = [self.sliderView.superview viewWithTag:1500];
    if (scrollView.contentOffset.y > bgImageHeight)
    {
        NSLog(@"%@",view);
        view.hidden = YES;
    }else{
        view.hidden = NO;
    }
    tableView_Offset_y = scrollView.contentOffset.y;
    
}



-(void)nextToSearchController
{
    SearchViewController *searchVC = [[SearchViewController alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark tableView 返回第一行
-(void)breakToTableHeaderTop
{
    if (self.listArr.count > 0)
    {
        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.firstTableView selectRowAtIndexPath:scrollIndexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
}

-(void)removeSelectTableView
{
    [GlobalMethod removeView:self.selectTableView];
    [GlobalMethod removeView:self.selectCityView];
}

#pragma mark 提示更新

-(void)showAlertChangeNewVersion
{
    static BOOL isShowed = false;
    //前往下载企业版
    if (isShowed) {
        return;
    }
    [RequestApi getFirstAlertInfoWithPKey:@"download_Copmany" delegate:nil success:^(NSDictionary *response) {
        if ([response[@"code"]isEqualToNumber:@200])
        {
            NSArray *arr = response[@"datas"];
            if (arr.count > 0) {
                
                NSDictionary *contentDic = [response[@"datas"] objectAtIndex:0];
                NSString *title   = contentDic[@"Title"];
                NSString *content = contentDic[@"Explain"];
                if (isStr(title))
                {
                    [self.kyAlertView showAlertView:@"提示：" message:isStr(content)?content:@""  subBottonTitle:@"前往"  cancelButtonTitle:@"取消" handler:^(AlertViewClickBottonType buttonType) {
                        if (buttonType == AlertViewClickBottonTypeSubBotton) {
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:title]];
                        }else{
                            isShowed = true;
                        }
                    }];
                }else{
                    
                }
            }
        }
    } failure:^(NSString *str) {
        
    }];

}


@end
