//
//  FirstSelectBGViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "FirstSelectBGViewController.h"
#import "FirstSelectTableViewCell.h"

@interface FirstSelectBGViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listArr;

@end

@implementation FirstSelectBGViewController

-(UITableView *)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = [UIColor grayColor];
    [self.tableView registerClass:[FirstSelectTableViewCell class] forCellReuseIdentifier:@"FirstSelectTableViewCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listArr.count;
}

-(void)setUpTableArray:(NSArray *)arr count:(NSInteger)count
{
    _listArr = arr;
    [self.tableView reloadData];
}

-(FirstSelectTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FirstSelectTableViewCell *cell = [FirstSelectTableViewCell selectButton:@""];
    cell.line.hidden = YES;
    cell.title = @"职位类别";
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
