//
//  NewFirstViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "NewFirstViewController.h"
#import "FirstSearchNavView.h"
#import "PositionCoreData+CoreDataClass.h"
#import "FirstMainManager.h"
#import "NewWorkDetailVC.h"     //职位详情
#import "SearchViewController.h"//搜索界面

#import "LoginViewController.h" //登录界面

/** view || cell */
#import "MainCell.h"
#import "FirstSelectMoreButtonView.h"       //屏蔽View
#import "SliderView.h"          //sliderView section
#import "ExperienceTableView.h" //筛选tableview
/** model */
#import "IndustryModel.h"       //行业类别model
#import "PositionPlistModel.h"  //职业类别model
/** tool */
#import "MJRefresh.h"
/** request 请求*/
#import "RequestApi+Login.h"
/** 子控制器 */
#import "FirstSelectBGViewController.h"
@interface NewFirstViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,SliderViewDelegate,UIScrollViewDelegate>
@property (nonatomic ,strong) FirstSearchNavView *searchNav;
@property (nonatomic ,strong) UITableView *firstTableView;
@property (nonatomic ,strong) ExperienceTableView *selectTableView;//筛选tableview
@property (nonatomic ,strong) FirstMainManager *manager;
@property (nonatomic ,strong) NSManagedObjectContext * context;

@property (nonatomic ,strong) NSArray *listArr;
@property (nonatomic ,strong) SliderView *sliderView;
@property (nonatomic ,strong) UIScrollView *hcAll;      //横向scrollview
@property (nonatomic ,strong) UIScrollView *vcAll;      //竖向scrollview
@property (nonatomic ,strong) FirstSelectMoreButtonView *moreHiddenView;
@property (nonatomic ,strong) UIView *selectBGView;


@property (nonatomic ,strong) NSMutableArray *industy_Arr;
@property (nonatomic ,strong) NSMutableArray *position_Arr;

@end

@implementation NewFirstViewController{
    NSInteger _pageIndex;
}

#pragma mark 懒加载
- (SliderView *)sliderView{
    if (_sliderView == nil) {
        NSString *imageSelect = @"向下箭头实心";
        NSString *imageIsSelect = @"向上箭头";
        _sliderView = [[SliderView alloc]initWithModels:@[[ModelBtn modelWithTitle:@"行业类别" imageName:imageSelect highImageName:imageIsSelect tag:1],[ModelBtn modelWithTitle:@"职业类别" imageName:imageSelect highImageName:imageIsSelect tag:2],[ModelBtn modelWithTitle:@"工作区域" imageName:imageSelect highImageName:imageIsSelect tag:3]] frame:CGRectMake(0, self.searchNav.bottom, KWIDTH, W(44)) isHasSlider:false isImageLeft:false isFirstOn:false delegate:self];
        _sliderView.backgroundColor = [UIColor whiteColor];
    }
    return _sliderView;
}

- (UIScrollView *)hcAll{
    if (_hcAll == nil) {
        _hcAll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.sliderView.bottom, KWIDTH, KHEIGHT - self.sliderView.bottom - TarBarHeight)];
        _hcAll.contentSize = CGSizeMake(KWIDTH * 2, 0);
        _hcAll.backgroundColor = [UIColor clearColor];
        _hcAll.delegate = self;
        _hcAll.pagingEnabled = true;
        _hcAll.showsVerticalScrollIndicator = false;
        _hcAll.showsHorizontalScrollIndicator = false;
    }
    return _hcAll;
}

-(UIScrollView *)vcAll
{
    if (_vcAll == nil) {
        _vcAll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, KHEIGHT -NAVIGATION_BarHeight -TarBarHeight)];
        _vcAll.contentSize = CGSizeMake(100, KHEIGHT -NAVIGATION_BarHeight -TarBarHeight);
        _vcAll.backgroundColor = [UIColor redColor];
        _vcAll.delegate = self;
        _vcAll.pagingEnabled = true;
        _vcAll.showsVerticalScrollIndicator = false;
        _vcAll.showsHorizontalScrollIndicator = false;
    }
    return _vcAll;
}

-(ExperienceTableView *)selectTableView
{
    if (!_selectTableView) {
        _selectTableView = [[ExperienceTableView alloc]init];
        _selectTableView.backgroundColor = [UIColor redColor];
        //        _selectTableView.delegate = self;
        //        _selectTableView.dataSource = self;
    }
    return _selectTableView;
}



#pragma mark 懒加载
-(FirstSearchNavView *)searchNav
{
    if (!_searchNav)
    {
        _searchNav = [[FirstSearchNavView alloc]init];
        [_searchNav setFrame:CGRectMake(0, 0, KWIDTH, NAVIGATIONBAR_HEIGHT)];
        _searchNav.backgroundColor = COLOR_MAINCOLOR;
    }
    return _searchNav;
}

-(UITableView *)firstTableView
{
    if (!_firstTableView)
    {
        _firstTableView = [[UITableView alloc]init];
        
        _firstTableView.delegate    = self;
        _firstTableView.dataSource  = self;
        _firstTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_firstTableView registerNib:[UINib nibWithNibName:@"MainCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
    }
    return _firstTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    _manager = [FirstMainManager share];
//    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    UIApplication *app = [UIApplication sharedApplication];
    id delegate = app.delegate;
    self.context = [delegate managedObjectContext];
    self.navigationController.navigationBarHidden = YES;
    
    _pageIndex = 1;
    
    [self createBasicView];
    [self setUpPlistModel];
    [self setModelIndex:_pageIndex];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.childViewControllers.count == 1) {
        return NO;
    }else{
        
    }
    return YES;
}

-(void)createBasicView
{
    
    [self.view addSubview:self.searchNav];
//    [self.view addSubview:self.hcAll];
    [self.view addSubview:self.vcAll];
    
//    DSWeak;
    /** 跳转到搜索界面 */
    self.searchNav.tapBlock = ^{
        
    };
    /** headerView  */
    UIView *headBGView     = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(150))];
    UIView *line           = [[UIView alloc]initWithFrame:CGRectMake(0, self.sliderView.bottom, KWIDTH, 10)];
    line.tag = 10000;
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = headBGView.frame;
    imageView.image = [UIImage imageNamed:@"first_BGimage"];
    [headBGView addSubview:imageView];
    
    self.sliderView.y = 0;
    [self.firstTableView setFrame:CGRectMake(0, line.bottom, KWIDTH, _vcAll.height - line.bottom)];
    _vcAll.contentSize =CGSizeMake(0,KHEIGHT - NAVIGATION_BarHeight - TarBarHeight);
    
    [self.vcAll addSubview:self.sliderView];
    [self.vcAll addSubview:headBGView];
    [self.vcAll addSubview:line];
    [self.vcAll addSubview:_firstTableView];
}



-(void)setCoreData
{
    NSFetchRequest *request ;
    NSError *error;
    request = [NSFetchRequest fetchRequestWithEntityName:@"PositionCoreData"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"status == %@",@"firstPosition"];
    request.predicate = predicate;
    
    NSArray *resultAry = [self.context executeFetchRequest:request error:&error];
    
    if (resultAry.count == 0) {
        
    }else{
        _listArr = [NSMutableArray arrayWithArray:resultAry];
    }
    NSLog(@"_listArr_listArr%@",_listArr);
    
    [self.firstTableView reloadData];
}



#pragma mark 设置公用信息

-(void)setUpPlistModel
{
    _industy_Arr = [NSMutableArray array];
    _position_Arr= [NSMutableArray array];
    [RequestApi getindustrylListDelegate:self Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr) {
            if ([dic[@"ParentID"] isEqualToNumber:@0])
            {
                IndustryModel *model = [IndustryModel modelObjectWithDictionary:dic];
                [_industy_Arr addObject:model];
            }
        }
    } failure:^(NSString *str) {
        
    }];
    
    [RequestApi getPositionTypeLsWithDelegate:self Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr) {
            if ([dic[@"ParentID"] isEqualToNumber:@0])
            {
                PositionPlistModel *model = [PositionPlistModel modelObjectWithDictionary:dic];
                [_position_Arr addObject:model];
            }
        }
    } failure:^(NSString *str) {
        
    }];
}


-(void)setModelIndex:(NSInteger)pageIndex
{
    [_manager getPositionWithkey:[GlobalData sharedInstance].GB_Key Top:@"20" cid:@"" position:@"" positionID:@"" industryID:@"" province:@"" city:@"" success:^(NSDictionary *dic) {
        
    } PageIndex:_pageIndex status:@"firstPosition" BlockHandelDoJobSuccess:^(NSString *str) {
        if ([str isEqualToString:@"成功"])
        {
            [self setCoreData];
        }else if ([str isEqualToString:@"100"]){
            [MBProgressHUD showError:@"网络不好，请检查网络!" toView:self.view];
            [self setCoreData];
        }
    }];
}


#pragma mark TableView的代理方法

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(55))];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, self.sliderView.bottom, KWIDTH, 10)];
    line.backgroundColor = COLOR_LINESCOLOR;
    
    [view addSubview:self.sliderView];
    [view addSubview:line];
    
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return W(55);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [tableView cellHeightForIndexPath:indexPath model:_listArr[indexPath.row] keyPath:@"model" cellClass:[MainCell class] contentViewWidth:KWIDTH];
    return h;
}

-(MainCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    if (!cell)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    }
    
    if (_listArr.count > 0)
    {
        PositionCoreData *positioModel = _listArr[indexPath.row];
        cell.model = positioModel;
        cell.selectMoreButton.tag = indexPath.row;
        [cell.selectMoreButton addTarget:self action:@selector(tapSelectMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (kStringIsEmpty([GlobalData sharedInstance].GB_Key)) {
        LoginViewController *login = [LoginViewController new];
        [self.navigationController pushViewController:login animated:YES];
        return;
    }
    
    PositionCoreData *positioModel = _listArr[indexPath.row];
    _manager.workDetail_pid = positioModel.c_Pid;
    NewWorkDetailVC *workDetail = [[NewWorkDetailVC alloc]init];
    workDetail.cid = positioModel.c_Cid;
    workDetail.pid = positioModel.c_Pid;
    [self.navigationController pushViewController:workDetail animated:YES];
    
}

#pragma mark 点击屏蔽按钮
-(void)tapSelectMoreButton:(UIButton *)sender
{
    DSWeak;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    MainCell *cell = [self.firstTableView cellForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [self.firstTableView convertRect:cell.frame toView:self.view];
    
    
    PositionCoreData *positioModel = _listArr[indexPath.row];
    
    
    if (!_moreHiddenView)
    {
        _moreHiddenView = [FirstSelectMoreButtonView initWithModel:nil];
        _moreHiddenView.tapRemoveBlock = ^{
            [weakSelf tapRemoveCompany:positioModel.c_Cid PositionCoreData:positioModel];
        };
        [_moreHiddenView setFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:1011];
        CGFloat top = rectInSuperview.origin.y + button.bottom;
        [_moreHiddenView resetViewWithBGViewFrame:top];
        UITapGestureRecognizer *tapHidden = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHiddenMoreView)];
        [_moreHiddenView.currWindow addGestureRecognizer:tapHidden];
        //        self.tabBarController.tabBar.hidden = YES;
    }
    [self.tabBarController.view addSubview:_moreHiddenView];
}
#pragma mark 屏蔽 删除
-(void)tapRemoveCompany:(NSString *)cid PositionCoreData:(PositionCoreData *)model
{
    NSMutableArray *listArrItems = [NSMutableArray arrayWithArray:self.listArr];
    [listArrItems removeObject:model];
    [self.context deleteObject:model];
    self.listArr = listArrItems;
    
    //    [self setModelIndex:_pageIndex];
    [self setCoreData];
    [self tapHiddenMoreView];
}

-(void)tapHiddenMoreView
{
    //    self.tabBarController.tabBar.hidden = NO;
    [_moreHiddenView removeFromSuperview];
    _moreHiddenView = nil;
}


#pragma mark slider delegate
- (void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(CustomSliderControl *)control
{
    NSArray *arr;
    //if (tag == 0) {
    //    arr = _industy_Arr;
   //     [self.selectTableView setUpTableArray:_industy_Arr count:_industy_Arr.count tag:tag industryTitle:_upLoadIndustry positionTitle:_upLoadPosition];
    //}else if (tag == 1){
    //    arr = _position_Arr;
    //    [self.selectTableView setUpTableArray:_industy_Arr count:_industy_Arr.count tag:tag industryTitle:_upLoadIndustry //positionTitle:_upLoadPosition];
    //    [self.selectTableView setUpTableArray:_position_Arr count:_position_Arr.count tag:tag];
   // }
   // [self setupChildVC:arr];   //创建筛选控件
    //    [self.selectTableView setFrame:CGRectMake(0, _sliderView.bottom + 10, KWIDTH, arr.count * 44)];
    //    [self.sliderView addSubview:self.selectTableView];
}

#pragma mark 初始化自控制器
- (void)setupChildVC:(NSArray *)arr
{
    FirstSelectBGViewController *selectVC = [[FirstSelectBGViewController alloc] init];
    
    selectVC.view.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - _sliderView.bottom - TarBarHeight);
    [selectVC setUpTableArray:arr count:0];
    
    [self addChildViewController:selectVC];
    
    UIView *line = (UIView *)[self.vcAll viewWithTag:10000];
    self.hcAll.y = line.bottom + TarBarHeight;
    [self.hcAll addSubview:selectVC.view];
    [self.vcAll addSubview:self.hcAll];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
