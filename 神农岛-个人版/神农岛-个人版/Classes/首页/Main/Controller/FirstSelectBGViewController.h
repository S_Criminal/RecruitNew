//
//  FirstSelectBGViewController.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstSelectBGViewController : UIViewController

-(void)setUpTableArray:(NSArray *)arr count:(NSInteger)count;

@end
