//
//  DrawRectTriangleView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "DrawRectTriangleView.h"

@implementation DrawRectTriangleView

- (void) drawRectWithStatus:(NSInteger)status rect:(CGRect)rect
{
    _i = status;
    [self drawRect:CGRectZero];
}

-(void)drawRect:(CGRect)rect
{
    switch (_i) {
        case 0:
            [self drawRectTriangle:rect];
            break;
        case 1:
            [self drawRectCircle:rect];
            break;
        default:
            break;
    }
}

-(void)drawRectTriangle:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPoint sPoints[3];//坐标点
    
    sPoints[0] =CGPointMake(rect.size.width / 2.0f, rect.origin.y);//坐标1
    
    sPoints[1] =CGPointMake(rect.origin.x, rect.size.height);//坐标2
    
    sPoints[2] =CGPointMake(rect.size.width, rect.size.height);//坐标3
    
    CGContextSetRGBStrokeColor(context, 0, 0, 0, 0);    //画笔线的颜色
    
    CGContextAddLines(context, sPoints, 3);//添加线
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    
    CGContextClosePath(context);//封起来
    
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
}

-(void)drawRectCircle:(CGRect)rect
{
    CGFloat PI = self.width / 2;
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor*aColor = [UIColor whiteColor];
    CGContextSetFillColorWithColor(context, aColor.CGColor);//填充颜色
    CGContextSetLineWidth(context, 3.0);//线的宽度
    CGContextAddArc(context, 250, 40, 40, 0, 2*PI, 0); //添加一个圆
    //kCGPathFill填充非零绕数规则,kCGPathEOFill表示用奇偶规则,kCGPathStroke路径,kCGPathFillStroke路径填充,kCGPathEOFillStroke表示描线，不是填充
    CGContextDrawPath(context, kCGPathFillStroke); //绘制路径加填充
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
