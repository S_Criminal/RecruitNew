//
//  mainCell.m
//  神农岛
//
//  Created by 宋晨光 on 16/3/30.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "MainCell.h"
#import "NSNumber+ToString.h"
//#import "all.pch"
//#import "PositionCoreData+CoreDataClass.h"
#import "PYPhotoBrowser.h"
//#import "UIImageView+WebCache.h"
#define META_CORNER 10
#define METACOLOR [HexStringColor colorWithHexString:@"#e2e2e2"]
#define MainLabelColor [HexStringColor colorWithHexString:@"#666666"]

@interface MainCell ()
//约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spacingTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spacingBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionNameConstraintWidth;
//分割线高度约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linesHeightConstraint;



//标签
@property (weak, nonatomic) IBOutlet UILabel *meta1;
@property (weak, nonatomic) IBOutlet UILabel *meta2;
@property (weak, nonatomic) IBOutlet UILabel *meta3;
@property (weak, nonatomic) IBOutlet UIView *lines;

//
@property (weak, nonatomic) IBOutlet UIImageView *headImage;

@property (weak, nonatomic) IBOutlet UILabel *positionL;
@property (weak, nonatomic) IBOutlet UILabel *companyLL;
@property (weak, nonatomic) IBOutlet UILabel *cityL;
//工作年限
@property (weak, nonatomic) IBOutlet UILabel *workTimeL;
@property (weak, nonatomic) IBOutlet UILabel *degreeL;
//日期

//薪资
@property (weak, nonatomic) IBOutlet UILabel *payL;
//已认证图片
@property (weak, nonatomic) IBOutlet UIImageView *AuthenticationImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *authenticationImageViewWidth;
//视频简介图片
@property (weak, nonatomic) IBOutlet UIImageView *videoImage;

//聊天
@property (weak, nonatomic) IBOutlet UIButton *chatImageButton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *meta2ToMeta1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageWidthConstraint;
//meta1到底部的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *meta1ToBottomConstraint;
//公司到职位间的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyToPositionConstraint;
//城市到公司的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityToCompanyConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameToIconConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityHeightConstraint;


@end

static NSString * const reuseIdentifier = @"mainCell";

@implementation MainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.headImage setCorner:4];
    self.headImage.contentMode = UIViewContentModeScaleAspectFill;
    
    self.companyToPositionConstraint.constant = 5;
    self.cityToCompanyConstraint.constant     = 5;
    
    self.iconImageWidthConstraint.constant = W(60);
    self.iconImageHeightConstraint.constant = W(60);
    
    self.companyToPositionConstraint.constant = 10;
    self.cityToCompanyConstraint.constant = 10;
    
    if (KWIDTH == 320) {
        self.companyToPositionConstraint.constant = 8.f;
        self.cityToCompanyConstraint.constant = 7.f;
        self.iconImageHeightConstraint.constant = 55;
        self.iconImageWidthConstraint.constant  = 55;
    }else if (KWIDTH == 375){
        self.companyToPositionConstraint.constant = 10;
        self.cityToCompanyConstraint.constant = 10;
        self.iconImageWidthConstraint.constant = 62;
        self.iconImageHeightConstraint.constant = 62;
    }
    
    self.iconToLeftConstraint.constant = W(15);          //图片到cell顶部的约束
    self.iconToTopConstraint.constant = W(15);           //图片到cell顶部的约束
    self.meta1ToBottomConstraint.constant = W(15);       //meta到底部的约束
    self.nameToIconConstraint.constant = W(15);          //z职位名称
    self.linesHeightConstraint.constant = W(10);
    
    
    
    self.positionL.font = [UIFont boldSystemFontOfSize:F(16)];
    NSLog(@"%f",F(16));
    self.companyLL.font = [UIFont systemFontOfSize:F(15)];
    self.cityL.font = [UIFont systemFontOfSize:F(13)];
    
    self.payL.font = [UIFont boldSystemFontOfSize:F(15)];
    self.workTimeL.font = [UIFont systemFontOfSize:F(13)];
    self.degreeL.font = [UIFont systemFontOfSize:F(13)];
    
    self.positionHeightConstraint.constant = F(16);
    self.companyHeightConstraint.constant = F(14);
    self.cityHeightConstraint.constant = F(13);
    
    self.meta1.font = [UIFont systemFontOfSize:F(12)];
    self.meta2.font = [UIFont systemFontOfSize:F(12)];
    self.meta3.font = [UIFont systemFontOfSize:F(12)];

    
    self.cityL.textColor        = MainLabelColor;
    self.workTimeL.textColor    = MainLabelColor;
    self.degreeL.textColor      = MainLabelColor;
    
    [self setMetaLabel:_meta1];
    [self setMetaLabel:_meta2];
    [self setMetaLabel:_meta3];


    self.lines.backgroundColor = COLOR_BGCOLOR;
    self.chatImageButton.hidden = YES;
    self.meta3.hidden = YES;
}

-(void)setMetaLabel:(UILabel *)label
{
    label.layer.borderWidth =1;
    label.layer.borderColor = METACOLOR.CGColor;
    [label setCorner:META_CORNER];
    label.textAlignment = NSTextAlignmentLeft;
    label.layer.masksToBounds = YES;
    [label sizeToFit];
}

-(void)setModel:(PositionCoreData *)model
{
    NSLog(@"%@",model.c_Logo);
    /** 公司logo */
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.c_Logo] placeholderImage:[UIImage imageNamed:@"矢量智能对象"]];
    /** 公司名 */
    NSString *name = model.c_Name;
    if (name.length > 15) {
        self.companyLL.text = [name substringToIndex:15];
    }else{
        self.companyLL.text = name;
    }
    /** 职位名 */
    NSString *pName = model.p_Name;
    if (pName.length > 10) {
        self.positionL.text = [pName substringToIndex:10];
    }else{
        self.positionL.text = pName;
    }
    /** 城市 工作经验 学历 */
    NSString *city      = [NSString stringWithFormat:@"%@",model.p_City];
    NSString *workExp   = [NSString stringWithFormat:@"  %@",model.p_Exp];
    NSString *education = [NSString stringWithFormat:@"  %@",model.p_Education];
    if (kStringIsEmpty(model.p_Exp)) {
        workExp = @"";
    }
    if (kStringIsEmpty(model.p_City)) {
        city = @"";
    }
    self.cityL.text     = [NSString stringWithFormat:@"%@%@%@",city,workExp,education];
    /** 企业规模  */
    NSString *scale     = model.c_Scale;
    
    if (kStringIsEmpty([GlobalMethod exchangeEmpty:scale])) {
        scale = @"   20人以下   ";
    }
    self.meta1.text     = scale;
    /** 企业性质 */
    NSString *i_Name    = model.i_Name;
    NSLog(@"%@",i_Name);
    if (kStringIsEmpty(i_Name) || [[GlobalMethod exchangeEmpty:model.i_Name] isEqualToString:@"<null>"]) {
        i_Name = @"   农药生产   ";
    }
    self.meta2.text     = i_Name;
    
    self.meta3.text     = @"   农业部定点生产企业   ";
    /** 薪资待遇 */
    self.payL.text = model.p_Pay;
    //企业是否已认证
    NSString *authentication = model.c_Auitstatus;
    if ([authentication isEqualToString:@"0"]) {
        self.AuthenticationImageView.hidden = YES;
        self.authenticationImageViewWidth.constant = 0;
    }else{
        self.AuthenticationImageView.hidden = NO;
    }
    //企业是否有视频
    NSString *isVideo = model.isVideo;
    self.videoImage.hidden = YES;
    if ([isVideo isEqualToString:@"0"]) {
        self.videoImage.hidden = YES;
    }else if ([isVideo isEqualToString:@"1"]){
        self.videoImage.hidden = NO;
    }
    CGFloat bottomMargin = 0 ;
    if (isIphone5) {
        bottomMargin = -20;
    }else if (isIphone6){
        bottomMargin = 0;
    }else if (isIphone6p){
        bottomMargin = 0;
    }
    
    NSLog(@"%f",self.meta1.height);
    _lines.x = W(15) * 3 + self.iconImageWidthConstraint.constant + self.meta1.height;
    NSLog(@"%f",_lines.x + self.linesHeightConstraint.constant);
    self.iconButton.frame = self.headImage.frame;
    [self setupAutoHeightWithBottomView:_lines bottomMargin:bottomMargin];
}

-(CGFloat)resetCellModel
{
    return (W(15) * 3 + self.iconImageWidthConstraint.constant + 18 + _linesHeightConstraint.constant);
}

-(void)layoutSubviews
{
    
}

//-(void)setCellInfoWithModel:(PositionCoreData *)model
//{
//    NSLog(@"%@",model.c_Logo);
//    /** 公司logo */
//    [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.c_Logo] placeholderImage:[UIImage imageNamed:@"矢量智能对象"] options:SDWebImageRetryFailed|SDWebImageLowPriority];
//    /** 公司名 */
//    NSString *name = model.c_Name;
//    if (name.length > 15) {
//        self.companyLL.text = [name substringToIndex:15];
//    }else{
//        self.companyLL.text = name;
//    }
//    /** 职位名 */
//    NSString *pName = model.p_Name;
//    if (pName.length > 10) {
//        self.positionL.text = [pName substringToIndex:10];
//    }else{
//        self.positionL.text = pName;
//    }
//    /** 城市 工作经验 学历 */
//    NSString *city      = [NSString stringWithFormat:@"%@",model.p_City];
//    NSString *workExp   = [NSString stringWithFormat:@"  %@",model.p_Exp];
//    NSString *education = [NSString stringWithFormat:@"  %@",model.p_Education];
//    if (kStringIsEmpty(model.p_Exp)) {
//        workExp = @"";
//    }
//    if (kStringIsEmpty(model.p_City)) {
//        city = @"";
//    }
//    self.cityL.text     = [NSString stringWithFormat:@"%@%@%@",city,workExp,education];
//    /** 企业规模  */
//    NSString *scale     = model.c_Scale;
//    if (kStringIsEmpty(model.c_Scale)) {
//        scale = @"20人以下";
//    }
//    self.meta1.text     = scale;
//    /** 企业性质 */
//    NSString *i_Name    = model.i_Name;
//    NSLog(@"%@",i_Name);
//    if (kStringIsEmpty(i_Name)) {
//        i_Name = @"农药生产";
//    }
//    self.meta2.text     = i_Name;
//    /** 薪资待遇 */
//    self.payL.text = model.p_Pay;
//    //企业是否已认证
//    NSString *authentication = model.c_Auitstatus;
//    if ([authentication isEqualToString:@"0"]) {
//        self.AuthenticationImageView.hidden = YES;
//        self.authenticationImageViewWidth.constant = 0;
//    }else{
//        self.AuthenticationImageView.hidden = NO;
//    }
//    //企业是否有视频
//    NSString *isVideo = model.isVideo;
//    self.videoImage.hidden = YES;
//    if ([isVideo isEqualToString:@"0"]) {
//        self.videoImage.hidden = YES;
//    }else if ([isVideo isEqualToString:@"1"]){
//        self.videoImage.hidden = NO;
//    }
//    
//}

-(void)positionWidthConstraint{
    if (KWIDTH == 320)
    {
        if (_AuthenticationImageView.hidden == YES) {
            self.positionNameConstraintWidth.constant = 130;
        }else{
            self.positionNameConstraintWidth.constant = 95;
        }
    }else{
        if (_AuthenticationImageView.hidden == NO) {
            self.positionNameConstraintWidth.constant = self.positionL.text.length * 20  + 10;
            if (self.positionNameConstraintWidth.constant > 180) {
                self.positionNameConstraintWidth.constant = 180;
            }
        }else{
            self.positionNameConstraintWidth.constant = 160;
        }
    }
}

- (IBAction)tapIconButton:(UIButton *)sender {
    PYPhotoBrowseView *photoBroseView = [[PYPhotoBrowseView alloc] init];
    // 2.1 设置图片源(UIImageView)数组
    photoBroseView.sourceImgageViews = [NSArray arrayWithObjects:self.headImage, nil];
    // 2.2 设置初始化图片下标（即当前点击第几张图片）
   // photoBroseView.currentIndex = gesture.view.tag - 100 ;
    if (photoBroseView.images.count == photoBroseView.sourceImgageViews.count)
    {
        // 3.显示(浏览)
        [photoBroseView show];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
