//
//  FirstSelectMoreButtonView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "FirstSelectMoreButtonView.h"

@implementation FirstSelectMoreButtonView

#pragma mark 懒加载
- (UIView *)bgView{
    if (_bgView == nil) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        [_bgView setCorner:4];
    }
    return _bgView;
}

- (UIImageView *)screenImageView{
    if (_screenImageView == nil) {
        _screenImageView = [UIImageView new];
        _screenImageView.image = [UIImage imageNamed:@"first_shielding"];
        _screenImageView.widthHeight = XY(W(15),W(15));
    }
    return _screenImageView;
}

- (UILabel *)screenLabel{
    if (_screenLabel == nil) {
        _screenLabel = [UILabel new];
        _screenLabel.font = [UIFont systemFontOfSize:F(15)];
        _screenLabel.textColor = COLOR_LABELThreeCOLOR;
        _screenLabel.text = @"点击屏蔽该企业";
        UITapGestureRecognizer *tapRemoveCompany = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapRemove)];
        [_screenLabel addGestureRecognizer:tapRemoveCompany];
        _screenLabel.userInteractionEnabled = YES;
    }
    return _screenLabel;
}

-(UIWindow *)currWindow
{
    if (!_currWindow) {
        _currWindow = [[UIWindow alloc] initWithFrame:CGRectMake(0, 0, KWIDTH,KHEIGHT)];
//        _currWindow.windowLevel = UIWindowLevelAlert + 1;
        _currWindow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        _currWindow.hidden = NO;
    }
    return _currWindow;
}

//画三角形
-(DrawRectTriangleView *)triangleView
{
    if (!_triangleView) {
        _triangleView = [DrawRectTriangleView new];
        _triangleView.i = 0;
        //[_triangleView drawRectWithStatus:0 rect:CGRectZero];
        _triangleView.backgroundColor = [UIColor clearColor];
        _triangleView.widthHeight = XY(W(15),W(15));
    }
    return _triangleView;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self addSubViews];
        [self setAnimate];
    }
    return self;
}

-(void)addSubViews
{
    [self addSubview:self.currWindow];
    [self.currWindow addSubview:self.triangleView];
    [self.currWindow addSubview:self.bgView];
    [self.bgView addSubview:self.screenLabel];
    [self.bgView addSubview:self.screenImageView];
}

-(void)setAnimate
{
    self.bgView.transform = CGAffineTransformMakeTranslation(1 / 300.0f, 1 / 270.0f);
    self.currWindow.alpha = 0;
    
    [UIView animateWithDuration:0.35f animations:^{
        self.bgView.transform = CGAffineTransformMakeTranslation(1, 1);
        self.currWindow.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    FirstSelectMoreButtonView * view = [FirstSelectMoreButtonView new];
//    view resetViewWithBGViewFrame:<#(CGFloat)#>
//    [view resetWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithBGViewFrame:(CGFloat)top
{
    NSLog(@"%f",top);
    
    [self.bgView setFrame:CGRectMake(10, top + 10 , KWIDTH - 10 * 2, W(45))];
    [self.triangleView setFrame:CGRectMake(self.bgView.right - 23, top, 15, 15)];
    
    [_screenImageView setFrame:CGRectMake(W(15), 15, 15, 15)];
    _screenImageView.centerY = _bgView.height / 2.0f;
    [_screenLabel setFrame:CGRectMake(_screenImageView.right + 15, 15, self.bgView.width - _screenImageView.right, self.height)];
    _screenLabel.centerY = _screenImageView.centerY;
    
}

//点击屏蔽事件
-(void)tapRemove
{
    if (self.tapRemoveBlock)
    {
        self.tapRemoveBlock();
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
