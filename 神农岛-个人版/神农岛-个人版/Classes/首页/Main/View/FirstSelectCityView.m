//
//  FirstSelectCityView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "FirstSelectCityView.h"

@implementation FirstSelectCityView

#pragma mark 懒加载

- (UIView *)cityBGView{
    if (_cityBGView == nil) {
        _cityBGView = [UIView new];
    }
    return _cityBGView;
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    FirstSelectCityView * view = [FirstSelectCityView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpView];
        [self setModel];
    }
    return self;
}


//添加subview
- (void)setUpView{
    [self addSubview:self.cityBGView];
}

-(void)setModel
{
    _proArr = [NSArray arrayWithObjects:@"全国",@"北京",@"天津",@"上海",@"重庆",@"河南省",
               @"吉林省",@"浙江省",@"江西省",@"山东省",@"湖北省",
               @"福建省",@"湖南省",@"广东省",@"江苏省",@"四川省",
               @"贵州省",@"云南省",@"山西省",@"甘肃省",
               @"青海省",@"黑龙江",@"河北省",@"内蒙古",@"陕西省",
               @"辽宁省",@"安徽省",@"海南省",@"台湾省",@"宁夏",
               @"广西",@"新疆",@"西藏",nil];
    
    
    [self createViewsNumber:_proArr.count];
    
}

-(void)createViewsNumber:(NSInteger)number
{
    NSInteger count = number / 4;
    NSLog(@"createViewsNumberCount%ld",count);
    CGFloat width = (KWIDTH - W(30) - 10 * 3) / 4;
    NSInteger tag = 100;
    
    for (int j = 0 ; j < count + 1; j++)
    {
        for (int i = 0; i < 4 ; i ++)
        {
            UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(W(15) + i * (width + 10), W(15) + j * (W(32) + 10) , width, W(32))];
            [self addSubview:button];
            if ((j == count) && i > 0) {
                [button removeFromSuperview];
            }
            button.tag = tag ++ ;
            NSLog(@"%ld",tag);
            button.layer.cornerRadius   = 5;
            button.layer.masksToBounds  = YES;
            button.layer.borderWidth = 1;
            button.layer.borderColor = COLOR_LINESCOLOR.CGColor;
            button.titleLabel.font = [UIFont systemFontOfSize:14];
            [button setTitleColor:COLOR_LABELSIXCOLOR forState:UIControlStateNormal];
        }
    }
    for (int i = 0; i < number; i ++) {
        UIButton *button = [self viewWithTag:100 + i];
        [button setTitle:_proArr[i] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)tapButton:(UIButton *)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectCityDelegateWithTitle:)]) {
        [self.delegate selectCityDelegateWithTitle:sender.titleLabel.text];
    }
}


#pragma mark 刷新view
- (void)resetViewWithModel:(id)model{
    //[GlobalMethod removeAllSubView:self withTag:TAG_LINE];//移除线
    //刷新view
    //self.cityBGView.widthHeight = XY(W(0), W(0));
    //self.cityBGView.leftTop = XY(W(15),W(15));
    
    
}

@end
