//
//  ExperienceTableView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ExperienceTableView.h"
#import "FirstSelectTableViewCell.h"

@implementation ExperienceTableView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        [self registerClass:[FirstSelectTableViewCell class] forCellReuseIdentifier:@"FirstSelectTableViewCell"];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listNameArray.count;
}

-(void)setUpTableArray:(NSArray *)arr count:(NSInteger)count tag:(NSInteger)tag industryTitle:(NSString *)industryTitle positionTitle:(NSString *)positionTitle
{
    self.listIDArray    = [NSMutableArray array];
    self.listNameArray  = [NSMutableArray array];
    [self.listNameArray addObject:@"不限"];
    [self.listIDArray addObject:@""];
    if (tag == 1) {
        self.isFirstOn = YES;
        for (IndustryModel *model in arr) {
            [self.listNameArray addObject:model.iName];
            [self.listIDArray addObject:[GlobalMethod doubleToString:model.iDProperty]];
        }
        self.titles = industryTitle;
    }else if (tag == 2){
        self.isFirstOn = NO;
        for (PositionPlistModel *model in arr) {
            [self.listNameArray addObject:model.pName];
            [self.listIDArray addObject:[GlobalMethod doubleToString:model.iDProperty]];
        }
        self.titles = positionTitle;
    }
    [self reloadData];
}

-(FirstSelectTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FirstSelectTableViewCell *cell = [FirstSelectTableViewCell selectButton:@""];
    cell.selectLabel.text = self.listNameArray[indexPath.row];
    cell.line.hidden = indexPath.row == 0;
    if ([self.titles isEqualToString:self.listNameArray[indexPath.row]]) {
        cell.selectLabel.textColor = COLOR_MAINCOLOR;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = self.listNameArray[indexPath.row];
    NSString *tag  = self.listIDArray[indexPath.row];
    if (self.selectDelegate && [self.selectDelegate respondsToSelector:@selector(protocolSelectExperienceTableView:selectTag:status:isFirstOn:)]) {
        [self.selectDelegate protocolSelectExperienceTableView:name selectTag:indexPath.row status:tag isFirstOn:self.isFirstOn];
    }
}

#pragma mark scrollView的联动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%@",scrollView);
    NSLog(@"%f",scrollView.contentOffset.y);
    UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
    //sc.userInteractionEnabled = NO;
    if (scrollView.contentOffset.y >= 0) {
        //x向上
        if (sc.contentOffset.y <= W(150)) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }else{
        if (scrollView.contentOffset.y <= 0 && sc.contentOffset.y > 0 ) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
    //sc.userInteractionEnabled = YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
