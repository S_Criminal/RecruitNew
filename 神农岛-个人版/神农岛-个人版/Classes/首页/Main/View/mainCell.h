//
//  mainCell.h
//  神农岛
//
//  Created by 宋晨光 on 16/3/30.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+LDSetCellInfo.h"
#import "PositionCoreData+CoreDataClass.h"
//@protocol mainSelectMoreDelegate <NSObject>
//
//-(void)protocolMainSelectMore:(UIButton *)sender;
//
//@end

@interface MainCell : UITableViewCell

@property (nonatomic ,strong) PositionCoreData *model;

@property (weak, nonatomic) IBOutlet UIButton *selectMoreButton;        //它的tag值为100
@property (weak, nonatomic) IBOutlet UIButton *iconButton;


+(CGFloat)fetchHeight;

-(CGFloat)resetCellModel;

/**
 * 公司图标
 */
//@property(strong,nonatomic)UIImageView *imageV;
///**
// * 职位名称
// */
//@property(strong,nonatomic)UILabel *workL;
///**
// * 公司名称
// */
//@property(strong,nonatomic)UILabel *companyL;
///**
// * 城市名称
// */
//@property(strong,nonatomic)UILabel *provinceL;
///**
// * 工作经验
// */
//@property(strong,nonatomic)UILabel *timeL;
///**
// * 学历
// */
//@property(strong,nonatomic)UILabel *educationL;
///**
// * 薪资
// */
//@property(strong,nonatomic)UILabel *moneyL;
///**
// * 标签
// */
//@property(strong,nonatomic)UILabel *label;
//




//+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSInde xPath *)indexPath;

@end
