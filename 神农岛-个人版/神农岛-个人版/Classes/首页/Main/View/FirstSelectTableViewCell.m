//
//  FirstSelectTableViewCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "FirstSelectTableViewCell.h"

@implementation FirstSelectTableViewCell

-(UILabel *)selectLabel
{
    if (!_selectLabel) {
        _selectLabel = [UILabel new];
        [_selectLabel setFrame:CGRectMake(W(15), 0, KWIDTH - W(15), self.height - 1)];
        _selectLabel.textColor = [UIColor blackColor];
        _selectLabel.font = [UIFont systemFontOfSize:F(15)];
    }
    return _selectLabel;
}

-(UIButton *)selectButton
{
    if (!_selectButton) {
        _selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectButton setFrame:CGRectMake(0, 0, KWIDTH, self.height - 1)];
        _selectButton.contentHorizontalAlignment = NSTextAlignmentLeft;
        [_selectButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_selectButton setTitleColor:COLOR_MAINCOLOR forState:UIControlStateSelected];
        _selectButton.backgroundColor = [UIColor whiteColor];
    }
    return _selectButton;
}

-(UIView *)line
{
    if (!_line)
    {
        _line = [[UIView alloc]initWithFrame:CGRectMake(W(15), _selectButton.bottom , KWIDTH - W(15), 1)];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}


+(instancetype)selectButton:(NSString *)title
{
    FirstSelectTableViewCell *cell = [FirstSelectTableViewCell new];
    return cell;
}



- (instancetype)init
{
    self = [super init];
    if (self) {
        [self.contentView addSubview:self.selectLabel];
        [self.contentView addSubview:self.line];
    }
    return self;
}


-(void)setTitle:(NSString *)title
{
    _title = title;
    _selectLabel.text = title;
    _line.hidden = [title isEqualToString:@"不限"] ? YES : NO;
}

@end
