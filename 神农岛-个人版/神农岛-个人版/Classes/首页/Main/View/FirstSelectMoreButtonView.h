//
//  FirstSelectMoreButtonView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawRectTriangleView.h"
@interface FirstSelectMoreButtonView : UIView

@property (nonatomic ,strong) DrawRectTriangleView *triangleView;//三角形

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIImageView *screenImageView;
@property (nonatomic ,strong) UILabel *screenLabel;

@property (nonatomic ,strong) UIWindow * currWindow;

@property (nonatomic ,strong) void (^tapRemoveBlock)();
#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithBGViewFrame:(CGFloat)top;

@end
