//
//  ExperienceTableView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PositionPlistModel.h"
#import "IndustryModel.h"

@protocol SelectTableDelegate <NSObject>

-(void)protocolSelectExperienceTableView:(NSString *)title selectTag:(NSInteger)tag status:(NSString *)status isFirstOn:(BOOL)isFirstOn;

@end

@interface ExperienceTableView : UITableView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSArray *listArr;
@property (nonatomic ,strong) NSMutableArray *listIDArray;
@property (nonatomic ,strong) NSMutableArray *listNameArray;
@property (nonatomic ,assign) BOOL isFirstOn;   //用来判断第一列还是第二列

@property (nonatomic ,strong) NSString *titles;

@property (nonatomic ,weak) id<SelectTableDelegate>selectDelegate;

-(void)setUpTableArray:(NSArray *)arr count:(NSInteger)count tag:(NSInteger)tag industryTitle:(NSString *)industryTitle positionTitle:(NSString *)positionTitle;

@end
