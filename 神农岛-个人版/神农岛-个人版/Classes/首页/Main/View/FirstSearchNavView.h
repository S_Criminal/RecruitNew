//
//  FirstSearchNavView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstSearchNavView : UIView
@property (nonatomic ,strong) UILabel *textLabel;
@property (nonatomic ,strong) void (^tapBlock)();

@end
