//
//  FirstSearchNavView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "FirstSearchNavView.h"

@implementation FirstSearchNavView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createViews];
    }
    return self;
}

-(UILabel *)textLabel
{
    if (!_textLabel) {
        _textLabel     = [[UILabel alloc]initWithFrame:CGRectMake(12,  8 + 20, KWIDTH - 24, 30)];
        _textLabel.backgroundColor = [HexStringColor colorWithHexString:@"#0ca884"];
        _textLabel.font = [UIFont systemFontOfSize:12];
        _textLabel.layer.cornerRadius = 15;
        _textLabel.layer.masksToBounds = YES;
        [_textLabel setAttributedText:[CustomTitleButtonStr setButtonAttributeTitleWithTitle:@"输入职位名或公司名" AndTitleImageName:@"first_search" ImageRect:CGRectMake(0, -3, 14, 14) color:@"#3febc4"]];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.userInteractionEnabled = YES;
    }
    return _textLabel;
}

-(void)createViews
{
    UITapGestureRecognizer *tapSearch = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(searchView)];
    [self.textLabel addGestureRecognizer:tapSearch];
    [self addSubview:self.textLabel];
}

-(void)searchView
{
    if (self.tapBlock != nil) {
        self.tapBlock();
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
