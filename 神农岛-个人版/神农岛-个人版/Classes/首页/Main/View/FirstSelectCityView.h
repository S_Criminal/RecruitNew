//
//  FirstSelectCityView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol selectCityDelegate <NSObject>

-(void)selectCityDelegateWithTitle:(NSString *)title;

@end


@interface FirstSelectCityView : UIView

@property (nonatomic ,strong) UIView *cityBGView;

@property (nonatomic ,weak) id<selectCityDelegate>delegate;

@property (nonatomic ,strong) NSArray *proArr;

#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model;


@end
