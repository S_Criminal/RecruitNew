//
//  FirstSelectTableViewCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstSelectTableViewCell : UITableViewCell

+(instancetype)selectButton:(NSString *)title;
#pragma mark 刷新view
@property (nonatomic ,strong) UILabel *selectLabel;
@property (nonatomic ,strong) UIButton *selectButton;
@property (nonatomic ,strong) UIView *line;
@property (nonatomic ,strong) NSString *title;

@end
