//
//  workDetail.m
//  神农岛
//
//  Created by 宋晨光 on 16/4/11.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "WorkDetailCell.h"
#import "PositionWorkModel.h"
#import "CityShortName.h"
@interface WorkDetailCell ()

@property (weak, nonatomic) IBOutlet UILabel *pNameL;
@property (weak, nonatomic) IBOutlet UILabel *pPayL;


@property (weak, nonatomic) IBOutlet UIImageView *pCityImage;
@property (weak, nonatomic) IBOutlet UILabel *pCity;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pCityImageConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pCityConstraint;


@property (weak, nonatomic) IBOutlet UILabel *pAge;
@property (weak, nonatomic) IBOutlet UILabel *pEducation;
@property (weak, nonatomic) IBOutlet UILabel *pNature;
@property (weak, nonatomic) IBOutlet UILabel *pTemptation;

@property (weak, nonatomic) IBOutlet UIView *lines;


@end

@implementation WorkDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (KWIDTH == 320)
    {
        self.positionToTopConstraint.constant = 18;
        self.positionToLeftConstraint.constant = 15;
        self.positionToCityImageConstraint.constant = 25;
        self.cityImageToBottomConstraint.constant = 12;
        
        
        self.pNameL.font = [UIFont boldSystemFontOfSize:15];
        self.pPayL.font = [UIFont systemFontOfSize:14];
        
        [self metaLabelStyle:self.pCity];
        [self metaLabelStyle:self.pEducation];
        [self metaLabelStyle:self.pNature];
        [self metaLabelStyle:self.pAge];
        
        self.left1Constraint.constant = 8;
        self.left2Constraint.constant = 8;
        self.left3Constraint.constant = 8;
        self.payAndPositionConstraint.constant = 14;
    }else if (KWIDTH == 375){
        self.pNameL.font = [UIFont systemFontOfSize:17];
        self.pPayL.font  = [UIFont systemFontOfSize:15];
        self.positionToLeftConstraint.constant = 15;
    }
    self.lines.backgroundColor = COLOR_SEPARATE_COLOR;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)metaLabelStyle:(UILabel *)label
{
    label.font = [UIFont systemFontOfSize:12];
}

-(void)setCellInfoWithModel:(PositionWorkModel *)model
{
    
    NSLog(@"modelmodel%@",model);
    
    NSString *position = [NSString stringWithFormat:@"%@",model.pName];
    if (isIphone6 || isIphone5) {
        if (position.length > 15) {
            position = [position substringToIndex:15];
        }
    }else{
        if (position.length > 16) {
            position = [position substringToIndex:18];
        }
    }
    
    self.pNameL.text =  position;
    
    self.pPayL.text  =  [NSString stringWithFormat:@"%@",model.pPay];
    
    
    NSLog(@"%@",model.pCity);
    if (model.pCity)
    {
        self.pCity.text = [CityShortName getAdressWithProvience:@"" City:model.pCity];
    }
    
    if (model.pCity.length > 6 && KWIDTH != 414) {
        self.left1Constraint.constant = 4;
        self.left2Constraint.constant = 4;
        self.left3Constraint.constant = 4;
    }
    
    self.pAge.text = model.pExp;
    self.pNature.text = model.pNature;
    self.pEducation.text = model.pEducation;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
