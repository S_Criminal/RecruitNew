//
//  workDetail.h
//  神农岛
//
//  Created by 宋晨光 on 16/4/11.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *left1Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *left2Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *left3Constraint;



//薪资和职位间的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payAndPositionConstraint;
//职位到顶部的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionToTopConstraint;
//职位到左边的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionToLeftConstraint;
//职位到城市图标的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionToCityImageConstraint;
//城市图标到底部的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityImageToBottomConstraint;



@end
