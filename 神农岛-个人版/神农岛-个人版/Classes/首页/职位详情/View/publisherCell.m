//
//  publisherCell.m
//  神农岛
//
//  Created by 宋晨光 on 16/4/11.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "publisherCell.h"
#import "UIImageView+WebCache.h"

@interface publisherCell ()

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *positionL;
@property (weak, nonatomic) IBOutlet UILabel *replyL;
@property (weak, nonatomic) IBOutlet UILabel *processRateL;
@property (weak, nonatomic) IBOutlet UILabel *processTimeL;
@property (weak, nonatomic) IBOutlet UIImageView *returnImage;





//简历处理率中心对齐
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *briefProcessConstraintCenter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *briefProcessConstraintLeft;

//简历处理用时
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *briefProcessTimeConstraintLeft;


@end

@implementation publisherCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    self.returnImage.hidden = YES;
    self.replyL.hidden = YES;
    if (KWIDTH == 320) {
        self.briefProcessConstraintCenter.constant = -10;
        self.briefProcessConstraintLeft.constant = 50;
        self.briefProcessTimeConstraintLeft.constant = 45;
        
        self.nameL.font = [UIFont systemFontOfSize:14];
        self.positionL.font = [UIFont systemFontOfSize:12];
        
    }else if (KWIDTH == 414){
        self.briefProcessTimeConstraintLeft.constant = 67;
    }
    self.processRateL.textAlignment = NSTextAlignmentCenter;
    self.processTimeL.textAlignment = NSTextAlignmentCenter;
}

-(void)setModel:(PositionWorkModel *)model
{
    
}

-(void)setCellInfoWithModel:(PositionWorkModel *)model
{
    NSString *hrPosition = model.uPosition;
    if (hrPosition.length == 0){
        hrPosition = @"HR经理";
    }
    self.nameL.text = model.uName;
    self.positionL.text = hrPosition;
    self.replyL.text = model.cReplyD;
    
    NSString *processRate = [GlobalMethod doubleToString:model.cProcessRate];
    NSString *cRrocessT   = [GlobalMethod doubleToString:model.cProcessT];
    self.processRateL.text = [NSString stringWithFormat:@"%@",processRate];
    self.processTimeL.text = [NSString stringWithFormat:@"%@",cRrocessT];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:model.uHead] placeholderImage:[UIImage imageNamed:@"个人头像"]];
}

//-(void)setCellInfoWithModel:(PositionModel *)model
//{
//    NSString *hrPosition = model.hRposition;
//    if (hrPosition.length == 0) {
//        hrPosition = @"HR经理";
//    }
//    self.nameL.text = model.hRname;
//    self.positionL.text = model.hRposition;
//    self.replyL.text = model.reply;
//    self.processRateL.text = model.processRate;
//    self.processTimeL.text = model.processT;
//    [self.icon sd_setImageWithURL:[NSURL URLWithString:model.hrImageView] placeholderImage:[UIImage imageNamed:@"个人头像"]];
////    self.icon.contentMode = UIViewContentModeScaleAspectFill;
////    self.icon.layer.masksToBounds = YES;
//    
//    self.icon.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHRImage:)];
//    [self.icon addGestureRecognizer:tapImage];
//    NSLog(@"%@",self.icon);
//    
//}

-(void)tapHRImage:(UITapGestureRecognizer *)sender
{
//    NSLog(@"%@",sender);
//    NSDictionary *dic = @{@"hrImage":sender.view};
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"publisherCell" object:nil userInfo:dic];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
