//
//  CompanyCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "CompanyCell.h"
#import "PositionWorkModel.h"

@implementation CompanyCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)createViews
{
    _companyL = [[UILabel alloc]init];
    _companyBriefL = [[UILabel alloc]init];
    
    _companyL.textColor = [HexStringColor colorWithHexString:@"333333"];
    
    
    _companyBriefL.textColor = [HexStringColor colorWithHexString:@"999999"];
    
    CGFloat comapnyFont = 16;
    CGFloat contentFont = 13;
    CGFloat imageWidth = 56;
    CGFloat compnayToImageTop = 7;
    CGFloat imageToTop = 19;
    CGFloat contentBottom = 4;
    CGFloat titleFont = 17;
    CGFloat viewLeftConstraint = 15;
    
    if (isIphone5) {
        titleFont = 15;
        comapnyFont = 14;
        contentFont = 12;
        imageWidth = 44;
        compnayToImageTop = 3;
        imageToTop = 12;
        contentBottom = 2;
    }else if (isIphone6){
        titleFont = 16;
        comapnyFont = 15;
        contentFont = 13;
        imageWidth = 50;
        compnayToImageTop = 4;
        imageToTop = 17;
        contentBottom = 3;
    }
    
    _companyL.font      = [UIFont systemFontOfSize:comapnyFont];
    _companyBriefL.font = [UIFont systemFontOfSize:contentFont];
    
    _headImage = [[UIImageView alloc]init];
    _headImage.layer.masksToBounds = YES;
    _headImage.layer.cornerRadius = imageWidth / 2;
    
    UIImageView *rightImg = [UIImageView new];
    rightImg.image = [UIImage imageNamed:@"computerRight"];
    
    [self addSubview:_headImage];
    [self addSubview:_companyBriefL];
    [self addSubview:_companyL];
    [self addSubview:rightImg];
    
    [rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-W(10));
        make.centerY.mas_equalTo(_headImage.centerY);
    }];
    
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(imageToTop);
        make.left.mas_equalTo(viewLeftConstraint);
        make.width.mas_equalTo(imageWidth);
        make.height.mas_equalTo(imageWidth);
        make.bottom.mas_equalTo(-imageToTop);
    }];
    
    [_companyL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headImage.mas_top).offset(compnayToImageTop);
        make.left.equalTo(_headImage.mas_right).offset(12);
        //                make.bottom.equalTo(companyBriefL.mas_top).offset(-19);
        make.right.mas_equalTo(-15);
    }];
    
    [_companyBriefL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_companyL.mas_left);
        make.width.mas_greaterThanOrEqualTo(10);
        make.bottom.mas_equalTo(_headImage.mas_bottom).offset(-contentBottom);
    }];
}

-(void)setCellInfoWithModel:(PositionWorkModel *)model
{
    NSLog(@"%@uHead",model.uHead);
    [_headImage sd_setImageWithURL:[NSURL URLWithString:model.cLogo] placeholderImage:[UIImage imageNamed:@"矢量智能对象"]];
    NSString *c_Name =  [NSString stringWithFormat:@"%@",model.cName];
    if (c_Name.length > 15) c_Name = [c_Name substringToIndex:15];
    _companyL.text = c_Name;
//    [_headImage sd_setImageWithURL:[NSURL URLWithString:model.uuHead] placeholderImage:[UIImage imageNamed:@"矢量智能对象"] options:SDWebImageRetryFailed|SDWebImageLowPriority];
    
    NSString *i_Name = model.iName;
    NSString *scale  = model.cScale;
    NSString *w_Name = model.wName;
    
    if (kStringIsEmpty(i_Name)) {
        i_Name = @"农业";
    }
    if (kStringIsEmpty(w_Name)) {
        w_Name = @"私营企业";
    }
    if (kStringIsEmpty(scale)) {
        scale = @"20人以下";
    }
    _companyBriefL.text = [NSString stringWithFormat:@"%@/%@/%@",i_Name,w_Name,scale];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
