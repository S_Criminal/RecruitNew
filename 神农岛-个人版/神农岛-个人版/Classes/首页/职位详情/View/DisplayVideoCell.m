//
//  DisplayVideoCell.m
//  神农岛
//
//  Created by 宋晨光 on 16/11/19.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "DisplayVideoCell.h"
#import "CLPlayerView.h"
#import "UIView+CLSetRect.h"

@interface DisplayVideoCell ()
/**button*/
//@property (nonatomic,weak) UIButton *button;
@end

@implementation DisplayVideoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    [button setImage:[UIImage imageNamed:@"播放"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    [self addSubview:button];
    _button = button;
//    [self layoutSubviews];
}

-(void)setUrl:(NSString *)url
{
    _url = url;
}

- (void)playAction:(UIButton *)button
{
    if (_videoDelegate && [_videoDelegate respondsToSelector:@selector(PlayVideoWithCell:)])
    {
        [_videoDelegate PlayVideoWithCell:self];
    }
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
//    _button.centerX = self.width/2.0;
//    _button.centerY = self.height/2.0;
}
@end
