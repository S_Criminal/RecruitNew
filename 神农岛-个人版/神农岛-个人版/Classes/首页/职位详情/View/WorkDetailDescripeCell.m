//
//  WorkDetailDescripeCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "WorkDetailDescripeCell.h"

@implementation WorkDetailDescripeCell{
    CGFloat titletToTop;
    CGFloat constraint;
}

#pragma mark 懒加载
- (UILabel *)titleL{
    if (_titleL == nil) {
        _titleL = [UILabel new];
        _titleL.font = [UIFont systemFontOfSize:F(15)];
//        [_titleL setFrame:CGRectMake(20, 20, 20, 20)];
        _titleL.textColor = [UIColor blackColor];
    }
    return _titleL;
}
- (UILabel *)contentL{
    if (_contentL == nil) {
        _contentL = [UILabel new];
        _contentL.font = [UIFont systemFontOfSize:F(14)];
//        [_titleL setFrame:CGRectMake(20, CGRectGetMaxY(_titleL.frame) + 20, 20, 20)];
        _titleL.textColor = [UIColor blackColor];
    }
    return _contentL;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUI];
        [self addSubview:self.titleL];
        [self addSubview:self.contentL];
    }
    return self;
}

-(void)setUI
{
    titletToTop = W(20);
    constraint = W(20);
}

-(void)setIndex:(NSInteger)index
{
    _index = index;
    if (_index == 1) {
        titletToTop = W(20);
    }else{
        titletToTop = 0;
    }
}



-(void)setModel:(WorkDetailDescripeModel *)model
{
    _model = model;
    
    NSString *content = [GlobalMethod exchangeEmpty:_model.content];
    content = [GlobalMethod exChangeEmptyFeedLine:content];
    NSLog(@"%@",content);
    
    self.titleL.text    = _model.title;
    self.contentL.text  = @"";
    
    [self.titleL sizeToFit];
    
    if (kStringIsEmpty(_model.content)) {
        [GlobalMethod removeAllSubViews:self];
        UILabel *label = [UILabel new];
        [self.contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(0.6);
            make.bottom.mas_equalTo(0);
        }];
        return;
    }
    
    [self setPositionDescribleLable:_contentL content:content];
    
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(titletToTop);
        make.right.mas_equalTo(-20);
    }];
    
    [self.contentL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(self.titleL.mas_bottom).offset(constraint);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-constraint);
    }];
    
//    [self setupAutoHeightWithBottomView:_contentL bottomMargin:0];
}

-(void)setPositionDescribleLable:(UILabel *)label content:(NSString *)str
{
    NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle2 setLineSpacing:7];
    [attributedString2 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle2 range:NSMakeRange(0, [str length])];
    [label setAttributedText:attributedString2];
    [label sizeToFit];
    
    label.textColor = [HexStringColor colorWithHexString:@"666666"];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
