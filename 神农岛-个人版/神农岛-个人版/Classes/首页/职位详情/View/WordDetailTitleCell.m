//
//  WordDetailTitleCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "WordDetailTitleCell.h"

@implementation WordDetailTitleCell{
    CGFloat titleFont;
    CGFloat left;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
        [self createViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    titleFont = 16;
    left = ViewLeftConstraint;
}

-(UIImageView *)leftImage
{
    if (!_leftImage) {
        _leftImage = [UIImageView new];
    }
    return _leftImage;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:F(titleFont)];
         _titleLabel.textColor = [UIColor blackColor];
    }
    return _titleLabel;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
    }
    return _line;
}

-(void)createViews
{
    
    [self addSubview:self.leftImage];
    [self addSubview:self.titleLabel];
    [self addSubview:self.line];
    [_line setFrame:CGRectMake(left, self.height - 1, KWIDTH, 1)];
    _line.backgroundColor = COLOR_SEPARATE_COLOR;
    
    [_leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(left);
        make.centerY.equalTo(self);
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_leftImage.mas_right).offset(W(10));
        make.centerY.equalTo(self);
    }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
