//
//  CompanyCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyCell : UITableViewCell
@property (nonatomic ,strong) UIImageView *headImage;
@property (nonatomic ,strong) UILabel *companyL;
@property (nonatomic ,strong) UILabel *companyBriefL;

@end
