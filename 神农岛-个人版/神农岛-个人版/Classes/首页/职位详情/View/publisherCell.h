//
//  publisherCell.h
//  神农岛
//
//  Created by 宋晨光 on 16/4/11.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+LDSetCellInfo.h"
#import "PositionWorkModel.h"

@interface publisherCell : UITableViewCell

@property (nonatomic ,copy) void(^hrImageBlock)(UIImageView  *);
@property (nonatomic ,strong) UIImageView *hrImageView;
@property (nonatomic ,strong) PositionWorkModel *model;


@end
