//
//  DisplayVideoCell.h
//  神农岛
//
//  Created by 宋晨光 on 16/11/19.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DisplayVideoCell;

@protocol VideoDelegate <NSObject>

- (void)PlayVideoWithCell:(DisplayVideoCell *)cell;


@end

@interface DisplayVideoCell : UITableViewCell
/**url*/
@property (nonatomic,copy) NSString *url;

@property (nonatomic,weak) id <VideoDelegate>videoDelegate;

@property (nonatomic ,weak)UIButton *button;


@end
