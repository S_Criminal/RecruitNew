//
//  NewPublisherInfoCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "NewPublisherInfoCell.h"

@implementation NewPublisherInfoCell


-(UILabel *)contentLabel
{
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        _contentLabel.numberOfLines = 0;
        _contentLabel.font = [UIFont systemFontOfSize:F(15)];
        _contentLabel.textColor = COLOR_LABELSIXCOLOR;
    }
    return _contentLabel;
}

-(UIButton *)contentButton{
    if (_contentButton == nil) {
        _contentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _contentButton.titleLabel.font = [UIFont systemFontOfSize:F(15)];
        _contentButton.titleLabel.numberOfLines = 0;
        [_contentButton setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
        _contentButton.widthHeight = XY(KWIDTH - W(30),W(40));
    }
    return _contentButton;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.contentButton];
        [self addSubview:self.contentLabel];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self buildConstraint];
    }
    return self;
}

-(void)buildConstraint
{
    [self.contentButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(W(20));
        make.right.bottom.mas_equalTo(W(-20));
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(W(20));
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(W(-20));
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
