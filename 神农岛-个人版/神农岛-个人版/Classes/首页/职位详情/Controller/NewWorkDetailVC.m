//
//  NewWorkDetailVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "NewWorkDetailVC.h"
#import "TextTableView.h"
/** request */
#import "RequestApi+Main.h"

/** view or cell */
#import "CompanyCell.h"
#import "WordDetailTitleCell.h"
#import "WorkDetailDescripeCell.h"  //职位描述
#import "DisplayVideoCell.h"        //视频简介cell
//#import "publisherCell.h"

#import "NewPublisherInfoCell.h"
#import "BottomLabelView.h"
#import "CLPlayerView.h"

/** model */
#import "FirstMainManager.h"
#import "PositionWorkModel.h"
#import "WorkDetailDescripeModel.h"
#import "BriefInfoModel.h"
/** 公司详情 */
#import "CompanyInfoVC.h"
#import "PositionTrendVC.h"         //职位偏好
#import "MyContactInfoVC.h"         //联系方式
/** tool */
#import "KYAlertView.h"             //提示框

#import "CustomTabBarController.h"

#import "ResumeDeliveryVC.h"    //投递简历

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDKUI.h>
// 自定义分享菜单栏需要导入的头文件
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
#import "CustomShareUI.h"       //自定义分享


@interface NewWorkDetailVC ()<UITableViewDelegate,UITableViewDataSource,VideoDelegate,bottomButtonViewDelegate,RequestDelegate,UIGestureRecognizerDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) FirstMainManager *manager;
@property (nonatomic ,strong) PositionWorkModel *model;

@property (nonatomic ,strong) NSArray *leftContentArr;
@property (nonatomic ,strong) NSArray *leftImageArr;
@property (nonatomic ,strong) NSMutableArray *descripeArr;

/**CLplayer 视频播放*/
@property (nonatomic ,strong) CLPlayerView *playerView;
@property (nonatomic ,strong) KYAlertView *kyAlertView;
@property (nonatomic ,strong) NSString *c_Video;

//简历完整度
@property (nonatomic ,strong) NSMutableAttributedString *completeStr;
@property (nonatomic ,assign) BOOL completeBriefBOOL;

@property (nonatomic ,strong) UIButton *hrTitleButton;
@property (nonatomic ,strong) NSString *hrContanctStr;

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) BottomLabelView *bottomView;

@end

@implementation NewWorkDetailVC{
    CGFloat titleFont;
    CGFloat videoHeight;
}

-(UIButton *)hrTitleButton
{
    if (!_hrTitleButton) {
        _hrTitleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _hrTitleButton.backgroundColor = [UIColor clearColor];
    }
    return _hrTitleButton;
}

-(instancetype)initWithCid:(NSString *)cid pid:(NSString *)pid
{
    NewWorkDetailVC *detail = [NewWorkDetailVC new];
    detail.cid = cid;
    detail.pid = pid;
    return detail;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _manager = [FirstMainManager share];
    _completeBriefBOOL = YES;
    [GlobalData sharedInstance].isDelivery = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self setUI];
    [self setNav];
    [self createTableView];
    [self createBottomView];
    [self setModel];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([GlobalData sharedInstance].isDelivery) {
        [self setModel];
        [GlobalData sharedInstance].isDelivery = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (_playerView)
    {
        if (_playerView.startButton.selected)
        {
            [_playerView pausePlay];
        }
    }
}

-(void)setUI
{
    videoHeight = (KWIDTH - W(17) * 2) / 4 * 3;
    titleFont = F(15);
}

-(void)setNav
{
    self.view.backgroundColor = [UIColor whiteColor];

    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"职位详情" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
        
    } rightTitle:@"分享" rightBlock:^{
        [weakSelf setShare];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setShare
{
    //视频暂停播放
    if (_playerView)
    {
         [_playerView pausePlay];
    }
   
    NSString *urlStr = [NSString stringWithFormat:@"%@Shareposition.html?id=%@",shareHttp,[GlobalMethod doubleToString:_model.iDProperty]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString * content = [NSString stringWithFormat:@"%@·高薪诚聘·%@-老刀招聘APP",self.model.cName,self.model.pName];
//    NSString * shareFriendContent = [NSString stringWithFormat:@"%@·高薪诚聘·%@-老刀招聘APP-农业人才-专业招聘",self.model.pPay,self.model.pName];
    
    NSString *titleFriend = [NSString stringWithFormat:@"招聘|%@",_model.cName];//朋友圈
    
    CustomShareUI *share = [[CustomShareUI alloc]initShareUIContent:content title:titleFriend url:url images:self.model.cLogo shareEnum:shareContentsWithPosition];
    share.positionName = _model.pName;
    share.p_Pay = _model.pPay;
//    share.contents = shareFriendContent;
//    [share shareWithContent];
    [self.view addSubview:share];
}



-(void)createTableView
{
//    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, 1)];
//    line.backgroundColor = COLOR_LINESCOLOR;
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - W(50)) style:UITableViewStylePlain];
    _tableView.delegate   = self;
    _tableView.dataSource = self;
    _tableView.rowHeight  = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 50;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:[GlobalMethod addNavLine]];
    [self.view addSubview:_tableView];
}

-(void)createBottomView
{
    self.bottomView = [BottomLabelView initWithTitle:[NSArray arrayWithObjects:@"投递简历",@"收藏职位", nil] frame:CGRectMake(0, KHEIGHT - W(50), KWIDTH , W(50)) bgColor:MAINCOLOR textColor:@"ffffff" delegate:self];
    self.bottomView.backgroundColor = COLOR_LINESCOLOR;
    self.bottomView.delegate = self;
    
    UIView *vericalLine = [[UIView alloc]initWithFrame:CGRectMake(self.bottomView.width / 2.0 - 0.5, W(15), 1, W(20))];
    vericalLine.backgroundColor = [UIColor whiteColor];
    [self.bottomView addSubview:vericalLine];
    [self.view addSubview:self.bottomView];
}

-(void)setModel
{
    self.leftContentArr = [NSArray arrayWithObjects:@"职位描述",@"职位描述",@"工作地址",@"视频简介",@"HR联系方式", nil];
    self.leftImageArr   = [NSArray arrayWithObjects:@"职位描述",@"职位描述",@"工作地址",@"视频简介",@"职位发布", nil];
    self.descripeArr    = [NSMutableArray array];
    
    DSWeak;
    
    [_manager GetPositionInfoWithPid:self.pid key:[GlobalData sharedInstance].GB_Key success:^(NSDictionary *dic) {
        if ([dic[@"code"] isEqualToNumber:RESPONSE_CODE_200])
        {
            weakSelf.tableView.hidden = NO;
            [GlobalMethod removeView:[weakSelf.view viewWithTag:1100]];
            NSLog(@"职位详情%@",dic);
            NSArray *arr = dic[@"datas"];
            if (arr.count == 0) {
                [MBProgressHUD showError:@"职位不存在" toView:weakSelf.view];
                [weakSelf.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, KWIDTH, KHEIGHT - NAVIGATIONBAR_HEIGHT)]];
                NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
                [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
                weakSelf.hideDelayTimer = timer;
            }
            else
            {
            NSLog(@"%@",arr);
            
            weakSelf.model = [PositionWorkModel modelObjectWithDictionary:arr.firstObject];
            /** 职位描述 */
            NSString *desripe = _model.pDescription;
            NSArray *array = [desripe componentsSeparatedByString:@"卍"];
            
            WorkDetailDescripeModel *officeModel = [[WorkDetailDescripeModel alloc]init];
            officeModel.title = @"任职资格:";
            officeModel.content = array[0];
            
            WorkDetailDescripeModel *requirementModel = [[WorkDetailDescripeModel alloc]init];
            requirementModel.title = @"岗位要求:";
            requirementModel.content = array[1];
            
            WorkDetailDescripeModel *temptationModel = [[WorkDetailDescripeModel alloc]init];
            temptationModel.title = @"岗位诱惑:";
            temptationModel.content = _model.pTemptation;
            
            [weakSelf.descripeArr addObject:officeModel];
            [weakSelf.descripeArr addObject:requirementModel];
            [weakSelf.descripeArr addObject:temptationModel];
            NSArray *videoArr = [_model.cVideo componentsSeparatedByString:@","];
            weakSelf.c_Video = videoArr.count > 0 ? videoArr.firstObject : @"";
            
            /** hr联系方式 */
            NSString *u_Phone = _model.uLphone;
            if (!kStringIsEmpty(u_Phone))
            {
                NSString *hrContanct = [NSString stringWithFormat:@"联系人    ：%@\n联系邮箱：%@\n联系电话：%@",_model.uName,_model.uEmail,_model.uLphone];
                //self.completeStr = [[NSMutableAttributedString alloc]initWithString:hrContanct];
                weakSelf.completeStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self exChangefirst:hrContanct second:@""]];
                _hrContanctStr = hrContanct;
            }
            }
        }else{
            weakSelf.tableView.hidden = YES;
            weakSelf.completeStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self exChangefirst:_hrContanctStr second:@""]];
            [weakSelf.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)]];
            if ([dic[@"code"] isEqualToNumber:@-1000]) {
                [MBProgressHUD showError:@"请检查网络" toView:weakSelf.view];
            }
        }
        
        _completeBriefBOOL = YES;
        NSLog(@"%@",self.completeStr);
        if (!self.completeStr)
        {
            if (!kObjectIsEmpty([GlobalData sharedInstance].GB_UserModel.datas0.firstObject)) {
                [RequestApi getBriefCompleteWithKey:[GlobalData sharedInstance].GB_Key pid:self.pid Delegate:self success:^(NSDictionary *response) {
                    NSLog(@"%@",response);
                    if ([response[@"datas"] integerValue] >= 80) {
                        [RequestApi CompleteBriefInfoWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil success:^(NSDictionary *response) {
                            NSLog(@"%@",response);
                            weakSelf.completeStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self exChangefirst:response[@"datas"] second:response[@"messge"]]];
                            [weakSelf.tableView reloadData];
                        } failure:^(NSString *errorStr) {
                            [weakSelf.tableView reloadData];
                        } ];
                    }else{
                        [RequestApi noCompleteBriefInfoWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil success:^(NSDictionary *response) {
                            _completeBriefBOOL = NO;
                            weakSelf.completeStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self exChangefirst:response[@"datas"] second:response[@"messge"]]];
                            [weakSelf.tableView reloadData];
                        } failure:^(NSString *errorStr) {
                            [weakSelf.tableView reloadData];
                        }];
                    }
                } failure:^(NSString *str) {
                    [weakSelf.tableView reloadData];
                }];
            }else{
                [RequestApi noCompleteBriefInfoWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil success:^(NSDictionary *response) {
                    _completeBriefBOOL = NO;
                    weakSelf.completeStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self exChangefirst:response[@"datas"] second:response[@"messge"]]];
                    [weakSelf.tableView reloadData];
                } failure:^(NSString *errorStr) {
                    [weakSelf.tableView reloadData];
                }];
            }
        }else{
            [weakSelf.tableView reloadData];
        }
    }];
}

-(void)handleHideTimer
{
    [GB_Nav popViewControllerAnimated:YES];
}

-(NSMutableAttributedString *)exChangefirst:(NSString *)first second:(NSString *)second
{
    NSString *content = [NSString stringWithFormat:@"%@%@",first,second];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:F(7)];//调整行间距
    
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc]initWithString:content];
    NSRange range = [content rangeOfString:second];
    [attribute addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:F(14)] range:range];
    [attribute addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:F(14)] range:NSMakeRange(0, [first length])];
    [attribute addAttribute:NSForegroundColorAttributeName value:COLOR_LABELSIXCOLOR range:NSMakeRange(0, [first length])];
    [attribute addAttribute:NSForegroundColorAttributeName value:COLOR_MAINCOLOR range:range];
    [attribute addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [content length])];
    
    return attribute;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 1 + self.descripeArr.count ;
    }else if (section == 2){
        return 2;
    }else if (section == 3){
        return kStringIsEmpty(_c_Video) ? 0 : 2;
    }
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *vis = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(20))];
    vis.backgroundColor = COLOR_LINESCOLOR;
    return vis;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (_model == nil) {
        return 0;
    }
//    if (section == 2){
//        return kStringIsEmpty(_model.pAddress) ? 0 : 10;
//    }else
    if (section == 3){
        return kStringIsEmpty(_c_Video) ? 0 : 10;
    }
    return W(10);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *videoCell = @"video";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    UITableViewCell *cell = [UITableViewCell new];
    if (!cell)
    {
//        cell = [[UITableViewCell alloc]initWithFrame:CGRectZero];
    }
    if (_model == nil) {
        return cell;
    }
    if (indexPath.section > 0 && indexPath.row == 0) {
        WordDetailTitleCell *cell = [[WordDetailTitleCell alloc]init];
        cell.leftImage.image = [UIImage imageNamed:self.leftImageArr[indexPath.section]];
        cell.titleLabel.text = self.leftContentArr[indexPath.section];
        return cell;
    }
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"workDetail" owner:0 options:nil]lastObject];
            [cell setCellInfoWithModel:_model];
        }else if (indexPath.row == 1){
            CompanyCell *cell = [[CompanyCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
            [cell setCellInfoWithModel:_model];
            return cell;
        }
    }else if (indexPath.section == 1 && indexPath.row > 0){
        WorkDetailDescripeCell *cell = [[WorkDetailDescripeCell alloc]init];
//        cell.datasArray = self.descripeArr;
        cell.model = self.descripeArr[indexPath.row - 1];
        return cell;
    }else if (indexPath.section == 2 && indexPath.row == 1){
        CGFloat top = 15;
        if (isIphone5)
        {
            top = 12;
        }
        UILabel *label = [[UILabel alloc]init];
        label.textColor = COLOR_LABELSIXCOLOR;
        label.font = [UIFont systemFontOfSize:F(14)];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.text = kStringIsEmpty(_model.pAddress) ? _model.pCity : _model.pAddress;
        [label sizeToFit];
        [cell.contentView addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.top.mas_equalTo(top);
            make.right.mas_equalTo(-15);
            make.bottom.mas_equalTo(-top);
        }];
    } else if (indexPath.section == 3 && indexPath.row == 1)
    {
        DisplayVideoCell *cells = [[DisplayVideoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:videoCell];
        cells.videoDelegate = self;
        CGFloat top = W(15);
        if (!kStringIsEmpty(_c_Video))
        {
            cells.url = _c_Video;
            [self creatVideo:cells];
            if (!kStringIsEmpty(_model.cVcover)) {
                [_playerView.videoImageView sd_setImageWithURL:[NSURL URLWithString:_model.cVcover] placeholderImage:nil];
            }else{
//                videoHeight = 1;
            }
            [cells addSubview:_playerView];
        }else{
            top = 0;
            videoHeight = 1;
        }
        [cells.button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(-15);
            make.top.mas_equalTo(top);
            make.bottom.mas_equalTo(-top);
            make.height.mas_equalTo(videoHeight);
        }];
        return cells;
    }
    //职位发布者
    else if (indexPath.section == 4)
    {
        if (indexPath.row == 1)
        {
            NewPublisherInfoCell *cell = [NewPublisherInfoCell new];
            NSLog(@"%@",self.completeStr);

            kStringIsEmpty(_hrContanctStr) ? [cell.contentButton setAttributedTitle:self.completeStr forState:UIControlStateNormal] :  [cell.contentLabel setAttributedText:self.completeStr];
            cell.contentButton.hidden = !kStringIsEmpty(_hrContanctStr);
            cell.contentLabel.hidden  = kStringIsEmpty(_hrContanctStr);
            [cell.contentButton addTarget:self action:@selector(tapHRContact) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tapHRContact
{
    if (!_completeBriefBOOL)
    {
        for (UITabBarController * tabVC in GB_Nav.viewControllers)
        {
            NSLog(@"%@",GB_Nav.viewControllers);
            if ([tabVC isKindOfClass:[UITabBarController class]])
            {
                tabVC.selectedIndex = 3;
                [GB_Nav popToRootViewControllerAnimated:YES];
                break;
            }
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1)
    {
        CompanyInfoVC *info = [[CompanyInfoVC alloc]initWithCid:[GlobalMethod doubleToString:self.model.cID]];
        [self.navigationController pushViewController:info animated:YES];
    }
}

-(void)creatVideo:(DisplayVideoCell *)cell
{
    NSLog(@"%@",_playerView);
    
    if (!_playerView)
    {
        _playerView = [[CLPlayerView alloc] initWithFrame:CGRectMake( W(15) , W(15) , KWIDTH - W(15) * 2, videoHeight)];
        [cell addSubview:_playerView];
        _playerView.url = [NSURL URLWithString:cell.url];
        _playerView.repeatPlay = YES;
    }
    DSWeak;
    //返回按钮点击事件回调
    [self.playerView backButton:^(UIButton *button) {
        NSLog(@"返回按钮被点击");
        
        BOOL isNotWIFI = [GlobalMethod IsEnableWifi];        //是否是wifi
//        BOOL is3GNet  = [GlobalMethod IsENable3G];          //3g
        BOOL isNetWork= [GlobalMethod IsEnableNetwork];     //是否有网络
        
        if (isNotWIFI) {
            [self alertViewMessageT:button title:@"非wifi网络，是否继续播放"];
        }else{
            [weakSelf.playerView playOrPauseButton:button];
            _playerView.videoImageView.hidden = YES;
        }
        if (!isNetWork) {
            [MBProgressHUD showError:@"无网络状态" toView:self.view];
        }
    }];
    
    //播放完成回调
    [self.playerView endPlay:^{
        //销毁播放器
        //        [weakSelf.playerView destroyPlayer];
        //        weakSelf.playerView = nil;
        _playerView.videoImageView.hidden = NO;
        [weakSelf.playerView pausePlay];
        NSLog(@"播放完成");
    }];
    
}

#pragma mark 滑动暂停视频播放

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView)
    {
        NSIndexPath * indexPathVideo = [NSIndexPath indexPathForRow:1 inSection:3];
        CGRect cellVideoR = [self.tableView rectForRowAtIndexPath:indexPathVideo];
        NSLog(@"%f",cellVideoR.origin.y);
        NSLog(@"%f",cellVideoR.origin.y -self.tableView.contentOffset.y);
        if ((cellVideoR.origin.y - self.tableView.contentOffset.y) > 600 )
        {
            NSLog(@"%f",self.tableView.contentOffset.y - cellVideoR.origin.y);
            if (_playerView.startButton.selected)
            {
                [_playerView pausePlay];
            }
        }
    }
}

#pragma mark 点击底部view
/**
 *  tag == 10  投递简历   11 收藏职位
 */
-(void)protocolBottomBtnSelect:(NSUInteger)tag btn:(CustomControl *)control
{
    if (tag == 10)
    {
        if (_completeBriefBOOL == NO)
        {
            [self alertTipSContent:@"简历信息不完善，请先完善简历!" leftButtonStr:@"取消" rightButtonStr:@"前往" indexStatus:0];
        }else{
            BriefInfoModel *infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
            if (kStringIsEmpty([GlobalData sharedInstance].trend_Complete))
            {
                [self alertTipSContent:@"请前往完善职位偏好!" leftButtonStr:@"取消" rightButtonStr:@"前往" indexStatus:2];
            }else if(kStringIsEmpty([GlobalData sharedInstance].u_Email)){
                [self alertTipSContent:@"请前往完善联系信息!" leftButtonStr:@"取消" rightButtonStr:@"前往" indexStatus:3];
            }else{
                ResumeDeliveryVC *resume = [[ResumeDeliveryVC alloc]initPid:self.pid];
                [self.navigationController pushViewController:resume animated:YES];
            }
        }
    }else if (tag == 11){
        [self savePosition];
    }
}


//收藏职位
-(void)savePosition
{
    [RequestApi savePositionWithKey:[GlobalData sharedInstance].GB_Key pid:self.pid Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"收藏成功" toView:self.view];
    } failure:^(NSDictionary *response) {
        
    }];
}

-(void)alertViewMessageT:(UIButton *)button title:(NSString *)title
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.playerView playOrPauseButton:button];
        _playerView.videoImageView.hidden = YES;
    }];
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:action];
    [alert addAction:cancle];
    
    [self presentViewController:alert animated:true completion:nil];
    
}

#pragma mark 点击底部button


//温馨提示
-(void)alertTipSContent:(NSString *)content leftButtonStr:(NSString *)leftStr rightButtonStr:(NSString *)rightStr indexStatus:(NSInteger)status
{
    if (!_kyAlertView)
    {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    
    DSWeak;
    
    [_kyAlertView showAlertView:@"提示"
                        message:content
                 subBottonTitle:leftStr
              cancelButtonTitle:rightStr
            handler:^(AlertViewClickBottonType bottonType)
    {
            if (bottonType == AlertViewClickBottonTypeSubBotton) {
                
            }else if (bottonType == AlertViewClickBottonTypeCancelButton)
            {
                if (status == 0){
                    [weakSelf tapHRContact];
                }
                else if (status == 1){
                    
                }else if (status == 2){
                    PositionTrendVC *trend = [PositionTrendVC new];
                    [GB_Nav pushViewController:trend animated:YES];
                }else if (status == 3){
                    MyContactInfoVC *infoVC = [MyContactInfoVC new];
                    [GB_Nav presentViewController:infoVC animated:YES completion:nil];
                }
            }
    }];
}


-(void)dealloc{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
    _kyAlertView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
