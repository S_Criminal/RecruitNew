//
//  NewWorkDetailVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewWorkDetailVC : BasicVC
@property (nonatomic ,strong) NSString *cid;
@property (nonatomic ,strong) NSString *pid;

-(instancetype)initWithCid:(NSString *)cid pid:(NSString *)pid;

@end
