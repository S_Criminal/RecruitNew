//
//  CompanyNewsBriefCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyNewsBriefCell.h"

#import "PYPhotoBrowser.h"

@implementation CompanyNewsBriefCell
{
    CGFloat scrollViewWidth;
    CGFloat scrollViewHeight;
    NSMutableArray *imageViewArr;
}

#pragma mark 懒加载

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = true;
        _scrollView.showsVerticalScrollIndicator = false;
        _scrollView.showsHorizontalScrollIndicator = false;
    }
    return _scrollView;
}

-(UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        _pageControl.autoresizingMask = UIViewAutoresizingNone;
    }
    return _pageControl;
}

- (UILabel *)contentLabel{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel new];
        [GlobalMethod setLabel:_contentLabel widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _contentLabel;
}

#pragma mark 初始化

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier path:(NSString *)path
{
    //    CompanyStrengthDisplayCell *cell = [[CompanyStrengthDisplayCell alloc]initWithStyle:style reuseIdentifier:reuseIdentifier];
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.path = path;
        [self setUI];
        
        [self.contentView addSubview:self.scrollView];
        [self.contentView addSubview:self.pageControl];
        [self.contentView addSubview:self.contentLabel];
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    scrollViewWidth     = KWIDTH - W(15) * 2;
    scrollViewHeight    = scrollViewWidth / 4 * 3;
    
    NSArray *arr = [self.path componentsSeparatedByString:@","];
    imageViewArr = [NSMutableArray array];
    for (int i =0; i < arr.count; i ++ )
    {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(i * scrollViewWidth, 0, scrollViewWidth, scrollViewHeight)];
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage:)];
        [imageView addGestureRecognizer:tapImage];
        imageView.tag = 100 + i;
        [imageView sd_setImageWithURL:[NSURL URLWithString:arr[i]] placeholderImage:[UIImage imageNamed:@"企业默认背景"]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.layer.masksToBounds = YES;
        imageView.userInteractionEnabled = YES;
        [imageViewArr addObject:imageView];
        [self.scrollView addSubview:imageView];
    }
    
    self.pageControl.numberOfPages = arr.count;
    
    [self.pageControl setFrame:CGRectMake(0, scrollViewHeight - 15 - W(15), KWIDTH, 30)];
    NSLog(@"%@",self.pageControl);
    
    //刷新view
    _scrollView.contentSize = CGSizeMake(scrollViewWidth * arr.count, 0);
}

-(void)tapImage:(UITapGestureRecognizer *)gesture
{
    PYPhotoBrowseView *photoBroseView = [[PYPhotoBrowseView alloc] init];
    // 2.1 设置图片源(UIImageView)数组
    photoBroseView.sourceImgageViews = imageViewArr;
    // 2.2 设置初始化图片下标（即当前点击第几张图片）
    photoBroseView.currentIndex = gesture.view.tag - 100 ;
    if (photoBroseView.images.count == photoBroseView.sourceImgageViews.count)
    {
        // 3.显示(浏览)
        [photoBroseView show];
    }
}

-(void)setModel:(CompanyInfoModel *)model
{
    scrollViewWidth     = KWIDTH - W(15) * 2;
    scrollViewHeight    = scrollViewWidth / 4 * 3;
    
    NSArray *arr = [self.path componentsSeparatedByString:@","];
    
    if (arr.count == 0)
    {
        return;
    }
    
    self.pageControl.numberOfPages = arr.count;
    
    [self.pageControl setFrame:CGRectMake(0, scrollViewHeight - 15 - W(15), KWIDTH, 30)];
    self.pageControl.centerX = KWIDTH / 2.0f;
    
    self.scrollView.widthHeight = XY(scrollViewWidth, scrollViewHeight);
    
    if (kStringIsEmpty(self.path))
    {
        self.scrollView.height = 0;
    }
    
    self.scrollView.leftTop = XY(W(15),W(15));
    
    self.contentLabel.leftTop = XY(W(15), self.scrollView.bottom + W(15));
    
    self.contentLabel = [GlobalMethod setAttributeLabel:self.contentLabel content:model.cIntrodution width:scrollViewWidth];
//    [GlobalMethod resetLabel:self.contentLabel text:model.cIntrodution isWidthLimit:0];
    
    [self setupAutoHeightWithBottomView:self.contentLabel bottomMargin:W(15)];
    NSLog(@"%@",self.contentLabel);
}


#pragma mark 获取高度
FETCH_CELL_HEIGHT(CompanyNewsBriefCell)
#pragma mark 刷新cell


- (CGFloat)resetCellWithModel:(CompanyInfoModel *)model{
    
    scrollViewWidth     = KWIDTH - W(15) * 2;
    scrollViewHeight    = scrollViewWidth / 4 * 3;
    
    
    self.scrollView.widthHeight = XY(scrollViewWidth, scrollViewHeight);

    
    self.scrollView.leftTop = XY(W(15),W(15));
    
    self.contentLabel.leftTop = XY(W(15), self.scrollView.bottom + W(15));
    
    self.contentLabel = [GlobalMethod setAttributeLabel:self.contentLabel content:model.cIntrodution width:scrollViewWidth];

    NSLog(@"%@",self.contentLabel);
    return self.contentLabel.bottom + W(15);
}


@end
