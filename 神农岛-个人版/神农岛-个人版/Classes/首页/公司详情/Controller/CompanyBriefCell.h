//
//  CompanyBriefCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyInfoModel.h"
#import <WebKit/WebKit.h>



@interface CompanyBriefCell : UITableViewCell<WKNavigationDelegate,WKUIDelegate>
@property (nonatomic ,strong) CompanyInfoModel *model;

@property (nonatomic ,strong) UILabel *htmlLabel;

@property (nonatomic ,strong) WKWebView *webView;
@property (nonatomic ,assign) NSInteger webIndex ;
@property (nonatomic ,strong) void (^refreshBlock)();


#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(CompanyInfoModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(CompanyInfoModel *)model;


@end
