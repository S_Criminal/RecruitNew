//
//  CompanyPositionCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyPositionModel.h"
@interface CompanyPositionCell : UITableViewCell

@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UILabel *contentLabel;
@property (nonatomic ,strong) UILabel *timeLabel;
@property (nonatomic ,strong) UILabel *payLabel;

@property (nonatomic ,strong) UIView *line;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(CompanyPositionModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(CompanyPositionModel *)model;

@end
