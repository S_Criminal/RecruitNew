//
//  CompanyBriefCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyBriefCell.h"
#import "CustomHTMLTransformString.h"

@implementation CompanyBriefCell{
    NSString *htmlStringStr;
}

-(WKWebView *)webView
{
    if (!_webView) {
        _webView = [[WKWebView alloc]init];
        [_webView setFrame:CGRectMake(15, 10, KWIDTH, 0)];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
        _webView.userInteractionEnabled = NO;
    }
    return _webView;
}

-(UILabel *)htmlLabel
{
    if (!_htmlLabel) {
        _htmlLabel = [UILabel new];
        _htmlLabel.attributedText = [[NSAttributedString alloc]init];
        _htmlLabel.numberOfLines = 0;
    }
    return _htmlLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        [self.contentView addSubview:self.webView];
        [self.contentView addSubview:self.htmlLabel];
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(CompanyBriefCell)
#pragma mark 刷新cell

- (CGFloat)resetCellWithModel:(CompanyInfoModel *)model
{
    
    self.htmlLabel.leftTop = XY(W(15), 0);
    _model = model;
   __block CGFloat height = 100;
    
    if ([[GlobalData sharedInstance].companyBriefHTML isEqualToString:model.cIntrodutions])
    {
        self.htmlLabel.attributedText = [GlobalData sharedInstance].companyBriefHTMLAttribute;
        height = [GlobalData sharedInstance].companyBriefHeight;
        self.htmlLabel.widthHeight = XY(KWIDTH - W(15) * 2, height);
        return height;
    }
    
    DSWeak;
    
    [weakSelf performSelectorInBackground:@selector(sonThread:) withObject:@"123"];

    
    return height;
}

-(void)sonThread:(id)object
{
    self.htmlLabel.attributedText = [self getHtmlAttributeString:_model.cIntrodutions];
    [self performSelectorOnMainThread:@selector(refreshMainThread) withObject:nil waitUntilDone:false];
}

-(void)refreshMainThread
{
    CGFloat height =  [self.htmlLabel.attributedText boundingRectWithSize:CGSizeMake(KWIDTH - W(15) * 2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    self.htmlLabel.widthHeight = XY(KWIDTH - W(15) * 2, height);
    
    [GlobalData sharedInstance].companyBriefHTML    = _model.cIntrodutions;
    [GlobalData sharedInstance].companyBriefHeight  = height;
    [GlobalData sharedInstance].companyBriefHTMLAttribute = self.htmlLabel.attributedText;
    if (self.refreshBlock)
    {
        self.refreshBlock();
    }
}
/*
-(void)performSelectorOnMainThread:(SEL)aSelector withObject:(CGFloat)height waitUntilDone:(BOOL)wait
{
    height =  [self.htmlLabel.attributedText boundingRectWithSize:CGSizeMake(KWIDTH - W(15) * 2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    self.htmlLabel.widthHeight = XY(KWIDTH - W(15) * 2, height);
    
    [GlobalData sharedInstance].companyBriefHTML    = _model.cIntrodutions;
    [GlobalData sharedInstance].companyBriefHeight  = height;
    [GlobalData sharedInstance].companyBriefHTMLAttribute = self.htmlLabel.attributedText;
    //__strong __typeof(self) strongSelf = weakSelf;
    if (self.refreshBlock)
    {
        self.refreshBlock();
        
    }
}
*/


-(void)setModel:(CompanyInfoModel *)model
{
    NSLog(@"%@",model.cName);
    NSLog(@"cIntrodutions%@",model.cIntrodutions);
}

-(void)loadWebUrl:(NSString *)urls
{
    NSString *urlStr = urls;
//    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;\" name=\"viewport\" /><meta name=\"apple-mobile-web-app-capable\" content=\"yes\"><meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\"><link rel=\"stylesheet\" type=\"text/css\" /><style type=\"text/css\"> #content{color:#222222;font-size:%.fpx;}</style> <style>img{width:%.fpx !important;}</style></head><body><div id=\"content\">%@</div>",W(6),KWIDTH - W(6),urls];
    
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    NSLog(@"加载完成");
    DSWeak;
    [webView evaluateJavaScript:@"document.body.scrollHeight" completionHandler:^(id content, NSError * _Nullable error) {
        CGFloat documentHeight = [content doubleValue];
        CGRect frame = webView.frame;
        frame.size.height = documentHeight;
        webView.frame = frame;
        [weakSelf setupAutoHeightWithBottomView:webView bottomMargin:0];
        weakSelf.webIndex ++;
        
        if (webView.frame.size.height > 10) {
            if (weakSelf.webIndex == 2)
            {
                if (self.refreshBlock)
                {
                    self.refreshBlock();
                }
            }
            return ;
        }
    }];
}


-(NSAttributedString *)getHtmlAttributeString:(NSString *)htmlStr
{
    NSTimeInterval date = [NSDate timeIntervalSinceReferenceDate];
    htmlStringStr = [CustomHTMLTransformString transformHtmlString:htmlStr font:F(15) imageWidth:KWIDTH - W(15) * 2];
    NSDictionary *dic;
    NSMutableParagraphStyle * detailParagtaphStyle = [[NSMutableParagraphStyle alloc]init];
    detailParagtaphStyle.alignment = NSTextAlignmentJustified;   //设置两端对齐
    [detailParagtaphStyle setLineSpacing:F(14)];                 //行间距
    if ([htmlStringStr rangeOfString:@"http"].location != NSNotFound) {
        dic = @{
                NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone],
                NSParagraphStyleAttributeName : detailParagtaphStyle,
                NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
    }else{
        dic = @{
                NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone],
                NSParagraphStyleAttributeName : detailParagtaphStyle,
                NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
    }
    
    NSAttributedString *htmlAttribute = [[NSAttributedString alloc]initWithData:[htmlStringStr dataUsingEncoding:NSUnicodeStringEncoding] options:dic documentAttributes:nil error:nil];
    
    NSLog(@"%f",date - [NSDate timeIntervalSinceReferenceDate]);
    
    [NSDate timeIntervalSinceReferenceDate];
    NSLog(@"%@",htmlAttribute);
    
    return htmlAttribute;
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
