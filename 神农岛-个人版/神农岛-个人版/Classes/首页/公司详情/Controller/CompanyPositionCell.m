//
//  CompanyPositionCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyPositionCell.h"

@implementation CompanyPositionCell

#pragma mark 懒加载

- (UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        [GlobalMethod setLabel:_titleLabel widthLimit:0 numLines:0 fontNum:F(16) textColor:[UIColor blackColor] text:@""];
    }
    return _titleLabel;
}

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [UILabel new];
        [GlobalMethod setLabel:_timeLabel widthLimit:0 numLines:0 fontNum:F(12) textColor:[HexStringColor colorWithHexString:@"999999"] text:@""];
    }
    return _timeLabel;
}

- (UILabel *)contentLabel{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel new];
        [GlobalMethod setLabel:_contentLabel widthLimit:0 numLines:0 fontNum:F(13) textColor:[HexStringColor colorWithHexString:@"999999"] text:@""];
    }
    return _contentLabel;
}

- (UILabel *)payLabel{
    if (!_payLabel) {
        _payLabel = [UILabel new];
        [GlobalMethod setLabel:_payLabel widthLimit:0 numLines:0 fontNum:F(12) textColor:[HexStringColor colorWithHexString:@"F97230"] text:@""];
    }
    return _payLabel;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.contentLabel];
        [self.contentView addSubview:self.payLabel];
        [self.contentView addSubview:self.line];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(CompanyPositionCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(CompanyPositionModel *)model
{
    NSString *position  = model.pName;
    NSString *city      = model.pCity;
    NSString *time      = model.updateDate.length > 6 ? [[model.updateDate substringToIndex:16] stringByReplacingOccurrencesOfString:@"T" withString:@" "] : @"";
    NSString *exp       = kStringIsEmpty(model.pExp) ? @"不限" : model.pExp ;
    NSString *education = kStringIsEmpty(model.pEducation) ? @"不限" : model.pEducation;
    
    NSString *content   = [NSString stringWithFormat:@"%@/%@/%@",city,exp,education];
    
    //刷新view
    [GlobalMethod resetLabel:self.titleLabel text:position isWidthLimit:0];
    if (self.titleLabel.height > 21) {
        [GlobalMethod resetLabel:self.titleLabel text:position isWidthLimit:0];
        NSLog(@"wrong");
    }
    self.titleLabel.leftTop = XY(W(15),W(13));
//    NSLog(@"titleLabel%f",self.titleLabel.bottom);
    [GlobalMethod resetLabel:self.timeLabel text:time isWidthLimit:0];
    self.timeLabel.rightCenterY = XY(KWIDTH - W(10),self.titleLabel.centerY);
    //
    [GlobalMethod resetLabel:self.contentLabel text:content isWidthLimit:0];
    self.contentLabel.leftTop = XY(W(15),self.titleLabel.bottom + W(13));
//    NSLog(@"contentLabel%f",self.contentLabel.bottom);
    //薪资
    [GlobalMethod resetLabel:self.payLabel text:model.pPay isWidthLimit:0];
    self.payLabel.rightCenterY = XY(KWIDTH - W(10),self.contentLabel.centerY);
    
    [self.line setFrame:CGRectMake(W(15), self.contentLabel.bottom + W(10), KWIDTH - W(15), 1)];
//    NSLog(@"self.line%f",self.line.bottom);
    return self.line.bottom;
}

@end
