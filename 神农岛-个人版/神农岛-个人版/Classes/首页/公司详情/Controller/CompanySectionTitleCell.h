//
//  CompanySectionTitleCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanySectionTitleCell : UITableViewCell
@property (nonatomic ,strong) UIImageView *iconImage;
@property (nonatomic ,strong) UILabel *label;
@property (nonatomic ,strong) UIView *line;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(id)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(id)model;

-(void)setImageAndTitle:(NSString *)image title:(NSString *)title;

@end
