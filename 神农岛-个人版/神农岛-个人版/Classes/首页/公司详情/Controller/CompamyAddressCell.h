//
//  CompamyAddressCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyInfoModel.h"
@interface CompamyAddressCell : UITableViewCell
@property (nonatomic ,strong) UILabel *addressL;
@property (nonatomic ,strong) CompanyInfoModel *model;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(CompanyInfoModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(CompanyInfoModel *)model;

@end
