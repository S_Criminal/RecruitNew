//
//  CompanyWelareTable.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyWelareTable.h"
#import "RequestApi+Main.h"
#import "ComWelfareModel.h"

#import "CompanyWelareImageCell.h"

@interface CompanyWelareTable ()<UITableViewDelegate,UITableViewDataSource,RequestDelegate>
@property (nonatomic ,strong) NSArray *listImageArr;
@property (nonatomic ,strong) NSArray *listTypeArr;

@property (nonatomic ,strong) NSMutableArray *newsWelareImageArr;
@end

@implementation CompanyWelareTable

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(instancetype)initWithCid:(NSString *)cid top:(CGFloat)top
{
    CompanyWelareTable *welare = [[CompanyWelareTable alloc]init];
    welare.cid = cid;
    welare.tableTop = top;
    return welare;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setBasicView];
    [self setModel];
}

-(void)setBasicView
{
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[CompanyWelareImageCell class] forCellReuseIdentifier:@"CompanyWelareImageCell"];
}

-(void)setModel
{
    DSWeak;
    [RequestApi GetWelfareListWithCID:self.cid Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *array = response[@"datas"];
        if (array.count > 0) {
            ComWelfareModel *model = [ComWelfareModel modelObjectWithDictionary:[array objectAtIndex:0]];
            weakSelf.listTypeArr = [model.cWType componentsSeparatedByString:@","];
            weakSelf.listImageArr= [model.cWPath componentsSeparatedByString:@","];
            [weakSelf createWelareImageArr];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSDictionary *response) {
        
    }];
}

-(void)createWelareImageArr{
    _newsWelareImageArr = [NSMutableArray array];
    int i ;
    for (i = 0; i < self.listImageArr.count; i++)
    {
        UIImageView *imageView = [[UIImageView alloc]init];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_listImageArr[i]]] placeholderImage:[UIImage imageNamed:@"企业默认背景"]];
        imageView.image = [GlobalMethod thumbnailWithImageWithoutScale:imageView.image size:CGSizeMake(KWIDTH - W(15) * 2, imageView.image.size.height* (KWIDTH - W(15) *2) / imageView.image.size.width)];

        [_newsWelareImageArr addObject:imageView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _newsWelareImageArr.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h;
    if (indexPath.row != 0) {
        return [CompanyWelareImageCell fetchHeight:self.listImageArr[indexPath.row - 1]];
    }
    return h;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    if (indexPath.row != 0)
    {
        CompanyWelareImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanyWelareImageCell"];
        [cell resetCellWithModel:self.listImageArr[indexPath.row - 1]];
        return cell;
    }
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= 0) {
        //x向上
        UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
        if (sc.contentOffset.y <= self.tableTop) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }else{
        UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
        if (scrollView.contentOffset.y <= 0 && sc.contentOffset.y > 0 ) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
