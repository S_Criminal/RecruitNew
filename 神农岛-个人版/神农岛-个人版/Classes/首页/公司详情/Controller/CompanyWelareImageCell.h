//
//  CompanyWelareImageCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyWelareImageCell : UITableViewCell

@property (nonatomic ,strong) UIImageView *imgView;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(NSString *)str;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(NSString *)str;


@end
