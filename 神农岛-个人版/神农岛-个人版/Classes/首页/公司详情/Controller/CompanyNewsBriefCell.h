//
//  CompanyNewsBriefCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyInfoModel.h"



@interface CompanyNewsBriefCell : UITableViewCell<UIScrollViewDelegate>
@property (nonatomic ,strong) UIScrollView * scrollView;
@property (nonatomic ,strong) UIPageControl *pageControl;

@property (nonatomic ,strong) UILabel *contentLabel;

@property (nonatomic ,strong) NSString  *path;
@property (nonatomic ,strong) CompanyInfoModel *model;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier path:(NSString *)path;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(CompanyInfoModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(CompanyInfoModel *)model;

@end
