//
//  CompanyPositionVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyPositionVC : UIViewController
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSString *cid;
@property (nonatomic ,assign) CGFloat tableTop;
-(instancetype)initWithCid:(NSString *)cid top:(CGFloat)top;

@end
