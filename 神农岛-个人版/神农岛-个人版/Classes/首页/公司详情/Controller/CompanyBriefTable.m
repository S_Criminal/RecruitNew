//
//  CompanyBriefTable.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyBriefTable.h"
#import "CompanySectionTitleCell.h"
#import "CompanyBriefCell.h"                //公司简介  html
#import "CompanyNewsBriefCell.h"            //公司简介
#import "CompamyAddressCell.h"              //公司地址
#import "CompanyStrengthDisplayCell.h"      //实力展示

#import "PYPhotoBrowser.h"
@interface CompanyBriefTable ()

@property (nonatomic ,strong) NSArray *imageArr;
@property (nonatomic ,strong) NSArray *titleArr;

@property (nonatomic ,assign) BOOL isHTMLDisplay;   //是否显示HTML

@end

@implementation CompanyBriefTable

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT - NAVIGATION_BarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _tableView;
}

-(instancetype)initStatus:(CompanyInfoModel *)model top:(CGFloat)top
{
    CompanyBriefTable *brief = [CompanyBriefTable new];
    brief.model = model;
    brief.tableTop = top;
    
    return brief;
}


-(void)setBlock3Method:(void(^)(NSString *s))block3{
    
}
                        

- (void)viewDidLoad
{
    
    self.myBlock = ^(int i){
        return [NSString stringWithFormat:@"2"];
    };
    
    void(^block2)(NSString *str) = ^void(NSString *str2){
        
    };
    block2(@"");
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setModel];
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[CompanySectionTitleCell class] forCellReuseIdentifier:@"CompanySectionTitleCell"];
    [self.tableView registerClass:[CompanyBriefCell class] forCellReuseIdentifier:@"CompanyBriefCell"];
    [self.tableView registerClass:[CompanyNewsBriefCell class] forCellReuseIdentifier:@"CompanyNewsBriefCell"];
    [self.tableView registerClass:[CompamyAddressCell class] forCellReuseIdentifier:@"CompamyAddressCell"];
    [self.tableView registerClass:[CompanyStrengthDisplayCell class] forCellReuseIdentifier:@"CompanyStrengthDisplayCell"];
}

-(void)setModel
{
    self.imageArr = [NSArray arrayWithObjects:@"公司简介",@"公司地址",@"公司简介", nil];
    self.titleArr = [NSArray arrayWithObjects:@"公司简介",@"公司地址",@"实力展示", nil];
    self.isHTMLDisplay = YES;
    if (kStringIsEmpty(self.model.cIntrodutions))
    {
        self.isHTMLDisplay = NO;
    }
    /*
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 处理耗时操作的代码块...
       [GlobalData sharedInstance].companyBriefHTMLAttribute  = [GlobalMethod getHtmlAttributeString:_model.cIntrodutions];
        //通知主线程刷新
        dispatch_async(dispatch_get_main_queue(), ^{
            //回调或者说是通知主线程刷新，
            [GlobalData sharedInstance].companyBriefHTML    = _model.cIntrodutions;
            [self.tableView reloadData];
        });
    });
    */
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (kStringIsEmpty(_model.cPaths) && section == 2)
    {
        return 0;
    }
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return W(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(10))];
    view.backgroundColor = COLOR_LINESCOLOR;
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 44;
    }
    CGFloat h = 0;
    if (indexPath.section == 0) {
        h = self.isHTMLDisplay ? [CompanyBriefCell fetchHeight:_model] : [CompanyNewsBriefCell fetchHeight:_model];
//        h = self.isHTMLDisplay ? [CompanyBriefCell fetchHeight:_model] : [tableView cellHeightForIndexPath:indexPath model:_model keyPath:@"model" cellClass:[CompanyNewsBriefCell class] contentViewWidth:KWIDTH];
    }else if (indexPath.section == 1){
        h = [CompamyAddressCell fetchHeight:_model];
    }else if (indexPath.section == 2){
        h = [tableView cellHeightForIndexPath:indexPath model:_model keyPath:@"model" cellClass:[CompanyStrengthDisplayCell class] contentViewWidth:KWIDTH];
    }
    return h;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *identifier = @"CompanySectionTitleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanySectionTitleCell"];
    if (indexPath.row == 0) {
       CompanySectionTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanySectionTitleCell"];
        [cell setImageAndTitle:self.imageArr[indexPath.section] title:self.titleArr[indexPath.section]];
        return cell;
    }
    if (indexPath.section == 0) {
        if (self.isHTMLDisplay) {
            CompanyBriefCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanyBriefCell"];
            [cell resetCellWithModel:_model];
            DSWeak;
            cell.refreshBlock = ^{
                [weakSelf refreshTable];
            };
            return cell;
        }else{
            CompanyNewsBriefCell *cell = [[CompanyNewsBriefCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CompanyNewsBriefCell" path:_model.cPath];
            [cell resetCellWithModel:_model];
            return cell;
        }
    }else if (indexPath.section == 1){
       CompamyAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompamyAddressCell"];
        [cell resetCellWithModel:_model];
        return cell;
    }else if (indexPath.section == 2){
        CompanyStrengthDisplayCell * cell = [[CompanyStrengthDisplayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CompanyStrengthDisplayCell" path:_model.cPaths];
        cell.model = _model;
        return cell;
    }
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= 0) {
        //x向上
        UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
        if (sc.contentOffset.y <= self.tableTop) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }else{
        UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
        if (scrollView.contentOffset.y <= 0 && sc.contentOffset.y > 0 ) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
}

-(void)refreshTable
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
