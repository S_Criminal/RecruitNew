//
//  CompanyWelareImageCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyWelareImageCell.h"

@implementation CompanyWelareImageCell

#pragma mark 懒加载

- (UIImageView *)imgView{
    if (_imgView == nil) {
        _imgView = [UIImageView new];
        _imgView.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _imgView.widthHeight = XY(W(15),W(15));
    }
    return _imgView;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.imgView];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(CompanyWelareImageCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(NSString *)str{
    
    //刷新view
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",str]] placeholderImage:[UIImage imageNamed:@"企业默认背景"]];
    self.imgView.contentMode = UIViewContentModeScaleAspectFill;
    self.imgView.layer.masksToBounds = YES;
    self.imgView.leftTop = XY(W(15),W(15));
    self.imgView.widthHeight = XY(KWIDTH - W(15) *2, self.imgView.image.size.height* (KWIDTH - W(15) *2)/self.imgView.image.size.width);

    return self.imgView.bottom ;
}

@end
