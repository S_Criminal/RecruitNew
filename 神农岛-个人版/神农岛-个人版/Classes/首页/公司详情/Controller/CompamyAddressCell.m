//
//  CompamyAddressCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompamyAddressCell.h"

@implementation CompamyAddressCell


#pragma mark 懒加载

- (UILabel *)addressL{
    if (_addressL == nil) {
        _addressL = [UILabel new];
        [GlobalMethod setLabel:_addressL widthLimit:0 numLines:2 fontNum:F(15) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _addressL;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.addressL];
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(CompamyAddressCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(CompanyInfoModel *)model
{
//    [GlobalMethod removeAllSubView:self withTag:TAG_LINE];//移除线
    //刷新view
//    [GlobalMethod setLabel:self.addressL widthLimit:KWIDTH - W(15) * 2 numLines:0 fontNum:F(13) textColor:COLOR_LABELSIXCOLOR text:model.cAddress];
    [GlobalMethod setAttributeLabel:self.addressL content:model.cAddress width:KWIDTH - W(15) * 2];
    self.addressL.leftTop = XY(W(15),W(12));
    
    return self.addressL.bottom + W(12);
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
