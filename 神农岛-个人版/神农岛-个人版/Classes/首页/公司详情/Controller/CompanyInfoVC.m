//
//  CompanyInfoVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "CompanyInfoVC.h"
#import "CompanyInfoModel.h"
#import "CompanyView.h"
#import "RequestApi+Main.h"
/** 自控制器 */
#import "CompanyBriefTable.h"   //公司首页
#import "CompanyPositionVC.h"   //招聘职位
#import "CompanyWelareTable.h"  //福利待遇

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDKUI.h>
// 自定义分享菜单栏需要导入的头文件
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
#import "CustomShareUI.h"       //自定义分享

@interface CompanyInfoVC ()<RequestDelegate,UIScrollViewDelegate>

@property(strong,nonatomic)UISegmentedControl *segmentControl;
@property(strong,nonatomic)UITableView *mainTable;
@property(strong,nonatomic)UITableView *moneyTable;
@property(strong,nonatomic)UITableView *positionTable;
@property (nonatomic ,strong) CompanyInfoModel *model;

@property (nonatomic ,strong) UIScrollView *v_ScrollView;
@property (nonatomic ,strong) UIScrollView *h_ScrollView;

@property (nonatomic ,strong) CompanyView *headerView;

@property (nonatomic ,strong) CompanyBriefTable *first;
@property (nonatomic ,strong) CompanyPositionVC *second;
@property (nonatomic ,strong) CompanyWelareTable *third;

@end

@implementation CompanyInfoVC{
    CGFloat headViewToTop;
    CGFloat segmentLeftConstraint;
    CGFloat segmentHeight;
    CGFloat headViewHeight;
}

-(UIScrollView *)v_ScrollView
{
    if (!_v_ScrollView) {
        _v_ScrollView = [UIScrollView new];
        _v_ScrollView.delegate = self;
    //    _v_ScrollView.contentSize = CGSizeMake(0, KHEIGHT - NAVIGATION_BarHeight);
        _v_ScrollView.contentSize = CGSizeMake(0, 1000);

        _v_ScrollView.backgroundColor = [UIColor clearColor];
        _v_ScrollView.pagingEnabled = false;
        _v_ScrollView.showsVerticalScrollIndicator = false;
        _v_ScrollView.showsHorizontalScrollIndicator = false;
    }
    return _v_ScrollView;
}

-(UIScrollView *)h_ScrollView
{
    if (!_h_ScrollView) {
        _h_ScrollView = [UIScrollView new];
        _h_ScrollView.delegate = self;
        _h_ScrollView.contentSize = CGSizeMake(KWIDTH, 0);
        _h_ScrollView.backgroundColor = [UIColor clearColor];
        _h_ScrollView.pagingEnabled = true;
        _h_ScrollView.showsVerticalScrollIndicator = false;
        _h_ScrollView.showsHorizontalScrollIndicator = false;
    }
    return _h_ScrollView;
}

-(instancetype)initWithCid:(NSString *)cid
{
    CompanyInfoVC *info = [CompanyInfoVC new];
    info.cid = cid;
    return info;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self setUI];
    [self setSegment];
    [self setModel];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.segmentControl selectedSegmentIndex] != 0) {
        self.h_ScrollView.contentOffset = CGPointMake(KWIDTH * [self.segmentControl selectedSegmentIndex], 0);
    }
    
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"公司详情" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"分享" rightBlock:^{
        [weakSelf setShare];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
    //v_ScrollView 的frame
    [self.v_ScrollView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT)];
    [self.view addSubview:self.v_ScrollView];
}

-(void)setShare
{
    NSString *content   =[NSString stringWithFormat:@"%@-招兵买马了！高薪诚聘！-老刀招聘APP",_model.cName];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@share-company.html?id=%@",shareHttp,[GlobalMethod doubleToString:_model.iDProperty]];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSString *titleFriend = [NSString stringWithFormat:@"招聘 | %@",_model.cName];//朋友圈
    CustomShareUI *share = [[CustomShareUI alloc]initShareUIContent:content title:titleFriend url:url images:self.model.cLogo shareEnum:shareContentsWithCompany];
    share.companyName = _model.cName;

    [self.view addSubview:share];
}

-(void)setUI
{
    if (isIphone5) {

    }else if (isIphone6){

    }
}

-(void)setSegment
{
    self.segmentControl = [[UISegmentedControl alloc]initWithItems:nil];
    [self.segmentControl setFrame:CGRectMake(W(16), W(95), KWIDTH - W(16) * 2 ,W(30))];
    [self.segmentControl insertSegmentWithTitle:@"公司首页" atIndex: 0 animated: NO ];
    [self.segmentControl insertSegmentWithTitle:@"招聘职位" atIndex: 1 animated: NO ];
    [self.segmentControl insertSegmentWithTitle:@"福利待遇" atIndex:2 animated:NO];
    self.segmentControl.selectedSegmentIndex = 0;
    self.segmentControl.tintColor = COLOR_MAINCOLOR;
}

-(void)setBasicView
{
    _headerView = [[[NSBundle mainBundle]loadNibNamed:@"CompanyView" owner:nil options:nil]lastObject];
    _headerView.model = _model;
    [_headerView setFrame:CGRectMake(0, 0, KWIDTH, W(135))];
    [self.segmentControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    [_headerView addSubview:self.segmentControl];

    [self.v_ScrollView addSubview:_headerView];
    [self.v_ScrollView addSubview:self.h_ScrollView];
    [self.h_ScrollView setFrame:CGRectMake(0, _headerView.height, KWIDTH, self.v_ScrollView.height - W(10))];
}

#pragma mark 初始化自控制器

- (void)setupChildVC
{
    UIView *line = [GlobalMethod addNavLineFrame:CGRectMake(0, 0, KWIDTH * 3, W(10))];
    [self.h_ScrollView addSubview:line];
    CGFloat top = _headerView.height;
    
    _first = [[CompanyBriefTable alloc]initStatus:_model top:top];
    _first.view.frame = CGRectMake(0, line.bottom, KWIDTH, self.h_ScrollView.height) ;
    _first.tableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - NAVIGATION_BarHeight) ;
    
    _second = [[CompanyPositionVC alloc]initWithCid:self.cid top:top];
    _second.view.frame = CGRectMake(KWIDTH, line .bottom, KWIDTH, KHEIGHT) ;
    _second.tableView.frame = CGRectMake(0, 0, KWIDTH,KHEIGHT - NAVIGATION_BarHeight) ;
    
    _third = [[CompanyWelareTable alloc]initWithCid:self.cid top:top];
    _third.view.frame = CGRectMake(KWIDTH * 2, line .bottom, KWIDTH, KHEIGHT) ;
    _third.tableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - NAVIGATION_BarHeight) ;
    
    [self addChildViewController:_first];
    [self addChildViewController:_second];
    [self addChildViewController:_third];
    
    [self.h_ScrollView addSubview:_first.view];
    [self.h_ScrollView addSubview:_second.view];
    [self.h_ScrollView addSubview:_third.view];
    
}


-(void)setModel
{
    DSWeak;
    [RequestApi GetCompanyInfoWithCID:self.cid Delegate:self success:^(NSDictionary *response) {
        weakSelf.model = [CompanyInfoModel modelObjectWithDictionary:response];
//        [self.mainTable reloadData];
//        [self createHeaderView];
        [weakSelf setBasicView];
        [weakSelf setupChildVC];
    }];
}

-(void)segmentAction:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        self.h_ScrollView.contentOffset = CGPointMake(KWIDTH * [sender selectedSegmentIndex], 0);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
