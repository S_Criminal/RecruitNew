//
//  CompanyBriefTable.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyInfoModel.h"

typedef void(^SuccessBlock)(NSString *str);

@interface CompanyBriefTable : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,assign) CGFloat tableTop;
@property (nonatomic ,strong) CompanyInfoModel *model;
-(instancetype)initStatus:(CompanyInfoModel *)model top:(CGFloat)top;




@property (nonatomic ,strong) SuccessBlock block;

@property (nonatomic ,strong) void(^FaiureBlock)(NSDictionary *dic);
@property (nonatomic ,strong) NSString *(^myBlock)(int);
- (void)yourMethod:(void (^)(NSString *str))blockName;


@end
