//
//  CompanySectionTitleCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanySectionTitleCell.h"

@implementation CompanySectionTitleCell

- (UIImageView *)iconImage{
    if (_iconImage == nil) {
        _iconImage = [UIImageView new];
        _iconImage.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _iconImage.widthHeight = XY(16,16);
    }
    return _iconImage;
}

- (UILabel *)label{
    if (_label == nil) {
        _label = [UILabel new];
        [GlobalMethod setLabel:_label widthLimit:0 numLines:0 fontNum:F(16) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _label;
}

- (UIView *)line{
    if (_line == nil) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}


#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.iconImage];
        [self.contentView addSubview:self.label];
        [self.contentView addSubview:self.line];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(CompanySectionTitleCell)

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(id)model{
//    [GlobalMethod removeAllSubView:self withTag:TAG_LINE];//移除线
    //刷新view
    self.iconImage.leftTop = XY(KWIDTH,W(0));
    self.iconImage.image = [UIImage imageNamed:@""];
    [GlobalMethod resetLabel:self.label text:@"" isWidthLimit:NO];
    self.label.leftTop = XY(KWIDTH,W(0));
    
    self.line.frame = CGRectMake(W(0), W(0), KWIDTH, 1);
    
    
    
    return self.bottom;
}

-(void)setImageAndTitle:(NSString *)image title:(NSString *)title
{
    self.iconImage.leftTop = XY(15,W(0));
    [GlobalMethod resetLabel:self.label text:title isWidthLimit:NO];
    self.label.leftTop = XY(_iconImage.right + W(10),W(0));
    
    self.iconImage.image = [UIImage imageNamed:image];
//    self.label.text = title;
//    [self.label sizeToFit];
    self.iconImage.centerY = 44 / 2;
    self.label.centerY =  44 / 2;
    self.line.frame = CGRectMake(15, 43, KWIDTH, 1);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
