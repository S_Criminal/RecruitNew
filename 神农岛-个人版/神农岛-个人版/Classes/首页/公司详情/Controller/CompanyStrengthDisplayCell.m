//
//  CompanyStrengthDisplayCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyStrengthDisplayCell.h"

#import "PYPhotoBrowser.h"

@implementation CompanyStrengthDisplayCell{
    CGFloat scrollViewWidth     ;
    CGFloat scrollViewHeight;
    NSMutableArray *imageViewArr;
}

#pragma mark 懒加载

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = true;
        _scrollView.showsVerticalScrollIndicator = false;
        _scrollView.showsHorizontalScrollIndicator = false;
    }
    return _scrollView;
}

-(UIPageControl *)pageControl
{
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        [_pageControl setFrame:CGRectMake(0, 0, KWIDTH, 30)];
        _pageControl.autoresizingMask = UIViewAutoresizingNone;
    }
    return _pageControl;
}

#pragma mark 初始化

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier path:(NSString *)path
{
//    CompanyStrengthDisplayCell *cell = [[CompanyStrengthDisplayCell alloc]initWithStyle:style reuseIdentifier:reuseIdentifier];
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.path = path;
        [self setUI];
        
        [self.contentView addSubview:self.scrollView];
        [self.contentView addSubview:self.pageControl];
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    
    scrollViewWidth     = KWIDTH - W(15) * 2;
    scrollViewHeight    = scrollViewWidth / 4 * 3;
    
    NSArray *arr = [self.path componentsSeparatedByString:@","];
    imageViewArr = [NSMutableArray array];
    for (int i =0; i < arr.count; i ++ )
    {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(i * scrollViewWidth, 0, scrollViewWidth, scrollViewHeight)];
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage:)];
        [imageView addGestureRecognizer:tapImage];
        imageView.tag = 100 + i;
        [imageView sd_setImageWithURL:[NSURL URLWithString:arr[i]] placeholderImage:[UIImage imageNamed:@"企业默认背景"]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.layer.masksToBounds = YES;
        imageView.userInteractionEnabled = YES;
        [imageViewArr addObject:imageView];
        [self.scrollView addSubview:imageView];
    }
    
    self.pageControl.numberOfPages = arr.count;
    
      NSLog(@"%@",self.pageControl);
    
    //刷新view
    
    _scrollView.contentSize = CGSizeMake(scrollViewWidth * arr.count, 0);
    
}

-(void)tapImage:(UITapGestureRecognizer *)gesture
{
    PYPhotoBrowseView *photoBroseView = [[PYPhotoBrowseView alloc] init];
    // 2.1 设置图片源(UIImageView)数组
    photoBroseView.sourceImgageViews = imageViewArr;
    // 2.2 设置初始化图片下标（即当前点击第几张图片）
    photoBroseView.currentIndex = gesture.view.tag - 100 ;
    if (photoBroseView.images.count == photoBroseView.sourceImgageViews.count)
    {
        // 3.显示(浏览)
        [photoBroseView show];
    }
}

-(void)setModel:(CompanyInfoModel *)model
{
    scrollViewWidth     = KWIDTH - W(15) * 2;
    scrollViewHeight    = scrollViewWidth / 4 * 3;
    
    NSArray *arr = [self.path componentsSeparatedByString:@","];
    
    self.pageControl.numberOfPages = arr.count;
    [self.pageControl setFrame:CGRectMake(0, scrollViewHeight - 15 - W(15), KWIDTH, 30)];
    self.pageControl.centerX = KWIDTH / 2;
    
    self.scrollView.widthHeight = XY(scrollViewWidth, scrollViewHeight);
    self.scrollView.leftTop = XY(W(15),W(15));
    
    [self setupAutoHeightWithBottomView:self.scrollView bottomMargin:W(15)];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.scrollView)
    {
        int page = self.scrollView.contentOffset.x / scrollViewWidth;
        self.pageControl.currentPage = page;
    }
}


@end
