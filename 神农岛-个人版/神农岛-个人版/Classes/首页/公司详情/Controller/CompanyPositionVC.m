//
//  CompanyPositionVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CompanyPositionVC.h"

#import "RequestApi+Main.h"

#import "CompanyPositionModel.h"
#import "CompanyPositionCell.h"

#import "NewWorkDetailVC.h"
#import "CompanyInfoVC.h"

@interface CompanyPositionVC ()<UITableViewDelegate,UITableViewDataSource,RequestDelegate>
@property (nonatomic ,strong) NSString *pid;
@property (nonatomic ,assign) CGFloat pageIndex;
@property (nonatomic ,strong) NSMutableArray *listArr;

@end

@implementation CompanyPositionVC

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(instancetype)initWithCid:(NSString *)cid top:(CGFloat)top
{
    CompanyPositionVC *position = [[CompanyPositionVC alloc]init];
    position.cid = cid;
    position.tableTop = top;
    return position;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _pageIndex = 1;
    [self.view addSubview:self.tableView];
    [self setFefresh];
    [self.tableView registerClass:[CompanyPositionCell class] forCellReuseIdentifier:@"CompanyPositionCell"];
    [self setModelWithPageIndex:_pageIndex];
}

#pragma mark 刷新 加载更多
-(void)setFefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
    //    [self.firstTableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    //    [header beginRefreshing];
    
    // 设置header
    self.tableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
//    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    //    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    //    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    _pageIndex = 1;
    [self setModelWithPageIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelWithPageIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
}

-(void)setModelWithPageIndex:(NSInteger)pageIndex
{
    if (pageIndex == 1)
    {
        self.pid = @"0";
        if (self.listArr) {
            [self.listArr removeAllObjects];
            self.listArr = nil;
        }
        self.listArr = [NSMutableArray array];
    }
    DSWeak;
    [RequestApi GetPositionWithCID:self.cid top:@"20" positiontype:@"" pid:self.pid Delegate:self success:^(NSDictionary *response) {
        if ([response[@"code"]isEqualToNumber:@200])
        {
            NSArray *datasArr = response[@"datas"];
            if (datasArr.count > 0)
            {
                for (NSDictionary *d in datasArr)
                {
                    NSLog(@"%@",d);
                    CompanyPositionModel *model = [CompanyPositionModel modelObjectWithDictionary:d];
                    [weakSelf.listArr addObject:model];
                }
                CompanyPositionModel *model = self.listArr.lastObject;
                weakSelf.pid = [NSString stringWithFormat:@"%@",[GlobalMethod doubleToString:model.row]];
            }
            if (weakSelf.listArr.count % 20 != 0)
            {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [weakSelf.tableView.mj_footer endRefreshing];
            }
            NSLog(@"listArr%@",self.listArr);
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSDictionary *response) {
        
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listArr.count == 0) {
        return 0;
    }
    NSLog(@"%f",[CompanyPositionCell fetchHeight:_listArr[indexPath.row]]);
    
    return [CompanyPositionCell fetchHeight:_listArr[indexPath.row]];
}

-(CompanyPositionCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CompanyPositionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanyPositionCell"];
    
    if (self.listArr.count == 0)
    {
        return cell;
    }
    [cell resetCellWithModel:_listArr[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listArr.count > 0) {
        CompanyPositionModel *model = _listArr[indexPath.row];
        NewWorkDetailVC *detail = [[NewWorkDetailVC alloc]initWithCid:[GlobalMethod doubleToString:model.cID] pid:[GlobalMethod doubleToString:model.iDProperty]];
        [GB_Nav pushViewController:detail animated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= 0) {
        //x向上
        UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
        if (sc.contentOffset.y <= self.tableTop) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }else{
        UIScrollView * sc = (UIScrollView *)scrollView.superview.superview.superview;
        if (scrollView.contentOffset.y <= 0 && sc.contentOffset.y > 0 ) {
            sc.contentOffset = CGPointMake(sc.contentOffset.x, sc.contentOffset.y + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
