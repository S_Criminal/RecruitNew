//
//  companyView.m
//  神农岛
//
//  Created by 宋晨光 on 16/4/1.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "CompanyView.h"

#import "UIImageView+WebCache.h"
@interface CompanyView ()
@property (weak, nonatomic) IBOutlet UIImageView *companyLogo;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *provienceL;
@property (weak, nonatomic) IBOutlet UILabel *cityL;
@property (weak, nonatomic) IBOutlet UILabel *temptationL;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconWidthConstraint;
//城市到公司间的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityToCompanyConstraint;



@end

@implementation CompanyView

-(void)awakeFromNib{
    [super awakeFromNib];
    if (isIphone5)
    {
        _iconToLeftConstraint.constant = 15;
        _iconToTopConstraint.constant = 15;
        _iconWidthConstraint.constant = 55;
        _iconHeightConstraint.constant = 55;
        self.companyName.font = [UIFont systemFontOfSize:15];
        self.cityToCompanyConstraint.constant = 4;
    }else if (isIphone6){
        _iconToTopConstraint.constant = 20;
    }
    [self.companyLogo setCorner:4];
}

-(void)setModel:(CompanyInfoModel *)model
{
    NSURL *url = [NSURL URLWithString:model.cLogo];
    [self.companyLogo sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"矢量智能对象"] options:0];
    self.companyName.text = model.cName;
    self.provienceL.text = model.cProvince;
    self.cityL.text = model.cCity;
    self.temptationL.text = [NSString stringWithFormat:@"%@/%@",model.wName,model.cScale];
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
//    UISegmentedControl *seg
}


@end
