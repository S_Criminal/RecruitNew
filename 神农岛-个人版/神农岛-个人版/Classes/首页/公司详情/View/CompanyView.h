//
//  companyView.h
//  神农岛
//
//  Created by 宋晨光 on 16/4/1.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyInfoModel.h"
@interface CompanyView : UIView
@property (nonatomic ,strong) CompanyInfoModel *model;
@end
