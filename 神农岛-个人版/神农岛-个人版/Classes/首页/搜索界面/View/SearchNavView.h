//
//  SearchNavView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface SearchNavView : UIView
@property (nonatomic ,strong) UITextField *searchT;
@property (nonatomic ,strong) UIButton *xButton;

@property (nonatomic, strong) void (^popBlock)();
@property (nonatomic, strong) void (^removeCellBlock)();
//@property (nonatomic ,strong)
@end
