//
//  NewCompanySearchView.m
//  神农岛
//
//  Created by 宋晨光 on 16/10/6.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "NewCompanySearchView.h"

@implementation NewCompanySearchView

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:self.frame];
        _bgView.y = 0;
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)setButtonArray:(NSArray *)array
{
//    if (kArrayIsEmpty(array)) {
//        return;
//    }
    if (_bgView) {
        [_bgView  removeFromSuperview];
        _bgView = nil;
    }
    [self addSubview:self.bgView];
    [self createViews:array];
}

-(void)createViews:(NSArray *)array
{
    CGFloat font = 15;
    CGFloat judgeFont = 12;
    CGFloat buttonHeight = 30;
    CGFloat left = 0;
    if (KWIDTH == 320) {
        font = 12;
        judgeFont = 8;
        buttonHeight = 20;
    }
    
    CGFloat w = 0;//保存前一个button的宽以及前一个button距离屏幕边缘的距离
    CGFloat h = 0;//用来控制button距离父视图的高
    for (int i = 0; i < array.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.tag = 100 + i;
        [button addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:[HexStringColor colorWithHexString:@"666666"] forState:UIControlStateNormal];
        button.layer.cornerRadius = 4;
        button.layer.masksToBounds = YES;
        button.layer.borderWidth = 1;
        button.layer.borderColor = COLOR_LINESCOLOR.CGColor;
        button.titleLabel.font = [UIFont systemFontOfSize:font];
        //根据计算文字的大小
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:judgeFont]};
        CGFloat length = [array[i] boundingRectWithSize:CGSizeMake(KWIDTH, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        //为button赋值
        [button setTitle:array[i] forState:UIControlStateNormal];
        //设置button的frame

        button.frame = CGRectMake(15 + w - left, h, length + 29 , buttonHeight);
        
        //当button的位置超出屏幕边缘时换行 320 只是button所在父视图的宽度
        left = 5;   
        if(15 + w + length + 15 > KWIDTH - 10){
            w = 0; //换行时将w置为0
            h = h + button.frame.size.height + 10;//距离父视图也变化
            button.frame = CGRectMake(15 + w, h, length + 29, buttonHeight);//重设button的frame
        }
        w = button.frame.size.width + button.frame.origin.x;
        [self.bgView addSubview:button];
        
    }
}

-(void)handleClick:(UIButton *)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectSeachTitle:)])
    {
        [self.delegate selectSeachTitle:sender.titleLabel.text];
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
