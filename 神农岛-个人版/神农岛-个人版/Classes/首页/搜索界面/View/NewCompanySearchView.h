//
//  NewCompanySearchView.h
//  神农岛
//
//  Created by 宋晨光 on 16/10/6.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol selectSearchDelegate <NSObject>

-(void)selectSeachTitle:(NSString *)title;

@end

@interface NewCompanySearchView : UIView

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,weak) id<selectSearchDelegate>delegate;

-(void)setButtonArray:(NSArray *)array;

@end
