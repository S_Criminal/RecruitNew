//
//  SearchNavView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "SearchNavView.h"
#import "CustomTextField.h"
@implementation SearchNavView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}

-(void)createView
{
    UIView *vi = [[UIView alloc]init];
    vi.backgroundColor = [UIColor whiteColor];
        
    //textField的设置
    _searchT = [[CustomTextField alloc]init];
    _searchT.tag = 1001;
//    _searchT.delegate = self;
    NSString * search = @"请输入职位名称或公司名";
    CGFloat leftConstraint = 34;
    CGFloat font = 14;
    CGFloat searchWidthFont = 250;
    CGFloat reduceFont ;
    if (isIphone5) {
        leftConstraint = 30;
        font = 13;
        searchWidthFont = 170;
        reduceFont = 5;
    }else if (isIphone6){
        searchWidthFont = 220;
    }
    
    _searchT.placeholder =search ;
    _searchT.returnKeyType = UIReturnKeySearch;// return键 标有search的搜索
    [_searchT setValue:[UIColor grayColor]forKeyPath:@"_placeholderLabel.textColor"];//设置placrholder的字体颜色
    [_searchT setValue:[UIFont boldSystemFontOfSize:12]forKeyPath:@"_placeholderLabel.font"];//设置placrholder的字体大小
    
    _searchT.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, leftConstraint, 0)];
    UIImageView *searchImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, -7, 14, 14)];
    searchImage.image = [UIImage imageNamed:@"icon_search"];
    _searchT.layer.cornerRadius = 16;
    [_searchT.leftView addSubview:searchImage];
    _searchT.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;//内容居中
    
    _searchT.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _searchT.leftViewMode = UITextFieldViewModeAlways;
    _searchT.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _searchT.font = [UIFont systemFontOfSize:font];
    
    
    UIButton *rightB = [[UIButton alloc]init];
    rightB.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [rightB setTitle:@"取消" forState:UIControlStateNormal];
    [rightB setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightB addTarget:self action:@selector(cancle) forControlEvents:UIControlEventTouchUpInside];
    
    _xButton  = [UIButton new];
    [_xButton setImage:[UIImage imageNamed:@"gray-x"] forState:UIControlStateNormal];
    [_xButton addTarget:self action:@selector(tapRemoveSearchtText) forControlEvents:UIControlEventTouchUpInside];
    _xButton.hidden = YES;
    
    [vi addSubview:_searchT];
    [vi addSubview:rightB];
    [vi addSubview:_xButton];
    [self addSubview:vi];
    
    [vi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.mas_equalTo(0);
        make.height.mas_equalTo(64);
    }];
    
    [_searchT mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(25);
        make.width.mas_equalTo(searchWidthFont);
        make.height.mas_equalTo(30);
    }];
    
    [_xButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_searchT.mas_right).offset(-15);
        make.centerY.mas_equalTo(_searchT.mas_centerY);
        make.width.height.mas_equalTo(20);
    }];
    
    [rightB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_searchT.mas_right).offset(0);
        make.right.mas_equalTo(0);
        make.centerY.mas_equalTo(_searchT.mas_centerY);
        make.width.mas_equalTo(60);
    }];
}

-(void)cancle
{
    if (self.popBlock)
    {
        self.popBlock();
    }
}

-(void)tapRemoveSearchtText
{
    self.searchT.text = @"";
    if (self.removeCellBlock)
    {
        self.removeCellBlock();
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
