//
//  SearchTableViewController.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSString *title;
-(instancetype)initTitle:(NSString *)title;

-(void)resectTable:(NSString *)title;

@end
