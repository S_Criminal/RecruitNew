//
//  SearchTableViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "SearchTableViewController.h"
#import "FirstMainManager.h"
#import "MainCell.h"

#import "NewWorkDetailVC.h"

#import "FirstSelectMoreButtonView.h"       //屏蔽View
#import "RequestApi+Main.h"

@interface SearchTableViewController ()
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic ,strong) FirstMainManager *manager;
@property (nonatomic ,strong) NSManagedObjectContext * context;

@property (nonatomic ,strong) FirstSelectMoreButtonView *moreHiddenView;

@end

@implementation SearchTableViewController{
    NSInteger _pageIndex;
}

-(instancetype)initTitle:(NSString *)title
{
    SearchTableViewController *search = [SearchTableViewController new];
    search.title = title;
    return search;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _manager = [FirstMainManager share];
    [self.view addSubview:self.tableView];
    
    _pageIndex = 1;
//    [self setModelPageIndex:_pageIndex];
    [self setFefresh];
    
    UIApplication *app = [UIApplication sharedApplication];
    id delegate = app.delegate;
    self.context = [delegate managedObjectContext];
    // Do any additional setup after loading the view.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MainCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
}

-(void)resectTable:(NSString *)title
{
    self.title = title;
    _pageIndex = 1;
    [self setModelPageIndex:_pageIndex];
    if (self.listArr.count > 0)
    {
        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView selectRowAtIndexPath:scrollIndexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
}

-(void)setModelPageIndex:(NSInteger)index
{
    DSWeak;
    [_manager getPositionWithkey:[GlobalData sharedInstance].GB_Key Top:@"20" cid:@"" position:self.title positionID:@"" industryID:@"" province:@"" city:@"" success:^(NSDictionary *dic) {
        
    } PageIndex:index status:@"searchPosition" BlockHandelDoJobSuccess:^(NSString *str) {
        if ([str isEqualToString:@"成功"]){
            [self setCoreData];
        }else if ([str isEqualToString:@"100"]){
            [MBProgressHUD showError:@"网络不好，请检查网络!" toView:self.view];
            [self setCoreData];
        }else if ([str isEqualToString:@"10000"]){
            [MBProgressHUD showError:@"没有更多数据" toView:self.view];
            [weakSelf.tableView reloadData];
        }
        NSLog(@"%@",weakSelf.view);
        
        UIView *noDateView = (UIView *)[weakSelf.view viewWithTag:1100];
        
        if (noDateView)
        {
            [GlobalMethod removeView:noDateView];
        }
        
        if ([str isEqualToString:@"没有数据"])
        {
            [weakSelf.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, 0, KWIDTH, self.view.height)]];
        }
    }];
}



#pragma mark 刷新 加载更多
-(void)setFefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
    //    [self.firstTableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    //    [header beginRefreshing];
    
    // 设置header
    self.tableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    //    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    //    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    _pageIndex = 1;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
}

-(void)setCoreData
{
    NSFetchRequest *request ;
    NSError *error;
    request = [NSFetchRequest fetchRequestWithEntityName:@"PositionCoreData"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"status == %@",@"searchPosition"];
    request.predicate = predicate;
    
    NSArray *resultAry = [self.context executeFetchRequest:request error:&error];
    
    if (resultAry.count == 0) {
        
    }else{
        _listArr = [NSMutableArray arrayWithArray:resultAry];
    }
    NSLog(@"_listArr_listArr%@",_listArr);
    
    [self.tableView reloadData];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%ld",_listArr.count);
    
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    CGFloat h = [cell resetCellModel];
    
    return h;
}

-(MainCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    if (_listArr.count > 0)
    {
        PositionCoreData *positioModel = _listArr[indexPath.row];
        cell.model = positioModel;
        NSLog(@"%@",cell);
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectMoreButton.tag = indexPath.row;
        [cell.selectMoreButton addTarget:self action:@selector(tapSelectMoreButton:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PositionCoreData *positioModel = _listArr[indexPath.row];
    _manager.workDetail_pid = positioModel.c_Pid;
    NewWorkDetailVC *workDetail = [[NewWorkDetailVC alloc]init];
    workDetail.cid = positioModel.c_Cid;
    workDetail.pid = positioModel.c_Pid;
    [self.navigationController pushViewController:workDetail animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y > 200) {
        [scrollView.superview.superview.superview endEditing:YES];
    }
    
}

#pragma mark 点击屏蔽
-(void)tapSelectMoreButton:(UIButton *)sender
{
    DSWeak;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    MainCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [self.tableView convertRect:cell.frame toView:self.view];
    
    
    PositionCoreData *positioModel = _listArr[indexPath.row];
    
    
    if (!_moreHiddenView)
    {
        _moreHiddenView = [FirstSelectMoreButtonView initWithModel:nil];
        _moreHiddenView.tapRemoveBlock = ^{
            [weakSelf tapRemoveCompany:positioModel.c_Cid PositionCoreData:positioModel];
        };
        [_moreHiddenView setFrame:CGRectMake(0, - NAVIGATIONBAR_HEIGHT - 1, KWIDTH, KHEIGHT)];
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:1011];
        CGFloat top = rectInSuperview.origin.y + button.bottom + NAVIGATION_BarHeight + 1;
        [_moreHiddenView resetViewWithBGViewFrame:top];
        UITapGestureRecognizer *tapHidden = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHiddenMoreView)];
        [_moreHiddenView.currWindow addGestureRecognizer:tapHidden];
    }
    [self.view addSubview:_moreHiddenView];
}

-(void)tapHiddenMoreView
{
    //    self.tabBarController.tabBar.hidden = NO;
    [_moreHiddenView removeFromSuperview];
    _moreHiddenView = nil;
}

#pragma mark 确认屏蔽
-(void)tapRemoveCompany:(NSString *)cid PositionCoreData:(PositionCoreData *)model
{
    DSWeak;
    
    NSMutableArray *listArrItems = [NSMutableArray arrayWithArray:self.listArr];
    
    [RequestApi editscompanyWithKey:[GlobalData sharedInstance].GB_Key editID:@"0" cid:model.c_Cid cname:model.c_Name Delegate:nil success:^(NSDictionary *response) {
        [listArrItems removeObject:model];
        [weakSelf.context deleteObject:model];
        weakSelf.listArr = listArrItems;
        [weakSelf setCoreData];
        [weakSelf tapHiddenMoreView];
        [MBProgressHUD showSuccess:@"屏蔽成功" toView:weakSelf.view];
    } failure:^(NSString *str) {
        [MBProgressHUD showSuccess:str toView:weakSelf.view];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
