//
//  SearchViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "SearchViewController.h"

/** view  */
#import "SearchNavView.h"
#import "NewCompanySearchView.h"

#import "RequestApi+Main.h"

#import "SearchTableViewController.h"   //搜索tableview

@interface SearchViewController ()<UITextFieldDelegate,RequestDelegate,selectSearchDelegate>
@property (nonatomic ,strong) UITextField *searchT;
@property (nonatomic ,strong) UIView *hotBGView;
@property (nonatomic ,strong) NewCompanySearchView *hotSearchView;
@property (nonatomic ,strong) NewCompanySearchView *historyView;    //历史记录view
@property (nonatomic ,strong) SearchTableViewController *table;

@property (nonatomic ,strong) UIButton *x_DeleteButton;     //搜索框的清空按钮
@property (nonatomic ,strong) UIButton *clearButton;

@property (nonatomic ,strong) NSMutableArray *historyArr;      //历史记录数组
@property (nonatomic ,strong) NSMutableArray *hotArr;          //热门搜索数组

@end

@implementation SearchViewController{
    //适配5s
    CGFloat hotViewHeight ;
    CGFloat hotLabelToTop ;
    CGFloat searchBfont;
}

-(SearchTableViewController *)table
{
    if (!_table) {
        _table = [SearchTableViewController new];
    }
    return _table;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUI];
    [self setNav];
    [self setModel];
    [self createBasicView];
}

//-(NewCompanySearchView *)hotSearchView
//{
//    if (!_hotSearchView)
//    {
//        _hotSearchView = [NewCompanySearchView new];
//    }
//    return _hotSearchView;
//}

-(UIView *)hotBGView
{
    if (!_hotBGView ) {
        _hotBGView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH , KHEIGHT - NAVIGATION_BarHeight)];
        _hotBGView.backgroundColor = [UIColor whiteColor];
    }
    return _hotBGView;
}

-(void)setNav
{
    DSWeak;
    /** navView */
    SearchNavView *navView = [[SearchNavView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.popBlock = ^(){
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    _searchT = navView.searchT;
    _searchT.delegate = self;
    self.x_DeleteButton = navView.xButton;
    
    navView.removeCellBlock = ^{
        self.table.view.hidden = YES;
        self.x_DeleteButton.hidden = YES;
        [self.view endEditing:YES];
        UIView *noDateView = (UIView *)[weakSelf.table.view viewWithTag:1100];
        [GlobalMethod removeView:noDateView];
    };
    
    [self.view addSubview:navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setUI
{
    hotViewHeight = W(120);
    hotLabelToTop = W(20);
    searchBfont = F(15);
}

#pragma mark 请求数据源
-(void)setModel
{
    self.hotArr = [NSMutableArray arrayWithObjects:@"区域经理",@"农艺师",@"销售代表",@"省区经理",@"产品经理",@"作物经理",@"销售经理",@"技术主管",@"研发工程师",@"推广专员", nil];
    if (!kStringIsEmpty([GlobalData sharedInstance].historyArray)) {
        self.historyArr = [NSMutableArray arrayWithArray:[[GlobalData sharedInstance].historyArray componentsSeparatedByString:@","]];
    }else{
        self.historyArr = [NSMutableArray array];
    }
}

-(void)createBasicView
{
    [self.view addSubview:self.hotBGView];
    //热门搜索label
    UILabel  *hotLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, W(20), KWIDTH - 30, 30)];
    hotLabel.font = [UIFont systemFontOfSize:F(14)];
    [hotLabel setAttributedText:[CustomTitleButtonStr setButtonAttributeTitleWithTitle:@"热门搜索" AndTitleImageName:@"热门搜索" ImageRect:CGRectMake(0, -1, 13, 13) color:@"272727"]];
    [_hotBGView addSubview:hotLabel];
    
    //创建热门搜索view
    _hotSearchView = [[NewCompanySearchView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(hotLabel.frame) + hotLabelToTop, KWIDTH, hotViewHeight)];
    _hotSearchView.delegate = self;
    [_hotSearchView setButtonArray:self.hotArr];
    [_hotBGView addSubview:_hotSearchView];
    
    //换一下 热门搜索  button
    UIButton *refreshButton = [[UIButton alloc]init];
    [refreshButton setFrame:CGRectMake(KWIDTH - 20 - 150, hotLabelToTop, 150, 30)];
    refreshButton.centerY = hotLabel.centerY;
    refreshButton.titleLabel.font = [UIFont systemFontOfSize:13];
    refreshButton.contentHorizontalAlignment = NSTextAlignmentRight;
    [refreshButton addTarget:self action:@selector(refreshSearchModel) forControlEvents:UIControlEventTouchUpInside];
    [refreshButton setAttributedTitle:[CustomTitleButtonStr setRightImageButtonAttributeTitleWithTitle:@"换一批 " AndTitleImageName:@"换一批" ImageRect:CGRectMake(0, -2, 12, 12) color:@"999999"] forState:UIControlStateNormal];
    [_hotBGView addSubview:refreshButton];
    
    ////搜索记录 label
    UILabel *historyLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_hotSearchView.frame) , KWIDTH - 30, 30)];
    historyLabel.font = [UIFont systemFontOfSize:F(14)];
    [historyLabel setAttributedText:[CustomTitleButtonStr setButtonAttributeTitleWithTitle:@"搜索记录" AndTitleImageName:@"搜索记录" ImageRect:CGRectMake(0, -2, 13, 13) color:@"272727"]];
    [_hotBGView addSubview:historyLabel];
    
    //历史记录view
    self.historyView = [[NewCompanySearchView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(historyLabel.frame) + 20, KWIDTH , hotViewHeight)];
    self.historyView.delegate = self;
    [self setUpHistoryView:@""];
    [_hotBGView addSubview:self.historyView];
    
    //搜索记录 清空搜索记录
    self.clearButton = [[UIButton alloc]initWithFrame:CGRectMake(KWIDTH - W(80), CGRectGetMaxY(_hotSearchView.frame), W(80), 30)];
    [self.clearButton setImage:[UIImage imageNamed:@"searchDelete"] forState:UIControlStateNormal];
    [self.clearButton addTarget:self action:@selector(clearHistory:) forControlEvents:UIControlEventTouchUpInside];
    self.clearButton.titleLabel.font = [UIFont systemFontOfSize:F(15)];
    [_hotBGView addSubview:self.clearButton];
    
}

//创建热门搜索view
-(void)createHotSearchView:(CGFloat)top
{
    //热门搜索View
    
}

//刷新热门搜索
-(void)refreshSearchModel
{
    [RequestApi getHotListWithKey:[GlobalData sharedInstance].GB_Key hType:@"0" Delegate:self success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        if (!kArrayIsEmpty(arr)) {
            NSMutableArray *ar = [NSMutableArray array];
            for (NSDictionary *dic in arr)
            {
                [ar addObject:dic[@"H_Value"]];
            }
            [_hotSearchView setButtonArray:ar];
        }else{
            [_hotSearchView setButtonArray:self.hotArr];
        }
    } failure:^(NSDictionary *response) {
        [MBProgressHUD showError:@"连接失败，请检查你的网络设置" toView:self.view];
    }];
}

#pragma mark 进行搜索
-(void)selectSeachTitle:(NSString *)title
{
    title = [GlobalMethod exchangeEmpty:title];
    _searchT.text = title;
    
    [_searchT resignFirstResponder];
    self.x_DeleteButton.hidden = NO;
    
    [self addChildViewController:self.table];
    
    self.table.view.frame = CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1);
    self.table.tableView.frame = self.table.view.frame;
    self.table.tableView.y = 0;
    [self.table resectTable:title];
    self.table.view.hidden = NO;
    [self.view addSubview:self.table.view];
    [self setUpHistoryView:title];
}

-(void)setUpHistoryView:(NSString *)title
{
    /** 历史记录添加元素 */
    title = [GlobalMethod exchangeEmpty:title];
    BOOL isAdd = YES;
    if (!kStringIsEmpty(title))
    {
        if (kArrayIsEmpty(self.historyArr)) {
            [self.historyArr addObject:title];
        }else{
            for (NSString *str in self.historyArr)
            {
                if ([str isEqualToString:title])
                {
                    isAdd = NO;
                    break;
                }
            }
            isAdd ? [self.historyArr addObject:title] : self.historyArr;
        }
    }
    
    [GlobalData sharedInstance].historyArray = [self.historyArr componentsJoinedByString:@","];
    [self.historyView setButtonArray:self.historyArr];
}

//清空历史记录数组
-(void)clearHistory:(UIButton *)button
{
    self.table.view.hidden = YES;
    [GlobalData sharedInstance].historyArray = nil;
    self.historyArr = [NSMutableArray array];
    [self.historyArr removeAllObjects];
    [self setUpHistoryView:@""];
}

#pragma mark textField delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    textField.text.length == 0 ? [MBProgressHUD showError:@"请输入职位名称或公司名" toView:self.view] : [self selectSeachTitle:textField.text];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.x_DeleteButton.hidden = NO;
    if (_searchT.text.length==0)
    {
        self.table.view.hidden = YES;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (range.location == 0) {
        self.x_DeleteButton.hidden = YES;
    }else{
        self.x_DeleteButton.hidden = NO;
    }
    return YES;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
