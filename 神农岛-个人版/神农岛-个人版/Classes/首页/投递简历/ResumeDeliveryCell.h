//
//  ResumeDeliveryCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResumeDeliveryModel.h"
@interface ResumeDeliveryCell : UITableViewCell
@property (nonatomic ,strong) UILabel *titleLable;
@property (nonatomic ,strong) UILabel *contentLabel;
@property (nonatomic ,strong) UIView *line;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(ResumeDeliveryModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(ResumeDeliveryModel *)model;

@end
