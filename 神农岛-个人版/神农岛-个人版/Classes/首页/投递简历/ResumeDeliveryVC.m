//
//  ResumeDeliveryVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ResumeDeliveryVC.h"
#import "RequestApi+Main.h"
#import "ResumeDeliveryModel.h"
#import "ResumeDeliveryCell.h"

#import "KYAlertView.h"

@interface ResumeDeliveryVC ()
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic ,strong) KYAlertView *kyAlertView;

@end

@implementation ResumeDeliveryVC

-(KYAlertView *)kyAlertView
{
    if (!_kyAlertView) {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    return _kyAlertView;
}

-(instancetype)initPid:(NSString *)pid
{
    ResumeDeliveryVC *resume = [ResumeDeliveryVC new];
    resume.pid = pid;
    return resume;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self.tableView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[ResumeDeliveryCell class] forCellReuseIdentifier:@"ResumeDeliveryCell"];
    [self setModel];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"选择简历进行投递" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(void)setModel
{
    self.listArr = [NSMutableArray array];
    DSWeak;
    [RequestApi getResumeWithKey:[GlobalData sharedInstance].GB_Key status:@"0" Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr)
        {
            ResumeDeliveryModel *model = [ResumeDeliveryModel modelObjectWithDictionary:dic];
            [weakSelf.listArr addObject:model];
        }
        [weakSelf.tableView reloadData];
        
    } failure:^(NSDictionary *response) {
        
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [ResumeDeliveryCell fetchHeight:_listArr[indexPath.row]];
    return h;
}

-(ResumeDeliveryCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ResumeDeliveryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResumeDeliveryCell"];
    [cell resetCellWithModel:_listArr[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_listArr.count > 0)
    {
        ResumeDeliveryModel *model = _listArr[indexPath.row];
        NSString *rid = [GlobalMethod doubleToString:model.iDProperty];
        
        DSWeak;
        NSString *title = kStringIsEmpty(model.pName) ? @"投递简历" : [NSString stringWithFormat:@"投递简历：%@",model.pName];
        [self.kyAlertView showAlertView:title message:@"发送不可撤回，确认发送?" subBottonTitle:@"取消" cancelButtonTitle:@"确认" handler:^(AlertViewClickBottonType bottonType) {
            if (bottonType == AlertViewClickBottonTypeCancelButton)
            {
                [RequestApi sendResumeWithKey:[GlobalData sharedInstance].GB_Key rid:rid pid:self.pid Delegate:nil success:^(NSDictionary *response) {
                    if ([response[@"code"] isEqualToNumber:@200])
                    {
                        [GlobalData sharedInstance].isDelivery = YES;
                        [self alertViewWithTitle:response[@"messge"]];
                    }
                } failure:^(NSString *string) {
                    [MBProgressHUD showError:string toView:weakSelf.view];
                }];
            }
        }];
    }
}

-(void)alertViewWithTitle:(NSString *)title
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
