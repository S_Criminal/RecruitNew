//
//  ResumeDeliveryVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC.h"

@interface ResumeDeliveryVC : BasicVC <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSString *pid;
-(instancetype)initPid:(NSString *)pid;

@end
