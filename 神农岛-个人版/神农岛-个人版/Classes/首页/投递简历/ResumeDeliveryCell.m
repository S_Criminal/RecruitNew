//
//  ResumeDeliveryCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ResumeDeliveryCell.h"

@implementation ResumeDeliveryCell

#pragma mark 懒加载

- (UILabel *)titleLable{
    if (_titleLable == nil) {
        _titleLable = [UILabel new];
        [GlobalMethod setLabel:_titleLable widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _titleLable;
}

- (UILabel *)contentLabel{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel new];
        [GlobalMethod setLabel:_contentLabel widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _contentLabel;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLable];
        [self.contentView addSubview:self.contentLabel];
        [self.contentView addSubview:self.line];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(ResumeDeliveryCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(ResumeDeliveryModel *)model{
    
    //刷新view
    [GlobalMethod setLabel:self.titleLable widthLimit:KWIDTH - W(15) * 2 numLines:1 fontNum:F(16) textColor:[UIColor blackColor] text:model.pName];
    self.titleLable.leftTop = XY(W(15),W(15));

    [GlobalMethod setLabel:self.contentLabel widthLimit:KWIDTH - W(15) * 2 numLines:1 fontNum:F(14) textColor:COLOR_LABELSIXCOLOR text:model.rCity];
    self.contentLabel.leftTop = XY(W(15),self.titleLable.bottom+W(12));
    
    [self.line setFrame:CGRectMake(W(15), self.contentLabel.bottom + W(10), KWIDTH - W(15), 1)];
    
    return self.line.bottom;
}

@end
