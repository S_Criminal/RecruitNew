//
//  LoginViewController.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/23.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicVC.h"
@interface LoginViewController : BasicVC
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end
