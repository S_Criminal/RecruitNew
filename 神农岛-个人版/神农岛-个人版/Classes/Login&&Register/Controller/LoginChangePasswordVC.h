//
//  LoginChangePasswordVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC.h"

@interface LoginChangePasswordVC : BasicVC
@property (nonatomic ,strong) NSString *phoneN;
@property (nonatomic ,strong) NSString *smsCode;

-(instancetype)initPhone:(NSString *)phone smsCode:(NSString *)smsCode;
@end
