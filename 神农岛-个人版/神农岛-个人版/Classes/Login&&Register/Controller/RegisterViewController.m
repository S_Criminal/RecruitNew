//
//  RegisterViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "RegisterViewController.h"
/** 用户协议 */
#import "UserAgreementViewController.h"
#import "LoginViewController.h"
#import "RequestApi+Login.h"

#import "PassWordViewController.h"

@interface RegisterViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneT;
@property (weak, nonatomic) IBOutlet UITextField *passwordT;

@property(strong,nonatomic)UIView *phoneV;
@property(strong,nonatomic)UIView *passwordV;
@property(strong,nonatomic)UIImageView *imagePhone;
@property(strong,nonatomic)UIImageView *imagePassword;

@property(strong,nonatomic)NSUserDefaults *userDefaults;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
//constaint约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *duiHaoLeftConstaint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightCon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneHeightConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordHeightConstaint;

@property (nonatomic ,strong) UILabel *phoneLabel;
@property (nonatomic ,strong) UILabel *passwordLabel;

@end

@implementation RegisterViewController{
    CGFloat contentFont;
    CGFloat textHeight;
    CGFloat textFont;
    CGFloat keyBoardChangeHeight;
}
#pragma mark 懒加载
-(UILabel *)phoneLabel
{
    if (!_phoneLabel)
    {
        _phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, KWIDTH / 2 + 40, _phoneHeightConstaint.constant)];
        _phoneLabel.text = @"请输入您的手机号";
        _phoneLabel.textColor = [HexStringColor colorWithHexString:@"ADAEAD"];
        _phoneLabel.font = [UIFont systemFontOfSize:contentFont];
//        [self.phoneT addSubview:_phoneLabel];
    }
    return _phoneLabel;
}

-(UILabel *)passwordLabel
{
    if (!_passwordLabel)
    {
        _passwordLabel = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, KWIDTH / 2, _passwordHeightConstaint.constant)];
        _passwordLabel.text = @"请输入密码";
        _passwordLabel.textColor = [HexStringColor colorWithHexString:@"ADAEAD"];
        _passwordLabel.font = [UIFont systemFontOfSize:contentFont];
//        [self.passwordT addSubview:_passwordLabel];
    }
    return _passwordLabel;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self setNav];
    [self setUI];
    [self createView];
    [self setPlaceHolderLabel];
    self.userDefaults = [NSUserDefaults standardUserDefaults];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNav];
}

-(void)setNav
{
    self.navigationController.navigationBarHidden = YES;
}
-(void)setUI
{
    contentFont = 15;
    textFont = 17;
    keyBoardChangeHeight = 0;
    if (KWIDTH == 320) {
        textHeight          = 30;
        textFont            = 15;
        contentFont         = 13;
        keyBoardChangeHeight= 50;
        self.bottomConstaint.constant = 80;
        self.topConstaint.constant = 100;
        self.duiHaoLeftConstaint.constant = 10;
        
        self.buttonHeightCon.constant = 35;
        self.phoneHeightConstaint.constant = 35;
        self.passwordHeightConstaint.constant = 35 - 1;
        if (KHEIGHT == 480) {
            self.bottomConstaint.constant=40;
            self.topConstaint.constant = 50;
        }
    }else if (KWIDTH == 375)
    {
        textHeight           = 40;
        keyBoardChangeHeight = 30;
        contentFont          = 15;
    }
    else
    {
        textHeight = 45;
    }
}

-(void)setPlaceHolderLabel
{
    [self.phoneT addSubview:self.phoneLabel];
    [self.passwordT addSubview:self.passwordLabel];
}

-(void)createView
{
    [self.registerButton setCorner:3];
    
    self.phoneT.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 0)];
    self.phoneT.leftViewMode = UITextFieldViewModeAlways;
    self.phoneT.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneT.delegate = self;
    self.phoneT.textColor = [UIColor whiteColor];
    self.phoneT.font = [UIFont systemFontOfSize:textFont];
    self.phoneT.delegate = self;
    
    self.passwordT.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 0)];
    self.passwordT.textColor = [UIColor whiteColor];
    self.passwordT.keyboardType = UIKeyboardTypeNumberPad;
    self.passwordT.font = [UIFont systemFontOfSize:textFont];
    self.passwordT.leftViewMode = UITextFieldViewModeAlways;
    self.passwordT.delegate =self;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.phoneT resignFirstResponder];
    [self.passwordT resignFirstResponder];
    self.phoneT.userInteractionEnabled = YES;
    //    self.imageTitle.hidden = NO;
}


- (IBAction)getConfirm:(id)sender
{
    //获取验证码
    DSWeak;
    [RequestApi getCodeWithTel:self.phoneT.text Type:@"reg" Delegate:nil success:^(NSDictionary *response) {
        [weakSelf rePostConfirm];
    } failure:^(NSString *errostr) {
        [MBProgressHUD showError:errostr toView:self.view];
    }];
}

-(void)rePostConfirm{
    //倒计时
    __block int timeout=60; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self.confirmButton setTitle:@"发送验证码" forState:UIControlStateNormal];
                self.confirmButton.userInteractionEnabled = YES;
            });
        }else{
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                //NSLog(@"____%@",strTime);
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:1];
                [self.confirmButton setTitle:[NSString stringWithFormat:@"%@秒后重新发送",strTime] forState:UIControlStateNormal];
                [UIView commitAnimations];
                self.confirmButton.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}

//用户注册
- (IBAction)loginButton:(UIButton *)sender
{
    DSWeak;
    
    [RequestApi phoneAndCodeWithPhone:self.phoneT.text smscode:self.passwordT.text type:@"reg" Delegate:self success:^(NSDictionary *response) {
        PassWordViewController *pass = [[PassWordViewController alloc]initWithPhone:self.phoneT.text smsCode:self.passwordT.text];
        [GB_Nav pushViewController:pass animated:YES];
    } failure:^(NSString *errostr) {
        [MBProgressHUD showError:errostr toView:weakSelf.view];
    }];
}
                                                                                                                                                                 
- (IBAction)loginButt:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:NO];
    
}

#pragma mark textfield的代理方法
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    if (self.phoneT == textField)  //判断是否时我们想要限定的那个输入框
    {
        if ([toBeString length] > 12) { //如果输入框内容不为11则弹出警告
            textField.text = [toBeString substringToIndex:11];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"手机号输入错误" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //点击按钮的响应事件；
            }]];
            [self presentViewController:alert animated:true completion:nil];
            return NO;
        }
    }
    return YES;
}
#pragma mark 提示框
-(void)alert:(NSString *)m message:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:m message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)alert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"注册失败" message:@"请重新输入验证码" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // When the user presses return, take focus away from the text field so that the keyboard is dismissed.
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    CGRect rect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
    self.view.frame = rect;
    [UIView commitAnimations];
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.phoneT) {
        _phoneLabel.hidden = YES;
    }else if (textField == self.passwordT) {
        _passwordLabel.hidden = YES;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShows:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHides:) name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == self.phoneT) {
        if (textField.text.length == 0) {
            _phoneLabel.hidden = NO;
        }
    }else if (textField == self.passwordT) {
        if (textField.text.length == 0) {
            _passwordLabel.hidden = NO;
        }
    }
}
#pragma mark --- 弹出键盘 ---
- (void)keyboarShows:(NSNotification *)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.view.frame;
        if (frame.origin.y == 0) {
            frame.origin.y = weakSelf.view.frame.origin.y - keyBoardChangeHeight;
            weakSelf.view.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark --- 收起键盘 ---
- (void)keyboarHides:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.view.frame;
        if (frame.origin.y < 0) {
            frame.origin.y = weakSelf.view.frame.origin.y + keyBoardChangeHeight;
            weakSelf.view.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark 老刀招聘用户协议
- (IBAction)userProtocol:(UIButton *)sender
{
    UserAgreementViewController *agreement = [[UserAgreementViewController alloc]init];
    [self.navigationController pushViewController:agreement animated:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
