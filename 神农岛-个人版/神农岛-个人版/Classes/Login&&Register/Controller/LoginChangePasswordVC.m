//
//  LoginChangePasswordVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "LoginChangePasswordVC.h"
#import "RequestApi+Login.h"
@interface LoginChangePasswordVC ()<UITextFieldDelegate>

@property (nonatomic ,strong) UITextField *firstT;
@property (nonatomic ,strong) UITextField *secondT;
@property (nonatomic ,strong) UIButton *confirmButton;
@property (nonatomic ,weak) NSTimer *hideDelayTimer;

@end

@implementation LoginChangePasswordVC

-(UITextField *)firstT
{
    if (!_firstT) {
        _firstT = [UITextField new];
        _firstT.delegate = self;
        _firstT.placeholder = @"请输入6-16位密码";
    }
    return _firstT;
}

-(UITextField *)secondT
{
    if (!_secondT) {
        _secondT =[UITextField new];
        _secondT.delegate = self;
        _secondT.placeholder = @"请再次输入密码";
    }
    return _secondT;
}

-(UIButton *)confirmButton
{
    if (!_confirmButton) {
        _confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setCorner:4];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        _confirmButton.backgroundColor = COLOR_MAINCOLOR;
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:F(17)];
        [_confirmButton addTarget:self action:@selector(tapConfirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

-(instancetype)initPhone:(NSString *)phone smsCode:(NSString *)smsCode
{
    LoginChangePasswordVC *change = [LoginChangePasswordVC new];
    change.phoneN = phone;
    change.smsCode = smsCode;
    return change;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"修改密码" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.firstT];
    [self.view addSubview:self.secondT];
    
    [self.view addSubview:self.confirmButton];
    
    [self.firstT setFrame:CGRectMake(W(25), NAVIGATION_BarHeight + W(50), KWIDTH - W(25) * 2, W(25))];
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.firstT.left, self.firstT.bottom + W(2), KWIDTH - self.firstT.left * 2, 1)]];
    self.secondT.frame = self.firstT.frame;
    self.secondT.y = self.firstT.bottom + W(15);
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.firstT.left, self.secondT.bottom + W(2) , self.firstT.width, 1)]];
    [self.confirmButton setFrame:CGRectMake(self.firstT.left, self.secondT.bottom + W(50), self.firstT.width, W(40))];
    
    [self setTextField:self.firstT];
    [self setTextField:self.secondT];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapView)];
    [self.view addGestureRecognizer:tap];
}

-(void)setTextField:(UITextField *)textField
{
    textField.delegate = self;
    textField.textColor = COLOR_LABELThreeCOLOR;
    textField.font = [UIFont systemFontOfSize:F(15)];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
}

-(void)tapConfirm:(UIButton *)sender
{
    NSString *str = @"";
    if (self.firstT.text.length < 6) {
        str = @"密码不能小于六位";
    }else if (self.firstT.text.length > 16){
        str = @"密码不能大于十六位";
    }
    
    if (str.length == 0) {
        if (![self.firstT.text isEqualToString:self.secondT.text])
        {
            [MBProgressHUD showError:@"两次密码不一致" toView:self.view];
            return;
        }
        
    }else{
        [MBProgressHUD showError:str toView:self.view];
        return;
    }
    
    
    DSWeak;
    [RequestApi forgetpwdWithPhone:self.phoneN smscode:self.smsCode pwdf:self.firstT.text Delegate:nil success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"修改密码成功" toView:weakSelf.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errostr) {
        [MBProgressHUD showSuccess:errostr toView:weakSelf.view];
    }];
}

-(void)handleHideTimer
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tapView
{
    [self.view endEditing:YES];
}

-(void)dealloc
{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
