//
//  LoginViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/23.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "CustomTabBarController.h"

#import "RequestApi+Login.h"
//#import "RequestApi.h"
#import "ForgetPasswordVC.h"        //忘记密码

#import <AVOSCloud/AVOSCloud.h>


@interface LoginViewController ()<UITextFieldDelegate,RequestDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *phoneImage;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImage;

@property (weak, nonatomic) IBOutlet UITextField *phoneT;
@property (weak, nonatomic) IBOutlet UITextField *passwordT;
//约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstaint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordHeightConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneHeightConstaint;

@property (nonatomic ,strong) UILabel *phoneLabel;
@property (nonatomic ,strong) UILabel *passwordLabel;


@end

@implementation LoginViewController
{
    CGFloat keyBoardChangeHeight;
    CGFloat textHeight;
    CGFloat textFont;
    CGFloat contentFont;
}

-(UILabel *)phoneLabel
{
    if (!_phoneLabel)
    {
        _phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, KWIDTH / 2 + 40, _phoneHeightConstaint.constant)];
        _phoneLabel.text = @"请输入您的手机号";
        _phoneLabel.textColor = [HexStringColor colorWithHexString:@"ADAEAD"];
        _phoneLabel.font = [UIFont systemFontOfSize:contentFont];
    }
    return _phoneLabel;
}

-(UILabel *)passwordLabel
{
    if (!_passwordLabel)
    {
        _passwordLabel = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, KWIDTH / 2, _passwordHeightConstaint.constant)];
        _passwordLabel.text = @"请输入密码";
        _passwordLabel.textColor = [HexStringColor colorWithHexString:@"ADAEAD"];
        _passwordLabel.font = [UIFont systemFontOfSize:contentFont];
    }
    return _passwordLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self setUI];
    [self setPlaceHolderLabel];
    [self creatViews];
}

-(void)setNav
{
    self.navigationController.navigationBarHidden = YES;
}
-(void)setPlaceHolderLabel
{
    [self.phoneT addSubview:self.phoneLabel];
    [self.passwordT addSubview:self.passwordLabel];
}

-(void)setUI
{
    textFont    = 17;
    textHeight  = 30;
    contentFont = 15;
    if (isIphone5) {
        keyBoardChangeHeight = 50;
        textHeight = 30;
        textFont = 15;
        contentFont = 13;
        self.bottomConstaint.constant           = 80;
        self.topConstaint.constant              = 100;
        self.buttonHeightConstaint.constant     = 35;
        self.phoneHeightConstaint.constant      = 35;
        self.passwordHeightConstaint.constant   = 35 - 1;
        if (KHEIGHT == 480)
        {
            self.topConstaint.constant = 50;
            self.bottomConstaint.constant = 30;
        }
    }else if (isIphone6){
        keyBoardChangeHeight    = 30;
        textHeight              = 40;
        contentFont             = 15;
    }else{
        keyBoardChangeHeight = 0;
        textHeight = 45;
    }
}

-(void)creatViews
{
    self.phoneT.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 0)];
    self.phoneT.leftViewMode = UITextFieldViewModeAlways;
    self.phoneT.delegate = self;
    self.phoneT.font = [UIFont systemFontOfSize:textFont];
    
    self.passwordT.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 16, 0)];
    self.passwordT.leftViewMode = UITextFieldViewModeAlways;
    self.passwordT.delegate =self;
    self.passwordT.secureTextEntry = YES;
    self.passwordT.font = [UIFont systemFontOfSize:textFont];
    
    [self.loginButton setCorner:3];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.phoneT) {
        _phoneLabel.hidden = YES;
    }else if (textField == self.passwordT) {
        _passwordLabel.hidden = YES;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShows:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHides:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.phoneT) {
        if (textField.text.length == 0) {
            _phoneLabel.hidden = NO;
        }
    }else if (textField == self.passwordT) {
        if (textField.text.length == 0) {
            _passwordLabel.hidden = NO;
        }
    }
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.phoneT resignFirstResponder];
    [self.passwordT resignFirstResponder];
}

#pragma mark textfield的代理方法
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField ==self.phoneT) {
        //        self.phoneImage.image = [UIImage imageNamed:@"2.jpg"];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    if (self.phoneT == textField)  //判断是否时我们想要限定的那个输入框
    {
        if ([toBeString length] >12) { //如果输入框内容不为11则弹出警告
            textField.text = [toBeString substringToIndex:11];

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"手机号输入错误" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //点击按钮的响应事件；
            }]];
            [self presentViewController:alert animated:true completion:nil];
            
            return NO;
        }
    }
    return YES;
}

//点击登录按钮

- (IBAction)loginB:(UIButton *)sender
{
    NSString *phoneN     = self.phoneT.text;
    NSString *passwordN  = self.passwordT.text;
    
//    phoneN    = @"17865646564";
//    phoneN    = @"13964649107";
//    phoneN    = @"17865645736";
//    phoneN = @"15589656521";
//    passwordN = @"123456";
    
    /*
    if (![GlobalMethod IsEnableNetwork]) {
        [MBProgressHUD showError:@"网络连接失败，请检查网络" toView:self.view];
        return;
    }
     */
    
    BOOL phone = [ValidateRegularExpression validatePhone:phoneN];
   
    if (phone == NO) {
        [MBProgressHUD showError:@"手机号输入不正确" toView:self.view];
        return;
    }
    
    if (passwordN.length < 6 ) {
        [MBProgressHUD showError:@"密码不能少于6位" toView:self.view];
        return;
    }
    
    
    
    DSWeak;
    [RequestApi getLoginWithPhone:phoneN Type:@"0" Delegate:nil pwd:passwordN success:^(NSDictionary *response) {
        if (!kDictIsEmpty(response))
        {
            NSLog(@"%@",response);
            [GB_Nav popToRootViewControllerAnimated:YES];
            [GlobalData sharedInstance].GB_Phone = phoneN;
            //创建频道
            AVInstallation *currentInstallation = [AVInstallation currentInstallation];
            [currentInstallation addUniqueObject:@"Personal" forKey:@"channels"];
            [currentInstallation saveInBackground];
            //传设备id
            [RequestApi bindDeviceWithKey:[GlobalData sharedInstance].GB_Key equimenumID:[GlobalData sharedInstance].GB_DeviceToken equimetype:@"2" Delegate:nil success:^(NSDictionary *response) {
                NSLog(@"%@",response);
                [GlobalData sharedInstance].isLogin = @"YES";
            } failure:^(NSString *errostr) {
                
            }];
        }
    } failure:^(NSString *errostr) {
        [MBProgressHUD showError:errostr toView:weakSelf.view];
    }];
    
}

-(void)alertViewWithMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    NSString *confirm = @"确定";
    
    if ([message isEqualToString:@"请求超时"])
    {
        confirm = @"请检查网络";
    }
    
    [alert addAction:[UIAlertAction actionWithTitle:confirm style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //点击按钮的响应事件；
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
    
}

- (IBAction)registerB:(id)sender
{
    RegisterViewController *regVC   = [[RegisterViewController alloc]init];
    [self.navigationController pushViewController:regVC animated:NO];

}
//忘记密码
- (IBAction)rePassword:(id)sender
{
    ForgetPasswordVC *forget = [ForgetPasswordVC new];
    [self.navigationController pushViewController:forget animated:YES];
}




#pragma mark --- 弹出键盘 ---
- (void)keyboarShows:(NSNotification *)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.view.frame;
        if (frame.origin.y == 0) {
            frame.origin.y = weakSelf.view.frame.origin.y - keyBoardChangeHeight;
            weakSelf.view.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark --- 收起键盘 ---
- (void)keyboarHides:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.view.frame;
        if (frame.origin.y < 0) {
            frame.origin.y = weakSelf.view.frame.origin.y + keyBoardChangeHeight;
            weakSelf.view.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)dealloc
{
     [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
