//
//  PassWordViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PassWordViewController.h"
#import "RequestApi+Login.h"

@interface PassWordViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate>
@property (nonatomic ,strong) UITextField *firstT;
@property (nonatomic ,strong) UITextField *secondT;
@property (nonatomic ,strong) UIButton *confirmButton;

@end

@implementation PassWordViewController

-(UITextField *)firstT
{
    if (!_firstT) {
        _firstT = [UITextField new];
        _firstT.placeholder = @"请输入6~16位的密码";
        _firstT.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _firstT;
}

-(UITextField *)secondT
{
    if (!_secondT) {
        _secondT = [UITextField new];
        _secondT.placeholder = @"请再次输入密码";
    }
    return _secondT;
}

-(UIButton *)confirmButton
{
    if (!_confirmButton) {
        _confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirmButton setCorner:4];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        _confirmButton.backgroundColor = COLOR_MAINCOLOR;
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
        [_confirmButton addTarget:self action:@selector(tapNext:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

-(instancetype)initWithPhone:(NSString *)phone smsCode:(NSString *)smsCode
{
    PassWordViewController *pass = [PassWordViewController new];
    pass.phone = phone;
    pass.smsCode = smsCode;
    return pass;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"填写密码" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.firstT];
    [self.view addSubview:self.secondT];
    [self.view addSubview:self.confirmButton];
    
    [self.firstT setFrame:CGRectMake(W(25), NAVIGATION_BarHeight + 1 + W(80), KWIDTH - W(25) * 2, 40)];
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.firstT.left, self.firstT.bottom, self.firstT.width, 1)]];
    self.secondT.frame = self.firstT.frame;
    self.secondT.y = self.firstT.bottom + W(15);
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.secondT.left, self.secondT.bottom, self.secondT.width, 1)]];
    [self.confirmButton setFrame:CGRectMake(self.firstT.left, self.secondT.bottom + W(50), self.firstT.width, W(40))];
    
    
    [self setTextField:self.firstT];
    [self setTextField:self.secondT];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapBack:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

-(void)tapBack:(UITapGestureRecognizer *)sender{
    
}

#pragma mark tap delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIControl class]]) {
        return NO;
    }
    return true;
}


-(void)setTextField:(UITextField *)textField
{
    textField.delegate = self;
    textField.textColor = COLOR_LABELThreeCOLOR;
    textField.font = [UIFont systemFontOfSize:F(15)];
}

#pragma mark 点击下一步
-(void)tapNext:(UIButton *)sender
{
    DSWeak;
    NSString *str = ![self.firstT.text isEqualToString:self.secondT.text] ? @"两次密码不一致" : @"";
    if (!kStringIsEmpty(str)) {
        [MBProgressHUD showError:str toView:self.view];
        return;
    }
    
    [RequestApi registerWithPhone:self.phone pwd:self.firstT.text smscode:self.smsCode Delegate:nil success:^(NSDictionary *response) {
        [GlobalData sharedInstance].GB_Phone = _phone;
        [GlobalData sharedInstance].GB_Key   = response[@"datas"];
        [GlobalData sharedInstance].isLogin = @"YES";
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSString *errostr) {
        [MBProgressHUD showError:errostr toView:self.view];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
