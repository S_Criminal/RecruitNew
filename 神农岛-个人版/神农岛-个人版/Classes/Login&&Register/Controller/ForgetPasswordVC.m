//
//  ForgetPasswordVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ForgetPasswordVC.h"
#import "RequestApi+Login.h"
#import "LoginChangePasswordVC.h"
@interface ForgetPasswordVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate>

@property (nonatomic ,strong) UITextField *phoneT;
@property (nonatomic ,strong) UITextField *confimT;
@property (nonatomic ,strong) UIButton *nextButton;
@property (nonatomic ,strong) UIButton *getConfirmButton;

@end

@implementation ForgetPasswordVC

-(UITextField *)phoneT
{
    if (!_phoneT) {
        _phoneT = [UITextField new];
        _phoneT.placeholder = @"手机号（仅支持中国大陆手机号）";
        _phoneT.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _phoneT;
}

-(UITextField *)confimT
{
    if (!_confimT) {
        _confimT = [UITextField new];
        _confimT.placeholder = @"验证码";
    }
    return _confimT;
}

-(UIButton *)nextButton
{
    if (!_nextButton) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_nextButton setCorner:4];
        [_nextButton setTitle:@"下一步" forState:UIControlStateNormal];
        _nextButton.backgroundColor = COLOR_MAINCOLOR;
        _nextButton.titleLabel.font = [UIFont systemFontOfSize:F(17)];
        [_nextButton addTarget:self action:@selector(tapNext:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextButton;
}

-(UIButton *)getConfirmButton
{
    if (!_getConfirmButton) {
        _getConfirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_getConfirmButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [GlobalMethod setRoundView:_getConfirmButton color:COLOR_MAINCOLOR numRound:2 width:1];
        [_getConfirmButton setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
        _getConfirmButton.titleLabel.font = [UIFont systemFontOfSize:F(14)];
        _getConfirmButton.backgroundColor = [UIColor whiteColor];
        [_getConfirmButton addTarget:self action:@selector(tapConfirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getConfirmButton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"忘记密码" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.phoneT];
    [self.view addSubview:self.confimT];
    [self.view addSubview:self.nextButton];
    [self.view addSubview:self.getConfirmButton];
    
    [self.phoneT setFrame:CGRectMake(W(25), NAVIGATION_BarHeight + 1 + W(80), KWIDTH - W(25) * 2, 40)];
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.phoneT.left, self.phoneT.bottom, self.phoneT.width, 1)]];
    self.confimT.frame = self.phoneT.frame;
    self.confimT.y = self.phoneT.bottom + W(5);
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.phoneT.left, self.confimT.bottom, self.phoneT.width, 1)]];
    [self.nextButton setFrame:CGRectMake(self.phoneT.left, self.confimT.bottom + W(50), self.phoneT.width, W(40))];
    [self.getConfirmButton setFrame:CGRectMake(KWIDTH - self.phoneT.left - W(90), self.confimT.y + W(6), W(90), W(25))];
    
    self.getConfirmButton.centerY = self.confimT.centerY;
    
    [self setTextField:self.phoneT];
    [self setTextField:self.confimT];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapBack:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

#pragma mark tap delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIControl class]]) {
        return NO;
    }
    return true;
}


-(void)setTextField:(UITextField *)textField
{
    textField.delegate = self;
    textField.textColor = COLOR_LABELThreeCOLOR;
    textField.font = [UIFont systemFontOfSize:F(15)];
}

#pragma mark 点击下一步
-(void)tapNext:(UIButton *)sender
{
    DSWeak;
    [RequestApi phoneAndCodeWithPhone:self.phoneT.text smscode:self.confimT.text type:@"forget" Delegate:nil success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        [weakSelf receiveKeyBoard];
        LoginChangePasswordVC *chang = [[LoginChangePasswordVC alloc]initPhone:weakSelf.phoneT.text smsCode:self.confimT.text];
        [GB_Nav pushViewController:chang animated:YES];
    } failure:^(NSString *errostr) {
        [MBProgressHUD showError:errostr toView:weakSelf.view];
    }];
}

#pragma mark 获取验证码

-(void)tapConfirm:(UIButton *)sender
{
    BOOL isPhone = [ValidateRegularExpression validatePhone:self.phoneT.text];
    if (!isPhone)
    {
        [MBProgressHUD showError:@"请输入正确的手机号" toView:self.view];
        return;
    }
    DSWeak;
    [RequestApi getCodeWithTel:self.phoneT.text Type:@"forget" Delegate:nil success:^(NSDictionary *response) {
        [weakSelf receiveKeyBoard];
        [MBProgressHUD showError:@"发送成功" toView:weakSelf.view];
    } failure:^(NSString *errostr) {
        [MBProgressHUD showError:errostr toView:weakSelf.view];
    }];
}

-(void)receiveKeyBoard
{
    [self.view endEditing:YES];
}

-(void)tapBack:(UITapGestureRecognizer *)sender
{
    [self receiveKeyBoard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
