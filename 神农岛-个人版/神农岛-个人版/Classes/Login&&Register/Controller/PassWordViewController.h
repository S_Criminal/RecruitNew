//
//  PassWordViewController.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PassWordViewController : BasicVC
@property (nonatomic ,strong) NSString *phone;
@property (nonatomic ,strong) NSString *smsCode;

-(instancetype)initWithPhone:(NSString *)phone smsCode:(NSString *)smsCode;

@end
