//
//  ZTTabBar.m
//  MainView
//
//  Created by mengxi on 16/2/16.
//  Copyright © 2016年 mirror. All rights reserved.
//

#import "ZTTabBar.h"

@interface ZTTabBar ()

@property (nonatomic, weak) UIButton *plusBtn;
@property (nonatomic, weak) UILabel  *doteLabel;

@end

@implementation ZTTabBar


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundImage:[[UIImage alloc] init]];
        [self setBackgroundColor:[HexStringColor colorWithHexString:@"EBEBF1"]];
        

        UIButton *plusBtn = [[UIButton alloc] init];
//        [plusBtn setBackgroundImage:[UIImage imageNamed:@"对号"] forState:UIControlStateNormal];
//        [plusBtn setBackgroundImage:[UIImage imageNamed:@"对号"] forState:UIControlStateHighlighted];
        
        UIButton *addButton = [[UIButton alloc]init];
        [addButton setFrame:CGRectMake(KWIDTH/2-10, KHEIGHT-15, 20, 10)];
        [addButton setBackgroundImage:[UIImage imageNamed:@"对号"] forState:UIControlStateNormal];

        plusBtn.size = plusBtn.currentBackgroundImage.size;
        [plusBtn addTarget:self action:@selector(plusBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:plusBtn];
        self.plusBtn = plusBtn;
        
        
        UILabel * doteLabel = [[UILabel alloc]init];
        doteLabel.hidden    = YES;
        doteLabel.layer.masksToBounds = YES;
        doteLabel.layer.cornerRadius  = 5;
        doteLabel.backgroundColor = [UIColor redColor];
        [self addSubview:doteLabel];
        self.doteLabel = doteLabel;
    }
    return self;
}

- (void)plusBtnClick
{

    if ([self.delegate respondsToSelector:@selector(tabBarDidClickPlusButton:)]) {
        [self.delegate tabBarDidClickPlusButton:self];
    }
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    self.plusBtn.centerX = self.width*0.5;
    self.plusBtn.centerY = self.height*0.5;

    

    CGFloat tabBarButtonW = self.width / 5;
    CGFloat tabBarButtonIndex = 0;
    CGFloat boteX;
    CGFloat boteY;
    CGFloat doteW = 10;
    if (KWIDTH == 320) {
        boteX =tabBarButtonW*2-23;
        boteY = 6;
    }else if (KWIDTH == 375)
    {
        boteX =tabBarButtonW*2-29.5;
        boteY = 7;
    }else if (KWIDTH == 414)
    {
        boteX =tabBarButtonW*2-33;
        boteY = 8;
    }else
    {
        boteX =tabBarButtonW*2-23;
    }
    self.doteLabel.frame = CGRectMake(boteX,boteY, doteW,doteW);
    for (UIView *child in self.subviews) {
        Class class = NSClassFromString(@"UITabBarButton");
        if ([child isKindOfClass:class]) {
            
            // 设置x
            child.x = tabBarButtonIndex * tabBarButtonW;
            // 设置宽度
            child.width = tabBarButtonW;
            // 增加索引
            tabBarButtonIndex++;
            if (tabBarButtonIndex == 2) {
                tabBarButtonIndex++;
            }
        }
    }
}

@end
