//
//  CustomTabBarController.h
//  神农岛
//
//  Created by 宋晨光 on 16/3/30.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabBarController : UITabBarController
+(instancetype)shareWithMessageManager;
@end
