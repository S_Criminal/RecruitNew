
//  CustomTabBarController.m
//  神农岛
//
//  Created by 宋晨光 on 16/3/30.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "CustomTabBarController.h"
#import "ZTTabBar.h"

#import "FirstViewController.h"
//#import "NewFirstViewController.h"
#import "NewsTableViewController.h"
#import "WorkPlaceViewController.h"
#import "MyViewController.h"
//#import "MyScrollViewController.h"
#import "DiDiViewController.h"

@interface CustomTabBarController ()<ZTTabBarDelegate>
//@property(strong,nonatomic)User *user;

@end

@implementation CustomTabBarController


+(instancetype)shareWithMessageManager
{
    static CustomTabBarController * m = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        m = [[CustomTabBarController alloc]init];
    });
    return m;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    _user = [User shareWithInfo];
//    self.navigationController.interactivePopGestureRecognizer.enabled = NO;//不允许侧滑
    //添加navigationController
    
    // 添加子控制器
    [self addChildVc:[[FirstViewController alloc] init] title:@"首页" image:@"主页" selectedImage:@"首页H"];
    [self addChildVc:[[NewsTableViewController alloc] init] title:@"消息" image:@"消息" selectedImage:@"消息H"];
    [self addChildVc:[[DiDiViewController alloc]init] title:@"职场" image:@"职场" selectedImage:@"职场H"];
    [self addChildVc:[[MyViewController alloc]init] title:@"我的" image:@"我" selectedImage:@"我的H"];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    self.hidesBottomBarWhenPushed = NO;
}

- (void)addChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    childVc.title = title;
    childVc.tabBarItem.image = [UIImage imageNamed:image];
    // 禁用图片渲染
    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //设置字体和图片的距离
    [childVc.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, -5)];
    // 设置文字的样式
    [childVc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[HexStringColor colorWithHexString:@"666666" ]} forState:UIControlStateNormal];
    [childVc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:COLOR_MAINCOLOR} forState:UIControlStateSelected];
    // 为子控制器包装导航控制器
//    UINavigationController *navigationVc = [self creatNavigationControllerWithViewController:childVc];
    // 添加子控制器
    [self addChildViewController:childVc];
}

- (UINavigationController *)creatNavigationControllerWithViewController:(UIViewController *)viewController
{
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:viewController];
//    [nav setNavigationBarHidden:YES];
    [nav.navigationBar setBarTintColor:COLOR_MAINCOLOR];
    
    [nav.navigationBar setTitleTextAttributes:@{
                                                NSForegroundColorAttributeName:[UIColor whiteColor],
                                                NSFontAttributeName:[UIFont systemFontOfSize:18]
                                                }];
    
    nav.navigationBar.alpha = 1;
    return nav;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
