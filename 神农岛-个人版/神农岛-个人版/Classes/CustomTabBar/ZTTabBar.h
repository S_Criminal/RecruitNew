//
//  ZTTabBar.h
//  MainView
//
//  Created by mengxi on 16/2/16.
//  Copyright © 2016年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZTTabBar;


@protocol ZTTabBarDelegate <UITabBarDelegate>

@optional

- (void)tabBarDidClickPlusButton:(ZTTabBar *)tabBar;

@end

@interface ZTTabBar : UITabBar

@property (nonatomic, weak) id<ZTTabBarDelegate> delegate;

@end