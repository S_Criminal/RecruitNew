//
//  BriefEditPublishVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/31.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefEditPublishVC.h"

#import "MyExperienceGlobalView.h"
//选择器
#import "GeneralPickerView.h"
//时间model
#import "DateArrModel.h"
//request
#import "RequestApi+MyBrief.h"

@interface BriefEditPublishVC ()<ExperienceDelegate,GeneralPickerViewDelegate,UITextViewDelegate,RequestDelegate>

@property (nonatomic ,strong) BaseNavView *navView;
@property (nonatomic ,strong) MyExperienceGlobalView *titleView;    //作品标题
@property (nonatomic ,strong) MyExperienceGlobalView *booksView;    //出版作品
@property (nonatomic ,strong) MyExperienceGlobalView *dateView;    //作品标题
@property (nonatomic ,strong) MyExperienceGlobalView *contentView;    //作品标题

@property (nonatomic ,strong) UITextView *textView;

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) GeneralPickerView *pickerView;

@property (nonatomic ,strong) NSArray *yearArr;
@property (nonatomic ,strong) NSArray *monthArr;
@property (nonatomic ,strong) DateArrModel *dateModel;  //日期model

@property (nonatomic, weak) NSTimer *hideDelayTimer;
@property (nonatomic ,strong) UIButton *deleteButton;//删除


@end

@implementation BriefEditPublishVC

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.frame = CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT);
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

-(UITextView *)textView
{
    if (!_textView) {
        _textView = [UITextView new];
        _textView.backgroundColor = [HexStringColor colorWithHexString:@"f1f1f1"];
        _textView.textColor = COLOR_LABELThreeCOLOR;
        _textView.font = [UIFont systemFontOfSize:F(14)];
        _textView.delegate = self;
    }
    return _textView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.bgView];
    [self setNav];
    [self setUpViews];
    [self setModel];
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"出版作品" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }];
    self.navView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setModel
{
    DSWeak;
    /** 请求日期年月数据 */
    self.dateModel = [[DateArrModel alloc]init];
    int minYear = (int) self.dateModel.nowYear - 60 ;
    
    [self.dateModel setYearArrayModelWithMinIndex:minYear maxIndex:0 yearBlock:^(NSMutableArray *yArr) {
        weakSelf.yearArr = yArr;
    } monthBlock:^(NSMutableArray *mArr) {
        weakSelf.monthArr = mArr;
    }];
    
    
    
    /*
    [RequestApi getkillsListWithKey:[GlobalData sharedInstance].GB_Key sclass:@"1" Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        if (!kArrayIsEmpty(arr)) {
            AllImageModel *model = [AllImageModel modelObjectWithDictionary:arr.firstObject];
            weakSelf.model = model;
            [GlobalMethod removeAllSubViews:weakSelf.bgView];//只移除控件
            [weakSelf setUpViews];
        }
    } failure:^(NSString *errorStr, id mark) {
        
    }];
     */
}

//保存数据
-(void)saveInfo
{
    self.editID    = kObjectIsEmpty(self.model) ? @"0" : [GlobalMethod doubleToString:self.model.iDProperty];
    NSString *content   = self.textView.text;
    NSString *title     = self.titleView.textField.text;//还要判断和默认的内容是否不一致
    NSString *sdesp     = self.booksView.textField.text;
    NSString *sdate     = [self setChangeTime:self.dateView.label.text];
    NSString *sClass    = @"1";                     //1是出版作品
    
    if (kStringIsEmpty(title) || [title isEqualToString:@"作品标题"]) {
        [MBProgressHUD showError:@"请填写作品标题" toView:self.view];
        return;
    }
    if (kStringIsEmpty(sdesp) || [sdesp isEqualToString:@"出版作品或出版社"]) {
        [MBProgressHUD showError:@"请填写出版作品或出版社" toView:self.view];
        return;
    }
    if ([sdate isEqualToString:@"出版日期"]) {
        [MBProgressHUD showError:@"请填写出版日期" toView:self.view];
        return;
    }
    DSWeak;
    self.navView.backBtn.userInteractionEnabled = NO;
    [RequestApi editPersonAchieveWithKey:[GlobalData sharedInstance].GB_Key editID:self.editID contents:content spath:@"" sdesp:sdesp sdate:sdate stitle:title sclass:sClass Delegate:self success:^(NSDictionary *response) {
        self.navView.backBtn.userInteractionEnabled = YES;

        AllImageModel *model = [[AllImageModel alloc]init];
        model.sContents = content;
        model.sDesp = sdesp;
        model.sSdate = sdate;
        model.iDProperty = [response[@"SCID"] doubleValue];
        
        if ([weakSelf.editID isEqualToString:[GlobalMethod doubleToString:model.iDProperty]])
        {
            
            [[[GlobalData sharedInstance].GB_UserModel.datas4 mutableCopy] removeObject:weakSelf.model];
        }
        [[[GlobalData sharedInstance].GB_UserModel.datas4 mutableCopy] addObject:model];
        
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        
        [MBProgressHUD showSuccess:@"保存成功" toView:weakSelf.view];
        if (self.blockDele) {
            self.blockDele();
        }
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:weakSelf selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//布局
-(void)setUpViews
{
    NSString *bgColor   = @"222222";
    NSString *textColor = @"666666";
    
    NSString *title     = self.model.sTitle;
    NSString *bookTitle = self.model.sDesp;
    NSString *dateTitle = self.model.sSdate;
    NSString *content   = self.model.sContents;
    dateTitle = [self setChangeTime:dateTitle];

    _titleView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"作品标题" Title:title status:NO tag:100 frame:CGRectMake( 0, +3, KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    _booksView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"出版作品或出版社" Title:bookTitle status:NO tag:101 frame:CGRectMake( 0, _titleView.bottom  , KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    _dateView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"出版日期" Title:dateTitle status:YES tag:102 frame:CGRectMake( 0, _booksView.bottom + W(25), KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    _contentView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"作品简介" Title:content status:YES tag:1000 frame:CGRectMake(0,_dateView.bottom , KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    [self.textView setFrame:CGRectMake(15, CGRectGetMaxY(_contentView.frame) + W(10), KWIDTH - 15 * 2, W(100))];

    [self.bgView addSubview:_titleView];
    [self.bgView addSubview:_booksView];
    [self.bgView addSubview:_dateView];
    [self.bgView addSubview:_contentView];
    [self.bgView addSubview:_textView];
    
    //新增加的隐藏删除工作经历按钮
    if (![self.editID isEqualToString:@"0"])
    {
        [self createDeleteButton];
    }
}
//删除教育经历
-(void)createDeleteButton
{
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setFrame:CGRectMake(MyViewLeftConstraint, CGRectGetMaxY(_textView.frame) + 15, KWIDTH - MyViewLeftConstraint * 2, 45)];
    [self.deleteButton setCorner:5];
    self.deleteButton.backgroundColor = COLOR_MAINCOLOR;
    self.deleteButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
    [self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteButton setTitle:@"删除作品" forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(tapDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.deleteButton];
}

-(void)tapDelete:(UIButton *)sender
{
    DSWeak;
    NSLog(@"%@",self.editID);
    [RequestApi requestDelskillcertWithKey:[GlobalData sharedInstance].GB_Key ID:self.editID Delegate:self success:^(NSDictionary *response) {
        [[[GlobalData sharedInstance].GB_UserModel.datas4 mutableCopy] removeObject:self.model];
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
        if (self.blockDele) {
            self.blockDele();
        }
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        
    }];
    
    
}

-(NSString *)setChangeTime:(NSString *)time
{
    if (kStringIsEmpty(time)) {
        return time;
    }
    NSArray *array = [time componentsSeparatedByString:@"-"];
    
    NSString *one = array[0];
    NSString *two = array[1];
    
    one = [one stringByReplacingOccurrencesOfString:@"年" withString:@""];
    two = [two stringByReplacingOccurrencesOfString:@"月" withString:@""];
    time = [NSString stringWithFormat:@"%@-%@",one,two];
    if (!kStringIsEmpty(time) && time.length > 7) {
        time = [time substringToIndex:7];
    }
    return time;
}


#pragma mark 点击label的协议方法

-(void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(MyExperienceControl *)control
{
    
    [self.view endEditing:YES];
    
    if (tag == 100 || tag == 101 || tag == 103 || tag == 1000) {
        return;
    }

    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    _pickerView.indexTag = tag;
    
    NSString *title = @"选择出版日期";
    NSString *firstTitle = [NSString stringWithFormat:@"%ld",self.dateModel.nowYear];
    [_pickerView setUpPickerViewTitle:title selectFirstTitle:firstTitle selectSecondTitle:@"" selectModel:1 firstArray:self.yearArr secondArray:self.monthArr];
    _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    [self.view addSubview:_pickerView];
    [self showPickerView];
    
}

#pragma mark pickerView的协议
-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    NSLog(@"===content%@===",content);
    [self setLabel:self.dateView.label Content:content];
    [self hidePickerView];
}

-(UILabel *)setLabel:(UILabel *)label Content:(NSString *)content
{
    if (!kStringIsEmpty(content)) {
        label.text = content;
        label.textColor = COLOR_LABELThreeCOLOR;
    }
    return label;
}


-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc
{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}


@end
