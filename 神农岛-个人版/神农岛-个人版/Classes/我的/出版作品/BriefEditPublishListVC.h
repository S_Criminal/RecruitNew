//
//  BriefEditPublishListVC.h
//  神农岛-个人版
//
//  Created by 刘惠萍 on 2017/6/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectCell.h"
//model
#import "AllImageModel.h"
@interface BriefEditPublishListVC : UIViewController

@end









@interface BriefEditPublishListCell : ProjectCell
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(AllImageModel *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(AllImageModel *)model;
@end
