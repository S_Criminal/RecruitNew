//
//  AboutLDRecurt.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/13.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "AboutLDRecurt.h"

@interface AboutLDRecurt ()

@end

@implementation AboutLDRecurt

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self create];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"关于老刀招聘" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)create
{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x - W(100)/2,   W(150),  W(100),  W(100))];
    imageView.image = [UIImage imageNamed:@"矢量智能对象"];
    
    UILabel *labelOne = [[UILabel alloc]initWithFrame:CGRectMake(W(50), CGRectGetMaxY(imageView.frame) + W(25), KWIDTH - W(50) * 2, 21)];
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    NSLog(@"%@",infoDic);
    
    NSString *currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"%@",currentVersion);
    labelOne.text = [NSString stringWithFormat:@"当前版本%@",currentVersion];
    
    UIImageView *imageL = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.center.x-W(152 / 2), CGRectGetMaxY(labelOne.frame) + 20, W(152), 20)];
    imageL.centerX = self.view.centerX;
    imageL.image = [UIImage imageNamed:@"优秀"];
    
    labelOne.textAlignment = NSTextAlignmentCenter;
    labelOne.font = [UIFont systemFontOfSize:F(15)];
    labelOne.textColor =COLOR_LABELSIXCOLOR;
    
    UILabel *labelLast = [[UILabel alloc]initWithFrame:CGRectMake(50, KHEIGHT - 80, KWIDTH-100, 21)];
    labelLast.text = @"山东老刀网络科技有限公司";
    labelLast.textAlignment = NSTextAlignmentCenter;
    labelLast.font = [UIFont systemFontOfSize:F(13)];
    labelLast.textColor = COLOR_LABELSIXCOLOR;
    
    UILabel *labelLastSecond = [[UILabel alloc]initWithFrame:CGRectMake(50, (labelLast.frame.origin.y - 10 - 20), KWIDTH - 100, 20)];
    labelLastSecond.text = [NSString stringWithFormat:@"Copyright@ 2017"];
    labelLastSecond.textAlignment = NSTextAlignmentCenter;
    labelLastSecond.font = [UIFont systemFontOfSize:F(13)];
    labelLastSecond.textColor = COLOR_LABELSIXCOLOR;
    
    [self.view addSubview:imageView];
    [self.view addSubview:imageL];
    [self.view addSubview:labelOne];
    [self.view addSubview:labelLast];
    [self.view addSubview:labelLastSecond];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
