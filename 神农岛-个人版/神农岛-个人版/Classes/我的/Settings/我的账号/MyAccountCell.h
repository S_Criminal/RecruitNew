//
//  MyAccountCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;

-(void)setLeftTitle:(NSString *)left rightTitle:(NSString *)right;

@end
