//
//  MyAccountCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyAccountCell.h"

@implementation MyAccountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leftLabel.font = [UIFont systemFontOfSize:F(16)];
    self.rightLabel.font = [UIFont systemFontOfSize:F(15)];
    
    self.leftConstraint.constant = W(15);
    self.leftLabel.textColor = [HexStringColor colorWithHexString:@"272727"];
    self.rightLabel.textColor = [HexStringColor colorWithHexString:@"a0a1a1"];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)setLeftTitle:(NSString *)left rightTitle:(NSString *)right
{
    UIView *line  = [GlobalMethod addNavLineFrame:CGRectMake(self.leftConstraint.constant, 43, KWIDTH, 1)];
    line.hidden =  [left isEqualToString:@"手机号"] ? NO : YES;
    [self.contentView addSubview:line];
    
    self.rightConstraint.constant = line.hidden ? 0 : W(30);
    
    [GlobalMethod resetLabel:self.leftLabel text:left isWidthLimit:0];
    [GlobalMethod resetLabel:self.rightLabel text:right isWidthLimit:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
