//
//  ChangePasswordVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ChangePasswordVC.h"

#import "RequestApi+MyBrief.h"
@interface ChangePasswordVC ()
@property (nonatomic ,strong) UILabel *titleL;
@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic ,strong) UIButton *nextButton;
@property (nonatomic, weak) NSTimer *hideDelayTimer;

@end

@implementation ChangePasswordVC

-(instancetype)initOld:(NSString *)old
{
    ChangePasswordVC *change = [ChangePasswordVC new];
    change.old = old;
    return change;
}

-(UILabel *)titleL
{
    if (!_titleL) {
        _titleL = [UILabel new];
        [GlobalMethod setLabel:_titleL widthLimit:0 numLines:0 fontNum:F(17) textColor:[HexStringColor colorWithHexString:@"282828"] text:@"请输入新密码"];
    }
    return _titleL;
}

-(UITextField *)textField
{
    if (!_textField) {
        _textField = [UITextField new];
        _textField.placeholder = @"请输入新的密码";
        _textField.font = [UIFont systemFontOfSize:F(16)];
    }
    return _textField;
}

-(UIButton *)confirButton
{
    if (!_nextButton) {
        _nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_nextButton setCorner:4];
        [_nextButton setTitle:@"确认修改" forState:UIControlStateNormal];
        [_nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _nextButton.backgroundColor = COLOR_MAINCOLOR;
        [_nextButton addTarget:self action:@selector(tapConfirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"重置密码" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.titleL];
    [self.view addSubview:self.textField];
    [self.view addSubview:self.confirButton];
    self.titleL.leftTop = XY(W(20), NAVIGATION_BarHeight + 1 + W(50));
    self.titleL.centerX = KWIDTH / 2;
    [self.textField setFrame:CGRectMake(W(20), self.titleL.bottom + W(80), KWIDTH - W(20) * 2, W(40))];
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.textField.x, self.textField.bottom, self.textField.width, 1)]];
    [self.confirButton setFrame:CGRectMake(self.textField.x, self.textField.bottom + W(50), self.textField.width, W(44))];
}

-(void)tapConfirm:(UIButton *)button
{
    BOOL isRight = self.textField.text.length > 5;
    isRight ?  [self nextVC] : [MBProgressHUD showError:@"密码不能小于6位" toView:self.view];
}

-(void)nextVC
{
    DSWeak;
    [RequestApi vaildePasswordWithKey:[GlobalData sharedInstance].GB_Key oldPwd:self.old newpwd:self.textField.text Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"修改密码成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

-(void)handleHideTimer
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc
{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
