//
//  ReEmailVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ReEmailVC.h"
#import "RequestApi+MyBrief.h"
@interface ReEmailVC ()<UITextFieldDelegate>
@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic ,strong) UIButton *confirButton;

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@end

@implementation ReEmailVC

-(UITextField *)textField
{
    if (!_textField) {
        _textField = [UITextField new];
        _textField.placeholder = @"请输入新的邮箱";
        _textField.font = [UIFont systemFontOfSize:F(16)];
    }
    return _textField;
}

-(UIButton *)confirButton
{
    if (!_confirButton) {
        _confirButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_confirButton setCorner:4];
        [_confirButton setTitle:@"确认" forState:UIControlStateNormal];
        [_confirButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _confirButton.backgroundColor = COLOR_MAINCOLOR;
        [_confirButton addTarget:self action:@selector(tapConfirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"修改邮箱" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.textField];
    [self.view addSubview:self.confirButton];
    [self.textField setFrame:CGRectMake(W(20), NAVIGATION_BarHeight + 1 + W(15), KWIDTH - W(20) * 2, W(40))];
    [self.view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(self.textField.x, self.textField.bottom, self.textField.width, 1)]];
    [self.confirButton setFrame:CGRectMake(self.textField.x, self.textField.bottom + W(18), self.textField.width, W(44))];
}

-(void)tapConfirm:(UIButton *)button
{
    BOOL isRight = [ValidateRegularExpression validateEmail:self.textField.text];
    isRight ?  [self exchangeEmail] : [MBProgressHUD showError:@"请填写正确邮箱" toView:self.view];
}

#pragma mark 修改邮箱

-(void)exchangeEmail
{
    DSWeak;
    [RequestApi exchangeEmailWithKey:[GlobalData sharedInstance].GB_Key content:self.textField.text Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"修改邮箱成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

-(void)handleHideTimer
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)dealloc
{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
