//
//  MyAccountVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyAccountVC.h"
#import "MyAccountCell.h"
#import "BriefInfoModel.h"

#import "ReEmailVC.h"   //修改邮箱
#import "RePasswordVC.h"//修改密码
@interface MyAccountVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listLeftArr;
@property (nonatomic ,strong) NSArray *listRightArr;

@end

@implementation MyAccountVC

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = COLOR_LINESCOLOR;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setModel];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"我的账号" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setModel
{
    BriefInfoModel *model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    NSString *phone = kStringIsEmpty([GlobalData sharedInstance].GB_Phone) ? @"" : [GlobalData sharedInstance].GB_Phone;
    NSString *email = kStringIsEmpty(model.uEmail) ? @"" : model.uEmail;
    NSArray *leftAr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"手机号",@"邮箱", nil],[NSArray arrayWithObjects:@"修改密码",nil], nil];
    NSArray *rightAr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:phone,email, nil],[NSArray arrayWithObjects:@"",nil], nil];
    self.listLeftArr = leftAr;
    self.listRightArr = rightAr;
}

-(void)setBasicView
{
    [self.view addSubview:self.tableView];
    [self.tableView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
    [self.tableView registerNib:[UINib nibWithNibName:@"MyAccountCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MyAccountCell"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? 2 : 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [GlobalMethod addNavLineFrame:CGRectMake(0, 0, KWIDTH, W(10))];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return W(10);
}

-(MyAccountCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyAccountCell"];
    [cell setLeftTitle:self.listLeftArr[indexPath.section][indexPath.row] rightTitle:self.listRightArr[indexPath.section][indexPath.row]];
    
    if (!(indexPath.section == 0 && indexPath.row == 0))
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 1) {
        ReEmailVC *email = [ReEmailVC new];
        [self.navigationController pushViewController:email animated:YES];
    }else if (indexPath.section == 1){
        RePasswordVC *password = [RePasswordVC new];
        [self.navigationController pushViewController:password animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
