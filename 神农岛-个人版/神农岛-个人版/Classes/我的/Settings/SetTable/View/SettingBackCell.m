//
//  SettingBackCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "SettingBackCell.h"

@implementation SettingBackCell{
    CGFloat leftFont;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
        [self createViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    _cellHeight = 50;
    leftFont    = F(16);
    if (isIphone5) {
        _cellHeight = 44;
        
    }else if (isIphone6){
        
    }else if (isIphone6p){
        
    }
}

-(void)createViews
{
    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, _cellHeight)];
    
    _label.textAlignment = NSTextAlignmentCenter;
    _label.text = @"退出账号";
    _label.font = [UIFont systemFontOfSize:leftFont];
    
    _label.textColor = [HexStringColor colorWithHexString:@"fe8753"];
    [self addSubview:_label];
    _label.centerY = _cellHeight / 2.0f;
}

-(void)buildConstraint
{
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
