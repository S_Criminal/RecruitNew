//
//  SettingBackCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingModel.h"
@interface SettingBackCell : UITableViewCell
@property (nonatomic ,strong) UILabel *label;
@property (nonatomic ,strong) SettingModel *model;
@property (nonatomic) CGFloat cellHeight;

@end
