//
//  SettingTableVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "SettingTableVC.h"

#import "SettingBackCell.h"
#import "LeftAndRightLaelCell.h"

#import "SettingModel.h"
#import "GlobalLeftModel.h"

#import "UserAgreementViewController.h"     //用户协议
#import "PrivacyViewController.h"           //隐私设置
#import "MyAccountVC.h"                     //我的账号

#import "AboutLDRecurt.h"                   //关于老刀招聘

#import "LoginViewController.h"             //登录

#import "KYAlertView.h"

#import "AboutLDRecurt.h"


@interface SettingTableVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listArr;
@property (nonatomic ,strong) KYAlertView *kyAlertView;

@property (nonatomic ,strong) NSString *nickStr;

@end

@implementation SettingTableVC

#pragma mark 懒加载

-(NSArray *)listArr
{
    if (!_listArr) {
        _listArr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"我的账号",@"清除缓存", nil],[NSArray arrayWithObjects:@"隐私设置",nil],[NSArray arrayWithObjects:@"关于老刀招聘",@"用户协议", nil],[NSArray arrayWithObjects:@"退出账号", nil], nil];
    }
    return _listArr;
}

-(KYAlertView *)kyAlertView
{
    if (!_kyAlertView) {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    return _kyAlertView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self createTableView];
    
    _nickStr = kStringIsEmpty([GlobalData sharedInstance].isNick) ? @"0" : [GlobalData sharedInstance].isNick;
    NSLog(@"%ld",self.childViewControllers.count);
}

-(void)setNav
{
    self.navigationController.navigationBarHidden = YES;
    [self.view addSubview:[BaseNavView initNavTitle:@"设置" leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    /** 当隐私开启设置改变后 刷新一下tableview */
    if (![_nickStr isEqualToString:[GlobalData sharedInstance].isNick])
    {
        _nickStr = kStringIsEmpty([GlobalData sharedInstance].isNick) ? @"0" : [GlobalData sharedInstance].isNick;
        [self.tableView reloadData];
    }
}

-(void)createTableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, KWIDTH, KHEIGHT - NAVIGATIONBAR_HEIGHT) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.scrollEnabled = NO;
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = COLOR_LINESCOLOR;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3 || section == 1)
    {
        return 1;
    }
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];
    line.backgroundColor = COLOR_LINESCOLOR;
    return line;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]init];
    }
    if (indexPath.section == 3 && indexPath.row == 0) {
        SettingBackCell *cell = [[SettingBackCell alloc]init];
        tableView.rowHeight = cell.cellHeight;
        NSLog(@"%f",cell.cellHeight);
        return cell;
    }else{
        LeftAndRightLaelCell *cell = [[LeftAndRightLaelCell alloc]init];
        [cell setStyle:0 leftContent:self.listArr[indexPath.section][indexPath.row] rightContent:@""];
        tableView.rowHeight = cell.cellHeight;
        if (indexPath.section == 0 && indexPath.row == 1) {
            [cell setStyle:0 leftContent:self.listArr[indexPath.section][indexPath.row] rightContent:[self cleanString]];
        }else if (indexPath.section == 1 && indexPath.row == 0){
            [cell setStyle:0 leftContent:self.listArr[indexPath.section][indexPath.row] rightContent:[self nickString]];
        }
        return cell;
    }
    return cell;
}

//获取缓存
-(NSString *)cleanString
{
    //清除SD的图片缓存
    float ImageCache = [[SDImageCache sharedImageCache]getSize]/1000/1000;
    NSString * CacheString = [NSString stringWithFormat:@"%.2fM",ImageCache];
    NSLog(@"%@",CacheString);
    return CacheString;
}

-(NSString *)nickString
{
    NSString *str = [[GlobalData sharedInstance].isNick isEqualToString:@"0"] ? @"已关闭" : @"已开启";
    return str;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.hidesBottomBarWhenPushed = YES;
    if (indexPath.section == 0 && indexPath.row == 0) {
        MyAccountVC *account = [MyAccountVC new];
        [self.navigationController pushViewController:account animated:YES];
    }else if (indexPath.section == 0 && indexPath.row == 1) {
        [self clearDates];
    }else if (indexPath.section == 1 && indexPath.row == 0) {
        PrivacyViewController *privacy = [PrivacyViewController new];
        [self.navigationController pushViewController:privacy animated:YES];
    }else if (indexPath.section == 2 && indexPath.row == 1){
        UserAgreementViewController *vc = [[UserAgreementViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 2 && indexPath.row == 0){
        AboutLDRecurt *recurt = [[AboutLDRecurt alloc]init];
        [self.navigationController pushViewController:recurt animated:YES];
    }else if (indexPath.section == 3 && indexPath.row == 0){
        [GlobalData sharedInstance].GB_Key = nil;
        
        [GlobalMethod clearAllDates];
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
}

/** 清除缓存 */
-(void)clearDates
{
    DSWeak;
    [self.kyAlertView showAlertView:@"清除缓存" message:@"确认清除缓存" subBottonTitle:@"取消" cancelButtonTitle:@"确认" handler:^(AlertViewClickBottonType bottonType) {
        if (bottonType == AlertViewClickBottonTypeCancelButton)
        {
           [GlobalMethod clearAllDates];
           [MBProgressHUD showSuccess:@"清除缓存成功" toView:weakSelf.view];
           [self.tableView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
