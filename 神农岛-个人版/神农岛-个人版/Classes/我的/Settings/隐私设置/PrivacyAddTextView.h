//
//  PrivacyAddTextView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol tapSearchDelegate <NSObject>

-(void)selectSeachTitle:(NSString *)title;

@end

@interface PrivacyAddTextView : UIView

@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic ,strong) UIButton *searchB;

@property (nonatomic ,weak) id<tapSearchDelegate>delegate;

-(void)resectViewHeight:(CGFloat)height;

@end
