//
//  PrivacySetNickNameView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacySetNickNameView.h"

@implementation PrivacySetNickNameView{
    NSInteger cityRow;
    CGFloat titleFont;
    CGFloat viewLeftConstraint;
    CGFloat pickerViewHeight;
    CGFloat titleHeight;
}

-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH - viewLeftConstraint * 2, titleHeight)];
        _titleLabel.font = [UIFont systemFontOfSize:F(titleFont)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = COLOR_MAINCOLOR;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.text = @"设置昵称";
    }
    return _titleLabel;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = COLOR_MAINCOLOR;
        [_bgView setCorner:5];
    }
    return _bgView;
}

-(UIView *)contentView
{
    if (!_contentView) {
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

-(UIButton *)leftButton
{
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.backgroundColor = COLOR_MAINCOLOR;
        _leftButton.titleLabel.font = [UIFont systemFontOfSize:titleFont];
        _leftButton.tag = 0;
        [_leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [_leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_leftButton addTarget:self action:@selector(tapCancle:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftButton;
}

-(UIButton *)rightButton
{
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.backgroundColor = COLOR_MAINCOLOR;
        _rightButton.titleLabel.font = [UIFont systemFontOfSize:titleFont];
        [_rightButton setTitle:@"确定" forState:UIControlStateNormal];
        _rightButton.tag = 1;
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightButton addTarget:self action:@selector(tapConfirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)tapHidden:(UITapGestureRecognizer *)sender
{
    NSLog(@"%@",sender.view);
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapHiddenDelegate)]) {
        [self.delegate tapHiddenDelegate];
    }
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        [self setUI];
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.titleLabel];
        
        
        [self builtConstraint];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHidden:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

-(void)setUI
{
    viewLeftConstraint = W(20);
    titleFont = F(16);
    pickerViewHeight = W(150);
    titleHeight = W(50);
}

-(void)builtConstraint
{
    [self.bgView setFrame:CGRectMake(viewLeftConstraint, (KHEIGHT - pickerViewHeight) / 2 - titleHeight, KWIDTH - viewLeftConstraint * 2, titleHeight * 2 + pickerViewHeight)];
    [self.leftButton setFrame:CGRectMake(0, self.bgView.height - titleHeight, self.bgView.width / 2, titleHeight)];
    [self.rightButton setFrame:CGRectMake(self.bgView.width / 2, self.bgView.height - titleHeight, self.bgView.width / 2, titleHeight)];
    [self.contentView setFrame:CGRectMake(0, _titleLabel.bottom, self.bgView.width, pickerViewHeight)];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.leftButton];
    [self.bgView addSubview:self.rightButton];
    [self.bgView addSubview:self.contentView];
    [self.bgView addSubview:[GlobalMethod addVerticalLineFrame:CGRectMake(self.leftButton.right - 0.5, self.leftButton.y  + W(4), 1, titleHeight - W(8)) color:[UIColor whiteColor]]];
}

-(void)tapCancle:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapConfirmOrCancle:)]) {
        [self.delegate tapConfirmOrCancle:button.tag];
    }
}

-(void)tapConfirm:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapConfirmOrCancle:)]) {
        [self.delegate tapConfirmOrCancle:button.tag];
    }
}
@end
