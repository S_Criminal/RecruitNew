//
//  PrivacyAddTableCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyAddTableCell.h"

@implementation PrivacyAddTableCell

- (UIControl *)control{
    if (_control == nil) {
        _control = [UIControl new];
        _control.backgroundColor = [UIColor whiteColor];
        [GlobalMethod setRoundView:_control color:COLOR_LINESCOLOR numRound:6 width:0.5];
        _control.userInteractionEnabled = NO;
    }
    return _control;
}

- (UILabel *)label{
    if (_label == nil) {
        _label = [UILabel new];
        [GlobalMethod setLabel:_label widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _label;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.control];
        [self.control addSubview:self.label];
        [self.control addSubview:self.deleteButton];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(PrivacyAddTableCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(PrivacyAddCompanyModel *)model{
    //    [GlobalMethod removeAllSubView:self withTag:TAG_LINE];//移除线
    //刷新view
    self.control.frame = CGRectMake(W(15), W(0), KWIDTH - W(15) * 2, W(45));
    
    [GlobalMethod resetLabel:self.label text:model.cName isWidthLimit:false];
    
    self.label.leftTop = XY(W(20),W(0));
    [self.label sizeToFit];
    self.label.width = self.control.width - W(20) - W(38);
    self.label.centerY = self.control.centerY;
    
    return self.control.bottom;
    
}


@end
