//
//  PrivacyHeaderView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyHeaderView.h"

@implementation PrivacyHeaderView

-(UITextField *)textField{
    if (_textField == nil) {
        _textField = [UITextField new];
        _textField.font = [UIFont systemFontOfSize:F(15)];
        _textField.textAlignment = NSTextAlignmentLeft;
        _textField.textColor = COLOR_LABELThreeCOLOR;
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.placeholder = @"你的昵称";
        _textField.delegate = self;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.leftTop = XY(W(35), W(36));
        _textField.widthHeight = XY(KWIDTH - W(35) * 2, W(44));
        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, W(34), 0)];
//        _textField.userInteractionEnabled = NO;
        [_textField setEnabled:NO];
//        [_textField addTarget:self action:@selector(textFileAction:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _textField;
}

- (UIControl *)control{
    if (_control == nil) {
        _control = [UIControl new];
        _control.tag = 1;
        _control.backgroundColor = [UIColor clearColor];
//        _control.widthHeight = XY(SCREEN_WIDTH,W(0));
    }
    return _control;
}

- (UILabel *)contentLabel{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel new];
        [GlobalMethod setLabel:_contentLabel widthLimit:0 numLines:0 fontNum:F(17) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _contentLabel;
}

-(UILabel *)hideLabel
{
    if (_hideLabel == nil) {
        _hideLabel = [UILabel new];
        [GlobalMethod setLabel:_hideLabel widthLimit:0 numLines:0 fontNum:F(15) textColor:[HexStringColor colorWithHexString:@"999999"] text:@"-----简历对以下企业不可见-----"];
        _hideLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _hideLabel;
}

- (UIImageView *)leftIcon{
    if (_leftIcon == nil) {
        _leftIcon = [UIImageView new];
        _leftIcon.leftTop = XY(0, 0);
        _leftIcon.image = [UIImage imageNamed:@"user"];
        _leftIcon.frame = CGRectMake(12, -7, 14, 14);
    }
    return _leftIcon;
}

-(UIButton *)editButton{
    if (_editButton == nil) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _editButton.tag = 1;
        [_editButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        _editButton.backgroundColor = [UIColor clearColor];
        _editButton.titleLabel.font = [UIFont systemFontOfSize:W(18)];
        [_editButton setImage:[UIImage imageNamed:@"Pencil"] forState:UIControlStateNormal];
//        [GlobalMethod setRoundView:_editButton color:[UIColor clearColor] numRound:5 width:0];
        _editButton.leftTop = XY(self.textField.width - self.textField.height, 0);
        _editButton.widthHeight = XY(self.textField.height,self.textField.height);
    }
    return _editButton;
}


#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubView];
    }
    return self;
}
//添加subview
- (void)addSubView{
    [self addSubview:self.textField];
    [self addSubview:self.hideLabel];
    [self.textField.leftView addSubview:self.leftIcon];
    [self.textField addSubview:self.editButton];
    [self addSubview:self.control];
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    PrivacyHeaderView * view = [PrivacyHeaderView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithModel:(NSString *)str{
    [self.hideLabel setFrame:CGRectMake(self.textField.x, self.textField.bottom + W(36), KWIDTH - self.textField.x * 2, 20)];
    self.control.frame = self.textField.frame;
    self.textField.text = str;
//    [GlobalMethod removeAllSubView:self withTag:TAG_LINE];//移除线
//    //刷新view
//    self.textField.frame = CGRectMake(W(0), W(0), SCREEN_WIDTH, W(0));
//    
//    self.control.frame = CGRectMake(W(0), W(0), SCREEN_WIDTH, W(0));
//    
//    self.leftIcon.leftTop = XY(SCREEN_WIDTH,W(0));
//    self.leftIcon.image = [UIImage imageNamed:@""];
//    self.editButton.frame = CGRectMake(W(0), W(0), SCREEN_WIDTH, W(0));
    
}



-(void)btnClick:(UIButton *)sender
{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
