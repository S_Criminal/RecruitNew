//
//  PrivacyHeaderView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyHeaderView : UIView<UITextFieldDelegate>
@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic ,strong) UIControl *control;
@property (nonatomic ,strong) UIImageView *leftIcon;
@property (nonatomic ,strong) UIButton *editButton;
@property (nonatomic ,strong) UILabel *contentLabel;
@property (nonatomic ,strong) UILabel *hideLabel;

#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(NSString *)str;


@end
