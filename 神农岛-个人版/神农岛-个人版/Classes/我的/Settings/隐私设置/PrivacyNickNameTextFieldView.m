//
//  PrivacyNickNameTextFieldView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyNickNameTextFieldView.h"

@implementation PrivacyNickNameTextFieldView

-(UILabel *)titleL
{
    if (!_titleL) {
        _titleL = [UILabel new];
        [GlobalMethod setLabel:_titleL widthLimit:1 numLines:0 fontNum:F(16) textColor:COLOR_LABELSIXCOLOR text:@"设置后企业只能看到你的昵称,不会看到你的真实姓名"];
    }
    return _titleL;
}

-(UITextField *)textField
{
    if (!_textField) {
        _textField = [UITextField new];
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.placeholder = @"填写昵称";
        _textField.font = [UIFont systemFontOfSize:F(15)];
    }
    return _textField;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubView];
    }
    return self;
}

//添加subview
- (void)addSubView{
    [self addSubview:self.textField];
    [self addSubview:self.titleL];
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    PrivacyNickNameTextFieldView * view = [PrivacyNickNameTextFieldView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithModel:(NSString *)model
{
    self.titleL.leftTop = XY(W(15), W(20));
    self.titleL.width = self.width - W(15) * 2;
    
    [GlobalMethod resetLabel:self.titleL text:@"设置后企业只能看到你的昵称,不会看到你的真实姓名" isWidthLimit:1];
    [GlobalMethod setAttributeLabel:self.titleL content:@"设置后企业只能看到你的昵称,不会看到你的真实姓名" width:self.width - W(15) * 2];
    self.textField.leftTop = XY(self.titleL.left, self.titleL.bottom + W(15));
    self.textField.widthHeight = XY(self.titleL.width, W(30));
    
    self.textField.text = model;
    
    self.titleL.textAlignment = NSTextAlignmentCenter;
}


@end
