//
//  PrivacyCompanyNameCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrivacyModel.h"

@protocol DeleteCompany <NSObject>

-(void)deleteCompany:(NSString *)senderIndex;

@end

@interface PrivacyCompanyNameCell : UITableViewCell
@property (nonatomic ,strong) UIControl *control;
@property (nonatomic ,strong) UILabel *label;

@property (nonatomic ,strong) UIButton *deleteButton;
@property (nonatomic ,weak) id<DeleteCompany>delegate;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(PrivacyModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(PrivacyModel *)model;


@end
