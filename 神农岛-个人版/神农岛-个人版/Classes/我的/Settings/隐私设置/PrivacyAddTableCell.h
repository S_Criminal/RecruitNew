//
//  PrivacyAddTableCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PrivacyAddCompanyModel.h"


@interface PrivacyAddTableCell : UITableViewCell

@property (nonatomic ,strong) UIControl *control;
@property (nonatomic ,strong) UILabel *label;

@property (nonatomic ,strong) UIButton *deleteButton;


#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(PrivacyAddCompanyModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(PrivacyAddCompanyModel *)model;

@end
