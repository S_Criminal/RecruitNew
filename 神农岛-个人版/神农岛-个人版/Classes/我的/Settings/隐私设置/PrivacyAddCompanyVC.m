//
//  PrivacyAddCompanyVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyAddCompanyVC.h"
#import "PrivacyAddTextView.h"
#import "PrivacyAddTableView.h"

@interface PrivacyAddCompanyVC ()<UITextFieldDelegate,tapSearchDelegate>
@property (nonatomic ,strong) PrivacyAddTableView *addTableView;
@property (nonatomic ,strong) PrivacyAddTextView *headerView;
@end

@implementation PrivacyAddCompanyVC

-(UITableView *)addTableView
{
    if (!_addTableView) {
        _addTableView = [PrivacyAddTableView new];
        _addTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _addTableView;
}

-(PrivacyAddTextView *)headerView
{
    if (!_headerView) {
        _headerView = [PrivacyAddTextView new];
        _headerView.delegate = self;
        _headerView.textField.delegate = self;
    }
    return _headerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"新增屏蔽企业" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
    self.view.backgroundColor = COLOR_BGCOLOR;
}

-(void)setBasicView
{
    [self.view addSubview:self.headerView];
    [self.view addSubview:self.addTableView];
    [self.headerView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, W(44))];
    [self.headerView resectViewHeight:W(44)];
    [self.addTableView setFrame:CGRectMake(0, self.headerView.bottom , KWIDTH, KHEIGHT - self.headerView.bottom)];
    self.addTableView.backgroundColor = COLOR_BGCOLOR;
}

#pragma mark 点击搜索

-(void)selectSeachTitle:(NSString *)title
{
    [self.headerView.textField resignFirstResponder];
    [self.addTableView resectTableView:title];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.addTableView resectTableView:textField.text];
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
