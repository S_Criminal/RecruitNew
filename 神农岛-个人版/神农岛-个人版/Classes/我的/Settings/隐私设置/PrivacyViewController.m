//
//  PrivacyViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyViewController.h"

#import "PrivacyAddCompanyVC.h"     //添加屏蔽企业

#import "PrivacyHeaderView.h"
#import "RequestApi+MyBrief.h"
#import "PrivacyModel.h"
#import "PrivacyCompanyNameCell.h"

#import "PrivacySetNickNameView.h"  //设置昵称悬浮view
#import "PrivacyNickNameTextFieldView.h"

#import "BriefInfoModel.h"
@interface PrivacyViewController ()<UITableViewDelegate,UITableViewDataSource,DeleteCompany,RequestDelegate,TapHiddenDelegate,UITextFieldDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) NSMutableArray *listArr;

@property (nonatomic ,strong) PrivacyHeaderView *header;
@property (nonatomic ,strong) PrivacySetNickNameView *nickView;
@property (nonatomic ,strong) PrivacyNickNameTextFieldView *nickTitleView;

@property (nonatomic ,strong) BriefInfoModel *model;
@property (nonatomic ,strong) UISwitch *turn;

@property (nonatomic ,strong) NSString *nickName;

@property (nonatomic ,strong) UITextField *textField;

@property (nonatomic ,assign) NSInteger pageIndex;

@end


@implementation PrivacyViewController{
    CGFloat keyBoardChangeHeight;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT)];
    }
    return _bgView;
}

-(PrivacyHeaderView *)header
{
    if (!_header) {
        _header = [PrivacyHeaderView initWithModel:nil];
    }
    return _header;
}

-(PrivacySetNickNameView *)nickView
{
    if (!_nickView) {
        _nickView = [[PrivacySetNickNameView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _nickView.delegate = self;
        
    }
    return _nickView;
}

-(PrivacyNickNameTextFieldView *)nickTitleView
{
    if (!_nickTitleView) {
        _nickTitleView = [PrivacyNickNameTextFieldView initWithModel:nil];
    }
    return _nickTitleView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bgView.backgroundColor = COLOR_BGCOLOR;
    _pageIndex = 1;
    self.nickName = @"";
    [self.view addSubview:self.bgView];
    [self setUI];
    [self setNav];
    [self setBasicView];
    //[self setRefresh];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModelIndex:_pageIndex];
}

-(void)setUI
{
    keyBoardChangeHeight = W(150);
}

-(void)setNav
{
    UIView *switchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
    _turn = [[UISwitch alloc]initWithFrame:CGRectMake(30, 7, 45, 20)];
    [switchView addSubview:_turn];
    [_turn addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"隐私设置" leftImageName:@"left" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightView:switchView rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    self.nickName = kStringIsEmpty([GlobalData sharedInstance].nickName) ? @"" : [GlobalData sharedInstance].nickName;
    self.turn.on = [[GlobalData sharedInstance].isNick isEqualToString:@"0"] ? NO : YES;
    
    
    [self.header setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, W(155))];
    [_header resetViewWithModel:self.nickName];
    [_header.control addTarget:self action:@selector(tapName:) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableHeaderView = _header;
    [self.tableView registerClass:[PrivacyCompanyNameCell class] forCellReuseIdentifier:@"PrivacyCompanyNameCell"];
    [self.bgView addSubview:self.tableView];
}

#pragma mark 刷新 加载更多
-(void)setRefresh
{
    [self getMoreTableView];    //上拉加载
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    //    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    //    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}


//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark 点击昵称
-(void)tapName:(UIControl *)sender
{
    [self.view addSubview:self.nickView];
    [self.nickView.contentView addSubview:self.nickTitleView];
    [self.nickTitleView setFrame:CGRectMake(0, 0, self.nickView.contentView.width, self.nickView.contentView.height)];
    
    self.nickView.bgView .transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    self.nickView.alpha = 0;
    
    [UIView animateWithDuration:0.35f animations:^{
        self.nickView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        self.nickView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    [self.nickTitleView resetViewWithModel:self.nickName];
    self.textField = self.nickTitleView.textField;
    self.textField.delegate = self;
    
}

//隐藏nickNameView
-(void)tapHiddenDelegate
{
    [GlobalMethod removeView:self.nickView];
}

-(void)switchAction:(id)sender
{
    UISwitch *switchButton = (UISwitch *)sender;
    BOOL isButtonOn = [switchButton isOn];
    
    if (kStringIsEmpty(self.nickName)) {
        [MBProgressHUD showError:@"请填写昵称" toView:self.view];
        return;
    }
    
    [self setNickNameRequest:isButtonOn];
    
}

-(void)setNickNameRequest:(BOOL)isButtonOn
{
    DSWeak;
    if (isButtonOn) {
        [RequestApi turnPrivateNickWithKey:[GlobalData sharedInstance].GB_Key nickName:_nickName isNick:@"1" Delegate:self success:^(NSDictionary *response) {
            NSLog(@"%@",response);
            [GlobalData sharedInstance].isNick      = @"1";
            [GlobalData sharedInstance].nickName    = weakSelf.nickName;
        } failure:^(NSString *errorStr, id mark) {
            
        }];
    }else{
        [RequestApi turnPrivateNickWithKey:[GlobalData sharedInstance].GB_Key nickName:_nickName isNick:@"0" Delegate:self success:^(NSDictionary *response) {
            NSLog(@"%@",response);
            [GlobalData sharedInstance].isNick      = @"0";
            [GlobalData sharedInstance].nickName    = weakSelf.nickName;
        } failure:^(NSString *errorStr, id mark) {
            
        }];
    }
}

-(void)setModelIndex:(NSInteger)index
{
    DSWeak;
    if (index == 1)
    {
        _listArr = [NSMutableArray array];
    }
    
    [RequestApi getsClistWithKey:[GlobalData sharedInstance].GB_Key top:@"0" Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        if (!kArrayIsEmpty(arr)) {
            for (NSDictionary *dic in arr)
            {
                PrivacyModel *model = [PrivacyModel modelObjectWithDictionary:dic];
                [weakSelf.listArr addObject:model];
            }
            [weakSelf.tableView reloadData];
        }
        
        if (_pageIndex != 1 && arr.count == 0) {
            [MBProgressHUD showError:@"没有更多数据" toView:weakSelf.view];
        }
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark tableView  dataSorce

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(50))];
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(W(35), 10, 20, 20)];
    iconImage.image = [UIImage imageNamed:@"隐私设置加"];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(iconImage.right + W(12), 10, view.width - iconImage.right - W(12), 20)];
    [GlobalMethod setLabel:label widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELSIXCOLOR text:@"新增屏蔽企业"];
    [label sizeToFit];
    iconImage.centerY = view.centerY - 1;
    label.centerY = view.centerY - 1;
    [view addSubview:iconImage];
    [view addSubview:label];
    [view addSubview:[GlobalMethod addNavLineFrame:CGRectMake(W(35), iconImage.bottom + 5, view.width, 1)]];
    
    UITapGestureRecognizer *tapAdd = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAdd)];
    [view addGestureRecognizer:tapAdd];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return W(50);;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [PrivacyCompanyNameCell fetchHeight:_listArr[indexPath.row]];
}

-(PrivacyCompanyNameCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PrivacyCompanyNameCell *cell = [tableView  dequeueReusableCellWithIdentifier:@"PrivacyCompanyNameCell"];
    cell.delegate = self;
    [cell resetCellWithModel:_listArr[indexPath.row]];
    return cell;
}

-(void)deleteCompany:(NSString *)senderIndex
{
    DSWeak;
    [RequestApi deletePrivacyCompany:[GlobalData sharedInstance].GB_Key ids:senderIndex Delegate:self success:^(NSDictionary *response) {
        weakSelf.pageIndex = 1;
        [weakSelf setModelIndex:weakSelf.pageIndex];
    } failure:^(NSString *errorStr, id mark) {
        [MBProgressHUD showError:errorStr toView:weakSelf.view];
    }];
}

#pragma mark 新增屏蔽企业

-(void)tapAdd
{
    PrivacyAddCompanyVC *add = [PrivacyAddCompanyVC new];
    [GB_Nav pushViewController:add animated:YES];
}

#pragma mark 点击确定取消按钮

-(void)tapConfirmOrCancle:(NSInteger)index
{
    if (index == 0) {
        
    }else{
        self.nickName = self.nickTitleView.textField.text;
        
        [self setNickNameRequest:self.turn.on];
        [_header resetViewWithModel: self.nickName];
    }
    [self tapHiddenDelegate];
}

#pragma mark textField的代理方法

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self reciveKeyBoard];
    return YES;
}

-(void)reciveKeyBoard
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShows:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHides:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark --- 弹出键盘 ---
- (void)keyboarShows:(NSNotification *)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.nickView.frame;
        if (frame.origin.y == 0) {
            frame.origin.y = weakSelf.nickView.frame.origin.y - keyBoardChangeHeight;
            weakSelf.nickView.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark --- 收起键盘 ---
- (void)keyboarHides:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.nickView.frame;
        if (frame.origin.y < 0) {
            frame.origin.y = weakSelf.nickView.frame.origin.y + keyBoardChangeHeight;
            weakSelf.nickView.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    NSLog(@"dealloc===");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
