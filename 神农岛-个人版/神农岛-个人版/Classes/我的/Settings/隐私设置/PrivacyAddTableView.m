//
//  PrivacyAddTableView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyAddTableView.h"
#import "RequestApi+MyBrief.h"
#import "PrivacyAddTableCell.h"
#import "PrivacyAddCompanyModel.h"

@implementation PrivacyAddTableView{
    NSInteger _pageIndex;
    NSString *_lastID;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setRefresh];
        [self addViews];
        [self registerClass:[PrivacyAddTableCell class] forCellReuseIdentifier:@"PrivacyAddTableCell"];
    }
    return self;
}

-(void)addViews
{
    self.delegate = self;
    self.dataSource = self;
}

-(void)resectTableView:(NSString *)title
{
    if (kStringIsEmpty(title))
    {
        [MBProgressHUD showError:@"请填写要屏蔽企业的名称" toView:self];
        return;
    }
    _pageIndex = 1;
    self.title = title;
    [self setModelIndex:_pageIndex];
}

-(void)setModelIndex:(NSInteger)pageIndex
{
    _listArr = [NSMutableArray array];
    DSWeak;
    [RequestApi searchCompanyNameWithKey:[GlobalData sharedInstance].GB_Key top:@"0" cName:self.title Delegate:nil success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        for (NSDictionary *d in arr)
        {
            PrivacyAddCompanyModel *model = [PrivacyAddCompanyModel modelObjectWithDictionary:d];
            [self.listArr addObject:model];
        }
        _lastID = arr.lastObject[@"ID"];
        
        
        
        [weakSelf reloadData];
    } failure:^(NSString *errorStr, id mark) {
        NSLog(@"%@",errorStr);
    }];
}

#pragma mark 刷新 加载更多
-(void)setRefresh
{
    [self getMoreTableView];    //上拉加载
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    //    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    //    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.mj_footer = footer;
    [self.mj_footer endRefreshing];
}


//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelIndex:_pageIndex];
    [self.mj_footer endRefreshing];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [GlobalMethod addNavLineFrame:CGRectMake(0, 0, KWIDTH, W(10))];
    view.backgroundColor = COLOR_BGCOLOR;
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return W(10);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [PrivacyAddTableCell fetchHeight:_listArr[indexPath.section]];
}

-(PrivacyAddTableCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PrivacyAddTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrivacyAddTableCell"];
    [cell resetCellWithModel:_listArr[indexPath.section]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PrivacyAddCompanyModel *model = _listArr[indexPath.section];
    [RequestApi editAddCompanyNameWithKey:[GlobalData sharedInstance].GB_Key editID:@"0" cid:[GlobalMethod doubleToString:model.iDProperty] cName:model.cName Delegate:nil success:^(NSDictionary *response) {
        [GB_Nav popViewControllerAnimated:YES];
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
