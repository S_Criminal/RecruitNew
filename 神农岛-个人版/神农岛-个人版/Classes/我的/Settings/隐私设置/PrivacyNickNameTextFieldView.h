//
//  PrivacyNickNameTextFieldView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyNickNameTextFieldView : UIView
@property (nonatomic ,strong) UILabel *titleL;
@property (nonatomic ,strong) UITextField *textField;


#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(NSString *)model;

@end
