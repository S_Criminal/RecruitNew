//
//  PrivacyCompanyNameCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/19.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyCompanyNameCell.h"

@implementation PrivacyCompanyNameCell

#pragma mark 懒加载

- (UIControl *)control{
    if (_control == nil) {
        _control = [UIControl new];
        _control.tag = 1;
        [_control addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        _control.backgroundColor = [UIColor whiteColor];
        [GlobalMethod setRoundView:_control color:COLOR_LINESCOLOR numRound:6 width:0.5];
//        _control.widthHeight = XY(SCREEN_WIDTH,W(0));
    }
    return _control;
}

- (UILabel *)label{
    if (_label == nil) {
        _label = [UILabel new];
        [GlobalMethod setLabel:_label widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _label;
}

-(UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.backgroundColor = [UIColor clearColor];
        [_deleteButton setImage:[UIImage imageNamed:@"greenDelete"] forState:UIControlStateNormal];
        [_deleteButton addTarget:self action:@selector(tapDelete:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}


#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.control];
        [self.control addSubview:self.label];
        [self.control addSubview:self.deleteButton];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(PrivacyCompanyNameCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(PrivacyModel *)model{
//    [GlobalMethod removeAllSubView:self withTag:TAG_LINE];//移除线
    //刷新view
    self.control.frame = CGRectMake(W(35), W(0), KWIDTH - W(35) * 2, W(45));

    
    [GlobalMethod resetLabel:self.label text:model.cname isWidthLimit:false];
    
    self.deleteButton.tag = model.iDProperty;
    
    self.label.leftTop = XY(W(20),W(0));
    [self.label sizeToFit];
    self.label.width = self.control.width - W(20) - W(38);
    self.label.centerY = self.control.centerY;
    
    self.deleteButton.widthHeight = XY(W(45), W(45));
    self.deleteButton.leftTop = XY(self.control.width - W(45),0);
    
    return self.control.bottom + W(18);
}

-(void)btnClick:(UIControl *)sender
{
    
}

-(void)tapDelete:(UIButton *)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteCompany:)]) {
        [self.delegate deleteCompany:[NSString stringWithFormat:@"%ld",sender.tag]];
    }
}



@end
