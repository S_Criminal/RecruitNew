//
//  PrivacySetNickNameView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/11.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TapHiddenDelegate <NSObject>

-(void)tapHiddenDelegate;
-(void)tapConfirmOrCancle:(NSInteger)index;

@end

@interface PrivacySetNickNameView : UIView

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIView *contentView;

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UIButton *leftButton;
@property (nonatomic ,strong) UIButton *rightButton;

@property (nonatomic ,strong) id<TapHiddenDelegate>delegate;


@end
