//
//  PrivacyAddTextView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PrivacyAddTextView.h"

@implementation PrivacyAddTextView

-(UITextField *)textField
{
    if (!_textField) {
        _textField = [UITextField new];
        _textField.placeholder =@"请输入要屏蔽的企业名称" ;
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.returnKeyType = UIReturnKeySearch;// return键 标有search的搜索
        [_textField setValue:[UIColor grayColor]forKeyPath:@"_placeholderLabel.textColor"];//设置placrholder的字体颜色
        [_textField setValue:[UIFont boldSystemFontOfSize:12]forKeyPath:@"_placeholderLabel.font"];//设置placrholder的字体大小
        
        _textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 0)];
        UIImageView *searchImage = [[UIImageView alloc]initWithFrame:CGRectMake(W(14), -7, 14, 14)];
        searchImage.centerY = _textField.leftView.centerY;
        searchImage.image = [UIImage imageNamed:@"icon_search"];
        [_textField.leftView addSubview:searchImage];
        
        [_textField setCorner:4];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.font = [UIFont systemFontOfSize:F(14)];
    }
    return _textField;
}

-(UIButton *)searchB
{
    if (!_searchB) {
        _searchB = [UIButton buttonWithType:UIButtonTypeCustom];
        _searchB.backgroundColor = COLOR_MAINCOLOR;
        [_searchB setTitle:@"搜索" forState:UIControlStateNormal];
        [_searchB setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _searchB.titleLabel.font = [UIFont systemFontOfSize:F(16)];
        [_searchB setCorner:F(4)];
        [_searchB addTarget:self action:@selector(tapSearch) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchB;
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_BGCOLOR;
        [self addViews];
    }
    return self;
}

-(void)addViews{
    [self addSubview:self.textField];
    [self addSubview:self.searchB];
}

-(void)resectViewHeight:(CGFloat)height
{
    [self.textField setFrame:CGRectMake(W(15), 0, KWIDTH - W(15) * 2 - W(80) - W(10), height - 10)];
    [self.searchB setFrame:CGRectMake(self.textField.right + W(10), 0, W(80), height - 10)];
    self.searchB.centerY = height / 2;
    self.textField.centerY = height / 2;
    
    
    
}

-(void)tapSearch
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectSeachTitle:)]) {
        [self.delegate selectSeachTitle:self.textField.text];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
