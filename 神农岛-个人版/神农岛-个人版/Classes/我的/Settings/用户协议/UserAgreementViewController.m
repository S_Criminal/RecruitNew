//
//  UserAgreementViewController.m
//  神农岛
//
//  Created by mac on 16/6/11.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "UserAgreementViewController.h"

#define UserLabelColor @"#5D5D5D"
@interface UserAgreementViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;

@end

@implementation UserAgreementViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view addSubview:[BaseNavView initNavTitle:@"用户协议" leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];

    [self creatSubViews];
    NSLog(@"%ld",self.childViewControllers.count);
    
}


- (void)creatSubViews
{
    self.myScrollView.showsVerticalScrollIndicator = FALSE;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,W(15), KWIDTH, 20)];
    titleLabel.text = @"老刀招聘移动应用用户协议";
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.myScrollView addSubview:titleLabel];
    
    //第一条
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(titleLabel.frame) + W( 20 + 10), 200, 20)];
    label1.text = @"一、提示条款";
    label1.textColor = [UIColor blackColor];
    label1.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label1];
    
    NSString *str1 = @"1.老刀招聘（域名：x5x5.cn）指山东老刀网络科技有限公司（以下简称老刀网络）经营的包括基于互联网及移动网向服务使用人（以下简称用户）提供服务的网络服务平台，该所有权及运作权均归老刀网络公司所有。\n2.本协议作为老刀网络与用户签订的服务协议，用以约束用户使用老刀招聘服务（包括但不限于互联网网站、移动应用程序及其相关服务）的行为，用户进行注册、登陆或使用老刀招聘的行为，即被视为完全同意及接受本协议全部条款。";
    UILabel *detailLabel1 = [self creatLabelWithX:10 Y:label1.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str1 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel1];

    //第二条
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel1.frame.origin.y+detailLabel1.frame.size.height+20, KWIDTH-20, 20)];
    label2.text = @"二、老刀招聘服务";
    label2.textColor = [UIColor blackColor];
    label2.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label2];

    NSString *str2 = @"1.老刀招聘运用自己的操作系统，通过国际互联网络等手段为会员职位信息、投递反馈等网络服务。老刀招聘保留随时修改或中断服务而不需知照用户的权利。老刀招聘行使修改或中断服务的权利，不需对用户或第三方负责。\n本协议的修改权归老刀招聘所有。协议内容一旦发生变动，老刀招聘将会在用户进入下一步使用前的页面提示修改内容。如果用户同意改动，则点击“同意服务条款”按钮。如果用户不接受，则及时取消该用户的使用服务资格。用户要继续使用老刀招聘服务需要两方面的确认：\n1)首先确认服务条款及其变动。\n2)同意接受所有的服务条款限制。";
    UILabel *detailLabel2 = [self creatLabelWithX:10 Y:label2.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str2 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel2];
    
    //第三条
    UILabel *label3 = [self creatLabelWithX:10 Y:detailLabel2.frame.origin.y+detailLabel2.frame.size.height+20 width:KWIDTH-20 height:20 text:@"三、用户必须自行准备如下设备和承担如下开支：" color:[UIColor blackColor] font:18];
    [self.myScrollView addSubview:label3];
    
    NSString *str3 = @"1.上网设备，包括并不限于电脑或者其他上网终端、调制解调器及其他上网装置。\n2.上网开支，包括并不限于网络接入费、上网设备租用费等。\n3.如果用户购买了老刀招聘的付费服务，即视为同意支付其相关费用。付费失败将导致付费服务终止。由于服务终止导致用户或第三方受到的影响，老刀招聘不负任何责任。";
    UILabel *detailLabel3 = [self creatLabelWithX:10 Y:label3.frame.origin.y+label3.frame.size.height+15 width:KWIDTH-20 height:5 text:str3 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel3];
    
    //第四条
    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel3.frame.origin.y+detailLabel3.frame.size.height+20, KWIDTH-20, 20)];
    label4.text = @"四、保护用户隐私权";
    label4.textColor = [UIColor blackColor];
    label4.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label4];
    
    NSString *str4 = @"1.本协议所称之用户隐私包括被法律确认为隐私内容，并符合下述范围的信息：\n1)用户注册老刀招聘时，根据应用要求提供的个人信息；\n2)在用户使用老刀招聘服务、参加网站活动、或访问网站网页时，网站自动接收并记录的用户浏览器上的服务器数据，包括但不限于IP地址、网站Cookie中的资料及用户要求取用的网页记录；\n2.老刀网络不会向任何人出售或出借用户的个人信息，除非事先得到用户的许可。\n3.为服务用户的目的，老刀招聘能通过使用用户的个人信息，向用户提供服务，包括但不限于向用户推荐职位、发出活动和服务信息等。用户在享用老刀招聘服务的同时，同意接受老刀招聘服务提供的各类信息服务。\n4.老刀网络承诺不公开或透露老刀招聘用户的密码、姓名、手机号码等在本站的非公开信息，除非因用户本人的需要、法律或其他合法程序的要求、服务条款的改变或修订等。\n5.用户的个人信息将在下述情况下部分或全部被披露：\n    1)经用户同意，向第三方披露；\n    2)如用户是合格的知识产权人并已提起投诉，应被投诉人要求，向被投诉人披露，以便双方处理可能的权利纠纷；\n    3)根据法律的有关规定，或者行政或司法机构的要求，向第三方或者行政、司法机构披露；\n    4)如果用户出现违反中国有关法律或者网站政策的情况，需要向第三方披露；\n    5)为提供用户所要求的产品和服务，而必须和第三方分享用户的个人信息；\n    6)其他老刀网络根据法律或者网站政策认为合适的披露\n6.同时用户须做到：\n    1)用户名和昵称的注册与使用应符合网络道德，遵守中华人民共和国的相关法律法规。\n    2)用户名和昵称中不能含有威胁、淫秽、漫骂、非法、侵害他人权益等有争议性的文字。\n    3)注册成功后，用户必须保护好自己的帐号和密码，因用户本人泄露而造成的任何损失由用户本人负责。另外，每个用户都要对其帐户中的所有活动和事件负全责。用户可随时改变其密码和图标，也可以结束旧的帐户重开一个新帐户。用户若发现任何非法使用用户帐号或安全漏洞的情况，应当立即通告老刀招聘。\n    4)不得盗用他人帐号，由此行为造成的后果自负。\n7.由于老刀招聘是一个职业招聘服务平台，当用户向老刀招聘递交简历时，用户即同意老刀招聘及相关招聘企业拥有法律上许可的对用户简历进行查询及使用的权利，由此产生的一切后果（包括但不限于相关招聘企业或其工作人员错误或非法使用前述用户简历信息）老刀网络不承担任何法律责任。\n8.用户完全了解及同意，在使用老刀招聘服务过程中，可能存在由于不可抗力或老刀网络不能预料或不能控制的原因（包括但不限于计算机病毒、黑客攻击、系统不稳定、通信线路等原因）造成的风险（包括但不限于数据丢失、个人信息泄露等），因此造成的任何法律后果，老刀网络不承担任何法律责任。";
    UILabel *detailLabel4 = [self creatLabelWithX:10 Y:label4.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str4 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel4];
    
    //第五条
    UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel4.frame.origin.y+detailLabel4.frame.size.height+20, KWIDTH-20, 20)];
    label5.text = @"五、责任说明";
    label5.textColor = [UIColor blackColor];
    label5.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label5];
    
    NSString *str5 = @"1.基于技术和不可预见的原因而导致的服务中断，或者因用户的非法操作而造成的损失，老刀网络不负责任。\n2.用户应当自行承担一切因自身行为而直接或者间接导致的民事或刑事法律责任。";
    UILabel *detailLabel5 = [self creatLabelWithX:10 Y:label5.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str5 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel5];
    
    //第六条
    UILabel *label6 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel5.frame.origin.y+detailLabel5.frame.size.height+20, KWIDTH-20, 20)];
    label6.text = @"六、用户必须做到：";
    label6.textColor = [UIColor blackColor];
    label6.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label6];
    
    NSString *str6 = @"1.不得利用老刀招聘危害国家安全、泄露国家秘密，不得侵犯国家社会集体的和公民的合法权益，不得利用老刀招聘制作、复制和传播下列信息：\n    1)煽动抗拒、破坏宪法和法律、行政法规实施的；\n    2)煽动颠覆国家政权，推翻社会主义制度的；\n    3)煽动分裂国家、破坏国家统一的；\n    4)煽动民族仇恨、民族歧视，破坏民族团结的；\n    5)捏造或者歪曲事实，散布谣言，扰乱社会秩序的；\n    6)宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的；\n    7)公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的；\n    8)损害国家机关信誉的；\n    9)其他违反宪法和法律行政法规的；\n    10)进行商业广告行为的。\n2.未经老刀网络的授权或许可，任何用户不得借用老刀招聘的名义从事任何商业活动，也不得将老刀招聘作为从事商业活动的场所、平台或其他任何形式的媒介。禁止将老刀招聘用作从事各种非法活动的场所、平台或者其他任何形式的媒介。如用户违反上述规定，则老刀网络有权直接采取一切必要的措施，包括但不限于删除用户发布的内容、取消用户在网站获得的星级、荣誉以及虚拟财富，暂停或查封用户帐号，乃至通过诉讼形式追究用户法律责任等。\n3.老刀网络保留经自行裁决和判断而过滤、编辑或移除任何上述内容的权利。\n4.老刀网络对用户或任何第三方发布、存储或上传的任何内容或其任何损失或损害，均不负责也不承担责任，对用户可能遇到的任何错误、中伤、诽谤、诬蔑、不作为、谬误、淫秽、色情或亵渎，老刀网络也不承担责任。作为互动服务的提供者，老刀网络对老刀招聘用户在任何公共论坛、个人主页或其它互动区域提供的任何陈述、声明或内容均不承担责任。老刀网络保留在任何时候为任何理由而不经通知地移除、筛查或编辑老刀招聘网站上发布或存储的任何内容的权利，且其具有绝对的裁量权如此行为，用户须自行负责备份和替换其在老刀招聘发布或存储的任何内容，成本和费用自理。";
    UILabel *detailLabel6 = [self creatLabelWithX:10 Y:label6.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str6 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel6];
    
    //第7条
    UILabel *label7 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel6.frame.origin.y+detailLabel6.frame.size.height+20, KWIDTH-20, 20)];
    label7.text = @"七、版权说明";
    label7.textColor = [UIColor blackColor];
    label7.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label7];
    
    NSString *str7 = @"1.任何用户接受本服务协议，即表明该用户主动将其在任何时间段在本站发表的任何形式的信息的著作财产权，包括并不限于：复制权、发行权、出租权、展览权、表演权、放映权、广播权、信息网络传播权、摄制权、改编权、翻译权、汇编权以及应当由著作权人享有的其他可转让权利无偿独家转让给老刀网络所有，同时表明该用户许可老刀网络有权利就任何主体侵权而单独提起诉讼，并获得全部赔偿。本协议已经构成《著作权法》第二十五条所规定的书面协议，其效力及于用户在老刀招聘上发布的任何受著作权法保护的作品内容，无论该内容形成于本协议签订前还是本协议签订后。\n2.用户同意并明确了解上述条款，不将已发表于本站的信息，以任何形式发布或授权其它网站（及媒体）使用。\n3.同时，老刀网络保留删除站内各类不符合规定的评论而不通知用户的权利。\n4.老刀网络是老刀招聘的制作者,拥有此应用内容及资源的版权,受国家知识产权保护,享有对本应用声明的最终解释与修改权；未经老刀网络的明确书面许可,任何单位或个人不得以任何方式,以任何文字作全部和局部复制、转载、引用和链接。否则老刀网络将追究其法律责任。";
    UILabel *detailLabel7 = [self creatLabelWithX:10 Y:label7.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str7 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel7];
    
    //第8条
    UILabel *label8 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel7.frame.origin.y+detailLabel7.frame.size.height+20, KWIDTH-20, 20)];
    label8.text = @"八、免责声明：";
    label8.textColor = [UIColor blackColor];
    label8.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label8];
    
    NSString *str8 = @"1.老刀招聘手机客户端的各种版本，包括但不限于JAVA版、Windows Mobile版、Android版、iOS版、Symbian版、BREW版、QT版等，均援引适用本用户服务条款，老刀网络拥有并保留其提供的各种手机客户端及在手机客户端中展示内容的所有知识产权。\n2.任何老刀招聘手机客户端的手机品牌合作商，如Apple, AppStore，并不因合作获得老刀招聘手机客户端的知识产权，也非老刀招聘手机客户端的赞助商，因老刀招聘手机客户端侵犯了任何第三方知识产权的，老刀网络承担相应的法律责任。\n3.老刀网络对于任何包含、经由或连接、下载或从老刀招聘所获得的任何内容、信息或广告，不声明或保证其正确性或可靠性；并且对于用户经老刀招聘上的内容、广告、展示而购买、取得的任何产品、信息或资料，老刀网络不负保证责任。用户自行负担使用老刀招聘的风险。\n4.老刀网络有权但无义务，改善或更正老刀招聘任何部分之任何疏漏、错误。\n5.老刀招聘用户评论仅代表其个人的观点，并不表示老刀招聘赞同其观点或证实其描述，老刀网络不承担由此引发的法律责任。任何单位或者个人认为老刀招聘用户发布的任何信息侵犯其权利，均可以通过下列联系方式通知老刀招聘：\n    邮寄地址：山东省潍坊市胜利东街2546号北海商务大厦12楼 老刀网络\n    邮政编码：261061\n    收件人：老刀网络法务部\n    客服电话：0536-8899151\n    客服信箱：DiDi@x5x5.cn\n敬请按照以下格式提供下述信息：\n明确指出您声称被侵权的受版权保护的作品。\n明确指出您声称侵犯受版权保护作品的材料，以及使我们能在本网站上找到该材料的信息，例如一个指向侵权材料的链接。\n您的联络方式，以便我们能回复您的投诉，最好包括电子邮件地址和电话号码。\n纳入以下声明：“本人本着诚信原则，认为被指侵犯版权的材料未获得版权所有人、其代理或法律的授权”。\n纳入以下声明：“本人宣誓，此通知里的信息是准确的，本人是声称被侵犯的排他性权利的版权所有人，或获得授权代表该权利所有人行为，所言不实甘受伪誓处罚”。\n通知必须经被授权人签字，该被授权人是代表声称被侵犯的排他性权利的所有人而行为。\n我们建议您在提起通知或通知应答之前咨询您的法律顾问。此外，我们提请您注意：如果对版权侵权所作的指称不实，则您可能承担损害赔偿（包括各种费用和律师费）的责任。";
    UILabel *detailLabel8 = [self creatLabelWithX:10 Y:label8.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str8 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel8];
    
    //第9条
    UILabel *label9 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel8.frame.origin.y+detailLabel8.frame.size.height+20, KWIDTH-20, 20)];
    label9.text = @"九、侵权者政策";
    label9.textColor = [UIColor blackColor];
    label9.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label9];
    
    NSString *str9 = @"对于被视为侵犯他人知识产权的任何用户，老刀网络可自行决定限制其对老刀招聘的访问或终止其帐户。";
    UILabel *detailLabel9 = [self creatLabelWithX:10 Y:label9.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str9 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel9];
    
    //第10条
    UILabel *label10 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel9.frame.origin.y+detailLabel9.frame.size.height+20, KWIDTH-20, 20)];
    label10.text = @"十、保证否认声明";
    label10.textColor = [UIColor blackColor];
    label10.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label10];
    
    NSString *str10 = @"除非老刀网络以书面形式明确另行规定，否则本网站、其中所包含的材料以及老刀招聘上提供的或与之相关而提供的服务均以“现状”提供，不带任何类型的保证，无论明示还是默示。就老刀招聘上的服务、信息、内容以及材料，老刀网络明确否认所有其它明示或默示的保证，包括但不限于有关适销性、适合特定目的、所有权以及不侵权的默示保证。老刀网络不声明或保证老刀招聘的材料或服务是准确的、完整的、可靠的、当前的或无差错的，并且否认有关老刀招聘及其内容或其任何部分的准确性或专有性的任何保证或声明。\n老刀网络对与定价、文本或摄影有关的排字错误或疏忽不负责。虽然老刀网络力图使您能对老刀招聘的服务进行安全访问和使用，但老刀网络不能也不会声明或保证本网站或其服务器是不含病毒或其它有害因素的；因此您应使用业界公认的软件查杀任何下载文件中的病毒。";
    UILabel *detailLabel10 = [self creatLabelWithX:10 Y:label10.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str10 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel10];
    
    //第11条
    UILabel *label11 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel10.frame.origin.y+detailLabel10.frame.size.height+20, KWIDTH-20, 20)];
    label11.text = @"十一、适用法律和争议解决";
    label11.textColor = [UIColor blackColor];
    label11.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label11];
    
    NSString *str11 = @"1.老刀招聘由老刀网络控制并运营。老刀招聘可在中国及其他国家进行访问。用户和老刀招聘均受益于与老刀招聘有关的可预见法律环境的建立。因此，用户和老刀招聘明确同意，因用户使用老刀招聘而引起或与之相关的一切争议、权利主张或其它事项，均受中华人民共和国法律的管辖，但不考虑其法律冲突原则。\n2.用户同意，如果出现任何因老刀招聘引起或与之相关的争议，用户和老刀网络应首先本着诚信原则通过协商加以解决。如果协商不成，用户将同意接受老刀网络住所地法院管辖。";
    UILabel *detailLabel11 = [self creatLabelWithX:10 Y:label11.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str11 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel11];
    
    //第12条
    UILabel *label12 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel11.frame.origin.y+detailLabel11.frame.size.height+20, KWIDTH-20, 20)];
    label12.text = @"十二、可分性";
    label12.textColor = [UIColor blackColor];
    label12.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label12];
    
    NSString *str12 = @"1.老刀招聘由老刀网络控制并运营。老刀招聘可在中国及其他国家进行访问。用户和老刀招聘均受益于与老刀招聘有关的可预见法律环境的建立。因此，用户和老刀招聘明确同意，因用户使用老刀招聘而引起或与之相关的一切争议、权利主张或其它事项，均受中华人民共和国法律的管辖，但不考虑其法律冲突原则。\n2.用户同意，如果出现任何因老刀招聘引起或与之相关的争议，用户和老刀网络应首先本着诚信原则通过协商加以解决。如果协商不成，用户将同意接受老刀网络住所地法院管辖。";
    UILabel *detailLabel12 = [self creatLabelWithX:10 Y:label12.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str12 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel12];
    
    
    //第13条
    UILabel *label13 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel12.frame.origin.y+detailLabel12.frame.size.height+20, KWIDTH-20, 20)];
    label13.text = @"十三、冲突选择";
    label13.textColor = [UIColor blackColor];
    label13.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label13];
    
    NSString *str13 = @"本协议是老刀网络与老刀招聘用户之间的法律关系的重要文件，老刀网络或者老刀招聘用户的任何书面或者口头意思表示与本协议不一致的，均应当以本协议为准，除非本协议被老刀网络声明作废或者被新版本代替。";
    UILabel *detailLabel13 = [self creatLabelWithX:10 Y:label13.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str13 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel13];
    
    
    //第14条
    UILabel *label14 = [[UILabel alloc]initWithFrame:CGRectMake(10, detailLabel13.frame.origin.y+detailLabel13.frame.size.height+20, KWIDTH-20, 20)];
    label14.text = @"十四、问题与意见";
    label14.textColor = [UIColor blackColor];
    label14.font = [UIFont systemFontOfSize:18];
    [self.myScrollView addSubview:label14];
    
    NSString *str14 = @"如果用户对本协议或老刀招聘的使用还存有任何疑问，可以联系我们。";
    UILabel *detailLabel14 = [self creatLabelWithX:10 Y:label14.frame.origin.y+20+15 width:KWIDTH-20 height:5 text:str14 color:[HexStringColor colorWithHexString:UserLabelColor] font:14];
    [self.myScrollView addSubview:detailLabel14];
    
    self.myScrollView.contentSize = CGSizeMake(0, detailLabel14.frame.origin.y+detailLabel14.frame.size.height+60);
}

- (void)creatNavigationView
{
    self.navigationItem.title = @"用户协议";
    self.navigationController.navigationBarHidden = NO;

}
- (UILabel *)creatLabelWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height text:(NSString *)text color:(UIColor *)color font:(CGFloat)font
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(x , y, width, height)];
    label.font = [UIFont systemFontOfSize:font];
    
    NSString *jobTemptStr1 = text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:jobTemptStr1];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:W(7)];//调整行间距
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [jobTemptStr1 length])];
    
    label.numberOfLines = 0;
    label.attributedText = attributedString;
    [label sizeToFit];
    
    label.textColor = color;
    
    return  label;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
