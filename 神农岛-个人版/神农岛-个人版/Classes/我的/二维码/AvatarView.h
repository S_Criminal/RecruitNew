//
//  AvatarView.h
//  农管家
//
//  Created by mengxi on 16/6/1.
//  Copyright © 2016年 mirror. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^avatarOnclick)();

@interface AvatarView : UIView

@property (nonatomic,   copy) NSString  * imageUrl;
@property (nonatomic, assign) NSInteger   cornerRadius;
@property (nonatomic,   copy) NSString  * authStatus;

@end
