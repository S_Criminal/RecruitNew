//
//  QRCodeMoreCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/25.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "QRCodeMoreCell.h"

@implementation QRCodeMoreCell

#pragma mark 懒加载

- (UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        [GlobalMethod setLabel:_titleLabel widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _titleLabel;
}

-(UIView *)line
{
    if (!_line) {
        _line  = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.line];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(QRCodeMoreCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(NSString *)indexRow{
    //刷新view
    if ([indexRow isEqualToString:@"取消"])
    {
        self.titleLabel.textColor = [HexStringColor colorWithHexString:@"FF0600"];
    }
    [GlobalMethod resetLabel:self.titleLabel text:indexRow isWidthLimit:0];
    
    self.titleLabel.leftTop = XY(W(0),W(0));
    self.titleLabel.centerX = KWIDTH / 2;
    self.titleLabel.centerY = W(45) / 2;
    
    [self.line setFrame:CGRectMake(W(20), W(44), KWIDTH - W(20) * 2, 1)];
    
    return self.bottom;
}


@end
