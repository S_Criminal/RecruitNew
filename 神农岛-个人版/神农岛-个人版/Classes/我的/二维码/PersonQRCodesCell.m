//
//  PersonQRCodesCell.m
//  农管家
//
//  Created by mengxi on 16/4/14.
//  Copyright © 2016年 mirror. All rights reserved.
//

#import "PersonQRCodesCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AvatarView.h"
#import "BriefInfoModel.h"

@interface PersonQRCodesCell ()

@property (nonatomic, strong) AvatarView  * photoImage;
@property (nonatomic, strong) UILabel     * name;
@property (nonatomic, strong) UILabel     * identifiter;
@property (nonatomic, strong) UILabel     * address;
@property (nonatomic, strong) UIView      * lineView;

@property (nonatomic, strong) UIImageView * QRcode;
@property (nonatomic, strong) UIImageView * avatarQR;
@property (nonatomic, strong) UILabel     * abs;


@property (nonatomic ,strong) UIView *bgView;


@end


@implementation PersonQRCodesCell

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        [_bgView setFrame:CGRectMake(W(15), W(94), KWIDTH  - W(15) * 2, KHEIGHT - W(158) - W(60))];
        [_bgView setCorner:5];
    }
    return _bgView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.bgView];
        [self setUpAllViews];
    }
    return self;
}

#pragma mark - 界面
- (void)setUpAllViews
{

    BriefInfoModel *model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;

    //头像-大
    self.photoImage = [[AvatarView alloc]init];
//    self.photoImage.imageUrl = self.userInfo.icon;
    self.photoImage.cornerRadius = W(115) / 2;
//    self.photoImage.authStatus = self.userInfo.authStatus;
    self.photoImage.imageUrl = model.uHead;
   

    self.name = [[UILabel alloc]init];
    self.name.textAlignment = NSTextAlignmentCenter;
    self.name.text = model.uName;
    self.name.font = [UIFont systemFontOfSize:F(18)];
    self.name.textColor = COLOR_LABELThreeCOLOR;
    
    
    self.identifiter = [[UILabel alloc]init];
    self.identifiter.textColor = [UIColor whiteColor];
    self.identifiter.font = [UIFont systemFontOfSize:F(11)];
    self.identifiter.layer.masksToBounds = YES;
    self.identifiter.layer.cornerRadius = 2;
    self.identifiter.textAlignment = NSTextAlignmentCenter;
   
    self.address = [[UILabel alloc]init];
    self.address.textAlignment = NSTextAlignmentCenter;
    
    NSString *provience = !model.uProvince ? @"" : model.uProvince;
    NSString *city = !model.uCity ? @"" : model.uCity;

    if ([model.uProvince isEqualToString:model.uCity]) {
        self.address.text = [NSString stringWithFormat:@"%@ %@",@"中国",provience];
    }else{
        self.address.text = [NSString stringWithFormat:@"%@ %@ %@",@"中国",provience,city];
    }
    
    self.address.font = [UIFont systemFontOfSize:F(14)];
    self.address.textColor = COLOR_LABELThreeCOLOR;
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = COLOR_SEPARATE_COLOR;
   
    //二维码
    self.QRcode = [[UIImageView alloc]init];
    self.QRcode.image = [HexStringColor creatCodeGeneratorWithMessage:[GlobalData sharedInstance].GB_UID withSize:ADAPTER_SIZE_HEIGHT_IPhone6(265)];
   
    self.avatarQR = [[UIImageView  alloc]init];
    [self.avatarQR sd_setImageWithURL:[NSURL URLWithString:model.uHead] placeholderImage:[UIImage imageNamed:@"个人默认头像"]];
    self.avatarQR.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarQR.clipsToBounds = YES;
    self.avatarQR.layer.cornerRadius = 5;
    self.avatarQR.layer.masksToBounds = YES;
    
    self.abs = [[UILabel alloc]init];
    self.abs.text = @"扫一扫上面的二维码，加入老刀招聘";
    self.abs.textColor = COLOR_LABELSIXCOLOR;
    self.abs.font = [UIFont systemFontOfSize:F(13)];
    self.abs.textAlignment = NSTextAlignmentCenter;
    
    [self.bgView addSubview:self.abs];
    [self.QRcode addSubview:self.avatarQR];
    
    [self.bgView  addSubview:self.QRcode];
    [self.bgView addSubview:self.lineView];
    [self.bgView addSubview:self.address];
    [self.bgView addSubview:self.identifiter];
    
    [self.bgView addSubview:self.name];
    [self addSubview:self.photoImage];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.photoImage.frame = CGRectMake( 0, W(21), W(115), W(115));
    self.photoImage.centerX = KWIDTH / 2;
    
    self.name.frame = CGRectMake( 0, W(57), self.bgView.width, W(20));
    
    self.address.frame = CGRectMake( 0,self.name.bottom + W(10), self.bgView.width, W(20));
    
    self.lineView.frame = CGRectMake( 0, self.address.bottom + W(15), self.bgView.width, 1);
    
    self.QRcode.frame = CGRectMake( W(45) , self.lineView.bottom + W(20), W(240), W(240));
    self.QRcode.centerX = self.bgView.width / 2;
    
    self.avatarQR.frame    = CGRectMake((self.QRcode.width-50) / 2, (self.QRcode.height-50)/2, W(50), W(50));
 
    self.abs.frame         = CGRectMake(0, CGRectGetMaxY(self.QRcode.frame) + W(25), 0, 15);
    [self.abs sizeToFit];
    self.abs.centerX       = self.QRcode.centerX;
}

@end
