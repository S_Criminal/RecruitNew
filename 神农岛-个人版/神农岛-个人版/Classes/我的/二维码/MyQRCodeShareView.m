//
//  MyQRCodeShareView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/13.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyQRCodeShareView.h"

@implementation MyQRCodeShareView

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

#pragma mark 懒加载

- (UIImageView *)iconImgView{
    if (_iconImgView == nil) {
        _iconImgView = [UIImageView new];
        _iconImgView.image = [UIImage imageNamed:@"矢量智能对象"];
        _iconImgView.widthHeight = XY(W(32),W(32));
    }
    return _iconImgView;
}

- (UILabel *)titleL{
    if (_titleL == nil) {
        _titleL = [UILabel new];
        [GlobalMethod setLabel:_titleL widthLimit:0 numLines:0 fontNum:F(20) textColor:[HexStringColor colorWithHexString:@"44BC62"] text:@"老刀招聘·农业人才招聘平台"];
    }
    return _titleL;
}

- (UIImageView *)headImgView{
    if (_headImgView == nil) {
        _headImgView = [UIImageView new];
        _headImgView.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _headImgView.widthHeight = XY(W(158),W(158));
        [_headImgView setCorner:W(158) / 2];
    }
    return _headImgView;
}

- (UILabel *)nameL{
    if (_nameL == nil) {
        _nameL = [UILabel new];
        [GlobalMethod setLabel:_nameL widthLimit:0 numLines:0 fontNum:F(25) textColor:[HexStringColor colorWithHexString:@"010101"] text:@"隋菲菲"];
    }
    return _nameL;
}

- (UILabel *)textL{
    if (_textL == nil) {
        _textL = [UILabel new];
        [GlobalMethod setLabel:_textL widthLimit:0 numLines:0 fontNum:F(22) textColor:[HexStringColor colorWithHexString:@"999999"] text:@"我在「老刀招聘」找工作"];
    }
    return _textL;
}

- (UIView *)bottomView{
    if (_bottomView == nil) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = COLOR_LINESCOLOR;
        _bottomView.widthHeight = XY(KWIDTH, W(119));
    }
    return _bottomView;
}

- (UIImageView *)qrCodeImgView{
    if (_qrCodeImgView == nil) {
        _qrCodeImgView = [UIImageView new];
        _qrCodeImgView.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _qrCodeImgView.widthHeight = XY(W(75),W(75));
    }
    return _qrCodeImgView;
}

- (UILabel *)qrTitleLabel{
    if (_qrTitleLabel == nil) {
        _qrTitleLabel = [UILabel new];
        [GlobalMethod setLabel:_qrTitleLabel widthLimit:0 numLines:0 fontNum:F(18) textColor:COLOR_LABELThreeCOLOR text:@"「老刀招聘」APP"];
    }
    return _qrTitleLabel;
}

- (UILabel *)qrContentLabel{
    if (_qrContentLabel == nil) {
        _qrContentLabel = [UILabel new];
        [GlobalMethod setLabel:_qrContentLabel widthLimit:0 numLines:0 fontNum:F(14) textColor:COLOR_LABELThreeCOLOR text:@"长按识别二维码下载"];
    }
    return _qrContentLabel;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
        self.infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
        self.backgroundColor = [UIColor blackColor];
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubView];
    }
    return self;
}

//添加subview
- (void)addSubView{
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.iconImgView];
    [self.bgView addSubview:self.titleL];
    [self.bgView addSubview:self.headImgView];
    [self.bgView addSubview:self.nameL];
    [self.bgView addSubview:self.textL];
    [self.bgView addSubview:self.bottomView];
    [self.bottomView addSubview:self.qrCodeImgView];
    [self.bottomView addSubview:self.qrTitleLabel];
    [self.bottomView addSubview:self.qrContentLabel];
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    MyQRCodeShareView * view = [MyQRCodeShareView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model{
    //刷新view
    [self.bgView setFrame:CGRectMake(0, W(50), KWIDTH, KHEIGHT - W(50) * 2)];
    
    self.titleL.leftTop = XY(W(15),W(70));
    self.titleL.centerX = KWIDTH / 2 + W(13);
    
    self.iconImgView.rightTop = XY(self.titleL.left - W(15),W(15) + 100);
    self.iconImgView.centerY = self.titleL.centerY;
    
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:self.infoModel.uHead] placeholderImage:[UIImage imageNamed:@"个人默认图片"]];
    NSLog(@"%@",self.infoModel.uHead);
    
    self.headImgView.leftTop = XY(W(15),self.titleL.bottom+W(55));
    self.headImgView.centerX = KWIDTH / 2;
    
    [GlobalMethod resetLabel:self.nameL text:self.infoModel.uName isWidthLimit:0];
    self.nameL.leftTop = XY(W(15),self.headImgView.bottom + W(36));
    
    self.nameL.centerX = KWIDTH / 2;
    
    //[GlobalMethod resetLabel:self.textL text:@"我在" isWidthLimit:0];
    self.textL.leftTop = XY(W(15),self.nameL.bottom+W(15));
    self.textL.centerX = KWIDTH / 2;
    self.bottomView.leftTop = XY(0,self.textL.bottom+W(54));
    
    self.qrCodeImgView.leftTop = XY(W(22.5f), W(22));
    self.qrCodeImgView.image = [HexStringColor creatCodeGeneratorWithLDRecurtMessage:@"" withSize:265];
    
    self.qrTitleLabel.leftTop   = XY(self.qrCodeImgView.right + W(13), self.qrCodeImgView.y + W(10));
    self.qrContentLabel.leftTop = XY(self.qrCodeImgView.right + W(21), self.qrTitleLabel.bottom + W(15));
    
}

@end
