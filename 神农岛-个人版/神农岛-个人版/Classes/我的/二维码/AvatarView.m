//
//  AvatarView.m
//  农管家
//
//  Created by mengxi on 16/6/1.
//  Copyright © 2016年 mirror. All rights reserved.
//

#import "AvatarView.h"

@interface AvatarView ()
@property (nonatomic,strong) UIImageView * avatar;

@end

@implementation AvatarView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.avatar];
    }
    return self;
}


- (UIImageView *)avatar
{
	if(_avatar == nil) {
		_avatar = [[UIImageView alloc] initWithFrame:self.bounds];
        _avatar.contentMode = UIViewContentModeScaleAspectFill;
        _avatar.clipsToBounds = YES;
        _avatar.layer.masksToBounds = YES;
        _avatar.layer.borderWidth = 3;
        _avatar.layer.borderColor = [UIColor whiteColor].CGColor;
	}
	return _avatar;
}

- (void)setCornerRadius:(NSInteger)cornerRadius{
    _cornerRadius = cornerRadius;
    self.avatar.layer.cornerRadius = cornerRadius;
}

- (void)setImageUrl:(NSString *)imageUrl{
    _imageUrl = imageUrl;
//    _imageUrl = [imageUrl stringByAppendingString:@"@80w_80h_1e_1c"];
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:_imageUrl] placeholderImage:[UIImage imageNamed:@"个人默认头像"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
//    self.avatar.contentMode = UIViewContentModeScaleAspectFill;
//    self.avatar.clipsToBounds = YES;
    
    self.avatar.frame = self.bounds;
}

- (void)setAuthStatus:(NSString *)authStatus{
    _authStatus = authStatus;
    if ([authStatus isEqualToString:@"1"]) {
    
    }else{
        
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.avatar.frame = self.bounds;
}

@end
