//
//  MyQRCodeTapMoreTable.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/25.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyQRCodeTapMoreTable.h"
#import "QRCodeMoreCell.h"
@implementation MyQRCodeTapMoreTable

-(UIButton *)cancleButton
{
    if (!_cancleButton) {
        _cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancleButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancleButton setTitleColor:[HexStringColor colorWithHexString:@"FF0600"] forState:UIControlStateNormal];
        _cancleButton.titleLabel.font = [UIFont systemFontOfSize:F(15)];
    }
    return _cancleButton;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self setModel];
        [self registerClass:[QRCodeMoreCell class] forCellReuseIdentifier:@"QRCodeMoreCell"];
    }
    return self;
}

-(void)setModel
{
    self.delegate   = self;
    self.dataSource = self;
    self.listArr = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"保存图片",@"分享", nil],[NSArray arrayWithObjects:@"取消", nil], nil];
    [self reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = _listArr[section];
    return arr.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [GlobalMethod addNavLineFrame:CGRectMake(0, 0, KWIDTH, W(10))];
    view.backgroundColor = COLOR_BGCOLOR;
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0 ? 0 : W(10);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = W(45);
    return h;
}

-(QRCodeMoreCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QRCodeMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QRCodeMoreCell"];
    [cell resetCellWithModel:_listArr[indexPath.section][indexPath.row]];
    cell.line.hidden = indexPath.row == 0 && indexPath.section == 0 ? NO : YES;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.moreDelegate && [self.moreDelegate respondsToSelector:@selector(protocolQRCodeMoreSection:row:)]) {
        [self.moreDelegate protocolQRCodeMoreSection:indexPath.section row:indexPath.row];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
