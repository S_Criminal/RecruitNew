//
//  MyQRCodeShareView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/13.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefInfoModel.h"

@interface MyQRCodeShareView : UIView

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIImageView *iconImgView;
@property (nonatomic ,strong) UILabel *titleL;
@property (nonatomic ,strong) UIImageView *headImgView;
@property (nonatomic ,strong) UILabel *nameL;
@property (nonatomic ,strong) UILabel *textL;
@property (nonatomic ,strong) UIView *bottomView;
@property (nonatomic ,strong) BriefInfoModel *infoModel;

@property (nonatomic ,strong) UIImageView *qrCodeImgView;
@property (nonatomic ,strong) UILabel *qrTitleLabel;
@property (nonatomic ,strong) UILabel *qrContentLabel;

#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model;

@end
