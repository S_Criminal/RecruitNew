//
//  QRCodeMoreCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/25.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRCodeMoreCell : UITableViewCell

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UIView *line;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(NSString *)indexRow;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(NSString *)indexRow;

@end
