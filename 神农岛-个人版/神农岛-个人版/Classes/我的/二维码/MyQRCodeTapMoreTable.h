//
//  MyQRCodeTapMoreTable.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/25.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QRCodeMoreDelegate <NSObject>

-(void)protocolQRCodeMoreSection:(NSInteger)indexSection row:(NSInteger)row;

@end

@interface MyQRCodeTapMoreTable : UITableView<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UIButton *cancleButton;
@property (nonatomic ,strong) NSArray *listArr;
@property (nonatomic ,weak)   id<QRCodeMoreDelegate>moreDelegate;
@end
