//
//  MyQRCodeVC.m
//  农管家
//
//  Created by mengxi on 16/5/7.
//  Copyright © 2016年 mirror. All rights reserved.
//

#import "MyQRCodeVC.h"
//保存图片到相册
#import <AssetsLibrary/AssetsLibrary.h>
#import "PersonQRCodesCell.h"
#import "MyQRCodeTapMoreTable.h"

#import "CustomShareUI.h"
#import "BriefInfoModel.h"
#import "MyQRCodeShareView.h"       //要分享的二维码界面

@interface MyQRCodeVC ()<QRCodeMoreDelegate>

@property (nonatomic, strong) PersonQRCodesCell * QRCodeView;
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIControl *hiddenView;
@property (nonatomic ,strong) MyQRCodeTapMoreTable *tableView;
@property (nonatomic ,strong) CustomShareUI *shareView;

@end

@implementation MyQRCodeVC

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _bgView.backgroundColor = COLOR_MAINCOLOR;
    }
    return _bgView;
}

-(UIView *)hiddenView
{
    if (!_hiddenView) {
        _hiddenView = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _hiddenView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        _hiddenView.alpha = 0;
        [_hiddenView addTarget:self action:@selector(tapHiddens) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hiddenView;
}


-(MyQRCodeTapMoreTable *)tableView
{
    if (!_tableView) {
        _tableView = [MyQRCodeTapMoreTable new];
        _tableView.moreDelegate = self;
        _tableView.scrollEnabled = NO;
    }
    return _tableView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view addSubview:self.bgView];
    [self setNav];
    [self.bgView addSubview:self.QRCodeView];
    [self.view addSubview:self.hiddenView];
    [self.hiddenView addSubview:self.tableView];
    [self setModel];
}

-(void)setModel
{
//    share-curiculum.html?id=
}

-(void)setNav
{
    DSWeak;
    
    BaseNavView *navView = [BaseNavView initNavTitle:@"老刀二维码名片" leftImageName:@"white_delete" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightImageName:@"moreThreeDot" righBlock:^{
        
        [weakSelf showHiddenTableView];
    }];

    navView.labelTitle.textColor = [UIColor whiteColor];

    [navView.labelTitle sizeToFit];
    
    [self.bgView addSubview:navView];
    
}


-(void)showHiddenTableView
{
    DSWeak;
    [self.tableView setFrame:CGRectMake(0, KHEIGHT, KWIDTH, W(45) * 3 + W(10))];
    [UIView animateWithDuration:0.35f animations:^{
        weakSelf.hiddenView.alpha = 1;
        [weakSelf.tableView setFrame:CGRectMake(0, KHEIGHT - W(45) * 3 - W(10), KWIDTH, W(45) *3 + W(10))];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)tapHiddens
{
    DSWeak;
    [self.tableView setFrame:CGRectMake(0, KHEIGHT - W(45) * 3 - W(10), KWIDTH, W(45) *3 + W(10))];
    [UIView animateWithDuration:0.35f animations:^{
        weakSelf.hiddenView.alpha = 0;
        [weakSelf.tableView setFrame:CGRectMake(0, KHEIGHT, KWIDTH, W(45) * 3 + W(10))];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - Setter Getter 
- (PersonQRCodesCell *)QRCodeView
{
    if (!_QRCodeView)
    {
        _QRCodeView = [[PersonQRCodesCell alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1,KWIDTH , self.bgView.height - NAVIGATION_BarHeight)];
        [_QRCodeView setCorner:5];
    }
    return _QRCodeView;
}

//点击保存图片 和 取消
-(void)protocolQRCodeMoreSection:(NSInteger)indexSection row:(NSInteger)row
{
    if (indexSection == 0 && row == 0){
        NSLog(@"保存");
        [self saveQRCodeToDevice];
    }else if (indexSection == 0 && row == 1){

        
        [self tapHiddens];
        
        BriefInfoModel *model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@share-curiculum.html?id=%@",shareHttp,[GlobalMethod doubleToString:model.uID]]];
        NSString *title     = [NSString stringWithFormat:@"我叫%@",model.uName];
        
        MyQRCodeShareView *view = [MyQRCodeShareView initWithModel:nil];
        [view setFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        [view resetViewWithModel:nil];
        
        UIImage * qrImage = [HexStringColor captureImageFromView:view];
        
        self.shareView  = [[CustomShareUI alloc]initShareUIContent:@"我在老刀招聘找工作" title:title url:url images:qrImage shareEnum:shareContentsWithQRCode];
        [self.bgView addSubview:self.shareView];
        
    }else if (indexSection == 1 && row == 0){
        [self tapHiddens];
    }
}


#pragma mark 保存图片到本地

//- (void)loadImageFinished:(UIImage *)image
//{
//    __block ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
//    [lib writeImageToSavedPhotosAlbum:image.CGImage metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
//        
//        NSLog(@"assetURL = %@, error = %@", assetURL, error);
//        lib = nil;
//        
//    }];
//}

- (void)saveQRCodeToDevice
{
    UIImage * image = [HexStringColor captureImageFromView:self.bgView];
    
    __block ALAssetsLibrary * library = [ALAssetsLibrary new];
    
    NSData * data = UIImageJPEGRepresentation(image, 1.0);
    
    DSWeak;
    [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
        if (!error) {
            [MBProgressHUD showHint:@"保存成功" icon:@"Checkmark" view:weakSelf.view afterDelay:1.5];
            [weakSelf tapHiddens];
        }
        library = nil;
    }];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIControl class]])
    {
        return NO;
    }
    return YES;
}

@end
