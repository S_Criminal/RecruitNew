//
//  MyAddBriefBasicInfoVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyAddBriefBasicInfoVC.h"
#import "LeftAndRightLaelCell.h"

/** model */
#import "DataModels.h"
#import "DateArrModel.h"        //时间数据源

#import "ChangeCityModelToArray.h"
/** tool 图片相关 */
#import "ImagePickerViewController.h"
#import "FLImageShowVC.h"
/** 自定义选择器 */
#import "GeneralPickerView.h"
/** 自定义弹窗 */
#import "KYAlertView.h"
/** request */
#import "RequestApi+Login.h"
#import "RequestApi+MyBrief.h"

@interface MyAddBriefBasicInfoVC ()<UITableViewDataSource,UITableViewDelegate,GeneralPickerViewDelegate,RequestDelegate,ImagePickerViewControllerDelegate,UITextFieldDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UIImageView *headImageView;
@property (nonatomic ,strong) UITextField *nameT;
@property (nonatomic ,strong) UILabel *sexL;
@property (nonatomic ,strong) UILabel *birthL;
@property (nonatomic ,strong) UILabel *cityL;

@property (nonatomic ,strong) NSArray *leftContentArr;
@property (nonatomic ,strong) NSArray *rightContentArr;

@property (nonatomic ,strong) NSArray *sexArray;    //性别数组
@property (nonatomic ,strong) NSArray *yearArray;   //年份数组
@property (nonatomic ,strong) NSArray *monthArray;  //月份数组
@property (nonatomic ,strong) NSArray *provienceArr;//省份数组
@property (nonatomic ,strong) NSArray *cityArr;      //城市数组
@property (nonatomic ,strong) BriefInfoModel *infoModel;
@property (nonatomic ,strong) DateArrModel *dateModel;

@property (nonatomic ,strong) GeneralPickerView *pickerView;
/** 图片相关 */
@property (nonatomic, strong) NSMutableArray * imageDataArray;
@property (nonatomic, strong) NSMutableArray * coverDataArray;
@property (nonatomic, strong) UIScrollView * myScrollView;
@property (nonatomic, strong) UIScrollView * coverScrollView;
@property (nonatomic, strong) UIButton     * cameraButton;
@property (nonatomic, strong) UIButton     * titleButton;

@property (nonatomic ,strong) RequestApi *manager;
@property (nonatomic ,strong) NSString *uploadImageURL;

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@end

@implementation MyAddBriefBasicInfoVC{
    CGFloat leftFont;
    CGFloat rightFont;
    CGFloat imageWidth;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        [_bgView setCorner:5];
    }
    return _bgView;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(MyViewLeftConstraint, NAVIGATIONBAR_HEIGHT + 10, KWIDTH - MyViewLeftConstraint * 2, KHEIGHT - NAVIGATIONBAR_HEIGHT - TABBAR_HEIGHT) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 44;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        _tableView.scrollEnabled = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setCorner:5];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.isKeyBoarShow = NO;
    self.imageDataArray = [[NSMutableArray alloc]init];
    self.coverDataArray = [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
    UIView *backGroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
    backGroundView.backgroundColor = COLOR_BGCOLOR;
    [self.view addSubview:backGroundView];
    [self setUI];
    [self setNav];
    [self setModel];
    [self createBasicView];
}

-(void)setUI
{
    leftFont = F(16);
    rightFont = F(15);
    imageWidth = 50;
}


-(void)setNav
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.backgroundColor = [UIColor whiteColor];
    DSWeak;
    [navView addSubview:[BaseNavView initNavTitle:@"基本信息" leftImageName:@"" leftBlock:^{
        [weakSelf checkChange];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }]];

    [self.view addSubview:[GlobalMethod addNavLine]];
    [self.view addSubview:navView];
}

#pragma mark 检查是否做出更改

-(void)checkChange
{
    [_nameT resignFirstResponder];
    NSString *message = @"";
    
    
    if (!_infoModel) {
        if (!kStringIsEmpty(self.nameT.text) || !kStringIsEmpty(_uploadImageURL) || ![self.sexL.text isEqualToString:@"请选择"] || ![self.birthL.text isEqualToString:@"请选择"] || ![self.cityL.text isEqualToString:@"请选择"]) {
            message = @"基本信息尚未保存，是否保存？";
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }else if (![_infoModel.uName isEqualToString:self.nameT.text]) {
        message = @"您的姓名已更改，是否保存";
    }else if (![_infoModel.uHead isEqualToString:_uploadImageURL]) {
        message = @"您的头像已更改，是否保存";
    }else if (![_infoModel.uSex isEqualToString:self.sexL.text]) {
        message = @"您的性别已更改，是否保存";
    }
    
    if (kStringIsEmpty(message))
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    KYAlertView *alertView = [KYAlertView sharedInstance];
    DSWeak;
    [alertView showAlertView:@"提示" message:message subBottonTitle:@"不保存" cancelButtonTitle:@"保存" handler:^(AlertViewClickBottonType bottonType) {
        if (bottonType == AlertViewClickBottonTypeSubBotton) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }else if (bottonType == AlertViewClickBottonTypeCancelButton)
        {
            [weakSelf saveInfo];
        }
    }];
}

#pragma mark 数据源
-(void)setModel
{
 
    ChangeCityModelToArray *model = [ChangeCityModelToArray new];
    [model changeModelToArray];
    
    DSWeak;
    self.leftContentArr = [NSArray arrayWithObjects:@"头像",@"姓名",@"性别",@"出生",@"籍贯", nil];
    NSArray *infoArr = [GlobalData sharedInstance].GB_UserModel.datas0;
    _infoModel = infoArr.firstObject;
    _sexArray = [NSArray arrayWithObjects:@"男",@"女", nil];
    
    self.dateModel = [DateArrModel new];
    [_dateModel setYearArrayModelWithMinIndex:1960 maxIndex:0 yearBlock:^(NSMutableArray *yArr) {
        weakSelf.yearArray = yArr;
    } monthBlock:^(NSMutableArray *mArr) {
        weakSelf.monthArray = mArr;
    }];
    
    // 获取文件在沙盒中路径
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"area" ofType:@"plist"];
    // 获取省份数组
    self.provienceArr = [NSArray arrayWithContentsOfFile:filePath];
    // 获取第一个省市对应的城市数组
    self.cityArr = self.provienceArr[0][@"Cities"];
    
}

#pragma mark 保存信息
-(void)saveInfo
{
//    BOOL headImage
    BOOL head   = kStringIsEmpty(_uploadImageURL);
    BOOL name   = kStringIsEmpty(_nameT.text);
    BOOL sex    = kStringIsEmpty(_sexL.text) || [_sexL.text isEqualToString:@"请选择"];
    BOOL birth  = kStringIsEmpty(_birthL.text) || [_birthL.text isEqualToString:@"请选择"];
    BOOL city   = kStringIsEmpty(_cityL.text) || [_cityL.text isEqualToString:@"请选择"];
    
    if (head) {
        [MBProgressHUD showError:@"请上传头像" toView:self.view];
        return;
    }else if (name) {
        [MBProgressHUD showError:@"请填写姓名" toView:self.view];
        return;
    }else if (sex){
        [MBProgressHUD showError:@"请填写性别" toView:self.view];
        return;
    }else if (birth){
        [MBProgressHUD showError:@"请填写出生年月" toView:self.view];
        return;
    }else if (city){
        [MBProgressHUD showError:@"请填写籍贯" toView:self.view];
        return;
    }
    if (_nameT.text.length > 6) {
        [MBProgressHUD showError:@"姓名长度不能超过六位" toView:self.view];
        return;
    }
    
    NSString *birthday = _birthL.text;
    birthday = birthday.length > 0 ? [birthday stringByReplacingOccurrencesOfString:@"年" withString:@""] : birthday;
    birthday = birthday.length > 0 ? [birthday stringByReplacingOccurrencesOfString:@"月" withString:@""] : birthday;
    
    self.infoModel.uName = _nameT.text;
    self.infoModel.uSex  = _sexL.text;
    NSArray *cityArr    = [_cityL.text componentsSeparatedByString:@"-"];
    
    NSString *provience = cityArr.count > 0 ? cityArr.firstObject : @"";
    NSString *citys = cityArr.count > 1 ? cityArr[1] : cityArr.lastObject;
    
    DSWeak;
    [RequestApi saveNewBriefInfoWithKey:[GlobalData sharedInstance].GB_Key uhead:_uploadImageURL name:_nameT.text sex:_sexL.text birthday:birthday provience:provience city:citys Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }];
    NSLog(@"%@",_infoModel.uName);
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)createBasicView
{
    _headImageView = [[UIImageView alloc]init];
    _nameT         = [UITextField new];
    _nameT.delegate = self;
    _sexL          = [UILabel new];
    _birthL        = [UILabel new];
    _cityL         = [UILabel new];
    
    //右边控件赋值
    NSString *headImage;
    NSString *birth;
    if (!kObjectIsEmpty(_infoModel)) {
        headImage = _infoModel.uHead;
        _uploadImageURL = _infoModel.uHead;
        _nameT.text = _infoModel.uName;
        _sexL.text  = _infoModel.uSex;
        birth       = _infoModel.uBirthday;
        if (birth.length > 6)
        {
            _birthL.text= [_infoModel.uBirthday substringToIndex:7];
        }
        if (![_infoModel.uProvince isEqualToString:_infoModel.uCity]) {
            _cityL.text = [NSString stringWithFormat:@"%@-%@",_infoModel.uProvince,_infoModel.uCity];
        }else{
            _cityL.text = _infoModel.uCity;
        }
    }
    
    [_headImageView sd_setImageWithURL:[NSURL URLWithString:headImage] placeholderImage:[UIImage imageNamed:@"个人默认头像"]];
    [_headImageView setCorner:imageWidth / 2];
    _headImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage)];
    _headImageView.userInteractionEnabled = YES;
    [_headImageView addGestureRecognizer:tapImage];
    
    //右边控件设置UI
    _nameT  = [self upViewText:_nameT];
    _sexL   = [self setUpLabelStyle:_sexL];
    _birthL = [self setUpLabelStyle:_birthL];
    _cityL  = [self setUpLabelStyle:_cityL];
    
    //把右边控件都放到数组里
    _rightContentArr = [NSArray arrayWithObjects:_headImageView,_nameT,_sexL,_birthL,_cityL, nil];
    [self.view addSubview:self.tableView];
    CGFloat reduceHeight = 270;
    if (KWIDTH == 320) {
        reduceHeight = 270 - 4;
    }else if (KWIDTH == 414){
        reduceHeight = 270 + 6;
    }
    //_tableView.height = KWIDTH == 320 ? 270 - 6 : 270;
    //_tableView.height = KWIDTH == 414 ? 270 + 6 : 270;
    _tableView.height = reduceHeight;
}


-(UILabel *)setUpLabelStyle:(UILabel *)label
{
    label.text = [GlobalMethod exchangeEmpty:label.text];
    if (kStringIsEmpty(label.text))
    {
        label.text = @"请选择";
        label.textColor = [HexStringColor colorWithHexString:@"b6b6b6"];
    }else{
        label.textColor = COLOR_MAINCOLOR;
    }
    label.font = [UIFont systemFontOfSize:rightFont];
    label.textAlignment = NSTextAlignmentRight;
    return label;
}

-(UITextField *)upViewText:(UITextField *)textField
{
    textField.textColor = COLOR_MAINCOLOR;
    textField.placeholder = @"请填写";
    [textField setValue:[HexStringColor colorWithHexString:@"b6b6b6"]forKeyPath:@"_placeholderLabel.textColor"];
    textField.font = [UIFont systemFontOfSize:rightFont];
    textField.textAlignment = NSTextAlignmentRight;
    return textField;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (!cell)
    {
        cell = [UITableViewCell new];
    }
    UILabel *leftLabel = [UILabel new];
    leftLabel = [self setLeftLabel:leftLabel index:indexPath.row];
    UIView *line = [GlobalMethod addNavLine];
    
    [cell addSubview:leftLabel];
    [cell addSubview:_rightContentArr[indexPath.row]];
    [cell addSubview:line];
    
    [_rightContentArr[indexPath.row] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(15);
        make.bottom.mas_equalTo(-15);
        make.left.mas_equalTo(leftLabel.mas_right).offset(10);
        if (indexPath.row == 0)
        {
            make.width.height.mas_equalTo(imageWidth);
        }
    }];
    
    [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(100);
        make.centerY.mas_equalTo(_rightContentArr[indexPath.row]);
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(leftLabel.mas_left);
        make.top.mas_equalTo(leftLabel.mas_bottom).offset(14);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo(tableView.width - 15);
    }];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_nameT resignFirstResponder];
    if (indexPath.row < 2) {
        return;
    }
    //弹出pickerView
    [self setUpPickerView:indexPath.row];
}

-(void)setUpPickerView:(NSInteger)select
{
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    _pickerView.indexTag = 100 + select;
    
    NSString *title = self.leftContentArr[select];
    NSInteger selectModel = 1;      //判断是否是时间选择器 2为时间选择器 3城市数组
    
    NSArray *firstArr;
    NSArray *secondArr;
    NSString *selectFirstTitle;
    NSString *selectSecondTitle;
    
    if (select == 2) {
        firstArr = _sexArray;
        selectFirstTitle = [_sexL.text isEqualToString:@"请选择"] ? _sexArray.firstObject : _sexL.text ;
    }else if (select == 3){
        selectModel = 2;
        firstArr    = self.yearArray;
        secondArr   = self.monthArray;
        NSArray *arr = [_birthL.text componentsSeparatedByString:@"-"];
        selectFirstTitle = arr.count > 1 ? arr.firstObject : [NSString stringWithFormat:@"%ld",self.dateModel.nowYear - 16];
        selectSecondTitle = arr.count > 1 ? arr[1] : arr.lastObject;
        _pickerView.startTime = _birthL.text;
    }else if (select == 4){
        selectModel = 3;
        firstArr    = self.provienceArr;
        secondArr   = self.cityArr;
        NSArray *arr = [_cityL.text componentsSeparatedByString:@"-"];
        selectFirstTitle = arr.count > 1 ? arr.firstObject : @"";
        selectSecondTitle = arr.count > 1 ? arr[1] : arr.lastObject;
    }
    
    [_pickerView setUpPickerViewTitle:title selectFirstTitle:selectFirstTitle selectSecondTitle:selectSecondTitle selectModel:selectModel firstArray:firstArr secondArray:secondArr];
    [self.view addSubview:_pickerView];
    [self showPickerView];
    
}

#pragma mark textField代理方法

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSString *cardStatus = [NSString stringWithFormat:@"%@",[GlobalData sharedInstance].cardStauts];
    if ([cardStatus isEqualToString:@"2"] && textField == self.nameT)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"实名认证后，姓名不可更改" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    return YES;
}

#pragma mark pickerView的代理协议

-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}
#pragma mark 点击PickerView的确定按钮呢
-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    NSLog(@"===content%@===",content);
    if (tag == 102) {
        _sexL.text = content;
        [self setUpLabelStyle:_sexL];
    }else if (tag == 103){
        //判断年龄 是否小于16
        if ([[content substringToIndex:4] integerValue] + 16 > self.dateModel.nowYear) {
            [MBProgressHUD showError:@"年龄不能小于16岁" toView:self.view];
        }else{
            _birthL.text = content;
            [self setUpLabelStyle:_birthL];
        }
    }else if (tag == 104){
        _cityL.text = content;
        [self setUpLabelStyle:_cityL];
    }
    [self hidePickerView];
}

-(UILabel *)setLeftLabel:(UILabel *)label index:(NSInteger)indexRow
{
    label.text = self.leftContentArr[indexRow];
    label.textColor = COLOR_LABELSIXCOLOR;
    label.font = [UIFont systemFontOfSize:leftFont];
    return label;
}

-(void)tapImage
{
    ImagePickerViewController *ivc = [[ImagePickerViewController alloc] init];
    ivc.delegate = self;
    ivc.photoNumber = 1;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:ivc];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - ImagePickerViewControllerDelegate
- (void)imagePickerViewController:(ImagePickerViewController *)ivc everyImageClick:(ALAsset *)asset index:(NSInteger)index imageUrlArray:(NSArray *)imageUrlArray imageIndexArray:(NSArray *)imageIndexArray
{
    FLImageShowVC *fvc = [[FLImageShowVC alloc] init];
    fvc.albumImageUrlArray = imageUrlArray;
    fvc.currentIndex = index;
    fvc.imageIndexArray = (NSMutableArray *)imageIndexArray;
    [ivc.navigationController pushViewController:fvc animated:YES];
}

- (void)imagePickerViewController:(ImagePickerViewController *)ivc finishClick:(NSArray *)assetArray
{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"选中的所有相片的url，可以通过上面注释的方法获取图片:%@",assetArray);
    NSData * data;
    if (!kArrayIsEmpty(assetArray)) {
        ALAsset * asset = assetArray[0];
        UIImage * image = [UIImage imageWithCGImage:asset.aspectRatioThumbnail];
        data = UIImagePNGRepresentation(image);
    }
    if (!data || data == nil) {
        [MBProgressHUD showSuccess:@"抱歉，暂不支持该图片格式\n请重新上传" toView:self.view];
    }
    else
    {
        [self getImageURL:data];
    }
}

- (void)imagePickerViewController:(ImagePickerViewController *)ivc firstImageClick:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    [self getImageURL:imageData];
}

-(void)getImageURL:(NSData *)data
{
    DSWeak;
    [[RequestApi share] getImageUrlWithAliYunWithImageData:data BlockHandelImageUrlStr:^(NSString *str) {
        NSString *strURL = [NSString stringWithFormat:@"%@%@",ssndHTTP,str];
        [weakSelf.headImageView sd_setImageWithURL:[NSURL URLWithString:strURL]];
        weakSelf.uploadImageURL = strURL;
    } BlockHandelSuccess:^(BOOL success) {
        if (success)
        {
            
        }
    }];
}
#pragma mark - 继续 选择图片
- (void)againChoosePictureButtonClick:(UIButton *)button
{
    
}

#pragma mark - 删除 选中图片
- (void)deleteButtonClick:(UIButton *)button
{
    
}

-(void)dealloc
{
    self.hideDelayTimer = nil;
    [self.hideDelayTimer invalidate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
