//
//  PositionTrendView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PositionTrendView.h"

@implementation PositionTrendView{
    NSInteger _index;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, self.height)];
        _bgView.backgroundColor = [UIColor clearColor];
        [self addSubview:_bgView];
    }
    return _bgView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)setButtonArray:(NSArray *)array index:(NSInteger)index
{
    [GlobalMethod removeAllSubViews:self];
//    [self.bgView removeFromSuperview];
//    self.bgView = nil;
    _index = index;
    if (kArrayIsEmpty(array)) {
        [self createNoDateViewsWithIndex:index];
    }else{
        [self createViews:array];
    }
}

-(void)createNoDateViewsWithIndex:(NSInteger)index
{
    NSString *title;
    if (index == 0){
        title = @"请添加期望城市";
    }else if (index == 1){
        title = @"请添加期望职位";
    }else if (index == 2){
        title = @"请添加薪资要求";
    }else if (index == 3){
        title = @"请添加职业类型";
    }
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(55))];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:F(15)];
    button.tag = 50 + index;
    [button addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

-(void)createViews:(NSArray *)array
{
    CGFloat font = F(14);
    CGFloat buttonHeight = W(30);
    CGFloat left = 0;
    CGFloat w = 0;//保存前一个button的宽以及前一个button距离屏幕边缘的距离
    CGFloat h = W(15);//用来控制button距离父视图的高
    for (int i = 0; i < array.count; i++)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = 100 + i;
        [button addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.backgroundColor = COLOR_MAINCOLOR;
        [button setCorner:buttonHeight / 2];
        button.titleLabel.font = [UIFont systemFontOfSize:font];
        
        //根据计算文字的大小
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:font]};
        CGFloat length = [array[i] boundingRectWithSize:CGSizeMake(KWIDTH, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        //为button赋值
        [button setTitle:array[i] forState:UIControlStateNormal];
        if (_index == 0 || _index == 1)
        {
            [button setImage:[UIImage imageNamed:@"smallWhiteDelete"] forState:UIControlStateNormal];
            button.imageEdgeInsets = UIEdgeInsetsMake(0,length + W(8),0,0 - length - W(5));
            button.titleEdgeInsets = UIEdgeInsetsMake(0,-W(6),0, 0 + W(5));
        }
        
        
        //设置button的frame
        length = length + W(15);
        
        button.frame = CGRectMake(W(15) + w - left, h, length + W(29) , buttonHeight);
        
        //当button的位置超出屏幕边缘时换行 320 只是button所在父视图的宽度
        left = W(5);
        if(W(15) + w + length + W(15) > KWIDTH - W(10)){
            w = 0; //换行时将w置为0
            h = h + button.frame.size.height + W(10);//距离父视图也变化
            button.frame = CGRectMake(W(15) + w, h, length + W(29), buttonHeight);//重设button的frame
        }
        
        w = button.frame.size.width + button.frame.origin.x;
        [self addSubview:button];
    }
}



-(void)handleClick:(UIButton *)sender
{
    NSLog(@"%@",sender.titleLabel.text);
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolSelectButton:)]) {
        [self.delegate protocolSelectButton:sender];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
