//
//  PositionTrendView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PositionTrendDelegate <NSObject>

-(void)protocolSelectButton:(UIButton *)btn;

@end

@interface PositionTrendView : UIView

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,weak) id <PositionTrendDelegate> delegate;


-(void)setButtonArray:(NSArray *)array index:(NSInteger)index;

@end
