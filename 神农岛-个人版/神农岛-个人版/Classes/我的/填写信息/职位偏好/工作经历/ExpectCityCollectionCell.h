//
//  ExpectCityCollectionCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpectCityModel.h"

@protocol SelectCityDelegate <NSObject>

@optional

- (void)protocolCityBtnSelect:(NSUInteger)tag btn:(UIButton *)btn;

@end

@interface ExpectCityCollectionCell : UICollectionViewCell
@property (nonatomic ,strong) ExpectCityModel *model;
@property (nonatomic ,strong) NSString *title;
@property (nonatomic ,strong) UIButton *titleB;
@property (nonatomic ,strong) UILabel *titleL;

@property (nonatomic ,weak) id<SelectCityDelegate> delegate;
@end
