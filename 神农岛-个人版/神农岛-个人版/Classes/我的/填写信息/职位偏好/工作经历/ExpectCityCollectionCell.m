//
//  ExpectCityCollectionCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ExpectCityCollectionCell.h"

@implementation ExpectCityCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.titleB];
    }
    return self;
}

-(UIButton *)titleB
{
    if (!_titleB) {
//        _titleB = [UIButton buttonWithType:UIButtonTypeCustom];
        _titleB = [[UIButton alloc]init];
        _titleB.titleLabel.font = [UIFont systemFontOfSize:F(13)];
        [_titleB setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_titleB setBackgroundImage:[UIImage imageNamed:@"自助产品灰"] forState:UIControlStateNormal];
        [_titleB setBackgroundImage:[UIImage imageNamed:@"自助产品绿"] forState:UIControlStateSelected];
        [_titleB addTarget:self action:@selector(tapB:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _titleB;
}

-(void)setTitle:(NSString *)title
{
    _title = title;
    
    _titleB.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.titleB setTitle:title forState:UIControlStateNormal];
    
}


-(void)tapB:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolCityBtnSelect:btn:)]) {
        [self.delegate protocolCityBtnSelect:sender.tag btn:sender];
    }
}




@end
