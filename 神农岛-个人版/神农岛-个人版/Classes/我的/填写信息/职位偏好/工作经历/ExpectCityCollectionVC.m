//
//  ExpectCityCollectionVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ExpectCityCollectionVC.h"
#import "CityShortName.h"
#import "ExpectCityCollectionCell.h"
#import "ExpectCityModel.h"
#import "BriefInfoModel.h"

#import "RequestApi+MyBrief.h"
#import "RequestApi+Login.h"

//model
#import "ModelCityProvience.h"  //元素model
#import "ExpectCityModel.h"
#import "BriefInfoModel.h"

@interface ExpectCityCollectionVC ()<SelectCityDelegate,RequestDelegate>

@property(strong,nonatomic)NSMutableArray *upLoadCityArray;
@property(strong,nonatomic)NSMutableArray *upLoadProvinceArray;

@property (nonatomic ,strong) NSArray *datasArr;
@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) NSDictionary *jsonProvienceDic;
@property (nonatomic ,strong) NSArray *jsonRootArray;
@property (nonatomic ,strong) NSMutableArray *jsonProvienceItemArray;//首字母数组


@end

@implementation ExpectCityCollectionVC{
    NSArray *areaArr;
    NSArray *sectionArr;//section数组
    NSMutableArray *proviencesArr;
    NSMutableArray *listArr;
    NSDictionary *provincesDic;
}

static NSString * const reuseIdentifier = @"collectionCell";


- (instancetype)init
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    // 设置cell的尺寸
    layout.itemSize = [UIScreen mainScreen].bounds.size;
    // 清空行距
    layout.minimumLineSpacing = 0;
    
    // 设置滚动的方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    return [super initWithCollectionViewLayout:layout];
}

//设置collectionView
-(void)setCollection
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1 , KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    // Register cell classes
    [self.collectionView registerClass:[ExpectCityCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    // Regitster header section
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"1"];
    [self.collectionView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"期望城市（多选）";
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO
    [self setCollection];
    [self setModel];
    [self setNav];
    // Request data source
}

-(void)setNav
{
    self.view.backgroundColor = [UIColor whiteColor];
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"期望城市" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveExpectCity];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setModel
{
    //已经存在的期望城市数组
    _upLoadCityArray = [NSMutableArray array];
    _upLoadProvinceArray = [NSMutableArray array];
    
    NSArray *cityArray = [[GlobalData sharedInstance].city componentsSeparatedByString:@","];
    
    
    for (NSString *str in cityArray) {
        NSString *city = [CityShortName getAdressWithProvience:@"" City:str];
        if (!kStringIsEmpty(city))
        {
            [_upLoadCityArray addObject:city];
        }
        
    }
    
    //请求城市字典
    
    DSWeak;
    [RequestApi getPublicInformationWithCityDelegate:nil Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSString *jsonStr = response[@"datas"];
        [GlobalData sharedInstance].GB_JsonCitys = jsonStr;
        [weakSelf exChangeProvienceJsonDates:jsonStr];
        
    } failure:^(NSString *str) {
        //请求失败，加载本地数据
        [weakSelf exChangeProvienceJsonDates:[GlobalData sharedInstance].GB_JsonCitys];
    }];
    
    // 获取文件在沙盒中路径
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"area" ofType:@"plist"];
    // 获取省份数组
    areaArr = [NSArray arrayWithContentsOfFile:filePath];
    proviencesArr = [NSMutableArray array];
    
    for (NSDictionary *dic in areaArr)
    {
        [proviencesArr addObject:dic[@"State"]];//取出所有的省份列表
    }
    
    /*
    _upLoadCityArray = [NSMutableArray array];
    _upLoadProvinceArray = [NSMutableArray array];
    
    NSArray *cityArray = [[GlobalData sharedInstance].city componentsSeparatedByString:@","];
    

    for (NSString *str in cityArray) {
       NSString *city = [CityShortName getAdressWithProvience:@"" City:str];
        [_upLoadCityArray addObject:city];
    }
    
    // 获取文件在沙盒中路径
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"area" ofType:@"plist"];
    // 获取省份数组
    areaArr = [NSArray arrayWithContentsOfFile:filePath];
    proviencesArr = [NSMutableArray array];
    
    for (NSDictionary *dic in areaArr)
    {
        [proviencesArr addObject:dic[@"State"]];//取出所有的省份列表
    }
    
    NSString *provience = [[NSBundle mainBundle]pathForResource:@"citydict" ofType:@"plist"];
    provincesDic = [NSDictionary dictionaryWithContentsOfFile:provience];//以字母顺序排列的城市列表
    NSArray *resultArray = [NSMutableArray arrayWithArray:provincesDic.allKeys];
    
    //首字母排序
    sectionArr = [resultArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch];
        
    }];
    */
}

#pragma mark 处理json字符串 + 排序
-(void)exChangeProvienceJsonDates:(NSString *)jsonStr
{
    self.jsonProvienceItemArray = [NSMutableArray array];
    self.jsonProvienceDic = [GlobalMethod exchangeStringToDic:jsonStr];
    NSArray *resultArray = [NSMutableArray arrayWithArray:self.jsonProvienceDic.allKeys];
    //首字母排序
    //sectionArr  分组头数组
    sectionArr = [resultArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch];
        
    }];
    
    for (int i = 0; i < sectionArr.count; i ++)
    {
        NSArray *arr = [self.jsonProvienceDic objectForKey:sectionArr[i]];
        [self.jsonProvienceItemArray addObject:arr];
    }
    
    [self.collectionView reloadData];
    
}

#pragma mark 保存
-(void)saveExpectCity
{
    
    if (_upLoadCityArray.count == 0) {
        [MBProgressHUD showError:@"至少选择一个期望城市" toView:self.view];
        return;
    }
    
    NSString *rProvience;
    NSString *rCity;
    //    NSArray *_upLoadCityArr = [_briManager.cityStr componentsSeparatedByString:@","];
    NSMutableArray *provinceArr = [NSMutableArray array];
    NSMutableArray *_upLoadProvinceArr = [NSMutableArray array];
    
    NSString *str;
    NSLog(@"%@",areaArr);
    
    for (NSDictionary *dic in areaArr)
    {
        NSString *provience = dic[@"State"];
        
        [provinceArr addObject:provience];
    }
    
    for (str in _upLoadCityArray)
    {
        for (NSString *provience in provinceArr)
        {
            NSString *prov = [CityShortName getAdressWithProvience:@"" City:provience];
            
            if ([str isEqualToString:prov])
            {
                [_upLoadProvinceArr addObject:str];
            }
        }
    }
    
    NSLog(@"%@",_upLoadProvinceArr);
    
    for (str in _upLoadCityArray)
    {
        for (NSDictionary *dic in areaArr)
        {
            NSMutableArray *arr = dic[@"Cities"];
            NSString *provience = dic[@"State"];
            
            for (NSDictionary *dic2 in arr)
            {
                NSLog(@"%@",dic2);
                NSString *state = dic[@"State"];
                
                NSString *dic2AllValues = [CityShortName getAdressWithProvience:@"" City:[dic2.allValues componentsJoinedByString:@","]] ;
                NSArray *dic2AllValuesArray = [dic2AllValues componentsSeparatedByString:@","];
                
                //if (dic2AllValuesArray containsObject:<#(nonnull id)#>) {
                //    <#statements#>
               // }
                if ([dic2AllValues containsString:str])
                {
                    NSLog(@"%@",dic2AllValuesArray);
                    
                    [_upLoadProvinceArr addObject:provience];
                }
            }
        }
    }
    
    NSString *prov = [_upLoadProvinceArr componentsJoinedByString:@","];
    NSLog(@"%@",prov);
//    _briManager.provienceStr = prov;
    NSString *city = [_upLoadCityArray componentsJoinedByString:@","];
    NSLog(@"%@",city);
    
    rCity = city;
    rProvience = prov;
    
    
    //BriefInfoModel *infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    //infoModel.rCity = rCity;
    //infoModel.rProvince = prov;
    
    //[GlobalData sharedInstance].city = rCity;
    //[GlobalData sharedInstance].provience = prov;
    //[GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
    
    [RequestApi upLoadNewBriefCityWithKey:[GlobalData sharedInstance].GB_Key rprovince:rProvience rcity:rCity Delegate:self success:^(NSDictionary *response) {
        
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        self.hideDelayTimer = timer;

    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc
{
    self.hideDelayTimer = nil;
    [self.hideDelayTimer invalidate];
}


//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize size={320,W(40)};
    return size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView * header;
        
        
        header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"1" forIndexPath:indexPath];
        
        for (UIView *view in header.subviews)
        {
            [view removeFromSuperview];
        }
        
        header.backgroundColor = COLOR_BGCOLOR;
        UILabel * title = [[UILabel alloc]initWithFrame:CGRectMake(W(15), 5, 200, 30)];
        
        NSMutableArray *arr = [NSMutableArray array];
        [arr addObject:@"省份列表"];
        [arr addObjectsFromArray:sectionArr];
        title.text = arr[indexPath.section];
        title.centerY = W(40) / 2;
        title.textColor = [UIColor blackColor];
        title.font = [UIFont systemFontOfSize:F(15)];
        [header addSubview:title];
        return header;
    }
    return nil;
}
#pragma mark <UICollectionViewDelegateFlowLayout> methods
// 指定单元格大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    CGFloat height = 30;
    if (isIphone5) {
        height = 25;
    }
    size = CGSizeMake ( KWIDTH / 4 + 4, height);
    return size;
}
//设置每个组的边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(W(15), W(15), W(15),W(15));
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
//    return 2;
    return sectionArr.count + 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.jsonProvienceItemArray.count == 0) return 0;

    if (section == 0)
    {
        return proviencesArr.count;
    }
    
    NSMutableArray *arr = self.jsonProvienceItemArray[section - 1];
    return arr.count ;
}

- (ExpectCityCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ExpectCityCollectionCell *cell = (ExpectCityCollectionCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    NSString *str ;
    
    if (self.jsonProvienceItemArray.count == 0)
    {
        return cell;
    }
    if (indexPath.section == 0) {
        str = proviencesArr[indexPath.row];
    }else{
        ModelCityProvience *model = [ModelCityProvience modelObjectWithDictionary:self.jsonProvienceItemArray[indexPath.section - 1][indexPath.row]];
        NSLog(@"%@",model);
        str = model.city;
        
    }
    
    str = [CityShortName getAdressWithProvience:@"" City:str];
    cell.delegate = self;

    
    if ([_upLoadCityArray containsObject:str])
    {
        NSLog(@"%@",_upLoadCityArray);
        NSLog(@"%@",str);
        
        [cell.titleB setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
        cell.titleB.selected = YES;
    }else{
        [cell.titleB setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cell.titleB.selected = NO;
    }
    
    //放最后
    cell.title = str;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"选中了第%ld个item",indexPath.row);
    ExpectCityCollectionCell *cell = (ExpectCityCollectionCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    
    cell.titleB.selected = YES;
    cell.backgroundColor = [UIColor greenColor];
    cell.contentView.backgroundColor = [UIColor blueColor];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)protocolCityBtnSelect:(NSUInteger)tag btn:(UIButton *)btn
{
    
    NSString *str = btn.titleLabel.text;
    
    if (_upLoadCityArray.count > 7 ) {
        
//        sender.selected = NO;
//        [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        for (NSString *city in _upLoadCityArray)
        {
            if ([str isEqualToString:city])
            {
                [_upLoadCityArray removeObject:city];
                return;
            }
        }
        [MBProgressHUD showError:@"最多不可超过8个" toView:self.view];
        
    }else{
        btn.selected = !btn.selected;
        if (btn.selected)
        {
            [btn setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
            [_upLoadCityArray addObject:str];
        }else{
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            for (NSString *city in _upLoadCityArray)
            {
                if ([str isEqualToString:city])
                {
                    [_upLoadCityArray removeObject:city];
                    break;
                }
            }
        }
        NSLog(@"%@",_upLoadCityArray);
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
