//
//  PositionTrendVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PositionTrendVC.h"
#import "EduORWorkTitleHeaderView.h"    //组的头sectionHeaderView
#import "PositionTrendView.h"           //职位偏好内容数组

#import "ExpectCityCollectionVC.h"      //期望城市
#import "ExpectPositionCollectionVC.h"  //期望工作



#import "BriefInfoModel.h"
#import "PrendInfoModel.h"      //职位偏好model
#import "RequestApi+Login.h"
#import "RequestApi+MyBrief.h"

#import "GeneralPickerView.h"   //自定义pickerView
#import "KYAlertView.h"         //自定义提示框

#define HEADERHEIGHT W(50)

@interface PositionTrendVC ()<PositionTrendDelegate,GeneralPickerViewDelegate,UIScrollViewDelegate>
@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,strong) UIScrollView *scrollView;

@property (nonatomic ,strong) PositionTrendView *workButtonView;
@property (nonatomic ,strong) PositionTrendView *positionButtonView;
@property (nonatomic ,strong) PositionTrendView *payButtonView;
@property (nonatomic ,strong) PositionTrendView *natureButtonView;
@property (nonatomic ,strong) GeneralPickerView *pickerView;
@property (nonatomic ,strong) NSArray *cityArr;
@property (nonatomic ,strong) NSMutableArray *positionArr;
@property (nonatomic ,strong) NSMutableArray *payArr;
@property (nonatomic ,strong) NSArray *payRootArr;
@property (nonatomic ,strong) NSMutableArray *payIDArr;
@property (nonatomic ,strong) NSString *pay;
@property (nonatomic ,strong) NSString *positionNatureStr;//职业类型
@property (nonatomic ,strong) NSArray  *natureArr;

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) NSArray  *infoArr;

@property (nonatomic ,strong) NSMutableArray  *prendArray;

@property (nonatomic ,strong) BriefInfoModel *infoModel;

@end

@implementation PositionTrendVC

-(UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1 , KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
        _scrollView.delegate = self;
        _scrollView.backgroundColor = COLOR_BGCOLOR;
    }
    return _scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
//    [self.view addSubview:self.tableView];
    
    self.infoArr = [GlobalData sharedInstance].GB_UserModel.datas0;
    _infoModel = self.infoArr.firstObject;
    _pay = _infoModel.rPay;
    _positionNatureStr = _infoModel.rNature;
    [GlobalData sharedInstance].city = _infoModel.rCity;
    [GlobalData sharedInstance].provience = _infoModel.rProvince;
    if (kStringIsEmpty(_infoModel.rPay) || kStringIsEmpty(_infoModel.pName)) {
        [GlobalData sharedInstance].trend_Complete     = nil;
    }else{
        [GlobalData sharedInstance].trend_Complete     = @"YES";
    }
    [self.view addSubview:self.scrollView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModel];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [GlobalData sharedInstance].city = [self.cityArr componentsJoinedByString:@","];
    NSLog(@"%@",[GlobalData sharedInstance].city);
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"职位偏好" leftImageName:@"" leftBlock:^{
        //[weakSelf checkChange];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

#pragma mark 检查是否做出更改

-(void)checkChange
{
    if (![_pay isEqualToString:_infoModel.rPay] && !kStringIsEmpty(_infoModel.rPay))
    {
        KYAlertView *alertView = [KYAlertView sharedInstance];
        DSWeak;
        [alertView showAlertView:@"提示" message:@"期望薪资已更改，是否保存？" subBottonTitle:@"不保存" cancelButtonTitle:@"保存" handler:^(AlertViewClickBottonType bottonType) {
            if (bottonType == AlertViewClickBottonTypeSubBotton) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }else if (bottonType == AlertViewClickBottonTypeCancelButton)
            {
               [weakSelf saveInfo];
            }
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//设置数据源
-(void)setModel
{
    
    
    _payIDArr       = [NSMutableArray array];
    _payArr         = [NSMutableArray array];
    _positionArr    = [NSMutableArray array];
    
    self.prendArray = [NSMutableArray array];
    self.natureArr  = [NSArray arrayWithObjects:@"全职",@"兼职",@"实习", nil];
    
    DSWeak;
    [RequestApi getBriefPrtendInfoWithKey:[GlobalData sharedInstance].GB_Key Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        
        NSArray *arr = response[@"datas"];
        
        NSMutableArray *typeArray = [NSMutableArray array];
        
        for (NSDictionary *d in arr)
        {
            PrendInfoModel *model = [PrendInfoModel modelObjectWithDictionary:d];
            [weakSelf.prendArray addObject:model];
        }
        
        for (PrendInfoModel *model in weakSelf.prendArray)
        {
            NSMutableDictionary *p_Dic = [NSMutableDictionary dictionary];
            [p_Dic setValue:model.pName forKey:@"pName"];
            [p_Dic setValue:[GlobalMethod doubleToString:model.rType] forKey:@"rType"];
            [p_Dic setValue:[GlobalMethod doubleToString:model.rTypes] forKey:@"rTypes"];
            NSString *str = [GlobalMethod exchangeToJson:p_Dic];
            if (model.rType != model.rTypes)
            {
                [self.positionArr addObject:str];
                [typeArray addObject:p_Dic];
            }
        }
        
        PrendInfoModel *model = kArrayIsEmpty(arr) ? nil : weakSelf.prendArray.firstObject;
        //薪资
        _pay = kStringIsEmpty(_pay) ? model.rPay : _pay;
        //期望职位
        NSArray *expectArr = self.positionArr;
        [GlobalData sharedInstance].position = expectArr;
        
        //省市数组
        //NSString *city = [model.rCity stringByReplacingOccurrencesOfString:@"市" withString:@""];
        
        if (!kStringIsEmpty(model.rCity))
        {
            self.cityArr = [model.rCity componentsSeparatedByString:@","];
        }
        
        [GlobalData sharedInstance].city = model.rCity;
        [GlobalData sharedInstance].provience = model.rProvince;
        
        [self createBasicViewCityArr:self.cityArr PositionArr:typeArray payStr:_pay r_Nature:_positionNatureStr];
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
    
    
    //薪资数组
    
    if (kArrayIsEmpty(_payRootArr))
    {
        self.payRootArr = [GlobalMethod getArrayToPlist:nil filePath:@"payModel"];
        for (NSDictionary *d in _payRootArr)
        {
            [_payArr addObject:d[@"Sname"]];
            [_payIDArr addObject:d[@"SID"]];
        }
    }
    
    //职业类型数组
    
    [RequestApi getSalarysListDelegate:self Success:^(NSDictionary *response) {
        
        NSArray *arr = response[@"datas"];
        if (arr.count > 0) {
            _payRootArr = response[@"datas"];
            _payArr = [NSMutableArray array];
            _payIDArr = [NSMutableArray array];
        }
        for (NSDictionary *d in _payRootArr)
        {
            [_payArr addObject:d[@"Sname"]];
            [_payIDArr addObject:d[@"SID"]];
        }
        [GlobalMethod whiteArrayToPlist:[NSMutableArray arrayWithArray:_payRootArr] filePath:@"payModel"];
    }];
}

//设置headerView
-(void)createBasicViewCityArr:(NSArray *)cityArr PositionArr:(NSArray *)positionArr payStr:(NSString *)payStr r_Nature:(NSString *)r_Nature
{
    
    [GlobalData sharedInstance].city = [cityArr componentsJoinedByString:@","];
    //[GlobalData sharedInstance].provience = model.rProvince;
    
    
    [GlobalMethod removeAllSubViews:self.scrollView];
    
    _pay = payStr;
    //工作地点SectionHeaderView
    EduORWorkTitleHeaderView * workPlaceView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, 5, KWIDTH, HEADERHEIGHT)];
    workPlaceView.editButton.tag = 50;
    [self setLabelTitle:@"期望城市" headerView:workPlaceView];
    [workPlaceView.editButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     *  工作地点数组
     */
    _workButtonView = [[PositionTrendView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(workPlaceView.frame) + 1, KWIDTH, W(60))];
    _workButtonView.delegate = self;
    [_workButtonView setButtonArray:cityArr index:0];
    //如果没有内容，高度为0
    _workButtonView.height = cityArr.count == 0 ? W(55) : (_workButtonView.subviews.lastObject.y + _workButtonView.subviews.lastObject.height + 10);
    
    
    
    
    //期望职位view
    EduORWorkTitleHeaderView *positionView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_workButtonView.frame) + 10, KWIDTH, HEADERHEIGHT)];
    positionView.editButton.tag = 51;
    [positionView.editButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self setLabelTitle:@"期望职位" headerView:positionView];
    
    /*
     *  期望职位数组
     */
    
    _positionButtonView = [[PositionTrendView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(positionView.frame) + 1, KWIDTH, W(60))];
    _positionButtonView.delegate = self;
    NSMutableArray *P_array = [NSMutableArray array];
    
    NSLog(@"%@",positionArr);
    
    if ([positionArr.firstObject isKindOfClass:[NSString class]]) {
        for (NSString *str in positionArr)
        {
            NSDictionary *dic = [GlobalMethod exchangeStringToDic:str];
            [P_array addObject:dic[@"pName"]];
        }
    }else{
        for (NSDictionary *dic in positionArr)
        {
            [P_array addObject:dic[@"pName"]];
        }
    }
    
    [_positionButtonView setButtonArray:P_array index:1];
    //如果没有内容，高度为0
    _positionButtonView.height = P_array.count == 0 ?  W(55) : (_positionButtonView.subviews.lastObject.y + _positionButtonView.subviews.lastObject.height + 10);
    //薪资要求View
    EduORWorkTitleHeaderView *payView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_positionButtonView.frame) + 10, KWIDTH, HEADERHEIGHT)];
    payView.editButton.tag = 52;
    [payView.editButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self setLabelTitle:@"薪资要求" headerView:payView];
    
    //薪资要求数组
    _payButtonView = [[PositionTrendView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(payView.frame) + 1, KWIDTH, W(60))];
    _payButtonView.delegate = self;
    NSArray *payArr = kStringIsEmpty(payStr) ? nil : [NSArray arrayWithObjects:payStr, nil];
    [_payButtonView setButtonArray:payArr index:2];
    _payButtonView.height = kStringIsEmpty(_pay) ?  W(55) : W(60);
    
    //职业类型View
    EduORWorkTitleHeaderView *natureView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, _payButtonView.bottom + 10, KWIDTH, HEADERHEIGHT)];
    natureView.editButton.tag = 53;
    [natureView.editButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self setLabelTitle:@"职业类型" headerView:natureView];
    //职业类型数组
    _natureButtonView= [[PositionTrendView alloc]initWithFrame:CGRectMake(0, natureView.bottom + 1, KWIDTH, W(60))];
    _natureButtonView.delegate = self;
    NSArray *natureArr = kStringIsEmpty(r_Nature) ? nil : [NSArray arrayWithObjects:r_Nature, nil];
    [_natureButtonView setButtonArray:natureArr index:3];
    _natureButtonView.height = kStringIsEmpty(r_Nature) ?  W(55) : W(60);
    
    [self.scrollView addSubview:workPlaceView];
    [self.scrollView addSubview:_workButtonView];
    [self.scrollView addSubview:positionView];
    [self.scrollView addSubview:_positionButtonView];
    [self.scrollView addSubview:payView];
    [self.scrollView addSubview:_payButtonView];
    [self.scrollView addSubview:_natureButtonView];
    [self.scrollView addSubview:natureView];
    
    self.scrollView.contentSize = CGSizeMake(0, _natureButtonView.bottom);
    
    //期望职位数组
    _positionButtonView = [[PositionTrendView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(positionView.frame) + 1, KWIDTH,W(60))];
    _positionButtonView.delegate = self;
    [_positionButtonView setButtonArray:P_array index:1];
    
    [GlobalData sharedInstance].city = [self.cityArr componentsJoinedByString:@","];
}

-(void)saveInfo
{
    NSLog(@"%@",[GlobalData sharedInstance].city);
    NSLog(@"%@",self.positionArr);
    
    NSString *city = [GlobalData sharedInstance].city;
    NSString *provience = [GlobalData sharedInstance].provience;
    
    NSString *payID = @"0";
    for (NSDictionary *dic in _payRootArr)
    {
        if ([_pay isEqualToString:dic[@"Sname"]])
        {
            payID = dic[@"SID"];
        }
    }
    
    if (kStringIsEmpty(city)) {
        [MBProgressHUD showError:@"至少选择一个期望城市" toView:self.view];
        return;
    }
    if (kStringIsEmpty(_pay)) {
        NSDictionary *dic = _payRootArr.firstObject;
        _pay = dic[@"Sname"];
//        [MBProgressHUD showError:@"请选择薪资要求" toView:self.view];
//        return;
    }
    if (kArrayIsEmpty(self.positionArr)) {
        [MBProgressHUD showError:@"至少选择一个期望职位" toView:self.view];
        return;
    }
    if (kStringIsEmpty(_positionNatureStr)) {
        [MBProgressHUD showError:@"请选择职业类型" toView:self.view];
        return;
    }
    
    NSMutableArray *finalArray = [NSMutableArray array];
    
    //删除字典里的pName键值对
    for (NSString *str in self.positionArr) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[GlobalMethod exchangeStringToDic:str]];
        [dic removeObjectForKey:@"pName"];
        [finalArray addObject:dic];
    }
    
    NSString *rtypes;
    
    if (finalArray.count > 0)
    {
        NSData * JSONData = [NSJSONSerialization dataWithJSONObject:finalArray
                                                            options:kNilOptions
                                                              error:nil];
         rtypes = [[NSString alloc]initWithData:JSONData encoding:NSUTF8StringEncoding];
    }
    
    DSWeak;
    [RequestApi editPositionTrendWithKey:[GlobalData sharedInstance].GB_Key rprovince:provience rcity:city pay:_pay payid:payID rtypes:rtypes nature:_positionNatureStr Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"保存成功" toView:weakSelf.view];
        [GlobalData sharedInstance].trend_Complete = @"YES";
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }];
}

-(void)handleHideTimer
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 点击按钮  （删除 添加）
-(void)protocolSelectButton:(UIButton *)btn
{
    if (btn.tag == 50 || btn.tag == 51 || btn.tag == 52 || btn.tag == 53) {
        [self btnClick:btn];
        return;
    }

    
    NSLog(@"%@",btn.titleLabel.text);
    NSLog(@"%@",self.positionArr);
    
    NSMutableArray *cityArr = [NSMutableArray arrayWithArray:self.cityArr];
    [cityArr removeObject:btn.titleLabel.text];
    
    NSMutableArray *positionArr = [NSMutableArray arrayWithArray:self.positionArr];
    //[positionArr removeObject:btn.titleLabel.text];
    
    //移除全部子View
    self.cityArr        = [NSMutableArray arrayWithArray:cityArr];
    self.positionArr    = [NSMutableArray array];
    NSLog(@"%@",self.positionArr);
    
    for (NSString *str in positionArr)
    {
        NSDictionary *dic = [GlobalMethod exchangeStringToDic:str];
        if (![dic[@"pName"] isEqualToString:btn.titleLabel.text])
        {
            [self.positionArr addObject:dic];
        }
    }
    NSLog(@"%@",self.positionArr);
    //self.positionArr    = [NSMutableArray arrayWithArray:positionArr];
    
    [GlobalMethod removeAllSubViews:self.scrollView];
    //view重新放置
    [self createBasicViewCityArr:cityArr PositionArr:self.positionArr payStr:_pay r_Nature:_positionNatureStr];
    
    
    
    //self.positionArr此时是Json数组
    positionArr = [NSMutableArray arrayWithArray:self.positionArr];
    self.positionArr = [NSMutableArray array];
    
    for (NSDictionary *dic in positionArr)
    {
        NSString *str = [GlobalMethod exchangeToJson:dic];
        [self.positionArr addObject:str];
    }
    [GlobalData sharedInstance].position = self.positionArr;
}

-(void)btnClick:(UIButton *)sender
{
    
    if (sender.tag == 50) {
        ExpectCityCollectionVC *vc = [[ExpectCityCollectionVC alloc]init];
        [self presentViewController:vc animated:YES completion:nil];
    }else if (sender.tag == 51){
        ExpectPositionCollectionVC *vc = [ExpectPositionCollectionVC new];
        [self presentViewController:vc animated:YES completion:nil];
    }else if (sender.tag == 52 ||sender.tag == 53){
        _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
        _pickerView.delegate = self;
        _pickerView.indexTag = sender.tag;
        NSString *title         = sender.tag == 52 ? @"薪资要求" : @"职业类型";
        NSString *firstTitle    = sender.tag == 52 ? _pay       : _positionNatureStr;
        NSArray *firstArr       = sender.tag == 52 ? _payArr    : _natureArr;
        [_pickerView setUpPickerViewTitle:title selectFirstTitle:firstTitle selectSecondTitle:@"" selectModel:1 firstArray:firstArr secondArray:nil];
        _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        [self.view addSubview:_pickerView];
    }
}

//pickerView的代理协议
//点击取消
-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    _pickerView.hidden = YES;
}

//点击确定
-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    if (tag == 52) {
        _pay = content;
    }else if (tag == 53){
        _positionNatureStr = content;
    }
    
    [GlobalMethod removeAllSubViews:self.scrollView];
    [self createBasicViewCityArr:self.cityArr PositionArr:self.positionArr payStr:_pay r_Nature:_positionNatureStr];
    _pickerView.hidden = YES;
}

-(void)setLabelTitle:(NSString *)title headerView:(EduORWorkTitleHeaderView *)view
{
    view.titleL.text = title;
    view.titleL.x = 15;
    view.titleL.y = 0;
    view.editButton.y = 0;
    view.titleL.centerY = HEADERHEIGHT / 2.0f;
    view.editButton.centerY = HEADERHEIGHT / 2.0f;
    NSLog(@"%f",view.height);
    [view.editButton setImage:[UIImage imageNamed:@"whiteAdd"] forState:UIControlStateNormal];
    view.backgroundColor = [UIColor whiteColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
