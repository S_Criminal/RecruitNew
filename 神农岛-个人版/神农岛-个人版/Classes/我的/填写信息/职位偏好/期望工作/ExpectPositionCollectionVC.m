///
//  ExpectPositionCollectionVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ExpectPositionCollectionVC.h"
#import "CityShortName.h"
#import "ExpectCityCollectionCell.h"
#import "ExpectPositionNameModel.h"
#import "BriefInfoModel.h"

#import "RequestApi+MyBrief.h"
#import "RequestApi+Login.h"
@interface ExpectPositionCollectionVC ()<SelectCityDelegate,RequestDelegate>

@property(strong,nonatomic)NSMutableArray *upLoadCityArray;
@property(strong,nonatomic)NSMutableArray *upLoadModel;

@property(strong,nonatomic)NSMutableArray *listArr;

@property (nonatomic ,strong) NSMutableArray *datasArr;
@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) NSMutableArray *sectionArr;
@property (nonatomic ,strong) NSMutableArray * contentArr;

@end

@implementation ExpectPositionCollectionVC{
    NSMutableArray *industryF;
    NSMutableArray *industryFID;
    NSMutableArray *industrySon;
    UIButton *button ;
}
static NSString * const reuseIdentifier = @"collectionCell";

- (instancetype)init
{
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    // 设置cell的尺寸
    layout.itemSize = [UIScreen mainScreen].bounds.size;
    // 清空行距
    layout.minimumLineSpacing = 0;
    
    // 设置滚动的方向
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    return [super initWithCollectionViewLayout:layout];
}

//设置collectionView
-(void)setCollection
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight , KWIDTH, KHEIGHT - NAVIGATION_BarHeight) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    // Register cell classes
    [self.collectionView registerClass:[ExpectCityCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    // Regitster header section
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"1"];
    [self.collectionView setFrame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, KHEIGHT - NAVIGATION_BarHeight)];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"期望职位（多选）";
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO
    [self setCollection];
    [self setModel];
    [self setNav];
    // Request data source
}

-(void)setNav
{
    DSWeak;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:[BaseNavView initNavTitle:@"期望职位" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveExpectPosition];
    }]];
}


#pragma mark request data source
-(void)setModel
{
    self.upLoadModel = [NSMutableArray array];
    self.upLoadCityArray = [NSMutableArray array];
    self.sectionArr = [NSMutableArray array];
    self.contentArr = [NSMutableArray array];
    self.datasArr = [NSMutableArray array];
    
    //所有子类别
    [RequestApi getPositionTypeLsWithDelegate:self Success:^(NSDictionary *response) {
        NSArray *array = response[@"datas"];
        for (NSDictionary *dic in array) {
            ExpectPositionNameModel *model = [ExpectPositionNameModel modelObjectWithDictionary:dic];
            //父数组
            [self.datasArr addObject:model];
            NSString *parentID = [GlobalMethod doubleToString:model.parentID];
            if ([parentID isEqualToString:@"0"])
            {
                [self.sectionArr addObject:model];
            }
        }
        for (ExpectPositionNameModel *p_model in _sectionArr)
        {
            NSMutableArray *arr = [NSMutableArray array];
            for (NSDictionary *dic in array)
            {
                ExpectPositionNameModel *model = [ExpectPositionNameModel modelObjectWithDictionary:dic];
                if (p_model.iDProperty == model.parentID)
                {
                    [arr addObject:model];
                }
            }
            [self.contentArr addObject:arr];
        }
        NSLog(@"%@",self.contentArr);
        NSLog(@"%@",self.sectionArr);
        
        [self.collectionView reloadData];


    } failure:^(NSString *str) {
    
    }];
    
    NSArray *positionArr = [GlobalData sharedInstance].position;
    
    if (positionArr.count > 0)
    {
        for (NSString *str in positionArr)
        {
            NSDictionary *dic = [GlobalMethod exchangeStringToDic:str];
            NSLog(@"%@",_upLoadModel);
            ExpectPositionNameModel *model = [ExpectPositionNameModel modelObjectWithDictionary:dic];
            model.pName = dic[@"pName"];
            model.iDProperty = [dic[@"rTypes"] doubleValue];;
            model.parentID = [dic[@"rType"] doubleValue];
            model.createDate = @"";
            model.pNamep = @"";
            [_upLoadModel addObject:model];
        }
    }
    

}

-(void)saveExpectPosition
{
    NSLog(@"%@",_upLoadModel);
    
    if (kArrayIsEmpty(_upLoadModel))
    {
        [MBProgressHUD showError:@"至少选择一个期望职位" toView:self.view];
        return;
    }
    
    NSMutableArray *finalArray = [NSMutableArray array];
    
    for (ExpectPositionNameModel *model in _upLoadModel)
    {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setValue:[GlobalMethod doubleToString:model.parentID] forKey:@"rType"];
        [dic setValue:[GlobalMethod doubleToString:model.iDProperty] forKey:@"rTypes"];
        [finalArray addObject:dic];
    }
    
    if (finalArray.count > 0)
    {
        NSData * JSONData = [NSJSONSerialization dataWithJSONObject:finalArray
                                                            options:kNilOptions
                                                              error:nil];
        NSString *rtypes  = [[NSString alloc]initWithData:JSONData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",rtypes);
        //[GlobalData sharedInstance].rtypesJson = rtypes;
        DSWeak;
        [RequestApi upLoadNewBriefPositionWithKey:[GlobalData sharedInstance].GB_Key rTypes:rtypes Delegate:self success:^(NSDictionary *response) {
            NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
            weakSelf.hideDelayTimer = timer;
        } failure:^(NSString *errorStr, id mark) {
            [MBProgressHUD showSuccess:errorStr toView:weakSelf.view];
        }];

    }else{
        //如果没有选择期望工作
        [MBProgressHUD showError:@"至少选择一个期望工作" toView:self.view];
        //        [self alertViewWithMessage:@"请选择一个期望工作" status:@"0"];
        
    }
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc
{
    self.hideDelayTimer = nil;
    [self.hideDelayTimer invalidate];
}


//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize size={320,W(40)};
    return size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView * header;
        
        
        header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"1" forIndexPath:indexPath];
        for (UIView *view in header.subviews)
        {
            [view removeFromSuperview];
        }
        header.backgroundColor = COLOR_BGCOLOR;
        UILabel * title = [[UILabel alloc]initWithFrame:CGRectMake(W(15), 5, 200, 30)];
        ExpectPositionNameModel *model = self.sectionArr[indexPath.section];
        title.text = model.pName;
        title.centerY = W(40) / 2;
        title.textColor = COLOR_LABELSIXCOLOR;
        title.font = [UIFont systemFontOfSize:F(15)];
        [header addSubview:title];
        return header;
        
    }
    return nil;
}


#pragma mark <UICollectionViewDelegateFlowLayout> methods
// 指定单元格大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    CGFloat height = 30;
    if (isIphone5) {
        height = 25;
    }
    size = CGSizeMake ( KWIDTH / 4 + 4, height);
    return size;
}
//设置每个组的边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(W(15), W(15), W(15),W(15));
}

//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.sectionArr.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (industryFID.count > 0) {
        
        NSDictionary *dic = industrySon[section];
        NSMutableArray *arr = dic[@"Cities"];
        return arr.count;
    }
    NSArray *arr = self.contentArr[section];
    return arr.count;
}

- (ExpectCityCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ExpectCityCollectionCell *cell = (ExpectCityCollectionCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    NSArray *array = self.contentArr[indexPath.section];
    ExpectPositionNameModel *model = array[indexPath.row];
    NSString *str = model.pName;
    cell.title = str;
    cell.delegate = self;
    cell.titleB.tag = [[GlobalMethod doubleToString:model.iDProperty] integerValue] + 10;
    
    NSLog(@"%@",_upLoadModel);
    for (ExpectPositionNameModel *model_M in _upLoadModel)
    {
        if (model_M.iDProperty == model.iDProperty)
        {
            [cell.titleB setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
            cell.titleB.selected = YES;
            break;
            
        }else{
            [cell.titleB setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            cell.titleB.selected = NO;
        }
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"选中了第%ld个item",indexPath.row);
    ExpectCityCollectionCell *cell = (ExpectCityCollectionCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    cell.titleB.selected = YES;
    cell.backgroundColor = [UIColor greenColor];
    cell.contentView.backgroundColor = [UIColor blueColor];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)protocolCityBtnSelect:(NSUInteger)tag btn:(UIButton *)btn
{
    NSLog(@"_upLoadModel%@",_upLoadModel);
    if (_upLoadModel.count > 7) {
        if (btn.selected)
        {
            btn.selected = NO;
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            for (ExpectPositionNameModel *model in _upLoadModel) {
                if ([[GlobalMethod doubleToString:model.iDProperty] integerValue] + 10 == tag)
                {
                    NSLog(@"%@",model);
                    NSLog(@"%@",_upLoadModel.firstObject);
                    [_upLoadModel removeObject:model];
                    break;
                }
            }
        }else{
            [MBProgressHUD showError:@"最多不可超过8个" toView:self.view];
        }
    }else{
        btn.selected = !btn.selected;
        if (btn.selected)
        {
            [btn setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
            for (ExpectPositionNameModel *model in self.datasArr)
            {
                if ([[GlobalMethod doubleToString:model.iDProperty] integerValue] + 10 == tag)
                {
                    [_upLoadModel addObject:model];
                    break;
                }
            }
        }else{
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            for (ExpectPositionNameModel *model in _upLoadModel)
            {
                if ([[GlobalMethod doubleToString:model.iDProperty] integerValue] + 10 == tag)
                {
                    [_upLoadModel removeObject:model];
                    break;
                }
            }
            for (ExpectPositionNameModel *model in _upLoadModel)
            {
                if ([[GlobalMethod doubleToString:model.iDProperty] integerValue] + 10 == tag)
                {
                    [_upLoadModel removeObject:model];
                    break;
                }
            }
        }
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
