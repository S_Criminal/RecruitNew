//
//  MyWorkExperienceVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/6.
//  Copyright © 2017年 Light. All rights reserved.
//

// 收起键盘 用的 view endEditing

#import "MyWorkExperienceVC.h"
#import "MyExperienceGlobalView.h"
#import "GeneralPickerView.h"
//请求
#import "RequestApi+Login.h"
#import "RequestApi+MyBrief.h"

#import "GlobalDateModel.h"
#import "PickerEduModel.h"          //学历model
#import "DateArrModel.h"            //时间model

@interface MyWorkExperienceVC ()<ExperienceDelegate,UITextFieldDelegate,GeneralPickerViewDelegate,RequestDelegate,UITextViewDelegate>

@property (nonatomic ,strong) MyExperienceGlobalView *companyView;
@property (nonatomic ,strong) MyExperienceGlobalView *positionView;
@property (nonatomic ,strong) MyExperienceGlobalView *startView;
@property (nonatomic ,strong) MyExperienceGlobalView *endView;
@property (nonatomic ,strong) MyExperienceGlobalView *degreeView;

@property (nonatomic ,strong) UITextView *textView; //工作内容textView
@property (nonatomic ,strong) UITextField *workTextField;

@property (nonatomic ,strong) GeneralPickerView *pickerView;

@property (nonatomic ,strong) NSArray *degreeArray;
@property (nonatomic ,strong) NSArray *yearArr;
@property (nonatomic ,strong) NSArray *monthArr;

@property (nonatomic ,strong) UIButton *deleteButton;
@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) NSString *isQuits;    //是否是离职状态

@property (nonatomic ,strong) BaseNavView *navView;

@property (nonatomic ,strong) UIView *bgView;

@end

@implementation MyWorkExperienceVC{
    CGFloat keyBoardChangeHeight;   //键盘收起的高度
    NSString *_company;
    NSString *_position;
    NSString *_startTime;
    NSString *_endTime;
    NSString *selectButtonIndex;
    NSString *_content;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isQuits = @"0";
    [self setUI];
    [self setNav];
    [self setBasicView];
    [self setModel];
}

-(void)setUI
{
    keyBoardChangeHeight = 150;
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"工作经历" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }];
    self.navView.backgroundColor = [UIColor whiteColor];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)saveInfo
{
    NSString *startTime  = [self setChangeTime:_startTime];
    NSString *endTime    = [self setChangeTime:_endTime];
    
    NSString *comName   = self.companyView.textField.text;
    NSString *position  = self.positionView.textField.text;
    NSString *content   = self.textView.text;
    
    if (kStringIsEmpty(comName)) {
            [MBProgressHUD showError:@"公司名称不能为空" toView:self.view];
            return;
    }else{
         if (comName.length < 4)
         {
             [MBProgressHUD showError:@"公司名称不能少于四位" toView:self.view];
             return;
         }
    }
    if (kStringIsEmpty(position)) {
        [MBProgressHUD showError:@"职业不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(content)) {
        content = @"";
        //return;
    }
    if (kStringIsEmpty(startTime)) {
        [MBProgressHUD showError:@"入职时间不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(endTime)) {
        [MBProgressHUD showError:@"离职时间不能为空" toView:self.view];
        return;
    }
    DSWeak;
    self.navView.backBtn.userInteractionEnabled = NO;
    [RequestApi editExperienceWithKey:[GlobalData sharedInstance].GB_Key expID:self.editID comname:comName position:position entryd:startTime quitd:endTime content:content quits:self.isQuits Delegate:nil success:^(NSDictionary *response) {
        self.navView.backBtn.userInteractionEnabled = YES;
        Datas1 *model = [[Datas1 alloc]init];
        model.wComName = comName;
        model.wPosition = position;
        model.wEntryD = startTime;
        model.wQuitD = endTime;
        model.wContent = content;
        NSLog(@"%lf",model.isQuits);
        NSLog(@"%lf",[weakSelf.isQuits doubleValue]);
        
        model.isQuits = [weakSelf.isQuits doubleValue];
        model.iDProperty = [response[@"WEID"] doubleValue];
        
        if ([weakSelf.editID isEqualToString:[GlobalMethod doubleToString:model.iDProperty]])
        {
            [[GlobalData sharedInstance].GB_UserModel.datas1 removeObject:weakSelf.datas1Model];
        }
        
        [[GlobalData sharedInstance].GB_UserModel.datas1 addObject:model];
        
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        
        [MBProgressHUD showSuccess:@"保存工作经历成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }failure:^(NSString *str) {
         self.navView.backBtn.userInteractionEnabled = YES;
        [MBProgressHUD showError:str toView:weakSelf.view];
    }];


}

-(NSString *)setChangeTime:(NSString *)time
{

    if (kStringIsEmpty(time)) {
        return time;
    }
    NSArray *array = [time componentsSeparatedByString:@"-"];
    
    NSString *one = array.firstObject;
    NSString *two = array[1];
    
    one = [one stringByReplacingOccurrencesOfString:@"年" withString:@""];
    two = [two stringByReplacingOccurrencesOfString:@"月" withString:@""];
    time = [NSString stringWithFormat:@"%@-%@",one,two];
    if (!kStringIsEmpty(time) && time.length > 7) {
        time = [time substringToIndex:7];
    }
    return time;
}

-(void)setDatas1Model:(Datas1 *)datas1Model
{
    _datas1Model = datas1Model;
}

-(void)setBasicView
{
    NSString *bgColor   = @"222222";
    NSString *textColor = @"666666";
    
    _company    = _datas1Model.wComName;
    _position   = _datas1Model.wPosition;
    _startTime  = _datas1Model.wEntryD;
    _endTime    = _datas1Model.wQuitD;
    _content    = _datas1Model.wContent;
    self.isQuits= [GlobalMethod doubleToString:_datas1Model.isQuits];
    NSLog(@"%lf",_datas1Model.isQuits);
    NSLog(@"%@",self.isQuits);
    
    _startTime = [self setChangeTime:_startTime];
    _endTime   = [self setChangeTime:_endTime];
    
    _companyView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"公司名称" Title:_company status:NO tag:100 frame:CGRectMake(0,+3, KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    
    _positionView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"你的职位" Title:_position status:NO tag:101 frame:CGRectMake(0, CGRectGetMaxY(_companyView.frame), KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    
    _startView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"入职时间" Title:_startTime status:YES tag:102 frame:CGRectMake(0,CGRectGetMaxY(_positionView.frame) + W(25), KWIDTH / 2, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    
    _endView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"离职时间" Title:_endTime status:YES tag:103 frame:CGRectMake(CGRectGetMaxX(_startView.frame) + 5,_startView.y, KWIDTH / 2, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    
    UIButton *OnButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [OnButton setFrame:CGRectMake(10, CGRectGetMaxY(_startView.frame) + 15, W(30), W(30))];
    OnButton.tag = 50;
    UILabel *onLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(OnButton.frame) + 10, OnButton.y, 50, OnButton.height)];
    onLabel.text = @"在职";
    
    UIButton *leaveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leaveButton setFrame:CGRectMake(CGRectGetMaxX(onLabel.frame) + 10, OnButton.y , W(30), W(30))];
    leaveButton.tag = 51;
    UILabel *leaveLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leaveButton.frame) + 10, OnButton.y, 50, OnButton.height)];
    leaveLabel.text = @"离职";
    
    OnButton.selected       = [self.isQuits isEqualToString:@"0"] ? YES : NO;
    leaveButton.selected    = [self.isQuits isEqualToString:@"1"] ? YES : NO;
    
    OnButton    = [self setSelectButton:OnButton label:onLabel];
    leaveButton = [self setSelectButton:leaveButton label:leaveLabel];
    
    _degreeView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"工作内容" Title:@"" status:YES tag:1000 frame:CGRectMake(0, CGRectGetMaxY(OnButton.frame) + 5, KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    _degreeView.label.textColor = COLOR_LABELThreeCOLOR;
    
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_degreeView.frame) + 10, KWIDTH - 15 * 2, W(100))];
    _textView.backgroundColor = [HexStringColor colorWithHexString:@"f1f1f1"];
    _textView.textColor = COLOR_LABELThreeCOLOR;
    _textView.font = [UIFont systemFontOfSize:F(14)];
    _textView.delegate = self;
    _textView.text = _datas1Model.wContent;
    
    
    [self.bgView addSubview:_companyView];
    [self.bgView addSubview:_positionView];
    [self.bgView addSubview:_startView];
    [self.bgView addSubview:_endView];
    
    [self.bgView addSubview:OnButton];
    [self.bgView addSubview:leaveButton];
    [self.bgView addSubview:onLabel];
    [self.bgView addSubview:leaveLabel];
    [self.bgView addSubview:_degreeView];
    [self.bgView addSubview:_textView];
    
    //新增加的隐藏删除工作经历按钮
    if (![self.editID isEqualToString:@"0"])
    {
        [self createDeleteButton];
    }
    
}

-(UIButton *)setSelectButton:(UIButton *)button label:(UILabel *)label
{
    [button addTarget:self action:@selector(tapSelectButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"ico_未选中"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"ico_选中"] forState:UIControlStateSelected];
    label.textColor = COLOR_LABELThreeCOLOR;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:F(15)];
    
    
    return button;
}

//删除教育经历
-(void)createDeleteButton
{
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setFrame:CGRectMake(MyViewLeftConstraint, CGRectGetMaxY(_textView.frame) + 15, KWIDTH - MyViewLeftConstraint * 2, 45)];
    [self.deleteButton setCorner:5];
    self.deleteButton.backgroundColor = COLOR_MAINCOLOR;
    self.deleteButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
    [self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteButton setTitle:@"删除工作经历" forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(tapDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.deleteButton];
}

-(void)tapDelete:(UIButton *)sender
{
    DSWeak;
    NSLog(@"%@",self.editID);
    
    [RequestApi deleteExperienceWithKey:[GlobalData sharedInstance].GB_Key workID:self.editID Delegate:self success:^(NSDictionary *response) {
        [[GlobalData sharedInstance].GB_UserModel.datas1 removeObject:self.datas1Model];
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        [MBProgressHUD showSuccess:@"删除工作经历成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }];
}

-(void)handleHideTimer{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 选择求职状态
-(void)tapSelectButton:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.tag == 50) {
        UIButton *button = (UIButton *)[self.view viewWithTag:51];
        button.selected = sender.selected == YES ? NO : NO ;
        self.isQuits = @"0";
    }else if (sender.tag == 51){
        self.isQuits = @"1";
        UIButton *button = (UIButton *)[self.view viewWithTag:50];
        button.selected = sender.selected == YES ? NO : NO ;
    }
}

-(void)setModel
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"pickerModel" ofType:@"plist"];
    self.degreeArray = [NSArray arrayWithContentsOfFile:path];
    NSLog(@"%@",self.degreeArray);
    if (!kArrayIsEmpty(self.degreeArray))
    {
        return;
    }
    self.degreeArray = [NSArray array];
    DSWeak;
    [RequestApi getEduListWithDelegate:self Success:^(NSDictionary *response) {
        if ([response[@"code"]isEqualToNumber:@200])
        {
            NSArray *datasArr = response[@"datas"];
            NSMutableArray *array = [NSMutableArray array];
            for (int i = 0; i < datasArr.count; i ++ )
            {
                NSDictionary *dic = [NSDictionary dictionaryWithDictionary:datasArr[i]];
                PickerEduModel *model = [PickerEduModel modelObjectWithDictionary:dic];
                [array addObject:model];
            }
            
            [GlobalMethod whiteArrayToPlist:array filePath:nil];
            weakSelf.degreeArray = array;
        }
    }];
    
    /** 请求日期年月数据 */
    DateArrModel *model = [[DateArrModel alloc]init];
    int minYear = (int) model.nowYear - 60 ;
    [model setYearArrayModelWithMinIndex:minYear maxIndex:0 yearBlock:^(NSMutableArray *yArr) {
        weakSelf.yearArr = yArr;
    } monthBlock:^(NSMutableArray *mArr) {
        weakSelf.monthArr = mArr;
    }];
    
}


#pragma mark 点击label的协议方法

-(void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(MyExperienceControl *)control
{
    [self.view endEditing:YES];
    
    NSInteger timeInteger = [control.controlLabel.text isEqualToString:@"入职时间"]  ? 23 : 23;
    
    NSArray *textArr = [control.controlLabel.text componentsSeparatedByString:@"-"];
    textArr          = textArr.count > 1 ? textArr : [[GlobalMethod getBirth] componentsSeparatedByString:@"-"];    //有出生年月且未填写信息时，默认出生年 + 20
    NSString *firstTitle = @"1970";
    NSString *secondTitle;
    //默认选择
    if (textArr.count > 1) {
        firstTitle = control.controlLabel.text.length > 4 ? textArr.firstObject : [NSString stringWithFormat:@"%ld",[[textArr.firstObject substringToIndex:4] integerValue] + timeInteger]  ;    //默认出生年 + 20
        secondTitle= textArr[1];
    }else{
        firstTitle = textArr.firstObject;
    }
    
    if (tag == 1000)
    {
        return;
    }
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    _pickerView.indexTag = tag;
    NSLog(@"%@",self.degreeArray);
    
    NSArray *firstArray ;
    NSArray *secondArray;
    NSString *title;
    
    title = tag == 102 ? @"入职时间" : @"离职时间";

    firstArray  = self.yearArr;
    secondArray = self.monthArr;

    [_pickerView setUpPickerViewTitle:title selectFirstTitle:firstTitle selectSecondTitle:secondTitle selectModel:2 firstArray:firstArray secondArray:secondArray];
    _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    [self.view addSubview:_pickerView];
    [self showPickerView];
}

#pragma mark pickerView的协议
-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    NSLog(@"===content%@===",content);
    
    NSString *endContent    = _endView.label.text;
    NSString *startContent  = _startView.label.text;
    startContent = [startContent stringByReplacingOccurrencesOfString:@"年" withString:@""];
    startContent = [startContent stringByReplacingOccurrencesOfString:@"月" withString:@""];
    endContent = [endContent stringByReplacingOccurrencesOfString:@"年" withString:@""];
    endContent = [endContent stringByReplacingOccurrencesOfString:@"月" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"年" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"月" withString:@""];
    
    if (tag == 102) {
        if (![endContent isEqualToString:@"离职时间"]) {
            if (([[endContent substringToIndex:4] integerValue] >= [[content substringToIndex:4]integerValue])) {
                
                if ([[endContent substringToIndex:4] integerValue] == [[content substringToIndex:4] integerValue]) {
                    if ([[endContent substringWithRange:NSMakeRange(5, 2)] integerValue] < [[content substringWithRange:NSMakeRange(5, 2)] integerValue]) {
                        [MBProgressHUD showError:@"入职时间不能大于离职时间" toView:self.view];
                    }else{
                        _startView.label = [self setLabel:_startView.label Content:content];
                        _startTime = content;
                    }
                }else{
                    _startView.label = [self setLabel:_startView.label Content:content];
                    _startTime = content;
                }
                
            }else{
                [MBProgressHUD showError:@"入职时间不能大于离职时间" toView:self.view];
            }
        }else{
            _startTime = content;
            _startView.label = [self setLabel:_startView.label Content:content];
        }
    }else if (tag == 103){
        if (!([[startContent substringToIndex:4] integerValue] >= [[content substringToIndex:4]integerValue])) {
          
                _endView.label = [self setLabel:_endView.label Content:content];
                _endTime = content;
            
        }else{
            if ([[startContent substringToIndex:4] integerValue] == [[content substringToIndex:4] integerValue]) {
                if ([[startContent substringWithRange:NSMakeRange(5, 2)] integerValue] > [[content substringWithRange:NSMakeRange(5, 2)] integerValue]) {
                    [MBProgressHUD showError:@"入职时间不能大于离职时间" toView:self.view];
                }else{
                    _endView.label = [self setLabel:_endView.label Content:content];
                    _endTime = content;
                }
            }else{
                if ([[startContent substringToIndex:4] integerValue] > [[content substringToIndex:4] integerValue]) {
                    [MBProgressHUD showError:@"入职时间不能大于离职时间" toView:self.view];
                }else{
                    _endView.label = [self setLabel:_endView.label Content:content];
                    _endTime = content;
                }
            }
            
        }
    }

    [self hidePickerView];
}

-(UILabel *)setLabel:(UILabel *)label Content:(NSString *)content
{
    if (!kStringIsEmpty(content)) {
        label.text = content;
        label.textColor = COLOR_LABELThreeCOLOR;
    }
    return label;
}



-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == self.textView) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShows:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHides:) name:UIKeyboardWillHideNotification object:nil];
    }
    return YES;
}


#pragma mark --- 弹出键盘 ---
- (void)keyboarShows:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.bgView.frame;
        if (frame.origin.y == NAVIGATION_BarHeight + 1) {
            frame.origin.y = weakSelf.bgView.frame.origin.y - keyBoardChangeHeight;
            weakSelf.bgView.frame = frame;
        }
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark --- 收起键盘 ---
- (void)keyboarHides:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.bgView.frame;
        if (frame.origin.y < NAVIGATION_BarHeight + 1) {
            frame.origin.y = weakSelf.bgView.frame.origin.y + keyBoardChangeHeight;
            weakSelf.bgView.frame = frame;
        }
    } completion:^(BOOL finished) {
        
    }];
}

-(void)dealloc{
     [[NSNotificationCenter defaultCenter]removeObserver:self];
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
