//
//  MyWorkExperienceListVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyWorkExperienceListVC.h"
#import "WorkExperienceCell.h"
#import "BaseClass.h"
#import "EduORWorkTitleHeaderView.h"

#import "MyWorkExperienceVC.h"

#import "Datas1.h"
@interface MyWorkExperienceListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listArr;

@end

@implementation MyWorkExperienceListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasiceView];
//    [self setModel];
    self.view.backgroundColor = COLOR_BGCOLOR;
}

-(void)setNav
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.backgroundColor = [UIColor whiteColor];
    [navView addSubview:[BaseNavView initNavTitle:@"个人经历" leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:navView];
}

-(void)setBasiceView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight , KWIDTH, KHEIGHT - NAVIGATION_BarHeight ) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self createTableHeaderView];
}

-(void)createTableHeaderView
{
    EduORWorkTitleHeaderView *sectionView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(50))];
    [sectionView.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
    sectionView.titleL.text = @"工作经历";
    self.tableView.tableHeaderView = sectionView;
}

-(void)tapEdit:(UIButton *)sender
{
    MyWorkExperienceVC *experience = [MyWorkExperienceVC new];
    experience.edit = @"add";
    experience.editID = @"0";
    [self presentViewController:experience animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModel];
}

-(void)setModel
{
    
    _listArr = [GlobalData sharedInstance].GB_UserModel.datas1;
    
    [self.tableView reloadData];
    
    NSLog(@"%@",_listArr);
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [WorkExperienceCell fetchHeight:_listArr[indexPath.section]];
    return h;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(WorkExperienceCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WorkExperienceCell *cell = [[WorkExperienceCell alloc]init];
    if (!cell)
    {
        cell = [[WorkExperienceCell alloc]init];
    }
    [cell.bgView setCorner:5];
    cell.line.hidden = YES;
    [cell resetCellWithModel:self.listArr[indexPath.section]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Datas1 *datas = [[GlobalData sharedInstance].GB_UserModel.datas1 objectAtIndex:indexPath.section];
    NSLog(@"%@",[GlobalData sharedInstance].GB_UserModel.datas1);
    NSLog(@"%@",datas);
    
    MyWorkExperienceVC *eduInfo = [MyWorkExperienceVC new];
    eduInfo.editID  = [GlobalMethod doubleToString:datas.iDProperty];
    eduInfo.edit    = @"upd";
    eduInfo.datas1Model = datas;
    [self presentViewController:eduInfo animated:YES completion:nil];
    
//    [GB_Nav pushViewController:workVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
