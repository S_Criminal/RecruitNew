//
//  MyContactEditInfoCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/12.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyContactEditInfoCell.h"

@implementation MyContactEditInfoCell{
    CGFloat _top;
    CGFloat _imageWidth;
}

#pragma mark 懒加载

- (UIView *)bgView{
    if (_bgView == nil) {
        _bgView = [UIView new];
        [_bgView setFrame:CGRectMake(MyViewLeftConstraint, 0, KWIDTH - MyViewLeftConstraint * 2, 20)];
        [_bgView setCorner:5];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UIImageView *)iconImage{
    if (_iconImage == nil) {
        _iconImage = [UIImageView new];
        _iconImage.backgroundColor = [UIColor clearColor];
        _iconImage.image = [UIImage imageNamed:@"联系电话"];
//        _iconImage.widthHeight = @[@(SCREEN_WIDTH),@(W(0))];
    }
    return _iconImage;
}

- (UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:F(16)];
        _titleLabel.textColor = COLOR_LABELThreeCOLOR ;
//        [GlobalMethod setLabel:_titleLabel widthLimit:0 numLines:0 fontNum:F(16) textColor:COLOR_LABEL text:@""];
    }
    return _titleLabel;
}

- (UITextField *)textField{
    if (_textField == nil) {
        _textField = [UITextField new];
        _textField.font = [UIFont systemFontOfSize:F(13)];
//        _textField.textAlignment = NSTextAlignmentCenter;
        _textField.textColor = COLOR_LABELSIXCOLOR;
//        _textField.borderStyle = UITextBorderStyleNone;
        _textField.backgroundColor = [UIColor clearColor];
        _textField.placeholder = @"点击填写";
//        _textField.widthHeight = @[@(SCREEN_WIDTH),@(W(0))];
    }
    return _textField;
}

-(NSArray *)titleArr
{
    if (!_titleArr)
    {
        _titleArr = [NSArray arrayWithObjects:@"手机",@"邮箱",@"微信",@"QQ", nil];
    }
    return _titleArr;
}

-(NSArray *)iconImageArr
{
    if (!_iconImageArr) {
        _iconImageArr = [NSArray arrayWithObjects:@"联系电话",@"邮箱地址",@"联系电话",@"联系电话", nil];
    }
    return _iconImageArr;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(MyContactEditInfoCell)

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUI];
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.iconImage];
        [self.bgView addSubview:self.titleLabel];
        [self.bgView addSubview:self.textField];
    }
    return self;
}

-(void)setUI
{
    _top = 20;
    _imageWidth = 30;
//    if (isIphone5) {
//        _top = 14;
//    }
}

-(void)setIndexRow:(NSInteger)indexRow
{
    _indexRow = indexRow;
}

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model
{
    NSString *textfieldContent;
    if (_indexRow == 0) {
        textfieldContent = model.rPhone;
    }else if (_indexRow == 1){
        textfieldContent = model.rEmail;
    }else if (_indexRow == 2){
        textfieldContent = model.uWechat;
    }else if (_indexRow == 3){
        textfieldContent = model.uQQ;
    }
    self.textField.text = kStringIsEmpty(textfieldContent) ? @"" :textfieldContent;
    
    self.bgView.frame = CGRectMake(W(15), 0, KWIDTH - 15 * 2, W(0));
    
    self.iconImage.leftTop = XY(20, _top);
    self.iconImage.widthHeight = XY(_imageWidth,_imageWidth);
    self.iconImage.image = [UIImage imageNamed:self.iconImageArr[_indexRow]];
    
    self.titleLabel.text = self.titleArr[_indexRow];
    self.titleLabel.leftTop = XY(CGRectGetMaxX(_iconImage.frame) + 20, _iconImage.y - 5);
    [self.titleLabel sizeToFit];
    
    [self.textField setFrame:CGRectMake(CGRectGetMaxX(_iconImage.frame) + 20, CGRectGetMaxY(_titleLabel.frame) + 6, self.bgView.width - self.titleLabel.left - W(20), 20)];
    self.textField.tag = _indexRow + 10;
    _textField.placeholder = [NSString stringWithFormat:@"点击填写您的%@",_titleArr[_indexRow]];
    
    self.height = _iconImage.bottom + _top ;
    self.bgView.height = self.height;
    return self.bottom;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
