//
//  MyContactEditInfoCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/12.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefInfoModel.h"

@interface MyContactEditInfoCell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIImageView *iconImage;
@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UITextField *textField;

@property (nonatomic ,assign) NSInteger indexRow;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) NSArray *iconImageArr;

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(BriefInfoModel *)model;
@end
