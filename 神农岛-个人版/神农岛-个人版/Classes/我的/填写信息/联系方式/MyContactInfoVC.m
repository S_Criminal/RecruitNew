//
//  MyContactInfoVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/7.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyContactInfoVC.h"

//#import "MyContactInfoCell.h"
#import "MyContactEditInfoCell.h"
#import "BriefInfoModel.h"

#import "RequestApi+MyBrief.h"

@interface MyContactInfoVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,RequestDelegate>
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) BriefInfoModel *model;
@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic, weak) NSTimer *hideDelayTimer;
@end

@implementation MyContactInfoVC{
    CGFloat keyBoardChangeHeight;
    NSString *_phone;
    NSString *_email;
    NSString *_weiChat;
    NSString *_qqChat;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.frame = CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1);
        _bgView.backgroundColor = COLOR_BGCOLOR;
    }
    return _bgView;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, self.bgView.height) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.bgView];
    _model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    [self setUI];
    [self setNav];
    [self setBasicView];
    [self setModel];
}

-(void)setUI
{
    keyBoardChangeHeight = 100;
}

-(void)setNav
{
    DSWeak;
    UIView * navView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.backgroundColor = [UIColor whiteColor];
    [navView addSubview:[BaseNavView initNavTitle:@"完善资料" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }]];
    [self.view addSubview:navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)saveInfo
{
    [self.bgView endEditing:YES];
    BOOL r_Phone = [ValidateRegularExpression validatePhone:_phone];
    BOOL r_email = [ValidateRegularExpression validateEmail:_email];
    
    if (kStringIsEmpty(_phone)) {
        [MBProgressHUD showError:@"请先填写手机号" toView:self.view];
        return;
    }else if (kStringIsEmpty(_email)){
        [MBProgressHUD showError:@"请先填写邮箱" toView:self.view];
        return;
    }
    if (!r_Phone) {
        [MBProgressHUD showError:@"手机号格式不正确" toView:self.view];
        return;
    }else if (!r_email){
        [MBProgressHUD showError:@"邮箱格式不正确" toView:self.view];
        return;
    }
    
    DSWeak;
    [RequestApi editContactInfoWithKey:[GlobalData sharedInstance].GB_Key phone:_phone mail:_email qq:_qqChat wechat:_weiChat Delegate:self success:^(NSDictionary *response) {
        [GlobalData sharedInstance].u_Email = _email;
        [MBProgressHUD showSuccess:@"保存联系方式成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }];
    
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setModel
{
    _model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    _phone = kStringIsEmpty(_model.rPhone) ? @"" : _model.rPhone;
    _email = kStringIsEmpty(_model.rEmail) ? @"" : _model.rEmail;
    _weiChat = kStringIsEmpty(_model.uWechat) ? @"" : _model.uWechat;
    _qqChat  = kStringIsEmpty(_model.uQQ) ? @"" : _model.uQQ;
}

-(void)setBasicView
{
    [self.bgView addSubview:self.tableView];
    [self.tableView registerClass:[MyContactEditInfoCell class] forCellReuseIdentifier:@"MyContactEditInfoCell"];
    self.tableView.tableHeaderView = [self createTableHeaderView];
}

-(UIView *)createTableHeaderView
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 40)];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, KWIDTH, 40)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:F(16)];
    titleLabel.textColor = COLOR_LABELSIXCOLOR;
    titleLabel.text = @"联系信息";
    [view addSubview:titleLabel];
    
    return view;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [MyContactEditInfoCell fetchHeight:nil];
}

-(MyContactEditInfoCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyContactEditInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyContactEditInfoCell"];
    if (!cell) {
        
    }
    cell.indexRow = indexPath.section;
    cell.textField.delegate = self;
    [cell resetCellWithModel:_model];
//    cell.model = _model;
    return cell;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //get cell
    MyContactEditInfoCell *cell  = (MyContactEditInfoCell *)[[[textField superview] superview]superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch (indexPath.section) {
        case 0:
            _phone = textField.text;
            break;
        case 1:
            _email = textField.text;
            break;
        case 2:
            _weiChat = textField.text;
            break;
        case 3:
            _qqChat = textField.text;
            break;
        default:
            break;
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.bgView endEditing:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.bgView endEditing:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    MyContactEditInfoCell *cell  = (MyContactEditInfoCell *)[[[textField superview] superview]superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    switch (indexPath.section) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            [self reciveKeyBoard];
            break;
        case 3:
            [self reciveKeyBoard];
            break;
        default:
            break;
    }
    
}

-(void)reciveKeyBoard
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShows:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHides:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark --- 弹出键盘 ---
- (void)keyboarShows:(NSNotification *)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.bgView.frame;
        if (frame.origin.y == NAVIGATION_BarHeight + 1) {
            frame.origin.y = weakSelf.bgView.frame.origin.y - keyBoardChangeHeight;
            weakSelf.bgView.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark --- 收起键盘 ---
- (void)keyboarHides:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.bgView.frame;
        if (frame.origin.y < NAVIGATION_BarHeight + 1) {
            frame.origin.y = weakSelf.bgView.frame.origin.y + keyBoardChangeHeight;
            weakSelf.bgView.frame = frame;
        }
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    self.hideDelayTimer = nil;
    [self.hideDelayTimer invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
