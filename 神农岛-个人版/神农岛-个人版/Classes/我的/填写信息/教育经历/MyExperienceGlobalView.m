//
//  MyExperienceGlobalView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyExperienceGlobalView.h"

@implementation MyExperienceGlobalView

+(instancetype)initWithPlaceHolderTitle:(NSString *)placeHolder Title:(NSString *)title status:(BOOL)status tag:(NSInteger)tag frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate
{
    return [[MyExperienceGlobalView alloc]initWithPlaceHolderTitle:placeHolder Title:title status:status tag:tag frame:frame bgColor:bgColor textColor:textColor delegate:delegate];
}

-(instancetype)initWithPlaceHolderTitle:(NSString *)placeHolder Title:(NSString *)title status:(BOOL)status tag:(NSInteger)tag frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate
{
    self = [super initWithFrame:frame];
    
    if (self != nil)
    {
        MyExperienceControl *control = [[MyExperienceControl alloc]initWithPlaceHolderTitle:placeHolder Title:title status:status tag:tag frame:CGRectMake(0, 0, frame.size.width, frame.size.height) bgColor:bgColor textColor:textColor delegate:delegate];
        control.tag = tag;
        [control addTarget:self action:@selector(clickB:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:control];
        
        self.textField = control.textField;
        self.label     = control.controlLabel;
        self.icon      = control.icon;
        self.backgroundColor = [UIColor whiteColor];
        self.delegate = delegate;
    }
    return self;
}

-(void)clickB:(MyExperienceControl *)sender
{
    //代理回调
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolSliderViewBtnSelect:btn:)]) {
        [self.delegate protocolSliderViewBtnSelect:sender.tag btn:sender];
    }
}


@end


@implementation MyExperienceControl

-(UILabel *)controlLabel
{
    if (!_controlLabel) {
        _controlLabel = [UILabel new];
        _controlLabel.font = [UIFont systemFontOfSize:F(15)];
        
    }
    return _controlLabel;
}

-(UITextField *)textField
{
    if (!_textField) {
        _textField = [UITextField new];
        _textField.font = [UIFont systemFontOfSize:F(15)];
    }
    return _textField;
}

-(instancetype)initWithPlaceHolderTitle:(NSString *)placeHolder Title:(NSString *)title status:(BOOL)status tag:(NSInteger)tag frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate
{
    self = [super initWithFrame:frame];
    if (self != nil)
    {
        UIView *line = [UIView new];
        if (tag == 1000) {
            line.hidden = YES;
        }
        
        if (status) //格式   YES label  NO  输入框
        {
            self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(frame.origin.x + 15, 15, W(28), W(28))];
            
            if (tag == 103)
            {
                _icon.hidden = YES;
                _icon.x = 0;
            }
            
            //设置图片和下划线
            [self setImageAndLine:_icon line:line];
            [self.controlLabel setFrame:CGRectMake(CGRectGetMaxX(_icon.frame) + 10, 20, self.width - CGRectGetMaxX(_icon.frame) - 10 * 2, 20)];
            self.controlLabel.centerY = _icon.centerY;
            if (title.length > 0)
            {
                self.controlLabel.text = title;
                self.controlLabel.textColor = COLOR_LABELThreeCOLOR;
            }else{
                self.controlLabel.text = placeHolder;
                self.controlLabel.textColor = [HexStringColor colorWithHexString:@"999999"];
            }
            
            self.controlLabel.centerY = _icon.centerY;
            
            [self addSubview:self.controlLabel];
            
        }else
        {
            self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(frame.origin.x + 15, 15, W(28), W(28))];
            //设置图片和下划线
            [self setImageAndLine:self.icon line:line];
            [self.textField setFrame:CGRectMake(CGRectGetMaxX(self.icon.frame) + 10, 0, self.width - CGRectGetMaxX(self.icon.frame) - 10 * 2, self.height)];
//            [self.textField setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
//            [self.textField setValue:[UIFont boldSystemFontOfSize:16]forKeyPath:@"_placeholderLabel.font"];
//            self.textField.placeholder = placeHolder;
            self.textField.text = title;
            self.textField.centerY = self.centerY;
            self.textField.textColor = COLOR_LABELThreeCOLOR;
            UIColor *color = [HexStringColor colorWithHexString:@"999999"];
            self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeHolder attributes:@{NSForegroundColorAttributeName: color}];
            [self addSubview:self.textField];
        }
    }
    return self;
}



-(void)setImageAndLine:(UIImageView *)imageView line:(UIView *)line
{
    imageView.image = [UIImage imageNamed:@"greenAdd"];
    imageView.centerY = self.centerY;
    [self addSubview:imageView];
    
    [line setFrame:CGRectMake(15, self.height - 1, self.width - 15 * 2, 1)];
    line.backgroundColor = COLOR_SEPARATE_COLOR;
    [self addSubview:line];
}


@end
