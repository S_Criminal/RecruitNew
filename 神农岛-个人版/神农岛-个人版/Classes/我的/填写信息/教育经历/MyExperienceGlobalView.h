//
//  MyExperienceGlobalView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyExperienceControl;
@protocol ExperienceDelegate <NSObject>

@optional
- (void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(MyExperienceControl *)control;

@end

@interface MyExperienceGlobalView : UIView

//代理
@property (nonatomic, weak) id<ExperienceDelegate> delegate;

@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic ,strong) UILabel *label;
@property (nonatomic ,strong) UIImageView *icon;

+(instancetype)initWithPlaceHolderTitle:(NSString *)placeHolder Title:(NSString *)title status:(BOOL)status tag:(NSInteger)tag frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate;
-(instancetype)initWithPlaceHolderTitle:(NSString *)placeHolder Title:(NSString *)title status:(BOOL)status tag:(NSInteger)tag frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate;

@end

@interface MyExperienceControl : UIControl

@property (nonatomic ,strong) UILabel *controlLabel;
@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic ,strong) UIImageView *icon;

-(instancetype)initWithPlaceHolderTitle:(NSString *)placeHolder Title:(NSString *)title status:(BOOL)status tag:(NSInteger)tag frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate;

@end


