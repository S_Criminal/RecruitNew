//
//  EduSchoolMatchInfoVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol selectSchoolMatch <NSObject>

-(void)didSelectTableSchoolMatch:(NSString *)str;

@end

@interface EduSchoolMatchInfoVC : UIViewController
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,weak) id<selectSchoolMatch>delegate;

-(void)changeArray:(NSArray *)array;

@end
