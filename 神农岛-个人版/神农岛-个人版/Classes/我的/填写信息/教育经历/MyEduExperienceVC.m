//
//  MyEduExperienceVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyEduExperienceVC.h"

#import "MyExperienceGlobalView.h"
#import "GeneralPickerView.h"
//请求
#import "RequestApi+Login.h"
#import "RequestApi+MyBrief.h"

//model
#import "BriefInfoModel.h"
#import "GlobalDateModel.h"
#import "PickerEduModel.h"          //学历model
#import "DateArrModel.h"            //时间model
#import "EducationModel.h"
#import "MatchSchollModel.h"        //匹配学校model

#import "EduSchoolMatchInfoVC.h"    //匹配tableview

#import "KYAlertView.h"

#import <objc/runtime.h>

//static char associatedObjectKey;


@interface MyEduExperienceVC ()<ExperienceDelegate,UITextFieldDelegate,GeneralPickerViewDelegate,selectSchoolMatch>

@property (nonatomic ,strong) MyExperienceGlobalView *schoolView;
@property (nonatomic ,strong) MyExperienceGlobalView *majorView;
@property (nonatomic ,strong) MyExperienceGlobalView *startView;
@property (nonatomic ,strong) MyExperienceGlobalView *endView;
@property (nonatomic ,strong) MyExperienceGlobalView *degreeView;
@property (nonatomic ,strong) UITextField *workTextField;

@property (nonatomic ,strong) GeneralPickerView *pickerView;

@property (nonatomic ,strong) NSArray *degreeArray;
@property (nonatomic ,strong) NSArray *yearArr;
@property (nonatomic ,strong) NSArray *monthArr;

@property (nonatomic ,strong) NSArray *listSearchArr;

@property (nonatomic ,strong) UIButton *deleteButton;
@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) EduSchoolMatchInfoVC *matchTableVC;

@property (nonatomic ,strong) BaseNavView *navView;

@property (nonatomic ,strong) NSString *uploadSchoolIconStr;

@property (nonatomic ,strong) BriefInfoModel *infoModel;
@property (nonatomic ,strong) DateArrModel *dateModel;

@end

static void *associatedObjectKey = &associatedObjectKey;

@implementation MyEduExperienceVC{
    NSString *_school    ;
    NSString *_major     ;
    NSString *_startTime ;
    NSString *_endTime   ;
    NSString *_degree    ;
}

-(void)setRuntimeStr:(NSString *)runtimeStr
{
    objc_setAssociatedObject(self, associatedObjectKey, @"嘿嘿", OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSString *)runtimeStr
{
    return objc_getAssociatedObject(self, &associatedObjectKey);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self setNav];
    [self setBasicView];
    [self setModel];
    [self setUpChildVC];
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"教育经历" leftImageName:@"" leftBlock:^{
        [weakSelf checkChange];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }];
    
    [self.view addSubview:self.navView];
    
    [self.view addSubview:[GlobalMethod addNavLine]];
    self.view.backgroundColor = [UIColor whiteColor];
}


#pragma mark  退出时 检查是否做出更改

-(void)checkChange
{
    NSString *message = @"";
    [self.view endEditing:YES];
    if (!_datas2Model) {
        if (!kStringIsEmpty(self.schoolView.textField.text) || !kStringIsEmpty(_majorView.textField.text) || ![self.startView.label.text isEqualToString:@"入学时间"] || ![self.endView.label.text isEqualToString:@"毕业时间"] || ![self.degreeView.label.text isEqualToString:@"学历"]) {
            message = @"教育经历尚未保存，是否保存？";
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }else if (![_school isEqualToString:self.schoolView.textField.text]) {
        message = @"学校已更改，是否保存";
    }else if (![_major isEqualToString:self.majorView.textField.text]) {
        message = @"专业已更改，是否保存";
    }else if (![_degree isEqualToString:self.degreeView.label.text]) {
        message = @"学历已更改，是否保存";
    }
    
    if (kStringIsEmpty(message))
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    KYAlertView *alertView = [KYAlertView sharedInstance];
    DSWeak;
    [alertView showAlertView:@"提示" message:message subBottonTitle:@"不保存" cancelButtonTitle:@"保存" handler:^(AlertViewClickBottonType bottonType) {
        if (bottonType == AlertViewClickBottonTypeSubBotton) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }else if (bottonType == AlertViewClickBottonTypeCancelButton)
        {
            [weakSelf saveInfo];
        }
    }];
}

//保存教育经历
-(void)saveInfo
{
    _startTime  = [self setChangeTime:self.startView.label.text];
    _endTime    = [self setChangeTime:self.endView.label.text];
    
    NSString *school = self.schoolView.textField.text;
    NSString *major  = self.majorView.textField.text;
    NSString *degree = self.degreeView.label.text;
    NSString *schlogo  = kStringIsEmpty(self.uploadSchoolIconStr) ? @"" : self.uploadSchoolIconStr;
    
    if (!kStringIsEmpty(school)) {
        if (school.length < 4) {
            [MBProgressHUD showError:@"学校名称不能小于四位" toView:self.view];
            return;
        }
    }
    if (kStringIsEmpty(major)) {
        [MBProgressHUD showError:@"专业不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(degree) || [degree isEqualToString:@"学历"]) {
        [MBProgressHUD showError:@"学历不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(_startTime)) {
        [MBProgressHUD showError:@"开学年份不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(_endTime)) {
        [MBProgressHUD showError:@"毕业年份不能为空" toView:self.view];
        return;
    }
    
    NSDecimalNumber *workID = [NSDecimalNumber decimalNumberWithString:self.editID];
    self.editID = [workID stringValue];
    
    DSWeak;
    
    self.navView.backBtn.userInteractionEnabled = NO;
    
    
    [RequestApi editEduexpWithKey:[GlobalData sharedInstance].GB_Key edit:self.edit expID:self.editID school:school major:major startd:_startTime endd:_endTime schlogo:schlogo Delegate:self degree:degree success:^(NSDictionary *response) {
        self.navView.backBtn.userInteractionEnabled = YES;
        Datas2 *model = [[Datas2 alloc]init];
        model.eSchool = school;
        model.eMajor = major;
        model.eStartD = _startTime;
        model.eEndD = _endTime;
        model.eDegree = degree;
        model.iDProperty = [response[@"EEID"] doubleValue];
        model.eBackUp = schlogo;
        
        if ([weakSelf.editID isEqualToString:[GlobalMethod doubleToString:model.iDProperty]])
        {
            [[GlobalData sharedInstance].GB_UserModel.datas2 removeObject:weakSelf.datas2Model];
        }
        
        [[GlobalData sharedInstance].GB_UserModel.datas2 addObject:model];
        
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        
        [MBProgressHUD showSuccess:@"保存教育经历成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *str) {
        self.navView.backBtn.userInteractionEnabled = YES;
        [MBProgressHUD showError:str toView:weakSelf.view];
    }];

    
}

-(void)setDatas2Model:(Datas2 *)datas2Model
{
    _datas2Model = datas2Model;
    
}

-(void)setBasicView
{
    NSString *bgColor   = @"222222";
    NSString *textColor = @"666666";
    
    
    _school    = _datas2Model.eSchool;
    _major     = _datas2Model.eMajor;
    _startTime = _datas2Model.eStartD;
    _endTime   = _datas2Model.eEndD;
    _degree    = _datas2Model.eDegree;
    
    
    
    
    _startTime = [self setChangeTime:_startTime];
    _endTime   = [self setChangeTime:_endTime];
    
    CGFloat heigt = W(48);
    
    _schoolView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"学校名称" Title:_school status:NO tag:100 frame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH,  heigt) bgColor:bgColor textColor:textColor delegate:self];
    [_schoolView.textField addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventEditingChanged];
    [_schoolView.icon sd_setImageWithURL:[NSURL URLWithString:_datas2Model.eBackUp] placeholderImage:[UIImage imageNamed:@"greenAdd"]];
    
    _majorView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"所学专业" Title:_major status:NO tag:101 frame:CGRectMake(0, CGRectGetMaxY(_schoolView.frame), KWIDTH, heigt ) bgColor:bgColor textColor:textColor delegate:self];
    
    _startView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"入学时间" Title:_startTime status:YES tag:102 frame:CGRectMake(0,CGRectGetMaxY(_majorView.frame) + W(30), KWIDTH / 2, heigt ) bgColor:bgColor textColor:textColor delegate:self];
    
    _endView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"毕业时间" Title:_endTime status:YES tag:103 frame:CGRectMake(CGRectGetMaxX(_startView.frame) + W(5),_startView.y, KWIDTH / 2,  heigt) bgColor:bgColor textColor:textColor delegate:self];
    
    _degreeView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"学历" Title:_degree status:YES tag:104 frame:CGRectMake(0, CGRectGetMaxY(_startView.frame), KWIDTH, heigt) bgColor:bgColor textColor:textColor delegate:self];
    
    [self.view addSubview:_schoolView];
    [self.view addSubview:_majorView];
    [self.view addSubview:_startView];
    [self.view addSubview:_endView];
    [self.view addSubview:_degreeView];
    if (!kStringIsEmpty(self.editID) && ![self.editID isEqualToString:@"0"]) {
        //删除教育经历
        [self createDeleteButton];
    }
}

//删除教育经历
-(void)createDeleteButton
{
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setFrame:CGRectMake(MyViewLeftConstraint, CGRectGetMaxY(_degreeView.frame) + W(15), KWIDTH - MyViewLeftConstraint * 2, 45)];
    [self.deleteButton setCorner:5];
    self.deleteButton.backgroundColor = COLOR_MAINCOLOR;
    self.deleteButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
    [self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteButton setTitle:@"删除教育经历" forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(tapDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.deleteButton];
}

//删除教育经历 点击事件
-(void)tapDelete:(UIButton *)sender
{
    DSWeak;
    NSDecimalNumber *workID = [NSDecimalNumber decimalNumberWithString:self.editID];
    self.editID = [workID stringValue];
    [RequestApi deleteEduWithKey:[GlobalData sharedInstance].GB_Key workID:self.editID Delegate:self success:^(NSDictionary *response) {
        [[GlobalData sharedInstance].GB_UserModel.datas2 removeObject:self.datas2Model];
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];


        [MBProgressHUD showSuccess:@"删除教育经历成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }];
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(NSString *)setChangeTime:(NSString *)time
{
    if (kStringIsEmpty(time) || [time containsString:@"时间"]) {
        return time;
    }
    NSArray *array = [time componentsSeparatedByString:@"-"];
    NSString *one = array.firstObject;
    NSString *two = array[1];
   
    one = [one stringByReplacingOccurrencesOfString:@"年" withString:@""];
    two = [two stringByReplacingOccurrencesOfString:@"月" withString:@""];
    time = [NSString stringWithFormat:@"%@-%@",one,two];
    if (!kStringIsEmpty(time) && time.length > 7)
    {
        time = [time substringToIndex:7];
    }
    return time;
}


-(void)setModel
{
    self.infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"pickerModel" ofType:@"plist"];
    self.degreeArray = [NSArray arrayWithContentsOfFile:path];
    NSLog(@"%@",self.degreeArray);
    if (!kArrayIsEmpty(self.degreeArray))
    {
        return;
    }
    self.degreeArray = [NSArray array];
    DSWeak;
    [RequestApi getEduListWithDelegate:self Success:^(NSDictionary *response) {
        if ([response[@"code"]isEqualToNumber:@200])
        {
            NSArray *datasArr = response[@"datas"];
            NSMutableArray *array = [NSMutableArray array];
            for (int i = 0; i < datasArr.count; i ++ )
            {
                NSDictionary *dic = [NSDictionary dictionaryWithDictionary:datasArr[i]];
                PickerEduModel *model = [PickerEduModel modelObjectWithDictionary:dic];
                [array addObject:model];
            }
            [GlobalMethod whiteArrayToPlist:array filePath:nil];
            weakSelf.degreeArray = array;
        }
    }];
    
    /** 请求日期年月数据 */
    self.dateModel = [[DateArrModel alloc]init];
    int minYear = (int) self.dateModel.nowYear - 60 ;
    
    [self.dateModel setYearArrayModelWithMinIndex:minYear maxIndex:0 yearBlock:^(NSMutableArray *yArr) {
        weakSelf.yearArr = yArr;
    } monthBlock:^(NSMutableArray *mArr) {
        weakSelf.monthArr = mArr;
    }];
}

#pragma mark 添加子视图控制器----匹配table
-(void)setUpChildVC
{
    self.matchTableVC = [EduSchoolMatchInfoVC new];
    self.matchTableVC.delegate = self;
    self.matchTableVC.view.frame = CGRectMake(0, self.schoolView.bottom, KWIDTH, KHEIGHT / 2 - 100);
    self.matchTableVC.view.hidden = YES;
    [self addChildViewController:self.matchTableVC];
    [self.view addSubview:self.matchTableVC.view];
}



#pragma mark  点击label的协议方法

-(void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(MyExperienceControl *)control
{
    
    if (tag == 100 || tag == 101) {
        return;
    }
    
    [self.schoolView.textField resignFirstResponder];
    [self.majorView.textField resignFirstResponder];
    
    NSInteger timeInteger = [control.controlLabel.text isEqualToString:@"入学时间"]  ? 20 : 23;
    
    NSArray *textArr = [control.controlLabel.text componentsSeparatedByString:@"-"];
    textArr          = textArr.count > 1 ? textArr : [[GlobalMethod getBirth] componentsSeparatedByString:@"-"];    //有出生年月且未填写信息时，默认出生年 + 20
    NSString *firstTitle = @"1970";
    NSString *secondTitle;
    //默认选择
    if (textArr.count > 1) {
        if ([textArr.firstObject integerValue] + 20 > self.dateModel.nowYear) {
            firstTitle = [NSString stringWithFormat:@"%ld",self.dateModel.nowYear];
        }else{
            firstTitle = [NSString stringWithFormat:@"%ld",[[self.infoModel.uBirthday substringToIndex:4] integerValue] + 20];
            firstTitle = control.controlLabel.text.length > 4 ? firstTitle: [NSString stringWithFormat:@"%ld",[[textArr.firstObject substringToIndex:4] integerValue] + timeInteger]  ;    //默认出生年 + 20
        }
        secondTitle= textArr[1];
    }else{
        firstTitle = textArr.firstObject;
        
    }
    
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    _pickerView.indexTag = tag;
    NSLog(@"%@",self.degreeArray);
    
    NSString *title;
    NSArray *firstArray ;
    NSArray *secondArray;
    
    NSInteger selectModel;  //判断是否是时间选择器，@2是时间选择器
    selectModel = 2;
    if (tag == 104)
    {
        title = @"学历";
        PickerEduModel *model = self.degreeArray.firstObject;
        firstTitle = [control.controlLabel.text isEqualToString:@"学历"] ? model.ename : control.controlLabel.text;
        firstArray  = [self arrayWithTag:tag Array:self.degreeArray];
        selectModel = 1;
    }else if (tag == 102){
        title = @"开学时间";
        firstArray  = self.yearArr;
        secondArray = self.monthArr;
    }else if (tag == 103){
        _pickerView.startTime = _startView.label.text;
        title = @"毕业时间";
        firstArray  = self.yearArr;
        secondArray = self.monthArr;
    }
    
    [_pickerView setUpPickerViewTitle:title selectFirstTitle:firstTitle selectSecondTitle:secondTitle selectModel:selectModel firstArray:firstArray secondArray:secondArray];
    _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    

    [self.view addSubview:_pickerView];
    [self showPickerView];
}

-(NSArray *)arrayWithTag:(NSInteger)tag Array:(NSArray *)array
{
    NSMutableArray *firArray = [NSMutableArray array];
    for (int i = 0; i < array.count; i++)
    {
        if (tag == 104)
        {
            PickerEduModel *model = array[i];
            [firArray addObject:model.ename];
        }
    }
    return firArray;
}



-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    NSLog(@"===content%@===",content);
    NSString *endContent    = _endView.label.text;
    NSString *startContent  = _startView.label.text;
    startContent = [startContent stringByReplacingOccurrencesOfString:@"年" withString:@""];
    startContent = [startContent stringByReplacingOccurrencesOfString:@"月" withString:@""];
    endContent = [endContent stringByReplacingOccurrencesOfString:@"年" withString:@""];
    endContent = [endContent stringByReplacingOccurrencesOfString:@"月" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"年" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"月" withString:@""];
    
    if (tag == 102) {
        if (![endContent isEqualToString:@"毕业时间"]) {
            if (([[endContent substringToIndex:4] integerValue] >= [[content substringToIndex:4]integerValue])) {
                if ([[endContent substringWithRange:NSMakeRange(5, 2)] integerValue] >= [[content substringWithRange:NSMakeRange(5, 2)] integerValue]) {
                    _startView.label = [self setLabel:_startView.label Content:content];
                }else{
                    [MBProgressHUD showError:@"开学时间不能大于毕业时间" toView:self.view];
                }
            }else{
                [MBProgressHUD showError:@"开学时间不能大于毕业时间" toView:self.view];
            }
        }else{
            _startView.label = [self setLabel:_startView.label Content:content];
        }
    }else if (tag == 103){

        if (!([[startContent substringToIndex:4] integerValue] >= [[content substringToIndex:4]integerValue])) {
            
            _endView.label = [self setLabel:_endView.label Content:content];
            _endTime = content;
            
        }else{
            if ([[startContent substringToIndex:4] integerValue] == [[content substringToIndex:4] integerValue]) {
                if ([[startContent substringWithRange:NSMakeRange(5, 2)] integerValue] > [[content substringWithRange:NSMakeRange(5, 2)] integerValue]) {
                    [MBProgressHUD showError:@"开学时间不能大于毕业时间" toView:self.view];
                }else{
                    _endView.label = [self setLabel:_endView.label Content:content];
                    _endTime = content;
                }
            }else{
                if ([[startContent substringToIndex:4] integerValue] > [[content substringToIndex:4] integerValue]) {
                    [MBProgressHUD showError:@"开学时间不能大于毕业时间" toView:self.view];
                }else{
                    _endView.label = [self setLabel:_endView.label Content:content];
                    _endTime = content;
                }
            }
        }
    }else if (tag == 104) {
        _degreeView.label = [self setLabel:_degreeView.label Content:content];
    }
    [self hidePickerView];
}

-(UILabel *)setLabel:(UILabel *)label Content:(NSString *)content
{
    if (!kStringIsEmpty(content)) {
        label.text = content;
        label.textColor = COLOR_LABELThreeCOLOR;
    }
    return label;
}

-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [_schoolView.textField resignFirstResponder];
    [_majorView.textField resignFirstResponder];
}

#pragma mark UITexteField delegate

-(void)textFieldChange:(UITextField *)sender
{
    if (sender.text.length == 0) {
        self.matchTableVC.view.hidden = YES;
        return;
    }
    [RequestApi matchSchoolNameWithKey:[GlobalData sharedInstance].GB_Key schoolName:sender.text WithDelegate:self Success:^(NSDictionary *response) {
        if ([response[@"code"]isEqualToNumber:@200])
        {
            _listSearchArr = response[@"datas"];
            if (_listSearchArr.count > 0)
            {
                self.matchTableVC.view.height = _listSearchArr.count > 4 ? KHEIGHT / 2 - W(80) : _listSearchArr.count *( W(50) + 1) + 10;
                [self.matchTableVC changeArray:_listSearchArr];
                self.matchTableVC.view.hidden = NO;
            }else{
                self.matchTableVC.view.hidden = YES;
            }
        }
    } failure:^(NSString *str) {
        
    }];
}

-(void)didSelectTableSchoolMatch:(MatchSchollModel *)model
{
    self.schoolView.textField.text = model.sName;
    [self.schoolView.icon sd_setImageWithURL:[NSURL URLWithString:model.sLogo] placeholderImage:[UIImage imageNamed:@"greenAdd"]];
    self.matchTableVC.view.hidden = YES;
    self.uploadSchoolIconStr = model.sLogo;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)dealloc
{
    [_pickerView removeFromSuperview];
    _pickerView = nil;
    self.hideDelayTimer = nil;
    [self.hideDelayTimer invalidate];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
