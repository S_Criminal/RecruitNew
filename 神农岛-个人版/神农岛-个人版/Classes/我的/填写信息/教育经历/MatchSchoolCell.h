//
//  MatchSchoolCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchSchollModel.h"
@interface MatchSchoolCell : UITableViewCell

@property (nonatomic ,strong) UILabel *label;
@property (nonatomic ,strong) UIView *line;
@property (nonatomic ,strong) UIImageView *iconImage;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(MatchSchollModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(MatchSchollModel *)str;

@end
