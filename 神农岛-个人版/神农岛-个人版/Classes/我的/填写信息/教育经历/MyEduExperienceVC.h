//
//  MyEduExperienceVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC.h"
#import "Datas2.h"
@interface MyEduExperienceVC : BasicVC
@property (nonatomic ,strong) NSString *edit;
@property (nonatomic ,strong) NSString *editID;
@property (nonatomic ,strong) Datas2 *datas2Model;

@property (nonatomic ,strong) NSString *runtimeStr;

@end
