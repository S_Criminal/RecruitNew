//
//  EduSchoolMatchInfoVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "EduSchoolMatchInfoVC.h"

#import "MatchSchoolCell.h"
#import "MatchSchollModel.h"

@interface EduSchoolMatchInfoVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSMutableArray *listArr;
@end

@implementation EduSchoolMatchInfoVC


-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, self.view.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    [self setModel];
    [self setBasicView];
    
}

-(void)setModel
{
    
}

-(void)setBasicView
{
    [self.tableView registerClass:[MatchSchoolCell class] forCellReuseIdentifier:@"MatchSchoolCell"];
    [self.view addSubview:self.tableView];
}



-(void)changeArray:(NSArray *)array
{
    
    self.listArr = [NSMutableArray array];
    
    for (NSDictionary *dic in array)
    {
        MatchSchollModel *model = [MatchSchollModel modelObjectWithDictionary:dic];
        [self.listArr addObject:model];
    }
    self.tableView.height = self.view.height;
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return W(50) + 1;
}

-(MatchSchoolCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchSchoolCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MatchSchoolCell"];
    [cell resetCellWithModel:_listArr[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectTableSchoolMatch:)]) {
        [self.delegate didSelectTableSchoolMatch:_listArr[indexPath.row]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
