//
//  MatchSchoolCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MatchSchoolCell.h"

@implementation MatchSchoolCell

#pragma mark 懒加载

- (UILabel *)label{
    if (_label == nil) {
        _label = [UILabel new];
        [GlobalMethod setLabel:_label widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _label;
}
- (UIView *)line{
    if (_line == nil) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

-(UIImageView *)iconImage
{
    if (!_iconImage) {
        _iconImage = [UIImageView new];
        _iconImage.image = [UIImage imageNamed:@"教育经历"];
    }
    return _iconImage;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.label];
        [self.contentView addSubview:self.line];
        [self.contentView addSubview:self.iconImage];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(MatchSchoolCell)

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(MatchSchollModel *)model
{
    //刷新view
    self.iconImage.leftTop = XY(W(15), W(7));
    self.iconImage.widthHeight = XY(W(36), W(36));
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:model.sLogo] placeholderImage:[UIImage imageNamed:@"教育经历"]];
    [GlobalMethod resetLabel:self.label text:model.sName isWidthLimit:0];
    self.label.leftTop = XY(self.iconImage.right + W(15),W(15));
    self.label.centerY = self.iconImage.centerY;
    
    self.line.widthHeight = XY(KHEIGHT - self.iconImage.left, 1);
    self.line.leftTop = XY(self.iconImage.left , W(50));
    
    return self.line.bottom;
}

@end
