//
//  MyEduExperienceVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyEduExperienceListVC.h"
#import "EducationCell.h"
#import "BaseClass.h"
#import "EduORWorkTitleHeaderView.h"
#import "MyEduExperienceVC.h"

#import "EducationModel.h"

@interface MyEduExperienceListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listArr;

@end

@implementation MyEduExperienceListVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasiceView];
    
    self.view.backgroundColor = COLOR_BGCOLOR;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModel];
}

-(void)setNav
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.backgroundColor = [UIColor whiteColor];
    [navView addSubview:[BaseNavView initNavTitle:@"个人经历" leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:navView];
}

-(void)setBasiceView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight , KWIDTH, KHEIGHT - NAVIGATION_BarHeight ) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self createTableHeaderView];
}


-(void)createTableHeaderView
{
    EduORWorkTitleHeaderView *sectionView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(50))];
    sectionView.titleL.text = @"教育经历";
    [sectionView.editButton addTarget:self action:@selector(tapAddEdu:) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableHeaderView = sectionView;
}

//添加新的教育经历
-(void)tapAddEdu:(UIButton *)sender
{
    MyEduExperienceVC *eduInfo = [[MyEduExperienceVC alloc]init];
    eduInfo.editID  = @"0";
    eduInfo.edit    = @"add";
    [self presentViewController:eduInfo animated:YES completion:nil];
}

-(void)setModel
{
    _listArr = [GlobalData sharedInstance].GB_UserModel.datas2;
    NSLog(@"%@",_listArr);
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [EducationCell fetchHeight:_listArr[indexPath.row]];
//    [self.tableView cellHeightForIndexPath:indexPath model:_listArr[indexPath.row] keyPath:@"model" cellClass:[EducationCell class] contentViewWidth:KWIDTH];
    return h;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(EducationCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EducationCell *cell = [[EducationCell alloc]init];
    if (!cell)
    {
        cell = [[EducationCell alloc]init];
    }
    [cell.bgView setCorner:5];
    cell.line.hidden = YES;
    [cell resetCellWithModel:_listArr[indexPath.section]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Datas2 *datas = [[GlobalData sharedInstance].GB_UserModel.datas2 objectAtIndex:indexPath.section];
    NSLog(@"%@",[GlobalData sharedInstance].GB_UserModel.datas2);
    NSLog(@"%@",datas);
    
    MyEduExperienceVC *eduInfo = [[MyEduExperienceVC alloc]init];
    
    eduInfo.editID  = [GlobalMethod doubleToString:datas.iDProperty];
    eduInfo.edit    = @"upd";
    eduInfo.datas2Model = datas;
//    EducationModel *model = [[EducationModel alloc]init];
//    model.eduID = @"222";
    [self presentViewController:eduInfo animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
