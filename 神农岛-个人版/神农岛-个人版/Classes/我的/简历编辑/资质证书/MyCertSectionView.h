//
//  MyCertSectionView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCertSectionView : UIView
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UIView *line;
#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model;
@end
