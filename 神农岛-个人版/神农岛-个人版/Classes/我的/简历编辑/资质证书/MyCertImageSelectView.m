//
//  MyCertImageSelectView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyCertImageSelectView.h"



@implementation MyCertImageSelectView

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.frame = self.frame;
        _bgView.y = 0;
        _bgView.backgroundColor = [UIColor clearColor];
    }
    return _bgView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createViews];
    }
    return self;
}

-(void)createViews
{
   
}

-(void)setupImageArray:(NSArray *)array
{
    if (_bgView)
    {
        [_bgView removeFromSuperview];
        _bgView = nil;
    }
    [self addSubview:self.bgView];
    self.userInteractionEnabled = YES;
    CGFloat imageWidth = (KWIDTH - W(15) * 2) / 4;
    for (int i = 0; i < 9 ; i ++ )
    {
        CertCustomControl *control = [[CertCustomControl alloc]initWithFrame:CGRectMake(0, 0, imageWidth, imageWidth)];
        control.widthHeight = XY(imageWidth, imageWidth);
        if (i < 4) {
            control.leftTop = XY(W(15) + (imageWidth + 0) * i, W(15));
        }else{
            control.leftTop = XY(W(15) + (imageWidth + 0) * (i - 4) , W(15) * 2 + imageWidth);
        }
        
        if (i < array.count)
        {
            id object = array[i];
            
            if ([object isKindOfClass:[NSString class]]) {
                [control.imgView sd_setImageWithURL:[NSURL URLWithString:array[i]]];
            }else{
                control.imgView.image = object;
            }
            control.deleteButton.hidden = NO;
            control.deleteButton.tag = i + 100;
            [control.deleteButton addTarget:self action:@selector(tapDeleteImage:) forControlEvents:UIControlEventTouchUpInside];
        }else if(array.count != 8)
        {
            if (i == array.count)
            {
                control.imgView.image = [UIImage imageNamed:@"绿色相机"];
                [control addTarget:self action:@selector(tapImageAlbum:) forControlEvents:UIControlEventTouchUpInside];
                NSLog(@"%@----%d",control,i);
            }
        }
        [self.bgView addSubview:control];
    }
}

-(void)tapImageAlbum:(UIControl *)control
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(protocolTapAlbum:)])
    {
        [self.delegate protocolTapAlbum:control];
    }
}

-(void)tapDeleteImage:(UIButton *)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(protocolDeleteButton:)])
    {
        [self.delegate protocolDeleteButton:sender];
    }
}

@end

@implementation CertCustomControl

-(UIImageView *)imgView
{
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds = YES;
    }
    return _imgView;
}

-(UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.backgroundColor = [UIColor clearColor];
        [_deleteButton setImage:[UIImage imageNamed:@"红底白叉"] forState:UIControlStateNormal];
        _deleteButton.hidden = YES;
    }
    return _deleteButton;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.imgView];
        [self addSubview:self.deleteButton];
        _imgView.frame = CGRectMake(0, 15, frame.size.width - 10, frame.size.height - 10);
        _deleteButton.frame = CGRectMake(_imgView.width - 15, 0, 30, 30);
    }
    return self;
}

@end
