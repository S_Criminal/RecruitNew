//
//  MyCertImageSelectView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CertCustomControl;

@protocol certImageSelectDelegate <NSObject>

-(void)protocolTapAlbum:(UIControl *)control;

-(void)protocolDeleteButton:(UIButton *)button;

@end


@interface MyCertImageSelectView : UIView

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) id <certImageSelectDelegate> delegate;



-(void)setupImageArray:(NSArray *)array;

@end


@interface CertCustomControl :UIControl
@property (nonatomic ,strong) UIImageView *imgView;
@property (nonatomic ,strong) UIButton *deleteButton;
@end
