//
//  MyCertViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyCertViewController.h"
#import "MyCertSectionView.h"
#import "MyCertImageSelectView.h"
//#import "CertImageModel.h"


#import "BasicVC+BaseImageSelectVC.h"


#import "RequestApi+MyBrief.h"
@interface MyCertViewController ()<certImageSelectDelegate,RequestDelegate>
@property (nonatomic ,strong) NSMutableArray *pathImageArr;
@property (nonatomic ,strong) NSMutableArray *uploadPathArray;
@property (nonatomic ,strong) MyCertImageSelectView *imageSelectView;
@property (nonatomic ,strong) NSString *imageTitle;
@property (nonatomic ,strong) AllImageModel *model;
@property (nonatomic ,strong) NSString *contents;
@property (nonatomic ,strong) NSString *titles;
@property (nonatomic ,strong)  BaseNavView *navView;

@end

@implementation MyCertViewController{
    NSInteger isNowUpLoadImage;
}

-(MyCertImageSelectView *)imageSelectView
{
    if (!_imageSelectView) {
        _imageSelectView = [MyCertImageSelectView new];
        _imageSelectView.delegate = self;
    }
    return _imageSelectView;
}

-(instancetype)initStatus:(NSString *)status
{
    MyCertViewController *cert = [MyCertViewController new];
    cert.status = status;
    return cert;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isNowUpLoadImage = 0;
    [self setNav];
    [self setModel];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"资质证书" leftImageName:@"" leftBlock:^{
        [GB_Nav dismissViewControllerAnimated:YES completion:nil];
        //        [GB_Nav popViewControllerAnimated:YES];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf save];
    }];
    [self.view addSubview:self.navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)save
{
    NSLog(@"%@",self.uploadPathArray);
    if (isNowUpLoadImage == 1) {
        [MBProgressHUD showError:@"图片正在上传,请稍后" toView:self.view];
        return;
    }
    if (self.uploadPathArray.count == 0) {
        [MBProgressHUD showError:@"请选择至少一张图片" toView:self.view];
        return;
    }
    
    NSString *editID;
    if (!kObjectIsEmpty(self.model)) {
        editID =  [GlobalMethod doubleToString:self.model.iDProperty];
        _contents = _model.sContents;
        _titles = _model.sTitle;
    }else{
        editID = @"0";
        _titles =@"";
        _contents = @"";
    }
    
    NSString *spath  = [self.uploadPathArray componentsJoinedByString:@","];
    
    [RequestApi editPersonAchieveWithKey:[GlobalData sharedInstance].GB_Key editID:editID contents:_contents spath:spath sdesp:@"" sdate:@"" stitle:_titles sclass:self.status Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
    } failure:^(NSString *errorStr, id mark) {
        
    }];
    
}

-(void)setModel
{
    
    self.pathImageArr       = [NSMutableArray array];
    self.uploadPathArray    = [NSMutableArray array];
    
//    for (AllImageModel *model in [GlobalData sharedInstance].GB_UserModel.datas4)
//    {
//        if ([[GlobalMethod doubleToString:model.sClass] isEqualToString:self.status])
//        {
//            NSArray *arr = [model.sPath componentsSeparatedByString:@","];
//            self.pathImageArr = [NSMutableArray arrayWithArray:arr];
//            self.model = model;
//        }
//    }
    
    NSLog(@"%@",self.uploadPathArray);
    
    DSWeak;
    [RequestApi getkillsListWithKey:[GlobalData sharedInstance].GB_Key sclass:self.status Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        if (!kArrayIsEmpty(arr)) {
            AllImageModel *model = [AllImageModel modelObjectWithDictionary:arr.firstObject];
            NSArray *array = [model.sPath componentsSeparatedByString:@","];
            weakSelf.pathImageArr = [NSMutableArray arrayWithArray:array];
            weakSelf.model = model;
        }
        [_imageSelectView setupImageArray:self.pathImageArr];
        weakSelf.uploadPathArray =[NSMutableArray arrayWithArray:self.pathImageArr];
    } failure:^(NSString *errorStr, id mark) {
        
    }];
    
}

-(void)setBasicView
{
    switch ([self.status integerValue]) {
        case 0:
            self.imageTitle = @"资质证书";
            break;
        case 1:
            self.imageTitle = @"出版作品";
            break;
        case 2:
            self.imageTitle = @"专利发明";
            break;
        case 3:
            self.imageTitle = @"荣誉奖项";
            break;
        case 4:
            self.imageTitle = @"语言能力";
            break;
        default:
            break;
    }
    self.navView.labelTitle.text = self.imageTitle;
    MyCertSectionView *sectionView = [[MyCertSectionView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, 45)];
    [sectionView resetViewWithModel:nil];
    sectionView.titleLabel.text = [NSString stringWithFormat:@"请添加%@",self.imageTitle];
    [self.view addSubview:sectionView];
    
    [self.imageSelectView setFrame:CGRectMake(0, sectionView.bottom , KWIDTH, 300)];
    [self.view addSubview:_imageSelectView];
}

-(void)protocolTapAlbum:(UIControl *)control
{
    [self showImageVC:8 - (int)self.pathImageArr.count];
}

-(void)protocolDeleteButton:(UIButton *)button
{
    [self.pathImageArr removeObjectAtIndex:button.tag - 100];
    [self.uploadPathArray removeObjectAtIndex:button.tag - 100];
    [self.imageSelectView setupImageArray:self.pathImageArr];
}

-(void)imagesSelect:(NSArray *)aryImages
{
    if (aryImages.count ==  0) {
        return;
    }
    NSLog(@"%@",self.uploadPathArray);
    [self.pathImageArr addObjectsFromArray:aryImages];
    [_imageSelectView setupImageArray:self.pathImageArr];
    isNowUpLoadImage = 1;
    NSLog(@"%@",aryImages);
    DSWeak;
    NSLog(@"%@",self.uploadPathArray);
    for (id object in aryImages)
    {
         NSData *data = UIImagePNGRepresentation(object);
         [[RequestApi share] getImageUrlWithAliYunWithImageData:data BlockHandelImageUrlStr:^(NSString *str) {
             str = [NSString stringWithFormat:@"%@%@",ssndHTTP,str];
             [self.uploadPathArray addObject:str];
         } BlockHandelSuccess:^(BOOL success) {
             if (weakSelf.pathImageArr.count == weakSelf.uploadPathArray.count)
             {
                 isNowUpLoadImage = 2;
             }
         }];
    }
}


-(void)dealloc
{
    NSLog(@"certDeallloc");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
