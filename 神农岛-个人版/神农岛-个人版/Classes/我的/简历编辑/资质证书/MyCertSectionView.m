//
//  MyCertSectionView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyCertSectionView.h"

@implementation MyCertSectionView
#pragma mark 懒加载

- (UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [UIImageView new];
        _iconImageView.image = [UIImage imageNamed:@"greenAdd"];
        _iconImageView.widthHeight = XY(W(20),W(20));
    }
    return _iconImageView;
}

- (UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:F(15)];
        _titleLabel.textColor = COLOR_LABELThreeCOLOR;
        _titleLabel.text = @"请添加证书图片";
    }
    return _titleLabel;
}
- (UIView *)line{
    if (_line == nil) {
        _line = [UIView new];
        _line.tag = 5;
        _line.backgroundColor = COLOR_LINESCOLOR;
    }
    return _line;
}


#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}
- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubView];
    }
    return self;
}
//添加subview
- (void)addSubView{
    [self addSubview:self.iconImageView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.line];
    
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    MyCertSectionView * view = [MyCertSectionView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model{

//    [GlobalMethod removeAllSubView:self withTag:5];//移除线
    //刷新view
    self.iconImageView.leftTop = XY(W(15),W(15));
    self.titleLabel.leftTop = XY(CGRectGetMaxX(_iconImageView.frame) + W(10), 15);
    [self.titleLabel sizeToFit];
    self.titleLabel.centerY = _iconImageView.centerY;
    
    self.line.leftTop = XY(_iconImageView.x, _iconImageView.bottom + 15);
    self.line.widthHeight = XY(KWIDTH - W(15), 1);
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
