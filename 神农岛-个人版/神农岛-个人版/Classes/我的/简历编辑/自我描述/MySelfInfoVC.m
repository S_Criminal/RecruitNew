//
//  MySelfInfoVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MySelfInfoVC.h"
#import "BriefInfoModel.h"
#import "RequestApi+MyBrief.h"

@interface MySelfInfoVC () <UITextViewDelegate,RequestDelegate>
@property (nonatomic ,strong) UITextView *textView;
@property (nonatomic ,strong) UILabel *placeHolderLabel;
@property (nonatomic, weak) NSTimer *hideDelayTimer;
@property (nonatomic ,strong) BriefInfoModel *model ;
@end

@implementation MySelfInfoVC{
    CGFloat textFont;
    CGFloat textViewSpacingFont;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUI];
    [self setNav];
    [self createBasicView];
    
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"自我描述" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textView becomeFirstResponder];
}

-(void)setUI
{
    textFont = 15;
    textViewSpacingFont = 6 + 1;
    if (isIphone5) {
        textFont = 13;
        textViewSpacingFont = 4;
    }
}

#pragma mark 保存自我介绍

-(void)saveInfo
{
    if (self.textView.text.length < 10)
    {
        [MBProgressHUD showError:@"自我描述不得少于10个字" toView:self.view];
        return;
    }
    DSWeak;
    [RequestApi saveMySelfWithKey:[GlobalData sharedInstance].GB_Key selfevaluation:self.textView.text Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showError:@"保存成功" toView:self.view];
        self.model.rSelfEvaluation = self.textView.text;
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)createBasicView
{
    self.model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    
    NSString *selfInfo = self.model.rSelfEvaluation;
    
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(10, NAVIGATION_BarHeight + 1 + 10, KWIDTH - 20, KHEIGHT - 282 - 20 - NAVIGATION_BarHeight - 1)];
    _textView.font = [UIFont systemFontOfSize:F(16)];
    //    _textView.contentInset = UIEdgeInsetsMake(50, 100, 0, 0);
    _textView.delegate = self;
    _textView.scrollEnabled = YES;
    
    _placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, KWIDTH - 20, 20)];
    
    if (kStringIsEmpty(selfInfo)) {
        selfInfo = @"";
        _placeHolderLabel.hidden = NO;
    }else{
        _placeHolderLabel.hidden = YES;
    }
    _textView.text = selfInfo;

    _placeHolderLabel.text = @"详细描述一下你自己，字数控制在10~100字";
    _placeHolderLabel.font = [UIFont systemFontOfSize:textFont];
    _placeHolderLabel.textColor = [HexStringColor colorWithHexString:@"999999"];
    [_textView addSubview:_placeHolderLabel];
    [self.view addSubview:_textView];
}

//监听事件
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    //获取键盘高度
    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    self.textView.contentInset=UIEdgeInsetsMake(0, 0,keyboardRect.size.height, 0);
}

- (void)handleKeyboardDidHidden
{
    self.textView.contentInset=UIEdgeInsetsZero;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{     //其实你可以加在这个代理方法中。当你将要编辑的时候。先执行这个代理方法的时候就可以改变间距了。这样之后输入的内容也就有了行间距。
    if (textView.text.length < 1) {
        textView.text = @"间距";
        _placeHolderLabel.hidden = NO;
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:F(7)];
    
    NSDictionary *attributes = @{
                                 
                                 NSFontAttributeName:[UIFont systemFontOfSize:F(15)],
                                 
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 
                                 };
    
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];
    if ([textView.text isEqualToString:@"间距"]) {           //之所以加这个判断是因为再次编辑的时候还会进入这个代理方法，如果不加，会把你之前输入的内容清空。你也可以取消看看效果。
        textView.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:attributes];//主要是把“间距”两个字给去了。
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (text.length > 0) {
        self.placeHolderLabel.hidden = YES;
    }
    if (textView.text.length == 1 && text.length == 0) {
        self.placeHolderLabel.hidden = NO;
    }
    return YES;
}

-(void)dealloc{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
