//
//  BriefEditInventionVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/4/1.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllImageModel.h"           //数据model

@interface BriefEditInventionVC : UIViewController
@property (nonatomic ,strong) NSString *editID;
@property (nonatomic ,strong) AllImageModel *model;
@property (nonatomic , strong) void (^blockDele) (void);

@end
