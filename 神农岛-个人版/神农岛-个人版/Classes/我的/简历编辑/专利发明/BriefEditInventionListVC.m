//
//  BriefEditInventionListVC.m
//  神农岛-个人版
//
//  Created by 刘惠萍 on 2017/6/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefEditInventionListVC.h"
//view
#import "EduORWorkTitleHeaderView.h"
//vc
#import "BriefEditInventionVC.h"
//cell
#import "BriefEditPublishListVC.h"
//request
#import "RequestApi+MyBrief.h"
@interface BriefEditInventionListVC ()<UITableViewDelegate,UITableViewDataSource,RequestDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic, assign) BOOL isRemoveAll;//是否移除全部

@end

@implementation BriefEditInventionListVC
#pragma mark lazy
- (NSMutableArray *)listArr {
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
#pragma mark viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setBasiceView];
    self.isRemoveAll = true;

    self.view.backgroundColor = COLOR_BGCOLOR;
    [self request];
}

-(void)setNav
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.backgroundColor = [UIColor whiteColor];
    [navView addSubview:[BaseNavView initNavTitle:@"专利发明" leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:navView];
}
-(void)setBasiceView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight , KWIDTH, KHEIGHT - NAVIGATION_BarHeight ) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self createTableHeaderView];
}
-(void)createTableHeaderView
{
    EduORWorkTitleHeaderView *sectionView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(50))];
    [sectionView.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
    sectionView.titleL.text = @"发明专利";
    self.tableView.tableHeaderView = sectionView;
}
-(void)tapEdit:(UIButton *)sender
{
    DSWeak;
    BriefEditInventionVC *experience = [BriefEditInventionVC new];
    experience.blockDele = ^{
        [weakSelf request];
    };
    experience.editID = @"0";
    [self presentViewController:experience animated:YES completion:nil];
}


#pragma mark request
- (void)request{
    DSWeak;
    [RequestApi getkillsListWithKey:[GlobalData sharedInstance].GB_Key sclass:@"2" Delegate:self success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        if (weakSelf.isRemoveAll) {
            [weakSelf.listArr removeAllObjects];
        }
        for (NSDictionary *d in arr) {
            AllImageModel *model = [AllImageModel modelObjectWithDictionary:d];
            [weakSelf.listArr addObject:model];
        }
        [weakSelf.tableView reloadData];
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [BriefEditInventionListCell fetchHeight:_listArr[indexPath.section]];
    return h;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(BriefEditInventionListCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BriefEditInventionListCell *cell = [[BriefEditInventionListCell alloc]init];
    if (!cell)
    {
        cell = [[BriefEditInventionListCell alloc]init];
    }
    [cell.bgView setCorner:5];
    [cell resetCellWithModel:_listArr[indexPath.section]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DSWeak;
    AllImageModel *datas = self.listArr[indexPath.section];
    
    BriefEditInventionVC *eduInfo = [BriefEditInventionVC new];
    eduInfo.blockDele = ^{
        [weakSelf request];
    };
    eduInfo.editID  = [GlobalMethod doubleToString:datas.iDProperty];
    eduInfo.model = datas;
    [self presentViewController:eduInfo animated:YES completion:nil];
    
}


@end









@implementation BriefEditInventionListCell

#pragma mark 获取高度
FETCH_CELL_HEIGHT(BriefEditInventionListCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(AllImageModel *)model{
    NSString *start = [[NSString stringWithFormat:@"%@年",model.sSdate] substringToIndex:4];
    self.firstLabel.text = model.sTitle;
    self.midLabel.text = [NSString stringWithFormat:@"专利号：%@",model.sDesp];
    self.lastLabel.text = start;
    
    [self buildConstraint];
    
    return self.bgView.bottom;
    
}
-(void)buildConstraint
{
    self.bgView.widthHeight = XY(KWIDTH - W(30), W(100));
    self.bgView.leftTop = XY(W(15), 0);
    self.firstLabel.leftTop = XY(W(15),W(16));
    self.firstLabel.widthHeight = XY(self.bgView.width- W(30) , 20);
    [GlobalMethod resetLabel:self.firstLabel text:self.firstLabel.text isWidthLimit:1];
    
    self.midLabel.leftTop = XY(W(15), self.firstLabel.bottom + W(6));
    self.midLabel.widthHeight = XY(self.bgView.width- W(30) , 20);
    [GlobalMethod resetLabel:self.midLabel text:self.midLabel.text isWidthLimit:1];
    
    self.lastLabel.leftTop = XY(W(15), self.midLabel.bottom + W(7));
    self.lastLabel.widthHeight = XY(self.bgView.width- W(30) , 20);
    [GlobalMethod resetLabel:self.lastLabel text:self.lastLabel.text isWidthLimit:1];
    self.bgView.height = self.lastLabel.bottom+W(16);
    
    
}
@end
