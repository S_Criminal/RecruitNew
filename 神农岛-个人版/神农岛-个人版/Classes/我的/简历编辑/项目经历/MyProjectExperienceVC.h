//
//  MyProjectExperienceVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Datas3.h"
@interface MyProjectExperienceVC : UIViewController

@property (nonatomic ,strong) NSString *editID;

@property (nonatomic ,strong) Datas3 *datas3;

@end
