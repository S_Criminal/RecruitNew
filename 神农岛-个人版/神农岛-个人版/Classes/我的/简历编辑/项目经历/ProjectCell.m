//
//  ProjectCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "ProjectCell.h"

@implementation ProjectCell

{
    CGFloat titleFont;
    CGFloat contentFont;
    CGFloat top;
}

#pragma mark 懒加载
- (UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [UIImageView new];
        
        _iconImageView.backgroundColor = [UIColor clearColor];
        _iconImageView.image = [UIImage imageNamed:@"教育经历"];
        
    }
    return _iconImageView;
}
- (UILabel *)firstLabel{
    if (_firstLabel == nil) {
        _firstLabel = [UILabel new];
        _firstLabel.text = @"山东医科大学";
        _firstLabel.textColor = COLOR_LABELThreeCOLOR;
        _firstLabel.font = [UIFont systemFontOfSize:titleFont];
    }
    return _firstLabel;
}
- (UILabel *)midLabel{
    if (_midLabel == nil) {
        _midLabel = [UILabel new];
        _midLabel.text = @"销售管理·本科";
        _midLabel.textColor = COLOR_LABELSIXCOLOR;
        _midLabel.font = [UIFont systemFontOfSize:contentFont];
    }
    return _midLabel;
}
- (UILabel *)lastLabel{
    if (_lastLabel == nil) {
        _lastLabel = [UILabel new];
        _lastLabel.text = @"2010年 - 2014年";
        _lastLabel.textColor = COLOR_LABELSIXCOLOR;
        _lastLabel.font = [UIFont systemFontOfSize:contentFont];
    }
    return _lastLabel;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
        [self createViews];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    titleFont = F(15);
    contentFont = F(12);
    top = 6;
    if (KWIDTH == 320) {
        top  = 6;
    }else if (KWIDTH == 375){
        top = 5;
    }
}
-(void)createViews
{
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.iconImageView];
    [self.bgView addSubview:self.firstLabel];
    [self.bgView addSubview:self.midLabel];
    [self.bgView addSubview:self.lastLabel];
}

-(void)setModel:(Datas3 *)model
{
    
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(ProjectCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(Datas3 *)model{
    
    
    NSString *start = [[NSString stringWithFormat:@"%@",model.pStartD] substringToIndex:4];
    NSString *end   = [[NSString stringWithFormat:@"%@",model.pEndD] substringToIndex:4];
    self.firstLabel.text = model.pName;
    self.midLabel.text = [NSString stringWithFormat:@"%@",model.pDuty];
    self.lastLabel.text = [NSString stringWithFormat:@"%@ - %@",start,end];
    
    [self buildConstraint];
    
    return self.bgView.bottom;
}

-(void)buildConstraint
{
    [_iconImageView setFrame:CGRectMake(W(15), W(16), W(48), W(48))];
    
    CGFloat labelLeft   = CGRectGetMaxX(_iconImageView.frame) + 15;
    CGFloat bgViewWidth = KWIDTH - MyViewLeftConstraint * 2;
    
    _firstLabel.leftTop = XY(labelLeft, _iconImageView.y - top);
    _firstLabel.widthHeight = XY(bgViewWidth - labelLeft - 15 , 20);
    [GlobalMethod resetLabel:self.firstLabel text:self.firstLabel.text isWidthLimit:1];
    
    _midLabel.leftTop = XY(labelLeft, CGRectGetMaxY(_firstLabel.frame) + W(6));
    _midLabel.widthHeight = XY(bgViewWidth- labelLeft - 15 , 20);
    [GlobalMethod resetLabel:self.midLabel text:self.midLabel.text isWidthLimit:1];
    
    _lastLabel.leftTop = XY(labelLeft, CGRectGetMaxY(_midLabel.frame) + W(7));
    _lastLabel.widthHeight = XY(bgViewWidth - labelLeft - 15 , 20);
    [GlobalMethod resetLabel:self.lastLabel text:self.lastLabel.text isWidthLimit:1];
    
    [_bgView setFrame:CGRectMake(MyViewLeftConstraint, 0, bgViewWidth, _iconImageView.bottom +  W(16))];
}

@end
