//
//  MyProjectExperienceVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyProjectExperienceVC.h"

#import "MyExperienceGlobalView.h"
#import "GeneralPickerView.h"
//请求
#import "RequestApi+Login.h"
#import "RequestApi+MyBrief.h"

#import "GlobalDateModel.h"
#import "PickerEduModel.h"          //学历model
#import "DateArrModel.h"            //时间model

@interface MyProjectExperienceVC ()<ExperienceDelegate,UITextFieldDelegate,GeneralPickerViewDelegate,RequestDelegate,UITextViewDelegate>

@property (nonatomic ,strong) MyExperienceGlobalView *companyView;
@property (nonatomic ,strong) MyExperienceGlobalView *positionView;
@property (nonatomic ,strong) MyExperienceGlobalView *startView;
@property (nonatomic ,strong) MyExperienceGlobalView *endView;
@property (nonatomic ,strong) MyExperienceGlobalView *degreeView;

@property (nonatomic ,strong) UITextView *textView; //工作内容textView
@property (nonatomic ,strong) UITextField *workTextField;

@property (nonatomic ,strong) GeneralPickerView *pickerView;

@property (nonatomic ,strong) NSArray *degreeArray;
@property (nonatomic ,strong) NSArray *yearArr;
@property (nonatomic ,strong) NSArray *monthArr;

@property (nonatomic ,strong) UIButton *deleteButton;
@property (nonatomic, weak) NSTimer *hideDelayTimer;

@property (nonatomic ,strong) NSString *isQuits;    //是否是正在进行

@property (nonatomic ,strong)  BaseNavView *navView;

@property (nonatomic ,strong) UIView *bgView;

@end

@implementation MyProjectExperienceVC{
    CGFloat keyBoardChangeHeight;   //键盘收起的高度
    NSString *_project;
    NSString *_identity;
    NSString *_startTime;
    NSString *_endTime;
    NSString *selectButtonIndex;
    NSString *_content;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isQuits = @"0";
    [self setUI];
    [self setNav];
    [self setBasicView];
    [self setModel];
}

-(void)setUI
{
    keyBoardChangeHeight = W(150);
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"项目经历" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }];
    
    [self.view addSubview:self.bgView];
    [self.view addSubview:self.navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
    self.navView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor whiteColor];
}


-(void)saveInfo
{
    NSString *startTime  = [self setChangeTime:_startTime];
    NSString *endTime    = [self setChangeTime:_endTime];
    
    NSString *project   = self.companyView.textField.text;
    NSString *identity  = self.positionView.textField.text;
    NSString *content   = self.textView.text;
    
    if (kStringIsEmpty(project)) {
        [MBProgressHUD showError:@"项目名称不能为空" toView:self.view];
        return;
    }else{
        if (project.length < 4)
        {
            [MBProgressHUD showError:@"项目名称不能少于四位" toView:self.view];
            return;
        }
    }
    if (kStringIsEmpty(identity)) {
        [MBProgressHUD showError:@"身份不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(content)) {
        [MBProgressHUD showError:@"项目内容不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(startTime)) {
        [MBProgressHUD showError:@"开始时间不能为空" toView:self.view];
        return;
    }
    if (kStringIsEmpty(endTime)) {
        [MBProgressHUD showError:@"结束时间不能为空" toView:self.view];
        return;
    }
    
    DSWeak;
    
    self.navView.backBtn.userInteractionEnabled = NO;
    [RequestApi editProjectExpWithKey:[GlobalData sharedInstance].GB_Key expID:self.editID project:project identity:identity startd:startTime endd:endTime description:content online:self.isQuits Delegate:self success:^(NSDictionary *response) {
        
        self.navView.backBtn.userInteractionEnabled = YES;

        Datas3 *model = [[Datas3 alloc]init];
        model.pName = project;
        model.pDuty = identity;
        model.pStartD = startTime;
        model.pEndD = endTime;
        model.pDescription = content;
        model.isOnline = [weakSelf.isQuits doubleValue];
        model.iDProperty = [response[@"PEID"] doubleValue];
        
        if ([weakSelf.editID isEqualToString:[GlobalMethod doubleToString:model.iDProperty]])
        {
            [[GlobalData sharedInstance].GB_UserModel.datas3 removeObject:weakSelf.datas3];
        }
        
        [[GlobalData sharedInstance].GB_UserModel.datas3 addObject:model];
        
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        
        [MBProgressHUD showSuccess:@"保存项目经历成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *str) {
        [MBProgressHUD showError:str toView:weakSelf.view];
        self.navView.backBtn.userInteractionEnabled = YES;
    }];

}

-(NSString *)setChangeTime:(NSString *)time
{
    if (kStringIsEmpty(time)) {
        return time;
    }
    NSArray *array = [time componentsSeparatedByString:@"-"];
    
    NSString *one = array[0];
    NSString *two = array[1];
    
    one = [one stringByReplacingOccurrencesOfString:@"年" withString:@""];
    two = [two stringByReplacingOccurrencesOfString:@"月" withString:@""];
    time = [NSString stringWithFormat:@"%@-%@",one,two];
    if (!kStringIsEmpty(time) && time.length > 7) {
        time = [time substringToIndex:7];
    }
    return time;
}



-(void)setDatas3:(Datas3 *)datas3
{
    _datas3 = datas3;
}


-(void)setBasicView
{
    NSString *bgColor   = @"222222";
    NSString *textColor = @"666666";
    
    _project    = _datas3.pName;
    _identity   = _datas3.pDuty;
    _startTime  = _datas3.pStartD;
    _endTime    = _datas3.pEndD;
    _content    = _datas3.pDescription;
    
    self.isQuits = [GlobalMethod doubleToString:_datas3.isOnline];
    
    _startTime = [self setChangeTime:_startTime];
    _endTime   = [self setChangeTime:_endTime];
    
    _companyView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"项目名称" Title:_project status:NO tag:100 frame:CGRectMake(0, 3, KWIDTH, 48) bgColor:bgColor textColor:textColor delegate:self];
    
    _positionView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"参与身份" Title:_identity status:NO tag:101 frame:CGRectMake(0, CGRectGetMaxY(_companyView.frame), KWIDTH, 48) bgColor:bgColor textColor:textColor delegate:self];
    
    _startView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"开始时间" Title:_startTime status:YES tag:102 frame:CGRectMake(0,CGRectGetMaxY(_positionView.frame) + 25, KWIDTH / 2, 48) bgColor:bgColor textColor:textColor delegate:self];
    
    _endView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"结束时间" Title:_endTime status:YES tag:103 frame:CGRectMake(CGRectGetMaxX(_startView.frame) + 5,_startView.y, KWIDTH / 2, 48) bgColor:bgColor textColor:textColor delegate:self];
    
    UISwitch *switchView = [[UISwitch alloc]initWithFrame:CGRectMake(W(10), CGRectGetMaxY(_startView.frame) + 15, 30, 25)];
    switchView.on = [self.isQuits isEqualToString:@"0"] ? YES : NO;
    [switchView addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    
    UILabel *isGettingLabel = [[UILabel alloc]initWithFrame:CGRectMake(switchView.right + W(10), switchView.y, KWIDTH, 20)];
    [GlobalMethod setLabel:isGettingLabel widthLimit:KWIDTH numLines:0 fontNum:F(16) textColor:COLOR_LABELThreeCOLOR text:@""];
    [GlobalMethod resetLabel:isGettingLabel text:@"正在进行" isWidthLimit:0];
    isGettingLabel.centerY = switchView.centerY;
    
    _degreeView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"项目内容" Title:@"" status:YES tag:1000 frame:CGRectMake(0, CGRectGetMaxY(switchView.frame) + 5, KWIDTH, 48) bgColor:bgColor textColor:textColor delegate:self];
    _degreeView.label.textColor = COLOR_LABELThreeCOLOR;
    
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_degreeView.frame) + 10, KWIDTH - 15 * 2, 100)];
    _textView.backgroundColor = [HexStringColor colorWithHexString:@"f1f1f1"];
    _textView.textColor = COLOR_LABELThreeCOLOR;
    _textView.font = [UIFont systemFontOfSize:F(14)];
    _textView.delegate = self;
    _textView.text = _content;
    
    [self.bgView addSubview:_companyView];
    [self.bgView addSubview:_positionView];
    [self.bgView addSubview:_startView];
    [self.bgView addSubview:_endView];
    
    [self.bgView addSubview:switchView];
    [self.bgView addSubview:isGettingLabel];
    [self.bgView addSubview:_degreeView];
    [self.bgView addSubview:_textView];
    
    //新增加的隐藏删除工作经历按钮
    if (![self.editID isEqualToString:@"0"])
    {
        [self createDeleteButton];
    }
}

#pragma mark 是否正在进行
-(void)changeSwitch:(UISwitch *)switchs
{
    BOOL isButtonOn = [switchs isOn];
    self.isQuits = isButtonOn ? @"0" : @"1";
    NSLog(@"%@",self.isQuits);
}

//删除教育经历
-(void)createDeleteButton
{
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setFrame:CGRectMake(MyViewLeftConstraint, CGRectGetMaxY(_textView.frame) + 15, KWIDTH - MyViewLeftConstraint * 2, 45)];
    [self.deleteButton setCorner:5];
    self.deleteButton.backgroundColor = COLOR_MAINCOLOR;
    self.deleteButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
    [self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteButton setTitle:@"删除项目经历" forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(tapDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:self.deleteButton];
}

-(void)tapDelete:(UIButton *)sender
{
    DSWeak;
    NSLog(@"%@",self.editID);
    
    [RequestApi deleteProjectExpWithKey:[GlobalData sharedInstance].GB_Key expID:self.editID Delegate:self success:^(NSDictionary *response) {
        [[GlobalData sharedInstance].GB_UserModel.datas3 removeObject:self.datas3];
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
        [MBProgressHUD showSuccess:@"删除项目经历成功" toView:self.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    }];
    
}

-(void)handleHideTimer{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)setModel
{
    
    /** 请求日期年月数据 */
    DateArrModel *model = [[DateArrModel alloc]init];
    int minYear = (int) model.nowYear - 60 ;
    DSWeak;
    [model setYearArrayModelWithMinIndex:minYear maxIndex:0 yearBlock:^(NSMutableArray *yArr) {
        weakSelf.yearArr = yArr;
    } monthBlock:^(NSMutableArray *mArr) {
        weakSelf.monthArr = mArr;
    }];
    
}


#pragma mark 点击label的协议方法

-(void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(MyExperienceControl *)control
{
    if (tag == 100 || tag == 101 || tag == 1000) return;
    
    [self.view endEditing:YES];

    
    NSInteger timeInteger = [control.controlLabel.text isEqualToString:@"开始时间"]  ? 20 : 23;
    
    NSArray *textArr = [control.controlLabel.text componentsSeparatedByString:@"-"];
    textArr          = textArr.count > 1 ? textArr : [[GlobalMethod getBirth] componentsSeparatedByString:@"-"];    //有出生年月且未填写信息时，默认出生年 + 20
    NSString *firstTitle = @"1970";
    NSString *secondTitle;
    //默认选择
    if (textArr.count > 1) {
        firstTitle = control.controlLabel.text.length > 4 ? textArr.firstObject : [NSString stringWithFormat:@"%ld",[[textArr.firstObject substringToIndex:4] integerValue] + timeInteger]  ;    //默认出生年 + 20
        secondTitle= textArr[1];
    }else{
        firstTitle = textArr.firstObject;
    }
    
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    _pickerView.indexTag = tag;
    NSLog(@"%@",self.degreeArray);
    
    NSArray *firstArray ;
    NSArray *secondArray;
    NSString *title;
    
    title = tag == 102 ? @"开始时间" : @"结束时间";
    
    firstArray  = self.yearArr;
    secondArray = self.monthArr;
    
    [_pickerView setUpPickerViewTitle:title selectFirstTitle:firstTitle selectSecondTitle:secondTitle selectModel:2 firstArray:firstArray secondArray:secondArray];
    _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    [self.view addSubview:_pickerView];
    [self showPickerView];
}

#pragma mark pickerView的协议
-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    NSString *endContent    = _endView.label.text;
    NSString *startContent  = _startView.label.text;
    startContent = [startContent stringByReplacingOccurrencesOfString:@"年" withString:@""];
    startContent = [startContent stringByReplacingOccurrencesOfString:@"月" withString:@""];
    endContent = [endContent stringByReplacingOccurrencesOfString:@"年" withString:@""];
    endContent = [endContent stringByReplacingOccurrencesOfString:@"月" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"年" withString:@""];
    content = [content stringByReplacingOccurrencesOfString:@"月" withString:@""];
    
    if (tag == 102) {
        if (![endContent isEqualToString:@"结束时间"]) {
            if (([[endContent substringToIndex:4] integerValue] >= [[content substringToIndex:4]integerValue])) {
                
                if ([[endContent substringToIndex:4] integerValue] == [[content substringToIndex:4] integerValue]) {
                    if ([[endContent substringWithRange:NSMakeRange(5, 2)] integerValue] < [[content substringWithRange:NSMakeRange(5, 2)] integerValue]) {
                        [MBProgressHUD showError:@"开始时间不能大于结束时间" toView:self.view];
                    }else{
                        _startView.label = [self setLabel:_startView.label Content:content];
                        _startTime = content;
                    }
                }else{
                    _startView.label = [self setLabel:_startView.label Content:content];
                    _startTime = content;
                }
                
            }else{
                [MBProgressHUD showError:@"开始时间不能大于结束时间" toView:self.view];
            }
        }else{
            _startTime = content;
            _startView.label = [self setLabel:_startView.label Content:content];
        }
    }else if (tag == 103){
        if (!([[startContent substringToIndex:4] integerValue] >= [[content substringToIndex:4]integerValue])) {
            _endView.label = [self setLabel:_endView.label Content:content];
            _endTime = content;
        }else{
            if ([[startContent substringToIndex:4] integerValue] == [[content substringToIndex:4] integerValue]) {
                if ([[startContent substringWithRange:NSMakeRange(5, 2)] integerValue] > [[content substringWithRange:NSMakeRange(5, 2)] integerValue]) {
                    [MBProgressHUD showError:@"开始时间不能大于结束时间" toView:self.view];
                }else{
                    _endView.label = [self setLabel:_endView.label Content:content];
                    _endTime = content;
                }
            }else{
                if ([[startContent substringToIndex:4] integerValue] > [[content substringToIndex:4] integerValue]) {
                    [MBProgressHUD showError:@"开始时间不能大于结束时间" toView:self.view];
                }else{
                    _endView.label = [self setLabel:_endView.label Content:content];
                    _endTime = content;
                }
            }
            
        }
    }
    
    [self hidePickerView];
}

-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(UILabel *)setLabel:(UILabel *)label Content:(NSString *)content
{
    if (!kStringIsEmpty(content)) {
        label.text = content;
        label.textColor = COLOR_LABELThreeCOLOR;
    }
    return label;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == self.textView) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShows:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHides:) name:UIKeyboardWillHideNotification object:nil];
    }
    return YES;
}


#pragma mark --- 弹出键盘 ---
- (void)keyboarShows:(NSNotification *)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillShowNotification object:nil];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.bgView.frame;
        if (frame.origin.y == NAVIGATION_BarHeight + 1) {
            frame.origin.y = weakSelf.bgView.frame.origin.y - keyBoardChangeHeight;
            weakSelf.bgView.frame = frame;
        }
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark --- 收起键盘 ---
- (void)keyboarHides:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:UIKeyboardWillHideNotification object:nil];
    
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = weakSelf.bgView.frame;
        if (frame.origin.y < NAVIGATION_BarHeight + 1) {
            frame.origin.y = weakSelf.bgView.frame.origin.y + keyBoardChangeHeight;
            weakSelf.bgView.frame = frame;
        }
    } completion:^(BOOL finished) {
        
    }];
}



@end
