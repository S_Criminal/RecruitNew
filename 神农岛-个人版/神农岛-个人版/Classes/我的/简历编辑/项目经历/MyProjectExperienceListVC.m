//
//  MyProjectExperienceListVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyProjectExperienceListVC.h"

#import "ProjectCell.h"
#import "EduORWorkTitleHeaderView.h"
#import "MyProjectExperienceVC.h"

#import "Datas3.h"

@interface MyProjectExperienceListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic ,strong) void(^block1)(NSString *str);

@end




@implementation MyProjectExperienceListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setBasiceView];
    
    self.view.backgroundColor = COLOR_BGCOLOR;
}

-(void)setNav
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, NAVIGATION_BarHeight)];
    navView.backgroundColor = [UIColor whiteColor];
    [navView addSubview:[BaseNavView initNavTitle:@"个人经历" leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:navView];
}

-(void)setBasiceView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight , KWIDTH, KHEIGHT - NAVIGATION_BarHeight ) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self createTableHeaderView];
}

-(void)createTableHeaderView
{
    EduORWorkTitleHeaderView *sectionView = [[EduORWorkTitleHeaderView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(50))];
    [sectionView.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
    sectionView.titleL.text = @"项目经历";
    self.tableView.tableHeaderView = sectionView;
}

-(void)tapEdit:(UIButton *)sender
{
    MyProjectExperienceVC *experience = [MyProjectExperienceVC new];
    experience.editID = @"0";
    [self presentViewController:experience animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModel];
}

-(void)setModel
{
    
    _listArr = [GlobalData sharedInstance].GB_UserModel.datas3;
    
    [self.tableView reloadData];
    
    NSLog(@"%@",_listArr);
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [ProjectCell fetchHeight:_listArr[indexPath.section]];
    return h;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

-(ProjectCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProjectCell *cell = [[ProjectCell alloc]init];
    if (!cell)
    {
        cell = [[ProjectCell alloc]init];
    }
    [cell.bgView setCorner:5];
    [cell resetCellWithModel:_listArr[indexPath.section]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Datas3 *datas = [[GlobalData sharedInstance].GB_UserModel.datas3 objectAtIndex:indexPath.section];
    NSLog(@"%@",[GlobalData sharedInstance].GB_UserModel.datas3);
    NSLog(@"%@",datas);
    
    MyProjectExperienceVC *eduInfo = [MyProjectExperienceVC new];
    eduInfo.editID  = [GlobalMethod doubleToString:datas.iDProperty];
    eduInfo.datas3 = datas;
    [self presentViewController:eduInfo animated:YES completion:nil];
    
    //    [GB_Nav pushViewController:workVC animated:YES];
}



@end
