//
//  ProjectCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/8.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Datas3.h"

@interface ProjectCell : UITableViewCell
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *firstLabel;
@property (nonatomic ,strong) UILabel *midLabel;
@property (nonatomic ,strong) UILabel *lastLabel;

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(Datas3 *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(Datas3 *)model;
@end
