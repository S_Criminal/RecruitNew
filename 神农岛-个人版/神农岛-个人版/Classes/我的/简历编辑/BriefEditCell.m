//
//  BriefEditCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefEditCell.h"

@implementation BriefEditCell{
    CGFloat titleFont;
    CGFloat contentFont;
}

#pragma mark 懒加载

-(UIImageView *)identificationImage
{
    if (!_identificationImage) {
        _identificationImage = [UIImageView new];
        _identificationImage.backgroundColor = [UIColor clearColor];
        _identificationImage.image = [UIImage imageNamed:@"identificationImage"];
        _identificationImage.hidden = YES;
    }
    return _identificationImage;
}

- (UIImageView *)iconImageView{
    if (_iconImageView == nil)
    {
        _iconImageView = [UIImageView new];
        _iconImageView.backgroundColor = [UIColor clearColor];
    }
    return _iconImageView;
}

- (UILabel *)titleL{
    if (_titleL == nil) {
        _titleL = [UILabel new];
        _titleL.font = [UIFont systemFontOfSize:F(16)];
        _titleL.textColor = [UIColor whiteColor];
    }
    return _titleL;
}

- (UILabel *)contentL{
    if (_contentL == nil) {
        _contentL = [UILabel new];
        _contentL.font = [UIFont systemFontOfSize:F(12)];
        _contentL.textColor = [UIColor whiteColor];
    }
    return _contentL;
}

- (UIButton *)editButton
{
    if (_editButton == nil)
    {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setImage:[UIImage imageNamed:@"white_add"] forState:UIControlStateNormal];
        _editButton.backgroundColor = [UIColor clearColor];
        _editButton.userInteractionEnabled = NO;
    }
    return _editButton;
}

#pragma mark 初始化

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.contentL];
        [self.contentView addSubview:self.editButton];
        [self.contentView addSubview:self.identificationImage];
        
    }
    return self;
}

-(void)buildConstraint
{
    [self.iconImageView setFrame:CGRectMake(15 + W(15), W(28), 20, 20)];
    CGFloat left = CGRectGetMaxX(_iconImageView.frame) + W(15) ;
    [self.titleL setFrame:CGRectMake(left, W(20), W(150), F(16))];
    [self.titleL sizeToFit];
    [self.editButton setFrame:CGRectMake(KWIDTH - W(20) - W(30), 0, W(25), W(25))];
    self.editButton.centerY = self.iconImageView.centerY;
    [self.contentL setFrame:CGRectMake(left, CGRectGetMaxY(_titleL.frame) + W(10), KWIDTH - left - self.editButton.width, F(12))];
    
    [self.identificationImage setFrame:CGRectMake(self.titleL.right + W(5), 0, W(15), W(15))];
    self.identificationImage.centerY = self.titleL.centerY;
    
}


-(void)setIndexRowP:(NSInteger)indexRowP
{
    _indexRowP = indexRowP;
}

-(void)setIndexSectionP:(NSInteger)indexSectionP
{
    _indexSectionP = indexSectionP;
}

-(void)setModel:(MyBriefInfoModel *)model
{
 
    _model = model;
    NSArray *array = [NSArray arrayWithObjects:model.myInfoDatas,model.myExperienceDatas,model.myPositionTrendDataas,model.myAchieveDatas,model.myOtherInfoDatas, nil];
    NSString *iconImage = array[_indexSectionP][0][_indexRowP];
    NSString * title    = array[_indexSectionP][1][_indexRowP];
    NSString *content   = array[_indexSectionP][2][_indexRowP];
    self.iconImageView.image = [UIImage imageNamed:iconImage];
    self.titleL.text    = title;
    self.contentL.text  = content;
    [self buildConstraint];
    [self setupAutoHeightWithBottomView:_contentL bottomMargin:W(20)];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
