//
//  BriefEditSectionView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefEditSectionView.h"

@implementation BriefEditSectionView{
    CGFloat titleFont;
}


#pragma mark 懒加载

- (UILabel *)titleL{
    if (_titleL == nil) {
        _titleL = [UILabel new];
        _titleL.backgroundColor = [UIColor clearColor];
        _titleL.textColor = COLOR_LABELThreeCOLOR;
        _titleL.font = [UIFont systemFontOfSize:F(17)];
    }
    return _titleL;
}

- (UIButton *)editButton{
    if (_editButton == nil) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _editButton.backgroundColor = [UIColor clearColor];
        _editButton.tag = 100;
        [_editButton setImage:[UIImage imageNamed:@"white_down"] forState:UIControlStateNormal];
        [_editButton setImage:[UIImage imageNamed:@"white_top"] forState:UIControlStateSelected];
    }
    return _editButton;
}
#pragma mark 初始化

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleL];
        [self addSubview:self.editButton];
        [self buildConstraint];
    }
    return self;
}


-(void)buildConstraint
{
    [self.titleL setFrame:CGRectMake(25, 15, 100, 20)];
    [self.editButton setFrame:CGRectMake(self.width - self.height, 13, self.height, self.height)];
    _titleL.centerY = self.centerY;
    _editButton.centerY = _titleL.centerY;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
