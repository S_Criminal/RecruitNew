//
//  BriefEditLanaguageVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/4/1.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefEditLanaguageVC.h"
#import "MyExperienceGlobalView.h"
#import "GeneralPickerView.h"       //选择器

@interface BriefEditLanaguageVC ()<UIPickerViewDelegate,GeneralPickerViewDelegate>
@property (nonatomic ,strong) BaseNavView *navView;
@property (nonatomic ,strong) MyExperienceGlobalView *speciesView;    //外语语种
@property (nonatomic ,strong) MyExperienceGlobalView *skilledView;    //熟练程度
@property (nonatomic ,strong) GeneralPickerView *pickerView;
@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) NSArray *lanaguageArray;
@property (nonatomic ,strong) NSArray *skilledArray;

@end

@implementation BriefEditLanaguageVC

#pragma mark lazy
-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.frame = CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT);
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

-(NSArray *)lanaguageArray
{
    if (!_lanaguageArray) {
        _lanaguageArray = @[@"英语",@""];
    }
    return _lanaguageArray;
}

-(NSArray *)skilledArray
{
    if (!_skilledArray) {
        _skilledArray = @[@"掌握",@"熟练",@"精通"];
    }
    return _skilledArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setModel];
    [self setUpViews];
    
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"语言能力" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }];
    self.navView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
    [self.view addSubview:self.bgView];
}

-(void)saveInfo
{
    
}

//请求model
-(void)setModel
{
    
}

//布局
-(void)setUpViews
{
    NSString *bgColor   = @"222222";
    NSString *textColor = @"666666";
    _speciesView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"外语语种" Title:@"" status:false tag:100 frame:CGRectMake( 0, +3, KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    _skilledView = [MyExperienceGlobalView initWithPlaceHolderTitle:@"熟练程度" Title:@"" status:true tag:101 frame:CGRectMake( 0, _speciesView.bottom  , KWIDTH, W(48)) bgColor:bgColor textColor:textColor delegate:self];
    [self.bgView addSubview:_speciesView];
    [self.bgView addSubview:_skilledView];
}


#pragma mark 点击label的协议方法

-(void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(MyExperienceControl *)control
{
    
    [self.view endEditing:YES];
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    _pickerView.indexTag = tag;
    
    NSString *title ;
    NSString *firstTitle = control.controlLabel.text;
    NSArray  *firstArrry;
    if (tag == 100) {
        title = @"请选择外语语种";
        firstArrry = self.lanaguageArray;
        firstTitle = self.lanaguageArray.firstObject;
    }else if (tag == 101){
        title = @"请选择熟练程度";
        firstArrry = self.skilledArray;
        firstTitle = self.skilledArray.firstObject;
    }

    [_pickerView setUpPickerViewTitle:title selectFirstTitle:firstTitle selectSecondTitle:@"" selectModel:1 firstArray:firstArrry secondArray:nil];
    _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    [self.view addSubview:_pickerView];
    [self showPickerView];
    
}

#pragma mark pickerView的协议
-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    NSLog(@"===content%@===",content);
    if (tag == 100) {
        _speciesView.label.text = content;
        _speciesView.label.textColor = COLOR_LABELSIXCOLOR;
    }else if (tag == 101){
        _skilledView.label.text = content;
        _skilledView.label.textColor = COLOR_LABELSIXCOLOR;
    }
    [self hidePickerView];
}


-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
