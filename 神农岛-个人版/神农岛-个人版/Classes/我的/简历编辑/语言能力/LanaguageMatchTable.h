//
//  LanaguageMatchTable.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/4/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanaguageMatchTable : UITableView

@property (nonatomic ,strong) NSArray *listArr;

@end
