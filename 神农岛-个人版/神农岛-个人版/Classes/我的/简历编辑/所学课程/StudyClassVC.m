//
//  StudyClassVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "StudyClassVC.h"
#import "RequestApi+MyBrief.h"

@interface StudyClassVC ()<UITextViewDelegate,RequestDelegate>
@property (nonatomic ,strong) UITextView *textView;
@property (nonatomic ,strong) UILabel *placeHolderLabel;
@property (nonatomic, weak) NSTimer *hideDelayTimer;
@property (nonatomic ,strong) NSString *editID;
//@property (nonatomic ,strong) BriefInfoModel *model;

@end

@implementation StudyClassVC

-(UITextView *)textView
{
    if (!_textView) {
        _textView = [UITextView new];
        
    }
    return _textView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.editID = @"0";
    [self setUI];
    [self setNav];
    [self createBasicView];
    [self setModel];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textView becomeFirstResponder];
}

-(void)setModel
{
    DSWeak;
    [RequestApi getkillsListWithKey:[GlobalData sharedInstance].GB_Key sclass:@"5" Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        if (!kArrayIsEmpty(arr)) {
            NSDictionary *d = arr.firstObject;
            weakSelf.textView.text = d[@"S_Contents"];
            weakSelf.editID = d[@"ID"];
            self.placeHolderLabel.hidden = YES;
        }
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

-(void)setUI
{
    
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"所学课程" leftImageName:@"" leftBlock:^{
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf saveInfo];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

#pragma mark 保存自我介绍

-(void)saveInfo
{
    if (self.textView.text.length < 10)
    {
        [MBProgressHUD showError:@"所学课程不得少于10个字" toView:self.view];
        return;
    }
    DSWeak;
    [RequestApi editPersonAchieveWithKey:[GlobalData sharedInstance].GB_Key editID:self.editID contents:self.textView.text spath:@"" sdesp:@"" sdate:@"" stitle:@"" sclass:@"5" Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showError:@"保存成功" toView:weakSelf.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        [MBProgressHUD showError:errorStr toView:weakSelf.view];
    }];
}


-(void)handleHideTimer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)createBasicView
{
    
    [self.textView setFrame:CGRectMake(W(10), NAVIGATION_BarHeight + 1 + 10, KWIDTH - 20, KHEIGHT - 282 - 20 - NAVIGATION_BarHeight - 1)];
    _textView.font = [UIFont systemFontOfSize:F(16)];
    //    _textView.contentInset = UIEdgeInsetsMake(50, 100, 0, 0);
    _textView.delegate = self;
    _textView.scrollEnabled = YES;
    
    _placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(W(8), W(7), KWIDTH - 20, 20)];
    
    if (kStringIsEmpty(_textView.text)) {
        _textView.text = @"";
        _placeHolderLabel.hidden = NO;
    }else{
        _placeHolderLabel.hidden = YES;
    }
    
    _placeHolderLabel.text = @"详细描述一下你的所学课程，字数控制在10~100字";
    _placeHolderLabel.font = [UIFont systemFontOfSize:F(15)];
    _placeHolderLabel.textColor = [HexStringColor colorWithHexString:@"999999"];
    [_textView addSubview:_placeHolderLabel];
    [self.view addSubview:_textView];
}

//监听事件
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    //获取键盘高度
    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    self.textView.contentInset=UIEdgeInsetsMake(0, 0,keyboardRect.size.height, 0);
}

- (void)handleKeyboardDidHidden
{
    self.textView.contentInset=UIEdgeInsetsZero;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{     //其实你可以加在这个代理方法中。当你将要编辑的时候。先执行这个代理方法的时候就可以改变间距了。这样之后输入的内容也就有了行间距。
    if (textView.text.length < 1) {
        textView.text = @"间距";
        _placeHolderLabel.hidden = NO;
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:F(7)];
    
    NSDictionary *attributes = @{
                                 
                                 NSFontAttributeName:[UIFont systemFontOfSize:F(15)],
                                 
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 
                                 };
    
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];
    if ([textView.text isEqualToString:@"间距"]) {           //之所以加这个判断是因为再次编辑的时候还会进入这个代理方法，如果不加，会把你之前输入的内容清空。你也可以取消看看效果。
        textView.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:attributes];//主要是把“间距”两个字给去了。
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (text.length > 0) {
        self.placeHolderLabel.hidden = YES;
    }
    if (textView.text.length == 1 && text.length == 0) {
        self.placeHolderLabel.hidden = NO;
    }
    return YES;
}

-(void)dealloc{
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
