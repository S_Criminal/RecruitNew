//
//  BriefEditCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyBriefInfoModel.h"


@interface BriefEditCell : UITableViewCell

@property (nonatomic ,strong) UIImageView *identificationImage;

@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *titleL;
@property (nonatomic ,strong) UILabel *contentL;
@property (nonatomic ,strong) UIButton *editButton;
@property (nonatomic ,strong) MyBriefInfoModel *model;

@property (nonatomic ,assign) NSInteger indexSectionP;
@property (nonatomic ,assign) NSInteger indexRowP;

@end
