//
//  BriefEditViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefEditViewController.h"
#import "MyBriefInfoModel.h"
#import "BriefInfoModel.h"          //datas0
#import "PrendInfoModel.h"          //职位偏好model
#import "BriefEditCell.h"

//#import "EduORWorkTitleHeaderView.h"
#import "BriefEditSectionView.h"


#import "MyAddBriefBasicInfoVC.h"   //我的基本信息

//section 0
#import "BriefEditViewController.h" //简历编辑界面
#import "MySelfInfoVC.h"            //自我描述
#import "MyCardVC.h"                //个人认证
//section 1
#import "MyEduExperienceListVC.h"   //教育经历列表
#import "MyWorkExperienceListVC.h"  //工作经历列表

#import "MyEduExperienceVC.h"
#import "MyWorkExperienceVC.h"
//section 2
#import "ExpectCityCollectionVC.h"      //期望城市
#import "ExpectPositionCollectionVC.h"  //期望职位
//section 3
#import "BriefEditPublishVC.h"          //出版作品
#import "BriefEditPublishListVC.h"
#import "BriefEditInventionVC.h"        //专利发明
#import "BriefEditInventionListVC.h"
#import "BriefEditLanaguageVC.h"        //语言能力
#import "MyCertViewController.h"        //资质证书
#import "MyProjectExperienceListVC.h"   //所做项目
#import "MyProjectExperienceVC.h"
#import "StudyClassVC.h"                //所学课程
//section 4
#import "MyLifeViewController.h"        //生活照片
//#import ""
/** pickerView */
#import "GeneralPickerView.h"
#import "KYAlertView.h"
/** requset */
#import "RequestApi+MyBrief.h"
#import "RequestApi+Login.h"

@interface BriefEditViewController ()<UITableViewDelegate,UITableViewDataSource,RequestDelegate,GeneralPickerViewDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *sectionArr;
@property (nonatomic ,strong) MyBriefInfoModel *model;
@property (nonatomic ,strong) BriefInfoModel *infoModel;
@property (nonatomic ,strong) NSArray *listArr;
@property (nonatomic ,strong) GeneralPickerView *pickerView;
@property (nonatomic ,strong) KYAlertView *kyAlertView;

@property (nonatomic ,strong) NSMutableArray *infoIdentificateArray;

@property (nonatomic ,strong) NSDictionary *infoIdentificateDic;


@property (nonatomic ,strong) NSMutableArray *payRootArr;
@property (nonatomic ,strong) NSMutableArray *payArr;
@property (nonatomic ,strong) NSMutableArray *payIDArr;

@property (nonatomic ,strong) NSMutableArray *positionArr;
@property (nonatomic ,strong) NSMutableArray  *prendArray;

//@property (nonatomic ,strong) NSMutableArray *sectionButtonArr;

@property (nonatomic ,strong) UIButton *sectionEditButton;

@property (nonatomic ,strong) NSString *pay;
@property (nonatomic ,strong) NSString *nature;

@end

@implementation BriefEditViewController{
    NSInteger putSectionIndex;
    CGFloat sectionHeaderHeight;
    CGFloat section0HeaderHeight;
}

#pragma mark 懒加载

-(KYAlertView *)kyAlertView
{
    if (!_kyAlertView) {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    return _kyAlertView;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 10) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}

-(NSArray *)sectionArr
{
    if (!_sectionArr ) {
        _sectionArr = [NSArray arrayWithObjects:@"个人简介",@"个人经历",@"职位偏好",@"个人成就",@"其他信息", nil];
    }
    return _sectionArr;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    putSectionIndex = 0;                //刚进入页面时，默认展示第一行
    [self setNav];
    [self setUI];
    [self createTableView];
    self.view.backgroundColor = COLOR_MAINCOLOR;
   
    [self request];
    
}

#pragma mark request
- (void)request{
    [RequestApi getkillsListWithKey:[GlobalData sharedInstance].GB_Key sclass:@"-1" Delegate:self success:^(NSDictionary *response) {
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModel];
}

-(void)setNav
{
    DSWeak;
    BaseNavView *navView = [BaseNavView initNavTitle:@"简历编辑" leftImageName:@"white_delete" leftBlock:^{
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    navView.labelTitle.textColor = [UIColor whiteColor];
    [navView.labelTitle sizeToFit];
    [self.view addSubview:navView];
}

-(void)setUI
{
    sectionHeaderHeight = W(46);
    section0HeaderHeight = W(46) + W(47);
}

-(void)createTableView
{
    [self.view addSubview:self.tableView];
}

-(void)setModel
{
    _model = [[MyBriefInfoModel alloc]init];
    
    _listArr = [NSArray arrayWithObjects:_model.myInfoDatas,_model.myExperienceDatas,_model.myPositionTrendDataas,_model.myAchieveDatas,_model.myOtherInfoDatas, nil];
    _infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    
    _nature = _infoModel.rNature;
    
    [GlobalData sharedInstance].city = _infoModel.rCity;
    [GlobalData sharedInstance].provience = _infoModel.rProvince;
    
    DSWeak;
    self.infoIdentificateArray = [NSMutableArray array];
    [RequestApi getBriefFillWithKey:[GlobalData sharedInstance].GB_Key Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSArray *arr = response[@"datas"];
        self.infoIdentificateDic = [arr objectAtIndex:0];
        NSDictionary *d = [arr objectAtIndex:0];
        [weakSelf.infoIdentificateArray addObject:d];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorStr, id mark) {
        
    }];
#pragma mark 获取职位偏好
    _prendArray     = [NSMutableArray array];
    _positionArr    = [NSMutableArray array];
    [RequestApi getBriefPrtendInfoWithKey:[GlobalData sharedInstance].GB_Key Delegate:self success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        
        NSArray *arr = response[@"datas"];
        
        NSMutableArray *typeArray = [NSMutableArray array];
        
        for (NSDictionary *d in arr)
        {
            PrendInfoModel *model = [PrendInfoModel modelObjectWithDictionary:d];
            [weakSelf.prendArray addObject:model];
        }
        
        for (PrendInfoModel *model in weakSelf.prendArray) {
            NSMutableDictionary *p_Dic = [NSMutableDictionary dictionary];
            [p_Dic setValue:model.pName forKey:@"pName"];
            [p_Dic setValue:[GlobalMethod doubleToString:model.rType] forKey:@"rType"];
            [p_Dic setValue:[GlobalMethod doubleToString:model.rTypes] forKey:@"rTypes"];
            NSString *str = [GlobalMethod exchangeToJson:p_Dic];
            if (model.rType != model.rTypes)
            {
                [self.positionArr addObject:str];
                [typeArray addObject:p_Dic];
            }
        }
        
        PrendInfoModel *model = kArrayIsEmpty(arr) ? nil : weakSelf.prendArray.firstObject;
        //薪资
        _pay = model.rPay;
        //期望职位
        NSArray *expectArr = self.positionArr;
        [GlobalData sharedInstance].position = expectArr;

        //省市数组
        
        [GlobalData sharedInstance].city = model.rCity;
        [GlobalData sharedInstance].provience = model.rProvince;
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
    
    
    //薪资数组
    _pay        = _infoModel.rPay;
    _payArr     = [NSMutableArray array];
    _payIDArr   = [NSMutableArray array];
    _payRootArr = [NSMutableArray array];
    if (kArrayIsEmpty(_payRootArr))
    {
        self.payRootArr = [GlobalMethod getArrayToPlist:nil filePath:@"payModel"];
        for (NSDictionary *d in _payRootArr)
        {
            [_payArr addObject:d[@"Sname"]];
            [_payIDArr addObject:d[@"SID"]];
        }
    }
    
    [RequestApi getSalarysListDelegate:self Success:^(NSDictionary *response) {
        
        NSArray *arr = response[@"datas"];
        if (arr.count > 0) {
            _payRootArr = response[@"datas"];
            _payArr = [NSMutableArray array];
            _payIDArr = [NSMutableArray array];
        }
        for (NSDictionary *d in _payRootArr)
        {
            [_payArr addObject:d[@"Sname"]];
            [_payIDArr addObject:d[@"SID"]];
        }
        [GlobalMethod whiteArrayToPlist:[NSMutableArray arrayWithArray:_payRootArr] filePath:@"payModel"];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = _listArr[section][0];
    if (section == putSectionIndex)
    {
        return arr.count;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITapGestureRecognizer *tapSection = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSection:)];
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH,sectionHeaderHeight)];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.tag = section;
    [bgView addGestureRecognizer:tapSection];
    
    BriefEditSectionView *view = [[BriefEditSectionView alloc]initWithFrame:CGRectMake(15, 0, KWIDTH - 15 * 2, bgView.height)];
    [view setCorner:5];
    if (putSectionIndex == section) {
        view.editButton.selected = YES;
    }
    view.titleL.text = self.sectionArr[section];
    view.editButton.userInteractionEnabled = NO;
    [bgView addSubview:view];
    CGFloat imageWidth = W(80);
    
    if (section == 0) {
        if (!_sectionEditButton) {
            _sectionEditButton = view.editButton;
        }
        
        
        bgView.height = section0HeaderHeight;
        view.y = bgView.height - sectionHeaderHeight;
        view.height =  sectionHeaderHeight;
        
        UIImageView *headImageBGView = [UIImageView new];
        headImageBGView.image = [UIImage imageNamed:@"HeadImageBGView"];
        [headImageBGView setFrame:CGRectMake(10, - 5, imageWidth, imageWidth)];
        headImageBGView.centerX = bgView.centerX;
        headImageBGView.userInteractionEnabled = YES;
        
        UIImageView *imageView = [UIImageView new];
        [imageView sd_setImageWithURL:[NSURL URLWithString:_infoModel.uHead] placeholderImage:[UIImage imageNamed:@"个人默认头像"]];
        [imageView setCorner:imageWidth / 2 - 8];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [bgView addSubview:headImageBGView];
        [headImageBGView addSubview:imageView];
        
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(8);
            make.right.bottom.mas_equalTo(-8);
        }];
    }
    return bgView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return W(46) + W(47);
    }
    return W(46);
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 5)];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h = [self.tableView cellHeightForIndexPath:indexPath model:_model keyPath:@"model" cellClass:[BriefEditCell class] contentViewWidth:KWIDTH];
    return h;
}

-(BriefEditCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BriefEditCell *cell = [[BriefEditCell alloc]init];
    if (!cell)
    {
        cell = [[BriefEditCell alloc]init];
    }
    cell.indexSectionP = indexPath.section;
    cell.indexRowP     = indexPath.row;
    cell.model         = _model;
    
    //字典为空 返回
    if (kDictIsEmpty(self.infoIdentificateDic)) {
        return cell;
    }
    if (indexPath.section == 0) {
        if (indexPath.row == 0 && ![self.infoIdentificateDic[@"BaseM"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 1 && ![self.infoIdentificateDic[@"Intros"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 2 && ![self.infoIdentificateDic[@"Auths"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0 && ![self.infoIdentificateDic[@"Edts"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 1 && ![self.infoIdentificateDic[@"Woks"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }
    }else if (indexPath.section == 2) {
        if (indexPath.row == 0 && ![self.infoIdentificateDic[@"Citys"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 1 && ![self.infoIdentificateDic[@"Pots"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 2 && ![self.infoIdentificateDic[@"Pays"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }
    }else if (indexPath.section == 3) {
        if (indexPath.row == 0 && ![self.infoIdentificateDic[@"Cbzp"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 1 && ![self.infoIdentificateDic[@"Skcs"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 2 && ![self.infoIdentificateDic[@"Zlfm"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 3 && ![self.infoIdentificateDic[@"Sxkc"] isEqualToNumber:@0]) {
           cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 4 && ![self.infoIdentificateDic[@"Pros"] isEqualToNumber:@0]) {
           cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 5 && ![self.infoIdentificateDic[@"Ryjx"] isEqualToNumber:@0]){
            cell.identificationImage.hidden = NO;
        }else if (indexPath.row == 6 && ![self.infoIdentificateDic[@"Yynl"] isEqualToNumber:@0]){
            cell.identificationImage.hidden = NO;
        }
    }else if (indexPath.section == 4) {
        if (indexPath.row == 0 && ![self.infoIdentificateDic[@"Lifes"] isEqualToNumber:@0]) {
            cell.identificationImage.hidden = NO;
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!(indexPath.section == 0 && indexPath.row == 0) && kStringIsEmpty(self.infoModel.uName)) {
        [self showKYAlertView];
    }else  if (indexPath.section == 0){
        if (indexPath.row == 0) {
            MyAddBriefBasicInfoVC * vc = [[MyAddBriefBasicInfoVC alloc]init];
            [self presentViewController:vc animated:YES completion:nil];
        }else if (indexPath.row == 1){
            MySelfInfoVC    *vc   = [[MySelfInfoVC alloc]init];
            [self presentViewController:vc animated:YES completion:nil];
        }else if (indexPath.row == 2){
            MyCardVC *card   = [[MyCardVC alloc]init];
            [self.navigationController pushViewController:card animated:YES];
        }
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            if ([GlobalData sharedInstance].GB_UserModel.datas2.count > 0){
                MyEduExperienceListVC *edu = [[MyEduExperienceListVC alloc]init];
                [self.navigationController pushViewController:edu animated:YES];
            }else{
                MyEduExperienceVC *edu = [MyEduExperienceVC new];
                edu.editID = @"0";
                edu.edit = @"add";
                [self presentViewController:edu animated:YES completion:nil];
            }
        }else if (indexPath.row == 1){
            if ([GlobalData sharedInstance].GB_UserModel.datas1.count > 0)
            {
                MyWorkExperienceListVC *exp = [[MyWorkExperienceListVC alloc]init];
                [self.navigationController pushViewController:exp animated:YES];
            }else{
                MyWorkExperienceVC *exp = [[MyWorkExperienceVC alloc]init];
                exp.editID = @"0";
                exp.edit = @"add";
                [self presentViewController:exp animated:YES completion:nil];
            }
        }
    }else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            ExpectCityCollectionVC *city = [ExpectCityCollectionVC new];
            [self presentViewController:city animated:YES completion:nil];
        }else if (indexPath.row == 1){
            ExpectPositionCollectionVC *position = [ExpectPositionCollectionVC new];
            [self presentViewController:position animated:YES completion:nil];
        }else if (indexPath.row == 2){  //薪资要求
            [self tapPay];
        }
    }else if (indexPath.section == 3){  //个人成就
        if (indexPath.row == 0) {//出版作品
            if (![self.infoIdentificateDic[@"Cbzp"] isEqualToNumber:@0])
            {
                BriefEditPublishListVC *project = [BriefEditPublishListVC new];
                [self.navigationController pushViewController:project animated:YES];
            }else{
                BriefEditPublishVC *project = [BriefEditPublishVC new];
                project.editID = @"0";
                [self presentViewController:project animated:YES completion:nil];
            }
            
            //MyCertViewController *cert = [[MyCertViewController alloc]initStatus:@"1"];
            //[self presentViewController:cert animated:YES completion:nil];
        }else if (indexPath.row == 1){
            MyCertViewController *cert = [[MyCertViewController alloc]initStatus:@"0"];
            [self presentViewController:cert animated:YES completion:nil];
        }else if (indexPath.row == 2){//专利发明
            if (![self.infoIdentificateDic[@"Zlfm"] isEqualToNumber:@0])
            {
                BriefEditInventionListVC *project = [BriefEditInventionListVC new];
                [self.navigationController pushViewController:project animated:YES];
            }else{
                BriefEditInventionVC *project = [BriefEditInventionVC new];
                project.editID = @"0";
                [self presentViewController:project animated:YES completion:nil];
            }
            //MyCertViewController *cert = [[MyCertViewController alloc]initStatus:@"2"];
        }else if (indexPath.row == 3){
            StudyClassVC *class = [[StudyClassVC alloc]init];
            [self presentViewController:class animated:YES completion:nil];
        }else if (indexPath.row == 4){
            if ([GlobalData sharedInstance].GB_UserModel.datas3.count > 0)
            {
                MyProjectExperienceListVC *project = [MyProjectExperienceListVC new];
                [self.navigationController pushViewController:project animated:YES];
            }else{
                MyProjectExperienceVC *project = [MyProjectExperienceVC new];
                project.editID = @"0";
                [self presentViewController:project animated:YES completion:nil];
            }
        }else if (indexPath.row == 5){
            MyCertViewController *cert = [[MyCertViewController alloc]initStatus:@"3"];
            [self presentViewController:cert animated:YES completion:nil];
        }else if (indexPath.row == 6){
            BriefEditLanaguageVC *cert = [BriefEditLanaguageVC new];
            [self presentViewController:cert animated:YES completion:nil];
            //MyCertViewController *cert = [[MyCertViewController alloc]initStatus:@"4"];
            //[self presentViewController:cert animated:YES completion:nil];
        }
    }else if (indexPath.section == 4){
        MyLifeViewController *life = [MyLifeViewController new];
        [self presentViewController:life animated:YES completion:nil];
    }
}

//点击section 刷新组
-(void)tapSection:(UITapGestureRecognizer *)sender
{
//    NSLog(@"%ld",sender.view.tag);
//    if (_sectionEditButton)
//    {
//        if (putSectionIndex == sender.view.tag)
//        {
//            UIButton *button = (UIButton *)[sender.view.subviews.firstObject viewWithTag:100];
//            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:putSectionIndex];
//            putSectionIndex = 10;
//            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationBottom];
//            [self.tableView reloadData];
//            button.selected = NO;
//            NSLog(@"%ld",putSectionIndex);
//        }else{
//            UIButton *button = (UIButton *)[sender.view.subviews.firstObject viewWithTag:100];
//            _sectionEditButton = button;
//            
//            putSectionIndex = sender.view.tag;
//            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:putSectionIndex];
//            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationTop];
//            [self.tableView reloadData];
//            _sectionEditButton.selected = YES;
//            
//        }
//    }
    
    putSectionIndex = sender.view.tag;
    [self.tableView reloadData];
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:putSectionIndex];
    
    UIButton *button = (UIButton *)[sender.view.subviews.firstObject viewWithTag:100];
    [button setImage:[UIImage imageNamed:@"white_down"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"white_top"] forState:UIControlStateSelected];
    button.selected = !button.selected;
    if (button.selected)
    {
        //[self.tableView reloadData];
        [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationTop];
    }else{
        putSectionIndex = 10;
        [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationTop];
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark 点击薪资要求

-(void)tapPay
{
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _pickerView.delegate = self;
    [_pickerView setUpPickerViewTitle:@"薪资要求" selectFirstTitle:_pay selectSecondTitle:@"" selectModel:1 firstArray:_payArr secondArray:nil];
    _pickerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:_pickerView];
    [self showPickerView];
}

//pickerView的代理协议
//点击取消
-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

//点击确定
-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    _pay = content;
    NSMutableArray *finalArray = [NSMutableArray array];
    
    //删除字典里的pName键值对
    for (NSString *str in self.positionArr) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[GlobalMethod exchangeStringToDic:str]];
        [dic removeObjectForKey:@"pName"];
        [finalArray addObject:dic];
    }
    
    NSString *rtypes;
    
    if (finalArray.count > 0)
    {
        NSData * JSONData = [NSJSONSerialization dataWithJSONObject:finalArray
                                                            options:kNilOptions
                                                              error:nil];
        rtypes = [[NSString alloc]initWithData:JSONData encoding:NSUTF8StringEncoding];
    }
    
    
    
    NSString *city = [GlobalData sharedInstance].city;
    NSString *provience = [GlobalData sharedInstance].provience;
    
    NSString *payID = @"0";
    for (NSDictionary *dic in _payRootArr)
    {
        if ([_pay isEqualToString:dic[@"Sname"]])
        {
            payID = dic[@"SID"];
        }
    }
    

    [RequestApi editPositionTrendWithKey:[GlobalData sharedInstance].GB_Key rprovince:provience rcity:city pay:_pay payid:payID rtypes:rtypes nature:_nature Delegate:self success:^(NSDictionary *response) {

    }];
    
    [self hidePickerView];

}

-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    CGFloat scrollHeight = putSectionIndex == 0 ? section0HeaderHeight : sectionHeaderHeight;
//    NSLog(@"%f",scrollView.contentOffset.y);
    
    if (scrollView.contentOffset.y <= section0HeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=section0HeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-section0HeaderHeight, 0, 0, 0);
    }
}

#pragma mark 弹窗 请先完善基本信息

-(void)showKYAlertView
{
    [self.kyAlertView showAlertView:@"提示" message:@"请先完善基本信息" subBottonTitle:@"取消" cancelButtonTitle:@"前往" handler:^(AlertViewClickBottonType bottonType) {
        if (bottonType == AlertViewClickBottonTypeCancelButton)
        {
            MyAddBriefBasicInfoVC * vc = [[MyAddBriefBasicInfoVC alloc]init];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
