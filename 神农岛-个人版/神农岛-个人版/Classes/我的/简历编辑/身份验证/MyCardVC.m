//
//  MyCardVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyCardVC.h"
#import "GlobalRuleViewController.h"
#import "MyMainManager.h"
#import "RequestApi+MyBrief.h"
/** tool 图片相关 */
#import "ImagePickerViewController.h"
#import "FLImageShowVC.h"

@interface MyCardVC () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,ImagePickerViewControllerDelegate,RequestDelegate>

@property (nonatomic ,strong) MyMainManager *manager;

//@property (nonatomic ,strong) UIButton *imageButton;

@property (nonatomic ,strong) UIButton *selectButton;

@property (nonatomic ,strong) UIImageView *selectImageView;

@property (nonatomic ,strong) NSString *buttonTitle;
@property (nonatomic ,strong) NSString *upLoadCardPath; //申请认证的图片

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@end

@implementation MyCardVC{
    NSString *card;
    CGFloat authenticateFont;
}

//-(UIButton *)imageButton
//{
//    if (!_imageButton) {
//        _imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_imageButton setBackgroundImage:[UIImage imageNamed:@"个人身份证"] forState:UIControlStateNormal];
//        
//        
//    }
//    return _imageButton;
//}

-(UIImageView *)selectImageView
{
    if (!_selectImageView) {
        _selectImageView = [UIImageView new];
        _selectImageView.image = [UIImage imageNamed:@"个人身份证"];
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage)];
        [_selectImageView addGestureRecognizer:tapImage];
    }
    return _selectImageView;
}

-(UIButton *)selectButton
{
    if (!_selectButton)
    {
        _selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectButton setTitle:@"申请认证" forState:UIControlStateNormal];
        _selectButton.titleLabel.font = [UIFont systemFontOfSize:F(17)];
        _selectButton.backgroundColor = COLOR_MAINCOLOR;
        [_selectButton addTarget:self action:@selector(applicationID:) forControlEvents:UIControlEventTouchUpInside];
        [_selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_selectButton setCorner:4];
    }
    return _selectButton;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNav];
    [self setModel];
    [self create];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"个人认证" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"须知" rightBlock:^{
        [weakSelf rule];
    }]];
    
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)rule
{
    GlobalRuleViewController *rule = [[GlobalRuleViewController alloc]init];
    rule.navTitle  = @"实名认证须知";
    rule.ruleKey    = @"authenticateRule";
    [self.navigationController pushViewController:rule animated:YES];
}

-(void)setModel
{
    NSString *cardPath = [GlobalData sharedInstance].cardPath;
    NSString *cardStatus = [NSString stringWithFormat:@"%@",[GlobalData sharedInstance].cardStauts];
    
    self.selectImageView.userInteractionEnabled = NO;
    if ([cardStatus isEqualToString:@"0"]) {
        self.buttonTitle = @"申请认证";
        self.selectImageView.userInteractionEnabled = YES;
    }else if ([cardStatus isEqualToString:@"1"]){
        self.buttonTitle = @"正在认证";
    }else if ([cardStatus isEqualToString:@"2"]){
        self.buttonTitle = @"已认证";
    }else if ([cardStatus isEqualToString:@"3"]){
        self.buttonTitle = @"认证失败";
        self.selectImageView.userInteractionEnabled = YES;
    }else{
        self.buttonTitle = @"申请认证";
    }
    
    [self.selectImageView sd_setImageWithURL:[NSURL URLWithString:cardPath] placeholderImage:[UIImage imageNamed:@"个人身份证"]];
    [self.selectButton setTitle:self.buttonTitle forState:UIControlStateNormal];
}

-(void)create
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(W(20), NAVIGATION_BarHeight + 1 + W(10), 5, 20)];
    label.textColor = COLOR_LABELThreeCOLOR;
    label.text = @"认证信息:";
    label.font = [UIFont systemFontOfSize:F(14)];
    [label sizeToFit];
    
    UILabel *idCard = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label.frame) + W(3), label.y, 5, 20)];
    idCard.text = @"身份证";
    idCard.textColor = COLOR_LABELSIXCOLOR;
    idCard.font = [UIFont systemFontOfSize:F(14)];
    [idCard sizeToFit];
    
    [self.view addSubview:label];
    [self.view addSubview:idCard];
    
    [self.selectImageView setFrame:CGRectMake(W(20), label.bottom + 10, KWIDTH - W(20) * 2, (KWIDTH - W(20) * 2) / 4 * 3)];
    [self.selectButton setFrame:CGRectMake(W(20), self.selectImageView.bottom + W(10), KWIDTH - W(20) * 2, W(50))];
    
    [self.view addSubview:self.selectImageView];
    [self.view addSubview:self.selectButton];
    
}

////申请实名认证
-(void)applicationID:(UIButton *)sender
{
    if ([self.buttonTitle isEqualToString:@"申请认证"])
    {
        if (_upLoadCardPath.length < 7) {
            [MBProgressHUD showError:@"请选择图片" toView:self.view];
        }else{
            DSWeak;
            [RequestApi getCardApplyWithKey:[GlobalData sharedInstance].GB_Key cardpath:self.upLoadCardPath Delegate:self success:^(NSDictionary *response, id mark) {
                [GlobalData sharedInstance].cardPath = weakSelf.upLoadCardPath;
                [GlobalData sharedInstance].cardStauts = @"1";
                [MBProgressHUD showSuccess:@"保存成功" toView:weakSelf.view];
                NSTimer *timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
                [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
                weakSelf.hideDelayTimer = timer;
            } failure:^(NSString *errorStr, id mark) {
                
            }];
        }
    }
}

-(void)handleHideTimer
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tapImage
{
    ImagePickerViewController *ivc = [[ImagePickerViewController alloc] init];
    ivc.delegate = self;
    ivc.photoNumber = 1;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:ivc];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - ImagePickerViewControllerDelegate
- (void)imagePickerViewController:(ImagePickerViewController *)ivc everyImageClick:(ALAsset *)asset index:(NSInteger)index imageUrlArray:(NSArray *)imageUrlArray imageIndexArray:(NSArray *)imageIndexArray
{
    FLImageShowVC *fvc = [[FLImageShowVC alloc] init];
    fvc.albumImageUrlArray = imageUrlArray;
    fvc.currentIndex = index;
    fvc.imageIndexArray = (NSMutableArray *)imageIndexArray;
    [ivc.navigationController pushViewController:fvc animated:YES];
}

- (void)imagePickerViewController:(ImagePickerViewController *)ivc finishClick:(NSArray *)assetArray
{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"选中的所有相片的url，可以通过上面注释的方法获取图片:%@",assetArray);
    NSData * data;
    if (!kArrayIsEmpty(assetArray)) {
        ALAsset * asset = assetArray[0];
        UIImage * image = [UIImage imageWithCGImage:asset.aspectRatioThumbnail];
        data = UIImagePNGRepresentation(image);
    }
    if (!data || data == nil) {
        [MBProgressHUD showSuccess:@"抱歉，暂不支持该图片格式\n请重新上传" toView:self.view];
    }
    else
    {
        [self getImageURL:data];
    }
}

- (void)imagePickerViewController:(ImagePickerViewController *)ivc firstImageClick:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    [self getImageURL:imageData];
}

-(void)getImageURL:(NSData *)data
{
    DSWeak;
    [[RequestApi share] getImageUrlWithAliYunWithImageData:data BlockHandelImageUrlStr:^(NSString *str) {
        NSString *strURL = [NSString stringWithFormat:@"%@%@",ssndHTTP,str];
        [weakSelf.selectImageView sd_setImageWithURL:[NSURL URLWithString:strURL]];
        weakSelf.upLoadCardPath = strURL;
    } BlockHandelSuccess:^(BOOL success) {
        if (success)
        {
            
        }
    }];
}



-(void)dealloc
{
    self.hideDelayTimer = nil;
    [self.hideDelayTimer invalidate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
