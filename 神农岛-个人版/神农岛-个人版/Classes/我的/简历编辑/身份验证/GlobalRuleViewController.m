//
//  GlobalRuleViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "GlobalRuleViewController.h"
#import <WebKit/WebKit.h>
#import "MyMainManager.h"
#import "RequestApi+Main.h"
@interface GlobalRuleViewController ()<WKUIDelegate,WKNavigationDelegate,RequestDelegate>

@property (nonatomic ,strong) WKWebView *webView;
@property (nonatomic ,strong) MyMainManager *manager;

@end

@implementation GlobalRuleViewController{
    CGFloat htmlFont;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
//    _manager = [MyMainManager share];
    [self setNav];
    [self setUI];
    [self createWebView];
    [self loadWeb];
}

-(void)setNav
{
    
    [self.view addSubview:[BaseNavView initNavTitle:_navTitle leftImageName:@"" leftBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setUI
{
    htmlFont = HTMLFONT;
    if (isIphone5) {
        htmlFont = HTMLFONT5;
    }else if (isIphone6){
        htmlFont = HTMLFONT6;
    }
}

-(void)createWebView
{
    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(5, NAVIGATION_BarHeight + 1, KWIDTH - 10, KHEIGHT)];
    self.webView.UIDelegate         = self;
    self.webView.navigationDelegate = self;
    //    self.webView.userInteractionEnabled = NO;
    [self.view addSubview:_webView];
}

-(void)loadWeb
{
    if (kStringIsEmpty(self.ruleKey))
    {
        return;
    }
    
    [RequestApi getBuyNoticeWithKey:[GlobalData sharedInstance].GB_Key cKey:self.ruleKey Delegate:self success:^(NSDictionary *response) {
        
            NSArray *arr = response[@"datas"];
            if (arr.count > 0)
            {
                NSDictionary *d = arr.firstObject;
                NSString *htmlStr =  [NSString stringWithFormat:@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;\" name=\"viewport\" /><meta name=\"apple-mobile-web-app-capable\" content=\"yes\"><meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\"><link rel=\"stylesheet\" type=\"text/css\" /><style type=\"text/css\"> #content{font-size:%.fpx;}</style> <style>img{width:%.fpx !important;}</style></head><body><div id=\"content\">%@</div>",htmlFont,KWIDTH - 20,d[@"S_Value"]];
                [_webView loadHTMLString:htmlStr baseURL:nil];
            }
    }];
    


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
