//
//  Datas1.h
//
//  Created by 晨光 宋 on 17/2/8
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Datas1 : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *wBackUph;
@property (nonatomic, strong) NSString *wSalary;
@property (nonatomic, strong) NSString *wQuitD;
@property (nonatomic, strong) NSString *wContent;
@property (nonatomic, strong) NSString *wScale;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *wPosition;
@property (nonatomic, strong) NSString *wEntryD;
@property (nonatomic, strong) NSString *wBackUp;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double isQuits;
@property (nonatomic, strong) NSString *wNature;
@property (nonatomic, strong) NSString *wBackUpd;
@property (nonatomic, assign) double wDelete;
@property (nonatomic, strong) NSString *wIndustry;
@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *wComName;
@property (nonatomic, assign) double rID;
@property (nonatomic, strong) NSString *wBackUpe;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
