//
//  BaseClass.m
//
//  Created by 晨光 宋 on 17/2/8
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "BaseClass.h"
#import "BriefInfoModel.h"
#import "Datas1.h"
#import "Datas2.h"
#import "Datas3.h"
#import "AllImageModel.h"   //datas4


NSString *const kBaseClassMessge = @"messge";
NSString *const kBaseClassDatas0 = @"datas0";
NSString *const kBaseClassDatas1 = @"datas1";
NSString *const kBaseClassDatas3 = @"datas3";
NSString *const kBaseClassCode = @"code";
NSString *const kBaseClassDatas2 = @"datas2";
NSString *const kBaseClassDatas4 = @"datas4";


@interface BaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BaseClass

@synthesize messge = _messge;
@synthesize datas0 = _datas0;
@synthesize datas1 = _datas1;
@synthesize datas3 = _datas3;
@synthesize code = _code;
@synthesize datas2 = _datas2;
@synthesize datas4 = _datas4;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.messge = [self objectOrNilForKey:kBaseClassMessge fromDictionary:dict];
    NSObject *receivedDatas0 = [dict objectForKey:kBaseClassDatas0];
    NSMutableArray *parsedDatas0 = [NSMutableArray array];
    
    if ([receivedDatas0 isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedDatas0) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedDatas0 addObject:[BriefInfoModel modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedDatas0 isKindOfClass:[NSDictionary class]]) {
       [parsedDatas0 addObject:[BriefInfoModel modelObjectWithDictionary:(NSDictionary *)receivedDatas0]];
    }

    self.datas0 = [NSArray arrayWithArray:parsedDatas0];
        
    NSObject *receivedDatas1 = [dict objectForKey:kBaseClassDatas1];
    NSMutableArray *parsedDatas1 = [NSMutableArray array];
    
    if ([receivedDatas1 isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedDatas1) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedDatas1 addObject:[Datas1 modelObjectWithDictionary:item]];
            }
        }
    } else if ([receivedDatas1 isKindOfClass:[NSDictionary class]]) {
        [parsedDatas1 addObject:[Datas1 modelObjectWithDictionary:(NSDictionary *)receivedDatas1]];
    }
    
    self.datas1 = [NSMutableArray arrayWithArray:parsedDatas1];
        
    NSObject *receivedDatas3 = [dict objectForKey:kBaseClassDatas3];
    NSMutableArray *parsedDatas3 = [NSMutableArray array];
    
    if ([receivedDatas3 isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedDatas3) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedDatas3 addObject:[Datas3 modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedDatas3 isKindOfClass:[NSDictionary class]]) {
       [parsedDatas3 addObject:[Datas3 modelObjectWithDictionary:(NSDictionary *)receivedDatas3]];
    }

    self.datas3 = [NSMutableArray arrayWithArray:parsedDatas3];
            self.code = [[self objectOrNilForKey:kBaseClassCode fromDictionary:dict] doubleValue];
    NSObject *receivedDatas2 = [dict objectForKey:kBaseClassDatas2];
    NSMutableArray *parsedDatas2 = [NSMutableArray array];
    
    if ([receivedDatas2 isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedDatas2) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedDatas2 addObject:[Datas2 modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedDatas2 isKindOfClass:[NSDictionary class]]) {
       [parsedDatas2 addObject:[Datas2 modelObjectWithDictionary:(NSDictionary *)receivedDatas2]];
    }

    self.datas2 = [NSMutableArray arrayWithArray:parsedDatas2];
    NSObject *receivedDatas4 = [dict objectForKey:kBaseClassDatas4];
    NSMutableArray *parsedDatas4 = [NSMutableArray array];
    
    if ([receivedDatas4 isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedDatas4) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedDatas4 addObject:[AllImageModel modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedDatas4 isKindOfClass:[NSDictionary class]]) {
       [parsedDatas4 addObject:[AllImageModel modelObjectWithDictionary:(NSDictionary *)receivedDatas4]];
    }

    self.datas4 = [NSArray arrayWithArray:parsedDatas4];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.messge forKey:kBaseClassMessge];
    NSMutableArray *tempArrayForDatas0 = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.datas0) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDatas0 addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDatas0 addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDatas0] forKey:kBaseClassDatas0];
    NSMutableArray *tempArrayForDatas1 = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.datas1) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDatas1 addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDatas1 addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDatas1] forKey:kBaseClassDatas1];
    NSMutableArray *tempArrayForDatas3 = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.datas3) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDatas3 addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDatas3 addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDatas3] forKey:kBaseClassDatas3];
    [mutableDict setValue:[NSNumber numberWithDouble:self.code] forKey:kBaseClassCode];
    NSMutableArray *tempArrayForDatas2 = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.datas2) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDatas2 addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDatas2 addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDatas2] forKey:kBaseClassDatas2];
    NSMutableArray *tempArrayForDatas4 = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.datas4) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDatas4 addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDatas4 addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDatas4] forKey:kBaseClassDatas4];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.messge = [aDecoder decodeObjectForKey:kBaseClassMessge];
    self.datas0 = [aDecoder decodeObjectForKey:kBaseClassDatas0];
    self.datas1 = [aDecoder decodeObjectForKey:kBaseClassDatas1];
    self.datas3 = [aDecoder decodeObjectForKey:kBaseClassDatas3];
    self.code = [aDecoder decodeDoubleForKey:kBaseClassCode];
    self.datas2 = [aDecoder decodeObjectForKey:kBaseClassDatas2];
    self.datas4 = [aDecoder decodeObjectForKey:kBaseClassDatas4];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_messge forKey:kBaseClassMessge];
    [aCoder encodeObject:_datas0 forKey:kBaseClassDatas0];
    [aCoder encodeObject:_datas1 forKey:kBaseClassDatas1];
    [aCoder encodeObject:_datas3 forKey:kBaseClassDatas3];
    [aCoder encodeDouble:_code forKey:kBaseClassCode];
    [aCoder encodeObject:_datas2 forKey:kBaseClassDatas2];
    [aCoder encodeObject:_datas4 forKey:kBaseClassDatas4];
}

- (id)copyWithZone:(NSZone *)zone {
    BaseClass *copy = [[BaseClass alloc] init];
    
    
    
    if (copy) {

        copy.messge = [self.messge copyWithZone:zone];
        copy.datas0 = [self.datas0 copyWithZone:zone];
        copy.datas1 = [self.datas1 copyWithZone:zone];
        copy.datas3 = [self.datas3 copyWithZone:zone];
        copy.code = self.code;
        copy.datas2 = [self.datas2 copyWithZone:zone];
        copy.datas4 = [self.datas4 copyWithZone:zone];
    }
    
    return copy;
}


@end
