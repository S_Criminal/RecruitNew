//
//  BaseClass.h
//
//  Created by 晨光 宋 on 17/2/8
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *messge;
@property (nonatomic, strong) NSArray *datas0;
/**
 * 工作经历
 */
@property (nonatomic, strong) NSMutableArray *datas1;

@property (nonatomic, strong) NSMutableArray *datas3;
@property (nonatomic, assign) double code;
/**
 * 教育经历
 */
@property (nonatomic, strong) NSMutableArray *datas2;
@property (nonatomic, strong) NSArray *datas4;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
