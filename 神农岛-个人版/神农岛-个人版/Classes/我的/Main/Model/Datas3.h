//
//  Datas3.h
//
//  Created by 晨光 宋 on 17/2/8
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Datas3 : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *pBackUpd;
@property (nonatomic, strong) NSString *pUrl;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *pEndD;
@property (nonatomic, assign) double isOnline;
@property (nonatomic, strong) NSString *pBackUph;
@property (nonatomic, strong) NSString *pBackUpe;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *pDescription;
@property (nonatomic, strong) NSString *pDuty;
@property (nonatomic, strong) NSString *pStartD;
@property (nonatomic, strong) NSString *pBackUp;
@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *eRemark;
@property (nonatomic, strong) NSString *rID;
@property (nonatomic, strong) NSString *pDelete;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
