//
//  Datas1.m
//
//  Created by 晨光 宋 on 17/2/8
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Datas1.h"


NSString *const kDatas1WBackUph = @"W_BackUph";
NSString *const kDatas1WSalary = @"W_Salary";
NSString *const kDatas1WQuitD = @"W_QuitD";
NSString *const kDatas1WContent = @"W_Content";
NSString *const kDatas1WScale = @"W_Scale";
NSString *const kDatas1ID = @"ID";
NSString *const kDatas1WPosition = @"W_Position";
NSString *const kDatas1WEntryD = @"W_EntryD";
NSString *const kDatas1WBackUp = @"W_BackUp";
NSString *const kDatas1CreateDate = @"CreateDate";
NSString *const kDatas1IsQuits = @"IsQuits";
NSString *const kDatas1WNature = @"W_Nature";
NSString *const kDatas1WBackUpd = @"W_BackUpd";
NSString *const kDatas1WDelete = @"W_Delete";
NSString *const kDatas1WIndustry = @"W_Industry";
NSString *const kDatas1UID = @"U_ID";
NSString *const kDatas1WComName = @"W_ComName";
NSString *const kDatas1RID = @"R_ID";
NSString *const kDatas1WBackUpe = @"W_BackUpe";


@interface Datas1 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Datas1

@synthesize wBackUph = _wBackUph;
@synthesize wSalary = _wSalary;
@synthesize wQuitD = _wQuitD;
@synthesize wContent = _wContent;
@synthesize wScale = _wScale;
@synthesize iDProperty = _iDProperty;
@synthesize wPosition = _wPosition;
@synthesize wEntryD = _wEntryD;
@synthesize wBackUp = _wBackUp;
@synthesize createDate = _createDate;
@synthesize isQuits = _isQuits;
@synthesize wNature = _wNature;
@synthesize wBackUpd = _wBackUpd;
@synthesize wDelete = _wDelete;
@synthesize wIndustry = _wIndustry;
@synthesize uID = _uID;
@synthesize wComName = _wComName;
@synthesize rID = _rID;
@synthesize wBackUpe = _wBackUpe;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.wBackUph = [self objectOrNilForKey:kDatas1WBackUph fromDictionary:dict];
            self.wSalary = [self objectOrNilForKey:kDatas1WSalary fromDictionary:dict];
            self.wQuitD = [self objectOrNilForKey:kDatas1WQuitD fromDictionary:dict];
            self.wContent = [self objectOrNilForKey:kDatas1WContent fromDictionary:dict];
            self.wScale = [self objectOrNilForKey:kDatas1WScale fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kDatas1ID fromDictionary:dict] doubleValue];
            self.wPosition = [self objectOrNilForKey:kDatas1WPosition fromDictionary:dict];
            self.wEntryD = [self objectOrNilForKey:kDatas1WEntryD fromDictionary:dict];
            self.wBackUp = [self objectOrNilForKey:kDatas1WBackUp fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kDatas1CreateDate fromDictionary:dict];
            self.isQuits = [[self objectOrNilForKey:kDatas1IsQuits fromDictionary:dict] doubleValue];
            self.wNature = [self objectOrNilForKey:kDatas1WNature fromDictionary:dict];
            self.wBackUpd = [self objectOrNilForKey:kDatas1WBackUpd fromDictionary:dict];
            self.wDelete = [[self objectOrNilForKey:kDatas1WDelete fromDictionary:dict] doubleValue];
            self.wIndustry = [self objectOrNilForKey:kDatas1WIndustry fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kDatas1UID fromDictionary:dict] doubleValue];
            self.wComName = [self objectOrNilForKey:kDatas1WComName fromDictionary:dict];
            self.rID = [[self objectOrNilForKey:kDatas1RID fromDictionary:dict] doubleValue];
            self.wBackUpe = [self objectOrNilForKey:kDatas1WBackUpe fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.wBackUph forKey:kDatas1WBackUph];
    [mutableDict setValue:self.wSalary forKey:kDatas1WSalary];
    [mutableDict setValue:self.wQuitD forKey:kDatas1WQuitD];
    [mutableDict setValue:self.wContent forKey:kDatas1WContent];
    [mutableDict setValue:self.wScale forKey:kDatas1WScale];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kDatas1ID];
    [mutableDict setValue:self.wPosition forKey:kDatas1WPosition];
    [mutableDict setValue:self.wEntryD forKey:kDatas1WEntryD];
    [mutableDict setValue:self.wBackUp forKey:kDatas1WBackUp];
    [mutableDict setValue:self.createDate forKey:kDatas1CreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isQuits] forKey:kDatas1IsQuits];
    [mutableDict setValue:self.wNature forKey:kDatas1WNature];
    [mutableDict setValue:self.wBackUpd forKey:kDatas1WBackUpd];
    [mutableDict setValue:[NSNumber numberWithDouble:self.wDelete] forKey:kDatas1WDelete];
    [mutableDict setValue:self.wIndustry forKey:kDatas1WIndustry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kDatas1UID];
    [mutableDict setValue:self.wComName forKey:kDatas1WComName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rID] forKey:kDatas1RID];
    [mutableDict setValue:self.wBackUpe forKey:kDatas1WBackUpe];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.wBackUph = [aDecoder decodeObjectForKey:kDatas1WBackUph];
    self.wSalary = [aDecoder decodeObjectForKey:kDatas1WSalary];
    self.wQuitD = [aDecoder decodeObjectForKey:kDatas1WQuitD];
    self.wContent = [aDecoder decodeObjectForKey:kDatas1WContent];
    self.wScale = [aDecoder decodeObjectForKey:kDatas1WScale];
    self.iDProperty = [aDecoder decodeDoubleForKey:kDatas1ID];
    self.wPosition = [aDecoder decodeObjectForKey:kDatas1WPosition];
    self.wEntryD = [aDecoder decodeObjectForKey:kDatas1WEntryD];
    self.wBackUp = [aDecoder decodeObjectForKey:kDatas1WBackUp];
    self.createDate = [aDecoder decodeObjectForKey:kDatas1CreateDate];
    self.isQuits = [aDecoder decodeDoubleForKey:kDatas1IsQuits];
    self.wNature = [aDecoder decodeObjectForKey:kDatas1WNature];
    self.wBackUpd = [aDecoder decodeObjectForKey:kDatas1WBackUpd];
    self.wDelete = [aDecoder decodeDoubleForKey:kDatas1WDelete];
    self.wIndustry = [aDecoder decodeObjectForKey:kDatas1WIndustry];
    self.uID = [aDecoder decodeDoubleForKey:kDatas1UID];
    self.wComName = [aDecoder decodeObjectForKey:kDatas1WComName];
    self.rID = [aDecoder decodeDoubleForKey:kDatas1RID];
    self.wBackUpe = [aDecoder decodeObjectForKey:kDatas1WBackUpe];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_wBackUph forKey:kDatas1WBackUph];
    [aCoder encodeObject:_wSalary forKey:kDatas1WSalary];
    [aCoder encodeObject:_wQuitD forKey:kDatas1WQuitD];
    [aCoder encodeObject:_wContent forKey:kDatas1WContent];
    [aCoder encodeObject:_wScale forKey:kDatas1WScale];
    [aCoder encodeDouble:_iDProperty forKey:kDatas1ID];
    [aCoder encodeObject:_wPosition forKey:kDatas1WPosition];
    [aCoder encodeObject:_wEntryD forKey:kDatas1WEntryD];
    [aCoder encodeObject:_wBackUp forKey:kDatas1WBackUp];
    [aCoder encodeObject:_createDate forKey:kDatas1CreateDate];
    [aCoder encodeDouble:_isQuits forKey:kDatas1IsQuits];
    [aCoder encodeObject:_wNature forKey:kDatas1WNature];
    [aCoder encodeObject:_wBackUpd forKey:kDatas1WBackUpd];
    [aCoder encodeDouble:_wDelete forKey:kDatas1WDelete];
    [aCoder encodeObject:_wIndustry forKey:kDatas1WIndustry];
    [aCoder encodeDouble:_uID forKey:kDatas1UID];
    [aCoder encodeObject:_wComName forKey:kDatas1WComName];
    [aCoder encodeDouble:_rID forKey:kDatas1RID];
    [aCoder encodeObject:_wBackUpe forKey:kDatas1WBackUpe];
}

- (id)copyWithZone:(NSZone *)zone {
    Datas1 *copy = [[Datas1 alloc] init];
    
    
    
    if (copy) {

        copy.wBackUph = [self.wBackUph copyWithZone:zone];
        copy.wSalary = [self.wSalary copyWithZone:zone];
        copy.wQuitD = [self.wQuitD copyWithZone:zone];
        copy.wContent = [self.wContent copyWithZone:zone];
        copy.wScale = [self.wScale copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.wPosition = [self.wPosition copyWithZone:zone];
        copy.wEntryD = [self.wEntryD copyWithZone:zone];
        copy.wBackUp = [self.wBackUp copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.isQuits = self.isQuits;
        copy.wNature = [self.wNature copyWithZone:zone];
        copy.wBackUpd = [self.wBackUpd copyWithZone:zone];
        copy.wDelete = self.wDelete;
        copy.wIndustry = [self.wIndustry copyWithZone:zone];
        copy.uID = self.uID;
        copy.wComName = [self.wComName copyWithZone:zone];
        copy.rID = self.rID;
        copy.wBackUpe = [self.wBackUpe copyWithZone:zone];
    }
    
    return copy;
}


@end
