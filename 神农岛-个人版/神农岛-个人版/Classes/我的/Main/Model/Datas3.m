//
//  Datas3.m
//
//  Created by 晨光 宋 on 17/2/8
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Datas3.h"


NSString *const kDatas3PBackUpd = @"P_BackUpd";
NSString *const kDatas3PUrl = @"P_Url";
NSString *const kDatas3ID = @"ID";
NSString *const kDatas3PName = @"P_Name";
NSString *const kDatas3PEndD = @"P_EndD";
NSString *const kDatas3IsOnline = @"IsOnline";
NSString *const kDatas3PBackUph = @"P_BackUph";
NSString *const kDatas3PBackUpe = @"P_BackUpe";
NSString *const kDatas3CreateDate = @"CreateDate";
NSString *const kDatas3PDescription = @"P_Description";
NSString *const kDatas3PDuty = @"P_Duty";
NSString *const kDatas3PStartD = @"P_StartD";
NSString *const kDatas3PBackUp = @"P_BackUp";
NSString *const kDatas3UID = @"U_ID";
NSString *const kDatas3ERemark = @"E_Remark";
NSString *const kDatas3RID = @"R_ID";
NSString *const kDatas3PDelete = @"P_Delete";


@interface Datas3 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Datas3

@synthesize pBackUpd = _pBackUpd;
@synthesize pUrl = _pUrl;
@synthesize iDProperty = _iDProperty;
@synthesize pName = _pName;
@synthesize pEndD = _pEndD;
@synthesize isOnline = _isOnline;
@synthesize pBackUph = _pBackUph;
@synthesize pBackUpe = _pBackUpe;
@synthesize createDate = _createDate;
@synthesize pDescription = _pDescription;
@synthesize pDuty = _pDuty;
@synthesize pStartD = _pStartD;
@synthesize pBackUp = _pBackUp;
@synthesize uID = _uID;
@synthesize eRemark = _eRemark;
@synthesize rID = _rID;
@synthesize pDelete = _pDelete;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.pBackUpd = [self objectOrNilForKey:kDatas3PBackUpd fromDictionary:dict];
            self.pUrl = [self objectOrNilForKey:kDatas3PUrl fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kDatas3ID fromDictionary:dict] doubleValue];
            self.pName = [self objectOrNilForKey:kDatas3PName fromDictionary:dict];
            self.pEndD = [self objectOrNilForKey:kDatas3PEndD fromDictionary:dict];
            self.isOnline = [[self objectOrNilForKey:kDatas3IsOnline fromDictionary:dict] doubleValue];
            self.pBackUph = [self objectOrNilForKey:kDatas3PBackUph fromDictionary:dict];
            self.pBackUpe = [self objectOrNilForKey:kDatas3PBackUpe fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kDatas3CreateDate fromDictionary:dict];
            self.pDescription = [self objectOrNilForKey:kDatas3PDescription fromDictionary:dict];
            self.pDuty = [self objectOrNilForKey:kDatas3PDuty fromDictionary:dict];
            self.pStartD = [self objectOrNilForKey:kDatas3PStartD fromDictionary:dict];
            self.pBackUp = [self objectOrNilForKey:kDatas3PBackUp fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kDatas3UID fromDictionary:dict] doubleValue];
            self.eRemark = [self objectOrNilForKey:kDatas3ERemark fromDictionary:dict];
            self.rID = [self objectOrNilForKey:kDatas3RID fromDictionary:dict];
            self.pDelete = [self objectOrNilForKey:kDatas3PDelete fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.pBackUpd forKey:kDatas3PBackUpd];
    [mutableDict setValue:self.pUrl forKey:kDatas3PUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kDatas3ID];
    [mutableDict setValue:self.pName forKey:kDatas3PName];
    [mutableDict setValue:self.pEndD forKey:kDatas3PEndD];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isOnline] forKey:kDatas3IsOnline];
    [mutableDict setValue:self.pBackUph forKey:kDatas3PBackUph];
    [mutableDict setValue:self.pBackUpe forKey:kDatas3PBackUpe];
    [mutableDict setValue:self.createDate forKey:kDatas3CreateDate];
    [mutableDict setValue:self.pDescription forKey:kDatas3PDescription];
    [mutableDict setValue:self.pDuty forKey:kDatas3PDuty];
    [mutableDict setValue:self.pStartD forKey:kDatas3PStartD];
    [mutableDict setValue:self.pBackUp forKey:kDatas3PBackUp];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kDatas3UID];
    [mutableDict setValue:self.eRemark forKey:kDatas3ERemark];
    [mutableDict setValue:self.rID forKey:kDatas3RID];
    [mutableDict setValue:self.pDelete forKey:kDatas3PDelete];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.pBackUpd = [aDecoder decodeObjectForKey:kDatas3PBackUpd];
    self.pUrl = [aDecoder decodeObjectForKey:kDatas3PUrl];
    self.iDProperty = [aDecoder decodeDoubleForKey:kDatas3ID];
    self.pName = [aDecoder decodeObjectForKey:kDatas3PName];
    self.pEndD = [aDecoder decodeObjectForKey:kDatas3PEndD];
    self.isOnline = [aDecoder decodeDoubleForKey:kDatas3IsOnline];
    self.pBackUph = [aDecoder decodeObjectForKey:kDatas3PBackUph];
    self.pBackUpe = [aDecoder decodeObjectForKey:kDatas3PBackUpe];
    self.createDate = [aDecoder decodeObjectForKey:kDatas3CreateDate];
    self.pDescription = [aDecoder decodeObjectForKey:kDatas3PDescription];
    self.pDuty = [aDecoder decodeObjectForKey:kDatas3PDuty];
    self.pStartD = [aDecoder decodeObjectForKey:kDatas3PStartD];
    self.pBackUp = [aDecoder decodeObjectForKey:kDatas3PBackUp];
    self.uID = [aDecoder decodeDoubleForKey:kDatas3UID];
    self.eRemark = [aDecoder decodeObjectForKey:kDatas3ERemark];
    self.rID = [aDecoder decodeObjectForKey:kDatas3RID];
    self.pDelete = [aDecoder decodeObjectForKey:kDatas3PDelete];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_pBackUpd forKey:kDatas3PBackUpd];
    [aCoder encodeObject:_pUrl forKey:kDatas3PUrl];
    [aCoder encodeDouble:_iDProperty forKey:kDatas3ID];
    [aCoder encodeObject:_pName forKey:kDatas3PName];
    [aCoder encodeObject:_pEndD forKey:kDatas3PEndD];
    [aCoder encodeDouble:_isOnline forKey:kDatas3IsOnline];
    [aCoder encodeObject:_pBackUph forKey:kDatas3PBackUph];
    [aCoder encodeObject:_pBackUpe forKey:kDatas3PBackUpe];
    [aCoder encodeObject:_createDate forKey:kDatas3CreateDate];
    [aCoder encodeObject:_pDescription forKey:kDatas3PDescription];
    [aCoder encodeObject:_pDuty forKey:kDatas3PDuty];
    [aCoder encodeObject:_pStartD forKey:kDatas3PStartD];
    [aCoder encodeObject:_pBackUp forKey:kDatas3PBackUp];
    [aCoder encodeDouble:_uID forKey:kDatas3UID];
    [aCoder encodeObject:_eRemark forKey:kDatas3ERemark];
    [aCoder encodeObject:_rID forKey:kDatas3RID];
    [aCoder encodeObject:_pDelete forKey:kDatas3PDelete];
}

- (id)copyWithZone:(NSZone *)zone {
    Datas3 *copy = [[Datas3 alloc] init];
    
    
    
    if (copy) {

        copy.pBackUpd = [self.pBackUpd copyWithZone:zone];
        copy.pUrl = [self.pUrl copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.pName = [self.pName copyWithZone:zone];
        copy.pEndD = [self.pEndD copyWithZone:zone];
        copy.isOnline = self.isOnline;
        copy.pBackUph = [self.pBackUph copyWithZone:zone];
        copy.pBackUpe = [self.pBackUpe copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.pDescription = [self.pDescription copyWithZone:zone];
        copy.pDuty = [self.pDuty copyWithZone:zone];
        copy.pStartD = [self.pStartD copyWithZone:zone];
        copy.pBackUp = [self.pBackUp copyWithZone:zone];
        copy.uID = self.uID;
        copy.eRemark = [self.eRemark copyWithZone:zone];
        copy.rID = [self.rID copyWithZone:zone];
        copy.pDelete = [self.pDelete copyWithZone:zone];
    }
    
    return copy;
}


@end
