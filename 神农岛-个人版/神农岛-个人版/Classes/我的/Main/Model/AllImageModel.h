//
//  AllImageModel.h
//
//  Created by   on 17/4/7
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface AllImageModel : NSObject

@property (nonatomic, strong) NSString *sDesp;
@property (nonatomic, strong) NSString *sPath;
@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *sType;
@property (nonatomic, assign) double sClass;
@property (nonatomic, strong) NSString *sSdate;
@property (nonatomic, strong) NSString *sTitle;
@property (nonatomic, strong) NSString *sEdate;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *sContents;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
