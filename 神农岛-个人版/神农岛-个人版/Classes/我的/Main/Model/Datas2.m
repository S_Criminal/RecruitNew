//
//  Datas2.m
//
//  Created by 晨光 宋 on 17/1/2
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Datas2.h"


NSString *const kDatas2EBackUpd = @"E_BackUpd";
NSString *const kDatas2EMajor = @"E_Major";
NSString *const kDatas2EIsent = @"E_Isent";
NSString *const kDatas2EBackUph = @"E_BackUph";
NSString *const kDatas2ESchool = @"E_School";
NSString *const kDatas2EEndD = @"E_EndD";
NSString *const kDatas2ID = @"ID";
NSString *const kDatas2EBackUpe = @"E_BackUpe";
NSString *const kDatas2CreateDate = @"CreateDate";
NSString *const kDatas2EMajors = @"E_Majors";
NSString *const kDatas2ERemark = @"E_Remark";
NSString *const kDatas2EStartD = @"E_StartD";
NSString *const kDatas2EBackUp = @"E_BackUp";
NSString *const kDatas2UID = @"U_ID";
NSString *const kDatas2EDelete = @"E_Delete";
NSString *const kDatas2RID = @"R_ID";
NSString *const kDatas2EDegree = @"E_Degree";


@interface Datas2 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Datas2

@synthesize eBackUpd = _eBackUpd;
@synthesize eMajor = _eMajor;
@synthesize eIsent = _eIsent;
@synthesize eBackUph = _eBackUph;
@synthesize eSchool = _eSchool;
@synthesize eEndD = _eEndD;
@synthesize iDProperty = _iDProperty;
@synthesize eBackUpe = _eBackUpe;
@synthesize createDate = _createDate;
@synthesize eMajors = _eMajors;
@synthesize eRemark = _eRemark;
@synthesize eStartD = _eStartD;
@synthesize eBackUp = _eBackUp;
@synthesize uID = _uID;
@synthesize eDelete = _eDelete;
@synthesize rID = _rID;
@synthesize eDegree = _eDegree;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.eBackUpd = [self objectOrNilForKey:kDatas2EBackUpd fromDictionary:dict];
            self.eMajor = [self objectOrNilForKey:kDatas2EMajor fromDictionary:dict];
            self.eIsent = [[self objectOrNilForKey:kDatas2EIsent fromDictionary:dict] doubleValue];
            self.eBackUph = [self objectOrNilForKey:kDatas2EBackUph fromDictionary:dict];
            self.eSchool = [self objectOrNilForKey:kDatas2ESchool fromDictionary:dict];
            self.eEndD = [self objectOrNilForKey:kDatas2EEndD fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kDatas2ID fromDictionary:dict] doubleValue];
            self.eBackUpe = [self objectOrNilForKey:kDatas2EBackUpe fromDictionary:dict];
            self.createDate = [self objectOrNilForKey:kDatas2CreateDate fromDictionary:dict];
            self.eMajors = [self objectOrNilForKey:kDatas2EMajors fromDictionary:dict];
            self.eRemark = [self objectOrNilForKey:kDatas2ERemark fromDictionary:dict];
            self.eStartD = [self objectOrNilForKey:kDatas2EStartD fromDictionary:dict];
            self.eBackUp = [self objectOrNilForKey:kDatas2EBackUp fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kDatas2UID fromDictionary:dict] doubleValue];
            self.eDelete = [[self objectOrNilForKey:kDatas2EDelete fromDictionary:dict] doubleValue];
            self.rID = [[self objectOrNilForKey:kDatas2RID fromDictionary:dict] doubleValue];
            self.eDegree = [self objectOrNilForKey:kDatas2EDegree fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.eBackUpd forKey:kDatas2EBackUpd];
    [mutableDict setValue:self.eMajor forKey:kDatas2EMajor];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eIsent] forKey:kDatas2EIsent];
    [mutableDict setValue:self.eBackUph forKey:kDatas2EBackUph];
    [mutableDict setValue:self.eSchool forKey:kDatas2ESchool];
    [mutableDict setValue:self.eEndD forKey:kDatas2EEndD];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kDatas2ID];
    [mutableDict setValue:self.eBackUpe forKey:kDatas2EBackUpe];
    [mutableDict setValue:self.createDate forKey:kDatas2CreateDate];
    [mutableDict setValue:self.eMajors forKey:kDatas2EMajors];
    [mutableDict setValue:self.eRemark forKey:kDatas2ERemark];
    [mutableDict setValue:self.eStartD forKey:kDatas2EStartD];
    [mutableDict setValue:self.eBackUp forKey:kDatas2EBackUp];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kDatas2UID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.eDelete] forKey:kDatas2EDelete];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rID] forKey:kDatas2RID];
    [mutableDict setValue:self.eDegree forKey:kDatas2EDegree];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.eBackUpd = [aDecoder decodeObjectForKey:kDatas2EBackUpd];
    self.eMajor = [aDecoder decodeObjectForKey:kDatas2EMajor];
    self.eIsent = [aDecoder decodeDoubleForKey:kDatas2EIsent];
    self.eBackUph = [aDecoder decodeObjectForKey:kDatas2EBackUph];
    self.eSchool = [aDecoder decodeObjectForKey:kDatas2ESchool];
    self.eEndD = [aDecoder decodeObjectForKey:kDatas2EEndD];
    self.iDProperty = [aDecoder decodeDoubleForKey:kDatas2ID];
    self.eBackUpe = [aDecoder decodeObjectForKey:kDatas2EBackUpe];
    self.createDate = [aDecoder decodeObjectForKey:kDatas2CreateDate];
    self.eMajors = [aDecoder decodeObjectForKey:kDatas2EMajors];
    self.eRemark = [aDecoder decodeObjectForKey:kDatas2ERemark];
    self.eStartD = [aDecoder decodeObjectForKey:kDatas2EStartD];
    self.eBackUp = [aDecoder decodeObjectForKey:kDatas2EBackUp];
    self.uID = [aDecoder decodeDoubleForKey:kDatas2UID];
    self.eDelete = [aDecoder decodeDoubleForKey:kDatas2EDelete];
    self.rID = [aDecoder decodeDoubleForKey:kDatas2RID];
    self.eDegree = [aDecoder decodeObjectForKey:kDatas2EDegree];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_eBackUpd forKey:kDatas2EBackUpd];
    [aCoder encodeObject:_eMajor forKey:kDatas2EMajor];
    [aCoder encodeDouble:_eIsent forKey:kDatas2EIsent];
    [aCoder encodeObject:_eBackUph forKey:kDatas2EBackUph];
    [aCoder encodeObject:_eSchool forKey:kDatas2ESchool];
    [aCoder encodeObject:_eEndD forKey:kDatas2EEndD];
    [aCoder encodeDouble:_iDProperty forKey:kDatas2ID];
    [aCoder encodeObject:_eBackUpe forKey:kDatas2EBackUpe];
    [aCoder encodeObject:_createDate forKey:kDatas2CreateDate];
    [aCoder encodeObject:_eMajors forKey:kDatas2EMajors];
    [aCoder encodeObject:_eRemark forKey:kDatas2ERemark];
    [aCoder encodeObject:_eStartD forKey:kDatas2EStartD];
    [aCoder encodeObject:_eBackUp forKey:kDatas2EBackUp];
    [aCoder encodeDouble:_uID forKey:kDatas2UID];
    [aCoder encodeDouble:_eDelete forKey:kDatas2EDelete];
    [aCoder encodeDouble:_rID forKey:kDatas2RID];
    [aCoder encodeObject:_eDegree forKey:kDatas2EDegree];
}

- (id)copyWithZone:(NSZone *)zone {
    Datas2 *copy = [[Datas2 alloc] init];
    
    
    
    if (copy) {

        copy.eBackUpd = [self.eBackUpd copyWithZone:zone];
        copy.eMajor = [self.eMajor copyWithZone:zone];
        copy.eIsent = self.eIsent;
        copy.eBackUph = [self.eBackUph copyWithZone:zone];
        copy.eSchool = [self.eSchool copyWithZone:zone];
        copy.eEndD = [self.eEndD copyWithZone:zone];
        copy.iDProperty = self.iDProperty;
        copy.eBackUpe = [self.eBackUpe copyWithZone:zone];
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.eMajors = [self.eMajors copyWithZone:zone];
        copy.eRemark = [self.eRemark copyWithZone:zone];
        copy.eStartD = [self.eStartD copyWithZone:zone];
        copy.eBackUp = [self.eBackUp copyWithZone:zone];
        copy.uID = self.uID;
        copy.eDelete = self.eDelete;
        copy.rID = self.rID;
        copy.eDegree = [self.eDegree copyWithZone:zone];
    }
    
    return copy;
}


@end
