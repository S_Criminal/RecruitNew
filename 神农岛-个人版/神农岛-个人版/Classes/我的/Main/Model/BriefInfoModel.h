//
//  BriefInfoModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/28.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BriefInfoModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double rType;
@property (nonatomic, strong) NSString *uName;
@property (nonatomic, strong) NSString *uApplyDate;
@property (nonatomic, assign) double uEquimenType;
@property (nonatomic, strong) NSString *rWorkTime;
@property (nonatomic, strong) NSString *uAduitDate;
@property (nonatomic, strong) NSString *pNamep;
@property (nonatomic, strong) NSString *rEmail;
@property (nonatomic, strong) NSString *uNumber;
@property (nonatomic, strong) NSString *uLphone;
@property (nonatomic, strong) NSString *rPhone;
@property (nonatomic, assign) double uResID;
@property (nonatomic, strong) NSString *rNature;
@property (nonatomic, strong) NSString *pName;
@property (nonatomic, strong) NSString *rMajorse;
@property (nonatomic, assign) double uRole;
@property (nonatomic, assign) double rMajors;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, assign) double uID;
@property (nonatomic, strong) NSString *rEducation;
@property (nonatomic, strong) NSString *uUpdateDate;
@property (nonatomic, strong) NSString *uAddress;
@property (nonatomic, assign) double uCadStatus;
@property (nonatomic, assign) double rPayID;
@property (nonatomic, strong) NSString *uArea;
@property (nonatomic, assign) double rID;
@property (nonatomic, assign) double uAge;
@property (nonatomic, strong) NSString *rLifeimg;
@property (nonatomic, strong) NSString *uEquimenNum;
@property (nonatomic, assign) double rJump;
@property (nonatomic, strong) NSString *rHeadp;
@property (nonatomic, assign) double uMerry;
@property (nonatomic, assign) double rTypes;
@property (nonatomic, assign) double uIsNick;
@property (nonatomic, assign) double rEducationID;
@property (nonatomic, strong) NSString *rSelfEimg;
@property (nonatomic, strong) NSString *uNickName;
@property (nonatomic, assign) double rFull;
@property (nonatomic, strong) NSString *rLifeDescription;
@property (nonatomic, strong) NSString *uBirthday;
@property (nonatomic, strong) NSString *rPosition;
@property (nonatomic, strong) NSString *uExtenCode;
@property (nonatomic, strong) NSString *uEmail;
@property (nonatomic, strong) NSString *uHead;
@property (nonatomic, assign) double rStatus;
@property (nonatomic, strong) NSString *rExplain;
@property (nonatomic, strong) NSString *rIndustry;
@property (nonatomic, strong) NSString *rProvince;
@property (nonatomic, strong) NSString *uWechat;
@property (nonatomic, assign) double uEmailVerify;
@property (nonatomic, strong) NSString *uInvitCode;
@property (nonatomic, strong) NSString *rCity;
@property (nonatomic, assign) double rNowState;
@property (nonatomic, strong) NSString *uLoginDate;
@property (nonatomic, strong) NSString *uQQ;
@property (nonatomic, strong) NSString *rSelfEvaluation;
@property (nonatomic, strong) NSString *rPay;
@property (nonatomic, strong) NSString *uCity;
@property (nonatomic, strong) NSString *uSex;
@property (nonatomic, strong) NSString *rDescription;
@property (nonatomic, strong) NSString *rWorkDate;
@property (nonatomic, assign) double rMajor;
@property (nonatomic, strong) NSString *rName;
@property (nonatomic, strong) NSString *uProvince;
@property (nonatomic, strong) NSString *uCardNum;
@property (nonatomic, assign) double uStatus;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
