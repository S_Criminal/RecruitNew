//
//  Datas2.h
//
//  Created by 晨光 宋 on 17/1/2
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Datas2 : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *eBackUpd;
@property (nonatomic, strong) NSString *eMajor;
@property (nonatomic, assign) double eIsent;
@property (nonatomic, strong) NSString *eBackUph;
@property (nonatomic, strong) NSString *eSchool;
@property (nonatomic, strong) NSString *eEndD;
@property (nonatomic, assign) double iDProperty;
@property (nonatomic, strong) NSString *eBackUpe;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *eMajors;
@property (nonatomic, strong) NSString *eRemark;
@property (nonatomic, strong) NSString *eStartD;
@property (nonatomic, strong) NSString *eBackUp;
@property (nonatomic, assign) double uID;
@property (nonatomic, assign) double eDelete;
@property (nonatomic, assign) double rID;
@property (nonatomic, strong) NSString *eDegree;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
