//
//  BriefInfoModel.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/28.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "BriefInfoModel.h"
NSString *const kBaseClassRType = @"R_Type";
NSString *const kBaseClassUName = @"U_Name";
NSString *const kBaseClassUApplyDate = @"U_ApplyDate";
NSString *const kBaseClassUEquimenType = @"U_EquimenType";
NSString *const kBaseClassRWorkTime = @"R_WorkTime";
NSString *const kBaseClassUAduitDate = @"U_AduitDate";
NSString *const kBaseClassPNamep = @"P_Namep";
NSString *const kBaseClassREmail = @"R_Email";
NSString *const kBaseClassUNumber = @"U_Number";
NSString *const kBaseClassULphone = @"U_Lphone";
NSString *const kBaseClassRPhone = @"R_Phone";
NSString *const kBaseClassUResID = @"U_ResID";
NSString *const kBaseClassRNature = @"R_Nature";
NSString *const kBaseClassPName = @"P_Name";
NSString *const kBaseClassRMajorse = @"R_Majorse";
NSString *const kBaseClassURole = @"U_Role";
NSString *const kBaseClassRMajors = @"R_Majors";
NSString *const kBaseClassCreateDate = @"CreateDate";
NSString *const kBaseClassUID = @"UID";
NSString *const kBaseClassREducation = @"R_Education";
NSString *const kBaseClassUUpdateDate = @"U_UpdateDate";
NSString *const kBaseClassUAddress = @"U_Address";
NSString *const kBaseClassUCadStatus = @"U_CadStatus";
NSString *const kBaseClassRPayID = @"R_PayID";
NSString *const kBaseClassUArea = @"U_Area";
NSString *const kBaseClassRID = @"RID";
NSString *const kBaseClassUAge = @"U_Age";
NSString *const kBaseClassRLifeimg = @"R_Lifeimg";
NSString *const kBaseClassUEquimenNum = @"U_EquimenNum";
NSString *const kBaseClassRJump = @"R_Jump";
NSString *const kBaseClassRHeadp = @"R_Headp";
NSString *const kBaseClassUMerry = @"U_Merry";
NSString *const kBaseClassRTypes = @"R_Types";
NSString *const kBaseClassUIsNick = @"U_IsNick";
NSString *const kBaseClassREducationID = @"R_EducationID";
NSString *const kBaseClassRSelfEimg = @"R_SelfEimg";
NSString *const kBaseClassUNickName = @"U_NickName";
NSString *const kBaseClassRFull = @"R_Full";
NSString *const kBaseClassRLifeDescription = @"R_LifeDescription";
NSString *const kBaseClassUBirthday = @"U_Birthday";
NSString *const kBaseClassRPosition = @"R_Position";
NSString *const kBaseClassUExtenCode = @"U_ExtenCode";
NSString *const kBaseClassUEmail = @"U_Email";
NSString *const kBaseClassUHead = @"U_Head";
NSString *const kBaseClassRStatus = @"R_Status";
NSString *const kBaseClassRExplain = @"R_Explain";
NSString *const kBaseClassRIndustry = @"R_Industry";
NSString *const kBaseClassRProvince = @"R_Province";
NSString *const kBaseClassUWechat = @"U_Wechat";
NSString *const kBaseClassUEmailVerify = @"U_EmailVerify";
NSString *const kBaseClassUInvitCode = @"U_InvitCode";
NSString *const kBaseClassRCity = @"R_City";
NSString *const kBaseClassRNowState = @"R_NowState";
NSString *const kBaseClassULoginDate = @"U_LoginDate";
NSString *const kBaseClassUQQ = @"U_QQ";
NSString *const kBaseClassRSelfEvaluation = @"R_SelfEvaluation";
NSString *const kBaseClassRPay = @"R_Pay";
NSString *const kBaseClassUCity = @"U_City";
NSString *const kBaseClassUSex = @"U_Sex";
NSString *const kBaseClassRDescription = @"R_Description";
NSString *const kBaseClassRWorkDate = @"R_WorkDate";
NSString *const kBaseClassRMajor = @"R_Major";
NSString *const kBaseClassRName = @"R_Name";
NSString *const kBaseClassUProvince = @"U_Province";
NSString *const kBaseClassUCardNum = @"U_CardNum";
NSString *const kBaseClassUStatus = @"U_Status";

@interface BriefInfoModel ()
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BriefInfoModel
@synthesize rType = _rType;
@synthesize uName = _uName;
@synthesize uApplyDate = _uApplyDate;
@synthesize uEquimenType = _uEquimenType;
@synthesize rWorkTime = _rWorkTime;
@synthesize uAduitDate = _uAduitDate;
@synthesize pNamep = _pNamep;
@synthesize rEmail = _rEmail;
@synthesize uNumber = _uNumber;
@synthesize uLphone = _uLphone;
@synthesize rPhone = _rPhone;
@synthesize uResID = _uResID;
@synthesize rNature = _rNature;
@synthesize pName = _pName;
@synthesize rMajorse = _rMajorse;
@synthesize uRole = _uRole;
@synthesize rMajors = _rMajors;
@synthesize createDate = _createDate;
@synthesize uID = _uID;
@synthesize rEducation = _rEducation;
@synthesize uUpdateDate = _uUpdateDate;
@synthesize uAddress = _uAddress;
@synthesize uCadStatus = _uCadStatus;
@synthesize rPayID = _rPayID;
@synthesize uArea = _uArea;
@synthesize rID = _rID;
@synthesize uAge = _uAge;
@synthesize rLifeimg = _rLifeimg;
@synthesize uEquimenNum = _uEquimenNum;
@synthesize rJump = _rJump;
@synthesize rHeadp = _rHeadp;
@synthesize uMerry = _uMerry;
@synthesize rTypes = _rTypes;
@synthesize uIsNick = _uIsNick;
@synthesize rEducationID = _rEducationID;
@synthesize rSelfEimg = _rSelfEimg;
@synthesize uNickName = _uNickName;
@synthesize rFull = _rFull;
@synthesize rLifeDescription = _rLifeDescription;
@synthesize uBirthday = _uBirthday;
@synthesize rPosition = _rPosition;
@synthesize uExtenCode = _uExtenCode;
@synthesize uEmail = _uEmail;
@synthesize uHead = _uHead;
@synthesize rStatus = _rStatus;
@synthesize rExplain = _rExplain;
@synthesize rIndustry = _rIndustry;
@synthesize rProvince = _rProvince;
@synthesize uWechat = _uWechat;
@synthesize uEmailVerify = _uEmailVerify;
@synthesize uInvitCode = _uInvitCode;
@synthesize rCity = _rCity;
@synthesize rNowState = _rNowState;
@synthesize uLoginDate = _uLoginDate;
@synthesize uQQ = _uQQ;
@synthesize rSelfEvaluation = _rSelfEvaluation;
@synthesize rPay = _rPay;
@synthesize uCity = _uCity;
@synthesize uSex = _uSex;
@synthesize rDescription = _rDescription;
@synthesize rWorkDate = _rWorkDate;
@synthesize rMajor = _rMajor;
@synthesize rName = _rName;
@synthesize uProvince = _uProvince;
@synthesize uCardNum = _uCardNum;
@synthesize uStatus = _uStatus;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.rType = [[self objectOrNilForKey:kBaseClassRType fromDictionary:dict] doubleValue];
        self.uName = [self objectOrNilForKey:kBaseClassUName fromDictionary:dict];
        self.uApplyDate = [self objectOrNilForKey:kBaseClassUApplyDate fromDictionary:dict];
        self.uEquimenType = [[self objectOrNilForKey:kBaseClassUEquimenType fromDictionary:dict] doubleValue];
        self.rWorkTime = [self objectOrNilForKey:kBaseClassRWorkTime fromDictionary:dict];
        self.uAduitDate = [self objectOrNilForKey:kBaseClassUAduitDate fromDictionary:dict];
        self.pNamep = [self objectOrNilForKey:kBaseClassPNamep fromDictionary:dict];
        self.rEmail = [self objectOrNilForKey:kBaseClassREmail fromDictionary:dict];
        self.uNumber = [self objectOrNilForKey:kBaseClassUNumber fromDictionary:dict];
        self.uLphone = [self objectOrNilForKey:kBaseClassULphone fromDictionary:dict];
        self.rPhone = [self objectOrNilForKey:kBaseClassRPhone fromDictionary:dict];
        self.uResID = [[self objectOrNilForKey:kBaseClassUResID fromDictionary:dict] doubleValue];
        self.rNature = [self objectOrNilForKey:kBaseClassRNature fromDictionary:dict];
        self.pName = [self objectOrNilForKey:kBaseClassPName fromDictionary:dict];
        self.rMajorse = [self objectOrNilForKey:kBaseClassRMajorse fromDictionary:dict];
        self.uRole = [[self objectOrNilForKey:kBaseClassURole fromDictionary:dict] doubleValue];
        self.rMajors = [[self objectOrNilForKey:kBaseClassRMajors fromDictionary:dict] doubleValue];
        self.createDate = [self objectOrNilForKey:kBaseClassCreateDate fromDictionary:dict];
        self.uID = [[self objectOrNilForKey:kBaseClassUID fromDictionary:dict] doubleValue];
        self.rEducation = [self objectOrNilForKey:kBaseClassREducation fromDictionary:dict];
        self.uUpdateDate = [self objectOrNilForKey:kBaseClassUUpdateDate fromDictionary:dict];
        self.uAddress = [self objectOrNilForKey:kBaseClassUAddress fromDictionary:dict];
        self.uCadStatus = [[self objectOrNilForKey:kBaseClassUCadStatus fromDictionary:dict] doubleValue];
        self.rPayID = [[self objectOrNilForKey:kBaseClassRPayID fromDictionary:dict] doubleValue];
        self.uArea = [self objectOrNilForKey:kBaseClassUArea fromDictionary:dict];
        self.rID = [[self objectOrNilForKey:kBaseClassRID fromDictionary:dict] doubleValue];
        self.uAge = [[self objectOrNilForKey:kBaseClassUAge fromDictionary:dict] doubleValue];
        self.rLifeimg = [self objectOrNilForKey:kBaseClassRLifeimg fromDictionary:dict];
        self.uEquimenNum = [self objectOrNilForKey:kBaseClassUEquimenNum fromDictionary:dict];
        self.rJump = [[self objectOrNilForKey:kBaseClassRJump fromDictionary:dict] doubleValue];
        self.rHeadp = [self objectOrNilForKey:kBaseClassRHeadp fromDictionary:dict];
        self.uMerry = [[self objectOrNilForKey:kBaseClassUMerry fromDictionary:dict] doubleValue];
        self.rTypes = [[self objectOrNilForKey:kBaseClassRTypes fromDictionary:dict] doubleValue];
        self.uIsNick = [[self objectOrNilForKey:kBaseClassUIsNick fromDictionary:dict] doubleValue];
        self.rEducationID = [[self objectOrNilForKey:kBaseClassREducationID fromDictionary:dict] doubleValue];
        self.rSelfEimg = [self objectOrNilForKey:kBaseClassRSelfEimg fromDictionary:dict];
        self.uNickName = [self objectOrNilForKey:kBaseClassUNickName fromDictionary:dict];
        self.rFull = [[self objectOrNilForKey:kBaseClassRFull fromDictionary:dict] doubleValue];
        self.rLifeDescription = [self objectOrNilForKey:kBaseClassRLifeDescription fromDictionary:dict];
        self.uBirthday = [self objectOrNilForKey:kBaseClassUBirthday fromDictionary:dict];
        self.rPosition = [self objectOrNilForKey:kBaseClassRPosition fromDictionary:dict];
        self.uExtenCode = [self objectOrNilForKey:kBaseClassUExtenCode fromDictionary:dict];
        self.uEmail = [self objectOrNilForKey:kBaseClassUEmail fromDictionary:dict];
        self.uHead = [self objectOrNilForKey:kBaseClassUHead fromDictionary:dict];
        self.rStatus = [[self objectOrNilForKey:kBaseClassRStatus fromDictionary:dict] doubleValue];
        self.rExplain = [self objectOrNilForKey:kBaseClassRExplain fromDictionary:dict];
        self.rIndustry = [self objectOrNilForKey:kBaseClassRIndustry fromDictionary:dict];
        self.rProvince = [self objectOrNilForKey:kBaseClassRProvince fromDictionary:dict];
        self.uWechat = [self objectOrNilForKey:kBaseClassUWechat fromDictionary:dict];
        self.uEmailVerify = [[self objectOrNilForKey:kBaseClassUEmailVerify fromDictionary:dict] doubleValue];
        self.uInvitCode = [self objectOrNilForKey:kBaseClassUInvitCode fromDictionary:dict];
        self.rCity = [self objectOrNilForKey:kBaseClassRCity fromDictionary:dict];
        self.rNowState = [[self objectOrNilForKey:kBaseClassRNowState fromDictionary:dict] doubleValue];
        self.uLoginDate = [self objectOrNilForKey:kBaseClassULoginDate fromDictionary:dict];
        self.uQQ = [self objectOrNilForKey:kBaseClassUQQ fromDictionary:dict];
        self.rSelfEvaluation = [self objectOrNilForKey:kBaseClassRSelfEvaluation fromDictionary:dict];
        self.rPay = [self objectOrNilForKey:kBaseClassRPay fromDictionary:dict];
        self.uCity = [self objectOrNilForKey:kBaseClassUCity fromDictionary:dict];
        self.uSex = [self objectOrNilForKey:kBaseClassUSex fromDictionary:dict];
        self.rDescription = [self objectOrNilForKey:kBaseClassRDescription fromDictionary:dict];
        self.rWorkDate = [self objectOrNilForKey:kBaseClassRWorkDate fromDictionary:dict];
        self.rMajor = [[self objectOrNilForKey:kBaseClassRMajor fromDictionary:dict] doubleValue];
        self.rName = [self objectOrNilForKey:kBaseClassRName fromDictionary:dict];
        self.uProvince = [self objectOrNilForKey:kBaseClassUProvince fromDictionary:dict];
        self.uCardNum = [self objectOrNilForKey:kBaseClassUCardNum fromDictionary:dict];
        self.uStatus = [[self objectOrNilForKey:kBaseClassUStatus fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rType] forKey:kBaseClassRType];
    [mutableDict setValue:self.uName forKey:kBaseClassUName];
    [mutableDict setValue:self.uApplyDate forKey:kBaseClassUApplyDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uEquimenType] forKey:kBaseClassUEquimenType];
    [mutableDict setValue:self.rWorkTime forKey:kBaseClassRWorkTime];
    [mutableDict setValue:self.uAduitDate forKey:kBaseClassUAduitDate];
    [mutableDict setValue:self.pNamep forKey:kBaseClassPNamep];
    [mutableDict setValue:self.rEmail forKey:kBaseClassREmail];
    [mutableDict setValue:self.uNumber forKey:kBaseClassUNumber];
    [mutableDict setValue:self.uLphone forKey:kBaseClassULphone];
    [mutableDict setValue:self.rPhone forKey:kBaseClassRPhone];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uResID] forKey:kBaseClassUResID];
    [mutableDict setValue:self.rNature forKey:kBaseClassRNature];
    [mutableDict setValue:self.pName forKey:kBaseClassPName];
    [mutableDict setValue:self.rMajorse forKey:kBaseClassRMajorse];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uRole] forKey:kBaseClassURole];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rMajors] forKey:kBaseClassRMajors];
    [mutableDict setValue:self.createDate forKey:kBaseClassCreateDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kBaseClassUID];
    [mutableDict setValue:self.rEducation forKey:kBaseClassREducation];
    [mutableDict setValue:self.uUpdateDate forKey:kBaseClassUUpdateDate];
    [mutableDict setValue:self.uAddress forKey:kBaseClassUAddress];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uCadStatus] forKey:kBaseClassUCadStatus];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rPayID] forKey:kBaseClassRPayID];
    [mutableDict setValue:self.uArea forKey:kBaseClassUArea];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rID] forKey:kBaseClassRID];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uAge] forKey:kBaseClassUAge];
    [mutableDict setValue:self.rLifeimg forKey:kBaseClassRLifeimg];
    [mutableDict setValue:self.uEquimenNum forKey:kBaseClassUEquimenNum];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rJump] forKey:kBaseClassRJump];
    [mutableDict setValue:self.rHeadp forKey:kBaseClassRHeadp];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uMerry] forKey:kBaseClassUMerry];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rTypes] forKey:kBaseClassRTypes];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uIsNick] forKey:kBaseClassUIsNick];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rEducationID] forKey:kBaseClassREducationID];
    [mutableDict setValue:self.rSelfEimg forKey:kBaseClassRSelfEimg];
    [mutableDict setValue:self.uNickName forKey:kBaseClassUNickName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rFull] forKey:kBaseClassRFull];
    [mutableDict setValue:self.rLifeDescription forKey:kBaseClassRLifeDescription];
    [mutableDict setValue:self.uBirthday forKey:kBaseClassUBirthday];
    [mutableDict setValue:self.rPosition forKey:kBaseClassRPosition];
    [mutableDict setValue:self.uExtenCode forKey:kBaseClassUExtenCode];
    [mutableDict setValue:self.uEmail forKey:kBaseClassUEmail];
    [mutableDict setValue:self.uHead forKey:kBaseClassUHead];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rStatus] forKey:kBaseClassRStatus];
    [mutableDict setValue:self.rExplain forKey:kBaseClassRExplain];
    [mutableDict setValue:self.rIndustry forKey:kBaseClassRIndustry];
    [mutableDict setValue:self.rProvince forKey:kBaseClassRProvince];
    [mutableDict setValue:self.uWechat forKey:kBaseClassUWechat];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uEmailVerify] forKey:kBaseClassUEmailVerify];
    [mutableDict setValue:self.uInvitCode forKey:kBaseClassUInvitCode];
    [mutableDict setValue:self.rCity forKey:kBaseClassRCity];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rNowState] forKey:kBaseClassRNowState];
    [mutableDict setValue:self.uLoginDate forKey:kBaseClassULoginDate];
    [mutableDict setValue:self.uQQ forKey:kBaseClassUQQ];
    [mutableDict setValue:self.rSelfEvaluation forKey:kBaseClassRSelfEvaluation];
    [mutableDict setValue:self.rPay forKey:kBaseClassRPay];
    [mutableDict setValue:self.uCity forKey:kBaseClassUCity];
    [mutableDict setValue:self.uSex forKey:kBaseClassUSex];
    [mutableDict setValue:self.rDescription forKey:kBaseClassRDescription];
    [mutableDict setValue:self.rWorkDate forKey:kBaseClassRWorkDate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rMajor] forKey:kBaseClassRMajor];
    [mutableDict setValue:self.rName forKey:kBaseClassRName];
    [mutableDict setValue:self.uProvince forKey:kBaseClassUProvince];
    [mutableDict setValue:self.uCardNum forKey:kBaseClassUCardNum];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uStatus] forKey:kBaseClassUStatus];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.rType = [aDecoder decodeDoubleForKey:kBaseClassRType];
    self.uName = [aDecoder decodeObjectForKey:kBaseClassUName];
    self.uApplyDate = [aDecoder decodeObjectForKey:kBaseClassUApplyDate];
    self.uEquimenType = [aDecoder decodeDoubleForKey:kBaseClassUEquimenType];
    self.rWorkTime = [aDecoder decodeObjectForKey:kBaseClassRWorkTime];
    self.uAduitDate = [aDecoder decodeObjectForKey:kBaseClassUAduitDate];
    self.pNamep = [aDecoder decodeObjectForKey:kBaseClassPNamep];
    self.rEmail = [aDecoder decodeObjectForKey:kBaseClassREmail];
    self.uNumber = [aDecoder decodeObjectForKey:kBaseClassUNumber];
    self.uLphone = [aDecoder decodeObjectForKey:kBaseClassULphone];
    self.rPhone = [aDecoder decodeObjectForKey:kBaseClassRPhone];
    self.uResID = [aDecoder decodeDoubleForKey:kBaseClassUResID];
    self.rNature = [aDecoder decodeObjectForKey:kBaseClassRNature];
    self.pName = [aDecoder decodeObjectForKey:kBaseClassPName];
    self.rMajorse = [aDecoder decodeObjectForKey:kBaseClassRMajorse];
    self.uRole = [aDecoder decodeDoubleForKey:kBaseClassURole];
    self.rMajors = [aDecoder decodeDoubleForKey:kBaseClassRMajors];
    self.createDate = [aDecoder decodeObjectForKey:kBaseClassCreateDate];
    self.uID = [aDecoder decodeDoubleForKey:kBaseClassUID];
    self.rEducation = [aDecoder decodeObjectForKey:kBaseClassREducation];
    self.uUpdateDate = [aDecoder decodeObjectForKey:kBaseClassUUpdateDate];
    self.uAddress = [aDecoder decodeObjectForKey:kBaseClassUAddress];
    self.uCadStatus = [aDecoder decodeDoubleForKey:kBaseClassUCadStatus];
    self.rPayID = [aDecoder decodeDoubleForKey:kBaseClassRPayID];
    self.uArea = [aDecoder decodeObjectForKey:kBaseClassUArea];
    self.rID = [aDecoder decodeDoubleForKey:kBaseClassRID];
    self.uAge = [aDecoder decodeDoubleForKey:kBaseClassUAge];
    self.rLifeimg = [aDecoder decodeObjectForKey:kBaseClassRLifeimg];
    self.uEquimenNum = [aDecoder decodeObjectForKey:kBaseClassUEquimenNum];
    self.rJump = [aDecoder decodeDoubleForKey:kBaseClassRJump];
    self.rHeadp = [aDecoder decodeObjectForKey:kBaseClassRHeadp];
    self.uMerry = [aDecoder decodeDoubleForKey:kBaseClassUMerry];
    self.rTypes = [aDecoder decodeDoubleForKey:kBaseClassRTypes];
    self.uIsNick = [aDecoder decodeDoubleForKey:kBaseClassUIsNick];
    self.rEducationID = [aDecoder decodeDoubleForKey:kBaseClassREducationID];
    self.rSelfEimg = [aDecoder decodeObjectForKey:kBaseClassRSelfEimg];
    self.uNickName = [aDecoder decodeObjectForKey:kBaseClassUNickName];
    self.rFull = [aDecoder decodeDoubleForKey:kBaseClassRFull];
    self.rLifeDescription = [aDecoder decodeObjectForKey:kBaseClassRLifeDescription];
    self.uBirthday = [aDecoder decodeObjectForKey:kBaseClassUBirthday];
    self.rPosition = [aDecoder decodeObjectForKey:kBaseClassRPosition];
    self.uExtenCode = [aDecoder decodeObjectForKey:kBaseClassUExtenCode];
    self.uEmail = [aDecoder decodeObjectForKey:kBaseClassUEmail];
    self.uHead = [aDecoder decodeObjectForKey:kBaseClassUHead];
    self.rStatus = [aDecoder decodeDoubleForKey:kBaseClassRStatus];
    self.rExplain = [aDecoder decodeObjectForKey:kBaseClassRExplain];
    self.rIndustry = [aDecoder decodeObjectForKey:kBaseClassRIndustry];
    self.rProvince = [aDecoder decodeObjectForKey:kBaseClassRProvince];
    self.uWechat = [aDecoder decodeObjectForKey:kBaseClassUWechat];
    self.uEmailVerify = [aDecoder decodeDoubleForKey:kBaseClassUEmailVerify];
    self.uInvitCode = [aDecoder decodeObjectForKey:kBaseClassUInvitCode];
    self.rCity = [aDecoder decodeObjectForKey:kBaseClassRCity];
    self.rNowState = [aDecoder decodeDoubleForKey:kBaseClassRNowState];
    self.uLoginDate = [aDecoder decodeObjectForKey:kBaseClassULoginDate];
    self.uQQ = [aDecoder decodeObjectForKey:kBaseClassUQQ];
    self.rSelfEvaluation = [aDecoder decodeObjectForKey:kBaseClassRSelfEvaluation];
    self.rPay = [aDecoder decodeObjectForKey:kBaseClassRPay];
    self.uCity = [aDecoder decodeObjectForKey:kBaseClassUCity];
    self.uSex = [aDecoder decodeObjectForKey:kBaseClassUSex];
    self.rDescription = [aDecoder decodeObjectForKey:kBaseClassRDescription];
    self.rWorkDate = [aDecoder decodeObjectForKey:kBaseClassRWorkDate];
    self.rMajor = [aDecoder decodeDoubleForKey:kBaseClassRMajor];
    self.rName = [aDecoder decodeObjectForKey:kBaseClassRName];
    self.uProvince = [aDecoder decodeObjectForKey:kBaseClassUProvince];
    self.uCardNum = [aDecoder decodeObjectForKey:kBaseClassUCardNum];
    self.uStatus = [aDecoder decodeDoubleForKey:kBaseClassUStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_rType forKey:kBaseClassRType];
    [aCoder encodeObject:_uName forKey:kBaseClassUName];
    [aCoder encodeObject:_uApplyDate forKey:kBaseClassUApplyDate];
    [aCoder encodeDouble:_uEquimenType forKey:kBaseClassUEquimenType];
    [aCoder encodeObject:_rWorkTime forKey:kBaseClassRWorkTime];
    [aCoder encodeObject:_uAduitDate forKey:kBaseClassUAduitDate];
    [aCoder encodeObject:_pNamep forKey:kBaseClassPNamep];
    [aCoder encodeObject:_rEmail forKey:kBaseClassREmail];
    [aCoder encodeObject:_uNumber forKey:kBaseClassUNumber];
    [aCoder encodeObject:_uLphone forKey:kBaseClassULphone];
    [aCoder encodeObject:_rPhone forKey:kBaseClassRPhone];
    [aCoder encodeDouble:_uResID forKey:kBaseClassUResID];
    [aCoder encodeObject:_rNature forKey:kBaseClassRNature];
    [aCoder encodeObject:_pName forKey:kBaseClassPName];
    [aCoder encodeObject:_rMajorse forKey:kBaseClassRMajorse];
    [aCoder encodeDouble:_uRole forKey:kBaseClassURole];
    [aCoder encodeDouble:_rMajors forKey:kBaseClassRMajors];
    [aCoder encodeObject:_createDate forKey:kBaseClassCreateDate];
    [aCoder encodeDouble:_uID forKey:kBaseClassUID];
    [aCoder encodeObject:_rEducation forKey:kBaseClassREducation];
    [aCoder encodeObject:_uUpdateDate forKey:kBaseClassUUpdateDate];
    [aCoder encodeObject:_uAddress forKey:kBaseClassUAddress];
    [aCoder encodeDouble:_uCadStatus forKey:kBaseClassUCadStatus];
    [aCoder encodeDouble:_rPayID forKey:kBaseClassRPayID];
    [aCoder encodeObject:_uArea forKey:kBaseClassUArea];
    [aCoder encodeDouble:_rID forKey:kBaseClassRID];
    [aCoder encodeDouble:_uAge forKey:kBaseClassUAge];
    [aCoder encodeObject:_rLifeimg forKey:kBaseClassRLifeimg];
    [aCoder encodeObject:_uEquimenNum forKey:kBaseClassUEquimenNum];
    [aCoder encodeDouble:_rJump forKey:kBaseClassRJump];
    [aCoder encodeObject:_rHeadp forKey:kBaseClassRHeadp];
    [aCoder encodeDouble:_uMerry forKey:kBaseClassUMerry];
    [aCoder encodeDouble:_rTypes forKey:kBaseClassRTypes];
    [aCoder encodeDouble:_uIsNick forKey:kBaseClassUIsNick];
    [aCoder encodeDouble:_rEducationID forKey:kBaseClassREducationID];
    [aCoder encodeObject:_rSelfEimg forKey:kBaseClassRSelfEimg];
    [aCoder encodeObject:_uNickName forKey:kBaseClassUNickName];
    [aCoder encodeDouble:_rFull forKey:kBaseClassRFull];
    [aCoder encodeObject:_rLifeDescription forKey:kBaseClassRLifeDescription];
    [aCoder encodeObject:_uBirthday forKey:kBaseClassUBirthday];
    [aCoder encodeObject:_rPosition forKey:kBaseClassRPosition];
    [aCoder encodeObject:_uExtenCode forKey:kBaseClassUExtenCode];
    [aCoder encodeObject:_uEmail forKey:kBaseClassUEmail];
    [aCoder encodeObject:_uHead forKey:kBaseClassUHead];
    [aCoder encodeDouble:_rStatus forKey:kBaseClassRStatus];
    [aCoder encodeObject:_rExplain forKey:kBaseClassRExplain];
    [aCoder encodeObject:_rIndustry forKey:kBaseClassRIndustry];
    [aCoder encodeObject:_rProvince forKey:kBaseClassRProvince];
    [aCoder encodeObject:_uWechat forKey:kBaseClassUWechat];
    [aCoder encodeDouble:_uEmailVerify forKey:kBaseClassUEmailVerify];
    [aCoder encodeObject:_uInvitCode forKey:kBaseClassUInvitCode];
    [aCoder encodeObject:_rCity forKey:kBaseClassRCity];
    [aCoder encodeDouble:_rNowState forKey:kBaseClassRNowState];
    [aCoder encodeObject:_uLoginDate forKey:kBaseClassULoginDate];
    [aCoder encodeObject:_uQQ forKey:kBaseClassUQQ];
    [aCoder encodeObject:_rSelfEvaluation forKey:kBaseClassRSelfEvaluation];
    [aCoder encodeObject:_rPay forKey:kBaseClassRPay];
    [aCoder encodeObject:_uCity forKey:kBaseClassUCity];
    [aCoder encodeObject:_uSex forKey:kBaseClassUSex];
    [aCoder encodeObject:_rDescription forKey:kBaseClassRDescription];
    [aCoder encodeObject:_rWorkDate forKey:kBaseClassRWorkDate];
    [aCoder encodeDouble:_rMajor forKey:kBaseClassRMajor];
    [aCoder encodeObject:_rName forKey:kBaseClassRName];
    [aCoder encodeObject:_uProvince forKey:kBaseClassUProvince];
    [aCoder encodeObject:_uCardNum forKey:kBaseClassUCardNum];
    [aCoder encodeDouble:_uStatus forKey:kBaseClassUStatus];
}

- (id)copyWithZone:(NSZone *)zone {
    BriefInfoModel *copy = [[BriefInfoModel alloc] init];
    
    if (copy) {
        
        copy.rType = self.rType;
        copy.uName = [self.uName copyWithZone:zone];
        copy.uApplyDate = [self.uApplyDate copyWithZone:zone];
        copy.uEquimenType = self.uEquimenType;
        copy.rWorkTime = [self.rWorkTime copyWithZone:zone];
        copy.uAduitDate = [self.uAduitDate copyWithZone:zone];
        copy.pNamep = [self.pNamep copyWithZone:zone];
        copy.rEmail = [self.rEmail copyWithZone:zone];
        copy.uNumber = [self.uNumber copyWithZone:zone];
        copy.uLphone = [self.uLphone copyWithZone:zone];
        copy.rPhone = [self.rPhone copyWithZone:zone];
        copy.uResID = self.uResID;
        copy.rNature = [self.rNature copyWithZone:zone];
        copy.pName = [self.pName copyWithZone:zone];
        copy.rMajorse = [self.rMajorse copyWithZone:zone];
        copy.uRole = self.uRole;
        copy.rMajors = self.rMajors;
        copy.createDate = [self.createDate copyWithZone:zone];
        copy.uID = self.uID;
        copy.rEducation = [self.rEducation copyWithZone:zone];
        copy.uUpdateDate = [self.uUpdateDate copyWithZone:zone];
        copy.uAddress = [self.uAddress copyWithZone:zone];
        copy.uCadStatus = self.uCadStatus;
        copy.rPayID = self.rPayID;
        copy.uArea = [self.uArea copyWithZone:zone];
        copy.rID = self.rID;
        copy.uAge = self.uAge;
        copy.rLifeimg = [self.rLifeimg copyWithZone:zone];
        copy.uEquimenNum = [self.uEquimenNum copyWithZone:zone];
        copy.rJump = self.rJump;
        copy.rHeadp = [self.rHeadp copyWithZone:zone];
        copy.uMerry = self.uMerry;
        copy.rTypes = self.rTypes;
        copy.uIsNick = self.uIsNick;
        copy.rEducationID = self.rEducationID;
        copy.rSelfEimg = [self.rSelfEimg copyWithZone:zone];
        copy.uNickName = [self.uNickName copyWithZone:zone];
        copy.rFull = self.rFull;
        copy.rLifeDescription = [self.rLifeDescription copyWithZone:zone];
        copy.uBirthday = [self.uBirthday copyWithZone:zone];
        copy.rPosition = [self.rPosition copyWithZone:zone];
        copy.uExtenCode = [self.uExtenCode copyWithZone:zone];
        copy.uEmail = [self.uEmail copyWithZone:zone];
        copy.uHead = [self.uHead copyWithZone:zone];
        copy.rStatus = self.rStatus;
        copy.rExplain = [self.rExplain copyWithZone:zone];
        copy.rIndustry = [self.rIndustry copyWithZone:zone];
        copy.rProvince = [self.rProvince copyWithZone:zone];
        copy.uWechat = [self.uWechat copyWithZone:zone];
        copy.uEmailVerify = self.uEmailVerify;
        copy.uInvitCode = [self.uInvitCode copyWithZone:zone];
        copy.rCity = [self.rCity copyWithZone:zone];
        copy.rNowState = self.rNowState;
        copy.uLoginDate = [self.uLoginDate copyWithZone:zone];
        copy.uQQ = [self.uQQ copyWithZone:zone];
        copy.rSelfEvaluation = [self.rSelfEvaluation copyWithZone:zone];
        copy.rPay = [self.rPay copyWithZone:zone];
        copy.uCity = [self.uCity copyWithZone:zone];
        copy.uSex = [self.uSex copyWithZone:zone];
        copy.rDescription = [self.rDescription copyWithZone:zone];
        copy.rWorkDate = [self.rWorkDate copyWithZone:zone];
        copy.rMajor = self.rMajor;
        copy.rName = [self.rName copyWithZone:zone];
        copy.uProvince = [self.uProvince copyWithZone:zone];
        copy.uCardNum = [self.uCardNum copyWithZone:zone];
        copy.uStatus = self.uStatus;
    }
    
    return copy;
}

@end
