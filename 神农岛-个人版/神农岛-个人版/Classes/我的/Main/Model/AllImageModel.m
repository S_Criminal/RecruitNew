//
//  AllImageModel.m
//
//  Created by   on 17/4/7
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "AllImageModel.h"


NSString *const kAllImageModelSDesp = @"S_Desp";
NSString *const kAllImageModelSPath = @"S_Path";
NSString *const kAllImageModelUID = @"U_ID";
NSString *const kAllImageModelSType = @"S_Type";
NSString *const kAllImageModelSClass = @"S_Class";
NSString *const kAllImageModelSSdate = @"S_Sdate";
NSString *const kAllImageModelSTitle = @"S_Title";
NSString *const kAllImageModelSEdate = @"S_Edate";
NSString *const kAllImageModelID = @"ID";
NSString *const kAllImageModelSContents = @"S_Contents";


@interface AllImageModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation AllImageModel

@synthesize sDesp = _sDesp;
@synthesize sPath = _sPath;
@synthesize uID = _uID;
@synthesize sType = _sType;
@synthesize sClass = _sClass;
@synthesize sSdate = _sSdate;
@synthesize sTitle = _sTitle;
@synthesize sEdate = _sEdate;
@synthesize iDProperty = _iDProperty;
@synthesize sContents = _sContents;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.sDesp = [self objectOrNilForKey:kAllImageModelSDesp fromDictionary:dict];
            self.sPath = [self objectOrNilForKey:kAllImageModelSPath fromDictionary:dict];
            self.uID = [[self objectOrNilForKey:kAllImageModelUID fromDictionary:dict] doubleValue];
            self.sType = [self objectOrNilForKey:kAllImageModelSType fromDictionary:dict];
            self.sClass = [[self objectOrNilForKey:kAllImageModelSClass fromDictionary:dict] doubleValue];
            self.sSdate = [self objectOrNilForKey:kAllImageModelSSdate fromDictionary:dict];
            self.sTitle = [self objectOrNilForKey:kAllImageModelSTitle fromDictionary:dict];
            self.sEdate = [self objectOrNilForKey:kAllImageModelSEdate fromDictionary:dict];
            self.iDProperty = [[self objectOrNilForKey:kAllImageModelID fromDictionary:dict] doubleValue];
            self.sContents = [self objectOrNilForKey:kAllImageModelSContents fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.sDesp forKey:kAllImageModelSDesp];
    [mutableDict setValue:self.sPath forKey:kAllImageModelSPath];
    [mutableDict setValue:[NSNumber numberWithDouble:self.uID] forKey:kAllImageModelUID];
    [mutableDict setValue:self.sType forKey:kAllImageModelSType];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sClass] forKey:kAllImageModelSClass];
    [mutableDict setValue:self.sSdate forKey:kAllImageModelSSdate];
    [mutableDict setValue:self.sTitle forKey:kAllImageModelSTitle];
    [mutableDict setValue:self.sEdate forKey:kAllImageModelSEdate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.iDProperty] forKey:kAllImageModelID];
    [mutableDict setValue:self.sContents forKey:kAllImageModelSContents];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}




@end
