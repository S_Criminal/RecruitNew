//
//  WorkExperienceCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/2.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Datas1.h"
@interface WorkExperienceCell : UITableViewCell
@property (nonatomic ,strong) UIView *bgView;
//@property (nonatomic ,strong) MyInfoThreeLineStyleView *styleView;
@property (nonatomic ,strong) Datas1 *model;
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *firstLabel;
@property (nonatomic ,strong) UILabel *midLabel;
@property (nonatomic ,strong) UILabel *lastLabel;

@property (nonatomic ,assign) NSInteger indexRow;

@property (nonatomic ,strong) UIView *line;

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(Datas1 *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(Datas1 *)model;

@end
