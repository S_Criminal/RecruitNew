//
//  MyNoInfoGlobalCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyNoInfoGlobalCell.h"

@implementation MyNoInfoGlobalCell{
    CGFloat viewLeftConstraint;
    CGFloat titleFont;
    CGFloat contentFont;
    CGFloat buttonHeight;
}

#pragma mark 懒加载
- (UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [UIImageView new];
        [_iconImageView setFrame:CGRectMake(viewLeftConstraint, 15, 50, 50)];
        _iconImageView.backgroundColor = [UIColor clearColor];
        _iconImageView.image = [UIImage imageNamed:@""];
        
    }
    return _iconImageView;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, KWIDTH - 10 * 2, 20)];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:F(titleFont)];
        _titleLabel.text = @"了解您";
        _titleLabel.textColor = COLOR_LABELThreeCOLOR;
        [_titleLabel setFrame:CGRectMake(CGRectGetMaxX(_iconImageView.frame) + W(10), CGRectGetMaxY(_iconImageView.frame) + W(15),  20, 20)];
        [_titleLabel sizeToFit];
        _titleLabel.centerY = _iconImageView.centerY;
    }
    return _titleLabel;
}

- (UILabel *)contentLabel{
    if (_contentLabel == nil)
    {
        _contentLabel = [UILabel new];
        _contentLabel.font = [UIFont systemFontOfSize:F(14)];
        _contentLabel.textColor = COLOR_LABELSIXCOLOR;
        _contentLabel.text = @"哈哈";
        [_contentLabel setFrame:CGRectMake(viewLeftConstraint, CGRectGetMaxY(_iconImageView.frame) + W(15), self.width - 50, 20)];
        [_contentLabel sizeToFit];
    }
    return _contentLabel;
}

- (UIButton *)editButton{
    if (_editButton == nil) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setFrame:CGRectMake(viewLeftConstraint, CGRectGetMaxY(_contentLabel.frame) + W(20), self.bgView.width - viewLeftConstraint * 2, W(40))];
        [_editButton setCorner:5];
        _editButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
        _editButton.backgroundColor = COLOR_MAINCOLOR;
        [_editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_editButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editButton;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUI];
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.iconImageView];
        [self.bgView addSubview:self.titleLabel];
        [self.bgView addSubview:self.contentLabel];
        [self.bgView addSubview:self.editButton];
    }
    return self;
}

-(void)setUI
{
    viewLeftConstraint  = 15;
    titleFont           = 15 ;
    contentFont         = 14 ;
}

-(void)btnClick:(UIButton *)sender
{
    
}

-(void)setModel:(MyBriefNoExistModel *)model
{
    _model = model;
    self.iconImageView.image = [UIImage imageNamed:model.icon];
    self.titleLabel.text = _model.title;
    self.contentLabel.text = _model.content;
    [self.editButton setTitle:model.buttonTitle forState:UIControlStateNormal];
    
    [_titleLabel sizeToFit];
    _titleLabel.centerY = _iconImageView.centerY;
    [_contentLabel sizeToFit];
    
    _bgView.height = _editButton.y + _editButton.height + 10;
    
    [self setupAutoHeightWithBottomView:_bgView bottomMargin:0];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
