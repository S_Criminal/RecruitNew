//
//  MyMoreTableViewCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyMoreTableViewCell : UITableViewCell

@property (nonatomic ,strong) UIButton *button;

@property (nonatomic ,strong) UIView *bgView;

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(id)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(id)model;

@end
