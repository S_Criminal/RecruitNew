//
//  MyNoInfoGlobalCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/3.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBriefNoExistModel.h"

@interface MyNoInfoGlobalCell : UITableViewCell

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UILabel *contentLabel;
@property (nonatomic ,strong) UIButton *editButton;
@property (nonatomic ,strong) MyBriefNoExistModel *model;

@end
