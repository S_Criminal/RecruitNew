//
//  MyTitleSectionView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "MyTitleSectionsView.h"

@implementation MyTitleSectionsView
- (UIView *)bgView{
    if (_bgView == nil) {
//        _bgView = [UIView new];
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.width, 5)];
//        [_bgView setFrame:self.frame];
        _bgView.y = self.y + 5;
        _bgView.height = W(44) - 5;
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UIImageView *)angleImage{
    if (_angleImage == nil) {
        _angleImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.width, 5)];
        _angleImage.backgroundColor = [UIColor clearColor];
        _angleImage.image = [UIImage imageNamed:@"topWhiteAngleImage"];
    }
    return _angleImage;
}

- (UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = COLOR_LABELThreeCOLOR;
        _titleLabel.font = [UIFont systemFontOfSize:F(17)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_titleLabel setFrame:CGRectMake(15, -5, KWIDTH - 20, 44 )];
    }
    return _titleLabel;
}

- (UIButton *)editButton{
    if (_editButton == nil) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setImage:[UIImage imageNamed:@"Pencil"] forState:UIControlStateNormal];
        _editButton.frame = CGRectMake(_bgView.width - _bgView.height, - 3, _bgView.height, _bgView.height);
    }
    return _editButton;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
        [_line setFrame:CGRectMake(15, W(43), _bgView.width - 15 * 2, 1)];
    }
    return _line;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.bgView];
        [self addSubview:self.angleImage];
        [self.bgView addSubview:self.titleLabel];
        [self.bgView addSubview:self.editButton];
        [self addSubview:self.line];
    }
    return self;
}

-(CGFloat)setTextToLabel:(NSString *)text
{
    self.titleLabel.text = text;
    [_titleLabel sizeToFit];
    _titleLabel.y = _bgView.y - 5 - 2 ;
    _titleLabel.height = _bgView.height;
    return _bgView.height + _angleImage.height;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
