//
//  MyInfoTwoLineStyleView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/29.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyInfoTwoLineStyleView : UIView
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *firstLabel;
@property (nonatomic ,strong) UILabel *lastLabel;
@property (nonatomic ,strong) UIView *line;


#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(id)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(id)model;

@end
