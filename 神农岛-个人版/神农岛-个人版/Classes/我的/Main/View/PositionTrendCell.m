//
//  PositionTrendCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/29.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "PositionTrendCell.h"

@implementation PositionTrendCell
#pragma mark 懒加载
-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
        [self createViews];
        
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    
}

-(void)createViews
{
    self.bgView.frame = CGRectMake(10, 0, KWIDTH - 20, W(50));
    
    _styleView = [[MyInfoTwoLineStyleView alloc]initWithFrame:CGRectMake(0, 0, _bgView.width, W(62 + 5))];
    _styleView.line.hidden = YES;
    _styleView.height       = [_styleView resetCellWithModel:nil];
    _secondStyleView = [[MyInfoTwoLineStyleView alloc]initWithFrame:CGRectMake(0, _styleView.bottom + W(1),  _bgView.width, W(62))];
    _secondStyleView.height = [_secondStyleView resetCellWithModel:nil];
    
    
    
    [_bgView addSubview:_styleView];
    [_bgView addSubview:_secondStyleView];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.line];
    
    self.bgView.height = W(_secondStyleView.height + _styleView.height + 1 - 5);
    
    _styleView.firstLabel.text = @"期望城市";
    _secondStyleView.firstLabel.text = @"期望职位";
    _styleView.iconImageView.image = [UIImage imageNamed:@"期望城市"];
    _secondStyleView.iconImageView.image = [UIImage imageNamed:@"期望职位"];
    
    [self.line setFrame:CGRectMake(15 , _styleView.bottom, _bgView.width - 15 * 2, 1)];
}


#pragma mark 获取高度
FETCH_CELL_HEIGHT(PositionTrendCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model{
    
    _model = model;
    _styleView.lastLabel.text = model.rCity;
    NSMutableArray *arr = [NSMutableArray array];

    for (BriefInfoModel *models in [GlobalData sharedInstance].GB_UserModel.datas0)
    {
        if (!kStringIsEmpty(models.pName) )
        {
             [arr addObject:models.pName];
        }
    }
    _secondStyleView.lastLabel.text = [arr componentsJoinedByString:@","];
    return self.bgView.bottom;
}
@end
