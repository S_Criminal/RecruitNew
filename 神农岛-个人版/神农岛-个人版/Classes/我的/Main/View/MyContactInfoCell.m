//
//  MyContactInfoCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/2.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyContactInfoCell.h"

@implementation MyContactInfoCell
#pragma mark 懒加载
-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
        [self createViews];
        [self.bgView addSubview:self.line];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    
}

-(void)createViews
{
    self.bgView.frame = CGRectMake(10, 0, KWIDTH - 20, W(50));
    
    _styleView = [[MyInfoTwoLineStyleView alloc]initWithFrame:CGRectMake(0, 0, _bgView.width, W(62 + 5))];
    _styleView.line.hidden = YES;
    _styleView.height       = [_styleView resetCellWithModel:nil];
    _secondStyleView = [[MyInfoTwoLineStyleView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_styleView.frame) + 1,  _bgView.width, W(62))];
    _secondStyleView.height = [_secondStyleView resetCellWithModel:nil];
    
    [_bgView addSubview:_styleView];
    [_bgView addSubview:_secondStyleView];
    [self addSubview:self.bgView];
    
    self.bgView.height = W(_secondStyleView.height + _styleView.height + 1 - 5);
    
    _styleView.iconImageView.image = [UIImage imageNamed:@"联系电话"];
    _styleView.firstLabel.text = @"手机";
    _secondStyleView.iconImageView.image = [UIImage imageNamed:@"邮箱地址"];
    _secondStyleView.firstLabel.text = @"邮箱";
    
    [self.line setFrame:CGRectMake(15 , _styleView.bottom, _bgView.width - 15 * 2, 1)];
    
}


#pragma mark 获取高度
FETCH_CELL_HEIGHT(MyContactInfoCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model{
    _model = model;
    _styleView.lastLabel.text       = [GlobalData sharedInstance].GB_Phone;
    _secondStyleView.lastLabel.text = model.rEmail;
    [_secondStyleView.lastLabel sizeToFit];

    return _bgView.bottom;
}

@end
