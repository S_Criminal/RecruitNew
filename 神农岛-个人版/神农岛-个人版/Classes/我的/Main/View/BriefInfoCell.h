//
//  BriefInfoCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/27.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefInfoModel.h"



@interface BriefInfoCell : UITableViewCell
@property (nonatomic ,strong) UIImageView *headImageView;
@property (nonatomic ,strong) UIImageView *headImageBGView;
@property (nonatomic ,strong) UIControl *control;
@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UILabel *cityL;
@property (nonatomic ,strong) UIImageView *sexImage;
@property (nonatomic ,strong) UIButton *nameB;

@property (nonatomic ,strong) UIButton *editButton;


@property (nonatomic ,strong) BriefInfoModel *model;
@property (nonatomic ,assign) CGFloat cellHeight;

@property (nonatomic, strong) void (^tapImageBlock)();

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(BriefInfoModel *)model;

@end
