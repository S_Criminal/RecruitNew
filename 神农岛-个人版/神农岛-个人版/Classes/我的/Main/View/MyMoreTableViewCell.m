//
//  MyMoreTableViewCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyMoreTableViewCell.h"

@implementation MyMoreTableViewCell


#pragma mark 懒加载
- (UIButton *)button{
    if (_button == nil) {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.tag = 1;
//        [_button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_button setTitle:@"更多" forState:UIControlStateNormal];
        [_button setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
        _button.titleLabel.font = [UIFont systemFontOfSize:F(16)];
        _button.backgroundColor = [UIColor whiteColor];
        [_button setFrame:CGRectMake(10, 5, KWIDTH - 10 * 2, W(44) - 5)];
        _button.userInteractionEnabled = NO;
//        _button.widthHeight = @[@(KWIDTH),@(W(0))];
    }
    return _button;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        [_bgView setFrame:CGRectMake(10, 0, KWIDTH - 10 * 2, W(44))];
    }
    return _bgView;
}

//#pragma mark 获取高度
//FETCH_CELL_HEIGHT(MyMoreTableViewCell)

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.bgView];
        [self addSubview:self.button];
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(MyMoreTableViewCell)

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(id)model{
    return W(44);
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
