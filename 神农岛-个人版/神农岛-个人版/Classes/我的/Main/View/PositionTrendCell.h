//
//  PositionTrendCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/29.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefInfoModel.h"
#import "MyInfoTwoLineStyleView.h"
@interface PositionTrendCell : UITableViewCell

@property (nonatomic ,strong) BriefInfoModel *model;
//@property (nonatomic ,strong) MyInfoTitleLabelView *infoView;
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIView *line;

@property (nonatomic ,strong) MyInfoTwoLineStyleView *styleView;
@property (nonatomic ,strong) MyInfoTwoLineStyleView *secondStyleView;

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(BriefInfoModel *)model;

@end
