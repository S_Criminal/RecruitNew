//
//  MyContactInfoCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/2.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefInfoModel.h"
#import "MyInfoTwoLineStyleView.h"
@interface MyContactInfoCell : UITableViewCell
@property (nonatomic ,strong) BriefInfoModel *model;

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIView *line;

@property (nonatomic ,strong) MyInfoTwoLineStyleView *styleView;
@property (nonatomic ,strong) MyInfoTwoLineStyleView *secondStyleView;

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model;
#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(BriefInfoModel *)model;

@end
