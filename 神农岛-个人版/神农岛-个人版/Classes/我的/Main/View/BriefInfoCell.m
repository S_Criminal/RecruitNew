//
//  BriefInfoCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/27.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "BriefInfoCell.h"


@implementation BriefInfoCell{
    CGFloat imageWidth;
    CGFloat viewLeftConstraint;
    CGFloat nameFont;
    CGFloat cityFont;
    CGFloat constraint;
    CGFloat bgViewToTop;
    CGFloat bgViewHeight;
    CGFloat bgViewHeightReduce;
}

#pragma mark 懒加载
-(UIImageView *)headImageBGView
{
    if (!_headImageBGView) {
        _headImageBGView = [UIImageView new];
        _headImageBGView.image = [UIImage imageNamed:@"HeadImageBGView"];
        [_headImageBGView setFrame:CGRectMake(10, 0, imageWidth, imageWidth)];
        _headImageBGView.userInteractionEnabled = YES;
    }
    return _headImageBGView;
}

-(UIControl *)control
{
    if (!_control) {
        _control = [UIControl new];
        [_control addTarget:self action:@selector(tapImage) forControlEvents:UIControlEventTouchUpInside];
    }
    return _control;
}

- (UIImageView *)headImageView{
    if (_headImageView == nil) {
        _headImageView = [UIImageView new];
        _headImageView.backgroundColor = [UIColor clearColor];
        _headImageView.image = [UIImage imageNamed:@"个人默认头像"];
        [_headImageView setCorner:(imageWidth - constraint * 2) / 2.0f];
        _headImageView.contentMode = UIViewContentModeScaleAspectFill;
        _headImageView.userInteractionEnabled = YES;
    }
    return _headImageView;
}

- (UIView *)bgView{
    if (_bgView == nil) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        [_bgView setCorner:5];
        _bgView.layer.borderWidth = 0.5f;
        _bgView.layer.borderColor = COLOR_LINESCOLOR.CGColor;
        _bgView.userInteractionEnabled = YES;
    }
    return _bgView;
}

- (UIButton *)nameB{
    if (_nameB == nil) {
        _nameB = [UIButton new];
        [_nameB setTitle:@"请登录" forState:UIControlStateNormal];
        _nameB.titleLabel.font = [UIFont systemFontOfSize:nameFont];
    }
    return _nameB;
}

- (UILabel *)cityL{
    if (_cityL == nil) {
        _cityL = [UILabel new];
        _cityL.textColor = [HexStringColor colorWithHexString:@"666666"];
        _cityL.font      = [UIFont systemFontOfSize:cityFont];
    }
    return _cityL;
}

- (UIImageView *)sexImage{
    if (_sexImage == nil) {
        _sexImage = [UIImageView new];
        _sexImage.backgroundColor = [UIColor clearColor];
        _sexImage.image = [UIImage imageNamed:@"新版男"];

    }
    return _sexImage;
}

-(UIButton *)editButton
{
    if (!_editButton) {
        _editButton = [UIButton new];
        [_editButton setImage:[UIImage imageNamed:@"Pencil"] forState:UIControlStateNormal];
    }
    return _editButton;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setUI];
        [self createView];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


-(void)setUI
{
    constraint = 14;
    viewLeftConstraint = MyViewLeftConstraint;
    nameFont           = F(17);
    cityFont           = W(14);
    imageWidth         = 134;
    bgViewHeight       = 110;
    bgViewToTop        = 95;
    if (isIphone5)
    {
        imageWidth         = 110;
        viewLeftConstraint = 10;
        bgViewHeight = 100;
        constraint         = 14;
        bgViewHeightReduce = 10;
        bgViewToTop        = W(95 - 25);
    }else if (isIphone6){
        constraint         = 16;
        imageWidth         = 134;
        bgViewHeightReduce = 20;
    }else if (isIphone6p){
        bgViewHeight = 115;
        bgViewHeightReduce = 25;
    }
}


-(void)createView
{
    [self.bgView addSubview:self.editButton];
    
    [self addSubview:self.bgView];
    [self addSubview:self.headImageBGView];
    [self addSubview:self.nameB];
    [self addSubview:self.sexImage];
    [self addSubview:self.cityL];
    
    [self.headImageBGView addSubview:self.headImageView];
//    [self.control addSubview:];
    
    UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage)];
    [_headImageView addGestureRecognizer:tapImage];
    
}


-(CGFloat)buildConstraint
{
    self.headImageBGView.centerX = KWIDTH / 2.0;
    [self.bgView setFrame:CGRectMake(viewLeftConstraint, bgViewToTop, KWIDTH - viewLeftConstraint * 2, bgViewHeight)];
    [self.editButton setFrame:CGRectMake(_bgView.width - W(50), 0,  W(50), W(50))];
    [self.nameB setFrame:CGRectMake(viewLeftConstraint, CGRectGetMaxY(_headImageBGView.frame) + W(-2), KWIDTH - viewLeftConstraint * 2, nameFont)];
    
    
    NSString *str = kStringIsEmpty([GlobalData sharedInstance].GB_Key) ? @"请重新登录" : @"请完善简历信息";
    self.nameB.tag = kStringIsEmpty([GlobalData sharedInstance].GB_Key) ? 150 : 100;
    
    self.cityL.hidden = NO;
    if (kStringIsEmpty(_model.uName))
    {
        self.bgView.height = bgViewHeight - bgViewHeightReduce;
        [self.nameB setAttributedTitle:[CustomTitleButtonStr setRightImageButtonAttributeTitleWithTitle:str AndTitleImageName:@"" ImageRect:CGRectZero color:@"333333"] forState:UIControlStateNormal];
        self.cityL.hidden = YES;
        self.nameB.y = CGRectGetMaxY(_headImageBGView.frame) + 5;
    }
    
    [self.nameB sizeToFit];
    
    [self.cityL setFrame:CGRectMake(viewLeftConstraint, self.nameB.bottom + W(5), 100, F(cityFont))];
    [self.cityL sizeToFit];
    
    self.nameB.centerX = KWIDTH /2.0f;
    self.cityL.centerX = KWIDTH /2.0f;
    
    [_sexImage setFrame:CGRectMake(self.cityL.right, 0, 13, 14)];
    _sexImage.centerY = self.cityL.centerY;
    
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(constraint);
        make.right.bottom.mas_equalTo(-constraint);
    }];
    self.cellHeight = _bgView.y + _bgView.height + W(10);
    
    
    
    return self.cellHeight;
}


#pragma mark 获取高度
FETCH_CELL_HEIGHT(BriefInfoCell)

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model
{
    
    _model = model;
    
    NSString *headImage = model.uHead;
    NSString *name = [NSString stringWithFormat:@"%@ ",model.uName];
    NSString *provience = [NSString stringWithFormat:@"%@",model.uProvince];
    NSString *city = [NSString stringWithFormat:@"%@",model.uCity];
    NSString *sex  = [NSString stringWithFormat:@"%@",model.uSex];
    NSArray *cityArr = [city componentsSeparatedByString:@","];
    
    if (name.length > 6) name = [name substringToIndex:6];
    
    if (!kStringIsEmpty(sex))
    {
        sex = [NSString stringWithFormat:@"新版%@",sex];
    }else{
        sex = @"新版男";
    }
    
    if (headImage.length > 6)
    {
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:headImage] placeholderImage:[UIImage imageNamed:@"个人默认头像"]];
    }else{
        self.headImageView.image = [UIImage imageNamed:@"个人默认头像"];
    }
    
    [self.nameB setAttributedTitle:[CustomTitleButtonStr setRightImageButtonAttributeTitleWithTitle:name AndTitleImageName:@"二维码" ImageRect:CGRectMake(2, -2, 18, 18) color:@"333333"] forState:UIControlStateNormal];
    //四大直辖市
    if ([provience isEqualToString:cityArr.firstObject]) {
        provience = @"";
    }
    self.cityL.text = [NSString stringWithFormat:@"中国 %@ %@ · ",provience,cityArr.firstObject];
    self.sexImage.image = [UIImage imageNamed:sex];
    
    return [self buildConstraint];
}



-(void)tapImage
{
    self.tapImageBlock();
}



@end
