//
//  MyInfoTwoLineStyleView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/29.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "MyInfoTwoLineStyleView.h"

@implementation MyInfoTwoLineStyleView

{
    CGFloat titleFont;
    CGFloat lineToTop;
    CGFloat contentFont;
}
#pragma mark 懒加载
- (UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [UIImageView new];
        _iconImageView.backgroundColor = [UIColor clearColor];
        _iconImageView.image = [UIImage imageNamed:@"职位偏好"];
        
    }
    return _iconImageView;
}
- (UILabel *)firstLabel{
    if (_firstLabel == nil) {
        _firstLabel = [UILabel new];
        _firstLabel.text = @"期望城市";
        _firstLabel.textColor = COLOR_LABELThreeCOLOR;
        _firstLabel.font = [UIFont systemFontOfSize:F(titleFont)];
    }
    return _firstLabel;
}

- (UILabel *)lastLabel{
    if (_lastLabel == nil)
    {
        _lastLabel = [UILabel new];
        _lastLabel.text = @"北京·上海·吉林";
        _lastLabel.textColor = COLOR_LABELSIXCOLOR;
        _lastLabel.font = [UIFont systemFontOfSize:F(contentFont)];
    }
    return _lastLabel;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        [self createViews];
    }
    return self;
}

-(void)setUI
{
    titleFont = 15;
    contentFont = 12;
    lineToTop = 5;
    if (KWIDTH == 375) {
        
    }
}

-(void)createViews
{
    [self addSubview:self.iconImageView];
    [self addSubview:self.firstLabel];
    [self addSubview:self.lastLabel];
    [self addSubview:self.line];
}

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(id)model
{
    [_iconImageView setFrame:CGRectMake(W(25), W(16), W(30), W(30))];
    [_firstLabel setFrame:CGRectMake(_iconImageView.right + W(25), _iconImageView.top - W(4), self.width - _iconImageView.right - W(25) * 2, F(16))];
    [_lastLabel setFrame:CGRectMake(_firstLabel.left , _firstLabel.bottom + W(10), self.width - _iconImageView.right - W(25) * 2, F(12))];
    
    return _iconImageView.bottom + W(16);
}

@end
