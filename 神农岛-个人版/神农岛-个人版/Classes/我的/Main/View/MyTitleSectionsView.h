//
//  MyTitleSectionView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTitleSectionsView : UIView
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIImageView *angleImage;
@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UIButton *editButton;
@property (nonatomic ,strong) UIView *line;

-(CGFloat)setTextToLabel:(NSString *)text;

@end
