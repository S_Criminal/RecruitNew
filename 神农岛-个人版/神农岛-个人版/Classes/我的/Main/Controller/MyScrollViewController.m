//
//  MyScrollViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/29.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "MyScrollViewController.h"
#import "SettingTableVC.h"

/** view && cell */
#import "BriefInfoCell.h"

/** model */
#import "BriefInfoModel.h"
#import "MyMainManager.h"

#define BGIMAGEHEIGHT 284.5f
@interface MyScrollViewController ()<UIScrollViewDelegate,UIGestureRecognizerDelegate>
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,strong) BriefInfoModel *infoModel;


@end

@implementation MyScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self createScrollView];
    [self setModel];
}

-(void)setNav
{
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.navigationBarHidden = YES;
    
    UIImageView *bgNavImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, BGIMAGEHEIGHT)];
    bgNavImage.image = [UIImage imageNamed:@"MYNavBGIMAGE"];
    [self.view addSubview:bgNavImage];
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"" leftImageName:@"settings" leftBlock:^{
        self.hidesBottomBarWhenPushed = YES;
        SettingTableVC *set = [[SettingTableVC alloc]init];
        [weakSelf.navigationController pushViewController:set animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    } rightImageName:@"share" righBlock:^{
        self.hidesBottomBarWhenPushed = YES;
        
        self.hidesBottomBarWhenPushed = NO;
    }]];
    
}

-(void)createScrollView
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - TarBarHeight)];
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollView];
}

-(void)setModel
{
    MyMainManager *manager = [MyMainManager share];
//    [manager getResumeWithKey:[GlobalData sharedInstance].GB_Key success:^(NSDictionary *dic) {
//        if ([dic[@"code"]isEqualToNumber:@200])
//        {
//            NSArray *arr = dic[@"datas0"];
//            if (arr.count > 0)
//            {
//                NSDictionary *d = [arr objectAtIndex:0];
//                if (!kDictIsEmpty(d)) {
//                    _infoModel = [BriefInfoModel modelObjectWithDictionary:d];
////                    [self.scrollView reloadInputViews];
//                    
//                }
//                
//            }
//        }
//    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
