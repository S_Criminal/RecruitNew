//
//  MyViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "MyViewController.h"

#import "LoginViewController.h"     //登录界面
#import "SettingTableVC.h"          //更多设置
#import "MyAddBriefBasicInfoVC.h"   //我的基本信息
#import "MyEduExperienceListVC.h"   //教育经历列表
#import "MyEduExperienceVC.h"       //填写教育经历信息
#import "PositionTrendVC.h"         //职位偏好
#import "MyWorkExperienceListVC.h"  //工作经历列表
#import "MyWorkExperienceVC.h"      //填写工作经历信息
#import "BriefEditViewController.h" //简历编辑界面
#import "MyCardVC.h"                //身份验证
#import "MyContactInfoVC.h"         //联系信息
#import "MyQRCodeVC.h"              //二维码
#import "LoginViewController.h"     //登录界面
/** view && cell */
#import "BriefInfoCell.h"
#import "EducationCell.h"       //教育经历
#import "PositionTrendCell.h"   //职位偏好
#import "WorkExperienceCell.h"  //工作经历
#import "MyContactInfoCell.h"   //联系方式

#import "MyNoInfoGlobalCell.h"  //尚未填写信息时
#import "MyMoreTableViewCell.h" //更多cell
#import "CustomShareUI.h"       //自定义分享界面

#import "KYAlertView.h"         //自定义弹窗

#import "MyTitleSectionsView.h"  //sectionHeader

/** model */
//#import "BriefInfoModel.h"
//#import "BriefEducationModel.h"

#import "DataModels.h"
#import "MyBriefNoExistModel.h"     //信息不存在时
#import "RequestApi+MyBrief.h"
#import "RequestApi+News.h"


//#import "BaseClass.h"


#define BGIMAGEHEIGHT 284.5f

@interface MyViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,CAAnimationDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) BriefInfoModel *infoModel;
//@property (nonatomic ,strong) BriefEducationModel *educationModel;

@property (nonatomic ,strong) NSMutableArray *datasModelArr;
@property (nonatomic ,strong) NSArray *sectionTitleArr;

@property (nonatomic ,strong) NSMutableArray *educationArr;
@property (nonatomic ,strong) NSMutableArray *experienceArr;

@property (nonatomic ,strong) NSArray *noInfoTitleArray;
@property (nonatomic ,strong) NSArray *noInfoContentArray;
@property (nonatomic ,strong) NSArray *noInfoImageArray;
@property (nonatomic ,strong) NSArray *noInfoImageContentArray;

@property (nonatomic ,strong) NSMutableArray *noBriefArray;
@property (nonatomic ,strong) BaseClass *baseModel;

@property (nonatomic ,strong) KYAlertView *kyAlertView;
@property (nonatomic ,strong) UIView *bgView;

@end

@implementation MyViewController{
    CGFloat sectionHeaderHeight;
}

#pragma mark 懒加载
-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, KHEIGHT - NAVIGATIONBAR_HEIGHT - TarBarHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(NSArray *)noInfoTitleArray
{
    if (!_noInfoTitleArray) {
        _noInfoTitleArray = [NSArray arrayWithObjects:@"完善教育档案",@"完善职位偏好",@"完善工作档案", nil];
    }
    return _noInfoTitleArray;
}

-(NSArray *)noInfoContentArray
{
    if (!_noInfoContentArray) {
        _noInfoContentArray = [NSArray arrayWithObjects:@"完善教育经历，以真才实学吸引HR的目光",@"及时更新职位偏好，让HR更好地了解您",@"及时更新工作经历，让HR轻松找到和联系您", nil];
    }
    return _noInfoContentArray;
}

-(KYAlertView *)kyAlertView
{
    if (!_kyAlertView) {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    return _kyAlertView;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _bgView.backgroundColor = [HexStringColor colorWithHexString:@"efeff4"];
    }
    return _bgView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUI];
    [self setNav];
    //检测各个section是否有数据
//    [self setModel];
    [self.bgView addSubview:self.tableView];
    [self.tableView registerClass:[BriefInfoCell class] forCellReuseIdentifier:@"BriefInfoCell"];
    [self.tableView registerClass:[MyMoreTableViewCell class] forCellReuseIdentifier:@"MyMoreTableViewCell"];
    [self setBasicView];
    NSLog(@"%ld",self.childViewControllers.count);
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self setRefresh];
}

-(void)setUI
{
    sectionHeaderHeight = W(44);
}
//下拉刷新
-(void)setRefresh
{
    [self.tableView.mj_header  beginRefreshing];
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    header.automaticallyChangeAlpha = YES;
    
//    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
//
//    // 隐藏状态
    header.stateLabel.hidden = YES;
    
    self.tableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    
    self.tableView.mj_header = header;
}

-(void)loadNewData
{
    [self setModel];
    [self.tableView.mj_header endRefreshing];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModel];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

}

-(void)setNav
{
    [self.view addSubview:self.bgView];
    self.navigationController.navigationBarHidden = YES;
    
    UIImageView *bgNavImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, W(BGIMAGEHEIGHT))];
    bgNavImage.image = [UIImage imageNamed:@"MYNavBGIMAGE"];
    [self.bgView addSubview:bgNavImage];
    DSWeak;
    [self.bgView addSubview:[BaseNavView initNavTitle:@"" leftImageName:@"settings" leftBlock:^{
        SettingTableVC *set = [[SettingTableVC alloc]init];
        [weakSelf.navigationController pushViewController:set animated:YES];
    } rightImageName:@"share" righBlock:^{
        [weakSelf getBriefComplete];
    }]];
}

#pragma mark 分享配置
-(void)setShare
{
    BriefInfoModel *infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    if (kStringIsEmpty(infoModel.pName))
    {
        [self alertTipSContent:@"请前往完善职位偏好!" leftButtonStr:@"取消" rightButtonStr:@"前往" indexStatus:0];
    }else if(kStringIsEmpty(infoModel.uEmail)){
        [self alertTipSContent:@"请前往完善联系信息!" leftButtonStr:@"取消" rightButtonStr:@"前往" indexStatus:1];
    }else{
        
        NSString *content = [NSString stringWithFormat:@"期望职位:%@\n期望薪资:%@",self.infoModel.pName,self.infoModel.rPay];
        NSString *urlStr = [NSString stringWithFormat:@"%@curiculum-vitae.html?id=%@",shareHttp,[GlobalMethod doubleToString:self.infoModel.uID]];
        NSURL *url = [NSURL URLWithString:urlStr];
        CustomShareUI *share =[[CustomShareUI alloc]initShareUIContent:content title:self.infoModel.uName url:url images:self.infoModel.uHead shareEnum:nil];
        [self.bgView addSubview:share];
    }
    
    
}

#pragma mark 获取简历完整度
-(void)getBriefComplete
{
    DSWeak;
    [RequestApi getCompleteDataWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        NSInteger completeNum = [response[@"datas"] integerValue];
        if (completeNum >= 80)
        {
            [weakSelf setShare];
        }else{
            [MBProgressHUD showError:[NSString stringWithFormat:@"简历完整度%ld%%,请先达到80%%!",completeNum] toView:self.bgView];
        }
    } failure:^(NSString *str) {
            [MBProgressHUD showError:str toView:self.bgView];
    }];
}

-(void)setBasicView
{
    UIButton *addInfoImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addInfoImageButton setFrame:CGRectMake(KWIDTH - W(100), KHEIGHT - TABBAR_HEIGHT - W(100), W(100), W(100))];
    [addInfoImageButton setImage:[UIImage imageNamed:@"MyAddInfoImage"] forState:UIControlStateNormal];
    [addInfoImageButton addTarget:self action:@selector(tapAddInfoBrief:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:addInfoImageButton];
}

#pragma mark 侧滑手势
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
//    if ([self isKindOfClass:[MyViewController class]]) {
//        return NO;
//    }
//    NSLog(@"%ld",self.childViewControllers.count);
    
    if (self.childViewControllers.count == 1) {
        return NO;
    }else{
        
    }
    return YES;
}

-(void)setModel
{
    _noInfoImageArray = [NSArray arrayWithObjects:@"教育档案",@"职位偏好档案",@"工作档案", nil];
    //model 数组
    _datasModelArr = [NSMutableArray array];
    _educationArr  = [NSMutableArray array];
    _experienceArr  = [NSMutableArray array];
    _noBriefArray   = [NSMutableArray array];
    
    [self setNoBriefModel];
    
    
    
    
    /**
     *  datas0  个人基本信息
     *
     *  datas2  教育经历
     *
     */
//    if (kObjectIsEmpty([GlobalData sharedInstance].GB_UserModel)) {
//        
//    }
    
    __block NSString *titleEdu = @"完善教育档案";
    __block NSString *titlePositionTrend = @"完善职位偏好";
    __block NSString *titleExp = @"完善工作档案";
    __block NSString *titlePhoneInfo = @"联系信息";
    
    if (kObjectIsEmpty([GlobalData sharedInstance].GB_UserModel)) {
        //[GlobalData sharedInstance].GB_UserModel = nil;
        //_infoModel = nil;
        //[self.educationArr removeAllObjects];
        //[self.experienceArr removeAllObjects];
    }else{
        
        
        _infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
        NSLog(@"%@",[GlobalData sharedInstance].GB_UserModel);
        NSLog(@"%@",_infoModel);
        NSLog(@"%@",[GlobalData sharedInstance].GB_UserModel.datas0.firstObject);
        NSLog(@"%@",_infoModel.uName);
        for (Datas2 *model2 in [GlobalData sharedInstance].GB_UserModel.datas2)
        {
            [self.educationArr addObject:model2];
        }
        for (Datas1 *model1 in [GlobalData sharedInstance].GB_UserModel.datas1)
        {
            [self.experienceArr addObject:model1];
        }
        if (!kStringIsEmpty(_infoModel.rCity)) titlePositionTrend = @"职位偏好";
        if (!kArrayIsEmpty(self.educationArr)) titleEdu = @"教育档案";
        if (!kArrayIsEmpty(self.experienceArr)) titleExp = @"工作档案";
    }
    
    self.sectionTitleArr = [NSArray arrayWithObjects:titleEdu,titlePositionTrend,titleExp,titlePhoneInfo, nil];
    
    DSWeak;
    
    [RequestApi getResumeWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil success:^(NSDictionary *response) {
        NSDictionary *dic = response;
        if ([dic[@"code"]isEqualToNumber:@200])
        {
            
            NSArray *arr = dic[@"datas0"];
            /*
             *  个人基本信息
             */
            if (arr.count > 0)
            {
                NSDictionary *d = [arr objectAtIndex:0];
                if (!kDictIsEmpty(d))
                {
                    [GlobalData sharedInstance].GB_UserModel = [BaseClass modelObjectWithDictionary:dic];
                    _baseModel = [BaseClass modelObjectWithDictionary:dic];
                    _infoModel = [BriefInfoModel modelObjectWithDictionary:d];
                    [GlobalData sharedInstance].isNick      = [GlobalMethod doubleToString:_infoModel.uIsNick];
                    [GlobalData sharedInstance].nickName    = _infoModel.uNickName;
                    [GlobalData sharedInstance].GB_Phone    = _infoModel.uLphone;
                    [GlobalData sharedInstance].u_Email = _infoModel.uEmail;
                    if (kStringIsEmpty(_infoModel.rPay) || kStringIsEmpty(_infoModel.pName)) {
                        [GlobalData sharedInstance].trend_Complete     = nil;
                    }else{
                        [GlobalData sharedInstance].trend_Complete     = @"YES";
                    }
                    
                    //[self.tableView reloadData];
                    if (!kStringIsEmpty(_infoModel.rCity)) titlePositionTrend = @"职位偏好";
                }else{
                    _infoModel = nil;
                }
                [_datasModelArr addObject:_infoModel];
            }else{
                [GlobalData sharedInstance].GB_UserModel = nil;
            }
            
             /*
              *  教育经历 section 1
              */
            
            NSArray *eduArrray = dic[@"datas2"];
            [weakSelf.educationArr removeAllObjects];
            if (!kArrayIsEmpty(eduArrray)) {
                titleEdu = @"教育经历";
                for (int i = 0; i <eduArrray.count ; i++)
                {
                    NSDictionary *d = eduArrray[i];
                    Datas2 *model = [Datas2 modelObjectWithDictionary:d];
                    [weakSelf.educationArr addObject:model];
                }
            }
            
            /*
             *  职位偏好 section 2
             */
            
            /*
             *  工作经历 section 3
             */
            NSArray *workExpArr = dic[@"datas1"];
            [weakSelf.experienceArr removeAllObjects];
            if (!kArrayIsEmpty(workExpArr)) {
                titleExp = @"工作经历";
                for (int i = 0; i < workExpArr.count; i++)
                {
                    NSDictionary *d = workExpArr[i];
                    Datas1 *model = [Datas1 modelObjectWithDictionary:d];
                    [_experienceArr addObject:model];
                }
            }
            weakSelf.sectionTitleArr = [NSArray arrayWithObjects:titleEdu,titlePositionTrend,titleExp,titlePhoneInfo, nil];
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSString *errorStr, id mark) {
        [weakSelf.tableView reloadData];
        if (kStringIsEmpty([GlobalData sharedInstance].GB_Key))
        {
            [weakSelf.kyAlertView showAlertView:@"提示" message:@"未登录状态，请登录！" subBottonTitle:@"取消" cancelButtonTitle:@"登录" handler:^(AlertViewClickBottonType bottonType) {
                if (bottonType == AlertViewClickBottonTypeCancelButton)
                {
                    LoginViewController *login = [LoginViewController new];
                    [GB_Nav pushViewController:login animated:YES];
                }
            }];
        }
    }];
    
    [RequestApi getDateWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        NSDictionary *dic = arr.firstObject;
        [GlobalData sharedInstance].cardPath = dic[@"U_CardPath"];
        [GlobalData sharedInstance].cardStauts = dic[@"U_CadStatus"];
        NSLog(@"%@", [GlobalData sharedInstance].cardPath);
    }];
    
}

-(void)setNoBriefModel
{
    [_noBriefArray addObject:@""];
    [self setNoBriefArray:_educationArr Title:@"展示您的真才实学" content:@"完善教育经历，以真才实学吸引HR的目光" icon:@"教育档案" buttonTitle:@"添加教育经历"];
    [self setNoBriefArray:_educationArr Title:@"让HR更好地了解您" content:@"及时更新职位偏好，让HR更好地了解您" icon:@"职位偏好档案" buttonTitle:@"添加职位偏好"];
    [self setNoBriefArray:_experienceArr Title:@"让用人经理找到您" content:@"及时更新工作经历，让HR轻松找到和您联系" icon:@"工作档案" buttonTitle:@"添加工作经历"];
}

-(void)setNoBriefArray:(NSMutableArray *)array Title:(NSString *)title content:(NSString *)content icon:(NSString *)icon buttonTitle:(NSString *)butTitle
{
    if (kArrayIsEmpty(array)) {
        MyBriefNoExistModel *model = [[MyBriefNoExistModel alloc]init];
        model.title     = title;
        model.content   = content;
        model.icon      = icon;
        model.buttonTitle= butTitle;
        [_noBriefArray addObject:model];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        if (kArrayIsEmpty(_educationArr)) {
            return 1;
        }
        if (_educationArr.count > 2) {
            return 3;
        }
        return _educationArr.count;
    }else if (section == 2){
        return 1;
    }else if (section == 3){
        if (kArrayIsEmpty(_experienceArr)) {
            return 1;
        }
        if (_experienceArr.count > 2) {
            return 3;
        }
        return _experienceArr.count;
    }else if (section == 4){
        return 1;
    }
    return 1;
}
//设置tableview的sectionHeader
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, sectionHeaderHeight)];
    
    MyTitleSectionsView *sectionView = [[MyTitleSectionsView alloc]initWithFrame:CGRectMake(MyViewLeftConstraint, 0, KWIDTH - MyViewLeftConstraint * 2, sectionHeaderHeight)];
    
    sectionView.editButton.tag = section + 10;
    
    [sectionView.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
    if (section > 0)
    {
        if (section - 1 > self.sectionTitleArr.count)
        {
            return nil;
        }
        NSString *title = self.sectionTitleArr[section - 1];
        if ([title containsString:@"完善"])
        {
            sectionView.titleLabel.font = [UIFont systemFontOfSize:F(14)];
            sectionView.editButton.hidden = YES;    //隐藏headerView右侧的编辑按钮
        }
        sectionHeaderHeight = [sectionView setTextToLabel:self.sectionTitleArr[section - 1]];
        sectionView.height = sectionHeaderHeight;
    }
    
    if ((section == 1 && kArrayIsEmpty(_educationArr)) || (section == 2 && kStringIsEmpty(_infoModel.rCity)) || (section == 3 && kArrayIsEmpty(_experienceArr)))
    {
        sectionView.angleImage.image = [UIImage imageNamed:@"topGreenAngleImage"];
    }
    
    view.hidden = section == 0 ? YES : NO;
    
    [view addSubview:sectionView];
    
    return view;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *clearView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 15)];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(MyViewLeftConstraint, 0, KWIDTH - MyViewLeftConstraint * 2, 15)];
    imageView.image = [UIImage imageNamed:@"bottomWhiteAngleImage"];
    clearView.backgroundColor = [UIColor clearColor];
    [clearView addSubview:imageView];
    clearView.hidden = section == 0 ? YES : NO;
    return clearView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0.001;
    }
    return sectionHeaderHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0.001;
    }
    return 15;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h;
    if (indexPath.section == 0) {
        h = [BriefInfoCell fetchHeight:_infoModel];
    }else if (indexPath.section == 1){
        if (!kArrayIsEmpty(_educationArr)) {
            if (indexPath.row == 2) {
                return W(44);
            }
            h = _educationArr.count > 1 && indexPath.row == 0 ? [EducationCell fetchHeight:_educationArr[indexPath.row]] : [EducationCell fetchHeight:_educationArr[indexPath.row]];
            h = indexPath.row == 0 && _educationArr.count == 0 ? h - 5 : h;
            h = indexPath.row == 0 && _educationArr.count == 1 ? h - 5 : h;
            h = indexPath.row == 1 && _educationArr.count == 2 ? h - 5 : h;
            
        }else{
            h = [self.tableView cellHeightForIndexPath:indexPath model:_noBriefArray[indexPath.section] keyPath:@"model" cellClass:[MyNoInfoGlobalCell class] contentViewWidth:KWIDTH];
        }
    }else if (indexPath.section == 2){
        if (!kStringIsEmpty(_infoModel.rCity)) {
            h = [PositionTrendCell fetchHeight:_infoModel];
        }else{
            h = [self.tableView cellHeightForIndexPath:indexPath model:_noBriefArray[indexPath.section] keyPath:@"model" cellClass:[MyNoInfoGlobalCell class] contentViewWidth:KWIDTH];
        }
    }else if(indexPath.section == 3){
        if (!kArrayIsEmpty(_experienceArr)) {
            if (indexPath.row == 2) {
                return W(44);
            }
            h = _experienceArr.count > 1 && indexPath.row == 0 ? [WorkExperienceCell fetchHeight:_experienceArr[indexPath.row]]  : [WorkExperienceCell fetchHeight:_experienceArr[indexPath.row]];
            h = indexPath.row == 0 && _experienceArr.count == 0 ? h - 5 : h;
            h = indexPath.row == 0 && _experienceArr.count == 1 ? h - 5 : h;
            h = indexPath.row == 1 && _experienceArr.count == 2 ? h - 5 : h;
        }else{
            h = [self.tableView cellHeightForIndexPath:indexPath model:_noBriefArray[indexPath.section] keyPath:@"model" cellClass:[MyNoInfoGlobalCell class] contentViewWidth:KWIDTH];
        }
    }else if(indexPath.section == 4){
        h = [MyContactInfoCell fetchHeight:_infoModel];
    }
    NSLog(@"%ld  %ld height %f",indexPath.section,indexPath.row,h);
    return h;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BriefInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BriefInfoCell"];
    if (!cell)
    {
        cell = [[BriefInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BriefInfoCell"];
    }
    if (indexPath.section == 0)
    {
        [cell resetCellWithModel:_infoModel];
        DSWeak;
        cell.tapImageBlock = ^(){
            [weakSelf tapImage];
        };
        [cell.nameB addTarget:self action:@selector(tapQRCode:) forControlEvents:UIControlEventTouchUpInside];
        [cell.editButton addTarget:self action:@selector(tapNextInfo:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else if (indexPath.section == 1){
        NSLog(@"%@",_educationArr);
        if (indexPath.row == 2) {
            MyMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyMoreTableViewCell"];
           [cell resetCellWithModel:nil];
            return cell;
        }
        if (!kArrayIsEmpty(_educationArr))
        {
            EducationCell *cell = [[EducationCell alloc]init];
            cell.line.hidden = ((indexPath.row == 0 && _educationArr.count == 1) || (indexPath.row == 1 && _educationArr.count == 2)) ? YES : NO;
            [cell resetCellWithModel:_educationArr[indexPath.row]];
            if (_educationArr.count > 2 && indexPath.row == 1) {
                cell.line.x = 0;
                cell.line.width = KWIDTH - MyViewLeftConstraint * 2;
            }
            cell.bgView.height = (_educationArr.count == 1 && indexPath.row == 0) || (_educationArr.count == 2 && indexPath.row == 1) ? cell.bgView.height - 5 : cell.bgView.height;
            return cell;
        }else{
            MyNoInfoGlobalCell *cell = [[MyNoInfoGlobalCell alloc]init];
            cell.editButton.tag = 20 + indexPath.section;
            [cell.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
            cell.model = _noBriefArray[indexPath.section];
            return cell;
        }
    }else if (indexPath.section == 2)
    {
        if (kStringIsEmpty(_infoModel.rCity)) {
            MyNoInfoGlobalCell *cell = [[MyNoInfoGlobalCell alloc]init];
            cell.editButton.tag = 20 + indexPath.section;
            [cell.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
            cell.model = _noBriefArray[indexPath.section];
            return cell;
        }else{
            PositionTrendCell *cell = [[PositionTrendCell alloc]init];
            [cell resetCellWithModel:_infoModel];
            return cell;
        }
    }else if (indexPath.section == 3){
        if (indexPath.row == 2) {
            MyMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyMoreTableViewCell"];
            [cell resetCellWithModel:nil];
            return cell;
        }
        if (!kArrayIsEmpty(_experienceArr)) {
            WorkExperienceCell *cell = [[WorkExperienceCell alloc]init];
            cell.line.hidden = ((indexPath.row == 0 && _experienceArr.count == 1) || (indexPath.row == 1 && _experienceArr.count == 2)) ? YES : NO;
            [cell resetCellWithModel:_experienceArr[indexPath.row]];
            if (_experienceArr.count > 2 && indexPath.row == 1) {
                cell.line.x = 0;
                cell.line.width = KWIDTH - MyViewLeftConstraint * 2;
            }
            cell.bgView.height = (_experienceArr.count == 1 && indexPath.row == 0) || (_experienceArr.count == 2 && indexPath.row == 1) ? cell.bgView.height - 5 : cell.bgView.height;
            return cell;
        }else{
            MyNoInfoGlobalCell *cell = [[MyNoInfoGlobalCell alloc]init];
            cell.editButton.tag = 20 + indexPath.section;
            [cell.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
            cell.model = _noBriefArray[indexPath.section];
            return cell;
        }
    }else if(indexPath.section == 4){
        MyContactInfoCell *cell = [[MyContactInfoCell alloc]init];
        [cell resetCellWithModel:_infoModel];
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 2)
    {
        MyEduExperienceListVC *edu = [[MyEduExperienceListVC alloc]init];
        [self.navigationController pushViewController:edu animated:YES];
    }else if (indexPath.section == 3 && indexPath.row == 2)
    {
        MyWorkExperienceListVC *exp = [[MyWorkExperienceListVC alloc]init];
        [self.navigationController pushViewController:exp animated:YES];
    }
}

-(void)tapImage
{
    MyAddBriefBasicInfoVC *addInfo = [[MyAddBriefBasicInfoVC alloc]init];
    [self presentViewController:addInfo animated:YES completion:nil];
    [addInfo tapImage];
}

#pragma mark 点击基本信息
-(void)tapQRCode:(UIButton *)sender
{
    if (sender.tag != 150)
    {
        if (!kStringIsEmpty(_infoModel.uName)) {
            MyQRCodeVC *qrCode = [MyQRCodeVC new];
            [self presentViewController:qrCode animated:YES completion:nil];
        }else{
            if (sender.tag == 100) {
                MyAddBriefBasicInfoVC * vc = [[MyAddBriefBasicInfoVC alloc]init];
                [self presentViewController:vc animated:YES completion:nil];
            }else{
                [self showKYAlertView];
            }
        }
    }else{
        LoginViewController *login = [LoginViewController new];
        [self.navigationController pushViewController:login animated:YES];
    }
}

-(void)tapNextInfo:(UIButton *)sender
{
    MyAddBriefBasicInfoVC *addInfo = [[MyAddBriefBasicInfoVC alloc]init];
    [self presentViewController:addInfo animated:YES completion:nil];
}

//跳转到简历编辑界面 (转场动画)
-(void)tapAddInfoBrief:(UIButton *)sender
{
    BriefEditViewController *vc = [[BriefEditViewController alloc]init];
    /*
    CATransition *transition = [CATransition animation];
    transition.duration = UIViewKeyframeAnimationOptionLayoutSubviews;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFromRight;
    transition.delegate = self;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    */
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)tapEdit:(UIButton *)sender
{
    if (kStringIsEmpty(_infoModel.uName) && sender.tag != 10) {
        [self showKYAlertView];
    }else  if (sender.tag == 11){
        MyEduExperienceListVC *edu = [[MyEduExperienceListVC alloc]init];
        [self.navigationController pushViewController:edu animated:YES];
    }else if (sender.tag == 12){
        PositionTrendVC *trend     = [[PositionTrendVC alloc]init];
        [self.navigationController pushViewController:trend animated:YES];
    }else if (sender.tag == 13){
        MyWorkExperienceListVC *exp = [[MyWorkExperienceListVC alloc]init];
        [self.navigationController pushViewController:exp animated:YES];
    }else if (sender.tag == 14){
        MyContactInfoVC *info = [MyContactInfoVC new];
        [self presentViewController:info animated:YES completion:nil];
    }else if (sender.tag == 21){
        MyEduExperienceVC *eduInfo  = [[MyEduExperienceVC alloc]init];
        eduInfo.editID = @"0";
        eduInfo.edit = @"add";
        [self presentViewController:eduInfo animated:YES completion:nil];
    }else if (sender.tag == 22){
        PositionTrendVC *trend     = [[PositionTrendVC alloc]init];
        [self.navigationController pushViewController:trend animated:YES];
    }else if (sender.tag == 23){
        MyWorkExperienceVC *expInfo = [[MyWorkExperienceVC alloc]init];
        expInfo.edit = @"add";
        expInfo.editID = @"0";
        [self presentViewController:expInfo animated:YES completion:nil];
    }
}

#pragma mark 弹窗 请先完善基本信息

-(void)showKYAlertView
{
    if (kStringIsEmpty([GlobalData sharedInstance].GB_Key))
    {
        [self.kyAlertView showAlertView:@"提示" message:@"您还未登录，无法进行此操作！" subBottonTitle:@"取消" cancelButtonTitle:@"登录" handler:^(AlertViewClickBottonType bottonType) {
            if (bottonType == AlertViewClickBottonTypeCancelButton)
            {
                LoginViewController * vc = [[LoginViewController alloc]init];
                [GB_Nav pushViewController:vc animated:YES];
            }
        }];
    }else{
        [self.kyAlertView showAlertView:@"提示" message:@"请先完善基本信息" subBottonTitle:@"取消" cancelButtonTitle:@"前往" handler:^(AlertViewClickBottonType bottonType) {
            if (bottonType == AlertViewClickBottonTypeCancelButton)
            {
                MyAddBriefBasicInfoVC * vc = [[MyAddBriefBasicInfoVC alloc]init];
                [self presentViewController:vc animated:YES completion:nil];
            }
        }];
    }
}

//温馨提示
-(void)alertTipSContent:(NSString *)content leftButtonStr:(NSString *)leftStr rightButtonStr:(NSString *)rightStr indexStatus:(NSInteger)status
{
    if (!_kyAlertView)
    {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    
    DSWeak;
    
    [_kyAlertView showAlertView:@"提示"
                        message:content
                 subBottonTitle:leftStr
              cancelButtonTitle:rightStr
                        handler:^(AlertViewClickBottonType bottonType)
     {
         if (bottonType == AlertViewClickBottonTypeSubBotton) {
             
         }else if (bottonType == AlertViewClickBottonTypeCancelButton)
         {
            if (status == 0){
                 PositionTrendVC *trend = [PositionTrendVC new];
                 [GB_Nav pushViewController:trend animated:YES];
             }else if (status == 1){
                 MyContactInfoVC *infoVC = [MyContactInfoVC new];
                 [GB_Nav presentViewController:infoVC animated:YES completion:nil];
             }
         }
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
