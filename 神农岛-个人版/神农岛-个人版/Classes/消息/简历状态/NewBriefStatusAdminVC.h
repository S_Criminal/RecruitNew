//
//  NewBriefStatusAdminVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/18.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBriefStatusAdminVC : UIViewController

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSString *status;
-(instancetype)initStatus:(NSString *)status;
@end
