//
//  BriefStatusViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/18.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefStatusViewController.h"
#import "SliderView.h"
#import "NewBriefStatusAdminVC.h"
#import "RequestApi+News.h"

@interface BriefStatusViewController ()<UIScrollViewDelegate,SliderViewDelegate>
@property (nonatomic ,strong) SliderView *sliderView;
@property (nonatomic ,strong) UIScrollView *scrollView;
@end

@implementation BriefStatusViewController

- (SliderView *)sliderView{
    if (_sliderView == nil) {
        _sliderView = [[SliderView alloc]initWithModels:@[[ModelBtn modelWithTitle:@"新简历" imageName:@"" tag:0],[ModelBtn modelWithTitle:@"已查看" imageName:@"" tag:1],[ModelBtn modelWithTitle:@"不合适" imageName:@"" tag:2]] frame:CGRectMake(0, NAVIGATIONBAR_HEIGHT + 1, KWIDTH, W(44)) isHasSlider:true isImageLeft:false isFirstOn:true delegate:self];
        _sliderView.backgroundColor = [UIColor whiteColor];
    }
    return _sliderView;
}

- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.sliderView.bottom +1, KWIDTH, KHEIGHT - self.sliderView.bottom -1)];
        _scrollView.contentSize = CGSizeMake(KWIDTH * 3, 0);
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = true;
        _scrollView.showsVerticalScrollIndicator = false;
        _scrollView.showsHorizontalScrollIndicator = false;
    }
    return _scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    
    [self setupChildVC];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"简历状态" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

#pragma mark 初始化自控制器
- (void)setupChildVC
{
    
    [self.view addSubview:self.sliderView];
    [self.view addSubview:self.scrollView];
    
    CGFloat top = W(10);
    
    [self.scrollView addSubview:[GlobalMethod addNavLineFrame:CGRectMake(0, 0, KWIDTH * 3, top)]];
    
    NewBriefStatusAdminVC *isNew = [[NewBriefStatusAdminVC alloc]initStatus:@"1"];
    isNew.view.frame  = CGRectMake(0, top, KWIDTH, self.scrollView.height - top);
    isNew.tableView.frame = CGRectMake(0, 0, KWIDTH, self.scrollView.height - top);
    
    NewBriefStatusAdminVC *isSee = [[NewBriefStatusAdminVC alloc]initStatus:@"2"];
    isSee.view.frame  = CGRectMake(KWIDTH, top, KWIDTH, self.scrollView.height - top);
    isSee.tableView.frame = CGRectMake(0, 0, KWIDTH, self.scrollView.height - top);
    
    NewBriefStatusAdminVC *isNoSuit = [[NewBriefStatusAdminVC alloc]initStatus:@"3"];
    isNoSuit.view.frame  = CGRectMake(KWIDTH * 2, top, KWIDTH, self.scrollView.height - top);
    isNoSuit.tableView.frame = CGRectMake(0, 0, KWIDTH, self.scrollView.height - top);
    
//    isNew.status = @"1";
//    isSee.status = @"2";
//    isNoSuit.status = @"3";
    
    [self addChildViewController:isNew];
    [self addChildViewController:isSee];
    [self addChildViewController:isNoSuit];
     
    [self.scrollView addSubview:isNew.view];
    [self.scrollView addSubview:isSee.view];
    [self.scrollView addSubview:isNoSuit.view];
}


#pragma mark scrollview delegat
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self fetchCurrentView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self fetchCurrentView];
    }
}

- (void)fetchCurrentView {
    // 获取已经滚动的比例
    double ratio = self.scrollView.contentOffset.x / KWIDTH;
    int    page  = (int)(ratio + 0.5);
    // scrollview 到page页时 将toolbar调至对应按钮
   
    [self.sliderView sliderToIndex:page noticeDelegate:NO];
}

#pragma mark slider delegate
- (void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(CustomSliderControl *)control{
    [UIView animateWithDuration:0.5 animations:^{
        self.scrollView.contentOffset = CGPointMake(KWIDTH * tag, 0);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
