//
//  NewBriefStatusAdminVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/18.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "NewBriefStatusAdminVC.h"
#import "SaveCell.h"

#import "RequestApi+News.h"
//#import "PositionModel.h"

#import "BriefStatusModel.h"
#import "DeliveryRecordVC.h"    //投递记录

@interface NewBriefStatusAdminVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSMutableArray *listArr;

@property (nonatomic ,strong) NSString *lastID;
@end

@implementation NewBriefStatusAdminVC
{
    CGFloat _pageIndex;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH,self.view.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

-(instancetype)initStatus:(NSString *)status
{
    NewBriefStatusAdminVC *brief = [NewBriefStatusAdminVC new];
    brief.status = status;
    return brief;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createViews];
    [self setRefresh];
    _pageIndex = 1;
    [self setModelPageIndex:_pageIndex];
}

-(void)createViews
{
    [self.tableView registerNib:[UINib nibWithNibName:@"SaveCell" bundle:nil] forCellReuseIdentifier:@"SaveCell"];
    [self.view addSubview:self.tableView];
    
}

#pragma mark 刷新 加载更多
-(void)setRefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
    //    [self.firstTableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    //    [header beginRefreshing];
    
    // 设置header
    self.tableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    //    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    //    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    _pageIndex = 1;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
}


-(void)setModelPageIndex:(NSInteger)pageIndex
{
    self.lastID = pageIndex == 1 ? @"0" : self.lastID;
    DSWeak;
    self.listArr = pageIndex == 1 ? [NSMutableArray array] : self.listArr;
    self.status = kStringIsEmpty(self.status) ? @"" : self.status;
    
    [RequestApi getarlistWithKey:[GlobalData sharedInstance].GB_Key pageIndex:pageIndex status:self.status top:@"20" lastId:self.lastID Delegate:nil Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr)
        {
            BriefStatusModel *model = [BriefStatusModel modelObjectWithDictionary:dic];
            [weakSelf.listArr addObject:model];
        }
        if (arr.count > 0) {
            BriefStatusModel *model = _listArr.lastObject;
            weakSelf.lastID = [NSString stringWithFormat:@"%@",[GlobalMethod doubleToString:model.iDProperty]];
            NSLog(@"%@",weakSelf.lastID);
//            [GlobalMethod removeAllSubView:weakSelf.view withTag:1100];
        }else{
            if (pageIndex == 1)
            {
                [weakSelf addNoDateView];
            }else{
                [MBProgressHUD showError:@"没有更多数据" toView:weakSelf.view];
            }
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSString *str) {
        [weakSelf addNoDateView];
    }];
}

-(void)addNoDateView
{
    [self.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, 0, KWIDTH, self.view.height)]];
    self.tableView.hidden = YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h;
    if (_listArr.count > 0)
    {
        h = [tableView cellHeightForIndexPath:indexPath model:_listArr[indexPath.row] keyPath:@"model" cellClass:[SaveCell class] contentViewWidth:KWIDTH];
    }
    return h;
}

-(SaveCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SaveCell"];
    if (_listArr.count > 0)
    {
        cell.model = _listArr[indexPath.row];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeliveryRecordVC *deliver = [[DeliveryRecordVC alloc]initModel:_listArr[indexPath.row]];
    [GB_Nav pushViewController:deliver animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
