//
//  WhoSeeMeVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "WhoSeeMeVC.h"
#import "NewsManager.h"
#import "PositionCoreData+CoreDataClass.h"
#import "seeCell.h"
#import "MJRefresh.h"

#import "CompanyInfoVC.h"       //公司详情

#import "RequestApi+News.h"
#import "NewSeeCell.h"

@interface WhoSeeMeVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NewsManager *manager;
@property (nonatomic ,strong) NSArray *listArr;

@property (nonatomic ,strong) NSManagedObjectContext * context;

@end

@implementation WhoSeeMeVC{
    NSInteger _pageIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _manager = [NewsManager share];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIApplication *app = [UIApplication sharedApplication];
    id delegate = app.delegate;
    self.context = [delegate managedObjectContext];
    
    _pageIndex = 1;
    
    [self setNav];
    [self setBasicView];
    [self setModelPageIndex:_pageIndex];
    [self setFefresh];
    [self changeNoReadNumber];
}

-(void)setNav
{
    [self.view addSubview:[GlobalMethod addNavLine]];
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"谁看过我" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }]];
}

-(void)setBasicView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"seeCell" bundle:nil] forCellReuseIdentifier:@"seeCell"];
    [self.tableView registerClass:[NewSeeCell class] forCellReuseIdentifier:@"NewSeeCell"];
}

#pragma mark 改变未读条数

-(void)changeNoReadNumber
{
    [RequestApi changeNoReadNumberWithWHOSeeMeWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil Success:^(NSDictionary *response) {
        
    } failure:^(NSString *errorStr) {
        
    }];
}

#pragma mark 刷新 加载更多
-(void)setFefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
    [self.tableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    // 设置header
    self.tableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    self.tableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    _pageIndex = 1;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
    //    [self.firstTableView.mj_footer endRefreshingWithNoMoreData];
}

-(void)setModelPageIndex:(NSInteger)pageIndex
{
    DSWeak;
    [_manager GetSeeRecordListWithKey:[GlobalData sharedInstance].GB_Key top:@"10" pageIndex:pageIndex success:^(NSDictionary *dic) {
        if ([dic[@"code"] isEqualToNumber:RESPONSE_CODE_200])
        {
            NSArray *arr = dic[@"datas"];
            if (pageIndex == 1 && kArrayIsEmpty(arr)) {
                [weakSelf addNoDateView];
            }else{
                [weakSelf setCoreData];
            }
        }else if ([dic[@"code"]isEqualToNumber:@-10]){
            if (pageIndex!=1)
            {
                [MBProgressHUD showSuccess:@"没有更多数据" toView:self.view];
            }
        }
    } failure:^(NSString *str) {
        [weakSelf addNoDateView];
    }];
}

-(void)addNoDateView
{
    [self.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)]];
    self.tableView.hidden = YES;
}

-(void)setCoreData
{
    NSFetchRequest *request ;
    NSError *error;
    request = [NSFetchRequest fetchRequestWithEntityName:@"PositionCoreData"];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"status == %@",@"chakanPersonal"];
    request.predicate = predicate;
    
    NSArray *resultAry = [self.context executeFetchRequest:request error:&error];
    
    if (resultAry.count == 0) {
        
    }else{
        _listArr = [NSMutableArray arrayWithArray:resultAry];
    }
    NSLog(@"_listArr_listArr%@",_listArr);
    
    if (kArrayIsEmpty(_listArr)) {
        [self.view addSubview:[GlobalMethod backgroundNoDateViewFrame:self.tableView.frame]];
    }
    self.tableView.hidden = NO;
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat h = [tableView cellHeightForIndexPath:indexPath model:_listArr[indexPath.row] keyPath:@"model" cellClass:[seeCell class] contentViewWidth:KWIDTH];
//    return h;
    CGFloat h = [NewSeeCell fetchHeight:_listArr[indexPath.row]];
    return h;
}

-(NewSeeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewSeeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewSeeCell"];
    
    [cell resetCellWithModel:_listArr[indexPath.row]];
//    seeCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"NewSeeCell"];
//    
//    cell = [[[NSBundle mainBundle]loadNibNamed:@"NewSeeCell" owner:0 options:nil]lastObject];
//    PositionCoreData *model =[_listArr objectAtIndex:indexPath.row];
//    cell.model = model;
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PositionCoreData *model =[_listArr objectAtIndex:indexPath.row];
    NSString *cid = model.c_Cid;
    CompanyInfoVC *info = [[CompanyInfoVC alloc]initWithCid:cid];
    [self.navigationController pushViewController:info animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
