//
//  seeCell.m
//  神农岛
//
//  Created by 宋晨光 on 16/4/2.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "seeCell.h"
#import "UIImageView+WebCache.h"


@interface seeCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstSpacingConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondSpacingConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstaint;
@property (weak, nonatomic) IBOutlet UIImageView *companyIcon;
@property (weak, nonatomic) IBOutlet UILabel *companyName;

@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation seeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if (KWIDTH == 320)
    {
        self.imageWidthConstaint.constant = 55;
        self.imageHeightConstaint.constant = 55;
        self.iconToTopConstraint.constant = 12;
        self.companyName.font = [UIFont systemFontOfSize:15];
        self.contentLabel.font = [UIFont systemFontOfSize:13];
        self.timeL.font = [UIFont systemFontOfSize:11];
    }
    [self.companyIcon setCorner:4];
}

-(void)setModel:(PositionCoreData *)model
{
    NSURL *url = [NSURL URLWithString:model.c_Logo];
    [self.companyIcon sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"矢量智能对象"]];
    self.companyName.text =[NSString stringWithFormat:@"%@",model.c_Name];
    [GlobalMethod resetLabel:self.companyName text:model.c_Name isWidthLimit:YES];
    NSString *time = [NSString stringWithFormat:@"%@", model.c_Time];
    if (time.length > 10)
    {
        self.timeL.text = [[time stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringToIndex:10];
    }
    self.contentLabel.text = @"查看了你的简历";
    CGFloat bottomMargin = 0;
    if (isIphone5) {
        bottomMargin = -10;
    }else if (isIphone6){
        bottomMargin = 0;
    }
    [self setupAutoHeightWithBottomView:self bottomMargin:bottomMargin];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
