//
//  NewSeeCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/28.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "NewSeeCell.h"

@implementation NewSeeCell

#pragma mark 懒加载

- (UIImageView *)imgView{
    if (_imgView == nil) {
        _imgView = [UIImageView new];
        _imgView.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _imgView.widthHeight = XY(W(15),W(15));
    }
    return _imgView;
}

- (UILabel *)companyL{
    if (_companyL == nil) {
        _companyL = [UILabel new];
        [GlobalMethod setLabel:_companyL widthLimit:0 numLines:0 fontNum:F(16) textColor:[HexStringColor colorWithHexString:@"272727"] text:@""];
    }
    return _companyL;
}

- (UILabel *)contentL{
    if (_contentL == nil) {
        _contentL = [UILabel new];
        [GlobalMethod setLabel:_contentL widthLimit:0 numLines:0 fontNum:F(14) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _contentL;
}

- (UILabel *)timeL{
    if (_timeL == nil) {
        _timeL = [UILabel new];
        [GlobalMethod setLabel:_timeL widthLimit:0 numLines:0 fontNum:F(12) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _timeL;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}


#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.companyL];
        [self.contentView addSubview:self.contentL];
        [self.contentView addSubview:self.timeL];
        [self.contentView addSubview:self.line];
    }
    return self;
}



#pragma mark 获取高度
FETCH_CELL_HEIGHT(NewSeeCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(PositionCoreData *)model{

    //刷新view
    //公司icon
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.c_Logo] placeholderImage:[UIImage imageNamed:@"矢量智能对象"]];
    self.imgView.leftTop = XY(W(15),W(15));
    self.imgView.widthHeight = XY(W(57), W(57));
    [self.imgView setCorner:F(3)];
    //公司名称
    [GlobalMethod resetLabel:self.companyL text:model.c_Name isWidthLimit:0];
    self.companyL.height = F(16);
    self.companyL.leftTop = XY(self.imgView.right + W(15),self.imgView.x);
    //内容label
    [GlobalMethod resetLabel:self.contentL text:@"查看了你的简历" isWidthLimit:0];
    self.contentL.leftTop = XY(self.imgView.right + W(15),self.companyL.bottom + W(8));
    self.contentL.height = F(14);
    //时间label
    NSString *time = [NSString stringWithFormat:@"%@", model.c_Time];
    if (time.length > 10)
    {
        [GlobalMethod resetLabel:self.timeL text:[[time stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringToIndex:10] isWidthLimit:0];
    }

    
    self.timeL.height = F(12);
    self.timeL.leftTop = XY(self.imgView.right + W(15),self.contentL.bottom+W(8.5));
    
    self.line.leftTop = XY(W(15), self.imgView.bottom + W(15));
    self.line.widthHeight = XY(KWIDTH - W(15), 1);
    
    return self.line.bottom + 1;
}


@end
