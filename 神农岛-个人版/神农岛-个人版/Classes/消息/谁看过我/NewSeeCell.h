//
//  NewSeeCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/28.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PositionCoreData+CoreDataProperties.h"

@interface NewSeeCell : UITableViewCell

@property (nonatomic ,strong) UIImageView *imgView;
@property (nonatomic ,strong) UILabel *companyL;
@property (nonatomic ,strong) UILabel *contentL;
@property (nonatomic ,strong) UILabel *timeL;
@property (nonatomic ,strong) UIView *line;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(PositionCoreData *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(PositionCoreData *)model;


@end
