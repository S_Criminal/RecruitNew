//
//  DeliveryRecordVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "DeliveryRecordVC.h"


#import "GeneralImageAndTitleModel.h"
#import "BriefStatusModel.h"
#import "DelivertRecordModel.h"
/** cell */
#import "SaveCell.h"
#import "GeneralLeftImageAndTitleLabelView.h"
#import "DeliveryRecordCell.h"

#import "NewWorkDetailVC.h"

@interface DeliveryRecordVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) GeneralImageAndTitleModel *sectionModel;

@property (nonatomic ,strong) NSMutableArray *listImageArr;
@property (nonatomic ,strong) NSArray *listTimeArr;
@property (nonatomic ,strong) NSArray *contentArr;
@property (nonatomic ,strong) NSArray *firstArr;

@property (nonatomic ,strong) DelivertRecordModel *deliverModel;

@property (nonatomic ,strong) NSString *status;

@end

@implementation DeliveryRecordVC

-(instancetype)initModel:(BriefStatusModel *)model
{
    DeliveryRecordVC *record = [DeliveryRecordVC new];
    record.model = model;
    return  record;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight + 1) style:UITableViewStyleGrouped];;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setModel];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"投递记录" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"SaveCell" bundle:nil] forCellReuseIdentifier:@"SaveCell"];
    [self.tableView registerClass:[GeneralLeftImageAndTitleLabelView class] forCellReuseIdentifier:@"GeneralLeftImageAndTitleLabelView"];
    [self.tableView registerClass:[DeliveryRecordCell class] forCellReuseIdentifier:@"DeliveryRecordCell"];
}

-(void)setModel
{
    self.sectionModel = [[GeneralImageAndTitleModel alloc]init];
    self.sectionModel.imageStr  = @"应聘进程";
    self.sectionModel.titles    = @"应聘进程";
    
    self.deliverModel = [DelivertRecordModel new];
    
    
    self.listImageArr = [NSMutableArray array];
//    self.listTimeArr  = [NSMutableArray array];
//    self.contentArr   = [NSMutableArray array];
    
    self.status = [GlobalMethod doubleToString:self.model.aBack];
    
    if ([_status isEqualToString:@"0"] || [_status isEqualToString:@"1"]){
        self.listImageArr = [NSMutableArray arrayWithObjects:@"信封",@"联系方式绿底",@"", nil];
        self.firstArr = [NSArray arrayWithObjects:@"您的简历已发送到HR邮箱",@"",@"",nil];
        self.listTimeArr = [NSArray arrayWithObjects:_model.seeDate,@"",@"", nil];
        self.contentArr = [NSArray arrayWithObjects:@"敬请耐心等待",@"",@"", nil];
    }else if ([_status isEqualToString:@"5"]){
        self.listImageArr = [NSMutableArray arrayWithObjects:@"信封",@"联系方式绿底", nil];
        self.firstArr = [NSArray arrayWithObjects:@"您的简历已发送到HR邮箱", @"您的履历不符合企业的职位要求",nil];
        self.listTimeArr = [NSArray arrayWithObjects:_model.createDate,_model.seeDatee, nil];
        self.contentArr = [NSArray arrayWithObjects:@"需要进一步与您沟通",@"去投递其他职位吧",@"", nil];
    }else{
        self.listImageArr = [NSMutableArray arrayWithObjects:@"信封",@"联系方式绿底",@"沟通", nil];
        self.firstArr = [NSArray arrayWithObjects:@"您的简历已发送到HR邮箱",@"您的履历已被企业初步认同",@"HR随时与您联系，请保持电话畅通",nil];
        self.listTimeArr = [NSArray arrayWithObjects:_model.createDate,_model.seeDateb,_model.seeDate, nil];
        self.contentArr = [NSArray arrayWithObjects:@"敬请耐心等待",@"需要进一步与您沟通",_model.hRMG,nil];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int i = 0;
    switch ([_status intValue]) {
        case 0:
            i = 2;
            break;
        case 1:
            i = 2;
            break;
        case 5:
            i = 3;
            break;
        default:
            i = 4;
            break;
    }
    
    return section == 1 ? i : 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat h;
    if (indexPath.section == 0)
    {
        h = [tableView cellHeightForIndexPath:indexPath model:self.model keyPath:@"model" cellClass:[SaveCell class] contentViewWidth:KWIDTH];
    }else if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            h = [GeneralLeftImageAndTitleLabelView fetchHeight:self.sectionModel];
        }else{
            self.deliverModel = [DelivertRecordModel new];
            self.deliverModel.imgStr = self.listImageArr[indexPath.row - 1];
            self.deliverModel.content = self.contentArr[indexPath.row - 1];
            self.deliverModel.title = self.firstArr[indexPath.row - 1];
            self.deliverModel.timel = self.listTimeArr[indexPath.row - 1];
            h = [DeliveryRecordCell fetchHeight:self.deliverModel];
        }
    }
    return h;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    
    if (indexPath.section == 0){
        SaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SaveCell"];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 1 && indexPath.row == 0){
        NSLog(@"%@",self.sectionModel);
        if (self.sectionModel)
        {
            GeneralLeftImageAndTitleLabelView *cell = [tableView dequeueReusableCellWithIdentifier:@"GeneralLeftImageAndTitleLabelView"];
            [cell resetCellWithModel:self.sectionModel];
            return cell;
        }
    }else if(indexPath.section == 1)
    {
        DeliveryRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryRecordCell"];
        self.deliverModel = [DelivertRecordModel new];
        self.deliverModel.imgStr = self.listImageArr[indexPath.row - 1];
        self.deliverModel.content = self.contentArr[indexPath.row - 1];
        self.deliverModel.title = self.firstArr[indexPath.row - 1];
        self.deliverModel.timel = self.listTimeArr[indexPath.row - 1];
        [cell resetCellWithModel:self.deliverModel];
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        NewWorkDetailVC *detail = [[NewWorkDetailVC alloc]init];
        detail.pid = [GlobalMethod doubleToString:self.model.pID];
        detail.cid = [GlobalMethod doubleToString:self.model.cID];
        [GB_Nav pushViewController:detail animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
