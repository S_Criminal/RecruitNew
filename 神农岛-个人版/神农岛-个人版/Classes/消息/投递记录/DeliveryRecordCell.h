//
//  DeliveryRecordCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DelivertRecordModel.h"

@interface DeliveryRecordCell : UITableViewCell
@property (nonatomic ,strong) UIImageView *iconImage;
@property (nonatomic ,strong) UILabel *firstL;
@property (nonatomic ,strong) UILabel *timeL;
@property (nonatomic ,strong) UILabel *contentL;
@property (nonatomic ,strong) UIView *verticaLine;


#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(DelivertRecordModel *)model;
#pragma mark 刷新cell

- (CGFloat)resetCellWithModel:(DelivertRecordModel *)model;


@end
