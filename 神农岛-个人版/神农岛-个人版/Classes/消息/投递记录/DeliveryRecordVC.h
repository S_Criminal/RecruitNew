//
//  DeliveryRecordVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC.h"
#import "BriefStatusModel.h"
@interface DeliveryRecordVC : BasicVC
@property (nonatomic ,strong) BriefStatusModel *model;
-(instancetype)initModel:(BriefStatusModel *)model;
@end
