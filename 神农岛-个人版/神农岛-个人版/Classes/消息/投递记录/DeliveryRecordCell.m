//
//  DeliveryRecordCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "DeliveryRecordCell.h"

@implementation DeliveryRecordCell


#pragma mark 懒加载

- (UIImageView *)iconImage{
    if (_iconImage == nil) {
        _iconImage = [UIImageView new];
        _iconImage.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _iconImage.widthHeight = XY(W(15),W(15));
    }
    return _iconImage;
}

- (UILabel *)firstL{
    if (_firstL == nil) {
        _firstL = [UILabel new];
        [GlobalMethod setLabel:_firstL widthLimit:0 numLines:0 fontNum:F(16) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _firstL;
}
- (UILabel *)timeL{
    if (_timeL == nil) {
        _timeL = [UILabel new];
        [GlobalMethod setLabel:_timeL widthLimit:0 numLines:0 fontNum:F(14) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _timeL;
}
- (UILabel *)contentL{
    if (_contentL == nil) {
        _contentL = [UILabel new];
        [GlobalMethod setLabel:_contentL widthLimit:0 numLines:0 fontNum:F(13) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _contentL;
}
- (UIView *)verticaLine{
    if (_verticaLine == nil) {
        _verticaLine = [UIView new];
        _verticaLine.backgroundColor = COLOR_MAINCOLOR;
    }
    return _verticaLine;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.iconImage];
        [self.contentView addSubview:self.firstL];
        [self.contentView addSubview:self.timeL];
        [self.contentView addSubview:self.contentL];
        [self.contentView addSubview:self.verticaLine];
        
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(DeliveryRecordCell)
#pragma mark 刷新cell


-(CGFloat)resetCellWithModel:(DelivertRecordModel *)model
{
    
    NSString *time = model.timel.length > 10 ? [[model.timel substringToIndex:16] stringByReplacingOccurrencesOfString:@"T" withString:@" "] : @"";
    
    self.iconImage.leftTop = XY(W(15),W(15));
    self.iconImage.widthHeight = XY(30, 30);
    self.iconImage.image = [UIImage imageNamed:model.imgStr];
    
    //标题
    self.firstL.leftTop = XY(self.iconImage.right + W(15),self.iconImage.top + 5);
    [GlobalMethod resetLabel:self.firstL text:model.title isWidthLimit:0];
    //时间
    self.timeL.leftTop = XY(self.firstL.left,self.firstL.bottom + W(10));
    [GlobalMethod resetLabel:self.timeL text:time isWidthLimit:0];
    //内容
    self.contentL.leftTop = XY(self.firstL.left,self.timeL.bottom + W(10));
    [GlobalMethod resetLabel:self.contentL text:model.content isWidthLimit:0];
    
    self.verticaLine.frame = CGRectMake(self.iconImage.centerX - 1, self.iconImage.bottom, 2, self.contentL.bottom - self.iconImage.height);
    
    return self.contentL.bottom ;
}


@end
