//
//  NewRedNumView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/27.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewRedNumView : UIView

@property (nonatomic, assign) int bagdeNumber;

@property (nonatomic ,strong) UILabel *redLabel;

-(CGRect)resetLabel:(int)title;

@end
