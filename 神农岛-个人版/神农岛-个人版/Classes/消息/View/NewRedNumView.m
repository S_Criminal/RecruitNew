//
//  NewRedNumView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/27.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "NewRedNumView.h"

@implementation NewRedNumView
-(UILabel *)redLabel
{
    if (!_redLabel) {
        _redLabel = [UILabel new];
        [GlobalMethod setLabel:_redLabel widthLimit:0 numLines:0 fontNum:F(13) textColor:[UIColor whiteColor] text:@"0"];
    }
    return _redLabel;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addSubview:self.redLabel];
    }
    return self;
}



-(CGRect)resetLabel:(int)title
{
    _redLabel.leftTop = XY(0, 0);
    
    [GlobalMethod resetLabel:_redLabel text:[NSString stringWithFormat:@"%d",title] isWidthLimit:0];
    
    return _redLabel.frame;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
