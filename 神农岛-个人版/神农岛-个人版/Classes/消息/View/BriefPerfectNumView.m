//
//  BriefPerfectNumView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "BriefPerfectNumView.h"

@implementation BriefPerfectNumView

-(UIButton *)deleteButton
{
    if (!_deleteButton) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    return _deleteButton;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        [self createViews];
    }
    return self;
}

-(void)setUI
{

}

-(void)createViews
{
    _iconImageView      = [UIImageView new];
    _contentLabel       = [UILabel new];
    
    _contentLabel.font = [UIFont systemFontOfSize:F(13)];
    _contentLabel.textColor = [HexStringColor colorWithHexString:@"666666"];
    
    [_iconImageView setFrame:CGRectMake(15, 0, W(17.5), W(17.5))];
    [_contentLabel setFrame:CGRectMake(CGRectGetMaxX(_iconImageView.frame) + W(10), W(10), W(KWIDTH - 15 - 9 - 35 - 15), F(13))];
    
    [self.deleteButton setFrame:CGRectMake(KWIDTH - W(44), 0, W(44), W(44))];
    
    _iconImageView.centerY      = self.bounds.size.height / 2;
    _contentLabel.centerY       = _iconImageView.centerY;
    self.deleteButton.centerY    = _iconImageView.centerY;
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, W(44) - 1, KWIDTH, 1)];
    line.backgroundColor = COLOR_SEPARATE_COLOR;
    [self addSubview:_iconImageView];
    [self addSubview:_contentLabel];
    [self addSubview:self.deleteButton];
    [self addSubview:line];
}

-(void)setModel:(BriefPerfectNumModel *)model
{
    _iconImageView.image    = [UIImage imageNamed:model.iconName];
    _contentLabel.text      = model.content;
    [self.deleteButton setImage:[UIImage imageNamed:model.deleteName] forState:UIControlStateNormal];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
