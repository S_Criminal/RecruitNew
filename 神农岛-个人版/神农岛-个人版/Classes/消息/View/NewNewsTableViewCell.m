//
//  NewNewsTableViewCell.m
//  神农岛
//
//  Created by 宋晨光 on 16/10/1.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "NewNewsTableViewCell.h"

@implementation NewNewsTableViewCell{
    CGFloat titleFont;
    CGFloat contentFont;
    CGFloat imageWidth;
    CGFloat toIconConstraint;
    CGFloat toTopBottomConstraint;
}

-(NewRedNumView *)redNumView
{
    if (!_redNumView) {
        _redNumView = [NewRedNumView new];
        _redNumView.backgroundColor = [UIColor redColor];
    }
    return _redNumView;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUI];
        [self setUpViews];
    }
    return self;
}

-(void)setUI
{
    titleFont = 17;
    contentFont = 15;
    imageWidth = 50;
    toTopBottomConstraint = 15;
    toIconConstraint = 3;
    if (KWIDTH == 320) {
        titleFont = 15;
        contentFont = 13;
        imageWidth = 44;
        toIconConstraint = 2;
        toTopBottomConstraint = 12;
    }else if (KWIDTH == 414){
        titleFont = 18;
        contentFont = 15;
        imageWidth = 50;
        toIconConstraint = 1;
        toTopBottomConstraint = 15;
    }
}

-(void)setUpViews
{
    _iconImageView = [UIImageView new];
    _titleLabel = [UILabel new];
    _contentLabel = [UILabel new];
    
    _titleLabel.font = [UIFont systemFontOfSize:titleFont];
    _contentLabel.font = [UIFont systemFontOfSize:contentFont];
    
    _titleLabel.textColor = [HexStringColor colorWithHexString:@"323232"];
    _contentLabel.textColor = [HexStringColor colorWithHexString:@"999999"];
    
    [self addSubview:self.iconImageView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.redNumView];
}

-(void)setListImageString:(NSString *)string ContentString:(NSString *)ConString subTitle:(NSString *)subTitle redNum:(int)num
{
    _iconImageView.image = [UIImage imageNamed:string];
    _titleLabel.text = ConString;
    _contentLabel.text = kStringIsEmpty(subTitle) ? @"暂无新消息" :subTitle;
    
    [self.redNumView setFrame:CGRectMake(KWIDTH - 15 - 30, 0, 30, 30)];
    
    CGRect rect = [_redNumView resetLabel:num];
    CGFloat add = 5;
    //add = num > 9 ? 8 : add;
    _redNumView.hidden = num == 0 ? YES : NO;
    _redNumView.widthHeight = XY(rect.size.height + add, rect.size.height + add);
    _redNumView.redLabel.centerX = _redNumView.size.height / 2;
    _redNumView.redLabel.centerY = _redNumView.size.height / 2;
    [_redNumView setCorner:_redNumView.size.height / 2];
    self.redNumView.centerY = _iconImageView.centerY;
}

-(void)setModel:(NewsTableListModel *)model
{
    _model = model;
    [_iconImageView setFrame:CGRectMake(toTopBottomConstraint + 1, toTopBottomConstraint - 1, imageWidth, imageWidth)];
    [_titleLabel setFrame:CGRectMake(CGRectGetMaxX(_iconImageView.frame) + 10, _iconImageView.top, 150, 20)];
    [_contentLabel setFrame:CGRectMake(CGRectGetMaxX(_iconImageView.frame) + 10, _iconImageView.bottom - 20, KWIDTH - _iconImageView.right - 10 - 30 - 15, 20)];
    
    
    
    [self setupAutoHeightWithBottomView:_contentLabel bottomMargin:toTopBottomConstraint];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
