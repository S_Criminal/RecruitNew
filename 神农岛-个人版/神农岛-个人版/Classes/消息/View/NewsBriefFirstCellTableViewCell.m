//
//  NewsBriefFirstCellTableViewCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "NewsBriefFirstCellTableViewCell.h"
#import "NewsTableListModel.h"

@interface NewsBriefFirstCellTableViewCell ()
@property (nonatomic ,strong)NSArray *imageArr;

@end


@implementation NewsBriefFirstCellTableViewCell{
    CGFloat labelFont;
}

-(NewRedNumView *)redNumView
{
    if (!_redNumView) {
        _redNumView = [NewRedNumView new];
        _redNumView.backgroundColor = [UIColor redColor];
    }
    return _redNumView;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


-(void)createViews
{
    
    NewsTableListModel *model = [[NewsTableListModel alloc]init];
    self.imageArr = model.firstImageArr;
    CGFloat viewWidth = KWIDTH / 3;
    for (int i = 0; i < 3; i ++)
    {
        self.control = [UIControl new];
        _control.x = viewWidth * i;
        _control.y = 0;
        _control.width  = viewWidth;
        _control.height = W(110);
        self.control.frame = _control.frame;
        self.control.tag = 10 + i;
        [self.control addTarget:self action:@selector(tapNewsCell:) forControlEvents:UIControlEventTouchUpInside];
        [self bgView:_control index:i contentModel:model];
    }
}


-(void)bgView:(UIControl *)control index:(int)index contentModel:(NewsTableListModel *)model
{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, W(20), W(45), W(45))];
    imageView.centerX = control.size.width / 2;
    imageView.tag = 100;
    NSLog(@"%f",imageView.centerX);
    NSLog(@"%f",control.size.width / 2);
    
    NSLog(@"%d ---- %@",index,model.firstImageArr);
    imageView.image = [UIImage imageNamed:model.firstImageArr[index]];
    
    UILabel *label  = [[UILabel alloc]initWithFrame:CGRectMake( 0, CGRectGetMaxY(imageView.frame) + W(12), control.size.width, F(15))];
    label.text      = model.firstContentArr[index];
    label.font      = [UIFont systemFontOfSize:F(15)];
    label.textColor = COLOR_LABELSIXCOLOR;
    label.textAlignment = NSTextAlignmentCenter;
    
    [control addSubview:imageView];
    [control addSubview:label];
    [self addSubview:control];
    
    control.tag == 10 ? [control addSubview:self.redNumView] : nil;
    
    [self setupAutoHeightWithBottomView:control bottomMargin:0];
}

-(void)tapNewsCell:(UIControl *)control
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(protocol:tag:)]) {
        [self.delegate protocol:control tag:control.tag];
    }
}


-(void)setRedStatusNum:(int)redStatusNum
{
    _redStatusNum = redStatusNum;
    _redNumView.hidden = redStatusNum == 0 ? YES : NO;
    CGFloat add = 5;
    CGRect rect = [_redNumView resetLabel:redStatusNum];
    
    UIControl *control = (UIControl *)[self viewWithTag:10];
    UIImageView *imgView = (UIImageView *)[control viewWithTag:100];
    
    _redNumView.widthHeight = XY(rect.size.height + add, rect.size.height + add);
    _redNumView.redLabel.centerX = _redNumView.size.height / 2;
    _redNumView.redLabel.centerY = _redNumView.size.height / 2;
    [_redNumView setCorner:_redNumView.size.height / 2];
    
    
    self.redNumView.y = imgView.y - 5;
    self.redNumView.x = imgView.bottom + 5;
    
}

@end
