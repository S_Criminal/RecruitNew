//
//  NewNewsTableViewCell.h
//  神农岛
//
//  Created by 宋晨光 on 16/10/1.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsTableListModel.h"

#import "NewRedNumView.h"


@interface NewNewsTableViewCell : UITableViewCell
@property (nonatomic ,strong)UIImageView *iconImageView;
@property (nonatomic ,strong)UILabel *titleLabel;
@property (nonatomic ,strong)UILabel *contentLabel;
@property (nonatomic ,strong) NewRedNumView *redNumView;

@property (nonatomic ,strong)NewsTableListModel *model;

-(void)setListImageString:(NSString *)string ContentString:(NSString *)ConString subTitle:(NSString *)subTitle redNum:(int)num;

@end
