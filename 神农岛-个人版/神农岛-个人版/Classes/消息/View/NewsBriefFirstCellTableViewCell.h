//
//  NewsBriefFirstCellTableViewCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsTableListModel.h"
#import "NewRedNumView.h"
@protocol NewsSelectFirstDelegate <NSObject>

-(void)protocol:(UIControl *)control tag:(NSInteger)tag;

@end

@interface NewsBriefFirstCellTableViewCell : UITableViewCell
@property (nonatomic ,strong)NewsTableListModel *model;
@property (nonatomic ,strong) UIControl *control;
@property (nonatomic ,strong) NewRedNumView *redNumView;

@property (nonatomic ,weak) id<NewsSelectFirstDelegate>delegate;

@property (nonatomic ,assign) int redStatusNum;

@end
