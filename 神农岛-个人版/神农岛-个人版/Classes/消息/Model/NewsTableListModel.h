//
//  NewsTableListModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsTableListModel : NSObject
@property (nonatomic ,strong) NSArray *firstImageArr;
@property (nonatomic ,strong) NSArray *firstContentArr;
@property (nonatomic ,strong) NSArray *listImageArr;
@property (nonatomic ,strong) NSArray *listTitleArr;


@end
