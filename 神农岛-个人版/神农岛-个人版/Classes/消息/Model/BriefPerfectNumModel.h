//
//  BriefPerfectNumModel.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BriefPerfectNumModel : NSObject

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *iconName;
@property (nonatomic, copy) NSString *deleteName;

@end
