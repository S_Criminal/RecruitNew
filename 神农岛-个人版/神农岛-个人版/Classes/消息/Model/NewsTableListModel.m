//
//  NewsTableListModel.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "NewsTableListModel.h"

@implementation NewsTableListModel

-(NSArray *)firstImageArr
{
    if (!_firstImageArr)
    {
        _firstImageArr = [NSArray arrayWithObjects:@"简历状态",@"简历管理",@"职位收藏", nil];
    }
    return _firstImageArr;
}

-(NSArray *)firstContentArr
{
    if (!_firstContentArr) {
        _firstContentArr = [NSArray arrayWithObjects:@"简历状态",@"简历管理",@"职位收藏", nil];
    }
    return _firstContentArr;
}

-(NSArray *)listImageArr
{
    if (!_listImageArr) {
        _listImageArr = [NSArray arrayWithObjects:@"谁看过我",@"小秘书", nil];
    }
    return _listImageArr;
}

-(NSArray *)listTitleArr
{
    if (!_listTitleArr) {
        _listTitleArr = [NSArray arrayWithObjects:@"谁看过我",@"小秘书", nil];
    }
    return _listTitleArr;
}

@end
