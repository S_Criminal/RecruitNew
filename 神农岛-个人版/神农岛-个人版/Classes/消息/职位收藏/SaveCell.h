//
//  SaveCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/16.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefStatusModel.h"

@interface SaveCell : UITableViewCell
@property (nonatomic ,strong) UIButton *editButton;
@property (nonatomic ,strong) BriefStatusModel *model;
//@property (weak, nonatomic) IBOutlet UIButton *selectMoreButton;        //它的tag值为100

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToLeftConstraint;

-(CGFloat)resetCellModel;

@end
