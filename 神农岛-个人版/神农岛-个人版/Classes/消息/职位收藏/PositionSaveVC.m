//
//  PositionSaveVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/16.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "PositionSaveVC.h"
#import "SaveCell.h"
#import "RequestApi+News.h"
#import "PositionModel.h"

#import "NewWorkDetailVC.h"

#import "AdminDeleteAllView.h"

@interface PositionSaveVC ()<UITableViewDelegate,UITableViewDataSource,deleteAllButton>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic ,strong) NSMutableArray *selectDeleteArray;
@property (nonatomic ,strong) BaseNavView *navView;
@property (nonatomic ,assign) CGFloat editButtonWidth;
@property (nonatomic ,assign) BOOL navStatus;

@property (nonatomic ,strong) AdminDeleteAllView *deleteAllView;    //底部删除View

@end

@implementation PositionSaveVC{
    NSString *_oid;
    NSInteger _pageIndex;
}

-(AdminDeleteAllView *)deleteAllView
{
    if (!_deleteAllView) {
        _deleteAllView = [AdminDeleteAllView initWithModel:nil];
        [_deleteAllView setCorner:4];
        _deleteAllView.delegate = self;
        _deleteAllView.backgroundColor = COLOR_MAINCOLOR;
    }
    return _deleteAllView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _editButtonWidth = W(15);
    [self setNav];
    [self setBasicTableView];
    _pageIndex = 1;
    _listArr = [NSMutableArray array];
    [self setFefresh];
    [self setModelPageIndex:_pageIndex];
    [self setBasicView];
}

-(void)setNav
{
    _navStatus = YES;
    [self.view addSubview:[GlobalMethod addNavLine]];
    [self setNavWithStatus];
    
    [self.view addSubview:self.navView];
}

//nav的rightTitle重新设置
-(void)setNavWithStatus
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"收藏" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"管理" rightBlock:^{
        weakSelf.navView.rightLabel.text = !_navStatus ? @"管理" : @"取消";
        _navStatus = !_navStatus;
        [weakSelf deleteMore];
    }];
}

-(void)setBasicTableView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1) style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:@"SaveCell" bundle:nil] forCellReuseIdentifier:@"SaveCell"];
}

-(void)setBasicView
{
    [self.deleteAllView setFrame:CGRectMake(W(20), KHEIGHT - W(15) - W(40), KWIDTH - W(20) * 2, W(40))];
    [self.deleteAllView resetViewWithModel:nil];
    self.deleteAllView.alpha = 0;
    [self.view addSubview:self.deleteAllView];
}

#pragma mark 刷新 加载更多

-(void)setFefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    // 设置header
    self.tableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    self.tableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    _pageIndex = 1;
    _listArr = [NSMutableArray array];
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelPageIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
}

-(void)setModelPageIndex:(NSInteger)pageIndex
{
    self.selectDeleteArray = [NSMutableArray array];
    DSWeak;
    if (pageIndex == 1) {
        _oid = @"0";
        [_listArr removeAllObjects];
    }
    [RequestApi savePositionListWithKey:[GlobalData sharedInstance].GB_Key top:@"10" pageID:_oid Delegate:self Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        for (NSDictionary *dic in arr)
        {
            PositionModel *model = [PositionModel modelObjectWithDictionary:dic];
            [weakSelf.listArr addObject:model];
        }
        if (_listArr.count > 0) {
            weakSelf.tableView.hidden = NO;
            PositionModel *model = _listArr.lastObject;
            _oid = [NSString stringWithFormat:@"%@",[GlobalMethod doubleToString:model.iDProperty]];
            NSLog(@"%@",_oid);
        }else{
            if ([_oid isEqualToString:@"0"])
            {
                [weakSelf.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)]];
                weakSelf.tableView.hidden = YES;
            }
        }
        
        if (arr.count % 10 == 0 && _pageIndex != 1)
        {
            [MBProgressHUD showError:@"没有更多数据" toView:self.view];
        }
        
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorstr) {
        
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_listArr.count == 0) {
        return 0;
    }
//    CGFloat h = [tableView cellHeightForIndexPath:indexPath model:_listArr[indexPath.row] keyPath:@"model" cellClass:[SaveCell class] contentViewWidth:KWIDTH];
    SaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SaveCell"];
    CGFloat h = [cell resetCellModel];
    
    return h;
}

-(SaveCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SaveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SaveCell"];
    if (_listArr.count == 0) {
        return cell;
    }
    [cell.editButton addTarget:self action:@selector(tapEdit:) forControlEvents:UIControlEventTouchUpInside];
    cell.editButton.tag = indexPath.row;
    cell.iconToLeftConstraint.constant = self.editButtonWidth;
    cell.model = _listArr[indexPath.row];
    for (PositionModel *model in _selectDeleteArray) {
        if (model == _listArr[indexPath.row]){
            cell.editButton.selected = YES;
            break;
        }
    }
    return cell;
}

#pragma mark 多选选中cell

-(void)tapEdit:(UIButton *)sender
{
    [self.selectDeleteArray addObject:_listArr[sender.tag]];
    
    for (PositionModel *model in self.selectDeleteArray)
    {
        if (model == _listArr[sender.tag] && sender.selected)
        {
            [self.selectDeleteArray removeObject:_listArr[sender.tag]];
            break;
        }
    }
    sender.selected = !sender.selected;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_navStatus)
    {
        if (_listArr.count > 0)
        {
            PositionModel *model = _listArr[indexPath.row];
            NewWorkDetailVC *detail = [[NewWorkDetailVC alloc]initWithCid:[GlobalMethod doubleToString:model.iDProperty] pid:[GlobalMethod doubleToString:model.pID]];
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
    
}

#pragma mark tableview的左滑删除
//判断是左滑 还是多选删除
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;;
}

//添加编辑模式
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

//删除的动作
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        PositionModel *model = self.listArr[indexPath.row];
        DSWeak;
        [RequestApi deleteSavePositionWithKey:[GlobalData sharedInstance].GB_Key pid:@"" saveId:[GlobalMethod doubleToString:model.iDProperty] Delegate:self Success:^(NSDictionary *response) {
            [weakSelf.listArr removeObject:model];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        } failure:^(NSString *str) {
            [MBProgressHUD showError:str toView:weakSelf.view];
        }];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

#pragma mark 多行删除

-(void)protocolDeleteAllButton:(NSInteger)tag
{
    if (tag == 1)
    {
        _selectDeleteArray = [NSMutableArray arrayWithArray:self.listArr];
        [self.tableView reloadData];
    }else if (tag == 2){
        [self deleteMorePosition];
    }
}

-(void)deleteMore
{
    if (_navStatus) {
        _editButtonWidth = W(15);
        [UIView animateWithDuration:0.35f animations:^{
            self.deleteAllView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
            self.deleteAllView.alpha = 0;
        } completion:^(BOOL finished) {
            
        }];
    }else{
        _editButtonWidth = W(15) * 3;
        self.deleteAllView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        [UIView animateWithDuration:0.35f animations:^{
            self.deleteAllView.transform = CGAffineTransformMakeScale(1, 1);
            self.deleteAllView.alpha = 1;
        } completion:^(BOOL finished) {
            
        }];
    }
    [self.tableView reloadData];
}
/**
 *  删除职位  删除多个id传del
 */
-(void)deleteMorePosition
{
    NSMutableArray *idArr = [NSMutableArray array] ;
    if (kArrayIsEmpty(_selectDeleteArray)) {
        PositionModel *model = _selectDeleteArray.firstObject;
        if (kStringIsEmpty(model.pName))
        {
            [MBProgressHUD showError:@"请选择至少一个职位" toView:self.view];
            return;
        }
    }
    
    for (PositionModel *model in _selectDeleteArray)
    {
        [idArr addObject:[GlobalMethod doubleToString:model.iDProperty]];
    }
    
    DSWeak;
    [RequestApi deleteSavePositionWithKey:[GlobalData sharedInstance].GB_Key pid:@"" saveId:[idArr componentsJoinedByString:@","] Delegate:nil Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        [MBProgressHUD showError:response[@"datas"] toView:weakSelf.view];
        weakSelf.navStatus = YES;
        
        NSMutableArray *type_Array = [NSMutableArray arrayWithArray:weakSelf.listArr];
        for (PositionModel *model in type_Array)
        {
            for (NSString *str in idArr)
            {
                if ([str isEqualToString:[GlobalMethod doubleToString:model.iDProperty]])
                {
                    [weakSelf.listArr removeObject:model];
                }
            }
        }
        weakSelf.navView.rightLabel.text = @"管理";
        [weakSelf deleteMore];
        //_pageIndex = 1;
        //[weakSelf setModelPageIndex:_pageIndex];
    } failure:^(NSString *str) {
        [MBProgressHUD showError:str toView:weakSelf.view];
    }];
}

#pragma mark - 取消选中单元格方法
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
