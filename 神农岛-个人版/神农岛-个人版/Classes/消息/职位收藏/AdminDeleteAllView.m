//
//  AdminDeleteAllView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/23.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "AdminDeleteAllView.h"

@implementation AdminDeleteAllView

#pragma mark 懒加载

-(UIButton *)allButton{
    if (_allButton == nil) {
        _allButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _allButton.tag = 1;
        [_allButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        _allButton.backgroundColor = [UIColor clearColor];
        _allButton.titleLabel.font = [UIFont systemFontOfSize:W(16)];
        [GlobalMethod setRoundView:_allButton color:[UIColor clearColor] numRound:5 width:0];
        [_allButton setTitle:@"全选" forState:(UIControlStateNormal)];
        _allButton.widthHeight = XY(KWIDTH - W(30),W(40));
    }
    return _allButton;
}

-(UIButton *)deleteButton{
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteButton.tag = 2;
        [_deleteButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        _deleteButton.backgroundColor = [UIColor clearColor];
        _deleteButton.titleLabel.font = [UIFont systemFontOfSize:W(16)];
        [GlobalMethod setRoundView:_deleteButton color:[UIColor clearColor] numRound:5 width:0];
        [_deleteButton setTitle:@"删除" forState:(UIControlStateNormal)];
        _deleteButton.widthHeight = XY(KWIDTH - W(30),W(40));
    }
    return _deleteButton;
}


#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}
- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubView];
    }
    return self;
}

//添加subview
- (void)addSubView
{
    [self addSubview:self.allButton];
    [self addSubview:self.deleteButton];
    [self addSubview:[GlobalMethod addVerticalLineFrame:CGRectMake((KWIDTH - W(20) * 2 - 1) / 2 - 0.5,  W(5), 1, W(40) - W(5) * 2) color:[UIColor whiteColor]]];
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    AdminDeleteAllView * view = [AdminDeleteAllView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model{
    //刷新view
    CGFloat width = (KWIDTH - W(20) * 2 - 1) / 2;
    self.allButton.widthHeight = XY((width), W(40));
    self.allButton.leftTop = XY(0,0);
    
    self.deleteButton.widthHeight = XY((width), W(40));
    self.deleteButton.leftTop = XY(self.allButton.right + 1,0);
}

#pragma mark 点击事件
- (void)btnClick:(UIButton *)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(protocolDeleteAllButton:)]) {
        [self.delegate protocolDeleteAllButton:sender.tag];
    }
}

@end
