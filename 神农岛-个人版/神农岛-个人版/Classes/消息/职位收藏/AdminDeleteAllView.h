//
//  AdminDeleteAllView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/23.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol deleteAllButton <NSObject>

-(void)protocolDeleteAllButton:(NSInteger)tag;

@end

@interface AdminDeleteAllView : UIView

@property (nonatomic ,strong) UIButton *allButton;
@property (nonatomic ,strong) UIButton *deleteButton;

@property (nonatomic ,weak) id<deleteAllButton>delegate;

#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model;

@end
