//
//  SaveCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/16.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "SaveCell.h"

#define META_CORNER 10
#define METACOLOR [HexStringColor colorWithHexString:@"#e2e2e2"]
#define MainLabelColor [HexStringColor colorWithHexString:@"#666666"]

@interface SaveCell ()
//约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spacingTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spacingBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionNameConstraintWidth;
//分割线高度约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *linesHeightConstraint;




//标签
@property (weak, nonatomic) IBOutlet UILabel *meta1;
@property (weak, nonatomic) IBOutlet UILabel *meta2;
@property (weak, nonatomic) IBOutlet UILabel *meta3;
@property (weak, nonatomic) IBOutlet UIView *lines;

//
@property (weak, nonatomic) IBOutlet UIImageView *headImage;

@property (weak, nonatomic) IBOutlet UILabel *positionL;
@property (weak, nonatomic) IBOutlet UILabel *companyLL;
@property (weak, nonatomic) IBOutlet UILabel *cityL;
//工作年限
@property (weak, nonatomic) IBOutlet UILabel *workTimeL;
@property (weak, nonatomic) IBOutlet UILabel *degreeL;
//日期

//薪资
@property (weak, nonatomic) IBOutlet UILabel *payL;
//已认证图片
@property (weak, nonatomic) IBOutlet UIImageView *AuthenticationImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *authenticationImageViewWidth;
//视频简介图片
@property (weak, nonatomic) IBOutlet UIImageView *videoImage;

//聊天
@property (weak, nonatomic) IBOutlet UIButton *chatImageButton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *meta2ToMeta1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageWidthConstraint;
//meta1到底部的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *meta1ToBottomConstraint;
//公司到职位间的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyToPositionConstraint;
//城市到公司的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityToCompanyConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconToTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameToIconConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *positionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityHeightConstraint;

@end

@implementation SaveCell

-(UIButton *)editButton
{
    if (!_editButton ) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setImage:[UIImage imageNamed:@"未选中News"] forState:UIControlStateNormal];
        [_editButton setImage:[UIImage imageNamed:@"选中时News"] forState:UIControlStateSelected];
    }
    return _editButton;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    [self.headImage setCorner:4];
    
    
    self.companyToPositionConstraint.constant = 5;
    self.cityToCompanyConstraint.constant     = 5;
    
    self.iconImageWidthConstraint.constant = W(60);
    self.iconImageHeightConstraint.constant = W(60);
    
    self.companyToPositionConstraint.constant = 10.5f;
    self.cityToCompanyConstraint.constant = 11;
    
    if (KWIDTH == 320) {
        self.companyToPositionConstraint.constant = 7.5f;
        self.cityToCompanyConstraint.constant = 6.5f;
        self.iconImageHeightConstraint.constant = 55;
        self.iconImageWidthConstraint.constant  = 55;
    }else if (KWIDTH == 375){
        self.companyToPositionConstraint.constant = 10;
        self.cityToCompanyConstraint.constant = 10;
        self.iconImageWidthConstraint.constant = 62;
        self.iconImageHeightConstraint.constant = 62;
    }
    
    self.iconToLeftConstraint.constant = W(15);          //图片到cell顶部的约束
    self.iconToTopConstraint.constant = W(15);           //图片到cell顶部的约束
    self.meta1ToBottomConstraint.constant = W(15);       //meta到底部的约束
    self.nameToIconConstraint.constant = W(15);          //z职位名称
    self.linesHeightConstraint.constant = W(15);
    
    
    self.positionL.font = [UIFont boldSystemFontOfSize:F(16)];
    NSLog(@"%f",F(16));
    self.companyLL.font = [UIFont systemFontOfSize:F(15)];
    self.cityL.font = [UIFont systemFontOfSize:F(13)];
    
    self.payL.font = [UIFont boldSystemFontOfSize:F(15)];
    self.workTimeL.font = [UIFont systemFontOfSize:F(13)];
    self.degreeL.font = [UIFont systemFontOfSize:F(13)];
    
    self.positionHeightConstraint.constant = F(16);
    self.companyHeightConstraint.constant = F(14);
    self.cityHeightConstraint.constant = F(13);
    
    self.meta1.font = [UIFont systemFontOfSize:F(12)];
    self.meta2.font = [UIFont systemFontOfSize:F(12)];
//    self.companyToPositionConstraint.constant = 5;
//    self.cityToCompanyConstraint.constant     = 5;
    
//    self.meta1ToBottomConstraint.constant  = 12 ;
//    
//    self.iconToLeftConstraint.constant = W(11);
    
//    if (KWIDTH == 320) {
//        
//        self.iconImageWidthConstraint.constant = 55;
//        self.iconImageHeightConstraint.constant = 55;
//        
//        self.positionL.font = [UIFont boldSystemFontOfSize:15];
//        self.companyLL.font = [UIFont systemFontOfSize:13];
//        self.payL.font = [UIFont boldSystemFontOfSize:14];
//        
//        self.cityL.font = [UIFont systemFontOfSize:12];
//        self.workTimeL.font = [UIFont systemFontOfSize:12];
//        self.degreeL.font = [UIFont systemFontOfSize:12];
//        
//        self.meta1.font = [UIFont systemFontOfSize:12];
//        self.meta2.font = [UIFont systemFontOfSize:12];
//        
//        self.cityL.textColor        = MainLabelColor;
//        self.workTimeL.textColor    = MainLabelColor;
//        self.degreeL.textColor      = MainLabelColor;
//        
//        self.iconToTopConstraint.constant = 11;         //图片到cell顶部的约束
//        self.meta1ToBottomConstraint.constant = 10;     //meta1到cell底部的距离
//        self.companyToPositionConstraint.constant = 1;
//        self.cityToCompanyConstraint.constant = 1;
//        
//    }else if (KWIDTH == 375){
//        self.iconImageWidthConstraint.constant = 60;
//        self.iconImageHeightConstraint.constant = 60;
//        
//        self.positionL.font = [UIFont boldSystemFontOfSize:16];
//        self.companyLL.font = [UIFont systemFontOfSize:14];
//        self.payL.font = [UIFont boldSystemFontOfSize:15];
//        
//        self.cityL.font = [UIFont systemFontOfSize:13];
//        self.workTimeL.font = [UIFont systemFontOfSize:13];
//        self.degreeL.font = [UIFont systemFontOfSize:13];
//        
//        self.meta1.font = [UIFont systemFontOfSize:12];
//        self.meta2.font = [UIFont systemFontOfSize:12];
//        
//        self.cityL.textColor        = MainLabelColor;
//        self.workTimeL.textColor    = MainLabelColor;
//        self.degreeL.textColor      = MainLabelColor;
//        
//        
//        self.iconToTopConstraint.constant = 12;         //图片到cell顶部的约束
//        self.meta1ToBottomConstraint.constant = 11;     //meta1到cell底部的距离
//        self.companyToPositionConstraint.constant = 3;
//        self.cityToCompanyConstraint.constant = 3;
//    }
    
    [self setMetaLabel:_meta1];
    [self setMetaLabel:_meta2];
    [self setMetaLabel:_meta3];
    
    self.lines.backgroundColor = COLOR_BGCOLOR;
    self.chatImageButton.hidden = YES;
    
}

-(void)setMetaLabel:(UILabel *)label
{
    label.layer.borderWidth =1;
    label.layer.borderColor = METACOLOR.CGColor;
    [label setCorner:META_CORNER];
    label.textAlignment = NSTextAlignmentLeft;
    label.layer.masksToBounds = YES;
    [label sizeToFit];
}

-(void)setModel:(BriefStatusModel *)model
{
    NSLog(@"%@",model.cLogo);
    /** 公司logo */
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.cLogo] placeholderImage:[UIImage imageNamed:@"矢量智能对象"] options:SDWebImageRetryFailed|SDWebImageLowPriority];
    /** 公司名 */
    NSString *name = model.cName;
    if (name.length > 15) {
        self.companyLL.text = [name substringToIndex:15];
    }else{
        self.companyLL.text = name;
    }
    /** 职位名 */
    NSString *pName = model.pName;
    if (pName.length > 10) {
        self.positionL.text = [pName substringToIndex:10];
    }else{
        self.positionL.text = pName;
    }
    /** 城市 工作经验 学历 */
    NSString *city      = [NSString stringWithFormat:@"%@",model.pCity];
    NSString *workExp   = [NSString stringWithFormat:@"  %@",model.pExp];
    NSString *education = [NSString stringWithFormat:@"  %@",model.pEducation];
    if (kStringIsEmpty(model.pExp)) {
        workExp = @"";
    }
    if (kStringIsEmpty(model.pCity)) {
        city = @"";
    }
    self.cityL.text     = [NSString stringWithFormat:@"%@%@%@",city,workExp,education];
    /** 企业规模  */
    NSString *scale     = model.cScale;
    if (kStringIsEmpty(model.cScale)) {
        scale = @"    20人以下    ";
    }
    self.meta1.text     = [NSString stringWithFormat:@"    %@    ",scale];
    /** 企业性质 */
    NSString *i_Name    = model.iName;
    NSLog(@"%@",i_Name);
    if (kStringIsEmpty(i_Name)) {
        i_Name = @"    农药生产    ";
    }
    self.meta2.text     = [NSString stringWithFormat:@"    %@   ",i_Name];

    /** 薪资待遇 */
    self.payL.text = model.pPay;
    //企业是否已认证
    NSString *authentication = [GlobalMethod doubleToString:model.cAduitStatus];
    if ([authentication isEqualToString:@"0"]) {
        self.AuthenticationImageView.hidden = YES;
        self.authenticationImageViewWidth.constant = 0;
    }else{
        self.AuthenticationImageView.hidden = NO;
    }
    
    //企业是否有视频
    self.videoImage.hidden = YES;
 
//    CGFloat bottomMargin = 0 ;
//    if (isIphone5) {
//        bottomMargin = -18;
//    }else if (isIphone6){
//        bottomMargin = -8;
//    }else if (isIphone6p){
//        bottomMargin = 0;
//    }
    CGFloat bottomMargin = 0 ;
    if (isIphone5) {
        bottomMargin = -20;
    }else if (isIphone6){
        bottomMargin = 0;
    }else if (isIphone6p){
        bottomMargin = 0;
    }
    
    [self.editButton setFrame:CGRectMake(0, 10, W(15) * 3, self.headImage.height)];
    self.editButton.centerY = self.headImage.centerY;

    self.editButton.hidden = self.iconToLeftConstraint.constant == W(15) ? YES : NO;
    self.editButton.selected = NO;
    [self addSubview:self.editButton];
    
    [self setupAutoHeightWithBottomView:_lines bottomMargin:bottomMargin];
    
}

-(CGFloat)resetCellModel
{
    return (W(15) * 3 + self.iconImageWidthConstraint.constant + 18 + _linesHeightConstraint.constant);
}


@end
