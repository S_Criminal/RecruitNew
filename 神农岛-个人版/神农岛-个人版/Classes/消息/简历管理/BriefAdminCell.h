//
//  BriefAdminCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefInfoModel.h"
@interface BriefAdminCell : UITableViewCell
@property (nonatomic ,strong) UILabel *postionL;
@property (nonatomic ,strong) UILabel *payL;
@property (nonatomic ,strong) UILabel *cityL;
@property (nonatomic ,strong) UIButton *moreButton;

@property (nonatomic ,strong) UIView *line;



#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(BriefInfoModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model;



@end
