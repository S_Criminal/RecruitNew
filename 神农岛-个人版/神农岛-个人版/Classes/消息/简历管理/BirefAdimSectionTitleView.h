//
//  BirefAdimSectionTitleView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AdminControl;

@interface BirefAdimSectionTitleView : UIView
@property (nonatomic ,strong) UIImageView *iconImageView;
@property (nonatomic ,strong) UILabel *leftLabel;
@property (nonatomic ,strong) AdminControl *control;

@property (nonatomic ,strong) NSString *title;

@property (nonatomic ,strong) void(^tapBlock)();

#pragma mark 创建
+ (instancetype)initWithModel:(NSString *)title frame:(CGRect)frame;

#pragma mark 刷新view
- (void)resetViewWithModel:(NSString *)title frame:(CGRect)frame;

@end

@interface AdminControl : UIControl
@property (nonatomic ,strong) UILabel *label;
@property (nonatomic ,strong) UIImageView *imageView;

+ (instancetype)initWithModel:(id)model frame:(CGRect)frame;

- (void)resetWithModel:(NSString *)title;

@end
