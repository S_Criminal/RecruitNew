//
//  BirefAdimSectionTitleView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BirefAdimSectionTitleView.h"

@implementation BirefAdimSectionTitleView

#pragma mark 懒加载

- (UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [UIImageView new];
        _iconImageView.image = [UIImage imageNamed:@"求职状态"];
        _iconImageView.widthHeight = XY(W(18),W(18));
        _iconImageView.leftTop = XY(15, 15);
        _iconImageView.centerY = self.height / 2.0f;
    }
    return _iconImageView;
}

- (UILabel *)leftLabel{
    if (_leftLabel == nil) {
        _leftLabel = [UILabel new];
        [GlobalMethod setLabel:_leftLabel widthLimit:0 numLines:0 fontNum:F(17) textColor:COLOR_LABELThreeCOLOR text:@"求职状态"];
        _leftLabel.leftTop = XY(self.iconImageView.right + 5, 0);
//        _leftLabel.text = @"求职状态";
//        [_leftLabel sizeToFit];
        _leftLabel.centerY = self.height / 2.0f;
//        _leftLabel.textColor = COLOR_LABELThreeCOLOR;
//        _leftLabel.font = [UIFont systemFontOfSize:F(17)];
    }
    return _leftLabel;
}

-(AdminControl *)control
{
    if (!_control) {
        _control = [AdminControl initWithModel:@"" frame:CGRectMake(self.leftLabel.right, 0, self.width - self.leftLabel.right, self.height)];
    }
    return _control;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}


//添加subview
- (void)addSubView{
    [self addSubview:self.iconImageView];
    [self addSubview:self.leftLabel];
    [self addSubview:self.control];
}

#pragma mark 创建
+ (instancetype)initWithModel:(NSString *)title frame:(CGRect)frame{
    
    BirefAdimSectionTitleView * view = [[BirefAdimSectionTitleView alloc]initWithFrame:frame];
    [view resetWithModel:title frame:frame];
    return view;
}

#pragma mark 刷新view
- (void)resetWithModel:(NSString *)title frame:(CGRect)frame{
    self.title = title;
    [self addSubView];
    [self.control resetWithModel:title];
}

@end


@implementation AdminControl

- (UILabel *)label{
    if (_label == nil) {
        _label = [UILabel new];
//        _label.rightTop = XY(self.width - self.imageView.width - 15, 0);
        _label.leftTop = XY(0, 0);
        _label.textColor = COLOR_MAINCOLOR;
        _label.font = [UIFont systemFontOfSize:F(15)];
        _label.widthHeight = XY(self.width - self.imageView.width - 15 - 10, 20);
        _label.textAlignment = NSTextAlignmentRight;
    }
    return _label;
}
- (UIImageView *)imageView{
    if (_imageView == nil) {
        _imageView = [UIImageView new];
        _imageView.image = [UIImage imageNamed:@"news_arrow"];
        _imageView.widthHeight = XY(W(10),W(18));
//        _imageView.leftTop = XY(self.width - 15 - 15, 5);
        _imageView.rightTop = XY(self.width - 15 , 5);
        _imageView.centerY = self.height / 2.0f;
    }
    return _imageView;
}


//#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}

////添加subview
- (void)addSubView
{
    [self addSubview:self.imageView];
    [self addSubview:self.label];
}

#pragma mark 创建
+ (instancetype)initWithModel:(NSString *)model frame:(CGRect)frame{
    
    AdminControl * view = [[AdminControl alloc]initWithFrame:frame];
    [view resetWithModel:model];
    return view;
}

#pragma mark 刷新view

- (void)resetWithModel:(NSString *)title
{
//    [self addSubView];
//    [GlobalMethod resetLabel:self.label text:title isWidthLimit:0];
    self.label.text = title;
//    [_label sizeToFit];

    _label.centerY = self.centerY;
}


@end
