//
//  BriefOnlineAdminTableVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefOnlineAdminTableVC.h"
#import "BriefAdminCell.h"
#import "SelectMoreControlView.h"

#import "RequestApi+MyBrief.h"
#import "RequestApi+News.h"
#import "BriefInfoModel.h"
@interface BriefOnlineAdminTableVC ()<UIGestureRecognizerDelegate,RequestDelegate>
@property (nonatomic ,strong) SelectMoreControlView *moreHiddenView;
@property (nonatomic ,strong) NSMutableArray *listOnlineArr;
@property (nonatomic ,strong) NSMutableArray *listDownArr;
@property (nonatomic ,strong) NSMutableArray *listArr;

@property (nonatomic ,strong) NSArray *contentArr;
@property (nonatomic ,strong) NSArray *imageArr;

@property (nonatomic ,strong) NSString *rid;
@property (nonatomic ,assign) NSInteger selectTag;

@end

@implementation BriefOnlineAdminTableVC

-(instancetype)initOnline:(BOOL)online
{
    BriefOnlineAdminTableVC *onlineVC = [BriefOnlineAdminTableVC new];
    onlineVC.onLine = online;
    return onlineVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[BriefAdminCell class] forCellReuseIdentifier:@"BriefAdminCell"];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    //[self setModel];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)setModel
{
    DSWeak;
    
    _listOnlineArr = [NSMutableArray array];
    _listDownArr   = [NSMutableArray array];
    _listArr       = [NSMutableArray array];
    
    [RequestApi getResumeWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil success:^(NSDictionary *response) {
        NSDictionary *dic = response;
        if ([dic[@"code"]isEqualToNumber:@200]) {
            
            NSArray *arr = dic[@"datas0"];
            
            for (NSDictionary *dic in arr)
            {
                if ([dic[@"R_NowState"] isEqualToNumber:@2]) {
                    BriefInfoModel *model = [BriefInfoModel modelObjectWithDictionary:dic];
                    NSString *city = model.rCity;
                    if (!kStringIsEmpty(city))
                    {
                        for (NSString *strCity in [city componentsSeparatedByString:@","])
                        {
                            BriefInfoModel *model = [BriefInfoModel modelObjectWithDictionary:dic];
                            model.rCity = strCity;
                            [weakSelf.listOnlineArr addObject:model];
                        }
                    }
                }else{
                    BriefInfoModel *model = [BriefInfoModel modelObjectWithDictionary:dic];
                    NSString *city = model.rCity;
                    if (!kStringIsEmpty(city))
                    {
                        for (NSString *strCity in [city componentsSeparatedByString:@","])
                        {
                            BriefInfoModel *model = [BriefInfoModel modelObjectWithDictionary:dic];
                            model.rCity = strCity;
                            [weakSelf.listDownArr addObject:model];
                        }
                    }
                }
            }
            
            weakSelf.listArr = weakSelf.onLine ? [NSMutableArray arrayWithArray:weakSelf.listOnlineArr] : [NSMutableArray arrayWithArray:weakSelf.listDownArr];
            
            NSLog(@"%ld",weakSelf.listDownArr.count);
            NSLog(@"%ld",weakSelf.listOnlineArr.count);
            NSLog(@"%ld",weakSelf.listArr.count);
            
            
            if (kArrayIsEmpty(weakSelf.listArr)) {
                [weakSelf.view addSubview:[GlobalMethod backgroundNoDateViewFrame:CGRectMake(0, -20, KWIDTH, self.view.height + 20)]];
            }else{
                [GlobalMethod removeView:(UIView *)[weakSelf.view viewWithTag:1100]];
            }
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSString *errorStr, id mark) {
        
    }];
    
    NSArray *onlineImageArr         = [NSArray arrayWithObjects:@"news_down",@"news_edit",@"news_delete", nil];
    NSArray *downLineImageArr       = [NSArray arrayWithObjects:@"news_put",@"news_edit",@"news_delete", nil];
    NSArray *onlineContentArr       = [NSArray arrayWithObjects:@"下线",@"编辑",@"删除", nil];
    NSArray *downLineContentArr     = [NSArray arrayWithObjects:@"上线",@"编辑",@"删除", nil];
    
    self.imageArr   = self.onLine ? onlineImageArr      : downLineImageArr;
    self.contentArr = self.onLine ? onlineContentArr    : downLineContentArr;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_listArr.count > 0)
    {
        NSLog(@"_listArr.count %ld   %d",_listArr.count,self.onLine);
    }
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BriefAdminCell fetchHeight:_listArr[indexPath.row]];
}

- (BriefAdminCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BriefAdminCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BriefAdminCell" forIndexPath:indexPath];
    [cell resetCellWithModel:_listArr[indexPath.row]];
    cell.moreButton.tag = indexPath.row;
    [cell.moreButton addTarget:self action:@selector(tapMore:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)tapMore:(UIButton *)sender
{
    DSWeak;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    BriefAdminCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [self.tableView convertRect:cell.frame toView:self.view.superview];
    
    BriefInfoModel *model = _listArr[indexPath.row];
    self.rid = [GlobalMethod doubleToString:model.rID];
    self.selectTag = indexPath.row;
    
    if (!_moreHiddenView)
    {
        _moreHiddenView = [SelectMoreControlView initWithModel:nil frame:CGRectMake(0, 0, KWIDTH, KHEIGHT) imageArr:self.imageArr contentArr:self.contentArr];
        _moreHiddenView.tapRemoveBlock = ^(NSInteger i){
            [weakSelf tapRemoveCompany:i];
        };
        [_moreHiddenView setFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        CGFloat top = rectInSuperview.origin.y + rectInSuperview.size.height + self.titleTop - 10 - 10;
        [_moreHiddenView resetViewWithBGViewFrame:top];
        UITapGestureRecognizer *tapHidden = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHiddenMoreView)];
        tapHidden.cancelsTouchesInView = NO;
        tapHidden.delegate = self;
        [_moreHiddenView addGestureRecognizer:tapHidden];
    }
    [self.view.superview.superview addSubview:_moreHiddenView];
    NSLog(@"%@",self.view.superview.superview.superview);
    NSLog(@"%@",self.view.superview.superview);
    NSLog(@"%@",self.view.superview);
}

-(void)tapRemoveCompany:(NSInteger)i
{
    if (i == 10) {
        //改变简历状态
        NSString *status = self.onLine ? @"0" : @"1";
        NSLog(@"%@",status);
        
        [self changeBriefStatus:status];
    }else if (i == 11){
        //编辑
        UITabBarController * tabVC = GB_Nav.viewControllers.firstObject;
        if ([tabVC isKindOfClass:[UITabBarController class]]) {
            tabVC.selectedIndex = 3;
        }
        [GB_Nav popViewControllerAnimated:true];
    }else if (i == 12){
        [self deleteBrief];
    }
    [_moreHiddenView removeFromSuperview];
    _moreHiddenView = nil;
}

#pragma mark 改变简历状态

-(void)changeBriefStatus:(NSString *)status
{
    DSWeak;
    [RequestApi briefOnDownOnlineWithKey:[GlobalData sharedInstance].GB_Key rid:self.rid status:status Delegate:self Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        if ([status isEqualToString:@"1"]) {
            [MBProgressHUD showSuccess:@"发送成功，请等候审核" toView:weakSelf.view];
        }else{
            [MBProgressHUD showSuccess:@"下架成功" toView:weakSelf.view];
        }
        [weakSelf setModel];
    } failure:^(NSString *str) {
        [MBProgressHUD showError:str toView:self.view];
    }];
}

-(void)deleteBrief
{
    DSWeak;
    [RequestApi deleteBriefWithKey:[GlobalData sharedInstance].GB_Key rid:self.rid Delegate:self Success:^(NSDictionary *response) {
        [MBProgressHUD showError:@"删除成功" toView:self.view];
        [weakSelf setModel];
    } failure:^(NSString *str) {
        [MBProgressHUD showError:str toView:self.view];
    }];
}

#pragma mark

-(void)tapHiddenMoreView
{
    [_moreHiddenView removeFromSuperview];
    _moreHiddenView = nil;
}

#pragma mark tap delegate 
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIControl class]]) {
        return NO;
    }
    return true;
}

#pragma mark 

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",scrollView.contentOffset.y);
    UIButton *button = (UIButton *)[self.view.superview.superview viewWithTag:100];
    NSLog(@"%@",button);
    if (scrollView.contentOffset.y <= 50) {
        button.hidden = NO;
    }else{
        button.hidden = YES;
    }
}

#pragma mark scrollview delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    UIButton *button = (UIButton *)[self.view.superview.superview viewWithTag:100];
    button.hidden = NO;
}

/*
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (decelerate) {
        UIButton *button = (UIButton *)[self.view.superview.superview viewWithTag:100];
        button.hidden = YES;
    }
}
*/



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
