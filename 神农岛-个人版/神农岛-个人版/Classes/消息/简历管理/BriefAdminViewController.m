//
//  BriefAdminViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/14.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefAdminViewController.h"

#import "BirefAdimSectionTitleView.h"//titleHeaderView
#import "BriefOnlineAdminTableVC.h"

#import "PositionTrendVC.h"
#import "GeneralPickerView.h"        //自定义pickerView
#import "SliderView.h"

#import "BriefInfoModel.h"
#import "AdimBriefStatusModel.h"

#import "RequestApi+News.h"
#import "RequestApi+Login.h"

@interface BriefAdminViewController ()<GeneralPickerViewDelegate,SliderViewDelegate,UIScrollViewDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) BirefAdimSectionTitleView *sectionView;
@property (nonatomic ,strong) GeneralPickerView *pickerView;
@property (nonatomic ,strong) NSMutableArray *firstArr;
@property (nonatomic ,strong) UIButton *positionTrendButton;


@property (strong, nonatomic) SliderView *sliderView;
@property (nonatomic, strong) UIScrollView *scAll;

@property (nonatomic ,strong) BriefOnlineAdminTableVC *onLine;
@property (nonatomic ,strong) BriefOnlineAdminTableVC *downLine;

@property (nonatomic ,strong) BriefInfoModel *infoModel;

@end

@implementation BriefAdminViewController

- (SliderView *)sliderView{
    if (_sliderView == nil) {
        _sliderView = [[SliderView alloc]initWithModels:@[[ModelBtn modelWithTitle:@"已上线" imageName:@"" tag:0],[ModelBtn modelWithTitle:@"已下线" imageName:@"" tag:1]] frame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, KWIDTH, W(44)) isHasSlider:false isImageLeft:false isFirstOn:true delegate:self];
//        _sliderView = [[SliderView alloc]initWithModels:@[[ModelBtn modelWithTitle:@"已上线"],[ModelBtn modelWithTitle:@"已下线"]] frame:CGRectMake(0, NAVIGATIONBAR_HEIGHT, KWIDTH, W(44)) isHasSlider:false isImageLeft:false delegate:self];
        _sliderView.backgroundColor = [UIColor whiteColor];
    }
    return _sliderView;
}
- (UIScrollView *)scAll{
    if (_scAll == nil) {
        _scAll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.sliderView.bottom +1, KWIDTH, KHEIGHT - self.sliderView.bottom -1)];
        _scAll.contentSize = CGSizeMake(KWIDTH * 2, 0);
        _scAll.backgroundColor = [UIColor clearColor];
        _scAll.delegate = self;
        _scAll.pagingEnabled = true;
        _scAll.showsVerticalScrollIndicator = false;
        _scAll.showsHorizontalScrollIndicator = false;
    }
    return _scAll;
}

-(BirefAdimSectionTitleView *)sectionView
{
    if (!_sectionView) {
        _sectionView = [BirefAdimSectionTitleView initWithModel:@"离职 - 立即到岗" frame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, 45)];
    }
    return _sectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setModel];
    
    [self setBasicView];
    [self setupChildVC];
    [self.view addSubview:self.positionTrendButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchCurrentView];
}

-(UIButton *)positionTrendButton
{
    if (!_positionTrendButton) {
        _positionTrendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_positionTrendButton setFrame:CGRectMake(W(20), KHEIGHT - 40 - W(40), KWIDTH - W(20) * 2, W(40))];
        _positionTrendButton.backgroundColor = COLOR_MAINCOLOR;
        _positionTrendButton.titleLabel.font = [UIFont systemFontOfSize:F(16)];
        _positionTrendButton.tag = 100;
        [_positionTrendButton setTitle:@"＋职位偏好" forState:UIControlStateNormal];
        [_positionTrendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_positionTrendButton addTarget:self action:@selector(tapPrend:) forControlEvents:UIControlEventTouchUpInside];
        [_positionTrendButton setCorner:4];
    }
    return _positionTrendButton;
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"简历管理" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setModel
{
    //self.firstArr = [NSArray arrayWithObjects:@"离职 - 立即到岗",@"在职 - 月底到岗",@"在职 - 考虑机会",@"在职 - 暂不考虑", nil];
    self.firstArr = [NSMutableArray array];
    _infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    [RequestApi statusBriefWithDelegate:self Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        if (arr.count > 0) {
            for (NSDictionary *d in arr)
            {
                AdimBriefStatusModel *model = [AdimBriefStatusModel modelObjectWithDictionary:d];
                if (model.rSID == _infoModel.rStatus) {
                    self.sectionView.control.label.text = model.rSname;
                }
                [self.firstArr addObject:model];
            }
        }
    } failure:^(NSString *str) {
        
    }];
}
#pragma mark 基本控件
-(void)setBasicView
{
    
    
    [self.sectionView.control addTarget:self action:@selector(tapSelectStatus:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sectionView];
    
    [self.view addSubview:self.sliderView];
    [self.view addSubview:self.scAll];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, _sectionView.bottom, KWIDTH, 1)];
    line.backgroundColor = COLOR_SEPARATE_COLOR;
    [self.view addSubview:line];
    
    self.sliderView.y = line.bottom;
    self.scAll.y      = _sliderView.bottom;
    self.scAll.height = KHEIGHT - _sliderView.bottom;
}

#pragma mark 初始化子控制器
- (void)setupChildVC
{
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH * 2, 10)];
    line.backgroundColor = COLOR_BGCOLOR;
    [self.scAll addSubview:line];
    
    self.onLine = [[BriefOnlineAdminTableVC alloc] initOnline:true];
    _onLine.titleTop = _sliderView.bottom + line.height;
    _onLine.view.frame = CGRectMake(0, line.bottom, self.scAll.width, self.scAll.height - line.height);
    [self addChildViewController:_onLine];
    
    self.downLine = [[BriefOnlineAdminTableVC alloc]initOnline:false];
    _downLine.titleTop = _sliderView.bottom + line.height;
    _downLine.view.frame = CGRectMake(KWIDTH, line.bottom, self.scAll.width, self.scAll.height - line.height);
    [self addChildViewController:_downLine];
    
    [self.scAll addSubview:_onLine.view];
    [self.scAll addSubview:_downLine.view];
}

#pragma mark 点击职位偏好
-(void)tapPrend:(UIButton *)sender
{
    PositionTrendVC *trend = [PositionTrendVC new];
    [GB_Nav pushViewController:trend animated:YES];
}

#pragma mark 选择求职状态
-(void)tapSelectStatus:(UIButton *)sender
{
    _pickerView = [[GeneralPickerView alloc]initWithFrame:self.view.frame];
    _infoModel = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    NSString *title;
    NSMutableArray *arr = [NSMutableArray array];
    for (AdimBriefStatusModel *models in self.firstArr) {
        if (models.rSID == _infoModel.rStatus) {
            title = models.rSname;
        }
        [arr addObject:models.rSname];
    }
    NSLog(@"%@",[GlobalMethod doubleToString:_infoModel.uStatus]);
    [_pickerView setUpPickerViewTitle:@"请选择求职状态" selectFirstTitle:title selectSecondTitle:@"" selectModel:1 firstArray:arr secondArray:nil];
    _pickerView.delegate = self;
    [self.view addSubview:_pickerView];
    [self showPickerView]; 
}

/*
 *   @param
 *   0离职-立即到岗
 *   1在职-月底到岗
 *   2在职-考虑机会
 *   3在职-暂不考虑)
 */

-(void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content
{
    _sectionView.control.label.text = content;
    NSInteger status = 0;
    for (AdimBriefStatusModel *model in self.firstArr) {
        if ([model.rSname isEqualToString:content]) {
            status = [[GlobalMethod doubleToString:model.rSID] integerValue];
        }
    }
    DSWeak;
    [RequestApi changeBriefStatusWithKey:[GlobalData sharedInstance].GB_Key status:[NSString stringWithFormat:@"%ld",status] Delegate:nil Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        _infoModel.rStatus = status;
        [GlobalMethod writeStr:[GlobalMethod exchangeModel:[GlobalData sharedInstance].GB_UserModel] forKey:LOCAL_USERMODEL];
    } failure:^(NSString *errorStr) {
        [MBProgressHUD showError:errorStr toView:weakSelf.view];
    }];
    [self hidePickerView];
}


-(void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag
{
    [self hidePickerView];
}

-(void)showPickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    _pickerView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
        _pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hidePickerView
{
    _pickerView.bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _pickerView.bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        _pickerView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark scrollview delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self fetchCurrentView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self fetchCurrentView];
    }
}

- (void)fetchCurrentView {
    // 获取已经滚动的比例
    double ratio = self.scAll.contentOffset.x / KWIDTH;
    int    page  = (int)(ratio + 0.5);
    // scrollview 到page页时 将toolbar调至对应按钮
    [self.sliderView sliderToIndex:page noticeDelegate:NO];
    [[GlobalMethod doubleToString:ratio] isEqualToString:@"0"] ? [_onLine setModel] :[_downLine setModel];
}

#pragma mark slider delegate
- (void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(CustomSliderControl *)control{
    tag == 0 ? [_onLine setModel] :[_downLine setModel];
    [UIView animateWithDuration:0.5 animations:^{
        self.scAll.contentOffset = CGPointMake(KWIDTH * tag, 0);
    } completion:^(BOOL finished) {
        
    }];
}



-(void)dealloc
{
    _pickerView = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
