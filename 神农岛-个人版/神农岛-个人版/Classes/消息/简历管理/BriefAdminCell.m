//
//  BriefAdminCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BriefAdminCell.h"

@implementation BriefAdminCell

- (UILabel *)postionL{
    if (_postionL == nil) {
        _postionL = [UILabel new];
        _postionL.leftTop = XY(15, W(10));
        [GlobalMethod setLabel:_postionL widthLimit:0 numLines:0 fontNum:F(17) textColor:[UIColor blackColor] text:@""];
    }
    return _postionL;
}

- (UILabel *)payL{
    if (_payL == nil) {
        _payL = [UILabel new];
        _payL.leftTop = XY(self.postionL.right + W(25), self.postionL.y);
        [GlobalMethod setLabel:_payL widthLimit:0 numLines:0 fontNum:F(14) textColor:[HexStringColor colorWithHexString:@"F97230"] text:@""];
    }
    return _payL;
}

- (UILabel *)cityL{
    if (_cityL == nil) {
        _cityL = [UILabel new];
        _cityL.leftTop = XY(self.payL.right + W(25), self.postionL.y);
        [GlobalMethod setLabel:_cityL widthLimit:0 numLines:0 fontNum:F(14) textColor:COLOR_LABELSIXCOLOR text:@""];
    }
    return _cityL;
}

-(UIButton *)moreButton{
    if (_moreButton == nil) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _moreButton.tag = 100;
//        [_moreButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_moreButton setImage:[UIImage imageNamed:@"first_More"] forState:UIControlStateNormal];
        _moreButton.titleLabel.font = [UIFont systemFontOfSize:W(18)];
        _moreButton.backgroundColor = [UIColor whiteColor];
        _moreButton.widthHeight = XY(self.width - self.cityL.right + 10,W(40));
    }
    return _moreButton;
}

-(UIView *)line
{
    if (!_line) {
        _line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 1)];
        _line.backgroundColor = COLOR_BGCOLOR;
    }
    return _line;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.postionL];
        [self.contentView addSubview:self.payL];
        [self.contentView addSubview:self.cityL];
        [self.contentView addSubview:self.moreButton];
        [self.contentView addSubview:self.line];
    }
    return self;
}


#pragma mark 获取高度
FETCH_CELL_HEIGHT(BriefAdminCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(BriefInfoModel *)model
{
    NSString *city = model.rCity.length > 10 ? [model.rCity substringToIndex:10] : model.rCity;
    
    [GlobalMethod resetLabel:self.postionL text:model.pName isWidthLimit:NO];
    [GlobalMethod resetLabel:self.payL text:model.rPay isWidthLimit:NO];
    [GlobalMethod resetLabel:self.cityL text:city isWidthLimit:NO];
    
    self.payL.leftTop = XY(self.postionL.right + W(25), 10 + 1);
    self.payL.centerY = self.postionL.centerY;
    
    self.cityL.leftTop = XY(self.payL.right + W(25), 10 + 1);
    
    if (!kStringIsEmpty(model.pName))
    {
        self.cityL.centerY = self.postionL.centerY;
    }
    
    [self.moreButton setFrame:CGRectMake(self.width - 10  - 40 , 1, 40, W(44))];
    
    self.line.y = W(44);

    return W(45);
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
