//
//  SelectMoreControlView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/18.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "SelectMoreControlView.h"

@implementation SelectMoreControlView

#pragma mark 懒加载
- (UIView *)bgView{
    if (_bgView == nil) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.width = KWIDTH - 10 * 2;
        [_bgView setCorner:4];
    }
    return _bgView;
}

-(UIView *)currWindow
{
    if (!_currWindow) {
        _currWindow = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH,KHEIGHT)];
        //        _currWindow.windowLevel = UIWindowLevelAlert + 1;
        _currWindow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        _currWindow.hidden = NO;
    }
    return _currWindow;
}

//画三角形
-(DrawRectTriangleView *)triangleView
{
    if (!_triangleView) {
        _triangleView = [DrawRectTriangleView new];
        _triangleView.i = 0;
        _triangleView.backgroundColor = [UIColor clearColor];
        _triangleView.widthHeight = XY(W(15),W(15));
        [_triangleView drawRect:CGRectZero];
    }
    return _triangleView;
}





#pragma mark 创建
+ (instancetype)initWithModel:(id)model frame:(CGRect)frame imageArr:(NSArray *)imageArr contentArr:(NSArray *)contentArr{
    SelectMoreControlView * view = [[SelectMoreControlView alloc]initWithFrame:frame imageArr:imageArr contentArr:contentArr];
    //    view resetViewWithBGViewFrame:<#(CGFloat)#>
    //    [view resetWithModel:model];
    return view;
}

-(instancetype)initWithFrame:(CGRect)frame imageArr:(NSArray *)imageArr contentArr:(NSArray *)contentArr
{
    self = [super initWithFrame:frame];
    if (self) {
        
//        [self addSubViewsImage:(N)];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        [self addSubViewsImage:imageArr contentArr:contentArr];
//        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

-(void)tapHiddenMoreView
{
    self.hidden = YES;
}

#pragma mark 刷新view
- (void)resetViewWithBGViewFrame:(CGFloat)top
{
    NSLog(@"%f",top);
    [self.bgView setFrame:CGRectMake(10, top + 10 , KWIDTH - 10 * 2, W(45))];
    self.bgView.height = W(45) * 3;
    [self.triangleView setFrame:CGRectMake(self.bgView.right - 28, top, 15, 15)];
    
}

-(void)addSubViewsImage:(NSArray *)imageArr contentArr:(NSArray *)contentArr
{
//    [self addSubview:self.currWindow];
//    [self.currWindow addSubview:self.triangleView];
//    [self.currWindow addSubview:self.bgView];
    [self addSubview:self.triangleView];
    [self addSubview:self.bgView];
    
    for (int i = 0; i < 3; i ++)
    {
        UIControl *control = [UIControl new];
        [control setFrame:CGRectMake(0,(W(44) + 1) * i, self.bgView.width, W(44))];
        control.tag = 10 + i;
        [control addTarget:self action:@selector(tapControl:) forControlEvents:UIControlEventTouchUpInside];
        
        [self setUpImageAndLable:contentArr[i] imageName:imageArr[i] control:control];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, control.bottom, control.width, 1)];
        line.backgroundColor = COLOR_SEPARATE_COLOR;
        [self.bgView addSubview:line];
        [self.bgView addSubview:control];
    }
    self.bgView.height = (W(44) + 1) * 3;
}

-(void)setUpImageAndLable:(NSString *)title imageName:(NSString *)imageName  control:(UIControl *)control
{
    UIImageView * screenImageView= [UIImageView new];
    screenImageView.image = [UIImage imageNamed:imageName];
    screenImageView.leftTop = XY(W(15), 0);
    screenImageView.widthHeight = XY(W(15),W(15));
    
    UILabel *screenLabel = [UILabel new];
    screenLabel.font = [UIFont systemFontOfSize:F(15)];
    screenLabel.leftTop = XY(screenImageView.right + W(10), 10);
    screenLabel.textColor = COLOR_LABELThreeCOLOR;
    screenLabel.text = title;
    [screenLabel sizeToFit];
    
    screenLabel.centerY     = control.height / 2.0f;
    screenImageView.centerY = control.height / 2.0f;
    
    
    [control addSubview:screenImageView];
    [control addSubview:screenLabel];
}

//点击事件
-(void)tapControl:(UIControl *)control
{
    self.tapRemoveBlock(control.tag);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
