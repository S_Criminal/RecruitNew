//
//  NewsTableViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "NewsTableViewController.h"
/** view && cell */
#import "BriefPerfectNumView.h"
#import "NewsBriefFirstCellTableViewCell.h"
#import "NewNewsTableViewCell.h"
#import "KYAlertView.h"
#import "UITabBar+littleRedDotBadge.h"
/** model */
#import "NewsTableListModel.h"
#import "BriefPerfectNumModel.h"
/** VC */
#import "WhoSeeMeVC.h"                  //谁看过我
#import "BriefAdminViewController.h"    //简历管理
#import "PositionSaveVC.h"              //职位收藏
#import "BriefStatusViewController.h"   //简历状态
#import "SmallSeceretaryVC.h"           //小秘书
#import "LoginViewController.h"
/** request */
#import "RequestApi+News.h"

@interface NewsTableViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,NewsSelectFirstDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) BriefPerfectNumView *topView;
@property (nonatomic ,strong) KYAlertView *kyAlertView;
@property (nonatomic ,strong) NewsTableListModel *listModel;
@property (nonatomic ,strong) BriefPerfectNumModel *perfectModel;

@property (nonatomic ,strong) NSArray *listImageArr;

@property (nonatomic ,strong) NSString *seceretaryStr;

@end

@implementation NewsTableViewController{
    CGFloat topHeight;
    BOOL isDisplayTopView;      //简历完整度是否显示  YES 显示 NO隐藏
    
    int _whoSeeMeRedNum;
    int _seceretaryRedNum;
    int _statusRedNum;
    
    NSInteger postNewsNum;            //储存到这界面时  推送未查看的个数
}

#pragma mark 懒加载
-(NewsTableListModel *)listModel
{
    if (!_listModel) {
        _listModel = [[NewsTableListModel alloc]init];
    }
    return _listModel;
}

-(BriefPerfectNumModel *)perfectModel
{
    if (!_perfectModel) {
        _perfectModel = [[BriefPerfectNumModel alloc]init];
    }
    return _perfectModel;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[NewsBriefFirstCellTableViewCell class] forCellReuseIdentifier:@"NewsBriefFirstCellTableViewCell"];
        [_tableView registerClass:[NewNewsTableViewCell class] forCellReuseIdentifier:@"NewNewsTableViewCell"];
    }
    return _tableView;
}


-(KYAlertView *)kyAlertView
{
    if (!_kyAlertView) {
        _kyAlertView = [KYAlertView sharedInstance];
    }
    return _kyAlertView;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    _seceretaryRedNum = 0;
    _whoSeeMeRedNum   = 0;
    _statusRedNum     = 0;
    postNewsNum       = 0;
    
    // Do any additional setup after loading the view.
    [self setUI];
    [self.view addSubview:[BaseNavView initNavTitle:@"消息列表" leftView:nil rightView:nil]];
    [self.view addSubview:[GlobalMethod addNavLine]];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    isDisplayTopView = YES;
    
    self.topView.height = 0;
    self.tableView.y    = NAVIGATION_BarHeight + 1;
    
    [self createBasicView];

}

-(void)setUI
{
    topHeight = W(44);
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.childViewControllers.count == 1) {
        return NO;
    }else{
        
    }
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setModeDisplay:isDisplayTopView];
    [self checkNews];
}

-(void)setModeDisplay:(BOOL)disPlay
{
    
    self.listImageArr = self.listModel.listImageArr;
    self.perfectModel.content = @"资料完整度70%，你的简历无法投递！";
    if (disPlay == NO || kStringIsEmpty([GlobalData sharedInstance].GB_Key))
    {
        self.topView.height = 0;
        self.tableView.y    = NAVIGATION_BarHeight + 1;
        return;
    }
    
    DSWeak;
    [RequestApi getAlertResumeWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        weakSelf.perfectModel.content = [response[@"code"] isEqualToNumber:@200] ? response[@"messge"] : @"";
        weakSelf.topView.model = _perfectModel;
        weakSelf.topView.height = kStringIsEmpty(weakSelf.perfectModel.content) ? 0 : topHeight;
        weakSelf.tableView.y    = kStringIsEmpty(weakSelf.perfectModel.content) ? NAVIGATION_BarHeight + 1: _topView.bottom  ;
    } failure:^(NSString *str) {
        weakSelf.topView.height = 0;
        weakSelf.tableView.y    = NAVIGATION_BarHeight + 1;
    }];
}

-(void)createBasicView
{
    UIView *tableLine = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT + 1, KWIDTH, 1)];
    tableLine.backgroundColor = COLOR_LINESCOLOR;
    [self.view addSubview:tableLine];
    
    self.topView = [[BriefPerfectNumView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(tableLine.frame), KWIDTH, W(44))];
    [self.topView.deleteButton addTarget:self action:@selector(tapDeleteTopView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_topView];
    
    [self.tableView setFrame:CGRectMake(0, CGRectGetMaxY(_topView.frame), KWIDTH, KHEIGHT - NAVIGATIONBAR_HEIGHT - TarBarHeight - 1)];
    _tableView.delegate    = self;
    _tableView.dataSource  = self;
    _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:self.tableView];
}

-(void)tapDeleteTopView:(UIButton *)sender
{
    isDisplayTopView = NO;
    [self setModeDisplay:isDisplayTopView];
}

#pragma mark TableView的代理方法

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _listImageArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat h = [self.tableView cellHeightForIndexPath:indexPath cellContentViewWidth:KWIDTH tableView:self.tableView];
    CGFloat h;
    if (indexPath.row == 0) {
        h = [self.tableView cellHeightForIndexPath:indexPath model:_listModel keyPath:@"model" cellClass:[NewsBriefFirstCellTableViewCell class] contentViewWidth:KWIDTH];
    }else{
        h = [self.tableView cellHeightForIndexPath:indexPath model:_listModel keyPath:@"model" cellClass:[NewNewsTableViewCell class] contentViewWidth:KWIDTH];
    }
    return h;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    }
    if (indexPath.row == 0)
    {
        NewsBriefFirstCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsBriefFirstCellTableViewCell"];
        cell.delegate = self;
        cell.redStatusNum = _statusRedNum;
        return cell;
    }else{
        NewNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewNewsTableViewCell"];
        cell.model = self.listModel;
        NSString *title = _whoSeeMeRedNum == 0 ? @"" : @"有新消息";
        [cell setListImageString:_listImageArr[indexPath.row - 1] ContentString:self.listModel.listTitleArr[indexPath.row - 1] subTitle:title redNum:_whoSeeMeRedNum];
        if (indexPath.row == 2)
        {
            [cell setListImageString:_listImageArr[indexPath.row - 1] ContentString:self.listModel.listTitleArr[indexPath.row - 1] subTitle:_seceretaryStr redNum:_seceretaryRedNum];
        }
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (kStringIsEmpty([GlobalData sharedInstance].GB_Key)) {
        [self showKyalertView];
        return;
    }
    if (indexPath.row == 1)
    {
        WhoSeeMeVC *see = [[WhoSeeMeVC alloc]init];
        [self.navigationController pushViewController:see animated:YES];
    }else if (indexPath.row == 2){
        SmallSeceretaryVC *small = [SmallSeceretaryVC new];
        [self.navigationController pushViewController:small animated:YES];
    }
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_seceretaryRedNum + _whoSeeMeRedNum + _statusRedNum > 0) {
        [self.tabBarController.tabBar showBadgeOnItemIndex:1];
    }else{
        [self.tabBarController.tabBar hideBadgeOnItemIndex:1];
    }
}

-(void)showKyalertView
{
    NSString *message = kObjectIsEmpty([GlobalData sharedInstance].GB_UserModel) ? @"您还未登录，无法进行此操作" : @"未登录状态，请重新登录";
    NSString *cancle = kObjectIsEmpty([GlobalData sharedInstance].GB_UserModel) ? @"登录" : @"重新登录";
    [self.kyAlertView showAlertView:@"提示" message:message subBottonTitle:@"取消" cancelButtonTitle:cancle handler:^(AlertViewClickBottonType bottonType) {
        if (bottonType == AlertViewClickBottonTypeCancelButton)
        {
            LoginViewController *login = [LoginViewController new];
            [self.navigationController pushViewController:login animated:YES];
        }
    }];
}

-(void)protocol:(UIControl *)control tag:(NSInteger)tag
{
    if (kStringIsEmpty([GlobalData sharedInstance].GB_Key)) {
        [self showKyalertView];
        return;
    }
    if (tag == 10) {
        BriefStatusViewController *status = [BriefStatusViewController new];
        [self.navigationController pushViewController:status animated:YES];
    }else if (tag == 11) {
        BriefAdminViewController *brief = [BriefAdminViewController new];
        [self.navigationController pushViewController:brief animated:YES];
    }else if (tag == 12){
        PositionSaveVC *save = [PositionSaveVC new];
        [self.navigationController pushViewController:save animated:YES];
    }
}

#pragma mark 消息 推送 红点
//先检查是否有推送
-(void)checkNews
{
    //角标清零
    [GlobalMethod zeroIcon];
    
    
    
//    NSArray *arr =  [[GlobalData sharedInstance].GB_PostNews componentsSeparatedByString:@","];
//    
//    if (arr.count == 0)
//    {
//        return;
//    }
//    
//    if (postNewsNum == arr.count)
//    {
//        return;
//    }
//    
//    postNewsNum = arr.count;

    //谁看过我
    DSWeak;
    [RequestApi postSeeRecordNumWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil Success:^(NSDictionary *response) {
        _whoSeeMeRedNum = [response[@"datas"] intValue];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorStr) {
        
    }];
    
    //小秘书未读数
    [RequestApi postSeceretaryNumberWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil Success:^(NSDictionary *response) {
        _seceretaryRedNum = [response[@"datas"] intValue];
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorStr) {
        
    }];
    
    [RequestApi postBriefStatusWithKey:[GlobalData sharedInstance].GB_Key Delegate:nil Success:^(NSDictionary *response) {
        _statusRedNum = [response[@"datas0"]intValue] + [response[@"datas1"]intValue] + [response[@"datas2"]intValue] + [response[@"datas3"]intValue] ;
        [weakSelf.tableView reloadData];
    } failure:^(NSString *errorStr) {
        
    }];
    
    
    [RequestApi secretaryMsgListWithKey:[GlobalData sharedInstance].GB_Key top:@"1" Delegate:nil Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        if (arr.count > 0)
        {
            NSDictionary *d = [arr objectAtIndex:0];
            _seceretaryStr = [NSString stringWithFormat:@"%@",d[@"Contents"]];
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSString *errorStr) {
        
    }];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
