//
//  SeceretaryPostNewsView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol didPostDelegate <NSObject>

-(void)postNews;

@end

@interface SeceretaryPostNewsView : UIView

@property (nonatomic ,strong) UITextView *textView;
@property (nonatomic ,strong) UIButton *postButton;
@property (nonatomic ,weak) id<didPostDelegate>delegate;

#pragma mark 创建
+ (instancetype)initWithModel:(id)model;

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model;


@end
