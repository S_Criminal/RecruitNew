//
//  SeceretaryCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "SeceretaryCell.h"
#import "BriefInfoModel.h"

@implementation SeceretaryCell{
    CGFloat imageWidth;
}

#pragma mark 懒加载

- (UILabel *)timeLabel{
    if (_timeLabel == nil) {
        _timeLabel = [UILabel new];
        [GlobalMethod setLabel:_timeLabel widthLimit:0 numLines:0 fontNum:F(13) textColor:COLOR_LABELCOLOR text:@""];
    }
    return _timeLabel;
}
- (UIView *)bgView{
    if (_bgView == nil) {
        _bgView = [UIView new];
        [_bgView setCorner:3];
    }
    return _bgView;
}

- (UILabel *)contentLabel{
    if (_contentLabel == nil) {
        _contentLabel = [UILabel new];
        [GlobalMethod setLabel:_contentLabel widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELCOLOR text:@""];
    }
    return _contentLabel;
}
- (UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [UIImageView new];
        _iconImageView.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _iconImageView.widthHeight = XY(W(15),W(15));
    }
    return _iconImageView;
}

-(UIImageView *)sharpImageView
{
    if (!_sharpImageView) {
        _sharpImageView = [UIImageView new];
    }
    return _sharpImageView;
}


#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = COLOR_BGCOLOR;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.dateModel = [DateArrModel new];
        [self setUI];
        
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.contentLabel];
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.sharpImageView];
    }
    return self;
}

-(void)setUI
{
    imageWidth = W(40);
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(SeceretaryCell)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(SeceretaryModel *)model
{
    NSString *status = [GlobalMethod doubleToString:model.type];
    //头像
    self.iconImageView.widthHeight = XY(imageWidth, imageWidth);
    
    //获取内容label的高度
    self.contentLabel.width = KWIDTH - imageWidth * 2 - W(15) * 4;
    [GlobalMethod resetLabel:self.contentLabel text:model.contents isWidthLimit:1];
    
    CGFloat h = [GlobalMethod fetchHeightFromLabel:self.contentLabel];
    
    //两行以上  设置行间距
    if ( h > 30 )
    {
        self.contentLabel = [GlobalMethod setAttributeLabel:self.contentLabel content:model.contents width:self.contentLabel.width];
    }
    
    self.contentLabel.leftTop = XY(W(10),W(10));
    self.bgView.height = self.contentLabel.height + W(10) * 2;
    self.bgView.width  = self.contentLabel.width + W(10) * 2;
    
    //时间
    //NSString *time = model.createDate.length > 10 ? [model.createDate substringWithRange:NSMakeRange(11, 8)] : model.createDate;
    //[GlobalMethod resetLabel:self.timeLabel text:time isWidthLimit:0];
    if (self.timeLabel.hidden) {
        self.timeLabel.y = 0;
        self.timeLabel.height = 0;
    }else{
        self.timeLabel.leftTop = XY(W(15),W(15));
    }
    
    self.timeLabel.centerX = KWIDTH / 2.0f;
    
    //多边形
    self.sharpImageView.widthHeight = XY(8, 12);
    //区分角色
    [status isEqualToString:@"0"] ? [self setSelfView] : [self setSepeartyView];
    
    return self.bgView.bottom;
}

//小秘书
-(void)setSepeartyView
{
    self.iconImageView.leftTop = XY(W(15),self.timeLabel.bottom + W(15));
    self.iconImageView.image = [UIImage imageNamed:@"矢量智能对象"];
    
    //对话框的背景view
    self.bgView.leftTop = XY(self.iconImageView.x + imageWidth + W(15),self.iconImageView.y);
    self.bgView.backgroundColor = [UIColor whiteColor];
    
    self.sharpImageView.leftTop = XY(self.bgView.left - 7.5f, 0);
    self.sharpImageView.centerY = self.iconImageView.centerY - 2;
    self.sharpImageView.image = [UIImage imageNamed:@"white多边"];
}

//自己
-(void)setSelfView
{
    BriefInfoModel *model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    self.iconImageView.leftTop = XY(KWIDTH - imageWidth - W(15),self.timeLabel.bottom + W(15));
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.uHead] placeholderImage:[UIImage imageNamed:@"个人默认头像"]];
    [self.iconImageView setCorner:3];
    
    //对话框的背景view
    self.bgView.rightTop = XY(self.iconImageView.left - W(15),self.iconImageView.y);
    self.bgView.backgroundColor = COLOR_MAINCOLOR;
    
    self.sharpImageView.leftTop = XY(self.bgView.right - 0.5, 0);
    self.sharpImageView.centerY = self.iconImageView.centerY - 2;
    self.sharpImageView.image = [UIImage imageNamed:@"green多边"];
}

-(void)changeTime:(NSString *)firstTime
{
    
}

//比较两个cell的time，时间相近第一个row的time隐藏

-(void)checkDisplayTime:(NSArray *)listArr indexRow:(NSInteger)indexRow indexSecondRow:(NSInteger)indexSecondRow
{
    if (indexSecondRow == 0)
    {
        SeceretaryModel *model       = listArr.firstObject;
        if ([[model.createDate substringWithRange:NSMakeRange(8, 2)] integerValue] == self.dateModel.nowDay)
        {
            self.timeLabel.text = [model.createDate substringWithRange:NSMakeRange(11, 8)];
        }else{
            self.timeLabel.text = [model.createDate substringToIndex:10];
        }
        [GlobalMethod resetLabel:self.timeLabel text:self.timeLabel.text isWidthLimit:0];
        self.timeLabel.centerX = KWIDTH / 2;
        self.timeLabel.hidden = NO;
        return;
    }
    
    SeceretaryModel *secondModel       = listArr[indexSecondRow];
    
    if (kStringIsEmpty(secondModel.createDate))
    {
        return;
    }
    
    NSLog(@"%@",secondModel.createDate);
    NSString *dayTime = [secondModel.createDate substringToIndex:10];//日期
    NSString *minTime = [secondModel.createDate substringWithRange:NSMakeRange(11, 8)];//时分秒
    
    NSUInteger secondYear = [self getCalendatarModelIndex:1 time:secondModel.createDate];
    NSUInteger secondMonth = [self getCalendatarModelIndex:2 time:secondModel.createDate];
    NSUInteger secondDay = [self getCalendatarModelIndex:3 time:secondModel.createDate];
    NSUInteger hour     = [self getCalendatarModelIndex:4 time:secondModel.createDate];
    NSUInteger minute   = [self getCalendatarModelIndex:5 time:secondModel.createDate];
    
    
    NSLog(@"%ld====%ld =====%ld",secondYear,secondMonth,secondDay);
    
#pragma mark  和当前日期的比较
    
    self.timeLabel.text = dayTime;
    
    //先比较年份
    if (secondYear  < self.dateModel.nowYear) {
        self.timeLabel.text = dayTime;
    }else if ([[secondModel.createDate substringWithRange:NSMakeRange(5, 2)] integerValue] < self.dateModel.nowMonth )
    {
        self.timeLabel.text = dayTime;
        //再比较月份
        NSLog(@"%@",[secondModel.createDate substringWithRange:NSMakeRange(5, 2)]);
    }else if ([[secondModel.createDate substringWithRange:NSMakeRange(8, 2)] integerValue] < self.dateModel.nowDay )
    {
        //比较日期
        NSLog(@"%@",[secondModel.createDate substringWithRange:NSMakeRange(8, 2)]);
        self.timeLabel.text = dayTime;
    }else if (hour < self.dateModel.nowHour){
        self.timeLabel.text = minTime;
    }else{
        self.timeLabel.text = minTime;
    }
    
    //重置label frame
    
    [GlobalMethod resetLabel:self.timeLabel text:self.timeLabel.text isWidthLimit:0];
    
    self.timeLabel.centerX = KWIDTH / 2;
    
    
    
    
#pragma mark  前后两个日期的比较
    
    
    //if (indexSecondRow >= listArr.count)
    //{
    //    return ;
    //}
    SeceretaryModel *model = listArr[indexRow];
    NSUInteger firstYear = [self getCalendatarModelIndex:1 time:model.createDate];
    NSUInteger firstMonth = [self getCalendatarModelIndex:2 time:model.createDate];
    NSUInteger firstDay  = [self getCalendatarModelIndex:3 time:model.createDate];
    NSUInteger firstHour = [self getCalendatarModelIndex:4 time:model.createDate];
    NSUInteger firstMinute = [self getCalendatarModelIndex:5 time:model.createDate];
    
    if (firstYear  == secondYear && firstMonth == secondMonth && firstDay == secondDay  )
    {
        self.timeLabel.hidden = YES;
    }else{
        self.timeLabel.hidden = NO;
    }
    //先与当前日期判断
    if (secondDay == self.dateModel.nowDay)
    {
        if (hour == firstHour && minute == firstMinute) {
            self.timeLabel.hidden = YES;
        }else{
            self.timeLabel.hidden = NO;
        }
    }
    
}


-(NSUInteger)getCalendatarModelIndex:(NSInteger)index time:(NSString *)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    time = [[time stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringToIndex:19];
    NSDate *now = [dateFormatter dateFromString:time];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:now];
    NSUInteger nowDate;
    if (index == 1)
    {
        nowDate = [dateComponent year];
    }else if (index == 2){
        nowDate = [dateComponent month];
    }else if (index == 3){
        nowDate = [dateComponent day];
    }else if (index == 4){
        nowDate = [dateComponent hour];
    }else if (index == 5){
        nowDate = [dateComponent minute];
    }
    
    return nowDate;
}

@end
