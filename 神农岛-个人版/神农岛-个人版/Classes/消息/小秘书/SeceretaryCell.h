//
//  SeceretaryCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeceretaryModel.h"
#import "DateArrModel.h"
@interface SeceretaryCell : UITableViewCell

@property (nonatomic ,strong) UILabel *timeLabel;
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UILabel *contentLabel;
@property (nonatomic ,strong) UIImageView *iconImageView;

@property (nonatomic ,strong) UIImageView *sharpImageView;

@property (nonatomic ,strong) DateArrModel *dateModel;


#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(SeceretaryModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(SeceretaryModel *)model;

-(void)checkDisplayTime:(NSArray *)listArr indexRow:(NSInteger)indexRow indexSecondRow:(NSInteger)indexSecondRow;

@end
