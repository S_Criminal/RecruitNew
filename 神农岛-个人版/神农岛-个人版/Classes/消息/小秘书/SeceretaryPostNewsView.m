//
//  SeceretaryPostNewsView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "SeceretaryPostNewsView.h"

@implementation SeceretaryPostNewsView

#pragma mark 懒加载

- (UITextView *)textView{
    if (!_textView) {
        _textView = [UITextView new];
        _textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
        _textView.keyboardType = UIKeyboardTypeDefault;
        _textView.font = [UIFont systemFontOfSize:F(16)];
        [_textView setCorner:4];
        _textView.layer.borderWidth = 1;
        _textView.layer.borderColor = [HexStringColor colorWithHexString:@"bfbfbf"].CGColor;
        _textView.returnKeyType = UIReturnKeyDone;
    }
    return _textView;
}
-(UIButton *)postButton{
    if (_postButton == nil) {
        _postButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _postButton.tag = 1;
        [_postButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        _postButton.backgroundColor = COLOR_BGCOLOR;
        [_postButton setTitleColor:COLOR_MAINCOLOR forState:UIControlStateNormal];
        [_postButton setTitle:@"发送" forState:UIControlStateNormal];
        _postButton.titleLabel.font = [UIFont systemFontOfSize:W(17)];
        [GlobalMethod setRoundView:_postButton color:[UIColor blackColor] numRound:5 width:0.5];
        _postButton.widthHeight = XY(KWIDTH - W(30),W(40));
    }
    return _postButton;
}

#pragma mark 初始化
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubView];
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubView];
    }
    return self;
}

//添加subview
- (void)addSubView{
    [self addSubview:self.textView];
    [self addSubview:self.postButton];
}

#pragma mark 创建
+ (instancetype)initWithModel:(id)model{
    SeceretaryPostNewsView * view = [SeceretaryPostNewsView new];
    [view resetViewWithModel:model];
    return view;
}

#pragma mark 刷新view
- (void)resetViewWithModel:(id)model
{
    //刷新view
    self.textView.widthHeight = XY(KWIDTH - W(70) - W(10) * 2 - W(15), W(33));
    self.textView.leftTop = XY(W(15),W(15));
    
    self.postButton.widthHeight = XY(W(70), W(33));
    self.postButton.leftTop = XY(self.textView.right + W(15),0);
    
    self.textView.centerY = self.height / 2;;
    self.postButton.centerY = self.height / 2;
}

#pragma mark 点击事件
- (void)btnClick:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(postNews)]) {
        [self.delegate postNews];
    }
}
@end
