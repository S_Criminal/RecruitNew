//
//  SmallSeceretaryVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/10.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "SmallSeceretaryVC.h"
#import "SeceretaryCell.h"
#import "SeceretaryModel.h"
#import "RequestApi+News.h"

#import "SeceretaryPostNewsView.h"
@interface SmallSeceretaryVC ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,didPostDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *listArr;

@property (nonatomic ,assign) NSInteger pageIndex;
@property (nonatomic ,strong) NSString *old;

@property (nonatomic ,strong) SeceretaryPostNewsView *footerView;



@property (nonatomic ,strong) UIView *bgView;

@end

@implementation SmallSeceretaryVC{
    CGFloat textViewY;
    CGFloat textViewHeight;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = COLOR_BGCOLOR;
    }
    return _bgView;
}

-(SeceretaryPostNewsView *)footerView
{
    if (!_footerView) {
        _footerView = [SeceretaryPostNewsView new];
        _footerView.textView.delegate = self;
        _footerView.delegate = self;
    }
    return _footerView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _pageIndex = 1;
    
    
    [self setBasicView];
    [self setFooterView];
    [self setUI];
    [self setNav];
    [self setModelIndex:_pageIndex];
    [self setRefresh];
    
}

-(void)setNav
{
    DSWeak;
    BaseNavView *navView = [BaseNavView initNavTitle:@"小秘书" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"" rightBlock:^{
        
    }];
    navView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:navView];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setBasicView
{
    [self.view addSubview:self.bgView];
    self.tableView.backgroundColor = COLOR_BGCOLOR;
    [self.bgView addSubview:self.tableView];
    [self.bgView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - 1 - NAVIGATION_BarHeight)];
    [self.tableView setFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1 - W(50) - W(10))];
    [self.tableView registerClass:[SeceretaryCell class] forCellReuseIdentifier:@"SeceretaryCell"];
}

-(void)setFooterView
{
    [self.footerView setFrame:CGRectMake(0, self.bgView.height - W(50), KWIDTH, W(50))];
    [self.footerView resetViewWithModel:nil];
    [self.bgView addSubview:self.footerView];

}
#pragma mark 刷新 加载更多
-(void)setRefresh
{
    [self refreshTableView];    //下拉刷新
}

-(void)refreshTableView
{
    //    [self.firstTableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    //    [header beginRefreshing];
    
    // 设置header
    self.tableView.mj_header = header;
}

//结束刷新
-(void)loadNewData
{
    _pageIndex ++ ;
    [self setModelIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

-(void)setUI
{
    textViewY = self.footerView.textView.y;
    textViewHeight = self.footerView.textView.height;
}

-(void)setModelIndex:(NSInteger)pageIndex
{
    self.footerView.textView.y = textViewY;
    self.footerView.textView.height = textViewHeight;
    if (pageIndex == 1) {
        self.old = @"0";
        self.listArr = [NSMutableArray array];
    }
    DSWeak;
    [RequestApi getSmallSecepartyWithKey:[GlobalData sharedInstance].GB_Key top:@"5" stid:self.old Delegate:self Success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        if (arr.count > 0) {
            for (NSDictionary *d in arr)
            {
                SeceretaryModel *model = [SeceretaryModel modelObjectWithDictionary:d];
                [weakSelf.listArr addObject:model];
            }
            
            SeceretaryModel *model = _listArr.lastObject;
            weakSelf.old = [GlobalMethod doubleToString:model.iDProperty];
            
            NSArray *result = [weakSelf.listArr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                SeceretaryModel *model1 = obj1;
                SeceretaryModel *model2 = obj2;
                
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                
                NSNumber *number1 = [NSNumber numberWithDouble:model1.iDProperty];
                NSNumber *number2 = [NSNumber numberWithDouble:model2.iDProperty];
                
//                NSNumber *number1 = [numberFormatter numberFromString:model1.createDate];
//                NSNumber *number2 = [numberFormatter numberFromString:model2.createDate];
                
                NSComparisonResult result = [number1 compare:number2];
                
                return result == NSOrderedDescending; // 升序
                //    return result == NSOrderedAscending;  // 降序
            }];
            NSLog(@"resultresult%@",result);
            
            weakSelf.listArr = [NSMutableArray arrayWithArray:result];
        }else{
            if (pageIndex != 1)
            {
                [MBProgressHUD showError:@"没有更多数据" toView:weakSelf.view];
            }
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSString *str) {
        
    }];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static SeceretaryCell * cell;
    if (cell == nil) {
        cell = [SeceretaryCell new];
    }
    [cell checkDisplayTime:_listArr indexRow:indexPath.row - 1 indexSecondRow:indexPath.row];
    CGFloat h = [cell resetCellWithModel:_listArr[indexPath.row]];
    if (indexPath.row == 0) {
        NSLog(@"h===%f",h);
        
    }
    return h;
}

-(SeceretaryCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SeceretaryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SeceretaryCell"];
    [cell checkDisplayTime:_listArr indexRow:indexPath.row - 1 indexSecondRow:indexPath.row];
    [cell resetCellWithModel:_listArr[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.bgView endEditing:YES];
}

#pragma mark 点击发送按钮

-(void)postNews
{
    DSWeak;
    [RequestApi postSmallSeceretaryNewsWithKey:[GlobalData sharedInstance].GB_Key contents:self.footerView.textView.text Delegate:self Success:^(NSDictionary *response) {
        weakSelf.pageIndex = 1;
        [weakSelf setModelIndex:weakSelf.pageIndex];
        [weakSelf.bgView endEditing:YES];
        weakSelf.footerView.textView.text = @"";
//        NSIndexPath *indePath = [NSIndexPath indexPathForRow:_listArr.count - 1 inSection:0];
//        [weakSelf.tableView scrollToRowAtIndexPath:indePath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    } failure:^(NSString *str) {
        [MBProgressHUD showError:@"发送失败" toView:weakSelf.view];
    }];
}


#pragma mark textview的代理方法

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == self.footerView.textView)
    {
        // 订阅键盘通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarShowss:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboarHidess:) name:UIKeyboardWillHideNotification object:nil];
    }
    return YES;
}

//textView  return键
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    CGFloat h = [GlobalMethod fetchHeightFromString:textView.text isLimitWidth:textView.width font:textView.font];
    if (h > 25 && h < 45)
    {
        textView.y = 0 ;
        textView.height = h + W(10);
    }else if (h > 45 && h < 60)
    {
        textView.y = - 20 ;
        textView.height = h + W(10) * 2;
    }
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}



#pragma mark --- 弹出键盘 ---
- (void)keyboarShowss:(NSNotification *)notification
{
    NSLog(@"%@",notification);
    NSDictionary *userInfo = notification.userInfo;
    CGRect board = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    if (board.origin.y == self.bgView.height) {
        
        [UIView animateWithDuration:duration animations:^{
            
            self.bgView.transform = CGAffineTransformIdentity;
            
        }];
        
    }else{
        
        [UIView animateWithDuration:duration animations:^{
            
            self.bgView.transform = CGAffineTransformMakeTranslation(0, - board.size.height);
            
        }];
    }
    NSLog(@"%f",board.size.height);
}


#pragma mark --- 收起键盘 ---

- (void)keyboarHidess:(NSNotification *)notification
{
    
    NSDictionary *userInfo = notification.userInfo;
    CGRect board = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    if (board.origin.y == KHEIGHT) {
        
        [UIView animateWithDuration:duration animations:^{
            
            self.bgView.transform = CGAffineTransformIdentity;
            
        }];
    }else{
        [UIView animateWithDuration:duration animations:^{
            
            self.bgView.transform = CGAffineTransformMakeTranslation(0, board.size.height);
            
        }];
    }
}




-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
