//
//  DiDiCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "DiDiCell.h"
#import "DiDi+CoreDataClass.h"

@interface DiDiCell ()
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *contentL;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation DiDiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.view.layer.cornerRadius = 6;
    self.view.layer.masksToBounds = YES;
    [self setUI];
}

-(void)setUI
{
    
    if (KWIDTH == 320)
    {
        self.backViewRightConstraint.constant = 13;
        self.backViewLeftConstraint.constant = 13;
        self.titleTopConstraint.constant = 0;
        self.titleBottomConstraint.constant = 0;
        self.titleLeftConstraint.constant =7;
        self.titleRightConstraint.constant = 7;
        self.contentTopConstraint.constant = 7;
        self.contentBottomConstraint.constant = 7;
        self.allTopConstraint.constant = 6;
        self.allBottomConstraint.constant = 6;
        self.titleL.numberOfLines = 1;
        
        self.titleL.font = [UIFont systemFontOfSize:16];
        self.contentL.font = [UIFont systemFontOfSize:13];
        self.imageHeightConstraint.constant = 150;
    }
}

-(void)setCellInfoWithModel:(DiDi *)model
{
    
    NSString *time = [[model.nTime stringByReplacingOccurrencesOfString:@"T" withString:@" "]substringToIndex:11];
    self.timeLabel.text = time;//日期
    NSString *icon = [NSString stringWithFormat:@"%@",model.nImage];
    [self.icon sd_setImageWithURL:[NSURL URLWithString:icon]];//logo
    self.titleL.text = [NSString stringWithFormat:@"%@",model.nTitle];
    
    _contentL.text = [NSString stringWithFormat:@"%@",model.nContent];
    //    _contentL.numberOfLines = 2;
    //    _contentL.lineBreakMode = NSLineBreakByWordWrapping;
    //    NSString *content = [NSString stringWithFormat:@"%@",model.nContent];
    //    //label设置行间距
    //    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:content];
    //    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    //    [paragraphStyle1 setLineSpacing:lineSpacing];
    //    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [content length])];
    //    [_contentL setAttributedText:attributedString1];
    //    [_contentL sizeToFit];
    //    _contentL.numberOfLines = 2;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
