//
//  DiDiCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiDiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;

@end
