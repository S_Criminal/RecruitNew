//
//  viviTable.m
//  神农岛
//
//  Created by 宋晨光 on 16/4/4.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "viviTable.h"
#import "all.pch"
#import "User.h"
#import "DiDiManager.h"
#import "DiDi.h"
#import "UITableViewCell+LDSetCellInfo.h"
#import "PushNewsModel.h"

#import "DiDiWebView.h"

#import "DiDiHistoryViewController.h"//老刀didi历史记录

#import "MJRefresh.h"

@interface viviTable ()

@property(strong,nonatomic)NSArray*arr;
@property(strong,nonatomic)UITableView *tableView;
@property(strong,nonatomic)DiDiManager *manager;
//coreData
@property (nonatomic)NSInteger pageIndex;
@property (nonatomic,strong)NSFetchedResultsController * fetchedResultsController;
@property (nonatomic ,strong)NSManagedObjectContext * managedObjectContext;
@property(strong,nonatomic)NSMutableArray *listArr;
@property (nonatomic ,strong)User *user;

@property (nonatomic ,strong)NSManagedObjectContext * context;

@property (nonatomic ,strong)NSString *lastID;

@end

@implementation viviTable{
    UIView *vi;
    NSInteger pageIndex;
    NSString *footerStr;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.pageIndex = 1;
    
    _listArr = [NSMutableArray array];
    //配置CoreData
    UIApplication *app = [UIApplication sharedApplication];
    id delegate = app.delegate;
    self.managedObjectContext = [delegate managedObjectContext];
    
    
    self.view.backgroundColor = [hexStringColor colorWithHexString:backColor];
    self.title = @"职场";
    _user = [User shareWithInfo];
    [self setNav];
    [self createTableView];
    
    [self requestPositionListWithPageIndex:_pageIndex];
    footerStr = @"上拉显示下10条";
    [self setMoreFresh];//上拉加载
}

#pragma mark 上拉加载更多
-(void)setMoreFresh{
    //上拉加载更多
    
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    
    if ([footerStr isEqualToString:@"已无更多数据"]) {
        [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    }else{
        [footer setTitle:footerStr forState:MJRefreshStatePulling];
    }
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // 忽略掉底部inset
    self.tableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

-(void)loadMoreData
{
    __block __weak viviTable *weak_self = self;
    weak_self.pageIndex++;
    [weak_self requestPositionListWithPageIndex:self.pageIndex];
    
    [self.tableView.mj_footer endRefreshing];
    
}

-(void)setNav
{
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"历史" style:UIBarButtonItemStylePlain target:self action:@selector(didiHistory:)];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:rightBarButtonFont5s], NSFontAttributeName, nil] forState:UIControlStateNormal];
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    CGFloat font = 18;
    if (kWidth == 320) {
        font = 17;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                      NSFontAttributeName:[UIFont systemFontOfSize:font]
                                                                      }];
}

-(void)didiHistory:(UIBarButtonItem *)sender
{
    
    self.hidesBottomBarWhenPushed = YES;
    DiDiHistoryViewController *history = [[DiDiHistoryViewController alloc]init];
    [self.navigationController pushViewController:history animated:YES];
    self.hidesBottomBarWhenPushed = NO;
    
}



#pragma mark coredata
-(void)setModel
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DiDi"];
    NSError * error;
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"nTime" ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    NSPredicate* predicate;
    
    predicate = [NSPredicate predicateWithFormat:@"status == %@",@"0"];
//    if ([_user.judgeID isEqualToString:@"findWork"]) {
//        predicate = [NSPredicate predicateWithFormat:@"status == %@",@"0"];
//    }else if ([_user.judgeID isEqualToString:@"findPeople"]){
//        predicate = [NSPredicate predicateWithFormat:@"status == %@",@"1"];
//    }
    
    request.predicate = predicate;
    NSArray *resultAry = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if (resultAry.count == 0)
    {
        vi.hidden = NO;
    }
    else
    {
        vi.hidden = YES;
        self.listArr = @[].mutableCopy;
        [self.listArr addObjectsFromArray:resultAry];
    }
    
    NSLog(@"%@",_listArr);
    
    [self.tableView reloadData];
    //    [self.tableView.mj_footer endRefreshing];
}

-(void)createTableView
{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kWidth, kHeight) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [hexStringColor colorWithHexString:backColor];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300;
    [self.view addSubview:self.tableView];
    [self displayNoHave];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(tapBack)];
    
    self.navigationController.navigationBarHidden = NO;
    
}
-(void)tapBack
{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]init];
    }
    
    cell = [[[NSBundle mainBundle]loadNibNamed:@"laodaoCell" owner:nil options:nil]lastObject];
    
    DiDi *model  = [_listArr objectAtIndex:indexPath.row];
    NSLog(@"%@",_listArr);
    
    [cell setCellInfoWithModel:model];
    // Configure the cell...
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     DiDi *model  = [_listArr objectAtIndex:indexPath.row];
    _manager.webUrl = model.nAddress;
    _manager.title = model.nTitle;
    _manager.imageUrl = model.nImage;
    self.hidesBottomBarWhenPushed = YES;
    DiDiWebView *didi = [[DiDiWebView alloc]init];
    [self.navigationController pushViewController:didi animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}


-(void)requestPositionListWithPageIndex:(NSInteger)pageindex
{
    _manager = [DiDiManager share];
//    [self.manager postDiDi];
    
    __weak __block viviTable * weak_self = self;
    
    
    [self.manager getNoticesListWithTop:@"10" success:^(NSDictionary *dic)
     {
         
     } PageIndex:pageindex BlockHandelDoJobSuccess:^(BOOL success)
     {
         if (success)
         {
             [self setModel];
         }
     }];
    
//    [weak_self.manager getHistoryDiDiListWithTop:@"10" nid:@"0" type:@"0" success:^(NSDictionary *dic) {
//        
//        if ([dic[@"code"]isEqualToNumber:@200])
//        {
//            NSArray *arr = dic[@"datas"];
//            if (arr.count > 0)
//            {
//                 weak_self.lastID  = [NSString stringWithFormat:@"%@",arr.lastObject[@"ID"]];
//                if (pageindex == 1)
//                {
//                    [weak_self deletePositionModelsBlockHandelDoJobSuccess:^(BOOL success) {
//                        [self inseartPositionArr:arr];
//                    }];
//                }else{
//                    [weak_self inseartPositionArr:arr];
//                }
//            }
//           
//        }
//    }];
//    
    
    
}

-(void)inseartPositionArr:(NSArray *)datas
{
    for (NSDictionary * dic in datas)
    {
        
        DiDi * model = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DiDi class]) inManagedObjectContext:self.context];
        
        model.nImage = [NSString stringWithFormat:@"%@",dic[@"N_Img"]];
        model.nTitle = [NSString stringWithFormat:@"%@",dic[@"N_Title"]];
        model.nTime = [NSString stringWithFormat:@"%@",dic[@"CreateDate"]];
        model.nContent = [NSString stringWithFormat:@"%@",dic[@"N_Description"]];
        model.nID = [NSString stringWithFormat:@"%@",dic[@"ID"]];
        model.status = @"0";
        model.nAddress = [NSString stringWithFormat:@"%@",dic[@"N_Url"]];
        
        NSLog(@"%@",model);
        [self.context save:nil];
    }
}

-(void)deletePositionModelsBlockHandelDoJobSuccess:(DidJobDoSuccess)isSuccess
{
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([DiDi class])];
    NSSortDescriptor * sortD = [NSSortDescriptor sortDescriptorWithKey:@"nID" ascending:NO];
    [request setSortDescriptors:@[sortD]];
    
    NSError * error;
    NSArray * result = [self.context executeFetchRequest:request error:&error];
    if (result ==nil) {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }
    if (result.lastObject) {
        for (int i = 0; i<result.count; i++) {
            [self.context deleteObject:result[i]];
        }
    }
    if (![self.context save:&error]) {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }else
    {
        //        NSLog(@"已经删除本地Models");
        isSuccess(YES);
    }
    
}

#pragma mark 没有数据时显示(no have datas display)

-(void)displayNoHave
{
    vi = [[UIView alloc]initWithFrame:CGRectMake(0, 64,kWidth, kHeight)];
    vi.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 64 + 60, 273, 123)];
    imageView.image = [UIImage imageNamed:@"无数据"];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(imageView.frame) + 20, kWidth - 40, 20)];
    label.text = @"暂无记录";
    label.textAlignment = NSTextAlignmentCenter;
    [vi addSubview:imageView];
    [vi addSubview:label];
    
    [self.view addSubview:vi];
    
}


@end
