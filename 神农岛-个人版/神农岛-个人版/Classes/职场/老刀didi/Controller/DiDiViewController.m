//
//  DiDiViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "DiDiViewController.h"
#import "DiDiCell.h"
#import "DiDi+CoreDataClass.h"
#import "RequestApi+News.h"
#import "DiDiWebView.h"
@interface DiDiViewController ()<UITableViewDelegate,UITableViewDataSource,RequestDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *listArr;
@property (nonatomic ,strong) NSManagedObjectContext *context;
@end

@implementation DiDiViewController{
    UIView *vi;
    NSInteger _pageIndex;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1 - TarBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = COLOR_BGCOLOR;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 300;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _pageIndex = 1;
    //配置CoreData
    UIApplication *app = [UIApplication sharedApplication];
    id delegate = app.delegate;
    self.context = [delegate managedObjectContext];
    
    [self displayNoHave];
    [self setNav];
    [self createBasicView];
    [self setModelIndex:_pageIndex];
    [self setFefresh];
}

-(void)setModelIndex:(NSInteger)pageIndex
{
    DSWeak;
    [[RequestApi share] getNoticesListWithTop:@"10" PageIndex:pageIndex Delegate:nil success:^(BOOL str) {
        [weakSelf setCoreData];
    } Success:^(NSDictionary *response) {
        NSLog(@"%@",response);
        if ([response[@"code"]isEqualToNumber:@-1000])
        {
            [MBProgressHUD showError:@"请检查网络!" toView:weakSelf.view];
        }
        [weakSelf setCoreData];
    }];
}

#pragma mark 刷新 加载更多
-(void)setFefresh
{
    [self refreshTableView];    //下拉刷新
    [self getMoreTableView];    //上拉加载
}

-(void)refreshTableView
{
    //    [self.firstTableView.mj_header  beginRefreshing];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    header.automaticallyChangeAlpha = YES;      // 设置自动切换透明度(在导航栏下面自动隐藏)
    
    header.lastUpdatedTimeLabel.hidden = YES;   //隐藏时间
    
    header.stateLabel.hidden = NO;              // 隐藏状态
    
    //    [header beginRefreshing];
    
    // 设置header
    self.tableView.mj_header = header;
}

-(void)getMoreTableView
{
    //上拉加载更多
    MJRefreshBackNormalFooter * footer =  [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    // 设置了底部inset
    [footer setTitle:@"正在加载" forState:MJRefreshStateNoMoreData];
    //    self.firstTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    // 忽略掉底部inset
    //    self.firstTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    self.tableView.mj_footer = footer;
    [self.tableView.mj_footer endRefreshing];
}

//结束刷新
-(void)loadNewData
{
    _pageIndex = 1;
    [self setModelIndex:_pageIndex];
    [self.tableView.mj_header endRefreshing];
}

//结束加载
-(void)loadMoreData
{
    _pageIndex ++ ;
    [self setModelIndex:_pageIndex];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark coredata
-(void)setCoreData
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DiDi"];
    NSError * error;
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"nTime" ascending:NO];
    [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    NSPredicate* predicate;
    
    predicate = [NSPredicate predicateWithFormat:@"status == %@",@"0"];
    
    request.predicate = predicate;
    NSArray *resultAry = [self.context executeFetchRequest:request error:&error];
    
    if (resultAry.count == 0)
    {
        vi.hidden = NO;
    }
    else
    {
        vi.hidden = YES;
        self.listArr = @[].mutableCopy;
        [self.listArr addObjectsFromArray:resultAry];
    }
    
    NSLog(@"%@",_listArr);
    
    [self.tableView reloadData];
    //    [self.tableView.mj_footer endRefreshing];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"职场" leftImageName:@"no" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)createBasicView
{
    [self.view addSubview:self.tableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    if (!cell) {
        
    }
    cell = [[[NSBundle mainBundle]loadNibNamed:@"DiDiCell" owner:nil options:nil]lastObject];
    DiDi *model  = [_listArr objectAtIndex:indexPath.row];
    [cell setCellInfoWithModel:model];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DiDi *model = _listArr[indexPath.row];
    DiDiWebView *didi = [[DiDiWebView alloc]initWithURL:model.nAddress titles:model.nTitle imgURL:model.nImage];
    [GB_Nav pushViewController:didi animated:YES];
}

#pragma mark 没有数据时显示(no have datas display)

-(void)displayNoHave
{
    vi = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATIONBAR_HEIGHT + 1,KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
    vi.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 64 + 60, 273, 123)];
    imageView.image = [UIImage imageNamed:@"无数据"];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(imageView.frame) + 20, KWIDTH - 40, 20)];
    label.text = @"暂无记录";
    label.textAlignment = NSTextAlignmentCenter;
    [vi addSubview:imageView];
    [vi addSubview:label];
    
    [self.view addSubview:vi];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
