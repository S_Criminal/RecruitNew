//
//  DiDiWebView.h
//  神农岛
//
//  Created by 宋晨光 on 16/7/20.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiDiWebView : BasicVC
@property (nonatomic ,strong) NSString *urlStr;
@property (nonatomic ,strong) NSString *titles;
@property (nonatomic ,strong) NSString *imgURL;

-(instancetype)initWithURL:(NSString *)urlStr titles:(NSString *)titles imgURL:(NSString *)imgURL;

@end
