//
//  DiDiWebView.m
//  神农岛
//
//  Created by 宋晨光 on 16/7/20.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "DiDiWebView.h"

#import <WebKit/WebKit.h>
#import "CustomShareUI.h"
@interface DiDiWebView ()<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic ,strong)WKWebView *wkWebView;

@end

@implementation DiDiWebView

-(instancetype)initWithURL:(NSString *)urlStr titles:(NSString *)titles imgURL:(NSString *)imgURL
{
    DiDiWebView *didi = [DiDiWebView new];
    didi.urlStr = urlStr;
    didi.titles = titles;
    didi.imgURL = imgURL;
    return didi;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self creatWebView];
    [self loadWeb];
}

-(void)setNav
{
    DSWeak;
    [self.view addSubview:[BaseNavView initNavTitle:@"" leftImageName:@"" leftBlock:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } rightTitle:@"分享" rightBlock:^{
        [weakSelf setShare];
    }]];
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)setShare
{
    CustomShareUI *shareView = [[CustomShareUI alloc]initShareUIContent:@"" title:self.titles url:[NSURL URLWithString:self.urlStr] images:self.imgURL shareEnum:nil];
    [self.view addSubview:shareView];
}

-(void)creatWebView
{
    self.wkWebView = [[WKWebView alloc]init];
    [self.wkWebView setFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, KHEIGHT - NAVIGATION_BarHeight - 1)];
    self.wkWebView.UIDelegate = self;
    self.wkWebView.navigationDelegate = self;
    [self.view addSubview:self.wkWebView];
}

-(void)loadWeb
{
    NSString *urlStr = self.urlStr;
    NSURL *url = [NSURL URLWithString:urlStr];
//    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.wkWebView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
