//
//  NewsManager.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^DicBlock)(NSDictionary *dic);
typedef void(^StrBlock)(NSString *str);
typedef void(^SuccessBlock)(BOOL success);

@interface NewsManager : NSObject

@property(strong,nonatomic)AFHTTPSessionManager *manager;
@property(strong,nonatomic)NSOperationQueue *operationQueue;

@property (nonatomic ,strong)NSManagedObjectContext * context;

+(instancetype)share;

-(void)GetSeeRecordListWithKey:(NSString *)key
                           top:(NSString *)top
                     pageIndex:(NSInteger)pageIndex
                       success:(DicBlock)block
                       failure:(StrBlock)failure;

@end
