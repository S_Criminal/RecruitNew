//
//  NewsManager.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "NewsManager.h"
#import "PositionCoreData+CoreDataProperties.h"

@interface NewsManager ()
@property (nonatomic ,strong) NSString *lastID;
@end

@implementation NewsManager

+(instancetype)share
{
    static NewsManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[NewsManager alloc]init];
        manager.operationQueue = [[NSOperationQueue alloc]init];
    });
    return manager;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        ////        //配置CoreData
        UIApplication *app = [UIApplication sharedApplication];
        id delegate = app.delegate;
        self.context = [delegate managedObjectContext];
        
    }
    return self;
}

-(void)GetSeeRecordListWithKey:(NSString *)key top:(NSString *)top pageIndex:(NSInteger)pageIndex success:(DicBlock)block failure:(StrBlock)failure{
    NSString *str = [NSString stringWithFormat:@"%@/Api/SeeRecord/SeeRecord.ashx?action=getseerecordlistp",HTTP];
    
    if (pageIndex == 1)
    {
        self.lastID = @"0";
    }else{
        
    }
    
    __weak __block NewsManager * weak_self = self;
    
    [self.manager POST:str parameters:@{@"key":key,@"top":top,@"srid":self.lastID} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"RecordMeCompany%@",dic);
        if ([dic[@"code"] isEqualToNumber:@200]) {
            
            NSArray * datas = [NSArray arrayWithArray:dic[@"datas"]];
            
            if (datas.count > 0)
            {
                weak_self.lastID = [NSString stringWithFormat:@"%@",datas.lastObject[@"ID"]];
                
                if (pageIndex == 1)
                {
                    [weak_self deletePositionModelsBlockHandelDoJobSuccess:^(BOOL success) {
                        [weak_self inseartPositionArr:datas status:@"chakanPersonal"];
                    } status:@"chakanPersonal"];
                }
                else
                {
                    [weak_self inseartPositionArr:datas status:@"chakanPersonal"];
                }
            }else{
                NSDictionary *d = @{@"code":@-10};
                block(d);
            }
        }
        block(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",[error description]);
        failure(@"500");
    }];
}

-(void)inseartPositionArr:(NSArray *)datas status:(NSString *)status
{
    for (NSDictionary * dic in datas)
    {
        PositionCoreData * model = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PositionCoreData class]) inManagedObjectContext:self.context];
        model.c_Logo = [NSString stringWithFormat:@"%@",dic[@"C_Logo"]];
        
        model.p_Name= [NSString stringWithFormat:@"%@",dic[@"P_Name"]];
        
        model.c_Name = [NSString stringWithFormat:@"%@",dic[@"C_Name"]];
        
        model.p_City = [NSString stringWithFormat:@"%@",dic[@"P_City"]];
        
        model.p_Exp = [NSString stringWithFormat:@"%@",dic[@"P_Exp"]];
        
        model.p_Education = [NSString stringWithFormat:@"%@",dic[@"P_Education"]];
        
        model.c_Scale = [NSString stringWithFormat:@"%@",dic[@"C_Scale"]];
        
        model.i_Name = [NSString stringWithFormat:@"     %@    ",dic[@"I_Name"]];
        
        model.c_Time = [NSString stringWithFormat:@"%@ ",dic[@"CreateDate"]];
        
        model.p_Pay = [NSString stringWithFormat:@"%@",dic[@"P_Pay"]];
        
        model.c_Pid = [NSString stringWithFormat:@"%@",dic[@"ID"]];
        
        model.c_Auitstatus = [NSString stringWithFormat:@"%@",dic[@"C_AduitStatus"]];//判断企业是否认证
        model.isVideo = [NSString stringWithFormat:@"%@",dic[@"IsVideo"]];
        
        model.c_Cid = [NSString stringWithFormat:@"%@",dic[@"C_ID"]];     //公司id
        
        model.status = status;
        //        int i = 0;
        //        i ++;
        //        model.positionSortID = [NSString stringWithFormat:@"%d",i];
        [self.context save:nil];
    }
}

-(void)deletePositionModelsBlockHandelDoJobSuccess:(SuccessBlock)isSuccess status:(NSString *)status
{
    NSFetchRequest * request;
    //    NSSortDescriptor * sortD;
    NSPredicate* predicate;
    NSLog(@"%@",status);
    
    request = [NSFetchRequest fetchRequestWithEntityName:@"PositionCoreData"];
    
    predicate = [NSPredicate predicateWithFormat:@"status == %@",@"chakanPersonal"];
    
    request.predicate = predicate;
    
    NSError * error;
    NSArray * result = [self.context executeFetchRequest:request error:&error];
    if (result ==nil)
    {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }
    if (result.lastObject)
    {
        for (int i = 0; i<result.count; i++)
        {
            [self.context deleteObject:result[i]];
        }
    }
    if (![self.context save:&error]) {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }else
    {
        isSuccess(YES);
    }
    
}

@end
