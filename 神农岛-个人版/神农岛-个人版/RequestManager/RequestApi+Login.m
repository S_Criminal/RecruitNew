//
//  RequestApi+Login.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi+Login.h"
#import "RequestApi.h"

@implementation RequestApi (Login)

+(void)getCodeWithTel:(NSString *)phone Type:(NSString *)type Delegate:(id <RequestDelegate>)delegate success:(void (^)(NSDictionary * response))block failure:(void (^)(NSString * errostr))failure
{
    NSDictionary *dic = @{@"phone":phone,@"type":type};
    [self postUrl:@"/Api/User/User.ashx?action=smscode" delegate:delegate parameters:dic success:^(NSDictionary *response, id mark) {
        NSLog(@"获取验证码%@",response);
        block(dic);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

+(void)getLoginWithPhone:(NSString *)phone Type:(NSString *)type Delegate:(id <RequestDelegate>)delegate pwd:(NSString *)pwd success:(void (^)(NSDictionary * response))block failure:(void (^)(NSString * errostr))failure
{
    NSDictionary *dic = @{@"phone":phone,@"pwd":pwd,@"type":type};
    [self postUrl:@"/v1/Api/User/User.ashx?action=login" delegate:delegate parameters:dic success:^(NSDictionary *response, id mark) {
        NSLog(@"用户登录%@",response);
        [GlobalData sharedInstance].GB_Key = [NSString stringWithFormat:@"%@",response[@"datas"]];
        [GlobalData sharedInstance].GB_UID = [NSString stringWithFormat:@"%@",response[@"UID"]];
        block(dic);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

+(void)phoneAndCodeWithPhone:(NSString *)phone
                     smscode:(NSString *)smscode
                        type:(NSString *)type
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errostr))failure
{
    NSDictionary *dic = @{@"phone":phone,@"smscode":smscode,@"type":type};
    [self postUrl:@"/Api/User/User.ashx?action=judgecode" delegate:delegate parameters:dic success:^(NSDictionary *response, id mark) {
        NSLog(@"用户登录%@",response);
        block(dic);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

/**
 *  登录时 传设备id   /Api/User/User.ashx?action=binddevice
 */
+(void)bindDeviceWithKey:(NSString *)key
             equimenumID:(NSString *)equimenumID
              equimetype:(NSString *)equimetype
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * errostr))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=binddevice" delegate:delegate parameters:@{@"key":key,@"equimenum":equimenumID,@"equimetype":equimetype} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//填写密码
+(void)registerWithPhone:(NSString *)phone
                     pwd:(NSString *)pwd
                 smscode:(NSString *)smscode
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * errostr))failure
{
    [self postUrl:@"/v1/Api/User/User.ashx?action=register" delegate:delegate parameters:@{@"phone":phone,@"pwd":pwd,@"smscode":smscode} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}
#pragma mark 修改密码

+(void)forgetpwdWithPhone:(NSString *)phone
                  smscode:(NSString *)smscode
                     pwdf:(NSString *)pwdf
                 Delegate:(id <RequestDelegate>)delegate
                  success:(void (^)(NSDictionary * response))block
                  failure:(void (^)(NSString * errostr))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=forgetpwd" delegate:delegate parameters:@{@"phone":phone,@"smscode":smscode,@"pwd":pwdf} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

//公用信息 学历

+(void)getEduListWithDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block
{
    [self postUrl:@"/Api/Public/Public.ashx?action=reducation" delegate:delegate parameters:nil success:^(NSDictionary *response, id mark) {
        NSLog(@"学历%@",response);
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark  公用信息 职位所有
+(void)getPositionTypeLsWithDelegate:(id <RequestDelegate>)delegate
                             Success:(void (^)(NSDictionary * response))block
                             failure:(void(^)(NSString * str))failure
{
    [self postUrl:@"/Page/PositionType/PositionType.ashx?action=getpositiontypels"
         delegate:delegate
       parameters:nil
          success:^(NSDictionary *response, id mark) {
              NSLog(@"职位%@",response);
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              
          }];
}
#pragma mark  公用信息 行业类别
+(void)getindustrylListDelegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))block
                        failure:(void(^)(NSString * str))failure
{
    [self postUrl:@"/Page/Industry/Industry.ashx?action=getindustryls"
         delegate:delegate
       parameters:nil
          success:^(NSDictionary *response, id mark) {
              NSLog(@"行业类别%@",response);
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              
          }];
}
//公用信息  薪资要求
+(void)getSalarysListDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block
{
    [self postUrl:@"/Api/Public/Public.ashx?action=salaryss"
         delegate:delegate
       parameters:nil
          success:^(NSDictionary *response, id mark) {
              NSLog(@"薪资%@",response);
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              
          }];
}

#pragma mark 公用信息 学校匹配
+(void)matchSchoolNameWithKey:(NSString *)key schoolName:(NSString *)name
                 WithDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block
                      failure:(void(^)(NSString * str))failure
{
    [self postUrl:@"/Api/Public/Public.ashx?action=searchsm" delegate:delegate parameters:@{@"key":key,@"sname":name} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

/** 公用信息 简历状态 */
+(void)statusBriefWithDelegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))block
                       failure:(void(^)(NSString * str))failure
{
    [self postUrl:@"/Api/Public/Public.ashx?action=rstatus" delegate:delegate parameters:nil success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

// 获取城市列表
+(void)getPublicInformationWithCityDelegate:(id <RequestDelegate>)delegate
                                        Success:(void (^)(NSDictionary * response))block
                                        failure:(void(^)(NSString * str))failure
{
    [self postUrl:@"/Api/Province/Province.ashx?action=getsort" delegate:delegate parameters:nil success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}
// 获取省份列表
+(void)getPublicInformationWithProvince:(NSString *)Province Delegate:(id <RequestDelegate>)delegate
                                Success:(void (^)(NSDictionary * response))block
                                failure:(void(^)(NSString * str))failure
{
    [self postUrl:@"/Api/Province/Province.ashx?action=getcity" delegate:delegate parameters:@{@"province":Province} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

@end
