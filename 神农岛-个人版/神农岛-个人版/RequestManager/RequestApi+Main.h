//
//  RequestApi+Main.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi.h"

@interface RequestApi (Main)


/**
 *  刷新热门搜索
 */
+(void)getHotListWithKey:(NSString *)key
                   hType:(NSString *)hType
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSDictionary * response))failure;
/**
 *  投递记录
 */
+(void)getResumeWithKey:(NSString *)key
                status:(NSString *)status
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSDictionary * response))failure;
/**
 *  投递简历
 */
+(void)sendResumeWithKey:(NSString *)key
                     rid:(NSString *)rid
                     pid:(NSString *)pid
               Delegate:(id <RequestDelegate>)delegate
                success:(void (^)(NSDictionary * response))block
                failure:(void (^)(NSString * string))failure;

/**
 *  获取地理信息
 /Api/Location/Location.ashx?action=locations
 */
+(void)postLocationsWithKey:(NSString *)key
                     longitude:(NSString *)longitude
                     latitude:(NSString *)latitude
                    version:(NSString *)version
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * string))failure;

/**
 *  获取公司职位列表
 */
+(void)GetPositionWithCID:(NSString *)cid
                     top:(NSString *)top
             positiontype:(NSString *)positiontype
                      pid:(NSString *)pid
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSDictionary * response))failure;

/**
 *  获取公司福利待遇
 */
+(void)GetWelfareListWithCID:(NSString *)cid
                 Delegate:(id <RequestDelegate>)delegate
                  success:(void (^)(NSDictionary * response))block
                  failure:(void (^)(NSDictionary * response))failure;

/**
 *  获取简历完整度
 */
+(void)getBriefCompleteWithKey:(NSString *)key
                           pid:(NSString *)pid
                      Delegate:(id <RequestDelegate>)delegate
                       success:(void (^)(NSDictionary * response))block
                       failure:(void (^)(NSString * errorStr))failure;
/**
 *  未完善个人查看企业联系方式
 */
+(void)noCompleteBriefInfoWithKey:(NSString *)key
                      Delegate:(id <RequestDelegate>)delegate
                          success:(void (^)(NSDictionary * response))block
                          failure:(void (^)(NSString * errorStr))failure;
/**
 *  完善个人查看企业联系方式
 */
+(void)CompleteBriefInfoWithKey:(NSString *)key
                         Delegate:(id <RequestDelegate>)delegate
                        success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * errorStr))failure;
/**
 *  获取公司详情
 */
+(void)GetCompanyInfoWithCID:(NSString *)cid
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block;
/**
 *  规则 HTML
 */
+(void)getBuyNoticeWithKey:(NSString *)key
                      cKey:(NSString *)cKey
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block;
/**
 *  屏蔽公司
 */
+(void)editscompanyWithKey:(NSString *)key
                    editID:(NSString *)editID
                       cid:(NSString *)cid cname:(NSString *)cname
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSString * str))failure;
/**
 *  收藏职位
 */
+(void)savePositionWithKey:(NSString *)key
                      pid:(NSString *)pid
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                    failure:(void (^)(NSDictionary * response))failure;

/**
 *  获取用户信息
 */
+(void)getUserInfoWithKey:(NSString *)key uid:(NSString *)uid
                 Delegate:(id <RequestDelegate>)delegate
                  success:(void (^)(NSDictionary * response))block
                  failure:(void (^)(NSString * str))failure;

/**
 *  获取版本id  企业提示
 */
+(void)getAppBundleIDWithCompanyDelegate:(id <RequestDelegate>)delegate
                  success:(void (^)(NSDictionary * response))block
                  failure:(void (^)(NSString * str))failure;

/**
 *  获取版本id  个人提示
 */
+(void)getAppBundleIDWithPersonDelegate:(id <RequestDelegate>)delegate
                          success:(void (^)(NSDictionary * response))block
                          failure:(void (^)(NSString * str))failure;
/**
 *  获取提示信息
 */
+(void)getFirstAlertInfoWithPKey:(NSString *)key
                        delegate:(id <RequestDelegate>)delegate
                        success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * str))failure;

@end
