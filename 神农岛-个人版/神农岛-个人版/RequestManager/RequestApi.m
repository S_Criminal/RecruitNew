//
//  RequestApi.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi.h"
#import "LoginViewController.h"

#import "BaseImage.h"
//阿里云
#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>

NSString * const AccessKey = @"EhDZSLSwfPX0LiF1";
NSString * const SecretKey = @"GQpdGFE5LIzDCQHkMzj8v78EcwrUcS";
NSString * const endPoint = @"http://oss-cn-qingdao.aliyuncs.com/";
NSString * const multipartUploadKey = @"";

OSSClient * client;

@implementation RequestApi

+(instancetype)share{
    static RequestApi *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager =  [[RequestApi alloc]init];
        manager.operationQueue = [[NSOperationQueue alloc]init];
    });
    return manager;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [self.manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        self.manager.requestSerializer.timeoutInterval = 5.f;
        
        //配置CoreData
        UIApplication *app = [UIApplication sharedApplication];
        id delegate = app.delegate;
        self.context = [delegate managedObjectContext];
        
        [self initOSSClient];
        
//        self.manager = [AFHTTPSessionManager manager];
//        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    return self;
}

+(void)postUrl:(NSString *)URL
      delegate:(id<RequestDelegate>)delegate
    parameters:(NSDictionary *)parameters
       success:(void (^)(NSDictionary *, id))success
       failure:(void (^)(NSString *, id))failure
{
    AFHTTPSessionManager *manager  = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    URL = [NSString stringWithFormat:@"%@%@",HTTP,URL];
    
    [manager POST:URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSData * data = responseObject;
        NSDictionary * dicResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        if (dicResponse == nil ) {
            //走回调 隐藏progress
            if (delegate != nil && [delegate respondsToSelector:@selector(protocolDidRequestFailure:)]) {
                [delegate protocolDidRequestFailure:@"数据请求失败"];
            }
            return;
        }
        //判断请求状态
        if([dicResponse[RESPONSE_CODE] isEqualToNumber:RESPONSE_CODE_200]){
            //走回调 请求成功
            if (delegate != nil && [delegate respondsToSelector:@selector(protocolDidRequestSuccess)]) {
                [delegate protocolDidRequestSuccess];
            }
            success(dicResponse,task);
        } else if([dicResponse[RESPONSE_CODE] isEqualToNumber:RESPONSE_CODE_NEGATIVE100]){
            //重新登陆
            if (delegate != nil && [delegate respondsToSelector:@selector(protocolDidRequestFailure:)]) {
                [delegate protocolDidRequestFailure:@"重新登陆"];
                [GB_Nav pushViewController:[LoginViewController new] animated:true];
            }
//            [GlobalMethod clearAllDates];
            [GlobalMethod clearUserInfo];
            failure(dicResponse[@"message"],nil);
            
        }else {
            //走回调 请求失败
            if (delegate != nil && [delegate respondsToSelector:@selector(protocolDidRequestFailure:)]) {
                [delegate protocolDidRequestFailure:dicResponse[RESPONSE_MESSAGE]];
            }
            if (!kStringIsEmpty(dicResponse[@"message"])) {
                failure(dicResponse[@"message"],nil);
            }else{
                failure(@"",nil);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",[error description]);
        //走回调 无网络
        if (delegate != nil && [delegate respondsToSelector:@selector(protocolDidRequestFailure:)]) {
            [delegate protocolDidRequestFailure:@"连接失败，请检查你的网络设置"];
        }
        if (failure != nil) {
            failure(@"连接失败，请检查你的网络设置",nil);
        }
    }];
}


/**
 *  实例化 阿里云上传 签名
 */
- (void)initOSSClient {
    
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:AccessKey
                                                                                                            secretKey:SecretKey];
    
    // 自实现签名，可以用本地签名也可以远程加签
    id<OSSCredentialProvider> credential1 = [[OSSCustomSignerCredentialProvider alloc] initWithImplementedSigner:^NSString *(NSString *contentToSign, NSError *__autoreleasing *error) {
        NSString *signature = [OSSUtil calBase64Sha1WithData:contentToSign withSecret:SecretKey];
        if (signature != nil) {
            *error = nil;
        } else {
            // construct error object
            *error = [NSError errorWithDomain:@"<your error domain>" code:OSSClientErrorCodeSignFailed userInfo:nil];
            return nil;
        }
        return [NSString stringWithFormat:@"OSS %@:%@", AccessKey, signature];
    }];
    
    id<OSSCredentialProvider> credential2 = [[OSSFederationCredentialProvider alloc] initWithFederationTokenGetter:^OSSFederationToken * {
        NSURL * url = [NSURL URLWithString:@"http://localhost:8080/distribute-token.json"];
        NSURLRequest * request = [NSURLRequest requestWithURL:url];
        OSSTaskCompletionSource * tcs = [OSSTaskCompletionSource taskCompletionSource];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionTask * sessionTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            [tcs setError:error];
                                                            return;
                                                        }
                                                        [tcs setResult:data];
                                                    }];
        [sessionTask resume];
        [tcs.task waitUntilFinished];
        if (tcs.task.error) {
            //nslog(@"get token error: %@", tcs.task.error);
            return nil;
        } else {
            NSDictionary * object = [NSJSONSerialization JSONObjectWithData:tcs.task.result
                                                                    options:kNilOptions
                                                                      error:nil];
            OSSFederationToken * token = [OSSFederationToken new];
            token.tAccessKey = [object objectForKey:@"accessKeyId"];
            token.tSecretKey = [object objectForKey:@"accessKeySecret"];
            token.tToken = [object objectForKey:@"securityToken"];
            token.expirationTimeInGMTFormat = [object objectForKey:@"expiration"];
            //nslog(@"get token: %@", token);
            return token;
        }
    }];
    
    
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 2;
    conf.timeoutIntervalForRequest = 30;
    conf.timeoutIntervalForResource = 24 * 60 * 60;
    
    client = [[OSSClient alloc] initWithEndpoint:endPoint credentialProvider:credential clientConfiguration:conf];
    
}

-(void)getImageUrlWithAliYunWithImageData:(NSData *)imageData
                   BlockHandelImageUrlStr:(void (^)(NSString * str))imageurlStr
                       BlockHandelSuccess:(void (^)(BOOL success))block;
{
    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:[NSDate date]];
    
    //    //nslog(@"%@",currentDateStr);
    
    NSString *string = [[NSString alloc]init];
    for (int i = 0; i < 32; i++) {
        int number = arc4random() % 36;
        if (number < 10) {
            int figure = arc4random() % 10;
            NSString *tempString = [NSString stringWithFormat:@"%d", figure];
            string = [string stringByAppendingString:tempString];
        }else {
            int figure = (arc4random() % 26) + 97;
            char character = figure;
            NSString *tempString = [NSString stringWithFormat:@"%c", character];
            string = [string stringByAppendingString:tempString];
        }
    }
    
    
    // required fields
    put.bucketName = @"sndlaodao";
    put.objectKey = [NSString stringWithFormat:@"upload/sndAPP/%@/%@.png",currentDateStr,string];
    
    
    put.uploadingData = imageData;
    
    // optional fields
    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
        //nslog(@"当前上传量-------------%lld , 当前总上传量---------------%lld,文件大小---------------- %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"updataImageProgress" object:nil userInfo:@{@"progress":[NSString stringWithFormat:@"%f",(CGFloat)totalByteSent/totalBytesExpectedToSend]}];
    };
    
    put.contentType = @"";
    put.contentMd5 = @"";
    put.contentEncoding = @"";
    put.contentDisposition = @"";
    
    OSSTask * putTask = [client putObject:put];
    
    [putTask continueWithBlock:^id(OSSTask *task) {
        
        //        nslog(@"objectKey:%@",ssndHTTP,put.objectKey);
        imageurlStr(put.objectKey);
        if (!task.error) {
            block (YES);
            //nslog(@"upload object success!");
            
        } else {
            block(NO);
            //nslog(@"upload object failed, error: %@" , task.error);
        }
        return nil;
    }];
}

- (void)updateImageAry:(NSArray *)aryDatas
               success:(void(^)(void))success
                  fail:(void(^)(void))fail{
    
    for (BaseImage * image  in aryDatas) {
        if (kStringIsEmpty(image.imageURL)) {
            return;
        }
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        
        NSString *currentDateStr = [GlobalMethod exchangeDate:[NSDate date] formatter:@"yyyy/MM/dd"];
        NSString *string = [GlobalMethod fetchTimeStamp];
        // required fields
        put.bucketName = @"leplaodao";
        put.objectKey = [NSString stringWithFormat:@"App/Upload/%@/%@.png",currentDateStr,string];
        put.uploadingData = UIImagePNGRepresentation(image); // 直接上传NSData
        
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            if (!task.error) {
                image.imageURL = [NSString stringWithFormat:@"%@%@",ssndHTTP,put.objectKey];
                BOOL isAllComplete = true;
                for (BaseImage * image in aryDatas) {
                    if (image.imageURL == nil) {
                        isAllComplete = false;
                        break;
                    }
                }
                if (isAllComplete) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        success();
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    fail();
                });
            }
            return nil;
        }];
        
    }
    
}

-(void)getImageUrlWithAliYunWithImageDataArray:(NSArray *)imageArrayData
                        BlockHandelImageUrlStr:(void (^)(NSString *))imageurlStr BlockHandelSuccess:(void (^)(BOOL))block
{
//    OSSPutObjectRequest * put = [OSSPutObjectRequest new];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    //设定时间格式,这里可以设置成自己需要的格式
//    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
//    //用[NSDate date]可以获取系统当前时间
//    NSString *currentDateStr = [dateFormatter stringFromDate:[NSDate date]];
//    
//    //    //nslog(@"%@",currentDateStr);
//    
//    NSString *string = [[NSString alloc]init];
//    for (int i = 0; i < 32; i++) {
//        int number = arc4random() % 36;
//        if (number < 10) {
//            int figure = arc4random() % 10;
//            NSString *tempString = [NSString stringWithFormat:@"%d", figure];
//            string = [string stringByAppendingString:tempString];
//        }else {
//            int figure = (arc4random() % 26) + 97;
//            char character = figure;
//            NSString *tempString = [NSString stringWithFormat:@"%c", character];
//            string = [string stringByAppendingString:tempString];
//        }
//    }
//    
//    
//    // required fields
//    put.bucketName = @"sndlaodao";
//    put.objectKey = [NSString stringWithFormat:@"upload/sndAPP/%@/%@.png",currentDateStr,string];
//    
//    
//    
//    put.uploadingData = imageData;
//    
//    // optional fields
//    put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
//        //nslog(@"当前上传量-------------%lld , 当前总上传量---------------%lld,文件大小---------------- %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
//        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"updataImageProgress" object:nil userInfo:@{@"progress":[NSString stringWithFormat:@"%f",(CGFloat)totalByteSent/totalBytesExpectedToSend]}];
//    };
//    
//    put.contentType = @"";
//    put.contentMd5 = @"";
//    put.contentEncoding = @"";
//    put.contentDisposition = @"";
//    
//    OSSTask * putTask = [client putObject:put];
//    
//    [putTask continueWithBlock:^id(OSSTask *task) {
//        
//        //        nslog(@"objectKey:%@",ssndHTTP,put.objectKey);
//        imageurlStr(put.objectKey);
//        if (!task.error) {
//            block (YES);
//            //nslog(@"upload object success!");
//            
//        } else {
//            block(NO);
//            //nslog(@"upload object failed, error: %@" , task.error);
//        }
//        return nil;
//    }];
}

//+ (void)switchRequest:(NSString *)URLString
//           parameters:(id)parameters
//                isGet:(BOOL)isGet
//constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
//              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
//              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
//    if (isGet == true) {
//        [[RequestApi share] GET:URLString parameters:parameters progress:nil success:success failure:failure];
//    } else if (block == nil) {
//        [[RequestApi share] POST:URLString parameters:parameters progress:nil success:success failure:failure];
//    } else {
//        [[RequestApi share]POST:URLString parameters:parameters constructingBodyWithBlock:block progress:nil success:success failure:failure];



@end
