//
//  RequestApi+Main.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi+Main.h"




@implementation RequestApi (Main)



#pragma mark 刷新热门搜索

+(void)getHotListWithKey:(NSString *)key
                   hType:(NSString *)hType
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSDictionary * response))failure
{
    [self postUrl:@"/Api/HotSearch/HotSearch.ashx?action=gethotslist" delegate:delegate parameters:@{@"key":key,@"htype":hType} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 投递记录

+(void)getResumeWithKey:(NSString *)key
                 status:(NSString *)status
               Delegate:(id <RequestDelegate>)delegate
                success:(void (^)(NSDictionary * response))block
                failure:(void (^)(NSDictionary * response))failure
{
    [self postUrl:@"/Api/Resume/Resume.ashx?action=getrplist" delegate:delegate parameters:@{@"key":key,@"status":status} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark  投递简历
 
+(void)sendResumeWithKey:(NSString *)key
                     rid:(NSString *)rid
                     pid:(NSString *)pid
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * string))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=sendresume" delegate:delegate parameters:@{@"key":key,@"rid":rid,@"pid":pid} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark  获取公司职位列表

+(void)GetPositionWithCID:(NSString *)cid
                      top:(NSString *)top
             positiontype:(NSString *)positiontype
                      pid:(NSString *)pid
                 Delegate:(id <RequestDelegate>)delegate
                  success:(void (^)(NSDictionary * response))block
                  failure:(void (^)(NSDictionary * response))failure
{
    [self postUrl:@"/Api/Position/Position.ashx?action=getpositionc" delegate:delegate parameters:@{@"cid":cid,@"top":top,@"positiontype":positiontype,@"pid":pid} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark  获取公司福利待遇

+(void)GetWelfareListWithCID:(NSString *)cid
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSDictionary * response))failure
{
    [self postUrl:@"/Api/ComWelfare/ComWelfare.ashx?action=getwelfarelist" delegate:delegate parameters:@{@"cid":cid} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 获取简历完整度

+(void)getBriefCompleteWithKey:(NSString *)key
                           pid:(NSString *)pid
                      Delegate:(id <RequestDelegate>)delegate
                       success:(void (^)(NSDictionary * response))block
                       failure:(void (^)(NSString * errorStr))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=getrfull" delegate:delegate parameters:@{@"key":key,@"pid":pid} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}


#pragma mark 未完善个人查看企业联系方式

+(void)noCompleteBriefInfoWithKey:(NSString *)key
                         Delegate:(id <RequestDelegate>)delegate
                          success:(void (^)(NSDictionary * response))block
                          failure:(void (^)(NSString * errorStr))failure
{
    [self postUrl:@"/Api/Public/Public.ashx?action=getcct" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark 未完善个人查看企业联系方式
+(void)CompleteBriefInfoWithKey:(NSString *)key
                       Delegate:(id <RequestDelegate>)delegate
                        success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * errorStr))failure
{
    [self postUrl:@"/Api/Public/Public.ashx?action=getccts" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark 公司详情
+(void)GetCompanyInfoWithCID:(NSString *)cid
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
{
    [self postUrl:@"/Api/Company/Company.ashx?action=getcompanyinfo" delegate:delegate parameters:@{@"cid":cid} success:^(NSDictionary *response, id mark) {
        NSMutableArray *arr = response[@"datas"];
        if (arr.count > 0) {
            NSDictionary *d = [arr objectAtIndex:0];
            block(d);
        }
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
#pragma mark 屏蔽公司
+(void)editscompanyWithKey:(NSString *)key
                    editID:(NSString *)editID
                       cid:(NSString *)cid cname:(NSString *)cname
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/ShiledCompany/ShiledCompany.ashx?action=editsc" delegate:delegate parameters:@{@"key":key,@"id":editID,@"cid":cid,@"cname":cname} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark 获取地理信息
+(void)postLocationsWithKey:(NSString *)key
               longitude:(NSString *)longitude
                latitude:(NSString *)latitude
                 version:(NSString *)version
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * string))failure
{
    [self postUrl:@"/Api/Location/Location.ashx?action=locations" delegate:delegate parameters:@{@"key":key,@"longitude":longitude,@"latitude":latitude,@"version":version} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
#pragma mark 收藏职位
+(void)savePositionWithKey:(NSString *)key
                       pid:(NSString *)pid
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSDictionary * response))failure
{
    [self postUrl:@"/Api/Collection/Collection.ashx?action=collections" delegate:delegate parameters:@{@"key":key,@"pid":pid} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 规则 HTML
+(void)getBuyNoticeWithKey:(NSString *)key
                      cKey:(NSString *)cKey
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
{
    [self postUrl:@"/Page/SysConfig/SysConfig.ashx?action=scinfos"
         delegate:delegate parameters:@{@"key":key,@"cKey":cKey}
          success:^(NSDictionary *response, id mark) {
        NSLog(@"%@",response);
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}


/**
 *  获取用户信息
 */
+(void)getUserInfoWithKey:(NSString *)key uid:(NSString *)uid
                 Delegate:(id <RequestDelegate>)delegate
                  success:(void (^)(NSDictionary * response))block
                  failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=userinfo" delegate:delegate parameters:@{@"key":key,@"uid":uid} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

///Api/Version/Version.ashx?action=versions
/**
 *  获取版本id
 */
+(void)getAppBundleIDWithCompanyDelegate:(id <RequestDelegate>)delegate
                          success:(void (^)(NSDictionary * response))block
                          failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/Version/Version.ashx?action=verss" delegate:delegate parameters:nil success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

/**
 *  获取版本id  个人提示
 */
+(void)getAppBundleIDWithPersonDelegate:(id <RequestDelegate>)delegate
                                success:(void (^)(NSDictionary * response))block
                                failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/Version/Version.ashx?action=versions" delegate:delegate parameters:nil success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}
#pragma  获取提示信息
+(void)getFirstAlertInfoWithPKey:(NSString *)key
                        delegate:(id <RequestDelegate>)delegate
                         success:(void (^)(NSDictionary * response))block
                         failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Page/PromptInforms/PromptInforms.ashx?action=piinfos" delegate:delegate parameters:@{@"pkey":key} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}


@end
