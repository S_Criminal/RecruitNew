//
//  RequestApi+MyBrief.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/6.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi+MyBrief.h"



@implementation RequestApi (MyBrief)

#pragma mark  是否填写标识

+(void)getBriefFillWithKey:(NSString *)key
                  Delegate:(id<RequestDelegate>)delegate
                   success:(void (^)(NSDictionary *response))block
                   failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=fillins" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark  获取用户信息

+(void)getResumeWithKey:(NSString *)key
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString *, id))failure
{
    [self postUrl:@"/Api/Resume/Resume.ashx?action=getresumes" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        block(response);
       } failure:^(NSString *errorStr, id mark) {
           failure(errorStr,mark);
       }];
}


#pragma mark 获取屏蔽列表
+(void)getsClistWithKey:(NSString *)key top:(NSString *)top
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/ShiledCompany/ShiledCompany.ashx?action=sclist" delegate:delegate parameters:@{@"key":key,@"top":top} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 删除屏蔽企业

+(void)deletePrivacyCompany:(NSString *)key
                         ids:(NSString *)ids
                   Delegate:(id<RequestDelegate>)delegate
                    success:(void (^)(NSDictionary *response))block
                    failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/ShiledCompany/ShiledCompany.ashx?action=delsc" delegate:delegate parameters:@{@"key":key,@"id":ids} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr,mark);
    }];
}

#pragma mark  模糊搜索公司列表

+(void)searchCompanyNameWithKey:(NSString *)key top:(NSString *)top
                          cName:(NSString *)cname
                       Delegate:(id<RequestDelegate>)delegate
                        success:(void (^)(NSDictionary *response))block
                        failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/ShiledCompany/ShiledCompany.ashx?action=findclist" delegate:delegate parameters:@{@"key":key,@"top":top,@"cname":cname} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr,mark);
    }];
}

#pragma mark  编辑添加屏蔽企业

+(void)editAddCompanyNameWithKey:(NSString *)key editID:(NSString *)editID
                             cid:(NSString *)cid
                           cName:(NSString *)cname
                        Delegate:(id<RequestDelegate>)delegate
                         success:(void (^)(NSDictionary *response))block
                         failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/ShiledCompany/ShiledCompany.ashx?action=editsc" delegate:delegate parameters:@{@"key":key,@"id":editID,@"cid":cid,@"cname":cname} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr,mark);
    }];
}

//保存用户基本信息
+(void)saveNewBriefInfoWithKey:(NSString *)key
                         uhead:(NSString *)uhead
                          name:(NSString *)name
                           sex:(NSString *)sex
                      birthday:(NSString *)birthday
                     provience:(NSString *)provience
                          city:(NSString *)city
                      Delegate:(id<RequestDelegate>)delegate
                       success:(void (^)(NSDictionary *response))block
{
    NSDictionary *dic = @{
                          @"key":key,
                          @"heads":uhead,
                          @"name":name,@"sex":sex,
                          @"birthday":birthday,
                          @"province":provience,@"city":city,
                          };
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=updusinfo"
         delegate:delegate
    parameters:dic success:^(NSDictionary *response, id mark) {
        
        NSLog(@"%@",response);
        block(response);
        
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

/**
 *  保存用户自我描述
 */
+(void)saveMySelfWithKey:(NSString *)key
          selfevaluation:(NSString *)selfevaluation
                Delegate:(id<RequestDelegate>)delegate
                 success:(void (^)(NSDictionary *response))block
                 failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=updrself" delegate:delegate parameters:@{@"key":key,@"selfevaluation":selfevaluation} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

/**
 *  获取钱包余额、推广人数、实名认证
 */
+(void)getDateWithKey:(NSString *)key
             Delegate:(id<RequestDelegate>)delegate
              success:(void (^)(NSDictionary *response))block
{
    [self postUrl:@"/Api/User/User.ashx?action=walletb"
         delegate:delegate
       parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
           
           NSLog(@"%@",response);
           block(response);
           
       } failure:^(NSString *errorStr, id mark) {
           
       }];
}
#pragma mark 申请实名认证
+(void)getCardApplyWithKey:(NSString *)key
                  cardpath:(NSString *)cardpath
                  Delegate:(id<RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response, id mark))success
                   failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=cardapply" delegate:delegate parameters:@{@"key":key,@"cardpath":cardpath} success:^(NSDictionary *response, id mark) {
        success(response,mark);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr,mark);
    }];
}


//编辑教育经历
+(void)editEduexpWithKey:(NSString *)key
                    edit:(NSString *)edit expID:(NSString *)expID
                  school:(NSString *)school major:(NSString *)major
                  startd:(NSString *)startd endd:(NSString *)endd
                 schlogo:(NSString *)schlogo
                Delegate:(id <RequestDelegate>)delegate
                  degree:(NSString *)degree
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v1/Api/EducationExp/EducationExp.ashx?action=editeduexp" delegate:delegate
         parameters:@{
                      @"key":key,@"edit":edit,@"id":expID,
                      @"school":school,@"major":major,@"startd":startd,@"endd":endd,@"degree":degree,@"schlogo":schlogo
                         }
          success:^(NSDictionary *response, id mark)
        {
            NSLog(@"%@",response);
            block(response);
        }
          failure:^(NSString *errorStr, id mark) {
              failure(errorStr);
          }];
}
//删除教育经历
+(void)deleteEduWithKey:(NSString *)key
                 workID:(NSString *)workID
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
{
    [self postUrl:@"/v1/Api/EducationExp/EducationExp.ashx?action=deleduexp" delegate:delegate
       parameters:@{
                    @"key":key,@"id":workID
                    }
          success:^(NSDictionary *response, id mark)
        {
         NSLog(@"%@",response);
         block(response);
        }
          failure:^(NSString *errorStr, id mark) {
              
          }];
}

// 编辑工作经历

+(void)editExperienceWithKey:(NSString *)key
                        expID:(NSString *)expID
                     comname:(NSString *)comname position:(NSString *)position
                      entryd:(NSString *)startd quitd:(NSString *)quitd
                     content:(NSString *)content
                       quits:(NSString *)quits
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v2/Api/WorkExp/WorkExp.ashx?action=editworkexp" delegate:delegate
       parameters:@{
                    @"key":key,@"id":expID,
                    @"comname":comname,@"position":position,@"entryd":startd,@"quitd":quitd,@"content":content,
                    @"quits":quits
                    }
          success:^(NSDictionary *response, id mark)
     {
         NSLog(@"%@",response);
         block(response);
     }
          failure:^(NSString *errorStr, id mark) {
              failure(errorStr);
          }];
}

// 删除工作经历

+(void)deleteExperienceWithKey:(NSString *)key
                        workID:(NSString *)workID
                      Delegate:(id<RequestDelegate>)delegate
                       success:(void (^)(NSDictionary *response))block
{
    [self postUrl:@"/v1/Api/WorkExp/WorkExp.ashx?action=delworkexp" delegate:delegate
       parameters:@{
                    @"key":key,@"id":workID
                    }
          success:^(NSDictionary *response, id mark)
     {
         NSLog(@"%@",response);
         block(response);
     }
          failure:^(NSString *errorStr, id mark) {
              
          }];
    
}
#pragma mark 编辑项目经历
+(void)editProjectExpWithKey:(NSString *)key
                        expID:(NSString *)expID
                     project:(NSString *)project identity:(NSString *)identity
                      startd:(NSString *)startd endd:(NSString *)quitd
                 description:(NSString *)content
                       online:(NSString *)online
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v2/Api/ProjectExp/ProjectExp.ashx?action=editproexp" delegate:delegate
       parameters:
     @{
       @"key":key,@"id":expID,@"name":project,
       @"duty":identity,
       @"startd":startd,@"endd":quitd,
       @"description":content,@"online":online
       }
          success:^(NSDictionary *response, id mark) {
              block(response);
       } failure:^(NSString *errorStr, id mark) {
           failure(errorStr);
       }];
}

#pragma mark  删除项目经历

+(void)deleteProjectExpWithKey:(NSString *)key
                         expID:(NSString *)expID
                      Delegate:(id <RequestDelegate>)delegate
                       success:(void (^)(NSDictionary * response))block
{
    [self postUrl:@"/v2/Api/ProjectExp/ProjectExp.ashx?action=delproexp" delegate:delegate parameters:@{@"key":key,@"id":expID} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//期望城市
+(void)upLoadNewBriefCityWithKey:(NSString *)key
                       rprovince:(NSString *)rprovince
                           rcity:(NSString *)rcity
                        Delegate:(id <RequestDelegate>)delegate
                         success:(void (^)(NSDictionary * response))block
                         failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=updrcity" delegate:delegate
       parameters:@{@"key":key,@"rprovince":rprovince,@"rcity":rcity}
          success:^(NSDictionary *response, id mark) {
              NSLog(@"%@",response);
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              failure(errorStr,mark);
          }];
}
//期望职位
+(void)upLoadNewBriefPositionWithKey:(NSString *)key
                              rTypes:(NSString *)rtypes
                            Delegate:(id <RequestDelegate>)delegate
                             success:(void (^)(NSDictionary * response))block
                             failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=updrexp" delegate:delegate
       parameters:@{@"key":key,@"rTypes":rtypes}
          success:^(NSDictionary *response, id mark) {
              NSLog(@"%@",response);
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              failure(errorStr,mark);
          }];
}

#pragma mark  编辑职位偏好

+(void)editPositionTrendWithKey:(NSString *)key
                   rprovince:(NSString *)rprovince
                      rcity :(NSString *)rcity
                        pay :(NSString *)pay
                      payid :(NSString *)payid
                     rtypes :(NSString *)rtypes
                         nature:(NSString *)nature
                    Delegate:(id<RequestDelegate>)delegate
                     success:(void (^)(NSDictionary *response))block
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=upduppre" delegate:delegate parameters:@{@"key":key,@"rprovince":rprovince,@"rcity":rcity,@"pay":pay,@"payid":payid,@"rtypes":rtypes,@"nature":nature} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
    
    }];
}

#pragma mark  获取职位偏好

+ (void)getBriefPrtendInfoWithKey:(NSString *)key
                         Delegate:(id<RequestDelegate>)delegate
                          success:(void (^)(NSDictionary *response))block
                          failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=getuppre" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//编辑联系信息
+(void)editContactInfoWithKey:(NSString *)key
                               phone:(NSString *)phone
                                mail:(NSString *)mail
                                  qq:(NSString *)qq
                              wechat:(NSString *)wechat
                            Delegate:(id <RequestDelegate>)delegate
                             success:(void (^)(NSDictionary * response))block
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=upducinfo" delegate:delegate
       parameters:@{@"key":key,@"phone":phone,@"mail":mail,@"qq":qq,@"wechat":wechat}
          success:^(NSDictionary *response, id mark) {
              NSLog(@"%@",response);
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              
          }];
}
#pragma mark  编辑个人成就
+(void)editPersonAchieveWithKey:(NSString *)key
                         editID:(NSString *)editID
                       contents:(NSString *)contents
                          spath:(NSString *)spath
                          sdesp:(NSString *)sdesp
                          sdate:(NSString *)sdate
                         stitle:(NSString *)stitle
                         sclass:(NSString *)sclass
                       Delegate:(id <RequestDelegate>)delegate
                        success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/SkillCert/SkillCert.ashx?action=editskillcert" delegate:delegate
       parameters:@{
                    @"key":key,@"id":editID,@"contents":contents,@"spath":spath,
                    @"stitle":stitle,@"sclass":sclass,@"sdesp":sdesp,
                    @"sdate":sdate
                    }
          success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
#pragma mark  获取个人成就列表


+(void)getkillsListWithKey:(NSString *)key
                    sclass:(NSString *)sclass
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/SkillCert/SkillCert.ashx?action=getskillcertlist" delegate:delegate parameters:@{@"key":key,@"sclass":sclass} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark  获取个人成就信息

+(void)getSkillCertInfoWithKey:(NSString *)key
                        sclass:(NSString *)sclass
                      Delegate:(id <RequestDelegate>)delegate
                       success:(void (^)(NSDictionary * response))block
                       failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/SkillCert/SkillCert.ashx?action=getskillcertinfo" delegate:delegate parameters:@{@"key":key,@"id":sclass} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
///删除个人成就
+(void)requestDelskillcertWithKey:(NSString *)key
                               ID:(NSString *)ID
                        Delegate:(id <RequestDelegate>)delegate
                         success:(void (^)(NSDictionary * response))block
                         failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/SkillCert/SkillCert.ashx?action=delskillcert" delegate:delegate parameters:@{@"key":key,@"id":ID} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
#pragma mark 获取生活照
/**
 *  获取生活照
 */
+(void)getLifeImageWithKey:(NSString *)key
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=getulinfo" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark  编辑生活照片

+(void)editPersonLifeWithKey:(NSString *)key
                       lifeimg :(NSString *)lifeimg
                       Delegate:(id <RequestDelegate>)delegate
                        success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=updrlife" delegate:delegate parameters:@{@"key":key,@"lifeimg":lifeimg}
          success:^(NSDictionary *response, id mark) {
              block(response);
          } failure:^(NSString *errorStr, id mark) {
              
          }];
}

/**
 *  是否开启隐私设置
 *
 */
+(void)turnPrivateNickWithKey:(NSString *)ket
                     nickName:(NSString *)nickName
                       isNick:(NSString *)isnick
                     Delegate:(id <RequestDelegate>)delegate
                      success:(void (^)(NSDictionary * response))block
                      failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=pbnick" delegate:delegate parameters:@{@"key":ket,@"nickname":nickName,@"isnick":isnick} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 修改邮箱


+(void)exchangeEmailWithKey:(NSString *)key
                    content:(NSString *)content
                   Delegate:(id <RequestDelegate>)delegate
                    success:(void (^)(NSDictionary * response))block
                    failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=updmail" delegate:delegate parameters:@{@"key":key,@"mail":content} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr,mark);
    }];
}

#pragma mark  验证原始密码

+(void)vaildePasswordWithKey:(NSString *)key
                      oldPwd:(NSString *)oldpwd
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=verifypwd" delegate:delegate parameters:@{@"key":key,@"oldpwd":oldpwd} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr,mark);
    }];
}


#pragma mark 修改登录密码

+(void)vaildePasswordWithKey:(NSString *)key
                      oldPwd:(NSString *)oldpwd
                      newpwd:(NSString *)pwd
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errorStr, id mark))failure
{
    [self postUrl:@"/Api/User/User.ashx?action=updpwd" delegate:delegate parameters:@{@"key":key,@"oldpwd":oldpwd,@"newpwd":pwd} success:^(NSDictionary *response, id mark) {
        block(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

@end
