//
//  RequestApi.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DiDi+CoreDataClass.h"
#import <AFNetworking.h>
@protocol RequestDelegate <NSObject>

@optional
- (void)protocolWillRequest;
- (void)protocolDidRequestSuccess;
- (void)protocolDidRequestFailure:(NSString *) errorStr;

@end

typedef void(^SuccessDicBlock)(NSDictionary *dic);

@interface RequestApi : NSObject

//@property(strong,nonatomic)AFHTTPSessionManager *manager;

@property (nonatomic ,strong) NSString *lastID;         //lastID

@property (strong,nonatomic)NSOperationQueue *operationQueue;

@property (nonatomic ,strong) NSManagedObjectContext *context;

@property (strong,nonatomic)AFHTTPSessionManager *manager;

+(instancetype)share;

#pragma mark 网络请求
// post
+ (void)postUrl:(NSString *)URL
       delegate:(id <RequestDelegate>)delegate
     parameters:(NSDictionary *)parameters
        success:(void (^)(NSDictionary * response, id mark))success
        failure:(void (^)(NSString * errorStr, id mark))failure;

//get
+ (void)getUrl:(NSString *)URL
    parameters:(NSDictionary *)parameters
       success:(void (^)(NSDictionary * response, id mark))success
       failure:(void (^)(NSString * errorStr, id mark))failure;
/**
 *  上传图片到阿里云
 */
-(void)getImageUrlWithAliYunWithImageData:(NSData *)imageData
                   BlockHandelImageUrlStr:(void (^)(NSString * str))imageurlStr
                       BlockHandelSuccess:(void (^)(BOOL success))block;

/**
 *  上传多张图片到阿里云
 */
- (void)updateImageAry:(NSArray *)aryDatas
               success:(void(^)(void))success
                  fail:(void(^)(void))fail;
//数组
-(void)getImageUrlWithAliYunWithImageDataArray:(NSArray *)imageArrayData
                   BlockHandelImageUrlStr:(void (^)(NSString * str))imageurlStr
                       BlockHandelSuccess:(void (^)(BOOL success))block;
@end
