//
//  RequestApi+News.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi+News.h"


@implementation RequestApi (News)
//谁看过我未读条数
+(void)postSeeRecordNumWithKey:(NSString *)key
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString *errorStr))failure
{
    [self postUrl:@"/Api/SeeRecord/SeeRecord.ashx?action=getseerecordcountp" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}


//简历状态通知未读条数

+(void)postBriefStatusWithKey:(NSString *)key
                     Delegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))success
                      failure:(void (^)(NSString *errorStr))failure
{
    [self postUrl:@"/Api/ApplyRecord/ApplyRecord.ashx?action=getapprecordcountp" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//小秘书未读条数
+(void)postSeceretaryNumberWithKey:(NSString *)key
                          Delegate:(id<RequestDelegate>)delegate
                           Success:(void (^)(NSDictionary *))success
                           failure:(void (^)(NSString *))failure
{
    [self postUrl:@"/Api/Secretary/Secretary.ashx?action=msgnrc" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

/**
 *  获取小秘书最后一条信息
 */
+(void)secretaryMsgListWithKey:(NSString *)key
                           top:(NSString *)top
                      Delegate:(id<RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString *errorStr))failure
{
    [self postUrl:@"/Api/Secretary/Secretary.ashx?action=msglist" delegate:delegate parameters:@{@"key":key,@"top":top} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 改变未读条数
//谁看过我
+(void)changeNoReadNumberWithWHOSeeMeWithKey:(NSString *)key
                                    Delegate:(id <RequestDelegate>)delegate
                                     Success:(void (^)(NSDictionary * response))success
                                     failure:(void (^)(NSString *errorStr))failure
{
    [self postUrl:@"/Api/SeeRecord/SeeRecord.ashx?action=updrecordseep" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//简历状态
+(void)changeNoReadNumberWithBriefStatusWithKey:(NSString *)key
                                           back:(NSString *)back
                                       Delegate:(id <RequestDelegate>)delegate
                                        Success:(void (^)(NSDictionary * response))success
                                        failure:(void (^)(NSString *errorStr))failure
{
    [self postUrl:@"/Api/ApplyRecord/ApplyRecord.ashx?action=updrecordappp" delegate:delegate parameters:@{@"key":key,@"back":back} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}


#pragma mark  简历的求职状态
+(void)changeBriefStatusWithKey:(NSString *)key
                         status:(NSString *)status
                       Delegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))success
                        failure:(void (^)(NSString *errorStr))failure
{
    [self postUrl:@"/v2/Api/Resume/Resume.ashx?action=updrstatus" delegate:delegate parameters:@{@"key":key,@"status":status} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

/**
 *  获取小秘书列表
 */
+(void)getSmallSecepartyWithKey:(NSString *)key
                            top:(NSString *)top
                           stid:(NSString *)stid
                       Delegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))success
                        failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/Secretary/Secretary.ashx?action=msglist" delegate:delegate parameters:@{@"key":key,@"top":top,@"stid":stid} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

#pragma mark 发送小秘书

+(void)postSmallSeceretaryNewsWithKey:(NSString *)key
                             contents:(NSString *)contents
                             Delegate:(id <RequestDelegate>)delegate
                              Success:(void (^)(NSDictionary * response))success
                              failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/Secretary/Secretary.ashx?action=sendmsg" delegate:delegate parameters:@{@"key":key,@"contents":contents} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark  简历上架 下架

+(void)briefOnDownOnlineWithKey:(NSString *)key
                            rid:(NSString *)rid
                         status:(NSString *)status
                       Delegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))success
                        failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=updrstatus" delegate:delegate parameters:@{@"key":key,@"rid":rid,@"status":status} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        NSLog(@"%@",errorStr);
        failure(errorStr);
    }];
}

#pragma mark  删除简历

+(void)deleteBriefWithKey:(NSString *)key
                      rid:(NSString *)rid
                 Delegate:(id <RequestDelegate>)delegate
                  Success:(void (^)(NSDictionary * response))success
                  failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=delrs" delegate:delegate parameters:@{@"key":key,@"resid":rid} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark  获取简历完整度

+(void)getCompleteDataWithKey:(NSString *)key
                     Delegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))success
                      failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=getrfull" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
#pragma mark  获取简历完整度提示

+(void)getAlertResumeWithKey:(NSString *)key
                    Delegate:(id <RequestDelegate>)delegate
                     Success:(void (^)(NSDictionary * response))success
                     failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/v1/Api/Resume/Resume.ashx?action=getrpromt" delegate:delegate parameters:@{@"key":key} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark  获取收藏列表

+(void)savePositionListWithKey:(NSString *)key
                           top:(NSString *)top
                        pageID:(NSString *)pageID
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString * str))failure
{
    [RequestApi postUrl:@"/Api/Collection/Collection.ashx?action=collectionlist" delegate:delegate
             parameters:@{@"key":key,@"top":top,@"id":pageID} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}
#pragma mark  删除收藏的职位
+(void)deleteSavePositionWithKey:(NSString *)key
                             pid:(NSString *)pid
                          saveId:(NSString *)saveID
                        Delegate:(id <RequestDelegate>)delegate
                         Success:(void (^)(NSDictionary * response))success
                         failure:(void (^)(NSString * str))failure
{
    [self postUrl:@"/Api/Collection/Collection.ashx?action=cancelclction" delegate:delegate
       parameters:@{@"key":key,@"pid":pid,@"id":saveID} success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        failure(errorStr);
    }];
}

#pragma mark 获取申请记录
+(void)getarlistWithKey:(NSString *)key
              pageIndex:(NSInteger)pageIndex status:(NSString *)status
                    top:(NSString *)top lastId:(NSString *)lastId
               Delegate:(id <RequestDelegate>)delegate
                Success:(void (^)(NSDictionary * response))success
                failure:(void (^)(NSString * str))failure
{
    [RequestApi postUrl:@"/Api/ApplyRecord/ApplyRecord.ashx?action=getarlist"
               delegate:delegate parameters:
     @{@"key":key,@"arid":lastId,@"type":status,@"top":top,@"status":@"0"}
                success:^(NSDictionary *response, id mark) {
        success(response);
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

//获取
-(void)getNoticesListWithTop:(NSString *)top
                   PageIndex:(NSInteger)pageindex
                    Delegate:(id<RequestDelegate>)delegate
                     success:(void (^)(BOOL bStr))blockMessage
                     Success:(void (^)(NSDictionary *))blockDic
{
    DSWeak;
    if (pageindex == 1) {
        self.lastID = @"0";
    }
    
    [RequestApi postUrl:@"/Page/Notice/Notice.ashx?action=nlists" delegate:delegate parameters:@{@"top":top,@"nid":self.lastID,@"type":@"0"} success:^(NSDictionary *response, id mark) {
        
        NSArray * datas = [NSArray arrayWithArray:response[@"datas"]];
        if (datas.count>0)
        {
            //                           weak_self.lastTime = [NSString stringWithFormat:@"%@",datas.lastObject[@"UpdateDate"]];
            weakSelf.lastID = [NSString stringWithFormat:@"%@",datas.lastObject[@"ID"]];
            if (pageindex == 1)
            {
                [weakSelf deletePositionModelsBlockHandelDoJobSuccess:^(bool success)
                 {
                     [weakSelf inseartPositionArr:datas];
                 }];
            }
            else
            {
                [weakSelf inseartPositionArr:datas];
            }
            blockMessage(YES);
        }
        blockMessage(NO);
        blockDic(response);
    } failure:^(NSString *errorStr, id mark) {
        NSDictionary *d = @{@"code":@-1000};
        blockDic(d);
    }];
}

-(void)inseartPositionArr:(NSArray *)datas
{
    for (NSDictionary * dic in datas)
    {
        DiDi * model = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DiDi class]) inManagedObjectContext:self.context];
        
        model.nImage = [NSString stringWithFormat:@"%@",dic[@"N_Img"]];
        model.nTitle = [NSString stringWithFormat:@"%@",dic[@"N_Title"]];
        model.nTime = [NSString stringWithFormat:@"%@",dic[@"CreateDate"]];
        model.nContent = [NSString stringWithFormat:@"%@",dic[@"N_Description"]];
        model.nID = [NSString stringWithFormat:@"%@",dic[@"ID"]];
        model.status = @"0";
        model.nAddress = [NSString stringWithFormat:@"%@",dic[@"N_Url"]];
        
        NSLog(@"%@",model);
        [self.context save:nil];
    }
}

-(void)deletePositionModelsBlockHandelDoJobSuccess:(void (^)(bool bStr))isSuccess
{
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([DiDi class])];
    NSSortDescriptor * sortD = [NSSortDescriptor sortDescriptorWithKey:@"nID" ascending:NO];
    [request setSortDescriptors:@[sortD]];
    
    NSError * error;
    NSArray * result = [self.context executeFetchRequest:request error:&error];
    if (result ==nil) {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }
    if (result.lastObject) {
        for (int i = 0; i<result.count; i++) {
            [self.context deleteObject:result[i]];
        }
    }
    if (![self.context save:&error]) {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }else
    {
        //        NSLog(@"已经删除本地Models");
        isSuccess(YES);
    }
    
}



@end
