//
//  FirstMainManager.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "FirstMainManager.h"
#import "PositionCoreData+CoreDataClass.h"
#import "FirstPositionModel.h"

@interface FirstMainManager ()
@property (nonatomic ,strong)NSManagedObjectContext * context;

@end

@implementation FirstMainManager

+(instancetype)share
{
    static FirstMainManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[FirstMainManager alloc]init];
        manager.operationQueue = [[NSOperationQueue alloc]init];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [self.manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        self.manager.requestSerializer.timeoutInterval = 5.f;
////        //配置CoreData
        UIApplication *app = [UIApplication sharedApplication];
        id delegate = app.delegate;
        self.context = [delegate managedObjectContext];
        
    }
    return self;
}

/**
 *  首页职位详情信息
 */

-(void)GetPositionInfoWithPid:(NSString *)pid key:(NSString *)key success:(DicBlock)block
                       //faiure:(void(^)(NSDictionary *))failure
{
    NSLog(@"%@",@{@"pid":pid,@"key":key});
    NSString *str = [NSString stringWithFormat:@"%@/Api/Position/Position.ashx?action=getpositioninfo",HTTP];

    [self.manager POST:str parameters:@{@"pid":pid,@"key":key} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"workDetail%@",dic);
        block(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSDictionary *dic = @{@"code":@-1000};
        block(dic);
    }];
}

-(void)getPositionWithkey:(NSString *)key Top:(NSString *)top cid:(NSString *)cid position:(NSString *)position positionID:(NSString *)positionID industryID:(NSString *)industryID province:(NSString *)province city:(NSString *)city success:(DicBlock)block PageIndex:(NSInteger)pageindex status:(NSString *)status BlockHandelDoJobSuccess:(StrBlock)isSuccess
{
    NSString *str;
    NSString *strB;
    _mainStatus = status;
    NSDictionary *dic;
    if (pageindex == 1)
    {
        self.lastID = @"0";
    }
    if ([_mainStatus isEqualToString:@"firstPosition"] || [_mainStatus isEqualToString:@"searchPosition"])
    {
        str = [NSString stringWithFormat:@"%@/Api/Position/Position.ashx?action=getposition",HTTP];
        dic = @{
                    @"key":key,@"top":top,@"pid":self.lastID,
                    @"province":province,@"city":city,
                    @"searchtitle":position,
                    @"idy":industryID,@"ptype":positionID
                    };
    }
    DSWeak;
    [weakSelf.manager POST:str parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"%@",dic);
        if ([dic[@"code"] isEqualToNumber:RESPONSE_CODE_200])
        {
            NSArray *datasArr = dic[@"datas"];
            if (datasArr.count > 0)
            {
                weakSelf.lastID = datasArr.lastObject[@"row"];
                if (pageindex == 1) {
                    [weakSelf deletePositionModelsBlockHandelDoJobSuccess:^(BOOL success)
                    {
                        [weakSelf inseartPositionArr:datasArr status:_mainStatus];
                    } status:_mainStatus];
                }else{
                    [weakSelf inseartPositionArr:datasArr status:_mainStatus];
                }
                isSuccess(@"成功");
            }else{
                if (pageindex == 1)
                {
                    isSuccess(@"没有数据");
                }else{
                    isSuccess(@"10000");    //没有更多数据
                }
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        isSuccess(@"100");
    }];
}

-(void)inseartPositionArr:(NSArray *)datas status:(NSString *)status
{
    for (NSDictionary * dic in datas)
    {
        PositionCoreData * model = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PositionCoreData class]) inManagedObjectContext:self.context];
        model.c_Logo = [NSString stringWithFormat:@"%@",dic[@"C_Logo"]];
        
        model.p_Name= [NSString stringWithFormat:@"%@",dic[@"P_Name"]];
        
        model.c_Name = [NSString stringWithFormat:@"%@",dic[@"C_Name"]];
        
        model.p_City = [NSString stringWithFormat:@"%@",dic[@"P_City"]];
        
        model.p_Exp = [NSString stringWithFormat:@"%@",dic[@"P_Exp"]];
        
        model.p_Education = [NSString stringWithFormat:@"%@",dic[@"P_Education"]];
        
        model.c_Scale = [NSString stringWithFormat:@"   %@   ",dic[@"C_Scale"]];
        
        model.i_Name = [NSString stringWithFormat:@"     %@    ",dic[@"I_Name"]];
        
        model.c_Time = [NSString stringWithFormat:@"%@ ",dic[@"UpdateDate"]];
        
        model.p_Pay = [NSString stringWithFormat:@"%@",dic[@"P_Pay"]];
        
        model.c_Pid = [NSString stringWithFormat:@"%@",dic[@"ID"]];
        
//        model.messageID = [NSString stringWithFormat:@"%@",dic[@"ID"]];
//        model.sortD = [NSString stringWithFormat:@"%@",dic[@"row"]];
        
        model.c_Auitstatus = [NSString stringWithFormat:@"%@",dic[@"C_AduitStatus"]];//判断企业是否认证
        model.isVideo = [NSString stringWithFormat:@"%@",dic[@"IsVideo"]];
        
        model.c_Cid = [NSString stringWithFormat:@"%@",dic[@"C_ID"]];     //公司id
        
        model.status = status;
//        int i = 0;
//        i ++;
//        model.positionSortID = [NSString stringWithFormat:@"%d",i];
        [self.context save:nil];
    }
}

-(void)deletePositionModelsBlockHandelDoJobSuccess:(SuccessBlock)isSuccess status:(NSString *)status
{
    NSFetchRequest * request;
//    NSSortDescriptor * sortD;
    NSPredicate* predicate;
    NSLog(@"%@",status);

    request = [NSFetchRequest fetchRequestWithEntityName:@"PositionCoreData"];
    
    predicate = [NSPredicate predicateWithFormat:@"status == %@",_mainStatus];

    
    request.predicate = predicate;
    
    
    
    
    NSError * error;
    NSArray * result = [self.context executeFetchRequest:request error:&error];
    if (result ==nil)
    {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }
    if (result.lastObject)
    {
        for (int i = 0; i<result.count; i++)
        {
            [self.context deleteObject:result[i]];
        }
    }
    if (![self.context save:&error]) {
        //        NSLog(@"%@",error.localizedDescription);
        return;
    }else
    {
        isSuccess(YES);
    }
    
}


//-(void)getPositionWithkeyssssssss:(NSString *)key Top:(NSString *)top cid:(NSString *)cid position:(NSString *)position province:(NSString *)province city:(NSString *)city edid:(NSString *)edid exid:(NSString *)exid success:(PositionBlock)block PageIndex:(NSInteger)pageindex status:(NSString *)status BlockHandelDoJobSuccess:(LoginDoBlock)isSuccess
//{
//    if(pageindex==1){
//        self.lastID = @"0";
//    }else{
//        
//    }
//    NSString *str;
//    
//    _mainStatus = status;
//    NSDictionary *dic;
//    
//    if ([_mainStatus isEqualToString:@"firstPosition"]||[_mainStatus isEqualToString:@"searchPosition"]) {
//        str = [NSString stringWithFormat:@"%@/Api/Position/Position.ashx?action=getposition",HTTP];
//        dic =@{@"key":key,@"top":top,@"pid":self.lastID,@"province":province,@"city":city,@"searchtitle":position};
//    }else if([_mainStatus isEqualToString:@"firstResume"] || [_mainStatus isEqualToString:@"searchBrief"]){
//        str = [NSString stringWithFormat:@"%@/v1/Api/Resume/Resume.ashx?action=getresumelist",HTTP];
//        //        self.lastID = @"rid";
//        dic =@{@"key":key,@"cid":cid,@"top":top,@"rid":self.lastID,@"position":position,
//               @"province":province,@"city":city,@"edid":edid,@"exid":exid};
//    }
//    
//    
//    NSLog(@"%@",dic);
//    NSLog(@"%ld",pageindex);
//    
//    __weak __block MainManager * weak_self = self;
//    
//    [self.manager POST:str parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        NSLog(@"firstModelCoredata%@",dic);
//        
//        if ([dic[@"code"] isEqualToNumber:@(200)])
//        {
//            NSArray * datas = [NSArray arrayWithArray:dic[@"datas"]];
//            NSLog(@"%@",datas);
//            if (datas.count>0)
//            {
//                if ([_mainStatus isEqualToString:@"firstPosition"]||[_mainStatus isEqualToString:@"searchPosition"]) {
//                    //                    self.lastID = @"pid";
//                    weak_self.lastID = [NSString stringWithFormat:@"%@",datas.lastObject[@"row"]];
//                }else if([_mainStatus isEqualToString:@"firstResume"]|| [_mainStatus isEqualToString:@"searchBrief"] ){
//                    weak_self.lastID = [NSString stringWithFormat:@"%@",datas.lastObject[@"row"]];
//                }
//                
//                if (pageindex == 1)
//                {
//                    [weak_self deletePositionModelsBlockHandelDoJobSuccess:^(BOOL success) {
//                        [weak_self inseartPositionArr:datas status:status];
//                    } status:status];
//                }else
//                {
//                    [weak_self inseartPositionArr:datas status:status];
//                }
//                if ([dic[@"code"]isEqualToNumber:@-100]) {
//                    isSuccess(dic[@"code"]);
//                }else if([dic[@"code"]isEqualToNumber:@200]){
//                    isSuccess(@"已无更多数据");
//                }
//            }else{
//                //                [weak_self deletePositionModelsBlockHandelDoJobSuccess:^(BOOL success) {
//                //
//                //                } status:status];
//                isSuccess(@"空");
//            }
//        }else if([dic[@"code"]isEqualToNumber:@-100])
//        {
//            isSuccess(@"未登录状态");
//        }else
//        {
//            isSuccess(@"-100");
//        }
//        
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"====%@====",[error localizedDescription]);
//        
//        isSuccess(@"网络状态不好");
//    }];
//    
//}



@end
