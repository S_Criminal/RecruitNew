//
//  FirstMainManager.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/30.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DicBlock)(NSDictionary *dic);
typedef void(^StrBlock)(NSString *str);
typedef void(^SuccessBlock)(BOOL success);

@interface FirstMainManager : NSObject

@property(strong,nonatomic)AFHTTPSessionManager *manager;
@property(strong,nonatomic)NSOperationQueue *operationQueue;

@property (nonatomic ,strong) NSString *mainStatus;
@property (nonatomic ,strong) NSString *lastID;


@property (nonatomic ,strong) NSString *workDetail_pid;

+(instancetype)share;

//-(void)getResumeWithKey:(NSString *)key success:(BriefBlock)block;

/**
 *  获取首页职位列表信息
 */
-(void)getPositionWithkey:(NSString *)key Top:(NSString *)top cid:(NSString *)cid position:(NSString *)position positionID:(NSString *)positionID industryID:(NSString *)industryID province:(NSString *)province city:(NSString *)city success:(DicBlock)block PageIndex:(NSInteger)pageindex status:(NSString *)status BlockHandelDoJobSuccess:(StrBlock)isSuccess;
/**
 *  首页职位详情信息
 */
-(void)GetPositionInfoWithPid:(NSString *)pid key:(NSString *)key success:(DicBlock)block;

@end
