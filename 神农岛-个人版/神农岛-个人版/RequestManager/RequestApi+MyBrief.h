//
//  RequestApi+MyBrief.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/6.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi.h"

@interface RequestApi (MyBrief)


/**
 *  是否填写标识
 */
+(void)getBriefFillWithKey:(NSString *)key
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  保存用户基本信息
 */
+(void)saveNewBriefInfoWithKey:(NSString *)key
                         uhead:(NSString *)uhead
                          name:(NSString *)name
                           sex:(NSString *)sex
                      birthday:(NSString *)birthday
                     provience:(NSString *)provience
                          city:(NSString *)city
                      Delegate:(id<RequestDelegate>)delegate
                       success:(void (^)(NSDictionary *response))block;

/**
 *  保存用户自我描述
 */
+(void)saveMySelfWithKey:(NSString *)key
                         selfevaluation:(NSString *)selfevaluation
                      Delegate:(id<RequestDelegate>)delegate
                       success:(void (^)(NSDictionary *response))block
                 failure:(void (^)(NSString * errorStr, id mark))failure;
/*
 *  获取简历信息
 */
+(void)getResumeWithKey:(NSString *)key
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString * errorStr, id mark))failure;
/**
 *  获取屏蔽列表
 */
+(void)getsClistWithKey:(NSString *)key top:(NSString *)top
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString * errorStr, id mark))failure;
/**
 *  删除屏蔽公司
 */
+(void)deletePrivacyCompany:(NSString *)key
                         ids:(NSString *)ids
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  模糊搜索公司列表
 */
+(void)searchCompanyNameWithKey:(NSString *)key top:(NSString *)top
                          cName:(NSString *)cname
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block
                failure:(void (^)(NSString * errorStr, id mark))failure;



/**
 *  编辑添加屏蔽企业 
 */
+(void)editAddCompanyNameWithKey:(NSString *)key editID:(NSString *)editID
                             cid:(NSString *)cid
                          cName:(NSString *)cname
                       Delegate:(id<RequestDelegate>)delegate
                        success:(void (^)(NSDictionary *response))block
                        failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  获取钱包余额、推广人数、实名认证
 */
+(void)getDateWithKey:(NSString *)key
                      Delegate:(id<RequestDelegate>)delegate
                       success:(void (^)(NSDictionary *response))block;
/**
 *  申请实名认证
 */
+(void)getCardApplyWithKey:(NSString *)key
                  cardpath:(NSString *)cardpath
                  Delegate:(id<RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response, id mark))success
                   failure:(void (^)(NSString * errorStr, id mark))failure;
/**
 *  编辑教育经历
 *  edit 判断是新增还是编辑
 */
//   /v1/Api/EducationExp/EducationExp.ashx?action=editeduexp
+(void)editEduexpWithKey:(NSString *)key
                    edit:(NSString *)edit expID:(NSString *)expID
                  school:(NSString *)school major:(NSString *)major
                  startd:(NSString *)startd endd:(NSString *)endd
                 schlogo:(NSString *)schlogo
                Delegate:(id <RequestDelegate>)delegate
                  degree:(NSString *)degree
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * str))failure;
/**
 *  删除教育经历
 *
 */
+(void)deleteEduWithKey:(NSString *)key
                 workID:(NSString *)workID
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block;

/**
 *  编辑工作经历
 *  edit 判断是新增还是编辑
 *  /v1/Api/WorkExp/WorkExp.ashx?action=editworkexp
 */


+(void)editExperienceWithKey:(NSString *)key
                    expID:(NSString *)expID
                  comname:(NSString *)comname position:(NSString *)position
                  entryd:(NSString *)startd quitd:(NSString *)quitd
                     content:(NSString *)content
                       quits:(NSString *)quits
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * str))failure;
/**
 *  删除工作经历
 *  /v1/Api/WorkExp/WorkExp.ashx?action=delworkexp
 */

+(void)deleteExperienceWithKey:(NSString *)key
                 workID:(NSString *)workID
               Delegate:(id<RequestDelegate>)delegate
                success:(void (^)(NSDictionary *response))block;
/**
 *  编辑项目经历
 */
+(void)editProjectExpWithKey:(NSString *)key
                        expID:(NSString *)expID
                     project:(NSString *)project identity:(NSString *)identity
                      startd:(NSString *)startd endd:(NSString *)quitd
                     description:(NSString *)content
                      online:(NSString *)online
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * str))failure;
/**
 *  删除项目经历
 */
+(void)deleteProjectExpWithKey:(NSString *)key
                       expID:(NSString *)expID
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block;

/**
 *  获取职位偏好
 */
+ (void)getBriefPrtendInfoWithKey:(NSString *)key
                         Delegate:(id<RequestDelegate>)delegate
                          success:(void (^)(NSDictionary *response))block
                          failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  编辑职位偏好
 */
+(void)editPositionTrendWithKey:(NSString *)key
                  rprovince:(NSString *)rprovince
                      rcity :(NSString *)rcity
                        pay :(NSString *)pay
                      payid :(NSString *)payid
                     rtypes :(NSString *)rtypes
                     nature:(NSString *)nature
                    Delegate:(id<RequestDelegate>)delegate
                     success:(void (^)(NSDictionary *response))block;

/**
 *  编辑期望城市
 */

+(void)upLoadNewBriefCityWithKey:(NSString *)key
                       rprovince:(NSString *)rprovince
                           rcity:(NSString *)rcity
                        Delegate:(id <RequestDelegate>)delegate
                         success:(void (^)(NSDictionary * response))block
                         failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  编辑期望职位
 */
+(void)upLoadNewBriefPositionWithKey:(NSString *)key
                              rTypes:(NSString *)rtypes
                            Delegate:(id <RequestDelegate>)delegate
                             success:(void (^)(NSDictionary * response))block
                             failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  编辑联系信息
 *
 */
+(void)editContactInfoWithKey:(NSString *)key
                              phone:(NSString *)phone
                               mail:(NSString *)mail
                               qq:(NSString *)qq
                               wechat:(NSString *)wechat
                            Delegate:(id <RequestDelegate>)delegate
                             success:(void (^)(NSDictionary * response))block;

/**
 *  获取个人成就列表
 */

+(void)getkillsListWithKey:(NSString *)key
                    sclass:(NSString *)sclass
                       Delegate:(id <RequestDelegate>)delegate
                        success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * errorStr, id mark))failure;
/**
 *  获取个人成就信息
 */
+(void)getSkillCertInfoWithKey:(NSString *)key
                    sclass:(NSString *)sclass
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSString * errorStr, id mark))failure;
///删除个人成就
+(void)requestDelskillcertWithKey:(NSString *)key
                               ID:(NSString *)ID
                         Delegate:(id <RequestDelegate>)delegate
                          success:(void (^)(NSDictionary * response))block
                          failure:(void (^)(NSString * errorStr, id mark))failure;
/**
 *  编辑个人成就
 *
 */
+(void)editPersonAchieveWithKey:(NSString *)key
                             editID:(NSString *)editID
                       contents:(NSString *)contents
                          spath:(NSString *)spath
                          sdesp:(NSString *)sdesp
                          sdate:(NSString *)sdate
                         stitle:(NSString *)stitle
                        sclass:(NSString *)sclass
                            Delegate:(id <RequestDelegate>)delegate
                             success:(void (^)(NSDictionary * response))block
                        failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  获取生活照
 */

+(void)getLifeImageWithKey:(NSString *)key
                  Delegate:(id <RequestDelegate>)delegate
                   success:(void (^)(NSDictionary * response))block
                   failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  编辑生活照片
 *
 */
+(void)editPersonLifeWithKey:(NSString *)key
                    lifeimg :(NSString *)lifeimg
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  是否开启隐私设置
 *
 */
+(void)turnPrivateNickWithKey:(NSString *)ket
                     nickName:(NSString *)nickName
                       isNick:(NSString *)isnick
                     Delegate:(id <RequestDelegate>)delegate
                      success:(void (^)(NSDictionary * response))block
                      failure:(void (^)(NSString * errorStr, id mark))failure;

/** 
 *  修改邮箱
 */

+(void)exchangeEmailWithKey:(NSString *)key
                    content:(NSString *)content
                   Delegate:(id <RequestDelegate>)delegate
                    success:(void (^)(NSDictionary * response))block
                    failure:(void (^)(NSString * errorStr, id mark))failure;


/**
 *  验证原始密码
 */

+(void)vaildePasswordWithKey:(NSString *)key
                    oldPwd:(NSString *)oldpwd
                   Delegate:(id <RequestDelegate>)delegate
                    success:(void (^)(NSDictionary * response))block
                    failure:(void (^)(NSString * errorStr, id mark))failure;

/**
 *  修改登录密码
 */

+(void)vaildePasswordWithKey:(NSString *)key
                      oldPwd:(NSString *)oldpwd
                      newpwd:(NSString *)pwd
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errorStr, id mark))failure;

@end

