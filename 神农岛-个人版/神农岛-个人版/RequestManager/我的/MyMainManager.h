//
//  MyMainManager.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/28.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^BriefBlock)(NSDictionary *dic);

@interface MyMainManager : NSObject

@property(strong,nonatomic)AFHTTPSessionManager *manager;

@property(strong,nonatomic)NSOperationQueue *operationQueue;


@property (nonatomic ,strong) NSString *ruleTitle;  //规则标题
@property (nonatomic ,strong) NSString *ruleKey;    //规则需要传递的key

+(void)getBuyNoticeWithKey:(NSString *)key cKey:(NSString *)cKey success:(BriefBlock)block;

+(instancetype)share;
/*
 *  简历信息
 */
-(void)getResumeWithKey:(NSString *)key success:(BriefBlock)block failure:(void (^)(NSString * errorStr, id mark))failure;
/*
 *  规则 html
 */
-(void)getBuyNoticeWithKey:(NSString *)key cKey:(NSString *)cKey success:(BriefBlock)block;
/*
 *  实名认证
 */
-(void)getCardApplyWithKey:(NSString *)key cardpath:(NSString *)cardpath success:(BriefBlock)block;
@end
