//
//  MyMainManager.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/28.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "MyMainManager.h"

@implementation MyMainManager

//DECLARE_SINGLETON(MyMainManager)
+(instancetype)share{
    static MyMainManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[MyMainManager alloc]init];
        manager.operationQueue = [[NSOperationQueue alloc]init];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [AFHTTPSessionManager manager];
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    return self;
}

+(void)getBuyNoticeWithKey:(NSString *)key cKey:(NSString *)cKey success:(BriefBlock)block
{
    [[MyMainManager share] getBuyNoticeWithKey:key cKey:cKey success:block];
}

#pragma mark  获取简历预览界面
//   /Api/Resume/Resume.ashx?action=getresumes
-(void)getResumeWithKey:(NSString *)key success:(BriefBlock)block failure:(void (^)(NSString * errorStr, id mark))failure
{
    NSString *str = [NSString stringWithFormat:@"%@/Api/Resume/Resume.ashx?action=getresumes",HTTP];
    [self.manager POST:str parameters:@{@"key":key} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"Resume%@",dic);
        block(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(@"失败",task);
        NSLog(@"%@",[error description]);
    }];
}

#pragma mark  规则 HTML
-(void)getBuyNoticeWithKey:(NSString *)key cKey:(NSString *)cKey success:(BriefBlock)block
{
    NSString *str = [NSString stringWithFormat:@"%@/Page/SysConfig/SysConfig.ashx?action=scinfos",HTTP];
    if (key.length < 7) {
        key = @"";
    }
    [self.manager POST:str parameters:@{@"key":key,@"ckey":cKey} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"%@",dic);
        block(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",[error description]);
    }];
}

#pragma mark 实名认证
//  /Api/User/User.ashx?action=cardapply
-(void)getCardApplyWithKey:(NSString *)key cardpath:(NSString *)cardpath success:(BriefBlock)block
{
    NSString *str = [NSString stringWithFormat:@"%@/Api/User/User.ashx?action=cardapply",HTTP];
    [self.manager POST:str parameters:@{@"key":key,@"cardpath":cardpath} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        block(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

@end
