//
//  RequestApi+News.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/9.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi.h"

@interface RequestApi (News)

/**
 *  获取小秘书列表/Api/Secretary/Secretary.ashx?action=msglist
 */
+(void)getSmallSecepartyWithKey:(NSString *)key
                            top:(NSString *)top
                           stid:(NSString *)stid
                       Delegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))success
                        failure:(void (^)(NSString * str))failure;
//发送小秘书
+(void)postSmallSeceretaryNewsWithKey:(NSString *)key
                             contents:(NSString *)contents
                             Delegate:(id <RequestDelegate>)delegate
                              Success:(void (^)(NSDictionary * response))success
                              failure:(void (^)(NSString * str))failure;
/**
 *  简历上架 下架
 */
+(void)briefOnDownOnlineWithKey:(NSString *)key
                            rid:(NSString *)rid
                         status:(NSString *)status
                     Delegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))success
                      failure:(void (^)(NSString * str))failure;
/**
 *  删除简历
 */
+(void)deleteBriefWithKey:(NSString *)key
                            rid:(NSString *)rid
                       Delegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))success
                        failure:(void (^)(NSString * str))failure;
/**
 *  获取简历完整度
 */
+(void)getCompleteDataWithKey:(NSString *)key
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString * str))failure;
/**
 *  获取简历完整度提示
 */
+(void)getAlertResumeWithKey:(NSString *)key
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString * str))failure;
/*
 *  获取didi列表
 */
-(void)getNoticesListWithTop:(NSString *)top PageIndex:(NSInteger )pageindex
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(BOOL str))blockMessage
                     Success:(void (^)(NSDictionary * response))blockDic;
/**
 *  获取收藏列表
 */
+(void)savePositionListWithKey:(NSString *)key
                           top:(NSString *)top
                        pageID:(NSString *)pageID
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString * str))failure;
/**
 * 删除收藏的职位
 */

+(void)deleteSavePositionWithKey:(NSString *)key
                             pid:(NSString *)pid
                          saveId:(NSString *)saveID
                        Delegate:(id <RequestDelegate>)delegate
                         Success:(void (^)(NSDictionary * response))success
                         failure:(void (^)(NSString * str))failure;

/**
 *  获取申请记录
 */
+(void)getarlistWithKey:(NSString *)key
              pageIndex:(NSInteger)pageIndex status:(NSString *)status
                    top:(NSString *)top lastId:(NSString *)lastId
               Delegate:(id <RequestDelegate>)delegate
                Success:(void (^)(NSDictionary * response))success
                failure:(void (^)(NSString * str))failure;

/**
 *  简历的求职状态
 */
+(void)changeBriefStatusWithKey:(NSString *)key
              status:(NSString *)status
               Delegate:(id <RequestDelegate>)delegate
                Success:(void (^)(NSDictionary * response))success
                failure:(void (^)(NSString *errorStr))failure;

#pragma mark 消息推送相关
/** 
 *  谁看过我
 */

+(void)postSeeRecordNumWithKey:(NSString *)key
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString *errorStr))failure;
/**
 *  简历状态通知 
 */

+(void)postBriefStatusWithKey:(NSString *)key
                      Delegate:(id <RequestDelegate>)delegate
                       Success:(void (^)(NSDictionary * response))success
                       failure:(void (^)(NSString *errorStr))failure;
/**
 *  获取小秘书未读条数
 */
+(void)postSeceretaryNumberWithKey:(NSString *)key
                          Delegate:(id <RequestDelegate>)delegate
                           Success:(void (^)(NSDictionary * response))success
                           failure:(void (^)(NSString *errorStr))failure;
/**
 *  获取小秘书最后一条信息
 */
+(void)secretaryMsgListWithKey:(NSString *)key
                           top:(NSString *)top
                      Delegate:(id <RequestDelegate>)delegate
                           Success:(void (^)(NSDictionary * response))success
                           failure:(void (^)(NSString *errorStr))failure;
#pragma mark 改变未读条数
//谁看过我
+(void)changeNoReadNumberWithWHOSeeMeWithKey:(NSString *)key
                                    Delegate:(id <RequestDelegate>)delegate
                                     Success:(void (^)(NSDictionary * response))success
                                     failure:(void (^)(NSString *errorStr))failure;
//简历状态
+(void)changeNoReadNumberWithBriefStatusWithKey:(NSString *)key
                                           back:(NSString *)back
                                    Delegate:(id <RequestDelegate>)delegate
                                     Success:(void (^)(NSDictionary * response))success
                                     failure:(void (^)(NSString *errorStr))failure;
@end
