//
//  RequestApi+Login.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "RequestApi.h"

@interface RequestApi (Login)
/**
 *  获取验证码
 *  注册和忘记密码
 */
+(void)getCodeWithTel:(NSString *)phone
                 Type:(NSString *)type
             Delegate:(id <RequestDelegate>)delegate
              success:(void (^)(NSDictionary * response))block
              failure:(void (^)(NSString * errostr))failure;

/**
 *  用户登录
 */
+(void)getLoginWithPhone:(NSString *)phone
                    Type:(NSString *)type
                Delegate:(id <RequestDelegate>)delegate
                     pwd:(NSString *)pwd
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * errostr))failure;
/**
 *  判断手机号和验证码是否正确
 */
+(void)phoneAndCodeWithPhone:(NSString *)phone
                     smscode:(NSString *)smscode
                        type:(NSString *)type
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errostr))failure;
/**
 *  注册账户 填写密码
 */
+(void)registerWithPhone:(NSString *)phone
                     pwd:(NSString *)pwd
                     smscode:(NSString *)smscode
                    Delegate:(id <RequestDelegate>)delegate
                     success:(void (^)(NSDictionary * response))block
                     failure:(void (^)(NSString * errostr))failure;
/** 
 *  修改密码
 */
+(void)forgetpwdWithPhone:(NSString *)phone
                 smscode:(NSString *)smscode
                     pwdf:(NSString *)pwdf
             Delegate:(id <RequestDelegate>)delegate
              success:(void (^)(NSDictionary * response))block
              failure:(void (^)(NSString * errostr))failure;
/**
 *  登录时 传设备id   
 */
+(void)bindDeviceWithKey:(NSString *)key
                     equimenumID:(NSString *)equimenumID
              equimetype:(NSString *)equimetype
                Delegate:(id <RequestDelegate>)delegate
                 success:(void (^)(NSDictionary * response))block
                 failure:(void (^)(NSString * errostr))failure;


#pragma mark 获取公共信息
//公用信息（薪资）
+(void)getSalarysListDelegate:(id <RequestDelegate>)delegate
                    Success:(void (^)(NSDictionary * response))block;
//公司规模列表
+(void)getScalesListDelegate:(id <RequestDelegate>)delegate
                     Success:(void (^)(NSDictionary * response))block;
// 公司性质列表
+(void)getnaturelListDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block;
//公司行业列表（类别）
+(void)getindustrylListDelegate:(id <RequestDelegate>)delegate
                        Success:(void (^)(NSDictionary * response))block
                        failure:(void(^)(NSString * str))failure;
//学历    简历的
+(void)getEduListWithDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block;
//公用信息 职位所有
+(void)getPositionTypeLsWithDelegate:(id <RequestDelegate>)delegate
                             Success:(void (^)(NSDictionary * response))block
                             failure:(void(^)(NSString * str))failure;
//公用信息 工作经验
+(void)getWorkExperienceDelegate:(id <RequestDelegate>)delegate
                         Success:(void (^)(NSDictionary * response))block;
//学历  职位的
+(void)getEduPositionListWithDelegate:(id <RequestDelegate>)delegate
                              Success:(void (^)(NSDictionary * response))block;
/** 公用信息 学校匹配 */
+(void)matchSchoolNameWithKey:(NSString *)key schoolName:(NSString *)name
                 WithDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block
                      failure:(void(^)(NSString * str))failure;
/** 公用信息 简历状态 */
+(void)statusBriefWithDelegate:(id <RequestDelegate>)delegate
                      Success:(void (^)(NSDictionary * response))block
                      failure:(void(^)(NSString * str))failure;


// 获取城市列表
+(void)getPublicInformationWithCityDelegate:(id <RequestDelegate>)delegate
                                        Success:(void (^)(NSDictionary * response))block
                                        failure:(void(^)(NSString * str))failure;
// 获取省份列表
+(void)getPublicInformationWithProvince:(NSString *)Province Delegate:(id <RequestDelegate>)delegate
                                        Success:(void (^)(NSDictionary * response))block
                                        failure:(void(^)(NSString * str))failure;

@end
