//
//  GlobalData.h
//  ChinaDream
//
//  Created by zhangfeng on 12-11-26.
//  Copyright (c) 2012年 eastedge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
////用户model
#import "BaseClass.h"
#import "PickerViewModel.h"
//#import "ModelUser.h
////公司model
//#import "ModelCompany.h"


//仅在appdelegate里赋值，无需做成实例变量
extern UINavigationController *GB_Nav;//全局导航条
//extern float GB_StartYCoordinate;//起始Y坐标
extern float STATUSBAR_HEIGHT;//状态栏高度
extern float NAVIGATIONBAR_HEIGHT;//导航条的高度
extern float TABBAR_HEIGHT;//工具栏高度

/*
 GB_UserName,GB_Pwd,GB_Permission会出现多次值的更改，所以要做成实例变量，可调用set方法来改变值
 这样可以避免写太多的alloc和release
 例如：要给GB_UserName重新赋值，[GB_UserName release];GB_UserName = nil;GB_UserName=[[NSString alloc] initWithString:@"aaa"];
 现在只需要这样调用[[GlobalData sharedGlobalData] setGB_UserName:@"aaa"];
 */

@interface GlobalData : NSObject

//单例
+ (GlobalData *)sharedInstance;


@property (nonatomic, strong) BaseClass * GB_UserModel;//用户模型
@property (nonatomic ,strong) PickerViewModel *GB_PickerModel;//pickerView模型
@property (nonatomic, strong) NSString * GB_Key;//登陆成功key
@property (nonatomic, strong) NSString * GB_UID;//登陆成功UID
@property (nonatomic, strong) NSString * GB_Phone;//登陆成功用户手机号
@property (nonatomic, strong) NSString * GB_DeviceToken;//设备号
@property (nonatomic, strong) NSString * isLogin;//判断是否是登录状态


@property (nonatomic ,strong) NSString * GB_PostNews;   //存放推送消息的

@property (nonatomic ,strong) NSString *birthDay;   //出生年月

@property (nonatomic ,strong) NSString *companyBriefHTML;//公司简介图片
@property (nonatomic ,strong) NSAttributedString *companyBriefHTMLAttribute;//公司简介html

@property (nonatomic ,assign) BOOL isDelivery;  //投递简历成功 

@property (nonatomic ,assign) CGFloat companyBriefHeight;

@property (nonatomic ,strong) NSString *city;
@property (nonatomic ,strong) NSString *provience;
@property (nonatomic ,strong) NSString *GB_JsonCitys;

@property (nonatomic ,strong) NSArray *position;//期望工作
@property (nonatomic ,strong) NSString *rtypesJson;

@property (nonatomic ,strong) NSString *cardPath;   //身份证图片
@property (nonatomic ,strong) NSString *cardStauts; //验证状态

@property (nonatomic ,strong) NSString *nickName;
@property (nonatomic ,strong) NSString *isNick;

@property (nonatomic ,strong) NSString *historyArray;



@property (nonatomic ,strong) NSString *trend_Complete;//职位偏好是否完善
@property (nonatomic ,strong) NSString *u_Email;



//@property (nonatomic, strong) NoticeView * GB_NoticeView;//global notice view

@end
