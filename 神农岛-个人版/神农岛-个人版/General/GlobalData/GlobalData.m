//
//  GlobalData.m
//  ChinaDream
//
//  Created by zhangfeng on 12-11-26.
//  Copyright (c) 2012年 eastedge. All rights reserved.
//

#import "GlobalData.h"

UINavigationController *GB_Nav = nil;
//int GB_OpenWaitShowCount = 0;
//float GB_StartYCoordinate = 0.0f;
float STATUSBAR_HEIGHT = 20.0f;//状态栏高度
float NAVIGATIONBAR_HEIGHT= 64.0f;//导航条的高度
float TABBAR_HEIGHT = 49.f;//工具栏高度

@implementation GlobalData
@synthesize GB_Key = _GB_Key;
@synthesize GB_UID = _GB_UID;
@synthesize GB_UserModel = _GB_UserModel;
@synthesize GB_PickerModel = _GB_PickerModel;
@synthesize GB_DeviceToken = _GB_DeviceToken;
@synthesize GB_PostNews    = _GB_PostNews;
@synthesize GB_Phone    = _GB_Phone;
@synthesize isLogin    = _isLogin;

@synthesize GB_JsonCitys    = _GB_JsonCitys;

//@synthesize GB_CompanyModel = _GB_CompanyModel;

//set get companymodel
//- (void)setGB_CompanyModel:(ModelCompany *)GB_CompanyModel{
//    [GlobalMethod writeStr:GB_CompanyModel != nil?[GlobalMethod exchangeModel:GB_CompanyModel]:@"" forKey:LOCAL_COMPANYMODEL];
//    _GB_CompanyModel = GB_CompanyModel;
//}
//
//- (ModelCompany *)GB_CompanyModel{
//    if (!_GB_CompanyModel) {
//        NSDictionary * dicItem = [GlobalMethod exchangeStringToDic:[GlobalMethod readStrFromUser:LOCAL_COMPANYMODEL]];
//        _GB_CompanyModel = [ModelCompany modelObjectWithDictionary:dicItem];
//    }
//    return _GB_CompanyModel;
//}
//


//set get userModel
- (void)setGB_UserModel:(BaseClass *)GB_UserModel{
    [GlobalMethod writeStr:GB_UserModel != nil?[GlobalMethod exchangeModel:GB_UserModel]:@"" forKey:LOCAL_USERMODEL];
    _GB_UserModel = GB_UserModel;
}
- (BaseClass *)GB_UserModel{
    if (!_GB_UserModel) {
        NSDictionary * dicItem = [GlobalMethod exchangeStringToDic:[GlobalMethod readStrFromUser:LOCAL_USERMODEL]];
        _GB_UserModel = [BaseClass modelObjectWithDictionary:dicItem];
    }
    return _GB_UserModel;
}
//pickerModel
-(void)setGB_PickerModel:(PickerViewModel *)GB_PickerModel
{
    [GlobalMethod writeStr:GB_PickerModel != nil?[GlobalMethod exchangeModel:GB_PickerModel]:@"" forKey:LOCAL_USERMODEL];
    _GB_PickerModel = GB_PickerModel;
}

-(PickerViewModel *)GB_PickerModel
{
    if (!_GB_UserModel) {
        NSDictionary * dicItem = [GlobalMethod exchangeStringToDic:[GlobalMethod readStrFromUser:LOCAL_PICKER_MODEL]];
        _GB_PickerModel = [[PickerViewModel alloc]init];
    }
    return _GB_PickerModel;
}

//set get key
- (void)setGB_Key:(NSString *)GB_Key{
    [GlobalMethod writeStr:GB_Key!=nil?GB_Key:@"" forKey:LOCAL_KEY];
    _GB_Key = GB_Key;
}
- (NSString*)GB_Key{
    if (!_GB_Key){
        _GB_Key = [GlobalMethod readStrFromUser:LOCAL_KEY];
    }
    return _GB_Key;
}

-(void)setGB_UID:(NSString *)GB_UID{
    [GlobalMethod writeStr:GB_UID!=nil?GB_UID:@"" forKey:LOCAL_UID];
    _GB_UID = GB_UID;
}
- (NSString*)GB_UID{
    if (!_GB_UID){
        _GB_UID = [GlobalMethod readStrFromUser:LOCAL_UID];
    }
    return _GB_UID;
}

-(void)setGB_Phone:(NSString *)GB_Phone{
    [GlobalMethod writeStr:GB_Phone!=nil?GB_Phone:@"" forKey:LOCAL_Phone];
    _GB_Phone = GB_Phone;
}
- (NSString*)GB_Phone{
    if (!_GB_Phone){
        _GB_Phone = [GlobalMethod readStrFromUser:LOCAL_Phone];
    }
    return _GB_Phone;
}

-(void)setIsLogin:(NSString *)isLogin
{
    [GlobalMethod writeStr:isLogin!=nil ? isLogin : @"" forKey:LOCAL_IsLogin];
    _isLogin = isLogin;
}

-(NSString *)isLogin
{
    if (!_isLogin) {
        _isLogin = [GlobalMethod readStrFromUser:LOCAL_IsLogin];
    }
    return _isLogin;
}

-(void)setGB_JsonCitys:(NSString *)GB_JsonCitys
{
    [GlobalMethod writeStr:GB_JsonCitys!=nil ? GB_JsonCitys : @"" forKey:LOCAL_GB_JsonCitys];
    _isLogin = GB_JsonCitys;
}

-(NSString *)GB_JsonCitys
{
    if (!_GB_JsonCitys) {
        _GB_JsonCitys = [GlobalMethod readStrFromUser:LOCAL_GB_JsonCitys];
    }
    return _GB_JsonCitys;
}


-(void)setGB_DeviceToken:(NSString *)GB_DeviceToken
{
    [GlobalMethod writeStr:GB_DeviceToken!=nil ? GB_DeviceToken : @"" forKey:LOCAL_DEVICETOKEN];
    _GB_DeviceToken = GB_DeviceToken;
}

-(NSString *)GB_DeviceToken
{
    if (!_GB_DeviceToken) {
        _GB_DeviceToken = [GlobalMethod readStrFromUser:LOCAL_DEVICETOKEN];
    }
    return _GB_DeviceToken;
}

-(void)setGB_PostNews:(NSString *)GB_PostNews
{
    [GlobalMethod writeStr:GB_PostNews!=nil ? GB_PostNews : @"" forKey:LOCAL_POSTNEWS];
    _GB_PostNews = GB_PostNews;
}

-(NSString *)GB_PostNews
{
    if (!_GB_PostNews) {
        _GB_PostNews = [GlobalMethod readStrFromUser:LOCAL_POSTNEWS];
    }
    return _GB_PostNews;
}

//set Notice View
//- (NoticeView *)GB_NoticeView{
//    if (_GB_NoticeView == nil) {
//        _GB_NoticeView = [NoticeView new];
//    }
//    return _GB_NoticeView;
//}

//实现单例
SYNTHESIZE_SINGLETONE_FOR_CLASS(GlobalData);







@end
