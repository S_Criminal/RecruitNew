//
//  GeneralPickerView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomePickerView;
@protocol GeneralPickerViewDelegate <NSObject>

@optional
//取消回调
- (void)protocolPickerViewBtnCancleSelect:(NSUInteger)tag;
//确定回调
- (void)protocolPickerViewBtnConfirmSelect:(NSUInteger)tag content:(NSString *)content;

@end

@interface GeneralPickerView : UIView<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UIButton *leftButton;
@property (nonatomic ,strong) UIButton *rightButton;

@property (nonatomic ,assign) NSInteger indexTag;   //tag标识
@property (nonatomic ,assign) NSInteger statusTag;  //判断是否是时间选择器  2为时间选择器  3城市数组  
@property (nonatomic ,strong) NSString *startTime;  //开始时间
@property (nonatomic ,strong) NSString *endTime;  //开始时间

@property (nonatomic ,strong) NSString *selectFirstStr;
@property (nonatomic ,strong) NSString *selectSecondStr;
@property (nonatomic ,strong) NSString *selectContentStr;

@property (nonatomic ,assign) NSInteger compont;        //判断几列
@property (nonatomic ,strong) NSArray *firstArray;      //第一列元素
@property (nonatomic ,strong) NSArray *secondArray;     //第二列元素
@property (nonatomic ,assign) NSInteger selectFirstIndex;    //默认选中的第几行
@property (nonatomic ,assign) NSInteger selectSecondIndex;    //默认选中的第几行
@property (nonatomic ,strong) UIPickerView *pickerView;

@property (nonatomic ,weak)id <GeneralPickerViewDelegate> delegate;




-(void)setUpPickerViewTitle:(NSString *)title selectFirstTitle:(NSString *)selectTitle selectSecondTitle:(NSString *)selectSecondTitle selectModel:(NSInteger)statusIndex firstArray:(NSArray *)firstArray secondArray:(NSArray *)secondArray;

@end

//pickerView选择器
//@interface CustomePickerView : UIPickerView
//
//-(instancetype)initTitle:(NSString *)title frame:(CGRect)frame pickOrDate:(BOOL)pickerDate modelFirstArray:(NSArray *)firstArr SecondArr:(NSArray *)secondArr;
//
//
//@end

//pickerView时间选择器
@interface CustomeDatePickerView : UIDatePicker

-(instancetype)initTitle:(NSString *)title frame:(CGRect)frame pickOrDate:(BOOL)pickerDate modelFirstArray:(NSArray *)firstArr SecondArr:(NSArray *)secondArr;


@end
