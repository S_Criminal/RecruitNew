//
//  GeneralPickerView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/5.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "GeneralPickerView.h"

#import "BottomLabelView.h"

#import "GlobalDateModel.h"
#import "CityShortName.h"

@implementation GeneralPickerView{
    NSInteger cityRow;
    CGFloat titleFont;
    CGFloat viewLeftConstraint;
    CGFloat pickerViewHeight;
    CGFloat titleHeight;
}


-(UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH - viewLeftConstraint * 2, titleHeight)];
        _titleLabel.font = [UIFont systemFontOfSize:F(titleFont)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = COLOR_MAINCOLOR;
        _titleLabel.textColor = [UIColor whiteColor];
    }
    return _titleLabel;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = COLOR_MAINCOLOR;
        [_bgView setCorner:5];
    }
    return _bgView;
}

-(UIButton *)leftButton
{
    if (!_leftButton) {
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftButton.backgroundColor = COLOR_MAINCOLOR;
        _leftButton.titleLabel.font = [UIFont systemFontOfSize:titleFont];
        [_leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [_leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_leftButton addTarget:self action:@selector(tapCancle:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftButton;
}

-(UIButton *)rightButton
{
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.backgroundColor = COLOR_MAINCOLOR;
        _rightButton.titleLabel.font = [UIFont systemFontOfSize:titleFont];
        [_rightButton setTitle:@"确定" forState:UIControlStateNormal];
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightButton addTarget:self action:@selector(tapConfirm:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        self.compont = 1;
        [self setUI];
        [self.bgView addSubview:self.titleLabel];
        
        [self setUpPickerView];
        
        [self builtConstraint];
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHidden)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

-(void)tapHidden
{
    _bgView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        _bgView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
        self.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)setUpPickerView
{
    _pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_titleLabel.frame), _titleLabel.width, pickerViewHeight)];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    _pickerView.backgroundColor = [UIColor whiteColor];
    [self.bgView addSubview:_pickerView];
    
    
}
/**
 *  statusIndex     @1 pickerView选择器  @2时间选择器  @3城市数组  @4
 *
 *
 */
-(void)setUpPickerViewTitle:(NSString *)title selectFirstTitle:(NSString *)selectTitle selectSecondTitle:(NSString *)selectSecondTitle selectModel:(NSInteger)statusIndex firstArray:(NSArray *)firstArray secondArray:(NSArray *)secondArray
{
    if (kArrayIsEmpty(firstArray)) {
        return;
    }
    
    self.firstArray = firstArray;
    self.secondArray = secondArray;
    if (_pickerView) {
        [_pickerView removeFromSuperview];
        _pickerView = nil;
    }
    [self setUpPickerView];
    //设置title
//    [self setLabelTitle:statusIndex];
    self.titleLabel.text = title;
   
    self.statusTag = statusIndex;
    
    //是否有第二个数组，如果有就显示两列,否则就一列
    
    if (kArrayIsEmpty(secondArray)) {
        self.compont = 1;
        NSInteger selectIndex = [self rowSelectIndex:selectTitle array:firstArray component:0];
        [self.pickerView selectRow:selectIndex inComponent:0 animated:YES];
    }else{
        self.compont = 2;
        if (statusIndex == 2) {
            if (selectTitle.length == 4)
            {
                selectTitle = [selectTitle stringByAppendingString:@"年"];
                selectSecondTitle = [selectSecondTitle stringByAppendingString:@"月"];
            }
        }
        
        
        if (statusIndex == 3) {
            NSInteger firstSelect = [self rowSelectIndex:selectTitle array:firstArray component:0];
            self.secondArray = self.statusTag == 3 ?  self.firstArray[firstSelect][@"Cities"] : secondArray;
            
            [self.pickerView reloadAllComponents];
            
            [self.pickerView selectRow:firstSelect inComponent:0 animated:YES];
            [self.pickerView selectRow:[self rowSelectIndex:selectSecondTitle array:_secondArray component:1] inComponent:1 animated:YES];
        }else{
            [self.pickerView selectRow:[self rowSelectIndex:selectTitle array:firstArray component:0] inComponent:0 animated:YES];
            [self.pickerView selectRow:[self rowSelectIndex:selectSecondTitle array:secondArray component:1] inComponent:1 animated:YES];
        }
    }

    
    if (kStringIsEmpty(selectTitle)) {
        _selectFirstStr = firstArray[0];
    }else{
        _selectFirstStr = selectTitle;
    }
}

//元素在数组里的第几个
-(NSInteger)rowSelectIndex:(NSString *)content array:(NSArray *)array component:(NSInteger)compoent
{
    NSInteger index = 0;
    if (kStringIsEmpty(content)) {
        return index;
    }
    NSString *arrayStr;
    
    NSLog(@"%ld",[_pickerView selectedRowInComponent:0]);
    NSLog(@"%@",self.firstArray);
    
    
    
    for (int i = 0; i < array.count; i ++ )
    {
        if (compoent == 0) {
            arrayStr = self.statusTag == 3 ? array[i][@"State"] : array[i];
        }else if (compoent == 1){
            /** 城市数组要先确定省份数组第几个 */
            NSLog(@"%@",array);
            
            arrayStr = [CityShortName getAdressWithProvience:@"" City:self.statusTag == 3 ? array[i][@"city"] : array[i]];

        }
        
        if ([content isEqualToString:arrayStr])
        {
            index = i;
        }
    }
    
    return index;
}

-(void)setIndexTag:(NSInteger)indexTag
{
    _indexTag = indexTag;
}

-(void)setStartTime:(NSString *)startTime
{
    _startTime = startTime;
    
}

-(void)setEndTime:(NSString *)endTime
{
    _endTime = endTime;
}

-(void)setUI
{
    viewLeftConstraint = W(20);
    titleFont = F(16);
    pickerViewHeight = W(150);
    titleHeight = W(50);
}

-(void)builtConstraint
{
    [self.bgView setFrame:CGRectMake(viewLeftConstraint, (KHEIGHT - pickerViewHeight) / 2 - titleHeight, KWIDTH - viewLeftConstraint * 2, titleHeight * 2 + pickerViewHeight)];
    [self.leftButton setFrame:CGRectMake(0, self.bgView.height - titleHeight, self.bgView.width / 2, titleHeight)];
    [self.rightButton setFrame:CGRectMake(self.bgView.width / 2, self.bgView.height - titleHeight, self.bgView.width / 2, titleHeight)];
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.leftButton];
    [self.bgView addSubview:self.rightButton];
    [self.bgView addSubview:[GlobalMethod addVerticalLineFrame:CGRectMake(self.leftButton.right - 0.5, self.leftButton.y  + W(4), 1, titleHeight - W(8)) color:[UIColor whiteColor]]];
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.compont;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component == 0) {
        return _firstArray.count;
    }else if (component == 1){
        return _secondArray.count;
    }
    return 10;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return @"row";
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f,0.0f,[pickerView rowSizeForComponent:component].width,[pickerView rowSizeForComponent:component].height)];
    
    NSString *title = @"row";
    
    if (component == 0) {
        
        if (self.statusTag == 3) {
            self.selectFirstStr = self.firstArray[0][@"State"];
            title = self.firstArray[row][@"State"];
        }else{
            self.selectFirstStr = kStringIsEmpty(self.selectFirstStr) ? self.firstArray[0] : self.selectFirstStr;
            title = self.firstArray[row];
        }
    }else if (component == 1){
        if (self.statusTag == 3) {
            self.selectSecondStr = self.secondArray[row][@"State"];
            title = self.secondArray[row][@"city"];
        }else{
            self.selectSecondStr = self.secondArray[0];
            title = self.secondArray[row];
        }
    }
    if (self.statusTag == 3)
    {
        title = [CityShortName getAdressWithProvience:@"" City:title];
    }
    
    label.text = title;
    
    label.font = [UIFont systemFontOfSize:titleFont - 1];
    
    [label setTextAlignment:NSTextAlignmentCenter];
    
    return label;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    /**
     *  如果是时间选择器
     */
    if (self.statusTag == 2)
    {
        if (component == 0)
        {
            //结束时间不能小于开始时间
            NSString *start;
            NSString *end;
            NSString *error;
            
            if (_startTime.length > 4)
            {
                error = @"结束时间不能小于开始时间";
                start = [_startTime substringToIndex:4];
                end = [self.firstArray[row] substringToIndex:4];
            }else if (_endTime.length > 4){
                error = @"开始时间不能大于结束时间";
                end = [_endTime substringToIndex:4];
                start = [self.firstArray[row] substringToIndex:4];
            }
//            if ([start integerValue] > [end integerValue])
//            {
//                [MBProgressHUD showError:@"结束时间不能小于开始时间" toView:self];
//                [_pickerView selectRow:[self rowSelectIndex:[start stringByAppendingString:@"年"] array:self.firstArray component:0] inComponent:0 animated:YES];
//                return;
//            }
        }else if (component == 1){
            
            self.selectSecondStr = self.secondArray[row];
        }
    }
    /**
     *  如果是城市数组
     */
    else if (self.statusTag == 3){
        NSString *proviences;
        NSString *city;
        switch (component) {
            case 0:// 选中省份表盘的某一行
                
                cityRow = row;
                self.secondArray = self.firstArray[row][@"Cities"];
                
                [pickerView reloadComponent:1];
                [pickerView selectRow:0 inComponent:1 animated:YES];
                
                city = self.secondArray[0][@"city"];
                proviences = [NSString stringWithFormat:@"%@",self.firstArray[row][@"State"]];
                
                self.selectFirstStr = proviences;
                self.selectSecondStr= city;
                NSLog(@"_selectSecondStr%@",_selectSecondStr);
                
                break;
                
            case 1:// 选中城市表盘的某一行
                
                city = self.secondArray[row][@"city"];
                
                NSLog(@"%@",self.firstArray[cityRow][@"State"]);
                proviences = [NSString stringWithFormat:@"%@",self.firstArray[cityRow][@"State"]];
                
                self.selectFirstStr  = proviences;
                self.selectSecondStr = city;
                break;
                
            default:
                break;
        }
    }else{
        if (component == 0){
            self.selectFirstStr = self.firstArray[row];
        }else if (component == 1){
            self.selectSecondStr = self.secondArray[row];
        }
    }
}

-(void)tapCancle:(UIButton *)sender
{
    //代理回调
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolPickerViewBtnCancleSelect:)]) {
        [self.delegate protocolPickerViewBtnCancleSelect:self.indexTag];
    }
}

-(void)tapConfirm:(UIButton *)sender
{
    NSString *content = [self getContent];
    //代理回调
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolPickerViewBtnConfirmSelect:content:)]) {
        [self.delegate protocolPickerViewBtnConfirmSelect:self.indexTag content:content];
    }
}

-(NSString *)getContent{
    
    if (kArrayIsEmpty(_secondArray))
    {
        return _selectFirstStr;
    }
    
    NSInteger row1 = [_pickerView selectedRowInComponent:0];
    NSInteger row2 = [_pickerView selectedRowInComponent:1];
    
    if (self.statusTag == 3)
    {
        _selectFirstStr = self.firstArray[row1][@"State"];
        _selectSecondStr= self.secondArray[row2][@"city"];
    }else{
        _selectFirstStr = self.firstArray[row1];
        _selectSecondStr= self.secondArray[row2];
    }
    
    if ([_selectFirstStr isEqualToString:_selectSecondStr]) {
        return _selectFirstStr;
    }
    _selectContentStr = [NSString stringWithFormat:@"%@-%@",self.selectFirstStr,self.selectSecondStr];
    
    return _selectContentStr;
}

@end

//@implementation GeneralPickerView
//
//-(instancetype)initTitle:(NSString *)title frame:(CGRect)frame pickOrDate:(BOOL)pickerDate modelFirstArray:(NSArray *)firstArr SecondArr:(NSArray *)secondArr
//{
//    self = [super initWithFrame:frame];
//    if (!self) {
//        
//    }
//    return self;
//}
//
//@end
