//
//  GlobalMethod.h
//  米兰港
//
//  Created by 隋林栋 on 15/3/3.
//  Copyright (c) 2015年 Sl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class NSDictionary;

@interface GlobalMethod : NSObject



//解析错误信息
+ (NSString *)returnErrorMessage:(id)error;

//去掉空格
+ (NSString *)exchangeEmpty:(NSString *)str;

//去掉首尾的换行
+(NSString *)exChangeEmptyFeedLine:(NSString *)str;

//处理小数点
+ (NSString *)exchangeNum:(double)num;

//转换为jsong
+ (NSString *)exchangeToJson:(id)object;

//判断是否侧滑
+ (BOOL)canLeftSlide;

//转换json
+ (NSString *)exchangeModel:(id)model;

//转换String to dic
+ (NSDictionary *)exchangeStringToDic:(NSString *)str;
//转换dic to ary
+ (NSMutableArray *)exchangeDic:(id)response toAryWithModelName:(NSString *)modelName;
//十六进制转颜色
+ (UIColor *)exchangeColorWith16:(NSString*)hexColor;

//移除全部子视图
+ (void)removeView:(UIView *)view ;
+ (void)removeAllSubViews:(UIView *)view ;
+ (void)removeAllSubView:(UIView *)view withTag:(NSInteger)tag;
//隐藏显示tag视图
+ (void)showHideViewWithTag:(int)tag inView:(UIView *)view isshow:(BOOL)isShow;

//显示提示
+ (void)showAlert:(NSString *)strAlert;

//获取版本号
+ (NSString *)getVersion;
+ (NSString *)getBuildVersion;

//适配label字号
+ (CGFloat)adaptLabelFont:(CGFloat)fontNum;
/**
 *  double转string
 */
+ (NSString *)doubleToString:(double)dou;
/** 获取出生年月 */
+ (NSString *)getBirth;



#pragma mark 角标清零
+ (void)zeroIcon;

#pragma mark 根据推送的消息进行跳转
+ (void)jumpWithPushJson;

#pragma mark 存储本地数据
//往plist存入数据
+ (void)whiteArrayToPlist:(NSMutableArray *)array filePath:(NSString *)filePath;
//从plist取出数据
+ (NSMutableArray *)getArrayToPlist:(NSMutableArray *)array filePath:(NSString *)filePath;
+ (void)writeStr:(NSString *)strValue forKey:(NSString *)strKey;

+ (NSString *)readStrFromUser:(NSString *)strKey;

+ (void)writeDate:(NSDate *)date  forKey:(NSString *)strKey;

+ (double)readDoubleFromUser:(NSString *)strKey;

+ (void)writeDouble:(double)numDouble forKey:(NSString *)strKey;

//写入数据库
+ (void)writeBool:(BOOL)bol local:(NSString *)noti;

//设置日期格式
+ (NSString *)exchangeDate:(NSDate *)date formatter:(NSString *)formate;
+ (NSDate *)exchangeString:(NSString *)str formatter:(NSString *)formate;
//获取时间戳
+ (NSString *)fetchTimeStamp;

/**
 *  处理图片大小
 */
+ (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize;
//处理label
+(UILabel *)setAttributeLabel:(UILabel *)label content:(NSString *)content width:(CGFloat)width;

+(NSAttributedString *)getHtmlAttributeString:(NSString *)htmlStr;

+ (BOOL)readBoolLocal:(NSString *)noti;

+ (NSDate *)readDateFromUser:(NSString *)strKey;

+ (void)writeDataToUser:(NSData *)data forKey:(NSString *)strKey;

+ (NSData *)readDataFromUser:(NSString *)strKey;

+(NSString *)removeHTML:(NSString *)html;

#pragma mark 拼写文件名
+ (NSString *)fetchDoumentPath:(NSString *)name;

#pragma mark 弹出提示
+ (void)showNotiInStatusBar:(NSString *)strMsg bageNum:(int)msgCount;

#pragma mark 跳转页面
//跳转店铺
+ (void)jumpToShopDetail:(double )sid;

#pragma mark 判断推送状态
+ (BOOL)IsEnablePush;

#pragma mark 判断网络状态
+ (BOOL)IsEnableNetwork;
+ (BOOL)IsEnableWifi;
+ (BOOL)IsENable3G;

#pragma mark 获取文件大小
+ (NSString *)fetchDoumentSize;

#pragma mark encode  decode
+(NSString*)encodeString:(NSString*)unencodedString;
+(NSString*)decodeString:(NSString*)encodedString;

#pragma mark 清空缓存

+(void)clearAllDates;

@end
