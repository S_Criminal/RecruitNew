//
//  GlobalMethods+UI.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "GlobalMethods+UI.h"

@implementation GlobalMethods (UI)

//设置label
+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
         aligent:(NSTextAlignment )aligent
            text:(NSString *)text
         bgColor:(UIColor *)color{
    label.text = text;
    label.numberOfLines = numLines;
    label.font = [UIFont systemFontOfSize:fontNum];
    label.textColor = textColor;
    label.textAlignment = aligent;
    label.backgroundColor = color;
    CGFloat widthMAX = [self fetchWidthFromLabel:label];
    label.height = [self fetchHeightFromLabel:label];
    if (widthLimit != 0 && widthMAX > widthLimit) {
        label.width = widthLimit;
        label.height = [self fetchHeightFromLabel:label];
    } else {
        label.width = widthMAX;
    }
}

+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
            text:(NSString *)text{
    [self setLabel:label widthLimit:widthLimit numLines:numLines fontNum:fontNum textColor:textColor aligent:NSTextAlignmentLeft text:text bgColor:[UIColor clearColor]];
}

+ (void)resetLabel:(UILabel *)label
              text:(NSString *)text
      isWidthLimit:(BOOL )isWidthLimit
{
    if (isWidthLimit) {
        [self setLabel:label widthLimit:label.width numLines:label.numberOfLines fontNum:label.font.pointSize textColor:label.textColor aligent:label.textAlignment text:text bgColor:label.backgroundColor];
    }else {
        [self setLabel:label widthLimit:0 numLines:label.numberOfLines fontNum:label.font.pointSize textColor:label.textColor aligent:label.textAlignment text:text bgColor:label.backgroundColor];
    }
}


#pragma mark 计算宽度
+ (CGFloat)fetchWidthFromLabel:(UILabel *)label{
    NSString * strContent = label.text;
    UIFont * font = label.font;
    NSAttributedString * attributeString = [[NSAttributedString alloc]initWithString:strContent attributes:@{NSFontAttributeName: font}];
    CGRect rect =[attributeString boundingRectWithSize:CGSizeMake(1000, 1000)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return ceil(rect.size.width);
}
#pragma mark 计算高度
+ (CGFloat)fetchHeightFromLabel:(UILabel *)label{
    return [self fetchHeightFromLabel:label heightLimit:10000];
}

+ (CGFloat)fetchHeightFromLabel:(UILabel *)label heightLimit:(CGFloat )height{
    if (label == nil||label.text==nil) {
        return 0;
    }
    NSString * strContent = label.text;
    UIFont * font = label.font;
    NSAttributedString * attributeString = [[NSAttributedString alloc]initWithString:strContent attributes:@{NSFontAttributeName: font}];
    CGRect rect =[attributeString boundingRectWithSize:CGSizeMake(label.width, height)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGFloat num_height_return = rect.size.height+1;
    //限制行数
    if (label.numberOfLines != 0) {
        attributeString = [[NSAttributedString alloc]initWithString:@"A" attributes:@{NSFontAttributeName: font}];
        rect =[attributeString boundingRectWithSize:CGSizeMake(label.width, height)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        num_height_return = num_height_return >label.numberOfLines*rect.size.height?label.numberOfLines*rect.size.height:num_height_return;
    }
    return ceil(num_height_return) ;
}



@end
