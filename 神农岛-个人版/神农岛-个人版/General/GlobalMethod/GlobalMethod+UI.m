//
//  GlobalMethod+UI.m
//  乐销
//
//  Created by 隋林栋 on 2016/12/16.
//  Copyright © 2016年 ping. All rights reserved.
//

#import "GlobalMethod+UI.h"

@implementation GlobalMethod (UI)

#pragma mark 计算label size
+ (CGFloat)fetchHeightFromLabel:(UILabel *)label{
    return [self fetchHeightFromLabel:label heightLimit:10000];
}

+ (CGFloat)fetchHeightFromLabel:(UILabel *)label heightLimit:(CGFloat )height{
    if (label == nil||label.text==nil) {
        return 0;
    }
    NSString * strContent = label.text;
    UIFont * font = label.font;
    NSAttributedString * attributeString = [[NSAttributedString alloc]initWithString:strContent attributes:@{NSFontAttributeName: font}];
    CGRect rect =[attributeString boundingRectWithSize:CGSizeMake(label.width, height)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGFloat num_height_return = rect.size.height+1;
    //限制行数
    if (label.numberOfLines != 0) {
        attributeString = [[NSAttributedString alloc]initWithString:@"A" attributes:@{NSFontAttributeName: font}];
        rect =[attributeString boundingRectWithSize:CGSizeMake(label.width, height)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        num_height_return = num_height_return >label.numberOfLines*rect.size.height?label.numberOfLines*rect.size.height:num_height_return;
    }
    return ceil(num_height_return) ;
}

+ (CGFloat)fetchHeightFromString:(NSString *)string isLimitWidth:(CGFloat)limitWidth  font:(UIFont *)font
{
    NSAttributedString * attributeString = [[NSAttributedString alloc]initWithString:string attributes:@{NSFontAttributeName:font}];
    CGRect rect =[attributeString boundingRectWithSize:CGSizeMake(limitWidth, 1000)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGFloat num_height_return = rect.size.height+1;
    return num_height_return;
}

+ (CGFloat)fetchWidthFromLabel:(UILabel *)label{
    if (label.text.length == 0) {
        return 0;
    }
    NSString * strContent = label.text;
    UIFont * font = label.font;
    NSAttributedString * attributeString = [[NSAttributedString alloc]initWithString:strContent attributes:@{NSFontAttributeName: font}];
    CGRect rect =[attributeString boundingRectWithSize:CGSizeMake(1000, 1000)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return ceil(rect.size.width);
}

//设置label
+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
         aligent:(NSTextAlignment )aligent
            text:(NSString *)text
         bgColor:(UIColor *)color{
    label.text = text;
    label.numberOfLines = numLines;
    label.font = [UIFont systemFontOfSize:fontNum];
    label.textColor = textColor;
    label.textAlignment = aligent;
    label.backgroundColor = color;
    CGFloat widthMAX= [self fetchWidthFromLabel:label];
    if (widthLimit != 0 ) {
        if (widthMAX < widthLimit) {
            label.width = widthMAX;
        }else {
            label.width = widthLimit;
        }
    }else {
        label.width = widthMAX;
    }
    label.height = [self fetchHeightFromLabel:label];
}

+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
            text:(NSString *)text{
    [self setLabel:label widthLimit:widthLimit numLines:numLines fontNum:fontNum textColor:textColor aligent:NSTextAlignmentLeft text:text bgColor:[UIColor clearColor]];
}

+ (void)resetLabel:(UILabel *)label
            text:(NSString *)text
    isWidthLimit:(BOOL )isWidthLimit
{
    if (isWidthLimit) {
        [self setLabel:label widthLimit:label.width numLines:label.numberOfLines fontNum:label.font.pointSize textColor:label.textColor aligent:label.textAlignment text:text bgColor:label.backgroundColor];
    }else {
        [self setLabel:label widthLimit:0 numLines:label.numberOfLines fontNum:label.font.pointSize textColor:label.textColor aligent:label.textAlignment text:text bgColor:label.backgroundColor];
    }
}

//设置圆角
+ (void)setRoundView:(UIView *)iv color:(UIColor *)color
{
    [self setRoundView:iv color:color numRound:4 width:4];
}

//设置圆角
+ (void)setRoundView:(UIView *)iv color:(UIColor *)color numRound:(int)numRound width:(CGFloat)width
{
    iv.layer.cornerRadius = numRound;//圆角设置
    iv.layer.masksToBounds = YES;
    [iv.layer setBorderWidth:width];
    iv.layer.borderColor = color.CGColor;
}

//设置textfield左间距
+ (void)setTextFileLeftPadding:(UITextField *)ut leftPadding:(float)leftPadding{
    CGRect frame = ut.frame;
    frame.size.width = leftPadding;
    UIView *leftV = [[UIView alloc] initWithFrame:frame];
    ut.leftViewMode = UITextFieldViewModeAlways;
    ut.leftView = leftV;
}

//添加竖线
+ (void)addLineFrame:(CGRect)rect inView:(UIView *)view isDark:(BOOL)isDark{
    UIView * viewLine = [UIView new];
    viewLine.frame = rect;
    viewLine.backgroundColor = COLOR_LINESCOLOR;
    [view addSubview:viewLine];
}

+(UIView *)addNavLine
{
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight, KWIDTH, 1)];
    line.backgroundColor = COLOR_SEPARATE_COLOR;
    return line;
}

+ (UIView *)addNavLineFrame:(CGRect)frame
{
    UIView *line = [[UIView alloc]initWithFrame:frame];
    line.backgroundColor = COLOR_LINESCOLOR;
    return line;
}

+ (UIView *)addVerticalLineFrame:(CGRect)frame color:(UIColor *)color
{
    UIView *line = [[UIView alloc]initWithFrame:frame];
    line.backgroundColor = color;
    return line;
}

+ (UIView *)backgroundNoDateViewFrame:(CGRect)frame
{
    UIView *bgView = [[UIView alloc]initWithFrame:frame];
    bgView.tag = 1100;
    bgView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 64 + 60, 273, 123)];
    imageView.image = [UIImage imageNamed:@"无数据"];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(imageView.frame) + 20, KWIDTH - 40, 20)];
    label.text = @"暂无记录";
    label.font = [UIFont systemFontOfSize:F(18)];
    label.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:imageView];
    [bgView addSubview:label];
    
    return bgView;
}

@end
