//
//  GlobalMethods+UI.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "GlobalMethods.h"

@interface GlobalMethods (UI)
//计算高度 宽度
+ (CGFloat)fetchHeightFromLabel:(UILabel *)label;
+ (CGFloat)fetchHeightFromLabel:(UILabel *)label heightLimit:(CGFloat )height;
+ (CGFloat)fetchWidthFromLabel:(UILabel *)label;

////设置label
+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
         aligent:(NSTextAlignment )aligent
            text:(NSString *)text
         bgColor:(UIColor *)color;


+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
            text:(NSString *)text;


+ (void)resetLabel:(UILabel *)label
              text:(NSString *)text
      isWidthLimit:(BOOL )isWidthLimit;

////设置圆角
//+ (void)setRoundView:(UIView *)iv color:(UIColor *)color;
//+ (void)setRoundView:(UIView *)iv color:(UIColor *)color numRound:(int)numRound width:(int)width;

////textfield添加左边距
//+ (void)setTextFileLeftPadding:(UITextField *)ut leftPadding:(float)leftPadding;
//
////添加竖线
//+ (void)addLineFrame:(CGRect)rect inView:(UIView *)view isDark:(BOOL)isDark;

@end
