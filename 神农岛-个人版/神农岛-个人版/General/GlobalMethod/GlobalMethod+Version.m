//
//  GlobalMethod+Version.m
//  乐销
//
//  Created by 隋林栋 on 2016/12/15.
//  Copyright © 2016年 ping. All rights reserved.
//

#import "GlobalMethod+Version.h"
//登陆vc
#import "LoginViewController.h"
//tabvc
#import "CustomTabBarController.h"
//window
#import "AppDelegate.h"
//baseNav
#import "BaseNavController.h"


@implementation GlobalMethod (Version)


//注销
+ (void)logoutSuccess{
    [self clearUserInfo];
    //重置根视图
    [self createRootNav];
}

//清除全局数据
+ (void)clearUserInfo{
    GlobalData * gbData = [GlobalData sharedInstance];
//    gbData.GB_UserModel = nil;
    gbData.GB_Key = nil;
//    gbData.GB_CompanyModel = nil;
}


//判断是否登陆
+(BOOL)isLoginSuccess{
    NSLog(@"%@",[GlobalData sharedInstance].GB_Key);
    NSString *key = [GlobalData sharedInstance].GB_Key;
    return kStringIsEmpty(key);
}

+ (void)loginWithBlock:(void (^)(void))block {
    if ([self isLoginSuccess]) {
        block();
    }else{
        [GB_Nav pushViewController:[LoginViewController new] animated:true];
        //        LoginView * loginView = [[[NSBundle mainBundle]loadNibNamed:@"LoginView" owner:self options:nil]lastObject];
        //        loginView.blockLoginSuccess = block;
        //        loginView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
        //        [GB_Nav.view addSubview:loginView];
        //        loginView.blockLoginSuccess();
    }
}

//创建rootNav
+ (void)createRootNav{
    BaseNavController * navMain = [[BaseNavController alloc]initWithRootViewController:[CustomTabBarController new]];
    navMain.navigationBarHidden = YES;
    GB_Nav = navMain;
    AppDelegate * delegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    delegate.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    delegate.window.rootViewController = GB_Nav;
    [delegate.window setBackgroundColor:[UIColor whiteColor]];
    [delegate.window makeKeyAndVisible];
    
}

//跳转页面
+ (void)jumpToVC:(NSString *)strClassName anminated:(BOOL)animated{
    id vc = [[NSClassFromString(strClassName) alloc]init];
    [GB_Nav pushViewController:vc animated:animated];
    
}



@end
