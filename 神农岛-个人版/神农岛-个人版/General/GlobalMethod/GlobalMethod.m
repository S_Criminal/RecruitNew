//
//  GlobalMethod.m
//  米兰港
//
//  Created by 隋林栋 on 15/3/3.
//  Copyright (c) 2015年 Sl. All rights reserved.
//

#import "GlobalMethod.h"
//数学
#import <math.h>
//提示框
//#import "NoticeView.h"
//网络请求
//#import "AFNetworkReachabilityManager.h"

#import "BriefInfoModel.h"

#import "DateArrModel.h"

#import "LoginViewController.h"

#import "CustomHTMLTransformString.h"   //string 转HTML

@implementation GlobalMethod

+(void)clearAllDates
{
   GlobalData *date = [GlobalData sharedInstance];
    date.GB_UserModel = nil;
    date.position     = nil;
    date.historyArray = nil;
    date.city         = nil;
    date.isLogin      = nil;
    [[SDImageCache sharedImageCache] clearDisk];
    [GlobalData sharedInstance].GB_PostNews = nil;
    [GlobalMethod zeroIcon];
}

#pragma mark //解析错误信息
+ (NSString *)returnErrorMessage:(id)error{
    if (error == nil) {
        return @"错误为空";
    }
    if ([error isKindOfClass:[NSString class]]) {
        NSString * strError = (NSString *)error;
        if (strError.length >0) {
            return strError;
        }
        return @"错误为空";
    }
    if ([error isKindOfClass:[NSDictionary class]]) {
        NSString * strError = [error objectForKey:@"Message"];
        return  strError;
    }
    return  @"错误为空";
}


#pragma mark 角标清零
+ (void)zeroIcon{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
}

#pragma mark 读存本地数据
//写入数据库
+ (void)writeBool:(BOOL)bol local:(NSString *)noti{
    if (noti == nil || noti.length==0) {
        return;
    }
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:[NSNumber numberWithBool:bol] forKey:noti];
    [user synchronize];
    
}

+ (BOOL)readBoolLocal:(NSString *)noti{
    if (noti == nil || noti.length==0) {
        return NO;
    }
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    NSNumber *num = [user objectForKey:noti];
    if (num == nil) return NO;
    return [num boolValue];
}

//处理小数点
+ (NSString *)exchangeNum:(double)num{
    NSString * strReturn =[NSString stringWithFormat:@"%.02f",round(num*100)/100 ];
    return strReturn;
}

//去掉空格
+ (NSString *)exchangeEmpty:(NSString *)str{
    
    if (str == nil || ![str isKindOfClass:[NSString class]]) {
        return @"";
    }
    NSString * strReturn = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    return strReturn;
}
//去掉首尾的换行
+(NSString *)exChangeEmptyFeedLine:(NSString *)str
{
    if (str == nil || ![str isKindOfClass:[NSString class]]) {
        return @"";
    }
    NSString *content = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return content;
}

//转换为jsong
+ (NSString *)exchangeToJson:(id)object{
    if (object == nil) {
        NSLog(@"error json转换错误");
        return @"";
    }
    NSData * dataJson = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:nil];
    NSString * strJson = [[NSString alloc]initWithData:dataJson encoding:NSUTF8StringEncoding];
    return strJson;
}

//转换json
+ (NSString *)exchangeModel:(id)model{
    NSDictionary * dicJson = [model dictionaryRepresentation];
    NSData * dataJson = [NSJSONSerialization dataWithJSONObject:dicJson options:NSJSONWritingPrettyPrinted error:nil];
    NSString * strJson = [[NSString alloc]initWithData:dataJson encoding:NSUTF8StringEncoding];
    return strJson!=nil?strJson:@"";
}

//转换String to dic
+ (NSDictionary *)exchangeStringToDic:(NSString *)str{
    if (str.length == 0) {
        return [NSDictionary dictionary];
    }
    NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * dicReturn = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return dicReturn;
}
+ (NSMutableArray *)exchangeDic:(id)response toAryWithModelName:(NSString *)modelName{
    if (response == nil || !isStr(modelName)) {
        return [NSMutableArray array];
    }
    id class = NSClassFromString(modelName);
    if (class == nil) {
        return [NSMutableArray array];
    }
    if ([response isKindOfClass:[NSDictionary class]]) {
        if ([class respondsToSelector:@selector(modelObjectWithDictionary:)]) {
            id model = [class performSelector:@selector(modelObjectWithDictionary:) withObject:response];
            return [NSMutableArray arrayWithObject:model];
        }
    }
    if ([response isKindOfClass:[NSArray class]]) {
        NSMutableArray * aryReturn = [NSMutableArray array];
        for (NSDictionary * dic in (NSArray *)response) {
            if ([class respondsToSelector:@selector(modelObjectWithDictionary:)]) {
                id model = [class performSelector:@selector(modelObjectWithDictionary:) withObject:dic];
                [aryReturn addObject:model];
            }
        }
        return aryReturn;
    }
    return [NSMutableArray array];
}
//十六进制转颜色
+ (UIColor *)exchangeColorWith16:(NSString*)hexColor
{
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&red];
    
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&green];
    
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f)green:(float)(green / 255.0f) blue:(float)(blue / 255.0f)alpha:1.0f];
}

+(NSString *)doubleToString:(double)dou
{
    NSString *string = [NSString stringWithFormat:@"%f",dou];
    NSDecimalNumber *workID = [NSDecimalNumber decimalNumberWithString:string];
    string = [workID stringValue];
    return string;
}


//移除全部子视图
+ (void)removeAllSubViews:(UIView *)view{
    if (view == nil) return;
    NSArray * aryView = view.subviews;
    for (UIView * viewSub in aryView) {
        [viewSub removeFromSuperview];
    }
}
//将视图移除
+ (void)removeView:(UIView *)view{
    if (view == nil) return;
    [view removeFromSuperview];
    view = nil;
}

+ (void)removeAllSubView:(UIView *)view withTag:(NSInteger)tag{
    if (view == nil) return;
    NSArray * aryView = view.subviews;
    for (UIView * viewSub in aryView) {
        if (viewSub.tag == tag) {
            [viewSub removeFromSuperview];
        }
    }
}

//隐藏显示tag视图
+ (void)showHideViewWithTag:(int)tag inView:(UIView *)viewAll isshow:(BOOL)isShow{
    UIView * viewTag = [viewAll viewWithTag:tag];
    if (viewTag != nil) {
        viewTag.hidden = !isShow;
    }
}

////显示提示
//+ (void)showAlert:(NSString *)strAlert{
//    [[GlobalData sharedInstance].GB_NoticeView showNotice:strAlert time:2 frame:[UIScreen mainScreen].bounds viewShow:[UIApplication sharedApplication].keyWindow ];
//}

//获取版本号
+ (NSString *)getVersion{
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    return appVersion;    
}
//获取版本号
+ (NSString *)getBuildVersion{
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDic objectForKey:@"CFBundleVersion"];
    appVersion = [appVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
    return appVersion;
}

//适配label字号
+ (CGFloat)adaptLabelFont:(CGFloat)fontNum{
    if (isIphone5) {
        return  fontNum - 1;
    } else if (isIphone6){
        return fontNum;
    } else if (isIphone6p){
        return fontNum + 1;
    }
    return fontNum + 2;
}

+(NSString *)getBirth
{
//    DateArrModel *model = [[DateArrModel alloc]init];
    BriefInfoModel *model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
    NSString *birth =  model.uBirthday.length > 10 ? [model.uBirthday substringToIndex:7] : @"";
    return birth;
//    !kStringIsEmpty(birth) ? model.nowYear - [[birth substringToIndex:4] integerValue]
}

+ (BOOL)canLeftSlide
{
    if (GB_Nav.viewControllers.count <=1) {
        return false;
    }
    for (UIViewController * vc in GB_Nav.viewControllers) {
        if ([vc isKindOfClass:[LoginViewController class]]) {
            return false;
        }
    }
    return true;
}

#pragma mark 根据推送的消息进行跳转
+ (void)jumpWithPushJson
{
  
}

#pragma mark 存储本地数据

+ (void)whiteArrayToPlist:(NSMutableArray *)array filePath:(NSString *)filePath
{
    if (array.count > 0)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *plistPath = [paths objectAtIndex:0];
        filePath = [NSString stringWithFormat:@"%@.plist",filePath];
        NSString *filename = [plistPath stringByAppendingPathComponent:filePath];
        
        [array writeToFile:filename atomically:YES];
        
        NSLog(@"%@",filename);
        
    }
}

+ (NSMutableArray *)getArrayToPlist:(NSMutableArray *)array filePath:(NSString *)filePath
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *plistPath = [paths objectAtIndex:0];
    
    filePath = [NSString stringWithFormat:@"%@.plist",filePath];
    NSString *filename = [plistPath stringByAppendingPathComponent:filePath];
    
    NSMutableArray *dataArray = [NSMutableArray arrayWithContentsOfFile:filename];
    
    return dataArray;
}

+ (void)writeStr:(NSString *)strValue forKey:(NSString *)strKey{
    if ([self isStr:strKey ] ) {
        NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
        [user setObject:strValue  forKey:strKey];
        [user synchronize];
    }
}

+ (void)writeDate:(NSDate *)date  forKey:(NSString *)strKey{
    if (date == nil ) return;
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:date  forKey:strKey];
    [user synchronize];
}

+ (NSString *)readStrFromUser:(NSString *)strKey{
    if ([self isStr:strKey]) {
        NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
        NSString * strValue = [user objectForKey:strKey];
        if (strValue != nil && [strValue isKindOfClass:[NSString class]]&& strValue.length>0) {
            return strValue;
        }
    }
    return @"";
}

+ (void)writeDouble:(double)numDouble forKey:(NSString *)strKey{
    NSNumber * num = [NSNumber numberWithDouble:numDouble];
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:num  forKey:strKey];
    [user synchronize];
}

+ (double)readDoubleFromUser:(NSString *)strKey{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    NSNumber * num = [user objectForKey:strKey];
    if (num != nil && [num isKindOfClass:[NSNumber class]]) {
        return [num doubleValue];
    }
    return 0;
}

+ (NSDate *)readDateFromUser:(NSString *)strKey{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    NSDate * dateLocal = [user objectForKey:strKey];
    return dateLocal;
}

+ (void)writeDataToUser:(NSData *)data forKey:(NSString *)strKey{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:data  forKey:strKey];
    [user synchronize];
}

+ (NSData *)readDataFromUser:(NSString *)strKey{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    NSData * dataLocal = [user objectForKey:strKey];
    return dataLocal;
}

//设置日期格式
+ (NSString *)exchangeDate:(NSDate *)date formatter:(NSString *)formate{
    if (date == nil || formate == nil) {
        return @"";
    }
    // 必填字段
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:formate];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:date];
    return currentDateStr;
}

+ (NSDate *)exchangeString:(NSString *)str formatter:(NSString *)formate{
    if (str == nil || formate == nil) {
        return [NSDate date];
    }
    // 必填字段
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:formate];
    //用[NSDate date]可以获取系统当前时间
    NSDate *currentDate = [dateFormatter dateFromString:str];
    return currentDate;
}

+ (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

+(UILabel *)setAttributeLabel:(UILabel *)label content:(NSString *)content width:(CGFloat)width
{
    label.width = width;
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:content];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    CGRect rect =[attributedString boundingRectWithSize:CGSizeMake(label.width, 1000)  options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    NSLog(@"%f",rect.size.height);
    CGFloat lineSpace = rect.size.height > 22 ? F(7) : 0;
    [paragraphStyle setLineSpacing:lineSpace];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [content length])];
    [label setAttributedText:attributedString];
    [label sizeToFit];
    return label;
}

+(NSAttributedString *)getHtmlAttributeString:(NSString *)htmlStr
{
    NSTimeInterval date = [NSDate timeIntervalSinceReferenceDate];
    NSString * htmlStringStr = [CustomHTMLTransformString transformHtmlString:htmlStr font:F(15) imageWidth:KWIDTH - W(15) * 2];
    NSDictionary *dic;
    NSMutableParagraphStyle * detailParagtaphStyle = [[NSMutableParagraphStyle alloc]init];
    detailParagtaphStyle.alignment = NSTextAlignmentJustified;   //设置两端对齐
    [detailParagtaphStyle setLineSpacing:F(14)];                   //行间距
    if ([htmlStringStr rangeOfString:@"http"].location != NSNotFound) {
        dic = @{
                NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone],
                NSParagraphStyleAttributeName : detailParagtaphStyle,
                NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
    }else{
        dic = @{
                NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleNone],
                NSParagraphStyleAttributeName : detailParagtaphStyle,
                NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
    }
    
    
    
    NSAttributedString *htmlAttribute = [[NSAttributedString alloc]initWithData:[htmlStringStr dataUsingEncoding:NSUnicodeStringEncoding] options:dic documentAttributes:nil error:nil];
    
    NSLog(@"%f",date - [NSDate timeIntervalSinceReferenceDate]);
    
    [NSDate timeIntervalSinceReferenceDate];
    
    return htmlAttribute;
}

//获取时间戳
+ (NSString *)fetchTimeStamp{
    return [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
}

#pragma mark 验证字符串

+ (BOOL)isStr:(NSString *)str{
    if (str != nil && [str isKindOfClass:[NSString class]] ) {
        return YES;
    }
    return NO;
}

+ (BOOL)isDic:(NSDictionary *)dic{
    if (dic != nil && [dic isKindOfClass:[NSDictionary class]]) {
        return YES;
    }
    return NO;
}


+(NSString *)removeHTML:(NSString *)html {
    
    NSScanner *theScanner;
    
    NSString *text = nil;
    
    
    
    theScanner = [NSScanner scannerWithString:html];
    
    
    
    while ([theScanner isAtEnd] == NO) {
        
        // find start of tag
        
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        
        
        
        // find end of tag
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        
        
        // replace the found tag with a space
        
        //(you can filter multi-spaces out later if you wish)
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@" "];
        
        
        
    }
    
    return html;
    
}

#pragma mark 弹出提示
+ (void)showNotiInStatusBar:(NSString *)strMsg  bageNum:(int)msgCount{
    UILocalNotification* noti = [[UILocalNotification alloc]init];
    NSDate* now = [NSDate date];
    noti.fireDate = [now dateByAddingTimeInterval:4];
    noti.timeZone = [NSTimeZone defaultTimeZone];
    noti.alertBody = strMsg;
    noti.soundName = UILocalNotificationDefaultSoundName;
    noti.alertAction = strMsg;
    noti.applicationIconBadgeNumber = msgCount;
    //    [noti setUserInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], @"notikey", nil]];
    //NSLog(@"您有%d条消息需要处理",msgCount);
    [[UIApplication sharedApplication] scheduleLocalNotification:noti];
}

#pragma mark 拼写文件名
+ (NSString *)fetchDoumentPath:(NSString *)name{
    NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"sld"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString *result = [path stringByAppendingPathComponent:name];
    
    return result;
}

+ (NSString*)fetchDoumentSize{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * strPath = [documentsDirectory stringByAppendingPathComponent:@"default"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    long long num = 0;
    if ([fileManager fileExistsAtPath:strPath]){
        num = [[fileManager attributesOfItemAtPath:strPath error:nil] fileSize];
    }
    return 0;
    //    NSLog(@"%@",[NSString stringWithFormat:@"%l",num/(1024.0*1024.0)]);
    //    return [NSString stringWithFormat:@"%l",num/(1024.0*1024.0)];
}

//跳转店铺
+ (void)jumpToShopDetail:(double )sid{
    //    ShopDetailHtmlViewController * shopHtmlVC = [[ShopDetailHtmlViewController alloc]init];
    //    shopHtmlVC.supplerID = sid;
    //    [GB_Nav pushViewController:shopHtmlVC animated:YES];
}

#pragma mark 判断推送状态
//+ (BOOL)IsEnablePush{
//    if (isIOS8) {// system is iOS8
//        UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
//        if (UIUserNotificationTypeNone != setting.types) {
//            return YES;
//        }
//    } else {//iOS7
//        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
//        if(UIRemoteNotificationTypeNone != type)
//            return YES;
//    }
//    return NO;
//}


#pragma mark 判断网络状态
+ (BOOL)IsEnableNetwork{
    if ([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) {
        return NO;
    }
    return YES;
}

+ (BOOL)IsEnableWifi{
    return ([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWiFi);
    
}

+ (BOOL)IsENable3G{
    return ([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWWAN);
    
}


#pragma mark encode  decode

+(NSString*)encodeString:(NSString*)unencodedString{
    
    // CharactersToBeEscaped = @":/?&=;+!@#$()~',*";
    
    // CharactersToLeaveUnescaped = @"[].";
    
    NSString*encodedString=(NSString*)
    
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              
                                                              (CFStringRef)unencodedString,
                                                              
                                                              NULL,
                                                              
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              
                                                              kCFStringEncodingUTF8));
    
    return encodedString;
    
}

//URLDEcode

+(NSString*)decodeString:(NSString*)encodedString

{
    
    //NSString *decodedString = [encodedString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    
    NSString*decodedString=(__bridge_transfer  NSString*)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                                                 
                                                                                                                 (__bridge CFStringRef)encodedString,
                                                                                                                 
                                                                                                                 CFSTR(""),
                                                                                                                 
                                                                                                                 CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    return decodedString;
    
}

@end

