//
//  GlobalMethod+Version.h
//  乐销
//
//  Created by 隋林栋 on 2016/12/15.
//  Copyright © 2016年 ping. All rights reserved.
//

#import "GlobalMethod.h"

@interface GlobalMethod (Version)

//注销
+ (void)logoutSuccess;
//清除全局数据
+ (void)clearUserInfo;

//判断是否登陆
+(BOOL)isLoginSuccess;

//登陆 with block
+ (void)loginWithBlock:(void (^)(void))block;

//创建rootNav
+ (void)createRootNav;

//跳转页面
+ (void)jumpToVC:(NSString *)strClassName anminated:(BOOL)animated;

@end
