//
//  GlobalMethod+UI.h
//  乐销
//
//  Created by 隋林栋 on 2016/12/16.
//  Copyright © 2016年 ping. All rights reserved.
//

#import "GlobalMethod.h"

@interface GlobalMethod (UI)
//计算高度 宽度
+ (CGFloat)fetchHeightFromLabel:(UILabel *)label;
+ (CGFloat)fetchHeightFromLabel:(UILabel *)label heightLimit:(CGFloat )height;
+ (CGFloat)fetchWidthFromLabel:(UILabel *)label;

+ (CGFloat)fetchHeightFromString:(NSString *)string isLimitWidth:(CGFloat)limitWidth  font:(UIFont *)font;

//设置label
+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
         aligent:(NSTextAlignment )aligent
            text:(NSString *)text
         bgColor:(UIColor *)color;
+ (void)setLabel:(UILabel *)label
      widthLimit:(CGFloat )widthLimit
        numLines:(NSInteger)numLines
         fontNum:(CGFloat)fontNum
       textColor:(UIColor *)textColor
            text:(NSString *)text;
+ (void)resetLabel:(UILabel *)label
              text:(NSString *)text
      isWidthLimit:(BOOL )isWidthLimit;


//设置圆角
+ (void)setRoundView:(UIView *)iv color:(UIColor *)color;
+ (void)setRoundView:(UIView *)iv color:(UIColor *)color numRound:(int)numRound width:(CGFloat)width;

//textfield添加左边距
+ (void)setTextFileLeftPadding:(UITextField *)ut leftPadding:(float)leftPadding;
//添加竖线
+ (void)addLineFrame:(CGRect)rect inView:(UIView *)view isDark:(BOOL)isDark;

//导航栏下添加横线
+ (UIView *)addNavLine;
+ (UIView *)addNavLineFrame:(CGRect)frame;
//添加白色竖线
+ (UIView *)addVerticalLineFrame:(CGRect)frame color:(UIColor *)color;

+ (UIView *)backgroundNoDateViewFrame:(CGRect)frame;

@end
