//
//  CustomHTMLTransformString.h
//  神农岛
//
//  Created by 宋晨光 on 16/10/5.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CustomHTMLTransformString : NSObject
+ (NSMutableAttributedString *)transformhtmlString:(NSString *)content font:(CGFloat)sizefont;
+ (NSString *)transformhtmlStringString:(NSString *)content font:(CGFloat)sizefont;
+ (NSString *)transformHtmlString:(NSString *)content font:(CGFloat)fontSize imageWidth:(CGFloat)imageWidth;
@end
