//
//  CustomHTMLTransformString.m
//  神农岛
//
//  Created by 宋晨光 on 16/10/5.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "CustomHTMLTransformString.h"


@implementation CustomHTMLTransformString

+ (NSMutableAttributedString *)transformhtmlString:(NSString *)content font:(CGFloat)sizefont
{
    
    NSString * htmlString = [NSString stringWithFormat:@"%@",content];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"<p>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"</p>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"</h1>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\t" withString:@"\n"];
    //    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"@1e_1c_1000w" withString:@""];
    for (int i = 0; i<100; i++)
    {
        if ([htmlString rangeOfString:@"\n\n"].location !=NSNotFound)
        {
            htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
        }else
        {
            break;
        }
    }
    NSString * string;
    for (int i = 0; i<3; i++) {
        string = [htmlString substringToIndex:1];
        if ([string isEqualToString:@"\n"]) {
            htmlString = [htmlString  stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }else
        {
            break;
        }
    }
    htmlString                     = [htmlString   stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/><br/>"];
    NSRegularExpression *regex     = [NSRegularExpression regularExpressionWithPattern:@"<img[^>]*src[^>]*>" options:NSRegularExpressionAllowCommentsAndWhitespace error:nil];
    NSArray *result                = [regex matchesInString:htmlString options:NSMatchingReportCompletion range:NSMakeRange(0, htmlString.length)];
        NSLog(@"=====------------%@",result);
    
    //左右边距为15时，为下面的值
    
    NSString * imagePX;
    if (KWIDTH == 320)
    {
        imagePX = @"296";
    }else if (KWIDTH == 375)
    {
        imagePX =@"345";
    }else if (KWIDTH == 414)
    {
        imagePX = @"384";
    }
    
    NSString * imageStyle = [NSString stringWithFormat:@"style=\"width: %@px; height: auto !important;\"/>",imagePX];
    NSString * appenStyle = [NSString stringWithFormat:@"style=\"width: %@px; height: auto !important;\"",imagePX];
    
    NSMutableArray * imgHtmlArr    = [NSMutableArray array];
    NSMutableArray * newImgHtmlArr = [NSMutableArray array];
    
    for (NSTextCheckingResult *item in result) {
        
        NSString *imgHtml = [htmlString substringWithRange:[item rangeAtIndex:0]];
        
        [imgHtmlArr addObject:imgHtml];
        
        NSString * newHtml;
        
        if ([imgHtml containsString:@"/>"]) {
            if ([imgHtml containsString:@"style="]) {
                newHtml = [imgHtml stringByReplacingOccurrencesOfString:@"style=\"\"" withString:appenStyle];
            }else{
                newHtml= [imgHtml stringByReplacingOccurrencesOfString:@"/>" withString:imageStyle];
            }
        }else{
            if ([imgHtml containsString:@"style="]) {
                newHtml = [imgHtml stringByReplacingOccurrencesOfString:@"style=\"\"" withString:appenStyle];
            }else{
                newHtml= [imgHtml stringByReplacingOccurrencesOfString:@">" withString:imageStyle];
            }
        }
        
        [newImgHtmlArr addObject:newHtml];
    }
    NSLog(@"%@",newImgHtmlArr);
    
    
    
    for (int i = 0; i<imgHtmlArr.count; i++) {
        htmlString                             = [htmlString   stringByReplacingOccurrencesOfString:imgHtmlArr[i] withString:newImgHtmlArr[i]];
        
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:7];
    
    
    NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyle};
    NSString * stirng2 = [htmlString substringFromIndex:htmlString.length-5];
    
    if ([stirng2 isEqualToString:@"<br/>"]) {
        htmlString = [htmlString  stringByReplacingCharactersInRange:NSMakeRange(htmlString.length-5, 5) withString:@""];
    }
    NSLog(@"%@",htmlString);
    
    htmlString  = [NSString stringWithFormat:@"<font style='font-size: %fpx';>%@%@",sizefont,htmlString,@"</font>"];
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [attrStr addAttributes:attributes range:NSMakeRange(0, attrStr.length)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[HexStringColor colorWithHexString:@"444444"] range:NSMakeRange(0, attrStr.length)];
    
    NSLog(@"%@",attrStr);
    
    return attrStr;
}

+ (NSString *)transformhtmlStringString:(NSString *)content font:(CGFloat)sizefont

{
    
    NSString * htmlString = [NSString stringWithFormat:@"%@",content];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"<p>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"</p>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"</h1>" withString:@"\n"];
    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\t" withString:@"\n"];
    //    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"@1e_1c_1000w" withString:@""];
    for (int i = 0; i<100; i++)
    {
        if ([htmlString rangeOfString:@"\n\n"].location !=NSNotFound)
        {
            htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
        }else
        {
            break;
        }
    }
    NSString * string;
    for (int i = 0; i<3; i++) {
        string = [htmlString substringToIndex:1];
        if ([string isEqualToString:@"\n"]) {
            htmlString = [htmlString  stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }else
        {
            break;
        }
    }
    htmlString                     = [htmlString   stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/><br/>"];
    NSRegularExpression *regex     = [NSRegularExpression regularExpressionWithPattern:@"<img[^>]*src[^>]*>" options:NSRegularExpressionAllowCommentsAndWhitespace error:nil];
    NSArray *result                = [regex matchesInString:htmlString options:NSMatchingReportCompletion range:NSMakeRange(0, htmlString.length)];
    NSLog(@"=====------------%@",result);
    
    //左右边距为15时，为下面的值
    
    NSString * imagePX;
    if (KWIDTH == 320)
    {
        imagePX = @"296";
    }else if (KWIDTH == 375)
    {
        imagePX =@"345";
    }else if (KWIDTH == 414)
    {
        imagePX = @"384";
    }
    
    NSString * imageStyle = [NSString stringWithFormat:@"style=\"width: %@px; height: auto !important;\"/>",imagePX];
    NSString * appenStyle = [NSString stringWithFormat:@"style=\"width: %@px; height: auto !important;\"",imagePX];
    
    NSMutableArray * imgHtmlArr    = [NSMutableArray array];
    NSMutableArray * newImgHtmlArr = [NSMutableArray array];
    
    for (NSTextCheckingResult *item in result) {
        
        NSString *imgHtml = [htmlString substringWithRange:[item rangeAtIndex:0]];
        
        [imgHtmlArr addObject:imgHtml];
        
        NSString * newHtml;
        
        if ([imgHtml containsString:@"/>"]) {
            if ([imgHtml containsString:@"style="]) {
                newHtml = [imgHtml stringByReplacingOccurrencesOfString:@"style=\"\"" withString:appenStyle];
            }else{
                newHtml= [imgHtml stringByReplacingOccurrencesOfString:@"/>" withString:imageStyle];
            }
        }else{
            if ([imgHtml containsString:@"style="]) {
                newHtml = [imgHtml stringByReplacingOccurrencesOfString:@"style=\"\"" withString:appenStyle];
            }else{
                newHtml= [imgHtml stringByReplacingOccurrencesOfString:@">" withString:imageStyle];
            }
        }
        
        [newImgHtmlArr addObject:newHtml];
    }
    NSLog(@"%@",newImgHtmlArr);
    
    
    
    for (int i = 0; i<imgHtmlArr.count; i++) {
        htmlString                             = [htmlString   stringByReplacingOccurrencesOfString:imgHtmlArr[i] withString:newImgHtmlArr[i]];
        
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:7];
    
    
    NSDictionary *attributes = @{NSParagraphStyleAttributeName:paragraphStyle};
    NSString * stirng2 = [htmlString substringFromIndex:htmlString.length-5];
    
    if ([stirng2 isEqualToString:@"<br/>"]) {
        htmlString = [htmlString  stringByReplacingCharactersInRange:NSMakeRange(htmlString.length-5, 5) withString:@""];
    }
    NSLog(@"%@",htmlString);
    
    htmlString  = [NSString stringWithFormat:@"<font style='font-size: %fpx';>%@%@",sizefont,htmlString,@"</font>"];
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    [attrStr addAttributes:attributes range:NSMakeRange(0, attrStr.length)];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[HexStringColor colorWithHexString:@"444444"] range:NSMakeRange(0, attrStr.length)];
    
    NSLog(@"%@",attrStr);
    
    return htmlString;
}

+ (NSString *)transformHtmlString:(NSString *)content font:(CGFloat)fontSize imageWidth:(CGFloat)imageWidth
{
    NSString * htmlString = [NSString stringWithFormat:@"%@",content];
    
//    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\n\n\n\n" withString:@"\n\n"];
//    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\n\n\n" withString:@"\n\n"];
//    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n\n"];
//    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"\n" withString:@"\n\n"];
//    htmlString = [htmlString   stringByReplacingOccurrencesOfString:@"<img" withString:@"\n<img"];
    NSLog(@"%@",htmlString);
    
    CGFloat spaceFont = 25;
    
    if (KWIDTH == 320)
    {
        spaceFont = 18;
    }
    
    htmlString = [NSString stringWithFormat:@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;\" name=\"viewport\" /><meta name=\"apple-mobile-web-app-capable\" content=\"yes\"><meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\"><link rel=\"stylesheet\" type=\"text/css\" /><style type=\"text/css\"> #content{color:#666666;font-size:%.fpx;}</style> <style>img{width:%.fpx !important;}</style></head><body><div id=\"content\" style=\"line-height:%.fpx\" >%@</div>",fontSize,imageWidth,spaceFont,content];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"<br/>"];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"/><<br/><br/><img" withString:@"<br/><img"];
    
    return htmlString;
    
}

@end
