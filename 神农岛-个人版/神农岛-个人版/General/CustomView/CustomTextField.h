//
//  customTextField.h
//  神农岛
//
//  Created by 宋晨光 on 16/4/13.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

-(CGRect)placeholderRectForBounds:(CGRect)bounds;

/**
 *  隐私设置 模糊搜索
 */
 + (CGRect)placeholderRectForBoundsWithPrivacySearch:(CGRect)bounds;
@end
