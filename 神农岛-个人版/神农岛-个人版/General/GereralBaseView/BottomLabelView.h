//
//  BottomLabelView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomControl;

@protocol bottomButtonViewDelegate <NSObject>

@optional
- (void)protocolBottomBtnSelect:(NSUInteger)tag btn:(CustomControl *)control;

@end

@interface BottomLabelView : UIView

//代理
@property (nonatomic, weak) id<bottomButtonViewDelegate> delegate;

@property (nonatomic ,strong) UILabel *labelV;

+(instancetype)initWithTitle:(NSArray *)titleArr frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate;

-(instancetype)initWithTitle:(NSArray *)titleArr frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate;

@end

@interface CustomControl : UIControl

@property (nonatomic ,strong) UILabel *controlLabel;

-(instancetype)initWithTitle:(NSString *)title frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor;

@end
