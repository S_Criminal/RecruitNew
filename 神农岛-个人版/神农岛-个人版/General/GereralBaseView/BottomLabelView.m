//
//  BottomLabelView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/31.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "BottomLabelView.h"

@implementation BottomLabelView

+(instancetype)initWithTitle:(NSArray *)titleArr frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate
{
    return [[BottomLabelView alloc]initWithTitle:titleArr frame:frame bgColor:bgColor textColor:textColor delegate:delegate];
}

-(instancetype)initWithTitle:(NSArray *)titleArr frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor delegate:(id)delegate;
{
    self = [super initWithFrame:frame];
    if (self != nil){
        CGFloat topConstraint = 7.5f;
        CGFloat w = frame.size.width / titleArr.count - 13;
        CGFloat h = frame.size.height - topConstraint * 2;
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(13 + w - 10, topConstraint, 20, h)];
        line.backgroundColor = COLOR_MAINCOLOR;  
        line.hidden = titleArr.count != 2;
        [self addSubview:line];
        
        for (int i = 0; i < titleArr.count; i++)
        {
            CustomControl *control = [[CustomControl alloc]initWithTitle:titleArr[i] frame:CGRectMake(13 + w * i, topConstraint , w, h) bgColor:bgColor textColor:textColor];
            control.tag = 10 + i;
            [control setCorner:4];
            
            [control addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            
            [self addSubview:control];
            
        }
    }
    self.backgroundColor = [UIColor whiteColor];
    if ([bgColor isEqualToString:@""]) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)btnClick:(CustomControl *)btn
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(protocolBottomBtnSelect:btn:)]) {
        [self.delegate protocolBottomBtnSelect:btn.tag btn:btn];
    }
}

@end

@implementation CustomControl

//+(instancetype)initWithTitle:(NSString *)title frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor
//{
//    return [CustomControl initWithTitle:title frame:frame bgColor:bgColor textColor:textColor];
//}

-(instancetype)initWithTitle:(NSString *)title frame:(CGRect)frame bgColor:(NSString *)bgColor textColor:(NSString *)textColor
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        label.text = title;
        label.textColor = [HexStringColor colorWithHexString:textColor];
        label.backgroundColor = [HexStringColor colorWithHexString:bgColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:F(16)];
        [self addSubview:label];
    }
    return self;
}





@end
