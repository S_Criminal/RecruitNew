//
//  GeneralLeftImageAndTitleLabelView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeneralImageAndTitleModel.h"
@interface GeneralLeftImageAndTitleLabelView : UITableViewCell

@property (nonatomic ,strong) UIImageView *leftImage;
@property (nonatomic ,strong) UILabel *titleL;

@property (nonatomic ,strong) UIView *line;

#pragma mark 获取cell高度
+ (CGFloat)fetchHeight:(GeneralImageAndTitleModel *)model;
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(GeneralImageAndTitleModel *)model;

@end
