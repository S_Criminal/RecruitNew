//
//  GeneralLeftImageAndTitleLabelView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/22.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "GeneralLeftImageAndTitleLabelView.h"

@implementation GeneralLeftImageAndTitleLabelView


#pragma mark 懒加载

- (UIImageView *)leftImage{
    if (_leftImage == nil) {
        _leftImage = [UIImageView new];
        _leftImage.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _leftImage.widthHeight = XY(W(15),W(15));
    }
    return _leftImage;
}

- (UILabel *)titleL{
    if (_titleL == nil) {
        _titleL = [UILabel new];
        [GlobalMethod setLabel:_titleL widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _titleL;
}

-(UIView *)line
{
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = COLOR_SEPARATE_COLOR;
    }
    return _line;
}

#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.leftImage];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.line];
    }
    return self;
}

#pragma mark 获取高度
FETCH_CELL_HEIGHT(GeneralLeftImageAndTitleLabelView)
#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(GeneralImageAndTitleModel *)model{
//    //刷新view
    self.leftImage.leftTop = XY(W(15),W(0));
    self.leftImage.image = [UIImage imageNamed:model.imageStr];
    [GlobalMethod resetLabel:self.titleL text:model.titles isWidthLimit:false];
    self.titleL.leftTop = XY(self.leftImage.right + W(10),W(0));
    
    self.leftImage.centerY = self.centerY - 1;
    self.titleL.centerY = self.centerY - 1;
    self.line.leftTop = XY(self.leftImage.x, self.height - 1);
    self.line.widthHeight = XY(KWIDTH - W(15), 1);
    
    return self.bottom;
}



@end
