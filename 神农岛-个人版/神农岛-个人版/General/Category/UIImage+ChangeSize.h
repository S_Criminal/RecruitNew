//
//  UIImage+ChangeSize.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ChangeSize)

//改变image的大小
- (UIImage*)transformWidth:(CGFloat)width
                    height:(CGFloat)height;


@end
