//
//  UIImage+ChangeSize.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/17.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "UIImage+ChangeSize.h"

@implementation UIImage (ChangeSize)

- (UIImage*)transformWidth:(CGFloat)width
                    height:(CGFloat)height
{

    CGFloat destW = width;
    
    CGFloat destH = height;

    CGFloat sourceW = width;
    
    CGFloat sourceH = height;

    CGImageRef imageRef = self.CGImage;
    
    CGContextRef  bitmap = CGBitmapContextCreate(NULL,  destW,  destH,  CGImageGetBitsPerComponent(imageRef),  4*destW,
                                   CGImageGetColorSpace(imageRef),
                                   (kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst));
    
    CGContextDrawImage(bitmap, CGRectMake(0, 0, sourceW, sourceH), imageRef);
    
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    
    UIImage  *result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    
    
    CGImageRelease(ref);
    
    return  result;
}


@end
