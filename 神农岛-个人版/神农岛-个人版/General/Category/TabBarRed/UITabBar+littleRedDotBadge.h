//
//  UITabBar+littleRedDotBadge.h
//  神农岛
//
//  Created by 宋晨光 on 16/7/20.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (littleRedDotBadge)
- (void)showBadgeOnItemIndex:(int)index;
- (void)hideBadgeOnItemIndex:(int)index;
@end
