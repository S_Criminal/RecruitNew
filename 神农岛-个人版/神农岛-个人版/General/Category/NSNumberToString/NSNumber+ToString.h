//
//  NSNumber+ToString.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/13.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (ToString)

+ (NSNumber *)stringToNumber:(NSString *)str;

@end
