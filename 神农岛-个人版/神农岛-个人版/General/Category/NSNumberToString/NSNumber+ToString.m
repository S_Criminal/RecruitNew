//
//  NSNumber+ToString.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/13.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "NSNumber+ToString.h"

@implementation NSNumber (ToString)

+ (NSNumber *)stringToNumber:(NSString *)str
{
    NSNumber *num = [NSNumber numberWithInteger:[str integerValue]];
    return num;
}

@end
