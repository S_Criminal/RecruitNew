//
//  UITextView+Attribute.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/31.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "UITextView+Attribute.h"

@implementation UITextView (Attribute)

-(void)textViewDidChange:(UITextView *)textView
{
    //    textview 改变字体的行间距
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    paragraphStyle.lineSpacing = 20;// 字体的行间距
    
    NSDictionary *attributes = @{
                                 
                                 NSFontAttributeName:[UIFont systemFontOfSize:15],
                                 
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 
                                 };
    
    textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];

}

@end
