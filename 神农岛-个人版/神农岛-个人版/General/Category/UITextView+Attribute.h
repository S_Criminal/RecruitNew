//
//  UITextView+Attribute.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/31.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Attribute)<UITextViewDelegate>

@end
