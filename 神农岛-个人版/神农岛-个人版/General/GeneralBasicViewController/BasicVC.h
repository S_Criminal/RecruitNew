//
//  BasicVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
//提示view
#import "NoticeView.h"
//loading view
#import "LoadingView.h"

@interface BasicVC : UIViewController<RequestDelegate,UIGestureRecognizerDelegate>


@property (nonatomic, strong) LoadingView * loadingView;//loading动画
@property (nonatomic, strong) NoticeView * noticeView;//提示语
@end
