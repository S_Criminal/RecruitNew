//
//  BasicVC+BaseImageSelectVC.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/21.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC.h"
//选择图片
#import "ImagePickerViewController.h"
@interface BasicVC (BaseImageSelectVC)

<ImagePickerViewControllerDelegate>
//选择图片
- (void)showImageVC:(int)imageNum;
- (void)imageSelect:(UIImage *)image;
- (void)imagesSelect:(NSArray *)aryImages;

@end
