//
//  BaseImage.h
//  乐销
//
//  Created by 隋林栋 on 2017/1/9.
//  Copyright © 2017年 ping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseImage : UIImage
@property (nonatomic, strong) NSString *imageURL;

+ (BaseImage *)imageWithCGImage:(CGImageRef)cgImage;

@end
