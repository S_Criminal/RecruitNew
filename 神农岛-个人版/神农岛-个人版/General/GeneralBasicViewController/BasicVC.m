//
//  BasicVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/4.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC.h"

@interface BasicVC ()

@end

@implementation BasicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    UIView * viewBG = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
    viewBG.backgroundColor = [UIColor whiteColor];
//    viewBG.backgroundColor = [HexStringColor colorWithHexString:@"efeff4"];
    [self.view insertSubview:viewBG atIndex:0];
    
    GB_Nav.interactivePopGestureRecognizer.delegate = self;

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = [GlobalMethod canLeftSlide];
}

#pragma mark 懒加载
- (LoadingView *)loadingView{
    if (_loadingView == nil) {
        _loadingView = [[[NSBundle mainBundle]loadNibNamed:@"LoadingView" owner:self options:nil]lastObject];
    }
    return _loadingView;
}

- (NoticeView *)noticeView{
    if (_noticeView == nil) {
        _noticeView = [NoticeView new];
    }
    return _noticeView;
}

#pragma mark 请求过程回调
- (void)protocolWillRequest{
    [self.loadingView hideLoading];
    [self.loadingView resetFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT) viewShow:self.view];
}

- (void)protocolDidRequestSuccess{
    [self.loadingView hideLoading];
}

- (void)protocolDidRequestFailure:(NSString *)errorStr{
    [self.loadingView hideLoading];
    [self.noticeView showNotice:errorStr time:2 frame:CGRectMake(0, 0, KWIDTH, KHEIGHT) viewShow:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
