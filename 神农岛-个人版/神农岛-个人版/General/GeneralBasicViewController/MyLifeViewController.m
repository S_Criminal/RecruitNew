//
//  MyLifeViewController.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/21.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "MyLifeViewController.h"

//sectionTitle
#import "MyCertSectionView.h"
//图片九宫格
#import "MyCertImageSelectView.h"


#import "BasicVC+BaseImageSelectVC.h"

#import "RequestApi+MyBrief.h"
#import "BriefInfoModel.h"

@interface MyLifeViewController ()<certImageSelectDelegate,RequestDelegate>
@property (nonatomic ,strong) NSMutableArray *pathImageArr;
@property (nonatomic ,strong) NSMutableArray *uploadPathArray;
@property (nonatomic ,strong) MyCertImageSelectView *imageSelectView;
@property (nonatomic ,strong) NSString *imageTitle;
//@property (nonatomic ,strong) AllImageModel *model;
@property (nonatomic ,strong) NSString *contents;
@property (nonatomic ,strong) NSString *titles;
@property (nonatomic ,strong)  BaseNavView *navView;

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@end

@implementation MyLifeViewController{
    NSInteger isNowUpLoadImage;
}

-(MyCertImageSelectView *)imageSelectView
{
    if (!_imageSelectView) {
        _imageSelectView = [MyCertImageSelectView new];
        _imageSelectView.delegate = self;
    }
    return _imageSelectView;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setNav];
    [self setModel];
    [self setBasicView];
}

-(void)setNav
{
    DSWeak;
    self.navView = [BaseNavView initNavTitle:@"生活照片" leftImageName:@"" leftBlock:^{
        [GB_Nav dismissViewControllerAnimated:YES completion:nil];
        //        [GB_Nav popViewControllerAnimated:YES];
    } rightTitle:@"保存" rightBlock:^{
        [weakSelf save];
    }];
    [self.view addSubview:self.navView];
    
    [self.view addSubview:[GlobalMethod addNavLine]];
}

-(void)save
{
    NSLog(@"%@",self.uploadPathArray);
    if (isNowUpLoadImage == 1) {
        [MBProgressHUD showError:@"图片正在上传,请稍后" toView:self.view];
        return;
    }else if (self.uploadPathArray.count == 0){
        [MBProgressHUD showError:@"请选择至少一张图片" toView:self.view];
        return;
    }
    
    NSString *lifePath  = [self.uploadPathArray componentsJoinedByString:@","];
    
    DSWeak;
    [RequestApi editPersonLifeWithKey:[GlobalData sharedInstance].GB_Key lifeimg:lifePath Delegate:self success:^(NSDictionary *response) {
        [MBProgressHUD showSuccess:@"保存成功" toView:weakSelf.view];
        NSTimer *timer = [NSTimer timerWithTimeInterval:1.5 target:weakSelf selector:@selector(handleHideTimer) userInfo:@(YES) repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        weakSelf.hideDelayTimer = timer;
    } failure:^(NSString *errorStr, id mark) {
        
    }];
}

-(void)handleHideTimer
{
    [GB_Nav dismissViewControllerAnimated:YES completion:nil];
}

-(void)setModel
{
    self.pathImageArr = [NSMutableArray array];
    self.uploadPathArray = [NSMutableArray array];
    
    
    DSWeak;
    [RequestApi getLifeImageWithKey:[GlobalData sharedInstance].GB_Key Delegate:self success:^(NSDictionary *response) {
        NSArray *arr = response[@"datas"];
        NSDictionary *d = kArrayIsEmpty(arr) ? nil :  arr.firstObject;
        NSString *life = kDictIsEmpty(d) ? @"" : d[@"R_Lifeimg"];
        weakSelf.pathImageArr = kStringIsEmpty(life) ? [NSMutableArray array] :[NSMutableArray arrayWithArray:[life componentsSeparatedByString:@","]];
        weakSelf.uploadPathArray =[NSMutableArray arrayWithArray:self.pathImageArr];
        [weakSelf.imageSelectView setupImageArray:self.pathImageArr];
    } failure:^(NSString *errorStr, id mark) {
        BriefInfoModel *model = [GlobalData sharedInstance].GB_UserModel.datas0.firstObject;
        NSArray *arr = [model.rLifeimg componentsSeparatedByString:@","];
        weakSelf.pathImageArr = kArrayIsEmpty(arr) ? [NSMutableArray array] : [NSMutableArray arrayWithArray:arr];
        weakSelf.uploadPathArray =[NSMutableArray arrayWithArray:self.pathImageArr];
        [weakSelf.imageSelectView setupImageArray:self.pathImageArr];
    }];
}

-(void)setBasicView
{
    MyCertSectionView *sectionView = [[MyCertSectionView alloc]initWithFrame:CGRectMake(0, NAVIGATION_BarHeight + 1, KWIDTH, 45)];
    [sectionView resetViewWithModel:nil];
    sectionView.titleLabel.text = [NSString stringWithFormat:@"请添加%@",@"生活照片"];
    [self.view addSubview:sectionView];
    
    [self.imageSelectView setFrame:CGRectMake(0, sectionView.bottom , KWIDTH, 300)];
    [self.view addSubview:_imageSelectView];
}

-(void)protocolTapAlbum:(UIControl *)control
{
    [self showImageVC:8 - (int)self.pathImageArr.count];
}

-(void)protocolDeleteButton:(UIButton *)button
{
    [self.pathImageArr removeObjectAtIndex:button.tag - 100];
    [self.uploadPathArray removeObjectAtIndex:button.tag - 100];
    [self.imageSelectView setupImageArray:self.pathImageArr];
}

-(void)imagesSelect:(NSArray *)aryImages
{
    if (aryImages.count ==  0) {
        return;
    }
    NSLog(@"%@",self.uploadPathArray);
    [self.pathImageArr addObjectsFromArray:aryImages];
    [_imageSelectView setupImageArray:self.pathImageArr];
    isNowUpLoadImage = 1;
    NSLog(@"%@",aryImages);
    DSWeak;
    NSLog(@"%@",self.uploadPathArray);
    for (id object in aryImages)
    {
        NSData *data = UIImagePNGRepresentation(object);
        [[RequestApi share] getImageUrlWithAliYunWithImageData:data BlockHandelImageUrlStr:^(NSString *str) {
            str = [NSString stringWithFormat:@"%@%@",ssndHTTP,str];
            [self.uploadPathArray addObject:str];
        } BlockHandelSuccess:^(BOOL success) {
            if (weakSelf.pathImageArr.count == weakSelf.uploadPathArray.count)
            {
                isNowUpLoadImage = 2;
            }
        }];
    }
}


-(void)dealloc
{
    NSLog(@"certDeallloc");
    [self.hideDelayTimer invalidate];
    self.hideDelayTimer = nil;
}


@end
