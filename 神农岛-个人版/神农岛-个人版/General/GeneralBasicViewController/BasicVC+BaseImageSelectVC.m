//
//  BasicVC+BaseImageSelectVC.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/1/21.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "BasicVC+BaseImageSelectVC.h"
#import "BaseImage.h"
@implementation BasicVC (BaseImageSelectVC)
//选择图片
- (void)showImageVC:(int)imageNum{
    ImagePickerViewController * vc = [[ImagePickerViewController alloc]init];
    vc.photoNumber = imageNum;
    vc.isNotShowSelectBtn = false;
//    [GB_Nav presentViewController:vc animated:YES completion:nil];
//    [GB_Nav pushViewController:vc animated:true];
    vc.delegate = self;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark 选择图片回调
- (void)imagePickerViewController: (ImagePickerViewController *)ivc finishClick:(NSArray *)assetArray{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [GB_Nav popViewControllerAnimated:true];
    ALAsset * asset = assetArray.lastObject;
    
    BaseImage *image = [BaseImage imageWithCGImage:asset.aspectRatioThumbnail];
    [self  imageSelect:image];
    
    NSMutableArray * muAry = [NSMutableArray array];
    for (ALAsset * asset in assetArray) {
        [muAry addObject:[BaseImage imageWithCGImage:asset.aspectRatioThumbnail]];
    }
    [self imagesSelect:muAry];
}

/**
 *  点击任何一张图片的回调
 *
 *  @param asset 点击照片的asset，可通过asset拿到图片(缩略图，高清图，全屏图)和路径
 *  @param index 点击照片所在的位置
 *  @param imageUrlArray 所有照片url
 */
- (void)imagePickerViewController:(ImagePickerViewController *)ivc everyImageClick:(ALAsset *)asset index:(NSInteger)index imageUrlArray:(NSArray *)imageUrlArray imageIndexArray:(NSArray *)imageIndexArray{
    
}

/**
 *  点击第一张图片（照相机）的回调
 *
 *  @param image 拍照的image
 */
- (void)imagePickerViewController: (ImagePickerViewController *)ivc firstImageClick:(UIImage *)image{
    [self  imageSelect:image];
    
    NSMutableArray * muAry = [NSMutableArray array];
    [muAry addObject:image];
    [self imagesSelect:muAry];
}

#pragma mark 图片选择
- (void)imageSelect:(UIImage *)image{
    
}
- (void)imagesSelect:(NSArray *)aryImages{
    
}

@end
