//
//  CityShortName.m
//  神农岛
//
//  Created by 宋晨光 on 16/8/16.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "CityShortName.h"

@implementation CityShortName
//封装地址
+(NSString *)getAdressWithProvience:(NSString *)provience City:(NSString *)city{
    city = [city stringByReplacingOccurrencesOfString:provience withString:@""];
    
    NSString * adress  = [provience stringByAppendingString:city];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"自治区" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"回族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"壮族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"维吾尔" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"朝鲜族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"哈尼族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"彝族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"土家族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"苗族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"藏族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"回族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"羌族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"布依族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"黎族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"侗族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"傣族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"白族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"景颇族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"傈僳族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"蒙古族" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"地区" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"自治县" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"柯尔克孜" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"哈萨克" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"特别行政区" withString:@""];
    adress  =   [adress stringByReplacingOccurrencesOfString:@"自治州" withString:@""];
    
    
    if (adress.length<2)
    {
        return @"";
    }
    
    NSString * str = [adress substringFromIndex:adress.length-1];
    if ([str isEqualToString:@" "])
    {
        adress  = [adress stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    return adress;
}

@end
