//
//  ValidateRegularExpression.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidateRegularExpression : NSObject
//验证邮箱的正则表达式
+ (BOOL) validateEmail:(NSString *)email;
//验证手机号的正则表达式
+ (BOOL)validatePhone:(NSString *)phone;
@end
