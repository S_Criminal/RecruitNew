//
//  hetStringColor.h
//  神农岛
//
//  Created by 宋晨光 on 16/3/29.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface HexStringColor : NSObject
+(UIColor*)colorWithHexString:(NSString *)stringToConvert;

/**
 *  生成个人二维码
 *
 *  @param message 用户ID
 *  @param size    大小
 *
 *  @return Image
 */
+(UIImage *)creatCodeGeneratorWithMessage:(NSString *)message withSize:(CGFloat)size;

//这两个方法是为了设置 二维码的。因为在上面生成二维码的方法中将 网址写死了。只能将这两个方法拿出来
+ (UIImage*)imageBlackToTransparent:(UIImage*)image withRed:(CGFloat)red andGreen:(CGFloat)green andBlue:(CGFloat)blue;

+ (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size;

/**
 *  生产老刀招聘二维码
 */
+(UIImage *)creatCodeGeneratorWithLDRecurtMessage:(NSString *)message withSize:(CGFloat)size;

/**
 *  生成屏幕截图
 *
 *  @param view 当前视图
 *
 *  @return Image
 */
+(UIImage *)captureImageFromView:(UIView *)view;

@end
