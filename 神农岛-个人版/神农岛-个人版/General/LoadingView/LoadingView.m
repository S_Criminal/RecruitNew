//
//  LoadingView.m
//  米兰港
//
//  Created by 隋林栋 on 15/3/5.
//  Copyright (c) 2015年 Sl. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

- (void)resetFrame:(CGRect)frame viewShow:(UIView *)viewShow{
    self.frame = frame;
    [viewShow addSubview:self];
    [self beginAnimate];
}

- (void)hideLoading{
    [self stopAnimate];
    [self removeFromSuperview];
}

#pragma mark Animate
//begin animate
- (void)beginAnimate{
    [self stopAnimate];
    CABasicAnimation * animate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    animate.toValue = @(M_PI*2);
    [animate setRepeatCount:MAXFLOAT];
    [animate setDuration:2];
    [animate setRemovedOnCompletion:true];
    [_imageLoading.layer addAnimation:animate forKey:nil];
}

//stop animate
- (void)stopAnimate{
    [_imageLoading.layer removeAllAnimations];
}


@end
