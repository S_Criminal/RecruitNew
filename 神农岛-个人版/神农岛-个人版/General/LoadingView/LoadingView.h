//
//  LoadingView.h
//  米兰港
//
//  Created by 隋林栋 on 15/3/5.
//  Copyright (c) 2015年 Sl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageLoading;

- (void)resetFrame:(CGRect)frame viewShow:(UIView *)viewShow;
- (void)hideLoading;

@end
