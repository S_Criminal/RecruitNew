//
//  CustomTitleButtonStr.m
//  神农岛
//
//  Created by 宋晨光 on 16/10/3.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import "CustomTitleButtonStr.h"

@implementation CustomTitleButtonStr
+(UIButton *)setRightStyle:(UIButton *)button labelContent:(NSString *)content imageName:(NSString *)imageName ImageRect:(CGRect)rect color:(NSString *)color{
    if (!color) {
        [button setTitleColor:[HexStringColor colorWithHexString:color] forState:UIControlStateNormal];
    }else{
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    [button setTitle:content forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
//    [button.titleLabel setFrame:CGRectMake(0, 0, 20, 20)];
    [button.titleLabel sizeToFit];
    NSLog(@"%@",button.titleLabel);
    NSLog(@"%f",rect.origin.x);
    button.imageView.frame = rect;
    button.imageView.x = CGRectGetMaxX(button.titleLabel.frame) + rect.origin.x;
    NSLog(@"%@",button.imageView);

    return button;
}
/**
 *  图片在左
 */
+(NSMutableAttributedString *)setButtonAttributeTitleWithTitle:(NSString * )title AndTitleImageName:(NSString *)imageName ImageRect:(CGRect)rect color:(NSString *)color
{
    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
    attch.image = [UIImage imageNamed:imageName];
    attch.bounds = rect;
    
    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
    NSMutableAttributedString *tittleAttr = [[NSMutableAttributedString alloc]initWithString:[@"   " stringByAppendingString:title]];
    
    [tittleAttr insertAttributedString:string1 atIndex:0];
    
    [tittleAttr addAttribute:NSForegroundColorAttributeName value:[HexStringColor colorWithHexString:color] range:NSMakeRange(0,tittleAttr.length)];
    
    return tittleAttr;
}

/** 
 *  图片在右
 */
+(NSMutableAttributedString *)setRightImageButtonAttributeTitleWithTitle:(NSString * )title AndTitleImageName:(NSString *)imageName ImageRect:(CGRect)rect color:(NSString *)color
{
    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
    attch.image = [UIImage imageNamed:imageName];
    attch.bounds = rect;
    
    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
    NSMutableAttributedString *tittleAttr = [[NSMutableAttributedString alloc]initWithString:[@" " stringByAppendingString:title]];
    
    [tittleAttr appendAttributedString:string1];
    
    [tittleAttr addAttribute:NSForegroundColorAttributeName value:[HexStringColor colorWithHexString:color] range:NSMakeRange(0,tittleAttr.length)];
    
    return tittleAttr;
}





@end
