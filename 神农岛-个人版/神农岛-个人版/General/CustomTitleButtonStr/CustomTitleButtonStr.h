//
//  CustomTitleButtonStr.h
//  神农岛
//
//  Created by 宋晨光 on 16/10/3.
//  Copyright © 2016年 宋晨光. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CustomTitleButtonStr : NSObject
+(NSMutableAttributedString *)setButtonAttributeTitleWithTitle:(NSString * )title AndTitleImageName:(NSString *)imageName ImageRect:(CGRect)rect color:(NSString *)color;
+(NSMutableAttributedString *)setRightImageButtonAttributeTitleWithTitle:(NSString * )title AndTitleImageName:(NSString *)imageName ImageRect:(CGRect)rect color:(NSString *)color;
+(UIButton *)setRightStyle:(UIButton *)button labelContent:(NSString *)content imageName:(NSString *)imageName ImageRect:(CGRect)rect color:(NSString *)color;
@end
