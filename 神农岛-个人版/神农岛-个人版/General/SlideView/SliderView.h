//
//  SliderView.h
//  乐销
//
//  Created by 隋林栋 on 2016/12/19.
//  Copyright © 2016年 ping. All rights reserved.
//


#import <UIKit/UIKit.h>

@class CustomSliderControl;
@protocol SliderViewDelegate <NSObject>


@optional
- (void)protocolSliderViewBtnSelect:(NSUInteger)tag btn:(CustomSliderControl *)control;

@end

@interface SliderView : UIView

//代理
@property (nonatomic, weak) id<SliderViewDelegate> delegate;
@property (nonatomic, strong) UIView * viewSlid;
@property (strong, nonatomic) UIScrollView *scrollView;


//自定义
+ (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isFirstOn:(BOOL)isFistOn delegate:(id)delegate;
+ (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isScroll:(BOOL)isScroll isFirstOn:(BOOL)isFistOn delegate:(id)delegate;

//自定义
- (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isFirstOn:(BOOL)isFistOn delegate:(id)delegate;
- (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isScroll:(BOOL)isScroll isFirstOn:(BOOL)isFistOn isFirstOn:(BOOL)isFistOn delegate:(id)delegate;

//滑动到指定
- (void)sliderToIndex:(int)index noticeDelegate:(BOOL)notice;
//当前选中
- (NSUInteger)selectedIndex;
//刷新指定title
- (void)refreshBtn:(int)indext  title:(NSString *)strTitle;

@end

#pragma mark 自定义 滑动按钮

@interface CustomSliderControl:UIControl
@property (nonatomic, strong) UILabel * labelTitle;
@property (nonatomic, strong) UIImageView * iv;
//@property  BOOL isSelect_Custom;

//这里放方法
- (void)resetControlFrame:(CGRect )rect
                      tag:(int )tag
                    title:(NSString *)title
                imageName:(NSString *)imageName
            highImageName:(NSString *)highImageName
              isImageLeft:(BOOL)isImageLeft
          hasLineVertical:(BOOL)hasLineVertical;

//更改title
- (void)changeTitle:(NSString *)strTitle;
@end
