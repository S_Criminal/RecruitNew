//
//  SliderView.m
//  乐销
//
//  Created by 隋林栋 on 2016/12/19.
//  Copyright © 2016年 ping. All rights reserved.
//


#import "SliderView.h"

@implementation SliderView

#define Height_View    44//默认总高度
#define Height_SlidView 5//默认图片

#pragma mark 懒加载
- (UIScrollView *)scrollView{
    if (_scrollView == nil) {
        _scrollView = [UIScrollView new];
        _scrollView.frame = CGRectMake(0, 0, self.width, self.height);
        _scrollView.contentSize = CGSizeMake(0, 0);
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = false;
        _scrollView.showsVerticalScrollIndicator = false;
    }
    return _scrollView;
}

#pragma mark 初始化
//自定义
+ (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isFirstOn:(BOOL)isFistOn delegate:(id)delegate{
    return [[SliderView alloc]initWithModels:aryModel frame:frame isHasSlider:isHasSlider isImageLeft:isImageLeft isFirstOn:isFistOn delegate:delegate];
}

+ (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isScroll:(BOOL)isScroll isFirstOn:(BOOL)isFistOn delegate:(id)delegate{
    return [[SliderView alloc]initWithModels:aryModel frame:frame isHasSlider:isHasSlider isImageLeft:isImageLeft isScroll:isScroll isFirstOn:isFistOn  delegate:delegate];
}

//自定义图片
- (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isFirstOn:(BOOL)isFistOn delegate:(id)delegate{
    return [[SliderView alloc]initWithModels:aryModel frame:frame isHasSlider:isHasSlider isImageLeft:isImageLeft isScroll:false isFirstOn:isFistOn  delegate:delegate];
}

- (instancetype)initWithModels:(NSArray *)aryModel frame:(CGRect)frame isHasSlider:(BOOL)isHasSlider isImageLeft:(BOOL)isImageLeft isScroll:(BOOL)isScroll isFirstOn:(BOOL)isFistOn delegate:(id)delegate{
    self = [super initWithFrame:frame];
    if (self != nil) {
        if (delegate != nil) self.delegate = delegate;
        if (aryModel.count == 0) return self;
        float height = frame.size.height;
        if (height == 0) {
            height = Height_View;
        }
        //设置frame
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, height);
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.scrollView];

        //添加按钮
        float btnWidth  = isScroll ? 0 : self.width/aryModel.count;
        float btnHeight = isHasSlider? height - Height_SlidView : height;
        float numX = 0;
        float widthSlider = 0;
        
        for (int i=0; i<aryModel.count; i++) {
            ModelBtn * model = aryModel[i];
            CustomSliderControl * control = [CustomSliderControl new];
            [control resetControlFrame:CGRectMake(numX, 0, btnWidth, btnHeight) tag:i title:model.title imageName:model.imageName highImageName:model.highImageName isImageLeft:isImageLeft hasLineVertical:i<aryModel.count-1];
            [control addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            if (isFistOn)
            {
                if (i == 0) {
                    control.selected = YES;
                    widthSlider = control.width;
                }
            }
            numX = control.right;
            [self.scrollView addSubview:control]; 
            self.scrollView.contentSize = CGSizeMake(numX, 0);
        }
        //添加滑动条
        if (isHasSlider) {
            self.viewSlid = [[UIView alloc]initWithFrame:CGRectMake(W(15), btnHeight + 1, widthSlider - W(30), Height_SlidView)];
            self.viewSlid.backgroundColor = COLOR_MAINCOLOR;
            [self.scrollView addSubview:self.viewSlid];
        }
    }
    
    return self;
}


//按钮点击
- (void)btnClick:(CustomSliderControl *)btn
{
    //设置按钮选择
    NSArray * ary = self.scrollView.subviews;
    for (UIView * viewSub in ary) {
        if ([viewSub isKindOfClass:[CustomSliderControl class]]) {
            CustomSliderControl * btnSub = (CustomSliderControl *)viewSub;
            btnSub.selected = NO;
        }
    }
    //滑动条滑动
    [self animateSlide:btn];
    //代理回调
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolSliderViewBtnSelect:btn:)]) {
        [self.delegate protocolSliderViewBtnSelect:btn.tag btn:btn];
    }
}


- (void)sliderToIndex:(int)index  noticeDelegate:(BOOL)notice{
    //设置按钮选择
    NSArray * ary = self.scrollView.subviews;
    for (UIView * viewSub in ary) {
        if ([viewSub isKindOfClass:[CustomSliderControl class]]) {
            CustomSliderControl * btnSub = (CustomSliderControl *)viewSub;
            //如果选择的没变则返回
            if (btnSub.tag == index) {
                if (btnSub.selected) {
                    return;
                }
                //滑动条滑动
                [self animateSlide:btnSub];
                //代理回调
                if (notice) {
                    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(protocolSliderViewBtnSelect:btn:)]) {
                        [self.delegate protocolSliderViewBtnSelect:index btn:btnSub];
                    }
                }
            }else{
                btnSub.selected = NO;
            }
        }
    }
}


- (NSUInteger)selectedIndex{
    NSArray * ary = self.scrollView.subviews;
    for (UIView * viewSub in ary) {
        if ([viewSub isKindOfClass:[CustomSliderControl class]]) {
            CustomSliderControl * btnSub = (CustomSliderControl *)viewSub;
            if (btnSub.selected) {
                return btnSub.tag;
            }
        }
    }
    return 0;
    
}

- (void)refreshBtn:(int)index title:(NSString *)strTitle{
    CustomSliderControl * btn = (CustomSliderControl *) [self viewWithTag:index];
    if (btn != nil && [btn isKindOfClass:[CustomSliderControl class]]) {
        [btn changeTitle:strTitle];
    }
}

#pragma mark 滑动动画
- (void)animateSlide:(UIControl *)btn{
    btn.selected = YES;
    //滑动条滑动
    [UIView animateWithDuration:0.3 animations:^{
        self.viewSlid.x = btn.x + W(15);
        self.viewSlid.width = btn.width - W(30);
//        self.viewSlid.centerX = btn.width / 2.0f;
        float numX = KWIDTH / 2 - btn.centerX;
        
        //判断是否可以滑动
        if (self.scrollView.contentSize.width - KWIDTH>0) {
            if (-numX<0) {
                self.scrollView.contentOffset = CGPointMake(0, 0);
            }else if(-numX>self.scrollView.contentSize.width - KWIDTH){
                self.scrollView.contentOffset = CGPointMake(self.scrollView.contentSize.width-KWIDTH, 0);
            }else{
                self.scrollView.contentOffset = CGPointMake(-numX, 0);
            }
        }
    }];
    
}
@end



@implementation CustomSliderControl

#pragma mark 初始化
- (instancetype)init{
    self = [super init];
    if (self) {
        [self addSubview:self.labelTitle];
        [self addSubview:self.iv];
    }
    return  self;
}

//懒加载
- (UILabel *)labelTitle{
    if (_labelTitle == nil) {
        _labelTitle = [UILabel new];
        [GlobalMethod setLabel:_labelTitle widthLimit:0 numLines:1 fontNum:F(16) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _labelTitle;
}

- (UIImageView *)iv{
    if (_iv == nil) {
        _iv = [UIImageView new];
        _iv.backgroundColor = [UIColor clearColor];
    }
    return  _iv;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    self.labelTitle.textColor = selected ? COLOR_MAINCOLOR : COLOR_LABELThreeCOLOR;
    self.iv.highlighted = selected;
}
#pragma mark 加载页面
- (void)resetControlFrame:(CGRect )rect
                      tag:(int )tag
                    title:(NSString *)title
                imageName:(NSString *)imageName
            highImageName:(NSString *)highImageName
              isImageLeft:(BOOL)isImageLeft
          hasLineVertical:(BOOL)hasLineVertical
{
    self.frame = rect;
    self.tag = tag;
    [GlobalMethod resetLabel:self.labelTitle text:title isWidthLimit:0];
    if (rect.size.width == 0) {
        self.width = self.labelTitle.width + W(10)*2;
    }
    self.labelTitle.center = CGPointMake(self.width/2.0, self.height/2.0);
    if (imageName != nil) {
        UIImage * image = [UIImage imageNamed:imageName];
        self.iv.image = image;
        self.iv.width = W(image.size.width);
        self.iv.height = W(image.size.height);
        if (highImageName != nil) {
            [self.iv setHighlightedImage:[UIImage imageNamed:highImageName]];
        }
        if (rect.size.width == 0) {
            self.width +=self.iv.width + W(10);
            self.labelTitle.center = CGPointMake(self.width/2.0, self.height/2.0);
        }
        if (isImageLeft) {
            self.labelTitle.left += self.iv.width/2.0 + W(10)/2.0;
            self.iv.left = self.labelTitle.left - self.iv.width - W(10);
        } else {
            self.labelTitle.left -= self.iv.width/2.0 + W(10)/2.0;
            self.iv.left = self.labelTitle.right + W(10);
        }
        self.iv.centerY = self.height/2.0;
    }
    if (hasLineVertical) {
        [GlobalMethod addLineFrame:CGRectMake(self.width - 1, W(4), 1, self.height - W(8)) inView:self isDark:false];
    }
}

//更改title
- (void)changeTitle:(NSString *)strTitle{
    self.labelTitle.text = strTitle;
}

@end
