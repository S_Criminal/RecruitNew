//
//  BaseNavView.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavView : UIView
@property (nonatomic, strong) UILabel * labelTitle;
@property (nonatomic, strong) UIControl * backBtn;
@property (nonatomic, strong) void (^leftBlock)();
@property (nonatomic, strong) void (^rightBlock)();
@property (nonatomic ,strong) UILabel *rightLabel;

/**
 *  左返回
 */
+ (instancetype)initNavTitle:(NSString *)title
                    leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock;

+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock
                  rightTitle:(NSString *)rightTitle
                  rightBlock:(void (^)())rightBlock
;

+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock
                  rightView:(UIView *)rigthView
                  rightBlock:(void (^)())rightBlock
;






//刷新页面
+ (instancetype)initNavTitle:(NSString *)title
                    leftView:(UIView *)leftView
                   rightView:(UIView *)rigthView;

//左返回 右view
+ (instancetype)initNavBackTitle:(NSString *)title
                       rightView:(UIView *)rigthView;

//左图片 右图片
+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock
              rightImageName:(NSString *)rightImageName
                   righBlock:(void (^)())rightBlock;

//返回 右图片
+ (instancetype)initNavBackWithTitle:(NSString *)title
                      rightImageName:(NSString *)rightImageName
                           righBlock:(void (^)())rightBlock;
//返回 右文字
+ (instancetype)initNavBackTitle:(NSString *)title
                      rightTitle:(NSString *)rightTitle
                      rightBlock:(void (^)())rightBlock;

//更改title
- (void)changeTitle:(NSString *)title;


@end
