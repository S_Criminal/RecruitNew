//
//  BaseNavView.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/24.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "BaseNavView.h"

@implementation BaseNavView

//初始化
- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.frame = CGRectMake(0, 0, KWIDTH, NAVIGATIONBAR_HEIGHT);
    }
    return self;
}

//懒加载
- (UILabel *)labelTitle{
    if (!_labelTitle) {
        _labelTitle = [UILabel new];
        [GlobalMethod setLabel:_labelTitle widthLimit:KWIDTH - 100 numLines:1 fontNum:17 textColor:COLOR_LABELCOLOR text:@""];
        [GlobalMethods setLabel:_labelTitle widthLimit:KWIDTH numLines:1 fontNum:17 textColor:COLOR_LABELCOLOR text:@""];
    }
    return _labelTitle;
}

- (UIControl *)backBtn{
    if (!_backBtn) {
        _backBtn = [UIControl new];
        [_backBtn addTarget:self action:@selector(btnBackClick) forControlEvents:UIControlEventTouchUpInside];
        [self resetControl:_backBtn imageName:@"left" isLeft:true];
    }
    return _backBtn;
}

#pragma mark 类方法

//左返回

+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock{
    BaseNavView * baseNav = [BaseNavView new];
    NSString *imageName = leftImageName;
    if ([leftImageName isEqualToString:@"no"]) {
        imageName = nil;
    }
    if ([leftImageName isEqualToString:@""]) {
        imageName = @"left";
    }
    [baseNav resetNavTitle:title leftImageName:imageName leftBlock:leftBlock];
    return baseNav;
}

//左返回 右文字


+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock
                  rightTitle:(NSString *)rightTitle
                  rightBlock:(void (^)())rightBlock
{
    BaseNavView * baseNav = [BaseNavView new];
    NSString *imageName = @"left";
    if ([leftImageName isEqualToString:@"no"]) {
        imageName = nil;
    }
    [baseNav resetNavTitle:title leftImageName:imageName leftBlock:leftBlock];
    [baseNav initNavBackTitle:title rightTitle:rightTitle rightBlock:rightBlock];
    return baseNav;
}





//刷新页面
+ (instancetype)initNavTitle:(NSString *)title
                    leftView:(UIView *)leftView
                   rightView:(UIView *)rigthView{
    BaseNavView * baseNav = [BaseNavView new];
    [baseNav resetNavTitle:title leftView:leftView rightView:rigthView];
    return baseNav;
}

//左返回 右view
+ (instancetype)initNavBackTitle:(NSString *)title
                       rightView:(UIView *)rigthView{
    BaseNavView * baseNav = [BaseNavView new];
    [baseNav resetNavBackTitle:title rightView:rigthView];
    return baseNav;
}

//左图片 右图片
+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock
              rightImageName:(NSString *)rightImageName
                   righBlock:(void (^)())rightBlock{
    BaseNavView * baseNav = [BaseNavView new];
    [baseNav resetNavTitle:title leftImageName:leftImageName leftBlock:leftBlock rightImageName:rightImageName righBlock:rightBlock];
    return baseNav;
}
//右view
+ (instancetype)initNavTitle:(NSString *)title
               leftImageName:(NSString *)leftImageName
                   leftBlock:(void (^)())leftBlock
                   rightView:(UIView *)rigthView
                  rightBlock:(void (^)())rightBlock
{
    BaseNavView * baseNav = [BaseNavView new];
    [baseNav resetNavTitle:title leftImageName:leftImageName leftBlock:leftBlock rightImageName:@"" righBlock:rightBlock];
    [baseNav resetNavBackTitle:title rightView:rigthView];
    return baseNav;
}


//左返回 右图片
+ (instancetype)initNavBackWithTitle:(NSString *)title
                      rightImageName:(NSString *)rightImageName
                           righBlock:(void (^)())rightBlock{
    BaseNavView * baseNav = [BaseNavView new];
    [baseNav resetNavBackWithTitle:title rightImageName:rightImageName righBlock:rightBlock];
    return baseNav;
}
//返回 右文字
+ (instancetype)initNavBackTitle:(NSString *)title
                      rightTitle:(NSString *)rightTitle
                      rightBlock:(void (^)())rightBlock{
    BaseNavView * baseNav = [BaseNavView new];
    [baseNav initNavBackTitle:title rightTitle:rightTitle rightBlock:rightBlock];
    
    return baseNav;
}



//返回 右文字
- (void)initNavBackTitle:(NSString *)title
              rightTitle:(NSString *)rightTitle
              rightBlock:(void (^)())rightBlock{
    self.rightBlock = rightBlock;
    if (rightTitle == nil){
        [self resetNavTitle:title leftView:self.backBtn rightView:nil];
        return;
    }
    UIControl * con = [UIControl new];
    [self resetControl:con title:rightTitle isLeft:false];
    [con addTarget:self action:@selector(btnRightClick) forControlEvents:UIControlEventTouchUpInside];
    [self resetNavTitle:title leftView:nil rightView:con];
}

//刷新页面 左view  右view
- (void)resetNavTitle:(NSString *)title
             leftView:(UIView *)leftView
            rightView:(UIView *)rigthView{
    //set title
    [GlobalMethod resetLabel:self.labelTitle text:title isWidthLimit:true];
    self.labelTitle.centerX = self.width/2.0;
    self.labelTitle.centerY = (NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT)/2.0 + STATUSBAR_HEIGHT;
    [self addSubview:self.labelTitle];
    //left view
    if (leftView != nil) {
        leftView.frame = CGRectMake(0, STATUSBAR_HEIGHT, leftView.width, NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT);
        [self addSubview:leftView];
    }
    //right view
    if (rigthView != nil) {
        rigthView.frame = CGRectMake(KWIDTH - rigthView.width, STATUSBAR_HEIGHT , rigthView.width, NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT);
        [self addSubview:rigthView];
    }
}

//左返回 右view
- (void)resetNavBackTitle:(NSString *)title
                rightView:(UIView *)rigthView{
    [self resetNavTitle:title leftView:self.backBtn rightView:rigthView];
}

//左返回
- (void)resetNavTitle:(NSString *)title
        leftImageName:(NSString *)leftImageName
            leftBlock:(void (^)())leftBlock
       {
    UIView * leftView = nil;
    if (leftImageName != nil) {
        self.leftBlock = leftBlock;
        UIControl * con = [UIControl new];
        [self resetControl:con imageName:leftImageName isLeft:true];
        [con addTarget:self action:@selector(btnLeftClick) forControlEvents:UIControlEventTouchUpInside];
        leftView = con;
    }
    [self resetNavTitle:title leftView:leftView rightView:nil];
}

//左图片 右图片
- (void)resetNavTitle:(NSString *)title
        leftImageName:(NSString *)leftImageName
            leftBlock:(void (^)())leftBlock
       rightImageName:(NSString *)rightImageName
            righBlock:(void (^)())rightBlock{
    UIView * leftView = nil;
    if (leftImageName != nil) {
        self.leftBlock = leftBlock;
        UIControl * con = [UIControl new];
        [self resetControl:con imageName:leftImageName isLeft:true];
        [con addTarget:self action:@selector(btnLeftClick) forControlEvents:UIControlEventTouchUpInside];
        leftView = con;
    }
    UIView * rightView = nil;
    if (rightImageName != nil) {
        self.rightBlock = rightBlock;
        UIControl * con = [UIControl new];
        [self resetControl:con imageName:rightImageName isLeft:false];
        [con addTarget:self action:@selector(btnRightClick) forControlEvents:UIControlEventTouchUpInside];
        rightView = con;
    }
    [self resetNavTitle:title leftView:leftView rightView:rightView];
}
//返回 右图片
- (void)resetNavBackWithTitle:(NSString *)title
               rightImageName:(NSString *)rightImageName
                    righBlock:(void (^)())rightBlock{
    self.rightBlock = rightBlock;
    UIControl * con = [UIControl new];
    [self resetControl:con imageName:rightImageName isLeft:false];
    [con addTarget:self action:@selector(btnRightClick) forControlEvents:UIControlEventTouchUpInside];
    [self resetNavTitle:title leftView:self.backBtn rightView:con];
}



//更改title
- (void)changeTitle:(NSString *)title{
    [GlobalMethod resetLabel:self.labelTitle text:title isWidthLimit:false];
    self.labelTitle.centerX = self.width/2.0;
}

#pragma mark 点击事件
- (void)btnBackClick{
    
    [GB_Nav popViewControllerAnimated:true];
}
- (void)btnRightClick{
    if (self.rightBlock != nil) {
        self.rightBlock();
    }
}
- (void)btnLeftClick{
    if (self.leftBlock != nil) {
        self.leftBlock();
    }
}


#pragma mark 通用方法
- (void)resetControl:(UIControl*) control
           imageName:(NSString *) imageName
              isLeft:(BOOL)isLeft{
    control.backgroundColor = [UIColor clearColor];
    
    UIImageView * iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    iv.backgroundColor = [UIColor clearColor];
    if (isLeft) {
        control.frame = CGRectMake(0, STATUSBAR_HEIGHT, 50, NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT);
        iv.left = 20;
    } else {
        control.frame = CGRectMake(KWIDTH - 50, STATUSBAR_HEIGHT, 50, NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT);
        iv.right = control.width - 20;
    }
    iv.centerY = control.height/2.0;
    [control addSubview: iv];
    
}

- (void)resetControl:(UIControl*) control
               title:(NSString *) title
              isLeft:(BOOL)isLeft{
    control.backgroundColor = [UIColor clearColor];
    
    UILabel * label = [UILabel new];
    self.rightLabel = label;
    [GlobalMethod setLabel:label widthLimit:0 numLines:1 fontNum:15 textColor:COLOR_MAINCOLOR text:title];
    label.backgroundColor = [UIColor clearColor];
    if (isLeft) {
        control.frame = CGRectMake(0, STATUSBAR_HEIGHT, 50, NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT);
        label.left =20;
    } else {
        control.frame = CGRectMake(KWIDTH - 50, STATUSBAR_HEIGHT, 50, NAVIGATIONBAR_HEIGHT - STATUSBAR_HEIGHT);
        label.right = control.width - 20;
    }
    label.centerY = control.height/2.0;
    [control addSubview: label];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
