//
//  WMImageViewCell.m
//  WMPageController
//
//  Created by Mark on 15/6/14.
//  Copyright (c) 2015年 yq. All rights reserved.
//

#import "WMImageViewCell.h"
//#import "YGSortsM.h"
//#import "YGFreshM.h"
@interface WMImageViewCell ()
@property (nonatomic, strong) UIView *divide;
@property (nonatomic, strong) UILabel *bottoml;
@end

static NSString * const reuseIdentifier = @"WMCollectionCell";

@implementation WMImageViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUpSubview];
    }
    return self;
}

+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:reuseIdentifier];
    id cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    return cell;
    
}

- (void)setUpSubview{
    
    // image
    UIImageView *imageView = [[UIImageView alloc] init];
    [self addSubview:imageView];
    _imageV = imageView;
    
    // bottom
    UIView *bottom = [[UIView alloc]init];
    bottom.backgroundColor = [UIColor whiteColor];
    [self addSubview:bottom];
    _bottom = bottom;
    
    //  centerl label
    UILabel *centerl = [[UILabel alloc]init];
    centerl.font = [UIFont systemFontOfSize:14];
    centerl.alpha = 0.7;
    centerl.hidden = YES;
    [self.bottom addSubview:centerl];
    _centerl = centerl;
    
    // center btn
    self.centerB = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.centerB setBackgroundImage:[UIImage imageNamed:@"btn_background"] forState:UIControlStateNormal];
    [self.centerB addTarget:self action:@selector(seeDetail) forControlEvents:UIControlEventTouchUpInside];
    self.centerB.hidden = YES;
    self.centerB.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.centerB setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.centerB setTitle:@"立即查看" forState:UIControlStateNormal];
    [self.bottom addSubview:self.centerB];
    
    // divide
    self.divide = [[UIView alloc]init];
    self.divide.backgroundColor = [UIColor blackColor];
    [self.bottom addSubview:self.divide];
    
    // bottoml
    self.bottoml = [[UILabel alloc]init];
    self.bottoml.font = [UIFont systemFontOfSize:14];
//    self.bottoml.textColor = GredColor;
    self.bottoml.hidden = YES;
    [self.bottom addSubview:self.bottoml];
    
    
}

- (void)seeDetail{
    
    if ([self.delegate respondsToSelector:@selector(imageViewCell:model:)]) {
        [self.delegate imageViewCell:self model:self.modelS];
    }
    
}

- (void)setModelS:(YGSortsM *)modelS{
    
    _modelS = modelS;
//    [self.imageV sd_setImageWithURL:[NSURL URLWithString:modelS.cat_img] placeholderImage:[UIImage imageNamed:@"timeline_image_placeholder"]];
//    self.centerl.text = modelS.cat_name;
    self.centerl.hidden = NO;
    [self.centerl sizeToFit];
    self.centerB.hidden = NO;
    [self setUpFrame];
    
}

- (void)setModelF:(YGFreshM *)modelF{
    
    _modelF = modelF;
//    [self.imageV sd_setImageWithURL:[NSURL URLWithString:modelF.original_img] placeholderImage:[UIImage imageNamed:@"timeline_image_placeholder"]];
//    [self.imageV sd_setImageWithURL:[NSURL URLWithString:modelF.tgoods_img] placeholderImage:[UIImage imageNamed:@"timeline_image_placeholder"]];

//    self.centerl.text = modelF.goods_name;
    self.centerl.hidden = NO;
    [self.centerl sizeToFit];
    
//    self.bottoml.text = [NSString stringWithFormat:@"RMB %@",modelF.shop_price];
    self.bottoml.hidden = NO;
    [self.bottoml sizeToFit];
    
    self.divide.hidden = YES;
    [self setUpFrame];
    
}

- (void)setUpFrame {
//    [super layoutSubviews];
    self.imageV.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - 50);

    // bottom
    CGFloat bottomY = self.bounds.size.height - 50;
    CGFloat bottomH = 50;
    CGFloat bottomW = self.bounds.size.width;
    self.bottom.frame = CGRectMake(0, bottomY, bottomW, bottomH);
    
    // centerl
    self.centerl.frame = CGRectMake((self.bounds.size.width - self.centerl.bounds.size.width ) * 0.5, 5, self.centerl.bounds.size.width, self.centerl.bounds.size.height);
    if (self.centerl.frame.size.width >= bottomW) {
        self.centerl.frame = CGRectMake((self.bounds.size.width - bottomW + 40) * 0.5, 5, bottomW - 40, self.centerl.bounds.size.height);
    }
    
    // center btn
    self.centerB.frame = CGRectMake((self.bounds.size.width - 80) * 0.5, 5 + CGRectGetMaxY(self.centerl.frame), 80, 20);
    
    // bottoml
    self.bottoml.frame = CGRectMake((self.bounds.size.width - self.bottoml.frame.size.width) * 0.5, CGRectGetMaxY(self.centerl.frame), self.bottoml.frame.size.width, self.bottoml.frame.size.height);
    
    // divide
    CGFloat divideY = CGRectGetMidY(self.bottoml.frame) - 1;
    self.divide.frame = CGRectMake(CGRectGetMinX(self.bottoml.frame), divideY, self.bottoml.bounds.size.width, 1);

    
}

@end
