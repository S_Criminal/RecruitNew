//
//  WMImageViewCell.h
//  WMPageController
//
//  Created by Mark on 15/6/14.
//  Copyright (c) 2015年 yq. All rights reserved.
//  分类展示界面cell

#import <UIKit/UIKit.h>
@class WMImageViewCell;
@class YGSortsM;
@class YGFreshM;

@protocol WMImageViewCellDelegate <NSObject>

- (void)imageViewCell:(WMImageViewCell *)imageCell model:(YGSortsM *)modelS;

@end

@interface WMImageViewCell : UICollectionViewCell

/**
 *  modelS
 */

@property (nonatomic, strong) YGSortsM *modelS;

/**
 *  modelF
 */

@property (nonatomic, strong) YGFreshM *modelF;

/**
 *  imageV
 */
@property (nonatomic, weak) UIImageView *imageV;

/**
 *  底部白条
 */
@property (nonatomic, strong) UIView *bottom;

/**
 *  center label
 */
@property (nonatomic, strong) UILabel *centerl;

/**
 *  center btn
 */
@property (nonatomic, strong) UIButton *centerB;


+ (instancetype)collectionWithCollectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, weak)id<WMImageViewCellDelegate>delegate;

@end
