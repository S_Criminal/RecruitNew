//
//  CustomShareUI.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/15.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {          //朋友圈和微信好友与其他不一样
    shareContentsWithCompany    = 1 << 0,
    shareContentsWithPosition   = 1 << 1,
    shareContentsWithSelfBrief  = 1 << 2,
    shareContentsWithQRCode     = 1 << 3,
} ShareEnum;

@interface CustomShareUI : UIControl

-(instancetype)initShareUIContent:(NSString *)content title:(NSString *)title url:(NSURL *)url images:(id)images shareEnum:(ShareEnum )shareEnum;

@property (nonatomic ,strong) NSString *contents;
@property (nonatomic ,strong) NSString *titles;
@property (nonatomic ,strong) NSURL *urls;
@property (nonatomic ,strong) id images;
@property (nonatomic ,assign) ShareEnum shareEnum;
@property (nonatomic ,strong) NSString *companyName;
@property (nonatomic ,strong) NSString *positionName;
@property (nonatomic ,strong) NSString *p_Pay;

@property (nonatomic ,strong) void(^shareBlock)(NSInteger i);

-(void)shareWithContent;

@end

