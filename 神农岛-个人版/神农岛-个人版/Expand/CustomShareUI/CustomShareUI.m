//
//  CustomShareUI.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/2/15.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "CustomShareUI.h"

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDKUI.h>
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>

#import "UIImage+ChangeSize.h"

@interface CustomShareUI ()<UIScrollViewDelegate>
@property (nonatomic ,strong) id publishContent;
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,assign) NSInteger shareType;

@end

@implementation CustomShareUI

-(UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        _scrollView.delegate = self;
        _scrollView.backgroundColor = [UIColor whiteColor];
    }
    return _scrollView;
}


//images

-(instancetype)initShareUIContent:(NSString *)content title:(NSString *)title url:(NSURL *)url images:(id)images shareEnum:(ShareEnum )shareEnum
{
    CustomShareUI *share = [CustomShareUI new];
    
    share.contents = content;
    share.images = images;
    share.titles = title;
    share.urls   = url;
    share.shareEnum = shareEnum;
    [share shareUpView];
    return share;
}


-(void)shareUpView/*只需要在分享按钮事件中 构建好分享内容publishContent传过来就好了*/
{
    
//    CustomShareUI *share = [CustomShareUI new];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
    blackView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    blackView.tag = 440;
    [window addSubview:blackView];
    
//    UIView *shareView = [[UIView alloc] initWithFrame:CGRectMake((KWIDTH - W(300))/2.0f, (KHEIGHT - W(270))/2.0f, W(300), W(270))];
    UIView *shareView = [[UIView alloc]initWithFrame:CGRectMake(0, KHEIGHT - W(155), KWIDTH, W(155))];
    shareView.backgroundColor = [HexStringColor colorWithHexString:@"f6f6f6"];
    shareView.tag = 441;
    [window addSubview:shareView];
    
    NSArray *btnImages = @[@"wechat_friend", @"circle_friends", @"qq_friend", @"qq_qzone", @"wechat_collection"];
    NSArray *btnTitles = @[@"微信好友", @"微信朋友圈", @"QQ好友", @"QQ空间", @"微信收藏"];
    
    [self.scrollView setFrame:CGRectMake(0, 0, KWIDTH, shareView.height - W(45))];
    [shareView addSubview:self.scrollView];
    
    
    
    
    for (NSInteger i = 0; i < btnImages.count; i++)
    {
        CGFloat buttonWidth  = (KWIDTH ) / 5;
        CGFloat buttonHeight = W(40);
        
//        CGFloat left  = i == 0 ? W(20) : 0;
//        buttonWidth   = i == 1 ? buttonWidth + W(20) : buttonWidth;
        UIControl *control = [[UIControl alloc]initWithFrame:CGRectMake( buttonWidth * i, W(20), buttonWidth, buttonHeight)];
        UIImageView *imageView = [UIImageView new];
        imageView.image = [UIImage imageNamed:btnImages[i]];
        [imageView setFrame:CGRectMake(0, 0, W(45), W(45))];
        imageView.centerX = buttonWidth / 2;
        UILabel *label = [UILabel new];
        [GlobalMethod setLabel:label widthLimit:0 numLines:0 fontNum:F(11) textColor:COLOR_LABELSIXCOLOR text:btnTitles[i]];
        [control addSubview:imageView];
        [control addSubview:label];
        
        [GlobalMethod resetLabel:label text:btnTitles[i] isWidthLimit:0];
        label.leftTop = XY(0, imageView.bottom + W(10));
        label.centerX = imageView.centerX;
        
        control.tag = 331+i;
        
        [control addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:control];
    }
    
    UIButton *cancleBtn = [[UIButton alloc] initWithFrame:CGRectMake((shareView.width - W(45))/2.0f, shareView.height - W(45) , W(45), W(45))];
    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:[HexStringColor colorWithHexString:@"FF0600"] forState:UIControlStateNormal];
    cancleBtn.titleLabel.font = [UIFont systemFontOfSize:F(15)];
    cancleBtn.tag = 300;
    [cancleBtn addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:cancleBtn];
    
    //为了弹窗不那么生硬，这里加了个简单的动画
    shareView.transform = CGAffineTransformMakeScale(1 / 300.0f, 1 / 270.0f);
    blackView.alpha = 0;
    [UIView animateWithDuration:0.35f animations:^{
        shareView.transform = CGAffineTransformMakeScale(1, 1);
        blackView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHidden)];
    [blackView addGestureRecognizer:tap];
}

-(void)tapHidden
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIView *blackView = [window viewWithTag:440];
    UIView *shareView = [window viewWithTag:441];
    
    shareView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        shareView.transform = CGAffineTransformMakeScale( 1 / 300.0f, 1 / 270.0f);
        blackView.alpha = 0;
    } completion:^(BOOL finished) {
        [shareView removeFromSuperview];
        [blackView removeFromSuperview];
    }];
}

/**
 *  微信好友
 *  SSDKPlatformSubTypeWechatSession    = 22,
 */

/**
 *  微信朋友圈
 *  SSDKPlatformSubTypeWechatTimeline   = 23,
 */

/**
 *  QQ好友
 *  SSDKPlatformSubTypeQQFriend         = 24,
 */

- (void)shareBtnClick:(UIButton *)btn
{
    //    NSLog(@"%@",[ShareSDK version]);
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIView *blackView = [window viewWithTag:440];
    UIView *shareView = [window viewWithTag:441];
    
    //为了弹窗不那么生硬，这里加了个简单的动画
    shareView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView animateWithDuration:0.35f animations:^{
        shareView.transform = CGAffineTransformMakeScale( 1 / 300.0f, 1 / 270.0f);
        blackView.alpha = 0;
    } completion:^(BOOL finished) {
        
        [shareView removeFromSuperview];
        [blackView removeFromSuperview];
    }];
    
    self.shareType = btn.tag;
//    if (self.shareBlock) {
//        self.shareBlock(btn.tag);
//    }
    
    
    [self shareWithContent];
    
    
}

-(void)shareWithContent
{
    
    if (self.shareType == 300)
    {
        return;
    }
    
    switch (self.shareType) {
        
        case 331:
        {
            _shareType = SSDKPlatformSubTypeWechatSession;   //微信好友
            if (self.shareEnum == shareContentsWithCompany){
                self.contents = @"我们招兵买马了！高薪诚聘！赶快加入我们！-老刀招聘APP-农业人才-专业招聘";
            }else if (self.shareEnum == shareContentsWithPosition) {
                self.contents = [NSString stringWithFormat:@"%@·高薪诚聘·%@-老刀招聘APP-农业人才-专业招聘",self.p_Pay,self.positionName];
            }
            
        }
            break;
            
        case 332:
        {
            _shareType = SSDKPlatformSubTypeWechatTimeline;  //微信朋友圈
            if (self.shareEnum == shareContentsWithCompany)
            {
                self.contents   = [NSString stringWithFormat:@"%@要招兵买马了！-老刀招聘APP",self.companyName];
                self.titles     = self.contents;
            }else if (self.shareEnum == shareContentsWithPosition) {
                self.titles = self.contents;
            }
        }
            break;
            
        case 333:
        {
            _shareType = SSDKPlatformSubTypeQQFriend;
        }
            break;
            
        case 334:
        {
            _shareType = SSDKPlatformSubTypeQZone;       //QQ空间
        }
            break;
            
        case 335:
        {
            _shareType = SSDKPlatformSubTypeWechatFav;   //微信收藏
        }
            break;
            
        case 336:
        {
            //shareType = ShareTypeSinaWeibo;
        }
            break;
            
        case 337:
        {
            //shareType = ShareTypeDouBan;
        }
            break;
            
        case 338:
        {
            //shareType = ShareTypeSMS;
        }
            break;
            
        case 339:
        {
            
        }
            break;
            
        default:
            break;
    }
    
    
    UIImage* image;
    
    
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    
    if ([self.images isKindOfClass:[NSString class]]) {
        self.images = [NSString stringWithFormat:@"%@",self.images];
        NSURL *URL = [NSURL URLWithString:self.images];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
        NSString* pathExtendsion = [URL.pathExtension lowercaseString];
        
        CGSize size = CGSizeZero;
        
        
        
        if([pathExtendsion isEqualToString:@"png"])
        {
            size =  [self getPNGImageSizeWithRequest:request];
            self.images = [NSString stringWithFormat:@"%@",self.images];
        }
        
        if(CGSizeEqualToSize(CGSizeZero, size))                    // 如果获取文件头信息失败,发送异步请求请求原图
        {
            NSData* data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:URL] returningResponse:nil error:nil];
            //        data = [SSDKImage checkThumbImageSize:data];
            image = [UIImage imageWithData:data];
            if(image)
            {
                size = image.size;
            }
            if (size.width > size.height)       image = [image transformWidth:100 height:100];
            else if (size.height > size.width)  image = [image transformWidth:100 height:100];
        }
    }
    
    id shareImages;
    if (image == nil) {
        shareImages = self.images;
    }else{
        shareImages = image;
    }
    
    if (self.shareEnum != shareContentsWithQRCode) {
        [shareParams SSDKSetupShareParamsByText:self.contents images:shareImages url:self.urls title:self.titles type:SSDKContentTypeAuto];
    }else{
        [shareParams SSDKSetupShareParamsByText:self.contents images:shareImages url:self.urls title:self.titles type:SSDKContentTypeImage];
    }
    
    [ShareSDK share:self.shareType //传入分享的平台类型
         parameters:shareParams
     onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) { // 回调处理....}];
         if (state == SSDKResponseStateSuccess)
         {
             NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
         }
         else if (state == SSDKResponseStateFail)
         {
             NSLog(@"%@",error.userInfo[@"error_message"]);
             [MBProgressHUD showError:error.userInfo[@"error_message"] toView:self.superview.superview.superview];
         }
     }];
}



//  获取PNG图片的大小
-(CGSize)getPNGImageSizeWithRequest:(NSMutableURLRequest*)request
{
    [request setValue:@"bytes=16-23" forHTTPHeaderField:@"Range"];
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if(data.length == 8)
    {
        int w1 = 0, w2 = 0, w3 = 0, w4 = 0;
        [data getBytes:&w1 range:NSMakeRange(0, 1)];
        [data getBytes:&w2 range:NSMakeRange(1, 1)];
        [data getBytes:&w3 range:NSMakeRange(2, 1)];
        [data getBytes:&w4 range:NSMakeRange(3, 1)];
        int w = (w1 << 24) + (w2 << 16) + (w3 << 8) + w4;
        int h1 = 0, h2 = 0, h3 = 0, h4 = 0;
        [data getBytes:&h1 range:NSMakeRange(4, 1)];
        [data getBytes:&h2 range:NSMakeRange(5, 1)];
        [data getBytes:&h3 range:NSMakeRange(6, 1)];
        [data getBytes:&h4 range:NSMakeRange(7, 1)];
        int h = (h1 << 24) + (h2 << 16) + (h3 << 8) + h4;
        return CGSizeMake(w, h);
    }
    return CGSizeZero;
}



@end
