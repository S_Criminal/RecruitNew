//
//  LeftAndRightLaelCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalLeftModel.h"

typedef enum {
    ENUM_Status_RightLabel,             //右边是   label
    ENUM_Status_RightTextField,         //        textField
    ENUM_Status_NoViews                 //        没控件
} ENUM_Status_ActionType;

@interface LeftAndRightLaelCell : UITableViewCell
@property (nonatomic ,strong) UILabel *leftLabel;
@property (nonatomic ,strong) UILabel *rightLabel;
@property (nonatomic ,strong) GlobalLeftModel *model;
@property (nonatomic ,assign) CGFloat cellHeight;

@property (nonatomic ,assign) ENUM_Status_ActionType InActionType; //操作类型

-(void)setStyle:(int)style leftContent:(NSString *)leftContent rightContent:(NSString *)rightContent;

-(void) resetModel:(NSString *)left right:(NSString *)right;

@end
