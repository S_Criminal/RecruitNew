//
//  LeftAndRightLaelCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 16/12/26.
//  Copyright © 2016年 Light. All rights reserved.
//

#import "LeftAndRightLaelCell.h"

@implementation LeftAndRightLaelCell{
    CGFloat viewLeftConstraint;
    CGFloat leftFont;
}

-(UILabel *)rightLabel
{
    if (!_rightLabel) {
        _rightLabel     = [UILabel new];
        _rightLabel.font = [UIFont systemFontOfSize:F(15)];
        _rightLabel.textAlignment    = NSTextAlignmentRight;
        _rightLabel.textColor = COLOR_MAINCOLOR;
    }
    return _rightLabel;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
//        [self createViews];
        [self createBasic];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setUI
{
    viewLeftConstraint = 15;
    _cellHeight = 50;
    if (isIphone5) {
        viewLeftConstraint = 12;
        _cellHeight = 44;
    }else if (isIphone6){
        
    }else if (isIphone6p){
        
    }
}

-(void)createBasic
{
    _leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(viewLeftConstraint, 0, KWIDTH, _cellHeight)];
    _leftLabel.font = [UIFont systemFontOfSize:F(16)];
    
    [self.contentView addSubview:_leftLabel];
    [self.contentView addSubview:self.rightLabel];
}

-(void)createViews:(ENUM_Status_ActionType)InActionType leftContent:(NSString *)leftContent rightContent:(NSString *)rightContent
{
    _leftLabel.text = leftContent;
    [_leftLabel sizeToFit];
    _leftLabel.height = _cellHeight;
    _leftLabel.centerY = _cellHeight / 2.0f;
    
    self.rightLabel.text     = rightContent;
    self.rightLabel.textAlignment = NSTextAlignmentRight;
    [self.rightLabel sizeToFit];
    self.rightLabel.x = KWIDTH - 15 - self.rightLabel.width;
    self.rightLabel.centerY = _cellHeight / 2.0f;
}


-(void)setStyle:(int)style leftContent:(NSString *)leftContent rightContent:(NSString *)rightContent;
{
    switch (style) {
        case 0:
            [self createViews:style leftContent:leftContent rightContent:rightContent];
            break;
        case 1:
            break;
        case 2:
            
            
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
