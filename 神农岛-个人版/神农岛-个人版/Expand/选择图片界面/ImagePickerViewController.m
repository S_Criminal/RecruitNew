//
//  ImagePickerViewController.m
//  DaGeng
//
//  Created by fu on 15/5/14.
//  Copyright (c) 2015年 fu. All rights reserved.
//

#import "ImagePickerViewController.h"

#import "FLImageShowVC.h"
#import "TitleImageAssetCell.h"
#import <AVFoundation/AVFoundation.h>
#define Cellidentifier @"cell"
#define NewWIDTH(a) [UIScreen mainScreen].bounds.size.width / 320 * a
#define SCREENWIDTH [UIScreen mainScreen].bounds.size.width
#define SCREENHEIGHT [UIScreen mainScreen].bounds.size.height

@interface ImagePickerViewController ()<UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    ALAssetsLibrary *_assetsLibrary;
    NSMutableArray *_groupArray;
    UILabel *_titleLabel;
    UITableView *_titleTabelView;
    UIView *_titleTableBackgroundView;
    UIImageView *_titleViewImageView;
    UIView *_titleView;
}

@end

@implementation ImagePickerViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.myCollectionView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)SelectedPhoto:(NSNotification *)text
{
    NSInteger  photoIndex = [text.userInfo[@"selectedPhotoIndex"] integerValue];
    NSString * buttonSelect = text.userInfo[@"yesOrNoSelectButton"];
    
    NSLog(@" --- 按钮是否选中 --- %@",buttonSelect);
    NSLog(@" --- 选择第几张图片 --- %ld",photoIndex);
    NSLog(@"－－－－－接收到通知------");
    
    ALAsset *asset = _imageAssetAray[photoIndex];
    
    if ([buttonSelect isEqualToString:@"yes"])
    {
        [_selectAssetArray addObject:asset];
        [_selectIndexArray addObject:[NSString stringWithFormat:@"%ld",photoIndex+1]];
    }
    else
    {
        [_selectAssetArray removeObject:asset];
        [_selectIndexArray removeObject:[NSString stringWithFormat:@"%ld",photoIndex+1]];
    }
    
    if (_selectAssetArray.count == 0)
    {
        [_okBtn setTitle:@"确定" forState:UIControlStateNormal];
    }
    else
    {
        [_okBtn setTitle:[NSString stringWithFormat:@"确定(%lu)",(unsigned long)[_selectAssetArray count]] forState:UIControlStateNormal];
    }
    NSLog(@"%@",_selectIndexArray);
    NSLog(@"%@",_selectAssetArray);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectedPhoto:) name:@"SelectedPhoto" object:nil];
    
    _okBtn.layer.cornerRadius = 15;
    _bottomView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
    
    _selectAssetArray = [NSMutableArray array];
    _thumbnailArray = [NSMutableArray array];
    
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"MyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:Cellidentifier];
    _imageAssetAray = [NSMutableArray array];
    _imageUrlArray = [NSMutableArray array];
    _selectIndexArray = [NSMutableArray array];
    _myCollectionView.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(NewWIDTH(92), NewWIDTH(92));
    layout.minimumLineSpacing = NewWIDTH(11);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.sectionInset = UIEdgeInsetsMake(NewWIDTH(11), NewWIDTH(11), NewWIDTH(11), NewWIDTH(11));
    self.myCollectionView.collectionViewLayout = layout;
    
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    
    [self createTitleTableView];
    [self getAlbumList];
    
    //导航栏自定义 ico_arrow_left
    [self leftBarButtonItem:@"  " image:[UIImage imageNamed:@"left"] action:@selector(back)];
    
    _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    _titleLabel.textColor = COLOR_MAINCOLOR;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_titleView addSubview:_titleLabel];
    _titleViewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_titleLabel.frame.size.width + _titleLabel.frame.origin.x + 14, 17, 9, 5)];
    _titleViewImageView.image = [UIImage imageNamed:@"ico_向下箭头"];
    _titleViewImageView.backgroundColor = [UIColor clearColor];
    [_titleView addSubview:_titleViewImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(titleTapAction:)];
    [_titleView addGestureRecognizer:tap];
//    _titleView.backgroundColor = [UIColor whiteColor];
    self.navigationItem.titleView = _titleView;
//    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    [self.okBtn setCorner:self.okBtn.height / 2.0f];
    
}


//获取相册列表
- (void)getAlbumList
{
    //获取相册列表
    _assetsLibrary = [[ALAssetsLibrary alloc] init];
    _groupArray = [NSMutableArray array];
    [_assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop)
     {
         
         if (group)
         {
             [_groupArray addObject:group];
             [_titleTabelView reloadData];
             
             NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
             _titleLabel.text = groupName;
             [_titleLabel sizeToFit];
             _titleLabel.center = CGPointMake(_titleView.frame.size.width / 2, 22);
             
             _titleViewImageView.center = CGPointMake(_titleLabel.frame.origin.x + _titleLabel.frame.size.width + _titleViewImageView.frame.size.width / 2 + 8, 44 / 2);
             NSInteger i = _groupArray.count;
             if (_groupArray.count > 6) i = 6;      //高度不能太高
             _titleTabelView.height = W(50) * i;    //高度根据胶卷个数设置
         }
         else
         {
             ALAssetsGroup *group = [_groupArray lastObject];
             NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
             [self getImageWithGroup:group name:groupName];
         }
     } failureBlock:^(NSError *error)
     {
         NSLog(@"error:%@",error.localizedDescription);
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"请在iPhone的\"设置-隐私-照片\"选项中，允许老刀招聘访问您的相片" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"设置",nil];
         alert.delegate = self;
         [alert show];
     }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%ld",buttonIndex);
    if (buttonIndex == 1)
    {
         [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

//根据相册获取下面的图片
- (void)getImageWithGroup:(ALAssetsGroup *)group name:(NSString *)name
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //根据相册获取下面的图片
        NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
        if (name && ![name isEqualToString:groupName])
        {
            return;
        }
        
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            
            if (result) {
                
                [_imageAssetAray addObject:result];
                
                [_thumbnailArray addObject:[UIImage imageWithCGImage:result.thumbnail]];
                
                ALAssetRepresentation *representation = result.defaultRepresentation;
                [_imageUrlArray addObject:representation.url];
                
                _imageAssetAray = [self reverseArray:_imageAssetAray];
                _thumbnailArray = [self reverseArray:_thumbnailArray];
                _imageUrlArray  = [self reverseArray:_imageUrlArray];
                
            }
            if (index == group.numberOfAssets - 1)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_myCollectionView reloadData];
                });
            }
        }];
    });
}

-(NSMutableArray *)reverseArray:(NSMutableArray *)array
{
    NSArray *arr = array;
    NSArray *reversedArray = [[arr reverseObjectEnumerator] allObjects];
    array = [NSMutableArray arrayWithArray:reversedArray];
    return array;
}

#pragma mark--UICollectionViewDataSource,UICollectionViewDelegate

#pragma mark <UICollectionViewDelegateFlowLayout> methods
// 设置每个View的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemWH = ([UIScreen mainScreen].bounds.size.width-10 ) / 4.0f;
    return CGSizeMake(itemWH, itemWH);
}

//cell的最小行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}

//cell的最小列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

// 设置每个图片的Margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _imageAssetAray.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:Cellidentifier forIndexPath:indexPath];
    cell.imageView.image = nil;
    cell.imageView.backgroundColor = [UIColor clearColor];
    if (_isNotShowSelectBtn)
    {
        cell.selectBtn.hidden = YES;
    }
    else
    {
        cell.selectBtn.hidden = NO;
        cell.selectBtn.selected = NO;
        cell.selectBtn.tag = indexPath.row;
        [cell.selectBtn addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (indexPath.row !=0)
    {
        ALAsset * asset = _imageAssetAray[indexPath.row - 1];
        
        //获取清晰图  （高清图 - 内存受不了fullResolutionImage）
        //ALAssetRepresentation * representation = [asset defaultRepresentation];
        //UIImage * fullResolutionImage = [UIImage imageWithCGImage:representation.fullScreenImage];
        
        //缩略图  aspectRatioThumbnail  thumbnail
        UIImage *image = [UIImage imageWithCGImage:asset.aspectRatioThumbnail];
        
        cell.imageView.image = image;
        //cell.imageView.image = fullResolutionImage;
        
        
        for (int i =0; i < _selectIndexArray.count; i++)
        {
            NSString *selectIndex = _selectIndexArray[i];
            if (indexPath.row == [selectIndex integerValue])
            {
                cell.selectBtn.selected = YES;
            }
        }
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"ico_相机"];
        cell.selectBtn.hidden = YES;
        
//        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
//        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
//        imagePicker.delegate = self;
////        imagePicker.allowsEditing = YES;
//        [self addChildViewController:imagePicker];
//        [cell.contentView addSubview:imagePicker.view];
//        cell.imageView.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 0)
    {
        
        ALAsset * asset = _imageAssetAray[indexPath.row - 1];
        if ([_delegate respondsToSelector:@selector(imagePickerViewController:everyImageClick:index:imageUrlArray:imageIndexArray:)])
        {
            [_delegate imagePickerViewController:self everyImageClick:asset index:indexPath.row - 1 imageUrlArray:_imageUrlArray imageIndexArray:self.selectIndexArray];
        }
    }
    else
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.sourceType  = UIImagePickerControllerSourceTypeCamera;
            imagePicker.delegate    = self;
            imagePicker.allowsEditing = YES;
//            [self addChildViewController:imagePicker];
//            [cell.contentView addSubview:imagePicker.view];
//            cell.imageView.hidden = YES;
            [self presentViewController:imagePicker animated:YES completion:nil];
            //下面代码放在出现拍照界面之后
            AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus != AVAuthorizationStatusAuthorized)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"请在iPhone的\"设置-隐私-相机\"选项中，允许老刀招聘访问您的相机" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"设置",nil];
                alert.delegate = self;
                [alert show];
                return;
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"亲，您的设备不支持照相机功能" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil] show];
        }
    }
}
#pragma mark 胶卷显示与隐藏
- (void)titleTapAction:(UITapGestureRecognizer *)tap
{
    CGFloat titleTabelY = _titleTableBackgroundView.frame.origin.y;
    if (titleTabelY <= - SCREENHEIGHT)
    {
        [UIView animateWithDuration:0.5 animations:^{
            _titleTableBackgroundView.frame  =CGRectMake(0, 64, _titleTableBackgroundView.frame.size.width,_titleTableBackgroundView.frame.size.height);
            //_titleViewImageView.image = [UIImage imageNamed:@"ico_向上箭头"];
            _titleViewImageView.image = [UIImage imageNamed:@"ico_向下箭头"];
        }];
        
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^{
            _titleTableBackgroundView.frame  =CGRectMake(0, - SCREENHEIGHT, _titleTableBackgroundView.frame.size.width,_titleTableBackgroundView.frame.size.height);
            _titleViewImageView.image = [UIImage imageNamed:@"ico_向上箭头"];
        }];
    }
    
}

- (void)createTitleTableView
{
    _titleTableBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, -SCREENHEIGHT, SCREENWIDTH, SCREENHEIGHT)];
    _titleTableBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    _titleTabelView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, 300) style:UITableViewStylePlain];
    _titleTabelView.delegate = self;
    _titleTabelView.dataSource  = self;
    _titleTabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_titleTabelView registerClass:[TitleImageAssetCell class] forCellReuseIdentifier:@"TitleImageAssetCell"];
    [self.view addSubview:_titleTableBackgroundView];
    [_titleTableBackgroundView addSubview:_titleTabelView];
    UITapGestureRecognizer *tapTableBackground = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(titleTapAction:)];
    tapTableBackground.cancelsTouchesInView = NO;
    tapTableBackground.delegate = self;
    [_titleTableBackgroundView addGestureRecognizer:tapTableBackground];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _groupArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return W(50);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TitleImageAssetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TitleImageAssetCell"];
    ALAssetsGroup *group = _groupArray[_groupArray.count - indexPath.row - 1];
    [cell resetCellWithModel:[NSString stringWithFormat:@"%@（%ld）",[group valueForProperty:ALAssetsGroupPropertyName],(long)group.numberOfAssets] imgStr:[UIImage imageWithCGImage:group.posterImage]];
//    if (!cell)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//
//    cell.textLabel.text = [NSString stringWithFormat:@"%@（%ld）",[group valueForProperty:ALAssetsGroupPropertyName],(long)group.numberOfAssets];
//    cell.imageView.image = [UIImage imageWithCGImage:group.posterImage];
//    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_selectAssetArray removeAllObjects];
    [_selectIndexArray removeAllObjects];
    ALAssetsGroup *group = _groupArray[_groupArray.count - indexPath.row - 1];
    _titleLabel.text = [group valueForProperty:ALAssetsGroupPropertyName];
    
    [_titleLabel sizeToFit];
    
    _titleLabel.center = CGPointMake(_titleView.frame.size.width / 2, 22);
    
    _titleViewImageView.center = CGPointMake(_titleLabel.frame.origin.x + _titleLabel.frame.size.width + _titleViewImageView.frame.size.width / 2, _titleViewImageView.center.y);
    
    [_imageAssetAray removeAllObjects];
    [_imageUrlArray removeAllObjects];
    NSString *title = [NSString stringWithFormat:@"%@",_titleLabel.text];
    [self getImageWithGroup:group name:title];
    
    [UIView animateWithDuration:0.5 animations:^{
        _titleTableBackgroundView.frame = CGRectMake(0, - SCREENHEIGHT, _titleTableBackgroundView.frame.size.width, _titleTableBackgroundView.frame.size.height);
        _titleViewImageView.image = [UIImage imageNamed:@"ico_向下箭头"];
    }];
    
}

#pragma mark tap delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    //if ([touch.view isMemberOfClass:[TitleImageAssetCell class]]) {
    //    return NO;
    //}
    NSLog(@"%@",touch.view);
    if ([touch.view isMemberOfClass:[UIView class]]) {
        return true;
    }
    return false;
}

- (void)selectBtnClick:(UIButton *)btn
{
    NSString * tagStr = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    BOOL isbool = [_selectIndexArray containsObject: tagStr];
    
    if (_selectIndexArray.count < self.photoNumber || isbool == YES)
    {
        btn.selected = !btn.selected;
        MyCollectionViewCell *cell = (MyCollectionViewCell *)[[btn superview] superview];
        NSIndexPath *indexPath = [_myCollectionView indexPathForCell:cell];
        ALAsset *asset = _imageAssetAray[indexPath.row - 1];
        
        if (btn.selected)
        {
            [_selectAssetArray addObject:asset];
            [_selectIndexArray addObject:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
        }
        else
        {
            [_selectAssetArray removeObject:asset];
            [_selectIndexArray removeObject:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
        }
        
        if (_selectAssetArray.count == 0)
        {
            [_okBtn setTitle:@"确定" forState:UIControlStateNormal];
        }
        else
        {
            [_okBtn setTitle:[NSString stringWithFormat:@"确定(%lu)",(unsigned long)[_selectAssetArray count]] forState:UIControlStateNormal];
        }
        NSLog(@"%@",_selectIndexArray);
        NSLog(@"%@",_selectAssetArray);
    }
    else
    {
        NSString * str = [NSString stringWithFormat:@"最多只能选择%ld张图片",self.photoNumber];
        NSString * cancelButtonTitle = NSLocalizedString(@"我知道了", nil);
        NSString * destructiveButtonTitle = NSLocalizedString(str, nil);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            //NSLog(@"取消");
        }];
        UIAlertAction *destructiveAction = [UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
            
        }];
        [alertController addAction:destructiveAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)leftBarButtonItem:(NSString *)titles image:(UIImage *)image action:(SEL)action
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = image;
    CGSize imgSize = image.size;
    imageView.frame = CGRectMake(0, 0, imgSize.width, imgSize.height);
    imageView.center = CGPointMake(imageView.center.x,22);
    imageView.backgroundColor = [UIColor clearColor];
    [view addSubview:imageView];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = COLOR_MAINCOLOR;
    lbl.backgroundColor = [UIColor clearColor];
    CGFloat  backLabelfont;
    
    if (KWIDTH == 320) {
        backLabelfont = 13;
    }else if (KWIDTH  == 375){
        backLabelfont = 14;
    }else if (KWIDTH == 414){
        backLabelfont = 15;
    }
    
    lbl.font = [UIFont systemFontOfSize:backLabelfont];
    lbl.text = titles;
    CGSize lblSize = [lbl sizeThatFits:lbl.frame.size];
    lbl.frame = CGRectMake(imgSize.width + 5, 0, lblSize.width, lblSize.height);
    lbl.center = CGPointMake(lbl.center.x, 22);
    [view addSubview:lbl];
    view.frame = CGRectMake(0, 0, 44, 44.0f);
    view.userInteractionEnabled = YES;
    UITapGestureRecognizer *handle = [[UITapGestureRecognizer alloc] initWithTarget:self action:action];
    [view addGestureRecognizer:handle];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
}


- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

- (IBAction)okBtnClick:(UIButton *)sender
{
    
    NSLog(@" --- 选择图片的个数 --- %ld",_selectAssetArray.count);
    
    if (_selectAssetArray.count > self.photoNumber) {
        
        NSString * str = [NSString stringWithFormat:@"只能选择%ld张图片",self.photoNumber];
        NSString * cancelButtonTitle = NSLocalizedString(@"我知道了", nil);
        NSString * destructiveButtonTitle = NSLocalizedString(str, nil);
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            //NSLog(@"取消");
        }];
        UIAlertAction *destructiveAction = [UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        
        }];
        
        [alertController addAction:destructiveAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else{
        NSLog(@"确定");
        
        if ([_delegate respondsToSelector:@selector(imagePickerViewController:finishClick:)])
        {
            [_delegate imagePickerViewController:self finishClick:_selectAssetArray];
        }
    }
}

//finish 将_selectAssetArray

#pragma mark--UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        if ([_delegate respondsToSelector:@selector(imagePickerViewController:firstImageClick:)])
        {
            [_delegate imagePickerViewController:self firstImageClick:image];
        }
    }];
}

@end
