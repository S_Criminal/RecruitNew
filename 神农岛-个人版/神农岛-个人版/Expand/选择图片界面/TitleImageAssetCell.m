//
//  TitleImageAssetCell.m
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import "TitleImageAssetCell.h"

@implementation TitleImageAssetCell

#pragma mark 懒加载

- (UILabel *)titleL{
    if (_titleL == nil) {
        _titleL = [UILabel new];
        [GlobalMethod setLabel:_titleL widthLimit:0 numLines:0 fontNum:F(15) textColor:COLOR_LABELThreeCOLOR text:@""];
    }
    return _titleL;
}

- (UIImageView *)imgView{
    if (_imgView == nil) {
        _imgView = [UIImageView new];
        _imgView.image = [UIImage imageNamed:@"zzrs_qyzz"];
        _imgView.widthHeight = XY(W(40),W(40));
    }
    return _imgView;
}


#pragma mark 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.imgView];
    }
    return self;
}

#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(NSString *)title imgStr:(UIImage *)image{
    //刷新view
    [GlobalMethod resetLabel:self.titleL text:title isWidthLimit:0];
    self.imgView.image = image;

//    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgStr] placeholderImage:[UIImage imageNamed:@""]];
    self.imgView.leftTop = XY(W(15),W(5));
    
    self.titleL.leftTop = XY(self.imgView.right + W(15),W(15));
    self.titleL.centerY = self.imgView.centerY;
    
    return self.imgView.bottom + W(5);
}

@end
