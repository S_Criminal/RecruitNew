//
//  TitleImageAssetCell.h
//  神农岛-个人版
//
//  Created by 宋晨光 on 17/3/20.
//  Copyright © 2017年 Light. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleImageAssetCell : UITableViewCell

@property (nonatomic ,strong) UILabel *titleL;
@property (nonatomic ,strong) UIImageView *imgView;


#pragma mark 刷新cell
- (CGFloat)resetCellWithModel:(NSString *)title imgStr:(UIImage *)image;

@end
