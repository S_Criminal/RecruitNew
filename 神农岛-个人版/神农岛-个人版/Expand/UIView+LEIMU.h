//
//  UIView+LEIMU.h
//  乐销
//
//  Created by mengxi on 16/10/14.
//  Copyright © 2016年 ping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MacroLocal_h.pch"

@interface UIView (LEIMU)

//添加属性
@property (nonatomic, assign) CGFloat topToUpView;


@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic,assign) CGFloat left;
@property (nonatomic,assign) CGFloat top;
@property (nonatomic,assign) CGFloat height;
@property (nonatomic,assign) CGFloat right;
@property (nonatomic,assign) CGFloat bottom;
@property (nonatomic,assign) CGFloat width;
@property (nonatomic,assign) CGPoint origin;
@property (nonatomic,assign) CGSize size;
@property (nonatomic,assign) CGFloat centerX;
@property (nonatomic,assign) CGFloat centerY;

//组合
@property (nonatomic,assign) STRUCT_XY leftCenterY;
@property (nonatomic,assign) STRUCT_XY leftTop;
@property (nonatomic,assign) STRUCT_XY leftBottom;
@property (nonatomic,assign) STRUCT_XY centerXTop;
@property (nonatomic,assign) STRUCT_XY centerXCenterY;
@property (nonatomic,assign) STRUCT_XY centerXBottom;
@property (nonatomic,assign) STRUCT_XY rightTop;
@property (nonatomic,assign) STRUCT_XY rightCenterY;
@property (nonatomic,assign) STRUCT_XY rightBottom;
//
////宽高
@property (nonatomic,assign) STRUCT_XY widthHeight;

-(void)setCorner:(float)corner;

@end
