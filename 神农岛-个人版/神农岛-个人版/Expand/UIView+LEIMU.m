//
//  UIView+LEIMU.m
//  乐销
//
//  Created by mengxi on 16/10/14.
//  Copyright © 2016年 ping. All rights reserved.
//

#import "UIView+LEIMU.h"

//runtime
#import <objc/runtime.h>
static void *topToUpViewKey = &topToUpViewKey;

@implementation UIView (LEIMU)

@dynamic leftCenterY;
@dynamic leftTop;
@dynamic leftBottom;
@dynamic centerXTop;
@dynamic centerXCenterY;
@dynamic centerXBottom;
@dynamic rightTop;
@dynamic rightCenterY;
@dynamic rightBottom;
@dynamic widthHeight;

#pragma mark 运行时
-(void)setTopToUpView:(CGFloat)topToUpView
{
    objc_setAssociatedObject(self, &topToUpViewKey, [NSNumber numberWithFloat:topToUpView], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(CGFloat)topToUpView
{
    NSNumber * num = objc_getAssociatedObject(self, &topToUpViewKey);
    if (num == nil || ![num isKindOfClass:[NSNumber class]]) {
        return 0;
    }
    return [num floatValue];
}


- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}


- (CGFloat)left
{
    return self.frame.origin.x;
}

- (void)setLeft:(CGFloat)left
{
    CGRect frame = self.frame;
    frame.origin.x = left;
    self.frame = frame;
}

- (CGFloat)top
{
    return self.frame.origin.y;
}

- (void)setTop:(CGFloat)top
{
    CGRect frame = self.frame;
    frame.origin.y = top;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    self.center = center;
}

- (CGFloat)centerX
{
    return self.center.x;
}

- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    self.center = center;
}

- (CGFloat)centerY
{
    return self.center.y;
}

#pragma mark 组合

- (void)setLeftTop:(STRUCT_XY)leftTop{
    self.left = leftTop.horizonX;
    self.top = leftTop.verticalY;
}
- (void)setLeftCenterY:(STRUCT_XY)leftCenterY{
    self.left =  leftCenterY.horizonX;
    self.centerY =  leftCenterY.verticalY;
}
- (void)setLeftBottom:(STRUCT_XY)leftBottom{
    self.left =  leftBottom.horizonX;
    self.bottom =  leftBottom.verticalY;
}
- (void)setCenterXTop:(STRUCT_XY)centerXTop{
    self.centerX =  centerXTop.horizonX;
    self.top =  centerXTop.verticalY;
}
- (void)setCenterXCenterY:(STRUCT_XY)centerXCenterY{
    self.centerX =  centerXCenterY.horizonX;
    self.centerY =  centerXCenterY.verticalY;
}
- (void)setCenterXBottom:(STRUCT_XY)centerXBottom{
    self.centerX =  centerXBottom.horizonX;
    self.bottom =  centerXBottom.verticalY;
}
- (void)setRightTop:(STRUCT_XY)rightTop{
    self.right =  rightTop.horizonX;
    self.top =  rightTop.verticalY;
}
- (void)setRightCenterY:(STRUCT_XY)rightCenterY{
    self.right =  rightCenterY.horizonX;
    self.centerY =  rightCenterY.verticalY;
}
- (void)setRightBottom:(STRUCT_XY)rightBottom{
    self.right =  rightBottom.horizonX;
    self.bottom =  rightBottom.verticalY;
}


#pragma mark 宽高
- (void)setWidthHeight:(STRUCT_XY)widthHeight{
    self.width =  widthHeight.horizonX;
    self.height =  widthHeight.verticalY;
}

- (void)setStructWithHeight:(CGSize)structWithHeight{
    self.width = structWithHeight.width;
    self.height = structWithHeight.height;
}


-(void)setCorner:(float)corner{
    self.layer.cornerRadius=corner;
    self.layer.masksToBounds=YES;
}


@end
