//
//  DiDi+CoreDataProperties.h
//  
//
//  Created by 宋晨光 on 17/1/9.
//
//  This file was automatically generated and should not be edited.
//

#import "DiDi+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DiDi (CoreDataProperties)

+ (NSFetchRequest<DiDi *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *nAddress;
@property (nullable, nonatomic, copy) NSString *nAuthor;
@property (nullable, nonatomic, copy) NSString *nContent;
@property (nullable, nonatomic, copy) NSString *nDescription;
@property (nullable, nonatomic, copy) NSString *nID;
@property (nullable, nonatomic, copy) NSString *nImage;
@property (nullable, nonatomic, copy) NSString *nReadNum;
@property (nullable, nonatomic, copy) NSString *nTime;
@property (nullable, nonatomic, copy) NSString *nTitle;
@property (nullable, nonatomic, copy) NSString *pageIndex;
@property (nullable, nonatomic, copy) NSString *status;

@end

NS_ASSUME_NONNULL_END
