//
//  PositionCoreData+CoreDataProperties.m
//  
//
//  Created by 宋晨光 on 16/12/30.
//
//  This file was automatically generated and should not be edited.
//

#import "PositionCoreData+CoreDataProperties.h"

@implementation PositionCoreData (CoreDataProperties)

+ (NSFetchRequest<PositionCoreData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PositionCoreData"];
}

@dynamic c_Logo;
@dynamic p_Name;
@dynamic c_Name;
@dynamic p_City;
@dynamic c_Time;
@dynamic p_Education;
@dynamic c_meta1;
@dynamic c_meta2;
@dynamic p_Pay;
@dynamic c_Pid;
@dynamic c_Cid;
@dynamic c_Scale;
@dynamic c_Auitstatus;
@dynamic isVideo;
@dynamic p_Exp;
@dynamic pt_Name;
@dynamic pt_NameP;
@dynamic i_Name;
@dynamic i_NameP;
@dynamic status;

@end
