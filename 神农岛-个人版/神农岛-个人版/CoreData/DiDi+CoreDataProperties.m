//
//  DiDi+CoreDataProperties.m
//  
//
//  Created by 宋晨光 on 17/1/9.
//
//  This file was automatically generated and should not be edited.
//

#import "DiDi+CoreDataProperties.h"

@implementation DiDi (CoreDataProperties)

+ (NSFetchRequest<DiDi *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DiDi"];
}

@dynamic nAddress;
@dynamic nAuthor;
@dynamic nContent;
@dynamic nDescription;
@dynamic nID;
@dynamic nImage;
@dynamic nReadNum;
@dynamic nTime;
@dynamic nTitle;
@dynamic pageIndex;
@dynamic status;

@end
