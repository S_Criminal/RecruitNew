//
//  PositionCoreData+CoreDataProperties.h
//  
//
//  Created by 宋晨光 on 16/12/30.
//
//  This file was automatically generated and should not be edited.
//

#import "PositionCoreData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PositionCoreData (CoreDataProperties)

+ (NSFetchRequest<PositionCoreData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *c_Logo;
@property (nullable, nonatomic, copy) NSString *p_Name;
@property (nullable, nonatomic, copy) NSString *c_Name;
@property (nullable, nonatomic, copy) NSString *p_City;
@property (nullable, nonatomic, copy) NSString *c_Time;
@property (nullable, nonatomic, copy) NSString *p_Education;
@property (nullable, nonatomic, copy) NSString *c_meta1;
@property (nullable, nonatomic, copy) NSString *c_meta2;
@property (nullable, nonatomic, copy) NSString *p_Pay;
@property (nullable, nonatomic, copy) NSString *c_Pid;
@property (nullable, nonatomic, copy) NSString *c_Cid;
@property (nullable, nonatomic, copy) NSString *c_Scale;
@property (nullable, nonatomic, copy) NSString *c_Auitstatus;
@property (nullable, nonatomic, copy) NSString *isVideo;
@property (nullable, nonatomic, copy) NSString *p_Exp;
@property (nullable, nonatomic, copy) NSString *pt_Name;
@property (nullable, nonatomic, copy) NSString *pt_NameP;
@property (nullable, nonatomic, copy) NSString *i_Name;
@property (nullable, nonatomic, copy) NSString *i_NameP;
@property (nullable, nonatomic, copy) NSString *status;

@end

NS_ASSUME_NONNULL_END
